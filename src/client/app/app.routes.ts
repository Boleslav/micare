﻿import { Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { CentrePortalRoutes } from './pages/centre-portal/index';
import { ParentsPortalRoutes } from './pages/parents-portal/index';
import { PublicRoutes } from './pages/public/index';

export const routes: Routes = [
	{ path: '', redirectTo: 'centre-portal', pathMatch: 'full' },
	...CentrePortalRoutes,
	...ParentsPortalRoutes,
	...PublicRoutes,
	{
		path: 'hangfireui',
		component: AppComponent,
		resolve: { url: 'hangfireUrlRedirectResolver' }
	},
	{
		path: 'swaggerui',
		component: AppComponent,
		resolve: { url: 'swaggerUrlRedirectResolver' }
	},
	{ path: ':centreName', redirectTo: 'parent-login/:centreName' },
	{ path: '**', component: AppComponent, pathMatch: 'full' }
];
