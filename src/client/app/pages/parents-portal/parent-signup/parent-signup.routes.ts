import { Route } from '@angular/router';
import { ParentSignupComponent } from './index';
import { CentreLogoResolver } from '../index';

export const ParentSignupRoutes: Route[] = [
	{
		path: 'parent-signup/:centreName',
		component: ParentSignupComponent,
		resolve: {
			centreLogo: CentreLogoResolver
		}
	}
];
