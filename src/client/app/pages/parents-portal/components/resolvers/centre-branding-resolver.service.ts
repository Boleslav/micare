import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { CentreService } from '../../index';
import { CentreBrandingDto } from '../../../../api/index';

@Injectable()
export class CentreBrandingResolver implements Resolve<CentreBrandingDto> {
    constructor(private _centreService: CentreService) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<CentreBrandingDto> {
        let centreName = route.params['centreName'];
        if (centreName !== undefined) {
            return this._centreService.getCentreBranding$(centreName);
        } else {
            return Observable.of(null);
        }
    }
}
