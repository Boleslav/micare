import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { CentreService } from '../../index';

@Injectable()
export class CentreExistsResolver implements Resolve<boolean> {
    constructor(private _centreService: CentreService) { }
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> {
        let centreName = route.params['centreName'];
        if (centreName !== undefined) {
            return this._centreService.doesCentreExist$(centreName);
        } else {
            return Observable.of(false);
        }
    }
}
