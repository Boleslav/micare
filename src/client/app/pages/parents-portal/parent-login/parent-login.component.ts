import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { CentreLogoDto } from '../../../api/index';
import { Config } from '../../../shared/index';

@Component({
	moduleId: module.id,
	selector: 'parent-login-cmp',
	templateUrl: 'parent-login.component.html',
	styleUrls: ['parent-login.component.css']
})

export class ParentLoginComponent implements OnInit {

	private logo: string = null;

	constructor(
		private _router: Router,
		private _route: ActivatedRoute) {

		if (Config.ENABLE_ENROLMENT_FEATURE !== 'true') {
			this._router.navigate(['/']);
		}
	}

	ngOnInit(): void {

		this._route
			.data
			.subscribe((data: { centreExists: boolean, centreLogo: CentreLogoDto }) => {

				if (!data.centreExists) {
					let centreName = this._route.snapshot.params['centreName'];
					this._router.navigate(['centre-not-found', centreName]);
				}

				if (data.centreLogo.imageBase64) {
					this.logo = data.centreLogo.imageBase64;
				}
			});
	}
}
