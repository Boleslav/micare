import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/index';

import {
    ParentLoginComponent, ParentLoginFormComponent,
    ParentLoginService
} from './index';

@NgModule({
    imports: [SharedModule],
    declarations: [ParentLoginComponent, ParentLoginFormComponent],
    providers: [ParentLoginService]
})

export class ParentLoginModule { }
