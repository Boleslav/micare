import { Route } from '@angular/router';
import { CentreLogoResolver, CentreExistsResolver } from '../index';
import { ParentLoginComponent } from './index';

export const ParentLoginRoutes: Route[] = [
  	{
        path: 'parent-login/:centreName',
        component: ParentLoginComponent,
        resolve: {
            centreLogo: CentreLogoResolver,
            centreExists: CentreExistsResolver
        }
    }
];
