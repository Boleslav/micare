import { Component, OnInit } from '@angular/core';
import { Params, ActivatedRoute } from '@angular/router';

@Component({
  moduleId: module.id,
  selector: 'centre-not-found-cmp',
  templateUrl: 'centre-not-found.component.html',
  styleUrls: ['centre-not-found.component.css']
})

export class CentreNotFoundComponent implements OnInit {

  private centreNameNotFound: string = null;
  constructor(private _route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this._route
      .params
      .subscribe((params: Params) => {
        let centreName = params['centreName'];
        if (centreName !== undefined && centreName !== null && centreName !== '')
          this.centreNameNotFound = centreName;
      });
  }
}
