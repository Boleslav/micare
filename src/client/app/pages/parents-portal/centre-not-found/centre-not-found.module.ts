import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/index';

import { CentreNotFoundComponent } from './index';

@NgModule({
    imports: [SharedModule],
    declarations: [CentreNotFoundComponent],
})

export class CentreNotFoundModule { }
