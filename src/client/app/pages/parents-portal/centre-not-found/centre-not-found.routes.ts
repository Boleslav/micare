import { Route } from '@angular/router';
import { CentreNotFoundComponent } from './index';

export const CentreNotFoundRoutes: Route[] = [
    {
        path: 'centre-not-found',
        component: CentreNotFoundComponent
    },
    {
        path: 'centre-not-found/:centreName',
        component: CentreNotFoundComponent
    }
];
