export * from './components/services/centre.service';
export * from './components/resolvers/centre-exists-resolver.service';
export * from './components/resolvers/centre-branding-resolver.service';
export * from './components/resolvers/centre-logo-resolver.service';
export * from './parents-portal.component';
export * from './parents-portal.module';
export * from './parents-portal.routing';
