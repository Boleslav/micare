import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../../shared/index';

import { EnrolmentConsentComponent } from './index';

@NgModule({
    imports: [SharedModule],
    declarations: [EnrolmentConsentComponent],
    providers: [],
    exports: [EnrolmentConsentComponent]
})

export class EnrolmentConsentModule { }
