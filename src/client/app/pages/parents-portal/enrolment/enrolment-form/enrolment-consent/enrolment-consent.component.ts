import { Component, ViewEncapsulation, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, AbstractControl, FormBuilder } from '@angular/forms';
import { ToasterService } from 'angular2-toaster';

import { EnrolmentService } from '../../index';
import { EnrolmentFormDto, UpdateEnrolmentFormDto } from '../../../../../api/index';
import { SpinningToast } from '../../../../../shared/index';
import { CentreService } from '../../../index';

@Component({
  moduleId: module.id,
  encapsulation: ViewEncapsulation.None,
  selector: 'enrolment-consent-cmp',
  templateUrl: 'enrolment-consent.component.html',
  styleUrls: ['enrolment-consent.component.css']
})

export class EnrolmentConsentComponent implements OnInit {

  @Output() onEnrolmentFormId = new EventEmitter<string>();
  private enrolmentFormDto: EnrolmentFormDto = null;
  private form: FormGroup;
  private consentPolices: AbstractControl;
  private consentSunHat: AbstractControl;
  private consentCollect: AbstractControl;
  private consentAbsentWhenSick: AbstractControl;
  private consentInformationShare: AbstractControl;
  private consentFees: AbstractControl;
  private consentTrueAndCorrect: AbstractControl;
  private consentIdentity: AbstractControl;

  constructor(private _router: Router,
    private _route: ActivatedRoute,
    private _enrolmentService: EnrolmentService,
    private _centreService: CentreService,
    private _toasterService: ToasterService,
    fb: FormBuilder) {

    this.form = fb.group({
      'consentPolices': [''],
      'consentSunHat': [''],
      'consentCollect': [''],
      'consentAbsentWhenSick': [''],
      'consentInformationShare': [''],
      'consentFees': [''],
      'consentTrueAndCorrect': [''],
      'consentIdentity': ['']
    });

    this.consentPolices = this.form.controls['consentPolices'];
    this.consentSunHat = this.form.controls['consentSunHat'];
    this.consentCollect = this.form.controls['consentCollect'];
    this.consentAbsentWhenSick = this.form.controls['consentAbsentWhenSick'];
    this.consentInformationShare = this.form.controls['consentInformationShare'];
    this.consentFees = this.form.controls['consentFees'];
    this.consentTrueAndCorrect = this.form.controls['consentTrueAndCorrect'];
    this.consentIdentity = this.form.controls['consentIdentity'];
  }

  ngOnInit(): void {
    this._enrolmentService
      .getEnrolmentForm$(this._centreService.SelectedCentreId)
      .subscribe((res: EnrolmentFormDto) => {
        this.enrolmentFormDto = res;
      });
  }

  saveChanges(): void {

    let savingToast = this._toasterService.pop(new SpinningToast());
    let updateEnrolmentForm = new UpdateEnrolmentFormDto();
    updateEnrolmentForm.init(this.enrolmentFormDto.toJSON());
    this._enrolmentService
      .updateEnrolmentForm$(updateEnrolmentForm)
      .subscribe(res => {
        this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
        this._toasterService.popAsync('success', 'Saved', '');
        this.enrolmentFormDto = res;
      },
      (error) => {
        this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
      });
  }
}
