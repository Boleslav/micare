import { Component, ViewEncapsulation, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { EnrolmentService } from '../../index';
import { ParentDto } from '../../../../../api/index';
import { CentreService } from '../../../index';

@Component({
  moduleId: module.id,
  encapsulation: ViewEncapsulation.None,
  selector: 'enrolment-parent-cmp',
  templateUrl: 'enrolment-parent.component.html',
  styleUrls: ['enrolment-parent.component.css']
})

export class EnrolmentParentComponent implements OnInit {

  @Output() onEnrolmentFormId = new EventEmitter<string>();
  private parentDto: ParentDto = null;

  constructor(private _router: Router,
    private _route: ActivatedRoute,
    private _enrolmentService: EnrolmentService,
    private _centreService: CentreService) {
  }

  ngOnInit(): void {
    this._enrolmentService
      .getParent$()
      .subscribe((res: ParentDto) => {
        this.parentDto = res;
        this.onEnrolmentFormId.emit(this.parentDto.enrolmentFormId);
      });
  }

  editParent(): void {
    this._router.navigate(['../parent-edit-enrolment', this.parentDto.parentId], { relativeTo: this._route });
  }
}
