import { Component, ViewEncapsulation, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToasterService } from 'angular2-toaster';

import { EnrolmentService } from '../../index';
import { CentreService } from '../../../index';
import { ParentUserService } from '../../../../../core/index';
import { EnrolmentFormChildCentreDto } from '../../../../../api/index';
import { SpinningToast } from '../../../../../shared/index';

@Component({
  moduleId: module.id,
  encapsulation: ViewEncapsulation.None,
  selector: 'enrolment-kids-cmp',
  templateUrl: 'enrolment-kids.component.html',
  styleUrls: ['enrolment-kids.component.css']
})

export class EnrolmentKidComponent implements OnInit {

  @Input() enrolmentFormId: string;
  private kids: EnrolmentFormChildCentreDto[];
  constructor(private _router: Router,
    private _route: ActivatedRoute,
    private _enrolmentService: EnrolmentService,
    private _userService: ParentUserService,
    private _centreService: CentreService,
    private _toasterService: ToasterService) {
  }

  ngOnInit(): void {
    this.getKids();
  }

  getKids(): void {

    this._enrolmentService
      .getChildApprovalStatusOptions$()
      .flatMap(() => this._enrolmentService.getEnrolmentFormChildren$(this._centreService.SelectedCentreId))
      .subscribe((res: EnrolmentFormChildCentreDto[]) => {
        this.kids = res;
      });
  }

  addKid(): void {
    this._router.navigate(['../kid-edit-enrolment'], { relativeTo: this._route });
  }

  deleteKid(kidId: string): void {
    let savingToast = this._toasterService.pop(new SpinningToast());

    this._enrolmentService
      .deleteKid$(kidId)
      .map(() => {
        this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
        this._toasterService.popAsync('success', 'Saved', '');
        this.kids = null;
      })
      .subscribe(() => this.getKids(),
      (error) => {
        this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
      });
  }

  requestEnrolmentApproval(kidId: string): void {

    let savingToast = this._toasterService.pop(
      new SpinningToast(`Requesting Enrolment at ${this._centreService.SelectedCentreQualifiedName}`));

    this._enrolmentService
      .requestEnrolChild(this._centreService.SelectedCentreId, kidId)
      .subscribe(() => {
        this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
        this._toasterService.popAsync('success', 'Enrolment requested', '');
      },
      (error) => {
        this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
      });
  }
}
