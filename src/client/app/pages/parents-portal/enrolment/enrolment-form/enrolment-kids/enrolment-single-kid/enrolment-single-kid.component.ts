import {
  Component, ViewEncapsulation, EventEmitter,
  Input, Output, OnInit
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { EnrolmentService } from '../../../index';
import { EnrolmentFormChildCentreDto } from '../../../../../../api/index';

@Component({
  moduleId: module.id,
  encapsulation: ViewEncapsulation.None,
  selector: 'enrolment-single-kid-cmp',
  templateUrl: 'enrolment-single-kid.component.html',
  styleUrls: ['enrolment-single-kid.component.css']
})

export class EnrolmentSingleKidComponent implements OnInit {

  @Input() kid: EnrolmentFormChildCentreDto;
  @Output() onDeleteKid = new EventEmitter<string>();
  @Output() onRequestApproval = new EventEmitter<string>();

  private approvalStatuses: { [key: string]: string } = null;

  constructor(private _enrolmentService: EnrolmentService,
    private _router: Router,
    private _route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this._enrolmentService
      .getChildApprovalStatusOptions$()
      .subscribe(res => this.approvalStatuses = res);
  }

  editKid(kidId: string): void {
    this._router.navigate(['../kid-edit-enrolment', kidId], { relativeTo: this._route });
  }

  deleteKid(kidId: string): void {
    this.onDeleteKid.emit(kidId);
  }

  requestEnrolmentApproval(kidId: string): void {

    this.kid.childApprovalStatus = 1;
    this.onRequestApproval.emit(kidId);
  }

  getStatus(childApprovalStatus: number): string {

    if (this.approvalStatuses !== null)
      return (childApprovalStatus === 0) ? 'Request Enrolment' : this.approvalStatuses[childApprovalStatus];
    return '';
  }
}
