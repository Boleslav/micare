import { Component, ViewEncapsulation, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { EnrolmentService } from '../../index';
import { SpinningToast } from '../../../../../shared/index';
import { ParentUserService } from '../../../../../core/index';
import { GuardianDto } from '../../../../../api/index';

@Component({
  moduleId: module.id,
  encapsulation: ViewEncapsulation.None,
  selector: 'enrolment-guardians-cmp',
  templateUrl: 'enrolment-guardians.component.html',
  styleUrls: ['enrolment-guardians.component.css']
})

export class EnrolmentGuardianComponent implements OnInit {

  @Input() enrolmentFormId: string;
  private guardianEnrolments: GuardianDto[] = null;

  constructor(private _router: Router,
    private _route: ActivatedRoute,
    private _enrolmentService: EnrolmentService,
    private _userService: ParentUserService,
    private _toasterService: ToasterService) {
  }

  ngOnInit(): void {
    this.getGuardians();
  }

  getGuardians() {
    this._enrolmentService
      .getGuardians$(this.enrolmentFormId)
      .subscribe(res => {
        this.guardianEnrolments = res;
      });
  }

  addGuardian(): void {
    let navigationExtras: NavigationExtras = {
      queryParams: { 'enrolmentFormId': this.enrolmentFormId },
      relativeTo: this._route
    };
    this._router.navigate(['../guardian-edit-enrolment'], navigationExtras);
  }

  deleteGuardian(guardianId: string): void {

    let savingToast = this._toasterService.pop(new SpinningToast());

    this._enrolmentService
      .deleteGuardian$(guardianId)
      .map(() => {
        this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
        this._toasterService.popAsync('success', 'Saved', '');
        this.guardianEnrolments = null;
      })
      .subscribe(() => this.getGuardians());
  }
}
