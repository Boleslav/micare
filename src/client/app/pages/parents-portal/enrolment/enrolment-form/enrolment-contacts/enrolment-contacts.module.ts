import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../../shared/index';

import { EnrolmentContactComponent, EnrolmentSingleContactComponent } from './index';

@NgModule({
    imports: [SharedModule],
    declarations: [EnrolmentContactComponent, EnrolmentSingleContactComponent],
    providers: [],
    exports: [EnrolmentContactComponent]
})

export class EnrolmentContactModule { }
