﻿import { Component, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { ContactDto } from '../../../../../../api/index';

@Component({
  moduleId: module.id,
  encapsulation: ViewEncapsulation.None,
  selector: 'enrolment-single-contact-cmp',
  templateUrl: 'enrolment-single-contact.component.html',
  styleUrls: ['enrolment-single-contact.component.css']
})

export class EnrolmentSingleContactComponent {

  @Input() contact: ContactDto;
  @Output() onDeleteContact = new EventEmitter<string>();

  constructor(private _router: Router,
    private _route: ActivatedRoute) {
  }

  editContact(contactId: string): void {
    let navigationExtras: NavigationExtras = {
      queryParams: { 'contactId': contactId },
      relativeTo: this._route
    };
    this._router.navigate(['../contact-edit-enrolment'], navigationExtras);
  }

  deleteContact(contactId: string): void {
    this.onDeleteContact.emit(contactId);
  }
}
