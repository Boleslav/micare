import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../shared/index';

import { EnrolmentParentModule } from './enrolment-parent/index';
import { EnrolmentGuardianModule } from './enrolment-guardians/index';
import { EnrolmentKidModule } from './enrolment-kids/index';
import { EnrolmentContactModule } from './enrolment-contacts/index';
import { EnrolmentConsentModule } from './enrolment-consent/index';
import { EnrolmentFormComponent } from './index';

@NgModule({
    imports: [
        SharedModule,
        EnrolmentParentModule,
        EnrolmentGuardianModule,
        EnrolmentKidModule,
        EnrolmentContactModule,
        EnrolmentConsentModule
    ],
    declarations: [EnrolmentFormComponent],
    providers: [],
    exports: [EnrolmentFormComponent]
})

export class EnrolmentFormModule { }
