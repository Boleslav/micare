﻿import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { EnrolmentService } from '../index';
import { CentreService } from '../../index';
import { ParentDto, UpdateParentDto } from '../../../../api/index';
import { ToasterService } from 'angular2-toaster';
import { SpinningToast } from '../../../../shared/index';

@Component({
  moduleId: module.id,
  encapsulation: ViewEncapsulation.None,
  selector: 'parent-edit-enrolment-cmp',
  templateUrl: 'parent-edit-enrolment.component.html',
  styleUrls: ['parent-edit-enrolment.component.css']
})

export class EnrolmentParentEditComponent implements OnInit {
  public maxDateOfBirth: string = null;
  public parentDto: ParentDto = null;
  private form: FormGroup;
  private title: AbstractControl;
  private homePhone: AbstractControl;
  private gender: AbstractControl;
  private dateOfBirth: AbstractControl;
  private nationality: AbstractControl;
  private streetAddress: AbstractControl;
  private suburb: AbstractControl;
  private postcode: AbstractControl;
  private state: AbstractControl;
  private crn: AbstractControl;
  private familyEthinicBackground: AbstractControl;
  private religiousOrCulturalPractices: AbstractControl;
  private predominantLanguagesAtHome: AbstractControl;
  private otherDetails: AbstractControl;

  constructor(private _router: Router,
    private _route: ActivatedRoute,
    private _enrolmentService: EnrolmentService,
    private _centreService: CentreService,
    private _toasterService: ToasterService,
    fb: FormBuilder) {

    let today = new Date();
    this.maxDateOfBirth = new Date(today.getFullYear() - 16, today.getMonth()).toJSON().split('T')[0];

    this.form = fb.group({
      'title': ['', Validators.required],
      'homePhone': ['', Validators.maxLength(12)],
      'gender': ['', Validators.required],
      'dateOfBirth': ['', Validators.required],
      'nationality': ['', Validators.compose([Validators.required, Validators.maxLength(50)])],
      'streetAddress': ['', Validators.compose([Validators.required, Validators.maxLength(200)])],
      'suburb': ['', Validators.compose([Validators.required, Validators.maxLength(50)])],
      'postcode': ['', Validators.compose([Validators.required, Validators.pattern('(\\d{4})')])],
      'state': ['', Validators.compose([Validators.required, Validators.maxLength(50)])],
      'crn': ['', Validators.pattern('[0-9]{9}[a-zA-Z]')],
      'familyEthinicBackground': ['', Validators.maxLength(1000)],
      'religiousOrCulturalPractices': ['', Validators.maxLength(1000)],
      'predominantLanguagesAtHome': ['', Validators.maxLength(1000)],
      'otherDetails': ['', Validators.maxLength(1000)]
    });
    this.title = this.form.controls['title'];
    this.homePhone = this.form.controls['homePhone'];
    this.gender = this.form.controls['gender'];
    this.dateOfBirth = this.form.controls['dateOfBirth'];
    this.nationality = this.form.controls['nationality'];
    this.streetAddress = this.form.controls['streetAddress'];
    this.suburb = this.form.controls['suburb'];
    this.postcode = this.form.controls['postcode'];
    this.state = this.form.controls['state'];
    this.crn = this.form.controls['crn'];
    this.familyEthinicBackground = this.form.controls['familyEthinicBackground'];
    this.religiousOrCulturalPractices = this.form.controls['religiousOrCulturalPractices'];
    this.predominantLanguagesAtHome = this.form.controls['predominantLanguagesAtHome'];
    this.otherDetails = this.form.controls['otherDetails'];
  }

  ngOnInit(): void {

    this._enrolmentService
      .getParent$()
      .subscribe(dto => {
        this.parentDto = dto;
      });
  }

  saveChanges() {
    let savingToast = this._toasterService.pop(new SpinningToast());
    let updateParent = new UpdateParentDto();
    updateParent.init(this.parentDto.toJSON());
    this._enrolmentService
      .updateParent$(updateParent)
      .subscribe(res => {
        this.parentDto = res;
        this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
        this._toasterService.popAsync('success', 'Saved', '');
        this._router.navigate(['../../enrolment-form'], { relativeTo: this._route });
      },
      (error) => {
        this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
      });
  }

  cancelEdit() {
    this._router.navigate(['../../enrolment-form'], { relativeTo: this._route });
  }

  onDateOfBirthChanged(date: Date): void {
    this.parentDto.dateOfBirth = new Date(date.setUTCMinutes(-date.getTimezoneOffset()));
  }
}
