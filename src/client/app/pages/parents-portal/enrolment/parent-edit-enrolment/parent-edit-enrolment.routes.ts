import { Route } from '@angular/router';
import { EnrolmentParentEditComponent } from './index';

export const EnrolmentParentEditRoutes: Route[] = [
  	{
    	path: 'parent-edit-enrolment/:id',
    	component: EnrolmentParentEditComponent
  	}
];
