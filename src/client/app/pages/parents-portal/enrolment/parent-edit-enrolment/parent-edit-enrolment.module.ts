﻿import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../shared/index';

import { EnrolmentParentEditComponent } from './index';

@NgModule({
  imports: [SharedModule],
    declarations: [EnrolmentParentEditComponent],
    exports: [EnrolmentParentEditComponent]
})

export class EnrolmentParentEditModule { }
