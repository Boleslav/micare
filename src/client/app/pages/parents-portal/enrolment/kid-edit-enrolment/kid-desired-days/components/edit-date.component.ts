import { Component, Input } from '@angular/core';
import { DefaultEditor } from '../../../../../../shared/ng2-smart-table/index';

@Component({
    template: `
    <input id="inp-desiredStartDate" type="date" [(ngModel)]="this.cell.newValue" class="form-control" useValueAsDate>
    `
})
export class EditDateComponent extends DefaultEditor {

    @Input() value: string | number;
}
