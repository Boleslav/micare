import { Route } from '@angular/router';
import { EnrolmentKidEditComponent } from './kid-edit-details/index';
import { KidDesiredDaysComponent } from './kid-desired-days/index';
import { KidHealthInfoComponent } from './kid-health-info/index';
import { KidCustodyInfoComponent } from './kid-custody-info/index';

export const EnrolmentKidEditRoutes: Route[] = [
	{
		path: 'kid-edit-enrolment',
		redirectTo: 'kid-edit-details'
	},
	{
		path: 'kid-edit-details',
		component: EnrolmentKidEditComponent
	},
	{
		path: 'kid-edit-details/:kidId',
		component: EnrolmentKidEditComponent
	},
	{
		path: 'kid-desired-days/:kidId',
		component: KidDesiredDaysComponent
	},
	{
		path: 'kid-health-info/:kidId',
		component: KidHealthInfoComponent
	},
	{
		path: 'kid-custody-info/:kidId',
		component: KidCustodyInfoComponent
	}
];
