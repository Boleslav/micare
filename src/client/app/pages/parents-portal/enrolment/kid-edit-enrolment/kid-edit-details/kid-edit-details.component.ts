﻿import { Component, OnInit, ViewEncapsulation, } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs';

import { AboriginalOrTorresValidator } from './index';
import { EnrolmentService } from '../../index';
import { CentreService } from '../../../index';
import { KidDto, CreateUpdateKidDto } from '../../../../../api/index';
import { ToasterService } from 'angular2-toaster';
import { SpinningToast } from '../../../../../shared/index';

@Component({
  moduleId: module.id,
  encapsulation: ViewEncapsulation.None,
  selector: 'kid-edit-details-cmp',
  templateUrl: 'kid-edit-details.component.html',
  styleUrls: ['kid-edit-details.component.css']
})

export class EnrolmentKidEditComponent implements OnInit {

  public maxDateOfBirth: string = null;
  public minDateOfBirth: string = null;
  public kidDto: KidDto = null;

  private form: FormGroup;
  private firstname: AbstractControl;
  private lastname: AbstractControl;
  private gender: AbstractControl;
  private dateOfBirth: AbstractControl;
  private countryOfBirth: AbstractControl;
  private streetAddress: AbstractControl;
  private suburb: AbstractControl;
  private postcode: AbstractControl;
  private state: AbstractControl;
  private crn: AbstractControl;
  private aboriginalOrTorres: FormGroup;
  private aboriginal: AbstractControl;
  private torres: AbstractControl;
  private neitherAboriginalNorTorres: AbstractControl;
  private primaryLanguage: AbstractControl;
  private secondaryLanguage: AbstractControl;

  constructor(private _router: Router,
    private _route: ActivatedRoute,
    private _enrolmentService: EnrolmentService,
    private _centreService: CentreService,
    private _toasterService: ToasterService,
    fb: FormBuilder) {

    let today = new Date();
    this.maxDateOfBirth = new Date().toJSON().split('T')[0];
    this.minDateOfBirth = new Date(today.getFullYear() - 7, today.getMonth()).toJSON().split('T')[0];

    this.form = fb.group({
      'firstname': ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      'lastname': ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      'crn': ['', Validators.compose([Validators.pattern('[0-9]{9}[a-zA-Z]')])],
      'dateOfBirth': ['', Validators.required],
      'gender': ['', Validators.required],
      'countryOfBirth': ['', Validators.maxLength(50)],
      'streetAddress': ['', Validators.compose([Validators.required, Validators.maxLength(200)])],
      'suburb': ['', Validators.compose([Validators.required, Validators.maxLength(50)])],
      'postcode': ['', Validators.compose([Validators.required, Validators.pattern('(\\d{4})')])],
      'state': ['', Validators.compose([Validators.required, Validators.maxLength(50)])],
      'primaryLanguage': ['', Validators.maxLength(50)],
      'secondaryLanguage': ['', Validators.maxLength(50)],
      'aboriginalOrTorres': fb.group({
        'aboriginal': [''],
        'torres': [''],
        'neitherAboriginalNorTorres': [''],
      }, { validator: AboriginalOrTorresValidator.validate('aboriginal', 'torres', 'neitherAboriginalNorTorres') })
    });

    this.firstname = this.form.controls['firstname'];
    this.lastname = this.form.controls['lastname'];
    this.crn = this.form.controls['crn'];
    this.dateOfBirth = this.form.controls['dateOfBirth'];
    this.gender = this.form.controls['gender'];
    this.countryOfBirth = this.form.controls['countryOfBirth'];
    this.streetAddress = this.form.controls['streetAddress'];
    this.suburb = this.form.controls['suburb'];
    this.postcode = this.form.controls['postcode'];
    this.state = this.form.controls['state'];
    this.aboriginalOrTorres = <FormGroup>this.form.controls['aboriginalOrTorres'];
    this.aboriginal = this.aboriginalOrTorres.controls['aboriginal'];
    this.torres = this.aboriginalOrTorres.controls['torres'];
    this.neitherAboriginalNorTorres = this.aboriginalOrTorres.controls['neitherAboriginalNorTorres'];
    this.primaryLanguage = this.form.controls['primaryLanguage'];
    this.secondaryLanguage = this.form.controls['secondaryLanguage'];
  }

  ngOnInit(): void {

    this._route
      .params
      .flatMap((params: Params) => Observable.of(params['kidId']))
      .flatMap(kidId =>
        (kidId === null || kidId === undefined) ?
          Observable.of(new KidDto()) :
          this._enrolmentService.getKid$(kidId))
      .subscribe(res => {
        this.kidDto = res;
        this.neitherAboriginalNorTorres.setValue(this.kidDto.aboriginal === false && this.kidDto.torres === false);
      });
  }

  saveChanges(): void {
    let savingToast = this._toasterService.pop(new SpinningToast());
    let updateKid: CreateUpdateKidDto = new CreateUpdateKidDto();
    updateKid.init(this.kidDto.toJSON());
    this._enrolmentService
      .createUpdateKid$(updateKid)
      .subscribe(res => {
        this.kidDto = res;
        this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
        this._toasterService.popAsync('success', 'Saved', '');

        let url: string = (updateKid.id === undefined) ? '../enrolment-form' : '../../enrolment-form';
        this._router.navigate([url], { relativeTo: this._route });
      },
      (error) => {
        this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
      });
  }

  saveChangesAndContinue(): void {
    let savingToast = this._toasterService.pop(new SpinningToast());
    let updateKid: CreateUpdateKidDto = new CreateUpdateKidDto();
    updateKid.init(this.kidDto.toJSON());
    this._enrolmentService
      .createUpdateKid$(updateKid)
      .subscribe(res => {
        this.kidDto = res;
        this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
        this._toasterService.popAsync('success', 'Saved', '');

        let url: string = (updateKid.id === undefined) ? '../kid-desired-days' : '../../kid-desired-days';
        this._router.navigate([url, this.kidDto.id], { relativeTo: this._route });
      },
      (error) => {
        this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
      });
  }

  cancelEdit() {
    let url: string = this.cancelLink();
    this._router.navigate([url], { relativeTo: this._route });
  }

  cancelLink(): string {
    if (this.kidDto === null)
      return '';
    return (this.kidDto.id === undefined) ? '../enrolment-form' : '../../enrolment-form';
  }

  selectAboriginalOrTorres(checked: boolean): void {
    if (checked) {
      this.neitherAboriginalNorTorres.setValue(false);
    }
  }

  selectNeitherAboriginalOrTorres(checked: boolean): void {
    if (checked) {
      this.aboriginal.setValue(false);
      this.torres.setValue(false);
    }
  }
}
