﻿import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../shared/index';

import { EnrolmentGuardianEditComponent } from './index';

@NgModule({
  imports: [SharedModule],
    declarations: [EnrolmentGuardianEditComponent],
    exports: [EnrolmentGuardianEditComponent]
})

export class EnrolmentGuardianEditModule { }
