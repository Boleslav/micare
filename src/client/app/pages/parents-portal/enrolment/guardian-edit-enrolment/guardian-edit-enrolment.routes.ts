import { Route } from '@angular/router';
import { EnrolmentGuardianEditComponent } from './index';

export const EnrolmentGuardianEditRoutes: Route[] = [
	{
		path: 'guardian-edit-enrolment',
		component: EnrolmentGuardianEditComponent
	}
];
