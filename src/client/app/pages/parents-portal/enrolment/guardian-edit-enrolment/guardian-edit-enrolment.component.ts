﻿import { Component, OnInit, ViewEncapsulation, } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs';

import { EnrolmentService } from '../index';
import { CentreService } from '../../index';
import { GuardianDto, CreateUpdateGuardianDto } from '../../../../api/index';
import { ToasterService } from 'angular2-toaster';
import { SpinningToast } from '../../../../shared/index';
import { EmailValidator, MobileValidator } from '../../../../validators/index';

@Component({
  moduleId: module.id,
  encapsulation: ViewEncapsulation.None,
  selector: 'guardian-edit-enrolment-cmp',
  templateUrl: 'guardian-edit-enrolment.component.html',
  styleUrls: ['guardian-edit-enrolment.component.css']
})

export class EnrolmentGuardianEditComponent implements OnInit {

  public maxDateOfBirth: string = null;
  public guardianDto: GuardianDto = null;

  private form: FormGroup;
  private title: AbstractControl;
  private firstname: AbstractControl;
  private lastname: AbstractControl;
  private mobile: AbstractControl;
  private email: AbstractControl;
  private homePhone: AbstractControl;
  private workPhone: AbstractControl;
  private gender: AbstractControl;
  private dateOfBirth: AbstractControl;
  private nationality: AbstractControl;
  private relationship: AbstractControl;
  private streetAddress: AbstractControl;
  private suburb: AbstractControl;
  private postcode: AbstractControl;
  private state: AbstractControl;
  private crn: AbstractControl;

  private relationshipOptions: { [key: string]: string }[] = new Array<{ [key: string]: string }>();
  private titles: { [key: string]: string }[] = new Array<{ [key: string]: string }>();
  private genders: { [key: string]: string }[] = new Array<{ [key: string]: string }>();

  constructor(private _router: Router,
    private _route: ActivatedRoute,
    private _enrolmentService: EnrolmentService,
    private _centreService: CentreService,
    private _toasterService: ToasterService,
    fb: FormBuilder) {

    let today = new Date();
    this.maxDateOfBirth = new Date(today.getFullYear() - 16, today.getMonth()).toJSON().split('T')[0];

    this.form = fb.group({
      'title': [''],
      'firstname': ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      'lastname': ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      'mobile': ['', MobileValidator.validate],
      'email': ['', Validators.compose([EmailValidator.validate, Validators.maxLength(100)])],
      'homePhone': ['', Validators.maxLength(100)],
      'workPhone': ['', Validators.maxLength(100)],
      'gender': ['',],
      'dateOfBirth': [''],
      'nationality': ['', Validators.maxLength(50)],
      'relationship': ['', Validators.compose([Validators.required, Validators.maxLength(50)])],
      'streetAddress': ['', Validators.maxLength(200)],
      'suburb': ['', Validators.maxLength(50)],
      'postcode': ['', Validators.pattern('(\\d{4})')],
      'state': ['', Validators.maxLength(50)],
      'crn': ['', Validators.pattern('[0-9]{9}[a-zA-Z]')]
    });
    this.title = this.form.controls['title'];
    this.firstname = this.form.controls['firstname'];
    this.lastname = this.form.controls['lastname'];
    this.mobile = this.form.controls['mobile'];
    this.email = this.form.controls['email'];
    this.homePhone = this.form.controls['homePhone'];
    this.workPhone = this.form.controls['workPhone'];
    this.gender = this.form.controls['gender'];
    this.dateOfBirth = this.form.controls['dateOfBirth'];
    this.nationality = this.form.controls['nationality'];
    this.relationship = this.form.controls['relationship'];
    this.streetAddress = this.form.controls['streetAddress'];
    this.suburb = this.form.controls['suburb'];
    this.postcode = this.form.controls['postcode'];
    this.state = this.form.controls['state'];
    this.crn = this.form.controls['crn'];
  }

  ngOnInit(): void {

    this._route
      .queryParams
      .map((params: Params) => params['guardianId'])
      .flatMap(guardianId =>
        (guardianId === null || guardianId === undefined) ?
          Observable.of(null) :
          this._enrolmentService.getGuardian$(guardianId))
      .subscribe(dto => {

        if ((dto !== null && dto !== undefined)) {
          this.guardianDto = dto;
        } else {
          this._route
            .queryParams
            .map((params: Params) => params['enrolmentFormId'])
            .subscribe(enrolmentFormId => {
              if (enrolmentFormId === null || enrolmentFormId === undefined || enrolmentFormId === null)
                throw new Error('Must provide enrolment form Id when creating new guardian');
              this.guardianDto = new GuardianDto();
              this.guardianDto.init({
                EnrolmentFormId: enrolmentFormId
              });
            });
        }
      });

    Observable
      .forkJoin(
      this._enrolmentService.getRelationshipOptions$(),
      this._enrolmentService.getTitles$(),
      this._enrolmentService.getGenders$()
      )
      .subscribe(res => {
        this.relationshipOptions = this._enrolmentService.convertEnumToArray(res[0]);
        this.titles = this._enrolmentService.convertEnumToArray(res[1]);
        this.genders = this._enrolmentService.convertEnumToArray(res[2]);
      });
  }

  saveChanges() {
    let savingToast = this._toasterService.pop(new SpinningToast());
    let updateGuardian = new CreateUpdateGuardianDto();
    updateGuardian.init(this.guardianDto.toJSON());
    this._enrolmentService
      .addOrUpdateGuardian$(updateGuardian)
      .subscribe(res => {
        this.guardianDto = res;
        this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
        this._toasterService.popAsync('success', 'Saved', '');
        this._router.navigate(['../enrolment-form'], { relativeTo: this._route });
      },
      (error) => {
        this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
      });
  }

  cancelEdit() {
    this._router.navigate(['../enrolment-form'], { relativeTo: this._route });
  }
}
