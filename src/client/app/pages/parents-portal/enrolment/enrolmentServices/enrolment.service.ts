﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ParentUserService } from '../../../../core/index';
import {
    ApiClientService, ParentDto, UpdateParentDto,
    KidDto, CreateUpdateKidDto, EnrolmentFormChildCentreDto,
    ContactDto, CreateUpdateContactDto,
    GuardianDto, CreateUpdateGuardianDto,
    EnrolmentFormDesiredDaysDto,
    CreateUpdateEnrolmentFormDesiredDaysDto,
    DeleteEnrolmentFormDesiredDaysDto,
    KidHealthInformationDto, CreateUpdateKidHealthInformationDto,
    EnrolmentFormDto, UpdateEnrolmentFormDto,
    ChildCustodyInformationDto, CreateUpdateChildCustodyInformationDto
} from '../../../../api/index';

@Injectable()
export class EnrolmentService {

    private _accessGranted: boolean = null;
    private relationshipOptions: { [key: string]: string } = null;
    private childApprovalStatusOptions: { [key: string]: string } = null;
    private careTypes: { [key: string]: string } = null;
    private titles: { [key: string]: string } = null;
    private genders: { [key: string]: string } = null;

    constructor(private _apiService: ApiClientService,
        private _parentService: ParentUserService) {
    }

    public enrolmentFormAccessGranted$(centreId: string): Observable<boolean> {
        if (this._accessGranted !== null)
            return Observable.of(this._accessGranted);

        return this._apiService
            .enrolmentForm_IsAccessEnrolmentFormGranted(this._parentService.ParentId, centreId)
            .map(res => this._accessGranted = res);
    }

    public getChildApprovalStatusOptions$(): Observable<{ [key: string]: string }> {
        if (this.childApprovalStatusOptions !== null)
            return Observable.of(this.childApprovalStatusOptions);
        return this._apiService
            .generics_GetEnumDescriptions('ChildApprovalStatusType')
            .map(res => this.childApprovalStatusOptions = res);
    }

    public getRelationshipOptions$(): Observable<{ [key: string]: string }> {
        if (this.relationshipOptions !== null)
            return Observable.of(this.relationshipOptions);
        return this._apiService
            .generics_GetEnumDescriptions('Relationship')
            .map(res => this.relationshipOptions = res);
    }

    public getParent$(): Observable<ParentDto> {
        return this._apiService.parent_GetParentAsync(this._parentService.ParentId);
    }

    public updateParent$(createUpdateParentEnrolmentDto: UpdateParentDto)
        : Observable<ParentDto> {
        return this._apiService.parent_UpdateParentAsync(createUpdateParentEnrolmentDto);
    }

    public getEnrolmentFormChildren$(centreId: string): Observable<EnrolmentFormChildCentreDto[]> {
        return this._apiService.enrolmentForm_GetChildrenForEnrolmentAsync(centreId);
    }

    public getKid$(kidId: string): Observable<KidDto> {
        return this._apiService.child_GetChildAsync(kidId);
    }

    public createUpdateKid$(kidDto: CreateUpdateKidDto): Observable<KidDto> {
        return this._apiService.child_CreateUpdateChildAsync(kidDto);
    }

    public deleteKid$(kidId: string): Observable<void> {
        return this._apiService.child_DeleteChildAsync(kidId);
    }

    public getKidDesiredDays$(kidId: string, centreId: string): Observable<EnrolmentFormDesiredDaysDto[]> {
        return this._apiService.enrolmentForm_GetChildDesiredDays(kidId, centreId);
    }

    public addUpdateKidDesiredDays$(desiredDays: CreateUpdateEnrolmentFormDesiredDaysDto): Observable<EnrolmentFormDesiredDaysDto> {
        return this._apiService.enrolmentForm_AddUpdateChildDesiredDays(desiredDays);
    }

    public deleteKidDesiredDays$(daysToDelete: DeleteEnrolmentFormDesiredDaysDto): Observable<void> {
        return this._apiService.enrolmentForm_DeleteChildDesiredDays(daysToDelete);
    }

    public getKidHealthInformation$(childId: string): Observable<KidHealthInformationDto> {
        return this._apiService.child_GetHealthInformation(childId);
    }

    public addUpdateKidHealthInformationDto$(healthInfo: CreateUpdateKidHealthInformationDto): Observable<KidHealthInformationDto> {
        return this._apiService.child_CreateUpdateHealthInformation(healthInfo);
    }

    public getChildCustodyInformation$(childId: string): Observable<ChildCustodyInformationDto> {
        return this._apiService.child_GetCustodyInformation(childId);
    }

    public addUpdateChildCustodyInformationDto$(
        custodyInfo: CreateUpdateChildCustodyInformationDto): Observable<ChildCustodyInformationDto> {
        return this._apiService.child_CreateUpdateCustodyInformation(custodyInfo);
    }

    public getCareTypes$(): Observable<{ [key: string]: string }> {
        if (this.careTypes !== null)
            return Observable.of(this.careTypes);
        return this._apiService
            .generics_GetEnumDescriptions('CareType')
            .map(res => this.careTypes = res);
    }

    public getTitles$() {
        if (this.titles !== null)
            return Observable.of(this.titles);
        return this._apiService
            .generics_GetEnumDescriptions('NameTitle')
            .map(res => this.titles = res);
    }

    public getGenders$() {
        if (this.genders !== null)
            return Observable.of(this.genders);
        return this._apiService
            .generics_GetEnumDescriptions('Gender')
            .map(res => this.genders = res);
    }

    public getCareTypesForCentre$(centreId: string): Observable<{ [key: string]: string }> {
        return this._apiService
            .room_GetCareTypesForCentre(centreId);
    }

    public getGuardians$(enrolmentFormId: string): Observable<GuardianDto[]> {
        return this._apiService.enrolmentForm_GetGuardians(enrolmentFormId);
    }

    public getGuardian$(guardianId: string): Observable<GuardianDto> {
        return this._apiService.enrolmentForm_GetGuardian(guardianId);
    }

    public addOrUpdateGuardian$(guardianDto: CreateUpdateGuardianDto): Observable<GuardianDto> {
        return this._apiService.enrolmentForm_AddOrUpdate(guardianDto);
    }

    public deleteGuardian$(guardianId: string): Observable<void> {
        return this._apiService.enrolmentForm_Delete(guardianId);
    }

    public getContacts$(enrolmentFormId: string): Observable<ContactDto[]> {
        return this._apiService.enrolmentForm_GetContacts(enrolmentFormId);
    }

    public getContact$(contactId: string): Observable<ContactDto> {
        return this._apiService.enrolmentForm_GetContact(contactId);
    }

    public addOrUpdateContact$(contactDto: CreateUpdateContactDto): Observable<ContactDto> {
        return this._apiService.enrolmentForm_AddOrUpdateContact(contactDto);
    }

    public deleteContact$(contactId: string) {
        return this._apiService.enrolmentForm_DeleteContact(contactId);
    }

    public requestEnrolChild(centreId: string, kidId: string) {
        return this._apiService.enrolmentForm_RequestEnrolChildAsync(centreId, kidId);
    }

    public getEnrolmentForm$(centreId: string): Observable<EnrolmentFormDto> {
        return this._apiService.enrolmentForm_GetEnrolmentForm(this._parentService.ParentId, centreId);
    }

    public updateEnrolmentForm$(updateEnrolmentForm: UpdateEnrolmentFormDto): Observable<EnrolmentFormDto> {
        return this._apiService.enrolmentForm_UpdateEnrolmentForm(updateEnrolmentForm);
    }

    public grantEnrolmentFormAccess$(centreId: string): Observable<void> {
        this._accessGranted = null;
        return this._apiService.enrolmentForm_GrantAccess(centreId);
    }

    public denyEnrolmentFormAccess$(centreId: string): Observable<void> {
        this._accessGranted = null;
        return this._apiService.enrolmentForm_DenyAccess(centreId);
    }

    public clear(): void {
        this._accessGranted = null;
    }

    public convertEnumToArray(en: { [key: string]: string }): { [key: string]: string }[] {
        let arr = new Array<{ [key: string]: string }>();
        for (let key in en) {
            if (en.hasOwnProperty(key)) {
                arr.push({
                    title: en[key],
                    value: key
                });
            }
        }
        return arr;
    }
}
