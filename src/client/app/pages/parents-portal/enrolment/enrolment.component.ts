import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalDirective } from 'ng2-bootstrap';
import { ToasterService } from 'angular2-toaster';
import { SpinningToast } from '../../../shared/index';
import { EnrolmentService } from './index';
import { CentreService } from '../index';

@Component({
  moduleId: module.id,
  selector: 'enrolment-cmp',
  templateUrl: 'enrolment.component.html',
  styleUrls: ['./enrolment.component.css']
})

export class EnrolmentComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('staticModal') public staticModal: ModalDirective;
  private enrolmentFormConsent: boolean = null;
  private centreName: string = null;
  constructor(
    private _enrolmentService: EnrolmentService,
    private _centreService: CentreService,
    private _toasterService: ToasterService,
    private _route: ActivatedRoute,
    private _router: Router) {

  }

  ngOnInit(): void {
    this.centreName = this._centreService.SelectedCentreQualifiedName;
    this._route
      .data
      .subscribe((data: { enrolmentFormConsent: boolean }) => {
        if (data.enrolmentFormConsent) {
          this.enrolmentFormConsent = true;
          this._router.navigate(['enrolment-form'], { relativeTo: this._route });
        } else {
          this.enrolmentFormConsent = false;
        }
      });
  }

  ngAfterViewInit(): void {
    if (!this.enrolmentFormConsent)
      this.staticModal.show();
  }

  ngOnDestroy(): void {
    this._enrolmentService.clear();
  }

  grantAccess(): void {
    this._enrolmentService
      .grantEnrolmentFormAccess$(this._centreService.SelectedCentreId)
      .subscribe(() => {
        this.staticModal.hide();
        this._router.navigate(['enrolment-form'], { relativeTo: this._route });
      });
  }

  denyAccess(): void {
    let savingToast = this._toasterService.pop(new SpinningToast('Logging out'));
    this._enrolmentService
      .denyEnrolmentFormAccess$(this._centreService.SelectedCentreId)
      .subscribe(() => {
        this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
        this._router.navigate(['parent-logout', this._centreService.SelectedCentreUniqueName]);
      });
  }
}
