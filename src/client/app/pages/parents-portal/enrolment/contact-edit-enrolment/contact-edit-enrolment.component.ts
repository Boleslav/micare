﻿import { Component, OnInit, ViewEncapsulation, } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs';

import { EnrolmentService } from '../index';
import { CentreService } from '../../index';
import { ContactDto, CreateUpdateContactDto } from '../../../../api/index';
import { ToasterService } from 'angular2-toaster';
import { SpinningToast } from '../../../../shared/index';
import { EmailValidator, MobileValidator } from '../../../../validators/index';

@Component({
  moduleId: module.id,
  encapsulation: ViewEncapsulation.None,
  selector: 'contact-edit-enrolment-cmp',
  templateUrl: 'contact-edit-enrolment.component.html',
  styleUrls: ['contact-edit-enrolment.component.css']
})

export class EnrolmentContactEditComponent implements OnInit {

  public maxDateOfBirth: string = null;
  public contactDto: ContactDto = null;
  public relationshipOptions: { [key: string]: string }[] = new Array<{ [key: string]: string }>();
  private titles: { [key: string]: string }[] = new Array<{ [key: string]: string }>();
  private genders: { [key: string]: string }[] = new Array<{ [key: string]: string }>();

  private form: FormGroup;
  private title: AbstractControl;
  private firstname: AbstractControl;
  private lastname: AbstractControl;
  private mobile: AbstractControl;
  private email: AbstractControl;
  private homePhone: AbstractControl;
  private gender: AbstractControl;
  private relationship: AbstractControl;
  private streetAddress: AbstractControl;
  private suburb: AbstractControl;
  private postcode: AbstractControl;
  private state: AbstractControl;
  private crn: AbstractControl;

  constructor(private _router: Router,
    private _route: ActivatedRoute,
    private _enrolmentService: EnrolmentService,
    private _centreService: CentreService,
    private _toasterService: ToasterService,
    fb: FormBuilder) {

    let today = new Date();
    this.maxDateOfBirth = new Date(today.getFullYear() - 16, today.getMonth()).toJSON().split('T')[0];

    this.form = fb.group({
      'title': [''],
      'firstname': ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      'lastname': ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      'mobile': ['', Validators.compose([Validators.required, MobileValidator.validate, Validators.maxLength(100)])],
      'email': ['', Validators.compose([Validators.required, EmailValidator.validate, Validators.maxLength(100)])],
      'homePhone': ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      'gender': [''],
      'relationship': ['', Validators.compose([Validators.required, Validators.maxLength(50)])],
      'streetAddress': ['', Validators.maxLength(200)],
      'suburb': ['', Validators.maxLength(50)],
      'postcode': ['', Validators.pattern('(\\d{4})')],
      'state': ['', Validators.maxLength(50)]
    });
    this.title = this.form.controls['title'];
    this.firstname = this.form.controls['firstname'];
    this.lastname = this.form.controls['lastname'];
    this.mobile = this.form.controls['mobile'];
    this.email = this.form.controls['email'];
    this.homePhone = this.form.controls['homePhone'];
    this.gender = this.form.controls['gender'];
    this.relationship = this.form.controls['relationship'];
    this.streetAddress = this.form.controls['streetAddress'];
    this.suburb = this.form.controls['suburb'];
    this.postcode = this.form.controls['postcode'];
    this.state = this.form.controls['state'];
    this.crn = this.form.controls['crn'];
  }

  ngOnInit(): void {

    this._route
      .queryParams
      .map((params: Params) => params['contactId'])
      .flatMap(contactId =>
        (contactId === null || contactId === undefined) ?
          Observable.of(null) :
          this._enrolmentService.getContact$(contactId))
      .subscribe(dto => {

        if ((dto !== null && dto !== undefined)) {
          this.contactDto = dto;
        } else {
          this._route
            .queryParams
            .map((params: Params) => params['enrolmentFormId'])
            .subscribe(enrolmentFormId => {
              if (enrolmentFormId === null || enrolmentFormId === undefined || enrolmentFormId === null)
                throw new Error('Must provide enrolment form Id when creating new contact');
              this.contactDto = new ContactDto();
              this.contactDto.init({
                EnrolmentFormId: enrolmentFormId
              });
            });
        }
      });

    Observable
      .forkJoin(
      this._enrolmentService.getRelationshipOptions$(),
      this._enrolmentService.getTitles$(),
      this._enrolmentService.getGenders$()
      )
      .subscribe(res => {
        this.relationshipOptions = this._enrolmentService.convertEnumToArray(res[0]);
        this.titles = this._enrolmentService.convertEnumToArray(res[1]);
        this.genders = this._enrolmentService.convertEnumToArray(res[2]);
      });
  }

  saveChanges() {
    let savingToast = this._toasterService.pop(new SpinningToast());
    let updateContact = new CreateUpdateContactDto();
    updateContact.init(this.contactDto.toJSON());
    this._enrolmentService
      .addOrUpdateContact$(updateContact)
      .subscribe(res => {
        this.contactDto = res;
        this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
        this._toasterService.popAsync('success', 'Saved', '');
        this._router.navigate(['../enrolment-form'], { relativeTo: this._route });
      },
      (error) => {
        this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
      });
  }

  cancelEdit() {
    this._router.navigate(['../enrolment-form'], { relativeTo: this._route });
  }
}
