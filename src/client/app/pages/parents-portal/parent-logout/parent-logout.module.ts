import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/index';

import { ParentLogoutComponent } from './index';

@NgModule({
    imports: [SharedModule],
    declarations: [ParentLogoutComponent],
    providers: []
})

export class ParentLogoutModule { }
