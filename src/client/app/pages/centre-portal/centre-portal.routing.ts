import { Routes } from '@angular/router';
import { CentrePortalComponent } from './centre-portal.component';
import { CentreAuthGuardService } from '../../auth/index';
import {
    NavigationGuardService, RolesGuardService,
    MixpanelGuard, RedirectComponent
} from '../../shared/index';

import { AdminDashboardRoutes } from './admin-dashboard/index';
import { AdminApprovalsRoutes } from './admin-approvals/index';
import { CentreDashboardRoutes } from './centre-dashboard/index';
import { CompaniesRoutes } from './companies/index';
import { CentreRoutes } from './centre/index';
import { RoomsRoutes } from './rooms/index';
import { StaffRoutes } from './staff/index';
import { BookingsRoutes } from './bookings/index';
import { ShutdownDaysRoutes } from './shutdowndays/index';
import { SwapsRoutes } from './swaps/index';
import { VacanciesRoutes } from './vacancies/index';
import { EnrolmentRoutes } from './enrolment/index';
import { LoginRoutes } from './login/index';
import { LogoutRoutes } from './logout/index';
import { SignupRoutes } from './signup/index';
import { StaffSignupRoutes } from './staff-signup/index';
import { EnrolmentApprovalsRoutes } from './enrolment-approvals/index';
import { FamilyEnrolmentFormRoutes } from './family-enrolment-form/index';
import { ToolsRoutes} from "./tools/tools.routing";

export const CentrePortalRoutes: Routes = [
    {
        path: 'centre-portal',
        component: CentrePortalComponent,
        canActivate: [CentreAuthGuardService, RolesGuardService],
        canActivateChild: [MixpanelGuard],
        children: [
            { path: '', redirectTo: 'centre-dashboard', canActivate: [NavigationGuardService], pathMatch: 'full' },
            ...CentreDashboardRoutes,
            ...StaffRoutes,
            ...CompaniesRoutes,
            ...CentreRoutes,
            ...RoomsRoutes,
            ...BookingsRoutes,
            ...ShutdownDaysRoutes,
            ...SwapsRoutes,
            ...VacanciesRoutes,
            ...EnrolmentRoutes,
            ...AdminDashboardRoutes,
            ...AdminApprovalsRoutes,
            ...EnrolmentApprovalsRoutes,
            ...FamilyEnrolmentFormRoutes,
            ...ToolsRoutes,
            { path: 'redirect/:redirectTo', component: RedirectComponent },
            { path: '**', redirectTo: 'swaps' }
        ]
    },
    ...LoginRoutes,
    ...LogoutRoutes,
    ...SignupRoutes,
    ...StaffSignupRoutes,
];
