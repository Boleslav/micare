import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiClientService, CentreStaffMemberDto } from '../../../../api/index';

@Injectable()
export class StaffService {

    constructor(private _apiService: ApiClientService) {

    }

    public GetCurrentStaffMembersForCentre$(centreId: string): Observable<CentreStaffMemberDto[]> {
        return this._apiService.staff_GetCurrentStaffMembersForCentreAsync(centreId);
    }

    public RemoveCentreStaffMember$(centreId: string, staffMemberId: string): Observable<void> {
        return this._apiService.staff_RemoveCentreStaffMemberAsync(centreId, staffMemberId);
    }

    public InviteStaffMember$(centreId: string, email: string): Observable<void> {
        return this._apiService.account_InviteStaffMember(centreId, email);
    }
}
