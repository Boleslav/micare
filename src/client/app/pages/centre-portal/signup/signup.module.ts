import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/index';

import { SignupComponent, SignupFormComponent, SignupService } from './index';

import { ReCaptchaModule } from 'angular2-recaptcha';


@NgModule({
    imports: [SharedModule,ReCaptchaModule],
    declarations: [SignupComponent, SignupFormComponent],
    providers: [SignupService]
})

export class SignupModule { }
