import { Component, ViewChild } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { EmailValidator, MobileValidator, ComplexPasswordValidator, EqualPasswordsValidator } from '../../../../validators/index';

import { SignupService } from '../signupService/signup.service';
import { ToasterService } from 'angular2-toaster';
import { Config, SpinningToast } from '../../../../shared/index';
import { RegisterCenterAdminModel } from '../../../../api/index';

@Component({
    moduleId: module.id,
    selector: 'signup-form-cmp',
    templateUrl: 'signupForm.component.html'
})

export class SignupFormComponent {

    @ViewChild('recaptcha') recaptcha: any;

    public form: FormGroup;
    public firstName: AbstractControl;
    public lastName: AbstractControl;
    public email: AbstractControl;
    public mobilePhone: AbstractControl;
    public password: AbstractControl;
    public confirmPassword: AbstractControl;
    public passwords: FormGroup;

    public submitted: boolean = false;
    public userCreated: boolean = false;
    public captcha: boolean = false;
    public captchaToken: string = null;
    public captcha_key: string = Config.RECAPTCHA_UI_KEY;
    public captcha_enabled: boolean = this.convertStringToBool(Config.RecaptchaEnabled);

    constructor(fb: FormBuilder,
        private _signupService: SignupService,
        private _toasterService: ToasterService) {

        this.form = fb.group({
            'firstName': ['', Validators.compose([Validators.required, Validators.minLength(1)])],
            'lastName': ['', Validators.compose([Validators.required, Validators.minLength(1)])],
            'email': ['', Validators.compose([Validators.required, EmailValidator.validate])],
            'mobilePhone': ['', Validators.compose([Validators.required, MobileValidator.validate])],
            'passwords': fb.group({
                'password': ['', Validators.compose([Validators.required, Validators.minLength(8), ComplexPasswordValidator.validate])],
                'confirmPassword': ['', Validators.compose([Validators.required, Validators.minLength(8)])]
            }, { validator: EqualPasswordsValidator.validate('password', 'confirmPassword') })
        });

        this.firstName = this.form.controls['firstName'];
        this.lastName = this.form.controls['lastName'];
        this.email = this.form.controls['email'];
        this.mobilePhone = this.form.controls['mobilePhone'];
        this.passwords = <FormGroup>this.form.controls['passwords'];
        this.password = this.passwords.controls['password'];
        this.confirmPassword = this.passwords.controls['confirmPassword'];

        if (this.captcha_enabled === false) {
            //if its disabled let user click button
            this.captcha = true;
        }

    }

    onSubmit(values: any) {

        this.submitted = true;

        let savingToast = this._toasterService.pop(new SpinningToast());

        var newCentreAdmin = new RegisterCenterAdminModel();
        newCentreAdmin.init({
            FirstName: values.firstName,
            LastName: values.lastName,
            Email: values.email,
            MobilePhone: values.mobilePhone,
            Password: values.passwords.password,
            ConfirmPassword: values.passwords.confirmPassword,
            CaptchaToken: this.captchaToken
        });

        this._signupService
            .RegisterCentreAdmin(newCentreAdmin)
            .subscribe(res => {
                this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                this._toasterService.pop('success', 'User Created', '');
                this.userCreated = true;
            },
            err => {
                this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                this.submitted = false; //user needs to be able to change things and try again
                this.recaptcha.reset();
            });
    };

    handleCorrectCaptcha(event: string): void {
        this.captchaToken = event;
        this.captcha = true;
    };

    handleCaptchaExpired(): void {
        this.captchaToken = null;
    }

    convertStringToBool(input: string): boolean {
        var result = false;
        if (input === 'true') {
            result = true;
        }

        return result;
    };
}
