import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiClientService, RegisterCenterAdminModel } from '../../../../api/index';
import {RegisterNewFamilyModel} from "../../../../api/client.service";

@Injectable()
export class SignupService {

    constructor(private _apiClient: ApiClientService) {
    }

    public RegisterCentreAdmin(newCentreAdmin: RegisterCenterAdminModel): Observable<RegisterCenterAdminModel> {
        return this._apiClient
            .account_RegisterCentreAdmin(newCentreAdmin).catch((error: any) => {

                if (error.status === 403 || error.status === 417) {
                    return Observable.throw(error);
                } else {
                    return Observable.throw('Something went wrong, but we are not quite sure what happened..');
                }
            });
    }

    public EnrollFamilies(newFamilies: RegisterNewFamilyModel[]): Observable<any|null> {
        return this._apiClient
            .account_RegisterFamilies(newFamilies).catch((error: any) => {

                if (error.status === 403 || error.status === 417) {
                    return Observable.throw(error);
                } else {
                    return Observable.throw('Something went wrong, but we are not quite sure what happened..');
                }
            });
    }

}
