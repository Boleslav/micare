import { Route } from '@angular/router';
import { SignupComponent } from './index';

import { MixpanelGuard } from '../../../shared/mixpanel/index';

export const SignupRoutes: Route[] = [
  	{
    	path: 'signup',
    	component: SignupComponent,
		canActivate: [MixpanelGuard]
  	}
];
