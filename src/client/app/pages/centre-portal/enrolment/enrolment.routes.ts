import { Route } from '@angular/router';
import { NavigationGuardService } from '../../../shared/index';
import { EnrolmentComponent } from './index';

export const EnrolmentRoutes: Route[] = [
	{
		path: 'enrolment',
		component: EnrolmentComponent,
		canActivate: [NavigationGuardService]
	},
];
