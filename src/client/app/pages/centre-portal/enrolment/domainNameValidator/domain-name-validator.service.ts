import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ApiClientService } from '../../../../api/index';

@Injectable()
export class DomainNameValidatorService {

    private searchTermStream: Subject<string>;

    constructor(private _apiClient: ApiClientService) {
        this.searchTermStream = new Subject<string>();
    }

    public ValidateAsync(name: string): void {
        this.searchTermStream.next(name);
    }

    public isDomainNameValid$(centreId: string): Observable<boolean> {
        return this.searchTermStream
            .debounceTime(300)
            .filter((val) => val !== '')
            .switchMap((term: string) => {
                return this._apiClient
                    .centre_GetDomainPathAvailability(centreId, term);
            });
    }

}
