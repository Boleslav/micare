﻿import { Component, OnInit, ElementRef } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ToasterService } from 'angular2-toaster';
import { NgUploaderOptions } from 'ngx-uploader';
import { ColorPickerService } from 'angular2-color-picker';

import { EnrolmentService, DomainNameValidatorService } from '../index';
import { CreateUpdateCentreBrandingDto, CentreLogoDto } from '../../../../api/index';
import { CentreUserService } from '../../../../core/index';
import { AccessTokenService } from '../../../../core/index';
import { SpinningToast, Config } from '../../../../shared/index';

@Component({
    moduleId: module.id,
    selector: 'enrolment-form-cmp',
    templateUrl: './enrolmentForm.component.html',
    styleUrls: ['enrolmentForm.component.css']
})
export class EnrolmentFormComponent implements OnInit {

    private validatorSubscription: Subscription;
    private originUrl: string = null;
    private form: FormGroup;
    private domainPath: AbstractControl;
    private additionalEnrolmentFormComments: AbstractControl;

    private enrolment: CreateUpdateCentreBrandingDto = null;
    private centreLogo: CentreLogoDto = null;
    private imageUploading: boolean = false;

    private logoName: string = null;
    private logoBase64: string = null;
    private readonly ImageEncoding: string = 'data:image/png;base64,';
    private readonly DefaultColor = '#944ea6';

    private approvalStatus: number;
    private submitted: boolean = false;

    private uploaderOptions: NgUploaderOptions;

    constructor(fb: FormBuilder,
        private _enrolmentService: EnrolmentService,
        private _domainValidator: DomainNameValidatorService,
        private _authService: AccessTokenService,
        private _userService: CentreUserService,
        private _toasterService: ToasterService,
        private _colorPickerService: ColorPickerService,
        private _element: ElementRef) {

        this.originUrl = Config.API + '/';
        this.approvalStatus = -1;

        this.form = fb.group({
            'domainPath': ['',
                Validators.compose([
                    Validators.required,
                    Validators.maxLength(100),
                    Validators.pattern('[A-Za-z0-9]*')])],
            'additionalEnrolmentFormComments': ['', Validators.maxLength(4000)]
        });

        this.domainPath = this.form.controls['domainPath'];
        this.additionalEnrolmentFormComments = this.form.controls['additionalEnrolmentFormComments'];

        this.uploaderOptions = new NgUploaderOptions({
            url: this.originUrl + 'api/Centre/UploadCentreLogo?centreId=' + this._userService.SelectedCentre.id,
            authToken: this._authService.getAccessToken(),
            calculateSpeed: true,
            filterExtensions: true,
            allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
            autoUpload: true
        });
    }

    ngOnInit(): void {

        this.validatorSubscription = this.subscribeDomainNameValidator();
        this._enrolmentService
            .GetCentreBranding$(this._userService.SelectedCentre.id)
            .subscribe(res => {
                this.enrolment = CreateUpdateCentreBrandingDto.fromJS(res.toJSON());
                if (this.enrolment.baseColour === undefined || this.enrolment.baseColour === null) {
                    this.enrolment.baseColour = this.DefaultColor;
                }
            });

        this.getCentreLogo();
    }

    getCentreLogo(): void {
        this.centreLogo = null;
        this._enrolmentService
            .GetCentreLogo$(this._userService.SelectedCentre.id)
            .subscribe(res => {
                this.centreLogo = res;

                if (this.centreLogo.imageBase64 !== undefined &&
                    this.centreLogo.imageBase64 !== null &&
                    this.centreLogo.imageBase64 !== '') {

                    this.logoName = 'Click Here to Upload Logo';
                    this.logoBase64 = this.ImageEncoding + this.centreLogo.imageBase64;
                }
            },
            err => {
                this.centreLogo = new CentreLogoDto();
            });
    }

    validateDomainPath(val: string) {
        if (this.domainPath.errors === null) {
            if (this.validatorSubscription.closed)
                this.validatorSubscription = this.subscribeDomainNameValidator();

            this.domainPath.markAsPending();
            this._domainValidator.ValidateAsync(val);
        } else {
            this.validatorSubscription.unsubscribe();
        }
    }

    subscribeDomainNameValidator(): Subscription {
        return this._domainValidator
            .isDomainNameValid$(this._userService.SelectedCentre.id)
            .subscribe(res => {
                let isValid: boolean = res;
                let validity = isValid ? null : { 'domainPathTaken': true };

                if (this.domainPath.value)
                    this.domainPath.setErrors(validity);
                this.domainPath.markAsDirty();
            });
    }

    hasSuccess(control: AbstractControl): boolean {
        return (control.touched || control.dirty || control.value) && control.valid;
    }

    hasError(control: AbstractControl) {
        return (control.touched || control.dirty) && control.invalid;
    }

    hasWarning(control: AbstractControl) {
        return control.pending;
    }

    onLogoUpload(event: any): void {

        if (!this.imageUploading) {
            this.logoName = 'Click Here to Upload Logo';
            this._toasterService.pop(new SpinningToast());
            this.imageUploading = true;
        }
    }

    onLogoUploadComplete(event: any): void {

        this._toasterService.clear();
        if (event.status > 400) {
            this._toasterService.pop('error', event.statusText);
            this.getCentreLogo();
        } else {
            this._toasterService.pop('success', 'Logo Uploaded');
        }
        this.imageUploading = false;
    }

    onSubmit(): void {

        this.submitted = true;
        let savingToast = this._toasterService.pop(new SpinningToast());

        var centreBrandingDto =
            new CreateUpdateCentreBrandingDto();
        centreBrandingDto.init({
            'CentreId': this._userService.SelectedCentre.id,
            'DomainPath': this.domainPath.value,
            'BaseColour': this.enrolment.baseColour,
            'AdditionalEnrolmentFormComments': this.additionalEnrolmentFormComments.value
        });
        this._enrolmentService
            .CreateUpdateCentreBranding$(centreBrandingDto)
            .subscribe(res => {

                this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                this._toasterService.pop('success', 'Saved');
                this.enrolment = res;
                this.submitted = false;
            },
            (err) => this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId));
    }
}
