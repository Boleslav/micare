import { NgModule } from '@angular/core';
import { ColorPickerModule } from 'angular2-color-picker';

import { SharedModule } from '../../../shared/index';
import {
    EnrolmentComponent, EnrolmentFormComponent,
    EnrolmentService, DomainNameValidatorService
} from './index';

@NgModule({
    imports: [
        SharedModule,
        ColorPickerModule
    ],
    declarations: [
        EnrolmentComponent,
        EnrolmentFormComponent
    ],
    providers: [EnrolmentService, DomainNameValidatorService]
})

export class EnrolmentModule { }
