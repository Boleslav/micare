import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiClientService, ChildPermanentBookingDto, PermanentBookingDto } from '../../../../api/index';

@Injectable()
export class BookingsService {

    constructor(private _apiService: ApiClientService) {
    }

    public GetCentrePermanentBookings$(centreId: string): Observable<ChildPermanentBookingDto[]> {
        return this._apiService.centre_CentreChildrenBookings(centreId);
    }

    public UpdatePermanentBooking$(booking: PermanentBookingDto): Observable<PermanentBookingDto> {
        return this._apiService.child_AddOrUpdatePermantBooking(booking);
    }

    public DeletePermanentBooking$(centreId: string, bookingId: string): Observable<void> {
        return this._apiService.child_DeletePermanentBooking(centreId, bookingId);
    }

    public GetPermanentBookings$(centreId: string, kidId: string): Observable<PermanentBookingDto[]> {
        return this._apiService.child_GetPermanentBookings(centreId, kidId);
    }
}
