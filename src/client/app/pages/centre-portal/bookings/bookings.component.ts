import {Component} from '@angular/core';

@Component({
	moduleId: module.id,
    selector: 'bookings-cmp',
    templateUrl: './bookings.component.html'
})

export class BookingsComponent {}
