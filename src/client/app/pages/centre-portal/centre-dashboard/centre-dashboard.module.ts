import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BsDropdownModule, ModalModule } from 'ng2-bootstrap';

import { SharedModule } from '../../../shared/index';
import { CentreDashboardComponent, CentreDashboardService } from './index';

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        BsDropdownModule,
        ModalModule
    ],
    declarations: [CentreDashboardComponent],
    providers: [CentreDashboardService],
    exports: [CentreDashboardComponent]
})

export class CentreDashboardModule { }
