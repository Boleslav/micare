import { Component, OnInit } from '@angular/core';

import { CentreDashboardService } from './centreDashboardService/centreDashboard.service';
import { CentreUserService } from '../../../core/index';

@Component({
    moduleId: module.id,
    selector: 'centre-dashboard-cmp',
    templateUrl: 'centre-dashboard.component.html'
})

export class CentreDashboardComponent implements OnInit {

    private currentYear = new Date().getFullYear();
    private prevYear = new Date().getFullYear() - 1;
    private nextYear = new Date().getFullYear() + 1;

    private centreName: string;
    private pendingApprovals: number = null;
    private availableDays: number = null;
    private parentCount: number = null;
    private totalIncome: number = null;

    private daysForSale: Array<number[]> = null;
    private daysSold: Array<number[]> = null;
    private monthlyRevenue: Array<number[]> = null;
    private estimatedFutureIncome: Array<number[]> = null;

    constructor(private _userService: CentreUserService,
        private _centreDashboardService: CentreDashboardService) {
        this.centreName = this._userService.SelectedCentre.name;
    }

    ngOnInit() {
        let centreId: string = this._userService.SelectedCentre.id;

        this._centreDashboardService
            .getCentrePendingApprovalsCount(centreId)
            .subscribe(res => this.pendingApprovals = res);

        this._centreDashboardService
            .getCentreAvailableDaysCount(centreId)
            .subscribe(res => this.availableDays = res);

        this._centreDashboardService
            .getCentreParentCount(centreId)
            .subscribe(res => this.parentCount = res);

        this._centreDashboardService
            .getCentreTotalIncome(centreId)
            .subscribe(res => this.totalIncome = res);


        this._centreDashboardService
            .getCentreDashboardData$(centreId)
            .subscribe(res => {
                this.daysForSale = this.mapGraphData(res.daysForSale);
                this.daysSold = this.mapGraphData(res.daysSold);
                this.monthlyRevenue = this.mapGraphData(res.monthlyRevenue);
                this.estimatedFutureIncome = this.mapGraphData(res.estimatedFutureIncome);
                this.initDaySalesChart();
                this.initIncomeChart();
            });
    }

    private mapGraphData(days: { [key: string]: number; }) {
        let arr = new Array<number[]>();
        for (var key in days) {
            if (days.hasOwnProperty(key)) {
                var day: number[] = [new Date(key).getTime(), days[key]];
                arr.push(day);
            }
        }
        return arr;
    }

    private initDaySalesChart(): void {
        var miSwaps: any = $('#miSwaps-table');
        miSwaps.highcharts({
            chart: {
                type: 'spline'
            },
            title: {
                text: 'MiCare ' + this.currentYear
            },
            subtitle: {
                text: 'Number of MiCare days'
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: { // don't display the dummy year
                    month: '%e. %b',
                    year: '%b'
                },
                title: {
                    text: 'Date'
                }
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Total'
                },
                min: 0
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x:%e. %b}: {point.y}'
            },

            plotOptions: {
                spline: {
                    marker: {
                        enabled: true
                    }
                }
            },

            series: [{
                name: 'To Give',
                data: this.daysForSale
            }, {
                name: 'Given',
                data: this.daysSold
            }
            ]
        });
    }

    private initIncomeChart(): void {
        var miSwapsIncome: any = $('#miSwaps-income-table');
        miSwapsIncome.highcharts({
            chart: {
                type: 'column'
            },

            title: {
                text: 'MiCare Income'
            },

            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: { // don't display the dummy year
                    month: '%e. %b',
                    year: '%b'
                },
                title: {
                    text: 'Week'
                }
            },

            yAxis: {
                allowDecimals: true,
                min: 0,
                title: {
                    text: 'Income'
                }
            },

            tooltip: {
                formatter: function () {
                    return '<b>Total<b>: ' + this.point.stackTotal;
                }
            },

            plotOptions: {
                column: {
                    stacking: 'normal'
                }
            },
            series: [{
                name: 'MiCare Income ' + this.prevYear + '-' + this.currentYear,
                data: this.monthlyRevenue,
                stack: 'actualIncome'
            }, {
                name: 'MiCare Projected Income ' + this.currentYear + '-' + this.nextYear,
                data: this.estimatedFutureIncome,
                stack: 'estimatedFutureIncome'
            }
            ]
        });
    }
}
