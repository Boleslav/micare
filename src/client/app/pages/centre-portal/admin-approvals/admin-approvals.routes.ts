import { Route } from '@angular/router';
import { SysAdminGuardService } from '../../../auth/index';
import { AdminApprovalsComponent } from './index';

export const AdminApprovalsRoutes: Route[] = [
	{
		path: 'admin-approvals',
		component: AdminApprovalsComponent,
		canActivate: [SysAdminGuardService]
	},
];
