export let AdminApprovalsFilter: any = {
    'Awaiting Approval': 0,
    'Denied': 1,
    'Approved': 2,
    'Unclaimed': 3,
    'Show All': 4
};
