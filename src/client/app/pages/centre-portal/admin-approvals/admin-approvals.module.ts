import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/index';
import { AdminApprovalsComponent, AdminApprovalsTableComponent, AdminApprovalsService } from './index';

@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [AdminApprovalsComponent, AdminApprovalsTableComponent],
    providers: [AdminApprovalsService],
    exports: [AdminApprovalsComponent, AdminApprovalsTableComponent]
})

export class AdminApprovalsModule { }
