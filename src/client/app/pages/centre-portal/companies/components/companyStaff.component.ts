import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';

import { Observable } from 'rxjs';
import { CompaniesService } from '../index';
import { LocalDataSource } from '../../../../shared/ng2-smart-table/ng2-smart-table/lib/data-source/local/local.data-source';
import { ToasterService } from 'angular2-toaster';
import { SpinningToast } from '../../../../shared/index';
import { CompanyDto } from '../../../../api/index';
import {CompanyFacilityViewModel} from "../../../../api/client.service";

@Component({
    moduleId: module.id,
    selector: 'company-staff-cmp',
    templateUrl: './companyStaff.component.html'
})

export class CompanyStaffComponent implements OnInit {

    private companyId: string;

    private staff: CompanyFacilityViewModel[];
    private company: CompanyDto = new CompanyDto();

    // staff search
    private typeaheadLoading: boolean;
    private typeaheadNoResults: boolean;
    private staffToAdd: CompanyFacilityViewModel[] = null;
    private selectedStaff: CompanyFacilityViewModel = null;
    private dataSource: Observable<CompanyFacilityViewModel[]>;
    private searchStaff: string = '';
    private isSysAdmin: boolean = false;


  constructor(private route: ActivatedRoute,
                private _companiesService: CompaniesService,
                // private _staffService: StaffService,
        private _toasterService: ToasterService) {

      this.route.params
        .map(params => params['companyId'])
        .subscribe((id) => {
          this.companyId = id;
          // this._staffService.GetCompanyStaff(id)
          //   .subscribe(staff => this.staff = staff);

          this._companiesService.GetCompany$(id)
            .subscribe(company => this.company = company);
        });

      // this.dataSource = Observable
      //   .create((observer: any) => {
      //     observer.next(this.searchStaff);
      //   })
      //   .mergeMap((searchTerm: string) => this._staffService.GetUnassignedStaff(searchTerm));

  }

    ngOnInit(): void {
        // this._companiesService.GetCompanies$()
        //   .subscribe(res => {
        //     this.initSettings();
        //     this.companies = res;
        //     this.source.load(this.companies);
        //   });
    }

  selectStaff(staff: CompanyFacilityViewModel) {
    this.selectedStaff = staff;
  }

  addStaff() {
    let staff = this.selectedStaff;
    if (!staff) {
      return;
    }

/*
    this.setActiveEx(staff, false)
      .subscribe(() => {
        let c = new CompanyFacilityViewModel();
        c.facilityId = staff.facilityId;
        c.facilityName = staff.facilityName;
        c.companyId = this.companyId;
        c.isActive = false;
        this.staff.push(c);

        this.selectedStaff = null;

      })
*/
  }

  setActive(staff: CompanyFacilityViewModel, setActive: boolean) {
    // this.setActiveEx(staff, setActive)
    //   .subscribe(() => {
    //     staff.isActive = setActive;
    //   });
  }

  setActiveEx(staff: CompanyFacilityViewModel, setActive: boolean) {
    // return this._staffService.SetActiveCompanyStaff(this.companyId, staff.facilityId, setActive);
  }

  public changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }

  public changeTypeaheadNoResults(e: boolean): void {
    this.typeaheadNoResults = e;
  }

  typeaheadOnSelect(e: TypeaheadMatch): void {
    let staff: CompanyFacilityViewModel = <CompanyFacilityViewModel>e.item;
    this.selectStaff(staff);
  }

}
