import { Component, OnInit } from '@angular/core';

import { CompaniesService, CompanyExclussiveStatusCellRenderer, CompanyButtonsCellRenderer } from '../index';
import { LocalDataSource } from '../../../../shared/ng2-smart-table/ng2-smart-table/lib/data-source/local/local.data-source';
import { ToasterService } from 'angular2-toaster';
import { SpinningToast } from '../../../../shared/index';
import { CompanyDto } from '../../../../api/index';

@Component({
    moduleId: module.id,
    selector: 'companies-table-cmp',
    templateUrl: './companiesTable.component.html'
})

export class CompaniesTableComponent implements OnInit {

    private settings: any;

    private companies: CompanyDto[];
    private source: LocalDataSource = new LocalDataSource();
    constructor(private _companiesService: CompaniesService,
        private _toasterService: ToasterService) {
    }

    ngOnInit(): void {
        this._companiesService.GetCompanies$()
          .subscribe(res => {
            this.initSettings();
            this.companies = res;
            this.source.load(this.companies);
          });
    }

    addCompany(event: any): void {
        this.createUpdateCompany(event);
    }

    editCompany(event: any): void {
        this.createUpdateCompany(event);
    }

    createUpdateCompany(event: any) {

        if (event.newData.name === '') {

            this._toasterService
                .popAsync('error', 'Missing Information', 'All fields must be provided');
            event.confirm.reject();

        } else {
            let savingToast = this._toasterService.pop(new SpinningToast());
            let createCompanyDto = new CompanyDto();
            createCompanyDto.init({
                Id: event.newData.id,
                Name: event.newData.name,
                IsExclussiveNetwork: event.newData.isExclussiveNetwork
            });
            this._companiesService.CreateUpdateCompany$(createCompanyDto)
                .toPromise()
                .then((res) => {
                    this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                    this._toasterService.popAsync('success', 'Saved', '');
                    return event.confirm.resolve(res);
                })
                .catch((error) => {
                    this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                    return event.confirm.reject();
                });
        }
    }

    removeCompany(event: any): void {
        alert('That\'s not possible to delete company directly. Please ask your database administrator for that!');
    }


    private initSettings(): void {
        this.settings = {
            mode: 'internal',
            hideHeader: false,
            hideSubHeader: false,
            sort: false,
            actions: {
                columnTitle: '',
                add: true,
                edit: true,
                delete: true,
                position: 'right'
            },
            add: {
                addButtonContent: 'Add Company',
                createButtonContent: 'Create',
                cancelButtonContent: 'Cancel',
                confirmCreate: true
            },
            edit: {
                editButtonContent: 'Edit Company',
                saveButtonContent: 'Save',
                cancelButtonContent: 'Cancel',
                confirmSave: true
            },
            delete: {
                deleteButtonContent: 'Remove Company',
                confirmDelete: true
            },
            columns: {
                name: {
                    title: 'Company Name',
                    filter: false
                },
                isExclussiveNetwork: {
                    title: 'Is Exclussive',
                    filter: false,
                    type: 'custom',
                    renderComponent: CompanyExclussiveStatusCellRenderer,
                    editor: {
                      type: 'custom',
                      component: CompanyExclussiveStatusCellRenderer
                    }
                },
                id: {
                  type: 'custom',
                  renderComponent: CompanyButtonsCellRenderer,
                  editor: {
                    type: 'custom',
                    component: CompanyButtonsCellRenderer
                  }
                }
            },
            noDataMessage: 'No Companies'
        };
    }
}
