import { Route } from '@angular/router';

import { NavigationGuardService } from '../../../shared/index';
import { CompaniesComponent, CompanyCentresComponent, CompanyStaffComponent } from './index';

export const CompaniesRoutes: Route[] = [
	{
		path: 'companies',
		component: CompaniesComponent,
		canActivate: [NavigationGuardService],
    children: [

    ]
	},
  {
    path: 'companies/:companyId/centres',
    component: CompanyCentresComponent,
    canActivate: [NavigationGuardService],
  },
  {
    path: 'companies/:companyId/staff',
    component: CompanyStaffComponent,
    canActivate: [NavigationGuardService],
  }
];
