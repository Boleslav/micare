import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/index';

import { LogoutComponent } from './index';

@NgModule({
    imports: [SharedModule],
    declarations: [LogoutComponent],
    providers: []
})

export class LogoutModule { }
