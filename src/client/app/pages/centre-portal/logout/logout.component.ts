import { Component, OnInit } from '@angular/core';

import { CentreUserService } from '../../../core/index';
import { AccessTokenService } from '../../../core/index';
import { Config, IntercomService } from '../../../shared/index';

@Component({
    moduleId: module.id,
    selector: 'logout-cmp',
    templateUrl: 'logout.component.html'
})

export class LogoutComponent implements OnInit {

    constructor(
        private _authenticationService: AccessTokenService,
        private _intercomService: IntercomService,
        private _userService: CentreUserService) {
    }

    ngOnInit() {
        this._authenticationService.logout();
        this._userService.SetLastSelectedCentreId(this._userService.SelectedCentre.id);
        this._userService.Clear();
        this._intercomService.userLoggedOut(Config.INTERCOM_APP_ID);
    }
}
