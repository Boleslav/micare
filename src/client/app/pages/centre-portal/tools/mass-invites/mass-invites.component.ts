import {Component} from '@angular/core';
import {LocalDataSource} from "../../../../shared/ng2-smart-table/ng2-smart-table/lib/data-source/local/local.data-source";
import {SignupService} from "../../signup/signupService/signup.service";
import { ToasterService } from 'angular2-toaster';
import {KidCreateViewModel, RegisterNewFamilyModel} from "../../../../api/client.service";
import {CentreUserService} from "../../../../core/centre-user.service";

@Component({
	moduleId: module.id,
    selector: 'mass-invites-cmp',
    templateUrl: './mass-invites.component.html'
})

export class ToolsMassInvitesComponent {
  private settings: any;

  private families: any[];
  private source: LocalDataSource = new LocalDataSource();
  private inputContent: string = '';

  isProcessing: boolean;

  static Family = class {
    parentEmail: string;
    parentFirstName: string;
    parentLastName: string;

    childFirstName: string;
    childLastName: string;
    childDob: string;
  }

  constructor(private _signupService: SignupService,
              private _toasterService: ToasterService,
              private _userService: CentreUserService
  ) {

    this.initSettings();
  }

  importData () {
    this.families = [];

    var records = this.inputContent.split('\n');
    records.forEach(r => {
      var cells = r.split('\t');

      var family = new ToolsMassInvitesComponent.Family();
      family.parentFirstName = cells[4] || '';
      family.parentLastName = cells[5] || '';
      family.parentEmail = cells[6] || '';

      family.childFirstName = cells[1] || '';
      family.childLastName = cells[2] || '';
      family.childDob = cells[3] || '';

      this.families.push(family);
    });

    this.source.load(this.families);
    this.inputContent = '';
  }

  processEnrollment() {
    if(!this.families.length)
      return;

    var familiesToSend: RegisterNewFamilyModel[] = [];
    this.families.forEach(f => {
      if (!f.parentEmail)
        return;

      var parent = new RegisterNewFamilyModel();
      parent.firstName = f.parentFirstName;
      parent.lastName = f.parentLastName;
      parent.email = f.parentEmail;

      var kid = new KidCreateViewModel();
      kid.firstName = f.childFirstName;
      kid.lastName = f.childLastName;
      kid.dob = new Date(f.childDob);

      kid.facilityId = parseInt(this._userService.SelectedCentre.id);

      parent.kids = [kid];

      familiesToSend.push(parent);
    });

    this.isProcessing = true;
    this._signupService.EnrollFamilies(familiesToSend)
      .subscribe(() =>  {
          this._toasterService.popAsync('success', 'Process finished successfully', '');
          this.families = [];
          this.source.load(this.families);
          this.isProcessing = false;
        },
        () => this.isProcessing = false
      );
  }
  addPerson(event: any): void {
    this.createUpdatePerson(event);
  }

  editPerson(event: any): void {
    this.createUpdatePerson(event);
  }

  createUpdatePerson(event: any) {

  }

  removePerson(event: any): void {

  }


  private initSettings(): void {
    this.settings = {
      mode: 'internal',
      hideHeader: false,
      hideSubHeader: true,
      sort: false,
      actions: false/*{
        columnTitle: '',
        add: false,
        // edit: true,
        delete: true,
        position: 'right'
      }*/,
      // add: {
      //   addButtonContent: 'Add Person',
      //   createButtonContent: 'Create',
      //   cancelButtonContent: 'Cancel',
      //   confirmCreate: true
      // },
      // edit: {
      //   editButtonContent: 'Edit Person',
      //   saveButtonContent: 'Save',
      //   cancelButtonContent: 'Cancel',
      //   confirmSave: true
      // },
      delete: {
        deleteButtonContent: 'Remove Person',
        confirmDelete: true
      },
      columns: {
        childLastName: {
          title: 'Child Family Name',
          filter: false
        },
        childFirstName: {
          title: 'Child First Name',
          filter: false
        },
        childDob: {
          title: 'Child DOB',
          filter: false
        },
        parentFirstName: {
          title: 'Parent First Name',
          filter: false
        },
        parentLastName: {
          title: 'Parent Family Name',
          filter: false
        },
        parentEmail: {
          title: 'Parent Email',
          filter: false
        },
      },
      noDataMessage: 'No Families to Enroll Provided'
    };
  }
}
