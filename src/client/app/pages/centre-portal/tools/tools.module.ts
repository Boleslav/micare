import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/index';
import {ToolsDefaultComponent, ToolsMassInvitesComponent} from "./index";

@NgModule({
    imports: [
      SharedModule
    ],
    declarations: [
        ToolsDefaultComponent,
        ToolsMassInvitesComponent,
    ]
})
export class ToolsModule { }
