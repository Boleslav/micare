import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    moduleId: module.id,
    encapsulation: ViewEncapsulation.None,
    selector: 'tools-default-cmp',
    templateUrl: 'tools-default.component.html',
    styleUrls: ['tools-default.component.css']
})
export class ToolsDefaultComponent {

    constructor() {

    }
}
