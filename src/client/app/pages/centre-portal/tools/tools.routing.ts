import { Routes } from '@angular/router';
import { CentrePortalComponent } from '../centre-portal.component';
import { CentreAuthGuardService } from '../../../auth/index';
import {
    NavigationGuardService, RolesGuardService,
    MixpanelGuard, RedirectComponent
} from '../../../shared/index';



import {ToolsDefaultComponent, ToolsMassInvitesComponent} from "./index";

export const ToolsRoutes: Routes = [
    {
        path: 'tools',
        component: ToolsDefaultComponent,
        canActivate: [CentreAuthGuardService, RolesGuardService],
        canActivateChild: [MixpanelGuard],
        children: [
        ]
    },
    {
      path: 'tools/mass-invites',
      canActivate: [NavigationGuardService, RolesGuardService],
      component: ToolsMassInvitesComponent
    }
];
