import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { FamilyEnrolmentService } from '../index';
import { ContactDto } from '../../../../api/index';

@Component({
    moduleId: module.id,
    selector: 'contact-view-cmp',
    templateUrl: 'contact-view.component.html'
})

export class ContactViewComponent implements OnInit {

    @Input('contact') contact: ContactDto;
    private titles: { [key: string]: string } = {};
    private genders: { [key: string]: string } = {};
    private relationships: { [key: string]: string } = {};
    constructor(private _enrolmentService: FamilyEnrolmentService) { ; }

    ngOnInit(): void {
        Observable.forkJoin(
            this._enrolmentService.getTitles$(),
            this._enrolmentService.getGenders$(),
            this._enrolmentService.getRelationships$())
            .subscribe(res => {
                this.titles = res[0];
                this.genders = res[1];
                this.relationships = res[2];
            });
    }
}
