import { Component, OnInit, Input } from '@angular/core';
import { EnrolmentFormDto } from '../../../../api/index';

@Component({
    moduleId: module.id,
    selector: 'consent-view-cmp',
    templateUrl: 'consent-view.component.html'
})

export class ConsentViewComponent implements OnInit {

    @Input('consent') consent: EnrolmentFormDto;
    constructor() { ; }

    ngOnInit(): void { ; }
}
