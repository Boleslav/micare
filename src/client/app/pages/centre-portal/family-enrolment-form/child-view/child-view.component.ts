import { Component, OnInit, Input } from '@angular/core';
import { ChildEnrolmentFormViewModel } from '../../../../api/index';

@Component({
    moduleId: module.id,
    selector: 'child-view-cmp',
    templateUrl: 'child-view.component.html'
})

export class ChildViewComponent implements OnInit {

    @Input('child') child: ChildEnrolmentFormViewModel;
    constructor() { ; }

    ngOnInit(): void { ; }
}
