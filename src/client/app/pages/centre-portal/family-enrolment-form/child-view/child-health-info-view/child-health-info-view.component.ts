import { Component, OnInit, Input } from '@angular/core';
import { KidHealthInformationDto } from '../../../../../api/index';

@Component({
    moduleId: module.id,
    selector: 'child-health-info-view-cmp',
    templateUrl: 'child-health-info-view.component.html'
})

export class ChildHealthInfoViewComponent implements OnInit {

    @Input('healthInfo') healthInfo: KidHealthInformationDto;
    constructor() { ; }

    ngOnInit(): void { ; }
}
