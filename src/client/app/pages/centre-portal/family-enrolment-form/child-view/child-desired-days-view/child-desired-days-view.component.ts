import { Component, OnInit, Input } from '@angular/core';
import { EnrolmentFormDesiredDaysDto } from '../../../../../api/index';
import { FamilyEnrolmentService } from '../../index';

@Component({
    moduleId: module.id,
    selector: 'child-desired-days-view-cmp',
    templateUrl: 'child-desired-days-view.component.html'
})

export class ChildDesiredDaysViewComponent implements OnInit {

    @Input('desiredDays') desiredDays: EnrolmentFormDesiredDaysDto[];
    private careTypes: { [key: string]: string } = {};
    constructor(private _enrolmentService: FamilyEnrolmentService) { ; }

    ngOnInit(): void {
        this._enrolmentService.getCareTypes$()
            .subscribe(res => this.careTypes = res);
    }
}
