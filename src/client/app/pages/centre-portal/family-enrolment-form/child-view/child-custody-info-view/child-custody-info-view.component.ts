import { Component, OnInit, Input } from '@angular/core';
import { ChildCustodyInformationDto } from '../../../../../api/index';

@Component({
    moduleId: module.id,
    selector: 'child-custody-info-view-cmp',
    templateUrl: 'child-custody-info-view.component.html'
})

export class ChildCustodyInfoViewComponent implements OnInit {

    @Input('custodyInfo') custodyInfo: ChildCustodyInformationDto;
    constructor() { ; }

    ngOnInit(): void { ; }
}
