import { Component, OnInit, Input } from '@angular/core';
import { FamilyEnrolmentService } from '../../index';
import { KidDto } from '../../../../../api/index';

@Component({
    moduleId: module.id,
    selector: 'child-details-view-cmp',
    templateUrl: 'child-details-view.component.html'
})

export class ChildDetailsViewComponent implements OnInit {

    @Input('child') child: KidDto;
    private genders: { [key: string]: string } = {};
    constructor(private _enrolmentService: FamilyEnrolmentService) { ; }

    ngOnInit(): void {
        this._enrolmentService
            .getGenders$()
            .subscribe(res => this.genders = res);
    }
}
