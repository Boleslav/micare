import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { ParentDto } from '../../../../api/index';
import { FamilyEnrolmentService } from '../index';

@Component({
    moduleId: module.id,
    selector: 'parent-view-cmp',
    templateUrl: 'parent-view.component.html'
})

export class ParentViewComponent implements OnInit {

    @Input() parent: ParentDto;
    private titles: { [key: string]: string } = {};
    private genders: { [key: string]: string } = {};

    constructor(private _enrolmentService: FamilyEnrolmentService) { ; }

    ngOnInit(): void {
        Observable.forkJoin(
            this._enrolmentService.getTitles$(),
            this._enrolmentService.getGenders$())
            .subscribe(res => {
                this.titles = res[0];
                this.genders = res[1];
            });
    }
}
