import { Route } from '@angular/router';
import { FamilyEnrolmentFormComponent } from './index';

export const FamilyEnrolmentFormRoutes: Route[] = [
  	{
        path: 'family-enrolment-form/:parentId',
        component: FamilyEnrolmentFormComponent
    }
];
