import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiClientService, CompleteEnrolmentFormViewModel } from '../../../../api/index';

@Injectable()
export class FamilyEnrolmentService {

    private titles: { [key: string]: string } = null;
    private genders: { [key: string]: string } = null;
    private careTypes: { [key: string]: string } = null;
    private relationships: { [key: string]: string } = null;


    constructor(private _apiService: ApiClientService) {
    }

    public getTitles$() {
        if (this.titles !== null)
            return Observable.of(this.titles);
        return this._apiService
            .generics_GetEnumDescriptions('NameTitle')
            .map(res => this.titles = res);
    }

    public getGenders$() {
        if (this.genders !== null)
            return Observable.of(this.genders);
        return this._apiService
            .generics_GetEnumDescriptions('Gender')
            .map(res => this.genders = res);
    }

    public getCareTypes$() {
        if (this.careTypes !== null)
            return Observable.of(this.careTypes);
        return this._apiService
            .generics_GetEnumDescriptions('CareType')
            .map(res => this.careTypes = res);
    }

    public getRelationships$() {
        if (this.relationships !== null)
            return Observable.of(this.relationships);
        return this._apiService
            .generics_GetEnumDescriptions('Relationship')
            .map(res => this.relationships = res);
    }

    public getEnrolmentForm$(parentId: string, centreId: string): Observable<CompleteEnrolmentFormViewModel> {
        return this._apiService.enrolmentForm_GetCompleteEnrolmentForm(parentId, centreId);
    }
}
