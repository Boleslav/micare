import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { MobileValidator, ComplexPasswordValidator, EqualPasswordsValidator } from '../../../../validators/index';

import { StaffSignupService } from '../staffSignupService/staffSignup.service';
import { ToasterService } from 'angular2-toaster';
import { SpinningToast } from '../../../../shared/index';
import { AcceptStaffInviteDto } from '../../../../api/index';

@Component({
    moduleId: module.id,
    selector: 'staff-signup-form-cmp',
    templateUrl: 'staffSignupForm.component.html'
})

export class StaffSignupFormComponent implements OnInit {
    private token: string;
    private email: string;

    private form: FormGroup;
    private firstName: AbstractControl;
    private lastName: AbstractControl;
    private mobilePhone: AbstractControl;
    private password: AbstractControl;
    private confirmPassword: AbstractControl;
    private passwords: FormGroup;

    private submitted: boolean = false;
    private userCreated: boolean = false;

    constructor(fb: FormBuilder,
        private _staffSignupService: StaffSignupService,
        private _toasterService: ToasterService,
        private _route: ActivatedRoute) {

        this.form = fb.group({
            'firstName': ['', Validators.compose([Validators.required, Validators.minLength(1)])],
            'lastName': ['', Validators.compose([Validators.required, Validators.minLength(1)])],
            'mobilePhone': ['', Validators.compose([Validators.required, MobileValidator.validate])],
            'passwords': fb.group({
                'password': ['', Validators.compose([Validators.required, Validators.minLength(8), ComplexPasswordValidator.validate])],
                'confirmPassword': ['', Validators.compose([Validators.required, Validators.minLength(8)])]
            }, { validator: EqualPasswordsValidator.validate('password', 'confirmPassword') })
        });

        this.firstName = this.form.controls['firstName'];
        this.lastName = this.form.controls['lastName'];
        this.mobilePhone = this.form.controls['mobilePhone'];
        this.passwords = <FormGroup>this.form.controls['passwords'];
        this.password = this.passwords.controls['password'];
        this.confirmPassword = this.passwords.controls['confirmPassword'];
    }

    ngOnInit(): void {
        this._route
            .params
            .switchMap((params: Params) => {
                this.token = params['token'];
                return this._staffSignupService.GetStaffMemberInvite$(this.token);
            }).subscribe(res => {
                this.email = res;
            });
    }

    onSubmit(values: any) {

        this.submitted = true;

        let savingToast = this._toasterService.pop(new SpinningToast());

        var acceptStaffInvite = new AcceptStaffInviteDto();
        acceptStaffInvite.init({
            FirstName: values.firstName,
            LastName: values.lastName,
            Email: this.email,
            MobilePhone: values.mobilePhone,
            Password: values.passwords.password,
            ConfirmPassword: values.passwords.confirmPassword,
            Token: this.token
        });

        this._staffSignupService
            .AcceptStaffMemberInvite$(acceptStaffInvite)
            .subscribe(res => {
                this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                this._toasterService.pop('success', 'Staff Member Created', '');
                this.userCreated = true;
            },
            err => {
                this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                this.submitted = false; //user needs to be able to change things and try again
            });
    };
}
