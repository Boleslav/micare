import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/index';

import { StaffSignupComponent, StaffSignupFormComponent, StaffSignupService } from './index';

@NgModule({
    imports: [SharedModule],
    declarations: [StaffSignupComponent, StaffSignupFormComponent],
    providers: [StaffSignupService]
})

export class StaffSignupModule { }
