import { Route } from '@angular/router';
import { StaffSignupComponent } from './index';

export const StaffSignupRoutes: Route[] = [
  	{
    	path: 'staff-signup/:token',
    	component: StaffSignupComponent
  	}
];
