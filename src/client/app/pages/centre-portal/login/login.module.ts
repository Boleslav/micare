import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/index';

import { LoginComponent, LoginFormComponent, LoginService } from './index';

@NgModule({
    imports: [SharedModule],
    declarations: [LoginComponent, LoginFormComponent],
    providers: [LoginService]
})

export class LoginModule { }
