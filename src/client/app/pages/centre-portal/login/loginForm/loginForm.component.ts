import { Injectable, Component } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { LoginService } from '../loginService/login.service';
import { ToasterService } from 'angular2-toaster';
import { SpinningToast } from '../../../../shared/index';

@Component({
    moduleId: module.id,
    selector: 'login-form-cmp',
    styleUrls: ['loginForm.component.css'],
    templateUrl: 'loginForm.component.html'
})

@Injectable()
export class LoginFormComponent {
    public form: FormGroup;
    public email: AbstractControl;
    public password: AbstractControl;

    public submitted: boolean = false;

    constructor(fb: FormBuilder,
        private _toasterService: ToasterService,
        private _loginService: LoginService,
        private _router: Router) {

        this.form = fb.group({
            'email': ['', Validators.required],
            'password': ['', Validators.required]
        });

        this.email = this.form.controls['email'];
        this.password = this.form.controls['password'];
    }

    onSubmit(values: any) {
        this.submitted = true;
        var authenticatingToast = this._toasterService.pop(new SpinningToast('Authenticating'));

        this._loginService
            .login(this.email.value, this.password.value)
            .subscribe(res => {
                this._router.navigate(['centre-portal']);
            },
            err => {
                this._toasterService.clear(authenticatingToast.toastId, authenticatingToast.toastContainerId);
                this.submitted = false;
            },
            () => {
                this.submitted = false;
            });
    };
}
