import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiClientService, SysAdminDashboardGraphDto } from '../../../../api/index';

@Injectable()
export class AdminDashboardService {

    constructor(private _apiClient: ApiClientService) {
    }

    public getTotalCentresCount$(): Observable<number> {
        return this._apiClient.sysAdminDashboard_GetTotalCentreCount();

    }

    public getTotalCentresAwaitingApprovalCount$(): Observable<number> {
        return this._apiClient.sysAdminDashboard_GetTotalCentresAwaitingApproval();
    }

    public getTotalIncome$(): Observable<number> {
        return this._apiClient.sysAdminDashboard_GetTotalIncome();
    }

    public getTotalCompletedSwaps$(): Observable<number> {
        return this._apiClient.sysAdminDashboard_GetTotalSwaps();
    }

    public getAdminDashboardData$(): Observable<SysAdminDashboardGraphDto> {
        return this._apiClient.sysAdminDashboard_GetGraphDashboardData();
    }
}
