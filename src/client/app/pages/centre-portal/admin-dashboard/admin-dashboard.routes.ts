import { Route } from '@angular/router';
import { SysAdminGuardService } from '../../../auth/index';
import { AdminDashboardComponent } from './index';

export const AdminDashboardRoutes: Route[] = [
    {
        path: 'admin-dashboard',
        component: AdminDashboardComponent,
        canActivate: [SysAdminGuardService]
    }
];
