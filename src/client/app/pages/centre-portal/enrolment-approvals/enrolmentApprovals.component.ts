import {Component} from '@angular/core';

@Component({
	moduleId: module.id,
    selector: 'enrolment-approvals-cmp',
    templateUrl: './enrolmentApprovals.component.html'
})

export class EnrolmentApprovalsComponent {}
