import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiClientService, CentreChildDto } from '../../../../api/index';

@Injectable()
export class EnrolmentApprovalsService {

    constructor(private _apiService: ApiClientService) {
    }

    public ApproveChildForCentre$(centreId: string, kidId: string): Observable<void> {
        return this._apiService.enrolmentForm_ApproveChildEnrolment(centreId, kidId);
    }

    public DenyChildForCentre$(centreId: string, kidId: string): Observable<void> {
        return this._apiService.enrolmentForm_DenyChildEnrolment(centreId, kidId);
    }

    public GetCentreChildren$(centreId: string): Observable<CentreChildDto[]> {
        return this._apiService.centre_GetCentreChildren(centreId);
    }
}
