import { Component, OnInit } from '@angular/core';

import { EnrolmentApprovalsFilter } from './enrolmentApprovalsFilter.model';
import { EnrolmentApprovalsService } from '../enrolmentApprovalsService/enrolmentApprovals.service';
import { CentreChildDto } from '../../../../api/index';
import { CentreUserService } from '../../../../core/index';

import { ToasterService } from 'angular2-toaster';
import { SpinningToast } from '../../../../shared/index';

@Component({
    moduleId: module.id,
    selector: 'enrolment-approvals-table-cmp',
    templateUrl: './enrolmentApprovalsTable.component.html',
    styleUrls: ['./enrolmentApprovalsTable.component.css']
})

export class EnrolmentApprovalsTableComponent implements OnInit {

    readonly DefaultApprovalStatus: string = 'Show All';

    private centreChildren: CentreChildDto[] = null;
    private approvalStatuses: any = EnrolmentApprovalsFilter;
    private approvalStatusFilter: any = {
        filterText: '',
        filterId: 0
    };

    constructor(
        private _enrolmentApprovalsService: EnrolmentApprovalsService,
        private _userService: CentreUserService,
        private _toasterService: ToasterService) {
        this.setApprovalStatus(this.DefaultApprovalStatus);
    }

    ngOnInit(): void {
        this._enrolmentApprovalsService
            .GetCentreChildren$(this._userService.SelectedCentre.id)
            .subscribe(
            res => {
                this.centreChildren = res;
            });
    }

    keys(): Array<string> {
        return Object.keys(this.approvalStatuses);
    }

    setApprovalStatus(status: string): void {
        this.approvalStatusFilter.filterText = status;
        this.approvalStatusFilter.filterId = this.approvalStatuses[status];
    }

    getDefaultApprovalStatusId(): number {
        return this.approvalStatuses[this.DefaultApprovalStatus];
    }

    approveChild(child: CentreChildDto): void {
        let savingToast = this._toasterService.pop(new SpinningToast());
        child.childApprovalStatus = this.approvalStatuses['Approved'];
        this._enrolmentApprovalsService
            .ApproveChildForCentre$(child.centreId, child.kidId)
            .subscribe(() => {
                this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                this._toasterService.popAsync('success', 'Saved', '');
            }, (err) => this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId));
    }

    denyChild(child: CentreChildDto): void {
        let savingToast = this._toasterService.pop(new SpinningToast());
        child.childApprovalStatus = this.approvalStatuses['Denied'];
        this._enrolmentApprovalsService
            .DenyChildForCentre$(child.centreId, child.kidId)
            .subscribe(() => {
                this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                this._toasterService.popAsync('success', 'Saved', '');
            }, (err) => this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId));
    }
}
