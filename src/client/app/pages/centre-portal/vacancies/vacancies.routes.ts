import { Route } from '@angular/router';
import { NavigationGuardService } from '../../../shared/index';
import { VacanciesComponent } from './index';

export const VacanciesRoutes: Route[] = [
	{
		path: 'vacancies',
		component: VacanciesComponent,
		canActivate: [NavigationGuardService]
	}
];
