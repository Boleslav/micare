import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/index';

import { VacanciesComponent, VacanciesTableComponent, VacanciesService } from './index';

@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [VacanciesComponent, VacanciesTableComponent],
    providers: [VacanciesService],
    exports: [VacanciesComponent, VacanciesTableComponent]
})

export class VacanciesModule { }
