import {Component} from '@angular/core';

@Component({
	moduleId: module.id,
    selector: 'vacancies-cmp',
    templateUrl: './vacancies.component.html'
})

export class VacanciesComponent {}
