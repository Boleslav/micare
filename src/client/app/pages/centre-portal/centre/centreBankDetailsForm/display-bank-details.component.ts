import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'display-bank-details-cmp',
    templateUrl: './display-bank-details.component.html'
})
export class DisplayBankDetailsComponent {

    @Output() onEdit: EventEmitter<any> = new EventEmitter();
    @Input() redactedPayoutMethod: string;

    setEditMode(): void {
        this.onEdit.emit(true);
    }
}
