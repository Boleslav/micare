import { Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { ToasterService } from 'angular2-toaster';

import { CentreService } from '../index';
import { CentreDto, CreateUpdateCentreDto } from '../../../../api/index';
import { CentreUserService } from '../../../../core/index';
import { SpinningToast } from '../../../../shared/index';

@Component({
    moduleId: module.id,
    selector: 'centre-form-cmp',
    templateUrl: './centreForm.component.html',
    styleUrls: ['centreForm.component.css']
})

export class CentreFormComponent implements OnInit {

    private form: FormGroup;
    private centreName: AbstractControl;
    private streetAddress: AbstractControl;
    private suburb: AbstractControl;
    private postcode: AbstractControl;
    private state: AbstractControl;
    private postalAddress: AbstractControl;
    private contactPhone: AbstractControl;
    private contactName: AbstractControl;
    private timezone: AbstractControl;

    private centreLoaded: boolean = false;
    private createUpdateCentreDto: CreateUpdateCentreDto;
    private approvalStatus: number = -1;
    private timezones: string[] = null;
    private submitted: boolean = false;

    constructor(fb: FormBuilder,
        private _centreService: CentreService,
        private _userService: CentreUserService,
        private _toasterService: ToasterService) {

        this.createUpdateCentreDto = new CreateUpdateCentreDto();
        this.createUpdateCentreDto.init({ 'Id': '' });
        this.form = fb.group({
            'centreName': ['', Validators.required],
            'streetAddress': ['', Validators.required],
            'suburb': ['', Validators.required],
            'postcode': ['', Validators.compose([Validators.required, Validators.pattern('(\\d{4})')])],
            'state': ['', Validators.required],
            'timezone': ['', Validators.required],
            'postalAddress': ['', Validators.required],
            'contactPhone': ['', Validators.required],
            'contactName': ['']
        });

        this.centreName = this.form.controls['centreName'];
        this.streetAddress = this.form.controls['streetAddress'];
        this.suburb = this.form.controls['suburb'];
        this.postcode = this.form.controls['postcode'];
        this.state = this.form.controls['state'];
        this.postalAddress = this.form.controls['postalAddress'];
        this.contactPhone = this.form.controls['contactPhone'];
        this.contactName = this.form.controls['contactName'];
        this.timezone = this.form.controls['timezone'];
    }

    ngOnInit(): void {

        if (this._userService.SelectedCentre !== null) {
            this._centreService
                .GetCentre$(this._userService.SelectedCentre.id)
                .subscribe((centreDto) => {
                    this.createUpdateCentreDto = CreateUpdateCentreDto.fromJS(centreDto.toJSON());
                    this.approvalStatus = centreDto.approvalStatus;
                    this.disablePostalAddressInput();
                    this.centreLoaded = true;
                });
        } else {
            this.createUpdateCentreDto = new CreateUpdateCentreDto();
            this.centreLoaded = true;
        }

        this._centreService
            .getTimeZones$()
            .subscribe(res => this.timezones = res);
    }

    onSubmit(): void {

        let savingToast = this._toasterService.pop(new SpinningToast());
        this.submitted = true;

        this._centreService
            .CreateCentre$(this.createUpdateCentreDto)
            .subscribe(res => {
                this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                this._toasterService.pop('success', 'Saved');
                this.submitted = false;
                this.createUpdateCentreDto = res;

                this._userService.Centres = null;
                let selectedCentre = new CentreDto();
                selectedCentre.init(this.createUpdateCentreDto.toJSON());
                this._userService.SelectedCentre = selectedCentre;
            },
            (err) => this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId));
    }

    disablePostalAddressInput(): void {

        if (this.createUpdateCentreDto.postToPhysicalAddress) {
            this.postalAddress.reset();
            this.postalAddress.disable();
        } else {
            this.postalAddress.enable();
            this.postalAddress.setValidators(Validators.required);
        }
    }
}
