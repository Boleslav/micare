import { Route } from '@angular/router';
import { NavigationGuardService } from '../../../shared/index';
import { CentreComponent } from './index';

export const CentreRoutes: Route[] = [
	{
		path: 'centre',
		component: CentreComponent,
		canActivate: [NavigationGuardService]
	},
];
