import { Route } from '@angular/router';

import { NavigationGuardService } from '../../../shared/index';
import { RoomComponent } from './index';

export const RoomsRoutes: Route[] = [
	{
		path: 'rooms',
		component: RoomComponent,
		canActivate: [NavigationGuardService]
	},
];
