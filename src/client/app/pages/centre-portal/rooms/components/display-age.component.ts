import { Component, Input, OnInit } from '@angular/core';

import { ViewCell } from '../../../../shared/ng2-smart-table/index';
import { AgeConverter } from './age-converter';

@Component({
    template: `
    <span>{{years}}</span>&#160;<small>Y</small>&#160;&#160;<span>{{months}}</span>&#160;<small>M</small>
    `
})
export class DisplayAgeComponent implements ViewCell, OnInit {

    @Input() value: string | number;
    private years: number;
    private months: number;

    ngOnInit() {

        let parseYears = parseFloat(this.value.toString());
        this.years = AgeConverter.getYearFraction(parseYears);

        let parseMonths = parseFloat(this.value.toString());
        this.months = AgeConverter.getMonthFraction(parseMonths);
    }
}
