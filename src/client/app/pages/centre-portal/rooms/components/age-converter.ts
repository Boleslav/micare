export class AgeConverter {
    public static getYearFraction(ageInFraction: number): number {
        const numberOfMonths: number = 12;
        let totalMonths: number = ageInFraction * numberOfMonths;
        let years: number = Math.floor(totalMonths / numberOfMonths);
        return years;
    }

    public static getMonthFraction(ageInFraction: number): number {
        const numberOfMonths: number = 12;
        let totalMonths: number = ageInFraction * numberOfMonths;
        let years: number = Math.floor(totalMonths / numberOfMonths) * numberOfMonths;
        let months: number = Math.ceil(totalMonths - years);
        return months;
    }

    public static getAgeFraction(years: number, months: number): number {
        const numberOfMonths: number = 12;
        let yearsInMonths: number = years * numberOfMonths;
        let totalMonths = yearsInMonths + months;
        return totalMonths / numberOfMonths;
    }
}
