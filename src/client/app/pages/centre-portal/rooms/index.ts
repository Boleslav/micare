export * from './roomsService/rooms.service';
export * from './components/select-age.component';
export * from './components/display-age.component';
export * from './components/display-care-type.component';
export * from './roomsTable/roomsTable.component';
export * from './rooms.component';
export * from './rooms.routes';
export * from './rooms.module';
