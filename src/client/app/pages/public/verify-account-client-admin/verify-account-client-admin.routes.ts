import { Route } from '@angular/router';
import { VerifyAccountClientAdminComponent } from './index';

export const VerifyAccountClientAdminRoutes: Route[] = [
	{
		path: 'verify-account-client-admin/:token', component: VerifyAccountClientAdminComponent
	}
];
