import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { ApiClientService } from '../../../api/index';

@Component({
  moduleId: module.id,
  selector: 'verify-account-client-admin-cmp',
  templateUrl: 'verify-account-client-admin.component.html'
})

export class VerifyAccountClientAdminComponent implements OnInit {

  private verificationCompleted: boolean = undefined;
  private token: string = undefined;

  constructor(private _apiClient: ApiClientService, private _route: ActivatedRoute) {
  }

  ngOnInit() {

    this._route
      .params
      .switchMap((params: Params) => {
        this.token = params['token'];
        return this._apiClient.account_VerifyEmail(this.token);
      })
      .subscribe(res => this.verificationCompleted = true);
  }
}
