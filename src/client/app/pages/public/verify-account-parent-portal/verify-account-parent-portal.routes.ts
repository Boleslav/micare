import { Route } from '@angular/router';
import { VerifyAccountParentPortalComponent } from './index';
import { CentreLogoResolver } from '../components/index';

export const VerifyAccountParentPortalRoutes: Route[] = [
	{
		path: 'verify-account-parent-portal/:centreName/:token',
		component: VerifyAccountParentPortalComponent,
		resolve: {
			centreLogo: CentreLogoResolver
		}
	}
];
