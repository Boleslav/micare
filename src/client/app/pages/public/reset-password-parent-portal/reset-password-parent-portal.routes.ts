import { Route } from '@angular/router';
import { ResetPasswordParentPortalComponent } from './index';
import { CentreLogoResolver } from '../components/index';

export const ResetPasswordParentPortalRoutes: Route[] = [
	{
		path: 'reset-password-parent-portal/:centreName',
		component: ResetPasswordParentPortalComponent,
		resolve: {
			centreLogo: CentreLogoResolver
		}
	}
];
