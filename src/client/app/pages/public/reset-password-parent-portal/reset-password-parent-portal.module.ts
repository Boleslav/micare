import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/index';

import { ResetPasswordParentPortalComponent } from './index';

@NgModule({
    imports: [SharedModule],
    declarations: [ResetPasswordParentPortalComponent],
    providers: []
})

export class ResetPasswordParentPortalModule { }
