import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/index';

import { ResetPasswordComponent } from './index';

@NgModule({
    imports: [SharedModule],
    declarations: [ResetPasswordComponent],
    providers: []
})

export class ResetPasswordModule { }
