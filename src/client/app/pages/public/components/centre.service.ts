import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiClientService, CentreLogoDto } from '../../../api/index';

@Injectable()
export class CentreService {

    private readonly ImageEncoding: string = 'data:image/png;base64,';

    constructor(private _apiClient: ApiClientService) { }

    public getCentreLogo$(uniqueName: string): Observable<CentreLogoDto> {

        return this._apiClient
            .centre_GetCentreLogoByUniqueName(uniqueName)
            .map(res => {
                let logoDto: CentreLogoDto = res;
                logoDto.imageBase64 = this.ImageEncoding + logoDto.imageBase64;
                return logoDto;
            });
    }

    public doesCentreExist$(uniqueName: string): Observable<boolean> {
        return this
            ._apiClient
            .centre_CentreExistsByUniqueName(uniqueName);
    }
}
