import { Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';

import { ToasterService } from 'angular2-toaster';
import { SpinningToast } from '../../../shared/index';
import { ApiClientService, ChangePwdModel, CentreLogoDto } from '../../../api/index';
import { ComplexPasswordValidator, EqualPasswordsValidator } from '../../../validators/index';

@Component({
    moduleId: module.id,
    selector: 'change-password-parent-portal-cmp',
    templateUrl: 'change-password-parent-portal.component.html'
})
export class ChangePasswordParentPortalComponent implements OnInit {

    public form: FormGroup;
    public password: AbstractControl;
    public confirmPassword: AbstractControl;

    private submitted: boolean = false;
    private logo: string = null;
    private centreName: string = null;
    private token: string = null;
    private passwordChanged: boolean = false;

    constructor(fb: FormBuilder,
        private _apiClient: ApiClientService,
        private _toasterService: ToasterService,
        private _route: ActivatedRoute) {

        this.form = fb.group({
            'password': ['', Validators.compose([Validators.required, Validators.minLength(8), ComplexPasswordValidator.validate])],
            'confirmPassword': ['', Validators.compose([Validators.required, Validators.minLength(8)])]
        }, { validator: EqualPasswordsValidator.validate('password', 'confirmPassword') });

        this.password = this.form.controls['password'];
        this.confirmPassword = this.form.controls['confirmPassword'];
    }

    ngOnInit() {

        this._route
            .params
            .subscribe((params: Params) => {
                let tokenParam: string = params['token'];
                this.token = tokenParam === undefined ? '' : tokenParam;

                this.centreName = params['centreName'];
                return this.getLogo$(this._route);
            });
    }

    getLogo$(route: ActivatedRoute) {
        route.data
            .subscribe((data: { centreLogo: CentreLogoDto }) => {
                if (data.centreLogo && data.centreLogo.imageBase64) {
                    this.logo = data.centreLogo.imageBase64;
                }
            });
    }

    onSubmit(): void {
        this.submitted = true;
        let savingToast = this._toasterService.pop(new SpinningToast());
        let changePwModel = new ChangePwdModel();
        changePwModel.init({
            NewPassword: this.password.value,
            ConfirmNewPassword: this.confirmPassword.value,
            Token: this.token
        });
        this._apiClient
            .account_ChangePassword(changePwModel)
            .subscribe(res => {
                this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                this.passwordChanged = true;
            },
            err => this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId),
            () => {

                this.submitted = false;
            });
    }
}
