import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/index';

import { ChangePasswordParentPortalComponent } from './index';

@NgModule({
    imports: [SharedModule],
    declarations: [ChangePasswordParentPortalComponent]
})

export class ChangePasswordParentPortalModule { }
