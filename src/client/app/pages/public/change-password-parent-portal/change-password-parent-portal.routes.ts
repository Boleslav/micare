import { Route } from '@angular/router';
import { ChangePasswordParentPortalComponent } from './index';
import { CentreLogoResolver } from '../components/index';

export const ChangePasswordParentPortalRoutes: Route[] = [
	{
		path: 'change-password-parent-portal/:centreName/:token',
		component: ChangePasswordParentPortalComponent,
		resolve: {
			centreLogo: CentreLogoResolver
		}
	}
];
