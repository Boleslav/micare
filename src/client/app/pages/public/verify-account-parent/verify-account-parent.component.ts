import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { ApiClientService } from '../../../api/index';

@Component({
  moduleId: module.id,
  selector: 'verify-account-parent-cmp',
  templateUrl: 'verify-account-parent.component.html'
})

export class VerifyAccountParentComponent implements OnInit {

  private verificationCompleted: boolean = undefined;
  private token: string = undefined;

  constructor(private _apiClient: ApiClientService, private _route: ActivatedRoute) {
  }

  ngOnInit() {

    this._route
      .params
      .switchMap((params: Params) => {
        this.token = this.token = params['token'];
        return this._apiClient.account_VerifyEmail(this.token);
      }).subscribe(res => {
        this.verificationCompleted = true;
      });
  }
}
