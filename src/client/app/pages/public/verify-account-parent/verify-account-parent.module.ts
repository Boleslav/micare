import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/index';

import { VerifyAccountParentComponent } from './index';

@NgModule({
    imports: [SharedModule],
    declarations: [VerifyAccountParentComponent],
    providers: []
})

export class VerifyAccountParentModule { }
