﻿import {CentreUserService} from "./core/centre-user.service";

export function HangFireFactory (userService: CentreUserService)  {
  userService.HangfireUrl$()
    .subscribe((url) => {
      window.location.href = url;
    });
  return '';
}
