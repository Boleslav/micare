import { Response, RequestOptionsArgs } from '@angular/http'; // ignore

import { BaseConfiguration } from './index';
import { ToasterService } from 'angular2-toaster';
import { AccessTokenService } from '../core/index';

export class BaseClient {

    private _toasterService: ToasterService;
    private _authService: AccessTokenService;

    constructor(baseConfiguration: BaseConfiguration) {
        this._toasterService = baseConfiguration.getToasterService();
        this._authService = baseConfiguration.getAccessTokenService();
    }

    protected transformOptions(options: RequestOptionsArgs) {
        options.headers.append('Authorization', 'Bearer ' + this._authService.getAccessToken());
        return Promise.resolve(options);
    }

    protected transformResult(url: string, response: Response, processor: (response: Response) => any) {
        const responseText = response.text();
        const status = response.status;
        if (status === 400) {
            let errorDescription: string = JSON.parse(responseText).error_description;
            if (errorDescription !== undefined) {
                this._toasterService.pop('error', '', errorDescription);
            }
        } else if (status === 403 || status >= 406 && status < 500) {
            this._toasterService.clear();
            let errorDescription = responseText;
            if (errorDescription === undefined || errorDescription === '') {
                errorDescription = 'A server error has occured';
            }
            this._toasterService.pop('error', '', errorDescription);
        } else if (status >= 500) {
            this._toasterService.pop('error', '', 'A server error has occured');
        }
        return processor(response);
    }
}
