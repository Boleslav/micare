import { NgModule, ModuleWithProviders } from '@angular/core';
import { Router } from '@angular/router';
import { XHRBackend, RequestOptions } from '@angular/http';

import { ToasterService } from 'angular2-toaster';
import { AccessTokenService } from '../core/index';
import { BaseConfiguration, Client, ApiClientService, HttpService } from './index';

@NgModule()
export class ApiModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: ApiModule,
            providers: [
                BaseConfiguration,
                Client,
                ApiClientService,
                {
                    provide: HttpService,
                    useFactory: (authenticationService: AccessTokenService, router: Router,
                        toaster: ToasterService, backend: XHRBackend, options: RequestOptions) => {
                        return new HttpService(authenticationService, router, toaster, backend, options);
                    },
                    deps: [AccessTokenService, Router, ToasterService, XHRBackend, RequestOptions]
                }
            ]
        };
    }
}
