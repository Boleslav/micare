﻿
import {AccessTokenService} from "../core/access-token.service";
import {HttpService} from "./http-service";
import { Router } from '@angular/router';
import { XHRBackend, RequestOptions } from '@angular/http';

import { ToasterService } from 'angular2-toaster';

export function HttpServiceFactory (authenticationService: AccessTokenService, router: Router,
                                    toaster: ToasterService, backend: XHRBackend, options: RequestOptions) {
  return new HttpService(authenticationService, router, toaster, backend, options);
}
