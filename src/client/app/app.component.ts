import { Component, ViewContainerRef } from '@angular/core';
import { Config, MixpanelService } from './shared/index';
import { ToasterConfig } from 'angular2-toaster';

/**
 * This class represents the main application component. Within the @Routes annotation is the configuration of the
 * applications routes, configuring the paths for the lazy loaded components (HomeComponent, AboutComponent).
 */
@Component({
	moduleId: module.id,
	selector: 'sd-app',
	templateUrl: 'app.component.html',
	styles: [`h1 { color: red; }`]
})

export class AppComponent {

	public toasterConfig: ToasterConfig;
	private viewContainerRef: ViewContainerRef;
	public constructor(viewContainerRef: ViewContainerRef,
		_mixpanelService: MixpanelService
	) {

		// You need this small hack in order to catch application root view container ref
		this.viewContainerRef = viewContainerRef;

		_mixpanelService.init(Config.MIXPANEL_TOKEN);

		this.toasterConfig =
			new ToasterConfig({
				showCloseButton: true,
				tapToDismiss: false,
				timeout: { success: 2000, info: 2000, error: 3000 }
			});
	}
}
