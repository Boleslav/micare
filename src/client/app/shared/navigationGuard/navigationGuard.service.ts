import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {
    CanActivate, CanActivateChild, Router,
    ActivatedRouteSnapshot,
    RouterStateSnapshot
} from '@angular/router';

import { Config } from '../../shared/index';
import { CentreUserService } from '../../core/index';

@Injectable()
export class NavigationGuardService implements CanActivate, CanActivateChild {

    constructor(private _userService: CentreUserService, private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {

        if (Config.ENABLE_AUTHORISATION === 'false') {
            return Observable.of(true);
        }

        return this._userService
            .hasCentres$()
            .flatMap(hasCentre => {
                // User has centre, can navigate across the app
                if (hasCentre) {
                    return Observable.of(true);
                }

                // If user has no centre, and is not already on centre page,
                // redirect to centre page
                if (state.url !== '/centre-portal/centre') {
                    this.router.navigate(['centre-portal/centre']);
                    return Observable.of(false);
                }

                // user has no centre, but is already on centre page
                return Observable.of(true);
            })
            .catch(err => {
                this.router.navigate(['login']);
                return Observable.of(false);
            });
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.canActivate(route, state);
    }
}
