import { Injectable } from '@angular/core';
import { Config } from '../index';

declare var mixpanel: any;

@Injectable()
export class MixpanelService {

    public init(token: string): void {
        mixpanel.init(token);
    }

    public track(message: string): void {
        if (Config.ENV === 'Prod')
            mixpanel.track(message, { Environment: Config.ENV });
    }
}
