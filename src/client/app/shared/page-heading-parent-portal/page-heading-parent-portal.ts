import { Component, Input } from '@angular/core';

@Component({
	moduleId: module.id,
	selector: 'page-heading-parent-portal-cmp',
	templateUrl: 'page-heading-parent-portal.html',
	styleUrls: ['./page-heading-parent-portal.css']
})

export class PageHeadingParentPortalComponent {
	@Input() pageDisplayName: string;
	@Input() breadcrumbs: string[];
}
