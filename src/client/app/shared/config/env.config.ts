// Feel free to extend this interface
// depending on your app specific config.
export interface EnvConfig {
	API?: string;
	ENV?: string;
	CLIENT_ID?: string;
	RECAPTCHA_UI_KEY?:string;
	RecaptchaEnabled?: string;
	Sample?: string;
	MIXPANEL_TOKEN?: string;
	INTERCOM_APP_ID?: string;
	ENABLE_AUTHORISATION?: string;
	ENABLE_ENROLMENT_FEATURE?: string;
}

export const Config: EnvConfig = JSON.parse('<%= ENV_CONFIG %>');
