import { Injectable } from '@angular/core';

declare var window: any;
declare var intercomSettings: any;

@Injectable()
export class IntercomService {

    private isInit: boolean = false;

    public initUserNotLoggedIn(appId: string): void {

        if (!this.isInit) {
            this.init(appId, {
                app_id: appId
            });
            this.isInit = true;
        }
        window.Intercom('boot');
    }

    public initUserLoggedIn(appId: string, userEmail: string): void {

        let intercomCmd = 'update';
        if (!this.isInit) {
            this.init(appId,
                {
                    app_id: appId,
                    email: userEmail
                });
            intercomCmd = 'boot';
            this.isInit = true;
        }

        window.Intercom(intercomCmd,
            {
                app_id: appId,
                email: userEmail
            });
    }

    public userLoggedOut(appId: string): void {
        this.isInit = false;
        window.Intercom('shutdown');
    }

    private init(appId: string, intercomSettings: any): void {
        var w = window;
        var ic = w.Intercom;
        if (typeof ic === 'function') {
            ic('reattach_activator');
            ic('update', intercomSettings);
        } else {
            var d = document;
            var i: any = function () {
                i.c(arguments);
            };
            i.q = [];
            i.c = function (args: any) {
                i.q.push(args);
            };
            w.Intercom = i;
            let l = function () {
                var s = d.createElement('script');
                s.type = 'text/javascript';
                s.async = true;
                s.src = 'https://widget.intercom.io/widget/' + appId;
                var x = d.getElementsByTagName('script')[0];
                x.parentNode.insertBefore(s, x);
            };
            if (w.attachEvent) {
                w.attachEvent('onload', l);
            } else {
                w.addEventListener('load', l, false);
            }
        }
    }
}
