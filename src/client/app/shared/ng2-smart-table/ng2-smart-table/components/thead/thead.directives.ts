import {
  ActionsComponent,
  ActionsTitleComponent,
  AddButtonComponent,
  CheckboxSelectAllComponent,
  ColumnTitleComponent,
  TitleComponent } from './cells/index';
import {
  TheadFitlersRowComponent,
  TheadFormRowComponent,
  TheadTitlesRowComponent } from './rows/index';
import { Ng2SmartTableTheadComponent } from './thead.component';

export const NG2_SMART_TABLE_THEAD_DIRECTIVES = [
  ActionsComponent,
  ActionsTitleComponent,
  AddButtonComponent,
  CheckboxSelectAllComponent,
  ColumnTitleComponent,
  TitleComponent,
  TheadFitlersRowComponent,
  TheadFormRowComponent,
  TheadTitlesRowComponent,
  Ng2SmartTableTheadComponent,
];
