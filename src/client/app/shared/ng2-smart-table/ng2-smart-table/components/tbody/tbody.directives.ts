import {
  TbodyCreateCancelComponent,
  TbodyEditDeleteComponent } from './cells/index';
import { Ng2SmartTableTbodyComponent } from './tbody.component';

export const NG2_SMART_TABLE_TBODY_DIRECTIVES = [
  TbodyCreateCancelComponent,
  TbodyEditDeleteComponent,
  Ng2SmartTableTbodyComponent,
];
