import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'spinner-cmp',
    templateUrl: './spinner.component.html',
    styleUrls: ['./spinner.component.css']
})
export class SpinnerComponent { }
