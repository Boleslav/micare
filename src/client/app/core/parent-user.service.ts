import { Injectable } from '@angular/core';

import { ApiClientService } from '../api/index';
import { LocalStorageService } from 'angular-2-local-storage';
import { AccessTokenService } from './index';

@Injectable()
export class ParentUserService {

    public static LAST_SELECTED_CENTRE = 'User_';

    private _parentId: string = null;
    get ParentId(): string {
        if (this._parentId === null) {
            this._parentId = this._storageService.get<string>(AccessTokenService.SUBJECT_ID);
        }
        return this._parentId;
    }

    private _email: string = null;
    get Email(): string {

        if (this._email === null) {
            this._email = this._storageService.get<string>(AccessTokenService.USERNAME);
        }
        return this._email;
    }

    constructor(private _apiClientService: ApiClientService,
        private _storageService: LocalStorageService) {
    }

    public SetLastSelectedCentreName(centreName: string): void {
        this._storageService.set(ParentUserService.LAST_SELECTED_CENTRE + this.ParentId, centreName);
    }

    public Clear(): void {
        this._storageService.remove(ParentUserService.LAST_SELECTED_CENTRE + this.ParentId);
        this._email = null;
        this._parentId = null;
    }
}
