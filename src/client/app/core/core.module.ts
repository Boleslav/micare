import {
  NgModule, Optional, SkipSelf
} from '@angular/core';
import { CentreUserService, ParentUserService, AccessTokenService } from './index';

@NgModule({
  exports: [],
  providers: [
    AccessTokenService,
    CentreUserService,
    ParentUserService
  ]
})
export class CoreModule {
  constructor( @Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }
}
