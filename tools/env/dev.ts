import {EnvConfig} from './env-config.interface';

const DevConfig: EnvConfig = {
  ENV: 'Developer',
  MIXPANEL_TOKEN: 'fe89cd75cd3d736328f34dabf44f0fc1',
  INTERCOM_APP_ID: 'c48ywbs0',
  RECAPTCHA_UI_KEY: '6LfY_Q4UAAAAAIqZld_Nyv7_mgattdybdxJojeF4',
  RecaptchaEnabled: 'false',
  CLIENT_ID: 'ba7e022c-aed1-4b9d-80f1-e92499788cad',
  ENABLE_AUTHORISATION: 'true',
  ENABLE_ENROLMENT_FEATURE: 'true'
};

export = DevConfig;

