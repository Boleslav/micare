import {EnvConfig} from './env-config.interface';

const ProdConfig: EnvConfig = {
  ENV: '__EnvironmentContext__',
  MIXPANEL_TOKEN: '__MIXPANEL_TOKEN__',
  INTERCOM_APP_ID: '__INTERCOM_APP_ID__',
  RECAPTCHA_UI_KEY: '__RECAPTCHA_UI_KEY__',
  RecaptchaEnabled: '__RecaptchaEnabled__',
  CLIENT_ID: '__CLIENT_ID__',
  ENABLE_AUTHORISATION: '__ENABLE_AUTHORISATION__',
  ENABLE_ENROLMENT_FEATURE: '__ENABLE_ENROLMENT_FEATURE__'
};

export = ProdConfig;

