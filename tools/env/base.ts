import {EnvConfig} from './env-config.interface';

const BaseConfig: EnvConfig = {
  // Sample API url
  API: 'https://localhost:44321'
};

export = BaseConfig;

