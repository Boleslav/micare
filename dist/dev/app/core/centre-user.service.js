"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var Observable_1 = require('rxjs/Observable');
require('rxjs/add/operator/find');
var index_1 = require('../api/index');
var angular_2_local_storage_1 = require('angular-2-local-storage');
var index_2 = require('./index');
var CentreUserService = (function () {
    function CentreUserService(_apiClientService, _storageService) {
        this._apiClientService = _apiClientService;
        this._storageService = _storageService;
        this.ImageEncoding = 'data:image/png;base64,';
        this._userId = null;
        this._email = null;
        this._centres = null;
        this._selectedCentre = null;
        this._hangfireUrl = null;
        this._swaggerUrl = null;
        this.isCentreManager = null;
        this.isSystemAdmin = null;
        this.logoDto = null;
    }
    Object.defineProperty(CentreUserService.prototype, "UserId", {
        get: function () {
            if (this._userId === null) {
                this._userId = this._storageService.get(index_2.AccessTokenService.SUBJECT_ID);
            }
            return this._userId;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CentreUserService.prototype, "Email", {
        get: function () {
            if (this._email === null) {
                this._email = this._storageService.get(index_2.AccessTokenService.USERNAME);
            }
            return this._email;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CentreUserService.prototype, "Centres", {
        get: function () {
            return this._centres;
        },
        set: function (val) {
            this._centres = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CentreUserService.prototype, "SelectedCentre", {
        get: function () {
            if (this._selectedCentre === null && this.Centres.length > 0) {
                this._selectedCentre = this.Centres[0];
            }
            return this._selectedCentre;
        },
        set: function (val) {
            this._selectedCentre = val;
            this.SetLastSelectedCentreId(val.id);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CentreUserService.prototype, "HangfireUrl", {
        get: function () {
            return this._hangfireUrl;
        },
        set: function (val) {
            this._hangfireUrl = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CentreUserService.prototype, "SwaggerUrl", {
        get: function () {
            return this._swaggerUrl;
        },
        set: function (val) {
            this._swaggerUrl = val;
        },
        enumerable: true,
        configurable: true
    });
    CentreUserService.prototype.hasCentres$ = function () {
        var _this = this;
        return this
            .getCentres$()
            .map(function () {
            return _this.Centres.length > 0;
        });
    };
    CentreUserService.prototype.getCentres$ = function () {
        var _this = this;
        if (this.Centres !== null)
            return Observable_1.Observable.of(this.Centres);
        return this.IsSysAdmin$()
            .flatMap(function (isSysAdmin) { return isSysAdmin ? _this.getCentresForSysAdmin$() : _this.getCentresForCentreStaff$(); })
            .map(function (res) { return _this.Centres = res; });
    };
    CentreUserService.prototype.IsCentreManager$ = function () {
        var _this = this;
        if (this.isCentreManager !== null)
            return Observable_1.Observable.of(this.isCentreManager);
        return this._apiClientService
            .sysAdmin_IsCentreAdmin()
            .map(function (res) { return _this.isCentreManager = res; });
    };
    CentreUserService.prototype.IsSysAdmin$ = function () {
        var _this = this;
        if (this.isSystemAdmin !== null)
            return Observable_1.Observable.of(this.isSystemAdmin);
        return this._apiClientService
            .sysAdmin_IsSystemAdmin()
            .map(function (res) { return _this.isSystemAdmin = res; });
    };
    CentreUserService.prototype.HangfireUrl$ = function () {
        var _this = this;
        if (this.HangfireUrl !== null)
            return Observable_1.Observable.of(this.HangfireUrl);
        return this._apiClientService
            .sysAdmin_HangfireUrl()
            .map(function (res) { return _this.HangfireUrl = res; });
    };
    CentreUserService.prototype.SwaggerUrl$ = function () {
        var _this = this;
        if (this.SwaggerUrl !== null)
            return Observable_1.Observable.of(this.SwaggerUrl);
        return this._apiClientService
            .sysAdmin_SwaggerUrl()
            .map(function (res) { return _this.SwaggerUrl = res; });
    };
    CentreUserService.prototype.GetCentreName = function (centreId) {
        var centre = this.Centres.find(function (c) { return c.id === centreId; });
        var centreName = centre !== undefined ? centre.name : '';
        return centreName;
    };
    CentreUserService.prototype.getCentreLogo$ = function (centreId) {
        var _this = this;
        if (this.logoDto !== null) {
            return Observable_1.Observable.of(this.logoDto);
        }
        return this._apiClientService
            .centre_GetCentreLogo(centreId)
            .map(function (res) {
            _this.logoDto = res;
            _this.logoDto.imageBase64 = _this.ImageEncoding + _this.logoDto.imageBase64;
        })
            .map(function () { return _this.logoDto; });
    };
    CentreUserService.prototype.Clear = function () {
        this._userId = null;
        this._email = null;
        this._centres = null;
        this._selectedCentre = null;
        this.isSystemAdmin = null;
        this.isCentreManager = null;
        this.logoDto = null;
    };
    CentreUserService.prototype.SetLastSelectedCentreId = function (centreId) {
        this._storageService.set(CentreUserService.LAST_SELECTED_CENTRE + this.UserId, centreId);
    };
    CentreUserService.prototype.GetLastSelectedCentreId = function () {
        return this._storageService.get(CentreUserService.LAST_SELECTED_CENTRE + this.UserId);
    };
    CentreUserService.prototype.searchCentres = function (searchTerm) {
        return this._apiClientService.sysAdmin_SearchCentres(searchTerm, 50);
    };
    CentreUserService.prototype.getCentresForCentreStaff$ = function () {
        return this._apiClientService
            .centre_GetCentresForUserAsync();
    };
    CentreUserService.prototype.getCentresForSysAdmin$ = function () {
        return this._apiClientService.sysAdmin_GetCentres('', {});
    };
    CentreUserService.LAST_SELECTED_CENTRE = 'LastCentre_';
    CentreUserService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.ApiClientService, angular_2_local_storage_1.LocalStorageService])
    ], CentreUserService);
    return CentreUserService;
}());
exports.CentreUserService = CentreUserService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb3JlL2NlbnRyZS11c2VyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUEyQixlQUFlLENBQUMsQ0FBQTtBQUMzQywyQkFBMkIsaUJBQWlCLENBQUMsQ0FBQTtBQUM3QyxRQUFPLHdCQUF3QixDQUFDLENBQUE7QUFFaEMsc0JBQW1FLGNBQWMsQ0FBQyxDQUFBO0FBQ2xGLHdDQUFvQyx5QkFBeUIsQ0FBQyxDQUFBO0FBQzlELHNCQUFtQyxTQUFTLENBQUMsQ0FBQTtBQUc3QztJQW1FSSwyQkFBb0IsaUJBQW1DLEVBQzNDLGVBQW9DO1FBRDVCLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBa0I7UUFDM0Msb0JBQWUsR0FBZixlQUFlLENBQXFCO1FBakUvQixrQkFBYSxHQUFXLHdCQUF3QixDQUFDO1FBRTFELFlBQU8sR0FBVyxJQUFJLENBQUM7UUFTdkIsV0FBTSxHQUFXLElBQUksQ0FBQztRQVN0QixhQUFRLEdBQWdCLElBQUksQ0FBQztRQVM3QixvQkFBZSxHQUFjLElBQUksQ0FBQztRQWFsQyxpQkFBWSxHQUFXLElBQUksQ0FBQztRQVM1QixnQkFBVyxHQUFXLElBQUksQ0FBQztRQVMzQixvQkFBZSxHQUFZLElBQUksQ0FBQztRQUNoQyxrQkFBYSxHQUFZLElBQUksQ0FBQztRQUM5QixZQUFPLEdBQWtCLElBQUksQ0FBQztJQUt0QyxDQUFDO0lBaEVELHNCQUFJLHFDQUFNO2FBQVY7WUFFSSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQ3hCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQVMsMEJBQWtCLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDbkYsQ0FBQztZQUNELE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQ3hCLENBQUM7OztPQUFBO0lBR0Qsc0JBQUksb0NBQUs7YUFBVDtZQUVJLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDdkIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBUywwQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNoRixDQUFDO1lBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDdkIsQ0FBQzs7O09BQUE7SUFHRCxzQkFBSSxzQ0FBTzthQUFYO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDekIsQ0FBQzthQUVELFVBQVksR0FBZ0I7WUFDeEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUM7UUFDeEIsQ0FBQzs7O09BSkE7SUFPRCxzQkFBSSw2Q0FBYzthQUFsQjtZQUNJLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzNELElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMzQyxDQUFDO1lBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUM7UUFDaEMsQ0FBQzthQUVELFVBQW1CLEdBQWM7WUFDN0IsSUFBSSxDQUFDLGVBQWUsR0FBRyxHQUFHLENBQUM7WUFDM0IsSUFBSSxDQUFDLHVCQUF1QixDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUN6QyxDQUFDOzs7T0FMQTtJQVFELHNCQUFJLDBDQUFXO2FBQWY7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQztRQUM3QixDQUFDO2FBRUQsVUFBZ0IsR0FBVztZQUN2QixJQUFJLENBQUMsWUFBWSxHQUFHLEdBQUcsQ0FBQztRQUM1QixDQUFDOzs7T0FKQTtJQU9ELHNCQUFJLHlDQUFVO2FBQWQ7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUM1QixDQUFDO2FBRUQsVUFBZSxHQUFXO1lBQ3RCLElBQUksQ0FBQyxXQUFXLEdBQUcsR0FBRyxDQUFDO1FBQzNCLENBQUM7OztPQUpBO0lBZU0sdUNBQVcsR0FBbEI7UUFBQSxpQkFNQztRQUxHLE1BQU0sQ0FBQyxJQUFJO2FBQ04sV0FBVyxFQUFFO2FBQ2IsR0FBRyxDQUFDO1lBQ0QsTUFBTSxDQUFDLEtBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztRQUNuQyxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFTSx1Q0FBVyxHQUFsQjtRQUFBLGlCQU9DO1FBTkcsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sS0FBSyxJQUFJLENBQUM7WUFDdEIsTUFBTSxDQUFDLHVCQUFVLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUV2QyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRTthQUNwQixPQUFPLENBQUMsVUFBQyxVQUFtQixJQUFLLE9BQUEsVUFBVSxHQUFHLEtBQUksQ0FBQyxzQkFBc0IsRUFBRSxHQUFHLEtBQUksQ0FBQyx5QkFBeUIsRUFBRSxFQUE3RSxDQUE2RSxDQUFDO2FBQy9HLEdBQUcsQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEtBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxFQUFsQixDQUFrQixDQUFDLENBQUM7SUFDeEMsQ0FBQztJQUVNLDRDQUFnQixHQUF2QjtRQUFBLGlCQU9DO1FBTkcsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsS0FBSyxJQUFJLENBQUM7WUFDOUIsTUFBTSxDQUFDLHVCQUFVLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUUvQyxNQUFNLENBQUMsSUFBSSxDQUFDLGlCQUFpQjthQUN4QixzQkFBc0IsRUFBRTthQUN4QixHQUFHLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxLQUFJLENBQUMsZUFBZSxHQUFHLEdBQUcsRUFBMUIsQ0FBMEIsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFTSx1Q0FBVyxHQUFsQjtRQUFBLGlCQU9DO1FBTkcsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsS0FBSyxJQUFJLENBQUM7WUFDNUIsTUFBTSxDQUFDLHVCQUFVLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUU3QyxNQUFNLENBQUMsSUFBSSxDQUFDLGlCQUFpQjthQUN4QixzQkFBc0IsRUFBRTthQUN4QixHQUFHLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxLQUFJLENBQUMsYUFBYSxHQUFHLEdBQUcsRUFBeEIsQ0FBd0IsQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFFTSx3Q0FBWSxHQUFuQjtRQUFBLGlCQU9DO1FBTkcsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsS0FBSyxJQUFJLENBQUM7WUFDMUIsTUFBTSxDQUFDLHVCQUFVLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUUzQyxNQUFNLENBQUMsSUFBSSxDQUFDLGlCQUFpQjthQUN4QixvQkFBb0IsRUFBRTthQUN0QixHQUFHLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxLQUFJLENBQUMsV0FBVyxHQUFHLEdBQUcsRUFBdEIsQ0FBc0IsQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFFTSx1Q0FBVyxHQUFsQjtRQUFBLGlCQU9DO1FBTkcsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsS0FBSyxJQUFJLENBQUM7WUFDekIsTUFBTSxDQUFDLHVCQUFVLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUUxQyxNQUFNLENBQUMsSUFBSSxDQUFDLGlCQUFpQjthQUN4QixtQkFBbUIsRUFBRTthQUNyQixHQUFHLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxLQUFJLENBQUMsVUFBVSxHQUFHLEdBQUcsRUFBckIsQ0FBcUIsQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFTSx5Q0FBYSxHQUFwQixVQUFxQixRQUFnQjtRQUNqQyxJQUFJLE1BQU0sR0FBYyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxFQUFFLEtBQUssUUFBUSxFQUFqQixDQUFpQixDQUFDLENBQUM7UUFDbEUsSUFBSSxVQUFVLEdBQVcsTUFBTSxLQUFLLFNBQVMsR0FBRyxNQUFNLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQztRQUNqRSxNQUFNLENBQUMsVUFBVSxDQUFDO0lBQ3RCLENBQUM7SUFFTSwwQ0FBYyxHQUFyQixVQUFzQixRQUFnQjtRQUF0QyxpQkFZQztRQVZHLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQztZQUN4QixNQUFNLENBQUMsdUJBQVUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3ZDLENBQUM7UUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDLGlCQUFpQjthQUN4QixvQkFBb0IsQ0FBQyxRQUFRLENBQUM7YUFDOUIsR0FBRyxDQUFDLFVBQUEsR0FBRztZQUNKLEtBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1lBQ25CLEtBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxHQUFHLEtBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUM7UUFDN0UsQ0FBQyxDQUFDO2FBQ0QsR0FBRyxDQUFDLGNBQU0sT0FBQSxLQUFJLENBQUMsT0FBTyxFQUFaLENBQVksQ0FBQyxDQUFDO0lBQ2pDLENBQUM7SUFFTSxpQ0FBSyxHQUFaO1FBQ0ksSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDcEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDbkIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDckIsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7UUFDNUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7UUFDMUIsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7UUFDNUIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7SUFDeEIsQ0FBQztJQUVNLG1EQUF1QixHQUE5QixVQUErQixRQUFnQjtRQUMzQyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQzdGLENBQUM7SUFFTSxtREFBdUIsR0FBOUI7UUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQVMsaUJBQWlCLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2xHLENBQUM7SUFFTSx5Q0FBYSxHQUFwQixVQUFxQixVQUFrQjtRQUNuQyxNQUFNLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLHNCQUFzQixDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUN6RSxDQUFDO0lBRU8scURBQXlCLEdBQWpDO1FBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxpQkFBaUI7YUFDeEIsNkJBQTZCLEVBQUUsQ0FBQztJQUN6QyxDQUFDO0lBRU8sa0RBQXNCLEdBQTlCO1FBSUksTUFBTSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLEVBQVUsRUFBRSxDQUFDLENBQUM7SUFDdEUsQ0FBQztJQS9LYyxzQ0FBb0IsR0FBRyxhQUFhLENBQUM7SUFIeEQ7UUFBQyxpQkFBVSxFQUFFOzt5QkFBQTtJQW1MYix3QkFBQztBQUFELENBbExBLEFBa0xDLElBQUE7QUFsTFkseUJBQWlCLG9CQWtMN0IsQ0FBQSIsImZpbGUiOiJhcHAvY29yZS9jZW50cmUtdXNlci5zZXJ2aWNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcy9PYnNlcnZhYmxlJztcclxuaW1wb3J0ICdyeGpzL2FkZC9vcGVyYXRvci9maW5kJztcclxuXHJcbmltcG9ydCB7IEFwaUNsaWVudFNlcnZpY2UsIENlbnRyZUR0bywgQ2VudHJlTG9nb0R0bywgU3RhdHVzIH0gZnJvbSAnLi4vYXBpL2luZGV4JztcclxuaW1wb3J0IHsgTG9jYWxTdG9yYWdlU2VydmljZSB9IGZyb20gJ2FuZ3VsYXItMi1sb2NhbC1zdG9yYWdlJztcclxuaW1wb3J0IHsgQWNjZXNzVG9rZW5TZXJ2aWNlIH0gZnJvbSAnLi9pbmRleCc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBDZW50cmVVc2VyU2VydmljZSB7XHJcblxyXG4gICAgcHJpdmF0ZSBzdGF0aWMgTEFTVF9TRUxFQ1RFRF9DRU5UUkUgPSAnTGFzdENlbnRyZV8nO1xyXG4gICAgcHJpdmF0ZSByZWFkb25seSBJbWFnZUVuY29kaW5nOiBzdHJpbmcgPSAnZGF0YTppbWFnZS9wbmc7YmFzZTY0LCc7XHJcblxyXG4gICAgcHJpdmF0ZSBfdXNlcklkOiBzdHJpbmcgPSBudWxsO1xyXG4gICAgZ2V0IFVzZXJJZCgpOiBzdHJpbmcge1xyXG5cclxuICAgICAgICBpZiAodGhpcy5fdXNlcklkID09PSBudWxsKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3VzZXJJZCA9IHRoaXMuX3N0b3JhZ2VTZXJ2aWNlLmdldDxzdHJpbmc+KEFjY2Vzc1Rva2VuU2VydmljZS5TVUJKRUNUX0lEKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3VzZXJJZDtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIF9lbWFpbDogc3RyaW5nID0gbnVsbDtcclxuICAgIGdldCBFbWFpbCgpOiBzdHJpbmcge1xyXG5cclxuICAgICAgICBpZiAodGhpcy5fZW1haWwgPT09IG51bGwpIHtcclxuICAgICAgICAgICAgdGhpcy5fZW1haWwgPSB0aGlzLl9zdG9yYWdlU2VydmljZS5nZXQ8c3RyaW5nPihBY2Nlc3NUb2tlblNlcnZpY2UuVVNFUk5BTUUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpcy5fZW1haWw7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBfY2VudHJlczogQ2VudHJlRHRvW10gPSBudWxsO1xyXG4gICAgZ2V0IENlbnRyZXMoKTogQ2VudHJlRHRvW10ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9jZW50cmVzO1xyXG4gICAgfVxyXG5cclxuICAgIHNldCBDZW50cmVzKHZhbDogQ2VudHJlRHRvW10pIHtcclxuICAgICAgICB0aGlzLl9jZW50cmVzID0gdmFsO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgX3NlbGVjdGVkQ2VudHJlOiBDZW50cmVEdG8gPSBudWxsO1xyXG4gICAgZ2V0IFNlbGVjdGVkQ2VudHJlKCk6IENlbnRyZUR0byB7XHJcbiAgICAgICAgaWYgKHRoaXMuX3NlbGVjdGVkQ2VudHJlID09PSBudWxsICYmIHRoaXMuQ2VudHJlcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3NlbGVjdGVkQ2VudHJlID0gdGhpcy5DZW50cmVzWzBdO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpcy5fc2VsZWN0ZWRDZW50cmU7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0IFNlbGVjdGVkQ2VudHJlKHZhbDogQ2VudHJlRHRvKSB7XHJcbiAgICAgICAgdGhpcy5fc2VsZWN0ZWRDZW50cmUgPSB2YWw7XHJcbiAgICAgICAgdGhpcy5TZXRMYXN0U2VsZWN0ZWRDZW50cmVJZCh2YWwuaWQpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgX2hhbmdmaXJlVXJsOiBzdHJpbmcgPSBudWxsO1xyXG4gICAgZ2V0IEhhbmdmaXJlVXJsKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2hhbmdmaXJlVXJsO1xyXG4gICAgfVxyXG5cclxuICAgIHNldCBIYW5nZmlyZVVybCh2YWw6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuX2hhbmdmaXJlVXJsID0gdmFsO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgX3N3YWdnZXJVcmw6IHN0cmluZyA9IG51bGw7XHJcbiAgICBnZXQgU3dhZ2dlclVybCgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9zd2FnZ2VyVXJsO1xyXG4gICAgfVxyXG5cclxuICAgIHNldCBTd2FnZ2VyVXJsKHZhbDogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5fc3dhZ2dlclVybCA9IHZhbDtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGlzQ2VudHJlTWFuYWdlcjogYm9vbGVhbiA9IG51bGw7XHJcbiAgICBwcml2YXRlIGlzU3lzdGVtQWRtaW46IGJvb2xlYW4gPSBudWxsO1xyXG4gICAgcHJpdmF0ZSBsb2dvRHRvOiBDZW50cmVMb2dvRHRvID0gbnVsbDtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9hcGlDbGllbnRTZXJ2aWNlOiBBcGlDbGllbnRTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgX3N0b3JhZ2VTZXJ2aWNlOiBMb2NhbFN0b3JhZ2VTZXJ2aWNlKSB7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBoYXNDZW50cmVzJCgpOiBPYnNlcnZhYmxlPGJvb2xlYW4+IHtcclxuICAgICAgICByZXR1cm4gdGhpc1xyXG4gICAgICAgICAgICAuZ2V0Q2VudHJlcyQoKVxyXG4gICAgICAgICAgICAubWFwKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLkNlbnRyZXMubGVuZ3RoID4gMDtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldENlbnRyZXMkKCk6IE9ic2VydmFibGU8Q2VudHJlRHRvW10+IHtcclxuICAgICAgICBpZiAodGhpcy5DZW50cmVzICE9PSBudWxsKVxyXG4gICAgICAgICAgICByZXR1cm4gT2JzZXJ2YWJsZS5vZih0aGlzLkNlbnRyZXMpO1xyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5Jc1N5c0FkbWluJCgpXHJcbiAgICAgICAgICAgIC5mbGF0TWFwKChpc1N5c0FkbWluOiBib29sZWFuKSA9PiBpc1N5c0FkbWluID8gdGhpcy5nZXRDZW50cmVzRm9yU3lzQWRtaW4kKCkgOiB0aGlzLmdldENlbnRyZXNGb3JDZW50cmVTdGFmZiQoKSlcclxuICAgICAgICAgICAgLm1hcChyZXMgPT4gdGhpcy5DZW50cmVzID0gcmVzKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgSXNDZW50cmVNYW5hZ2VyJCgpOiBPYnNlcnZhYmxlPGJvb2xlYW4+IHtcclxuICAgICAgICBpZiAodGhpcy5pc0NlbnRyZU1hbmFnZXIgIT09IG51bGwpXHJcbiAgICAgICAgICAgIHJldHVybiBPYnNlcnZhYmxlLm9mKHRoaXMuaXNDZW50cmVNYW5hZ2VyKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2FwaUNsaWVudFNlcnZpY2VcclxuICAgICAgICAgICAgLnN5c0FkbWluX0lzQ2VudHJlQWRtaW4oKVxyXG4gICAgICAgICAgICAubWFwKHJlcyA9PiB0aGlzLmlzQ2VudHJlTWFuYWdlciA9IHJlcyk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIElzU3lzQWRtaW4kKCk6IE9ic2VydmFibGU8Ym9vbGVhbj4ge1xyXG4gICAgICAgIGlmICh0aGlzLmlzU3lzdGVtQWRtaW4gIT09IG51bGwpXHJcbiAgICAgICAgICAgIHJldHVybiBPYnNlcnZhYmxlLm9mKHRoaXMuaXNTeXN0ZW1BZG1pbik7XHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLl9hcGlDbGllbnRTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5zeXNBZG1pbl9Jc1N5c3RlbUFkbWluKClcclxuICAgICAgICAgICAgLm1hcChyZXMgPT4gdGhpcy5pc1N5c3RlbUFkbWluID0gcmVzKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgSGFuZ2ZpcmVVcmwkKCk6IE9ic2VydmFibGU8c3RyaW5nPiB7XHJcbiAgICAgICAgaWYgKHRoaXMuSGFuZ2ZpcmVVcmwgIT09IG51bGwpXHJcbiAgICAgICAgICAgIHJldHVybiBPYnNlcnZhYmxlLm9mKHRoaXMuSGFuZ2ZpcmVVcmwpO1xyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5fYXBpQ2xpZW50U2VydmljZVxyXG4gICAgICAgICAgICAuc3lzQWRtaW5fSGFuZ2ZpcmVVcmwoKVxyXG4gICAgICAgICAgICAubWFwKHJlcyA9PiB0aGlzLkhhbmdmaXJlVXJsID0gcmVzKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgU3dhZ2dlclVybCQoKTogT2JzZXJ2YWJsZTxzdHJpbmc+IHtcclxuICAgICAgICBpZiAodGhpcy5Td2FnZ2VyVXJsICE9PSBudWxsKVxyXG4gICAgICAgICAgICByZXR1cm4gT2JzZXJ2YWJsZS5vZih0aGlzLlN3YWdnZXJVcmwpO1xyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5fYXBpQ2xpZW50U2VydmljZVxyXG4gICAgICAgICAgICAuc3lzQWRtaW5fU3dhZ2dlclVybCgpXHJcbiAgICAgICAgICAgIC5tYXAocmVzID0+IHRoaXMuU3dhZ2dlclVybCA9IHJlcyk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIEdldENlbnRyZU5hbWUoY2VudHJlSWQ6IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICAgICAgbGV0IGNlbnRyZTogQ2VudHJlRHRvID0gdGhpcy5DZW50cmVzLmZpbmQoYyA9PiBjLmlkID09PSBjZW50cmVJZCk7XHJcbiAgICAgICAgbGV0IGNlbnRyZU5hbWU6IHN0cmluZyA9IGNlbnRyZSAhPT0gdW5kZWZpbmVkID8gY2VudHJlLm5hbWUgOiAnJztcclxuICAgICAgICByZXR1cm4gY2VudHJlTmFtZTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0Q2VudHJlTG9nbyQoY2VudHJlSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8Q2VudHJlTG9nb0R0bz4ge1xyXG5cclxuICAgICAgICBpZiAodGhpcy5sb2dvRHRvICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBPYnNlcnZhYmxlLm9mKHRoaXMubG9nb0R0byk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzLl9hcGlDbGllbnRTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5jZW50cmVfR2V0Q2VudHJlTG9nbyhjZW50cmVJZClcclxuICAgICAgICAgICAgLm1hcChyZXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sb2dvRHRvID0gcmVzO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sb2dvRHRvLmltYWdlQmFzZTY0ID0gdGhpcy5JbWFnZUVuY29kaW5nICsgdGhpcy5sb2dvRHRvLmltYWdlQmFzZTY0O1xyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAubWFwKCgpID0+IHRoaXMubG9nb0R0byk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIENsZWFyKCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuX3VzZXJJZCA9IG51bGw7XHJcbiAgICAgICAgdGhpcy5fZW1haWwgPSBudWxsO1xyXG4gICAgICAgIHRoaXMuX2NlbnRyZXMgPSBudWxsO1xyXG4gICAgICAgIHRoaXMuX3NlbGVjdGVkQ2VudHJlID0gbnVsbDtcclxuICAgICAgICB0aGlzLmlzU3lzdGVtQWRtaW4gPSBudWxsO1xyXG4gICAgICAgIHRoaXMuaXNDZW50cmVNYW5hZ2VyID0gbnVsbDtcclxuICAgICAgICB0aGlzLmxvZ29EdG8gPSBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBTZXRMYXN0U2VsZWN0ZWRDZW50cmVJZChjZW50cmVJZDogc3RyaW5nKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5fc3RvcmFnZVNlcnZpY2Uuc2V0KENlbnRyZVVzZXJTZXJ2aWNlLkxBU1RfU0VMRUNURURfQ0VOVFJFICsgdGhpcy5Vc2VySWQsIGNlbnRyZUlkKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgR2V0TGFzdFNlbGVjdGVkQ2VudHJlSWQoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fc3RvcmFnZVNlcnZpY2UuZ2V0PHN0cmluZz4oQ2VudHJlVXNlclNlcnZpY2UuTEFTVF9TRUxFQ1RFRF9DRU5UUkUgKyB0aGlzLlVzZXJJZCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHNlYXJjaENlbnRyZXMoc2VhcmNoVGVybTogc3RyaW5nKTogT2JzZXJ2YWJsZTxDZW50cmVEdG9bXT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9hcGlDbGllbnRTZXJ2aWNlLnN5c0FkbWluX1NlYXJjaENlbnRyZXMoc2VhcmNoVGVybSwgNTApO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0Q2VudHJlc0ZvckNlbnRyZVN0YWZmJCgpOiBPYnNlcnZhYmxlPENlbnRyZUR0b1tdPiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2FwaUNsaWVudFNlcnZpY2VcclxuICAgICAgICAgICAgLmNlbnRyZV9HZXRDZW50cmVzRm9yVXNlckFzeW5jKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXRDZW50cmVzRm9yU3lzQWRtaW4kKCk6IE9ic2VydmFibGU8Q2VudHJlRHRvW10+IHtcclxuICAgICAgICAvLyBUaGVyZSBpcyBjdXJyZW50bHkgbm8gd2F5IGluIG5zd2FnIHRvIHBhc3MgaW4gb3B0aW9uYWwgcGFyYW1ldGVycy5cclxuICAgICAgICAvLyBQbGFubmVkIHNvbHV0aW9uIHdpbGwgYmUgZGVsaXZlcmVkIGluIGFuIHVua25vd24gdGltZTpcclxuICAgICAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vTlN3YWcvTlN3YWcvaXNzdWVzLzYwOFxyXG4gICAgICAgIHJldHVybiB0aGlzLl9hcGlDbGllbnRTZXJ2aWNlLnN5c0FkbWluX0dldENlbnRyZXMoJycsIDxTdGF0dXM+e30pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
