"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('./index');
var angular_2_local_storage_1 = require('angular-2-local-storage');
var AccessTokenService = (function () {
    function AccessTokenService(_storageService) {
        this._storageService = _storageService;
        this._isParent = false;
    }
    Object.defineProperty(AccessTokenService.prototype, "IsParent", {
        get: function () {
            return this._isParent;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AccessTokenService.prototype, "CentreName", {
        get: function () {
            return this._storageService.get(index_1.ParentUserService.LAST_SELECTED_CENTRE);
        },
        enumerable: true,
        configurable: true
    });
    AccessTokenService.prototype.saveAccessToken = function (tokenJson, isParent) {
        this._storageService.set(AccessTokenService.ACCESS_TOKEN, tokenJson[AccessTokenService.ACCESS_TOKEN]);
        this._storageService.set(AccessTokenService.REFRESH_TOKEN, tokenJson[AccessTokenService.REFRESH_TOKEN]);
        this._storageService.set(AccessTokenService.TOKEN_EXPIRATION, tokenJson[AccessTokenService.TOKEN_EXPIRATION]);
        this._storageService.set(AccessTokenService.SUBJECT_ID, tokenJson[AccessTokenService.SUBJECT_ID]);
        this._storageService.set(AccessTokenService.USERNAME, tokenJson[AccessTokenService.USERNAME]);
        if (isParent !== undefined && isParent !== null)
            this._isParent = isParent;
        else
            this._isParent = false;
    };
    AccessTokenService.prototype.getAccessToken = function () {
        var token = this._storageService.get(AccessTokenService.ACCESS_TOKEN);
        return token;
    };
    AccessTokenService.prototype.hasValidAccessToken = function () {
        if (this.getAccessToken()) {
            return !this.hasTokenExpired();
        }
        return false;
    };
    AccessTokenService.prototype.hasTokenExpired = function () {
        var expires = this._storageService.get(AccessTokenService.TOKEN_EXPIRATION);
        var expiresAt = new Date(expires);
        var now = new Date();
        if (expiresAt.getTime() < now.getTime()) {
            return true;
        }
        return false;
    };
    AccessTokenService.prototype.getRefreshToken = function () {
        var refreshToken = this._storageService.get(AccessTokenService.REFRESH_TOKEN);
        return refreshToken;
    };
    AccessTokenService.prototype.clearStorage = function () {
        this._storageService.remove(AccessTokenService.ACCESS_TOKEN);
        this._storageService.remove(AccessTokenService.REFRESH_TOKEN);
        this._storageService.remove(AccessTokenService.TOKEN_EXPIRATION);
        this._storageService.remove(AccessTokenService.SUBJECT_ID);
        this._storageService.remove(AccessTokenService.USERNAME);
    };
    AccessTokenService.prototype.logout = function () {
        this.clearStorage();
        this._isParent = null;
    };
    AccessTokenService.SUBJECT_ID = 'as:sid';
    AccessTokenService.USERNAME = 'userName';
    AccessTokenService.ACCESS_TOKEN = 'access_token';
    AccessTokenService.REFRESH_TOKEN = 'refresh_token';
    AccessTokenService.TOKEN_EXPIRATION = '.expires';
    AccessTokenService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [angular_2_local_storage_1.LocalStorageService])
    ], AccessTokenService);
    return AccessTokenService;
}());
exports.AccessTokenService = AccessTokenService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb3JlL2FjY2Vzcy10b2tlbi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBMkIsZUFBZSxDQUFDLENBQUE7QUFFM0Msc0JBQWtDLFNBQVMsQ0FBQyxDQUFBO0FBQzVDLHdDQUFvQyx5QkFBeUIsQ0FBQyxDQUFBO0FBRzlEO0lBaUJJLDRCQUFvQixlQUFvQztRQUFwQyxvQkFBZSxHQUFmLGVBQWUsQ0FBcUI7UUFUaEQsY0FBUyxHQUFZLEtBQUssQ0FBQztJQVVuQyxDQUFDO0lBVEQsc0JBQUksd0NBQVE7YUFBWjtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQzFCLENBQUM7OztPQUFBO0lBRUQsc0JBQUksMENBQVU7YUFBZDtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBUyx5QkFBaUIsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBQ3BGLENBQUM7OztPQUFBO0lBS00sNENBQWUsR0FBdEIsVUFBdUIsU0FBYyxFQUFFLFFBQWlCO1FBQ3BELElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLFlBQVksRUFBRSxTQUFTLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztRQUN0RyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLEVBQUUsU0FBUyxDQUFDLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7UUFDeEcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsZ0JBQWdCLEVBQUUsU0FBUyxDQUFDLGtCQUFrQixDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQztRQUM5RyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLEVBQUUsU0FBUyxDQUFDLGtCQUFrQixDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFDbEcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsUUFBUSxFQUFFLFNBQVMsQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1FBRTlGLEVBQUUsQ0FBQyxDQUFDLFFBQVEsS0FBSyxTQUFTLElBQUksUUFBUSxLQUFLLElBQUksQ0FBQztZQUM1QyxJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQztRQUM5QixJQUFJO1lBQ0EsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7SUFDL0IsQ0FBQztJQUVNLDJDQUFjLEdBQXJCO1FBQ0ksSUFBSSxLQUFLLEdBQVcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQVMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDdEYsTUFBTSxDQUFDLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRU0sZ0RBQW1CLEdBQTFCO1FBRUksRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUN4QixNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDbkMsQ0FBQztRQUNELE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFDakIsQ0FBQztJQUVNLDRDQUFlLEdBQXRCO1FBQ0ksSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUM1RSxJQUFJLFNBQVMsR0FBRyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNsQyxJQUFJLEdBQUcsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDO1FBQ3JCLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxPQUFPLEVBQUUsR0FBRyxHQUFHLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3RDLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUNELE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFDakIsQ0FBQztJQUVNLDRDQUFlLEdBQXRCO1FBQ0ksSUFBSSxZQUFZLEdBQVcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQVMsa0JBQWtCLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDOUYsTUFBTSxDQUFDLFlBQVksQ0FBQztJQUN4QixDQUFDO0lBRU0seUNBQVksR0FBbkI7UUFDSSxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUM3RCxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUM5RCxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ2pFLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLGtCQUFrQixDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzNELElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQzdELENBQUM7SUFFTSxtQ0FBTSxHQUFiO1FBQ0ksSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO0lBQzFCLENBQUM7SUF0RXNCLDZCQUFVLEdBQUcsUUFBUSxDQUFDO0lBQ3RCLDJCQUFRLEdBQUcsVUFBVSxDQUFDO0lBQzlCLCtCQUFZLEdBQVcsY0FBYyxDQUFDO0lBQ3RDLGdDQUFhLEdBQVcsZUFBZSxDQUFDO0lBQ3hDLG1DQUFnQixHQUFHLFVBQVUsQ0FBQztJQVBqRDtRQUFDLGlCQUFVLEVBQUU7OzBCQUFBO0lBMEViLHlCQUFDO0FBQUQsQ0F6RUEsQUF5RUMsSUFBQTtBQXpFWSwwQkFBa0IscUJBeUU5QixDQUFBIiwiZmlsZSI6ImFwcC9jb3JlL2FjY2Vzcy10b2tlbi5zZXJ2aWNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgUGFyZW50VXNlclNlcnZpY2UgfSBmcm9tICcuL2luZGV4JztcclxuaW1wb3J0IHsgTG9jYWxTdG9yYWdlU2VydmljZSB9IGZyb20gJ2FuZ3VsYXItMi1sb2NhbC1zdG9yYWdlJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIEFjY2Vzc1Rva2VuU2VydmljZSB7XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyByZWFkb25seSBTVUJKRUNUX0lEID0gJ2FzOnNpZCc7XHJcbiAgICBwdWJsaWMgc3RhdGljIHJlYWRvbmx5IFVTRVJOQU1FID0gJ3VzZXJOYW1lJztcclxuICAgIHByaXZhdGUgc3RhdGljIEFDQ0VTU19UT0tFTjogc3RyaW5nID0gJ2FjY2Vzc190b2tlbic7XHJcbiAgICBwcml2YXRlIHN0YXRpYyBSRUZSRVNIX1RPS0VOOiBzdHJpbmcgPSAncmVmcmVzaF90b2tlbic7XHJcbiAgICBwcml2YXRlIHN0YXRpYyBUT0tFTl9FWFBJUkFUSU9OID0gJy5leHBpcmVzJztcclxuXHJcbiAgICBwcml2YXRlIF9pc1BhcmVudDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgZ2V0IElzUGFyZW50KCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9pc1BhcmVudDtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgQ2VudHJlTmFtZSgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9zdG9yYWdlU2VydmljZS5nZXQ8c3RyaW5nPihQYXJlbnRVc2VyU2VydmljZS5MQVNUX1NFTEVDVEVEX0NFTlRSRSk7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfc3RvcmFnZVNlcnZpY2U6IExvY2FsU3RvcmFnZVNlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc2F2ZUFjY2Vzc1Rva2VuKHRva2VuSnNvbjogYW55LCBpc1BhcmVudDogYm9vbGVhbikge1xyXG4gICAgICAgIHRoaXMuX3N0b3JhZ2VTZXJ2aWNlLnNldChBY2Nlc3NUb2tlblNlcnZpY2UuQUNDRVNTX1RPS0VOLCB0b2tlbkpzb25bQWNjZXNzVG9rZW5TZXJ2aWNlLkFDQ0VTU19UT0tFTl0pO1xyXG4gICAgICAgIHRoaXMuX3N0b3JhZ2VTZXJ2aWNlLnNldChBY2Nlc3NUb2tlblNlcnZpY2UuUkVGUkVTSF9UT0tFTiwgdG9rZW5Kc29uW0FjY2Vzc1Rva2VuU2VydmljZS5SRUZSRVNIX1RPS0VOXSk7XHJcbiAgICAgICAgdGhpcy5fc3RvcmFnZVNlcnZpY2Uuc2V0KEFjY2Vzc1Rva2VuU2VydmljZS5UT0tFTl9FWFBJUkFUSU9OLCB0b2tlbkpzb25bQWNjZXNzVG9rZW5TZXJ2aWNlLlRPS0VOX0VYUElSQVRJT05dKTtcclxuICAgICAgICB0aGlzLl9zdG9yYWdlU2VydmljZS5zZXQoQWNjZXNzVG9rZW5TZXJ2aWNlLlNVQkpFQ1RfSUQsIHRva2VuSnNvbltBY2Nlc3NUb2tlblNlcnZpY2UuU1VCSkVDVF9JRF0pO1xyXG4gICAgICAgIHRoaXMuX3N0b3JhZ2VTZXJ2aWNlLnNldChBY2Nlc3NUb2tlblNlcnZpY2UuVVNFUk5BTUUsIHRva2VuSnNvbltBY2Nlc3NUb2tlblNlcnZpY2UuVVNFUk5BTUVdKTtcclxuXHJcbiAgICAgICAgaWYgKGlzUGFyZW50ICE9PSB1bmRlZmluZWQgJiYgaXNQYXJlbnQgIT09IG51bGwpXHJcbiAgICAgICAgICAgIHRoaXMuX2lzUGFyZW50ID0gaXNQYXJlbnQ7XHJcbiAgICAgICAgZWxzZVxyXG4gICAgICAgICAgICB0aGlzLl9pc1BhcmVudCA9IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRBY2Nlc3NUb2tlbigpOiBzdHJpbmcge1xyXG4gICAgICAgIGxldCB0b2tlbjogc3RyaW5nID0gdGhpcy5fc3RvcmFnZVNlcnZpY2UuZ2V0PHN0cmluZz4oQWNjZXNzVG9rZW5TZXJ2aWNlLkFDQ0VTU19UT0tFTik7XHJcbiAgICAgICAgcmV0dXJuIHRva2VuO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBoYXNWYWxpZEFjY2Vzc1Rva2VuKCk6IGJvb2xlYW4ge1xyXG5cclxuICAgICAgICBpZiAodGhpcy5nZXRBY2Nlc3NUb2tlbigpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiAhdGhpcy5oYXNUb2tlbkV4cGlyZWQoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBoYXNUb2tlbkV4cGlyZWQoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgbGV0IGV4cGlyZXMgPSB0aGlzLl9zdG9yYWdlU2VydmljZS5nZXQoQWNjZXNzVG9rZW5TZXJ2aWNlLlRPS0VOX0VYUElSQVRJT04pO1xyXG4gICAgICAgIGxldCBleHBpcmVzQXQgPSBuZXcgRGF0ZShleHBpcmVzKTtcclxuICAgICAgICB2YXIgbm93ID0gbmV3IERhdGUoKTtcclxuICAgICAgICBpZiAoZXhwaXJlc0F0LmdldFRpbWUoKSA8IG5vdy5nZXRUaW1lKCkpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0UmVmcmVzaFRva2VuKCkge1xyXG4gICAgICAgIGxldCByZWZyZXNoVG9rZW46IHN0cmluZyA9IHRoaXMuX3N0b3JhZ2VTZXJ2aWNlLmdldDxzdHJpbmc+KEFjY2Vzc1Rva2VuU2VydmljZS5SRUZSRVNIX1RPS0VOKTtcclxuICAgICAgICByZXR1cm4gcmVmcmVzaFRva2VuO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBjbGVhclN0b3JhZ2UoKSB7XHJcbiAgICAgICAgdGhpcy5fc3RvcmFnZVNlcnZpY2UucmVtb3ZlKEFjY2Vzc1Rva2VuU2VydmljZS5BQ0NFU1NfVE9LRU4pO1xyXG4gICAgICAgIHRoaXMuX3N0b3JhZ2VTZXJ2aWNlLnJlbW92ZShBY2Nlc3NUb2tlblNlcnZpY2UuUkVGUkVTSF9UT0tFTik7XHJcbiAgICAgICAgdGhpcy5fc3RvcmFnZVNlcnZpY2UucmVtb3ZlKEFjY2Vzc1Rva2VuU2VydmljZS5UT0tFTl9FWFBJUkFUSU9OKTtcclxuICAgICAgICB0aGlzLl9zdG9yYWdlU2VydmljZS5yZW1vdmUoQWNjZXNzVG9rZW5TZXJ2aWNlLlNVQkpFQ1RfSUQpO1xyXG4gICAgICAgIHRoaXMuX3N0b3JhZ2VTZXJ2aWNlLnJlbW92ZShBY2Nlc3NUb2tlblNlcnZpY2UuVVNFUk5BTUUpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBsb2dvdXQoKSB7XHJcbiAgICAgICAgdGhpcy5jbGVhclN0b3JhZ2UoKTtcclxuICAgICAgICB0aGlzLl9pc1BhcmVudCA9IG51bGw7XHJcbiAgICB9XHJcbn1cclxuIl19
