"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require('@angular/core');
var index_1 = require('./index');
var CoreModule = (function () {
    function CoreModule(parentModule) {
        if (parentModule) {
            throw new Error('CoreModule is already loaded. Import it in the AppModule only');
        }
    }
    CoreModule = __decorate([
        core_1.NgModule({
            exports: [],
            providers: [
                index_1.AccessTokenService,
                index_1.CentreUserService,
                index_1.ParentUserService
            ]
        }),
        __param(0, core_1.Optional()),
        __param(0, core_1.SkipSelf()), 
        __metadata('design:paramtypes', [CoreModule])
    ], CoreModule);
    return CoreModule;
}());
exports.CoreModule = CoreModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb3JlL2NvcmUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7QUFBQSxxQkFFTyxlQUFlLENBQUMsQ0FBQTtBQUN2QixzQkFBeUUsU0FBUyxDQUFDLENBQUE7QUFVbkY7SUFDRSxvQkFBcUMsWUFBd0I7UUFDM0QsRUFBRSxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztZQUNqQixNQUFNLElBQUksS0FBSyxDQUNiLCtEQUErRCxDQUFDLENBQUM7UUFDckUsQ0FBQztJQUNILENBQUM7SUFkSDtRQUFDLGVBQVEsQ0FBQztZQUNSLE9BQU8sRUFBRSxFQUFFO1lBQ1gsU0FBUyxFQUFFO2dCQUNULDBCQUFrQjtnQkFDbEIseUJBQWlCO2dCQUNqQix5QkFBaUI7YUFDbEI7U0FDRixDQUFDO21CQUVjLGVBQVEsRUFBRTttQkFBRSxlQUFRLEVBQUU7O2tCQUZwQztJQVFGLGlCQUFDO0FBQUQsQ0FQQSxBQU9DLElBQUE7QUFQWSxrQkFBVSxhQU90QixDQUFBIiwiZmlsZSI6ImFwcC9jb3JlL2NvcmUubW9kdWxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICBOZ01vZHVsZSwgT3B0aW9uYWwsIFNraXBTZWxmXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENlbnRyZVVzZXJTZXJ2aWNlLCBQYXJlbnRVc2VyU2VydmljZSwgQWNjZXNzVG9rZW5TZXJ2aWNlIH0gZnJvbSAnLi9pbmRleCc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGV4cG9ydHM6IFtdLFxyXG4gIHByb3ZpZGVyczogW1xyXG4gICAgQWNjZXNzVG9rZW5TZXJ2aWNlLFxyXG4gICAgQ2VudHJlVXNlclNlcnZpY2UsXHJcbiAgICBQYXJlbnRVc2VyU2VydmljZVxyXG4gIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIENvcmVNb2R1bGUge1xyXG4gIGNvbnN0cnVjdG9yKCBAT3B0aW9uYWwoKSBAU2tpcFNlbGYoKSBwYXJlbnRNb2R1bGU6IENvcmVNb2R1bGUpIHtcclxuICAgIGlmIChwYXJlbnRNb2R1bGUpIHtcclxuICAgICAgdGhyb3cgbmV3IEVycm9yKFxyXG4gICAgICAgICdDb3JlTW9kdWxlIGlzIGFscmVhZHkgbG9hZGVkLiBJbXBvcnQgaXQgaW4gdGhlIEFwcE1vZHVsZSBvbmx5Jyk7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ==
