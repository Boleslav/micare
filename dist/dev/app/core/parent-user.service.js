"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../api/index');
var angular_2_local_storage_1 = require('angular-2-local-storage');
var index_2 = require('./index');
var ParentUserService = (function () {
    function ParentUserService(_apiClientService, _storageService) {
        this._apiClientService = _apiClientService;
        this._storageService = _storageService;
        this._parentId = null;
        this._email = null;
    }
    Object.defineProperty(ParentUserService.prototype, "ParentId", {
        get: function () {
            if (this._parentId === null) {
                this._parentId = this._storageService.get(index_2.AccessTokenService.SUBJECT_ID);
            }
            return this._parentId;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ParentUserService.prototype, "Email", {
        get: function () {
            if (this._email === null) {
                this._email = this._storageService.get(index_2.AccessTokenService.USERNAME);
            }
            return this._email;
        },
        enumerable: true,
        configurable: true
    });
    ParentUserService.prototype.SetLastSelectedCentreName = function (centreName) {
        this._storageService.set(ParentUserService.LAST_SELECTED_CENTRE + this.ParentId, centreName);
    };
    ParentUserService.prototype.Clear = function () {
        this._storageService.remove(ParentUserService.LAST_SELECTED_CENTRE + this.ParentId);
        this._email = null;
        this._parentId = null;
    };
    ParentUserService.LAST_SELECTED_CENTRE = 'User_';
    ParentUserService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.ApiClientService, angular_2_local_storage_1.LocalStorageService])
    ], ParentUserService);
    return ParentUserService;
}());
exports.ParentUserService = ParentUserService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb3JlL3BhcmVudC11c2VyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUEyQixlQUFlLENBQUMsQ0FBQTtBQUUzQyxzQkFBaUMsY0FBYyxDQUFDLENBQUE7QUFDaEQsd0NBQW9DLHlCQUF5QixDQUFDLENBQUE7QUFDOUQsc0JBQW1DLFNBQVMsQ0FBQyxDQUFBO0FBRzdDO0lBcUJJLDJCQUFvQixpQkFBbUMsRUFDM0MsZUFBb0M7UUFENUIsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFrQjtRQUMzQyxvQkFBZSxHQUFmLGVBQWUsQ0FBcUI7UUFsQnhDLGNBQVMsR0FBVyxJQUFJLENBQUM7UUFRekIsV0FBTSxHQUFXLElBQUksQ0FBQztJQVc5QixDQUFDO0lBbEJELHNCQUFJLHVDQUFRO2FBQVo7WUFDSSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQVMsMEJBQWtCLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDckYsQ0FBQztZQUNELE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQzFCLENBQUM7OztPQUFBO0lBR0Qsc0JBQUksb0NBQUs7YUFBVDtZQUVJLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDdkIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBUywwQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNoRixDQUFDO1lBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDdkIsQ0FBQzs7O09BQUE7SUFNTSxxREFBeUIsR0FBaEMsVUFBaUMsVUFBa0I7UUFDL0MsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsaUJBQWlCLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLFFBQVEsRUFBRSxVQUFVLENBQUMsQ0FBQztJQUNqRyxDQUFDO0lBRU0saUNBQUssR0FBWjtRQUNJLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLGlCQUFpQixDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNwRixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUNuQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztJQUMxQixDQUFDO0lBL0JhLHNDQUFvQixHQUFHLE9BQU8sQ0FBQztJQUhqRDtRQUFDLGlCQUFVLEVBQUU7O3lCQUFBO0lBbUNiLHdCQUFDO0FBQUQsQ0FsQ0EsQUFrQ0MsSUFBQTtBQWxDWSx5QkFBaUIsb0JBa0M3QixDQUFBIiwiZmlsZSI6ImFwcC9jb3JlL3BhcmVudC11c2VyLnNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBBcGlDbGllbnRTZXJ2aWNlIH0gZnJvbSAnLi4vYXBpL2luZGV4JztcclxuaW1wb3J0IHsgTG9jYWxTdG9yYWdlU2VydmljZSB9IGZyb20gJ2FuZ3VsYXItMi1sb2NhbC1zdG9yYWdlJztcclxuaW1wb3J0IHsgQWNjZXNzVG9rZW5TZXJ2aWNlIH0gZnJvbSAnLi9pbmRleCc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBQYXJlbnRVc2VyU2VydmljZSB7XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBMQVNUX1NFTEVDVEVEX0NFTlRSRSA9ICdVc2VyXyc7XHJcblxyXG4gICAgcHJpdmF0ZSBfcGFyZW50SWQ6IHN0cmluZyA9IG51bGw7XHJcbiAgICBnZXQgUGFyZW50SWQoKTogc3RyaW5nIHtcclxuICAgICAgICBpZiAodGhpcy5fcGFyZW50SWQgPT09IG51bGwpIHtcclxuICAgICAgICAgICAgdGhpcy5fcGFyZW50SWQgPSB0aGlzLl9zdG9yYWdlU2VydmljZS5nZXQ8c3RyaW5nPihBY2Nlc3NUb2tlblNlcnZpY2UuU1VCSkVDVF9JRCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzLl9wYXJlbnRJZDtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIF9lbWFpbDogc3RyaW5nID0gbnVsbDtcclxuICAgIGdldCBFbWFpbCgpOiBzdHJpbmcge1xyXG5cclxuICAgICAgICBpZiAodGhpcy5fZW1haWwgPT09IG51bGwpIHtcclxuICAgICAgICAgICAgdGhpcy5fZW1haWwgPSB0aGlzLl9zdG9yYWdlU2VydmljZS5nZXQ8c3RyaW5nPihBY2Nlc3NUb2tlblNlcnZpY2UuVVNFUk5BTUUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpcy5fZW1haWw7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfYXBpQ2xpZW50U2VydmljZTogQXBpQ2xpZW50U2VydmljZSxcclxuICAgICAgICBwcml2YXRlIF9zdG9yYWdlU2VydmljZTogTG9jYWxTdG9yYWdlU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBTZXRMYXN0U2VsZWN0ZWRDZW50cmVOYW1lKGNlbnRyZU5hbWU6IHN0cmluZyk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuX3N0b3JhZ2VTZXJ2aWNlLnNldChQYXJlbnRVc2VyU2VydmljZS5MQVNUX1NFTEVDVEVEX0NFTlRSRSArIHRoaXMuUGFyZW50SWQsIGNlbnRyZU5hbWUpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBDbGVhcigpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLl9zdG9yYWdlU2VydmljZS5yZW1vdmUoUGFyZW50VXNlclNlcnZpY2UuTEFTVF9TRUxFQ1RFRF9DRU5UUkUgKyB0aGlzLlBhcmVudElkKTtcclxuICAgICAgICB0aGlzLl9lbWFpbCA9IG51bGw7XHJcbiAgICAgICAgdGhpcy5fcGFyZW50SWQgPSBudWxsO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
