"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var rxjs_1 = require('rxjs');
var router_1 = require('@angular/router');
var index_1 = require('../../shared/index');
var index_2 = require('../../core/index');
var NavigationGuardService = (function () {
    function NavigationGuardService(_userService, router) {
        this._userService = _userService;
        this.router = router;
    }
    NavigationGuardService.prototype.canActivate = function (route, state) {
        var _this = this;
        if (index_1.Config.ENABLE_AUTHORISATION === 'false') {
            return rxjs_1.Observable.of(true);
        }
        return this._userService
            .hasCentres$()
            .flatMap(function (hasCentre) {
            if (hasCentre) {
                return rxjs_1.Observable.of(true);
            }
            if (state.url !== '/centre-portal/centre') {
                _this.router.navigate(['centre-portal/centre']);
                return rxjs_1.Observable.of(false);
            }
            return rxjs_1.Observable.of(true);
        })
            .catch(function (err) {
            _this.router.navigate(['login']);
            return rxjs_1.Observable.of(false);
        });
    };
    NavigationGuardService.prototype.canActivateChild = function (route, state) {
        return this.canActivate(route, state);
    };
    NavigationGuardService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_2.CentreUserService, router_1.Router])
    ], NavigationGuardService);
    return NavigationGuardService;
}());
exports.NavigationGuardService = NavigationGuardService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zaGFyZWQvbmF2aWdhdGlvbkd1YXJkL25hdmlnYXRpb25HdWFyZC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBMkIsZUFBZSxDQUFDLENBQUE7QUFDM0MscUJBQTJCLE1BQU0sQ0FBQyxDQUFBO0FBQ2xDLHVCQUlPLGlCQUFpQixDQUFDLENBQUE7QUFFekIsc0JBQXVCLG9CQUFvQixDQUFDLENBQUE7QUFDNUMsc0JBQWtDLGtCQUFrQixDQUFDLENBQUE7QUFHckQ7SUFFSSxnQ0FBb0IsWUFBK0IsRUFBVSxNQUFjO1FBQXZELGlCQUFZLEdBQVosWUFBWSxDQUFtQjtRQUFVLFdBQU0sR0FBTixNQUFNLENBQVE7SUFDM0UsQ0FBQztJQUVELDRDQUFXLEdBQVgsVUFBWSxLQUE2QixFQUFFLEtBQTBCO1FBQXJFLGlCQTRCQztRQTFCRyxFQUFFLENBQUMsQ0FBQyxjQUFNLENBQUMsb0JBQW9CLEtBQUssT0FBTyxDQUFDLENBQUMsQ0FBQztZQUMxQyxNQUFNLENBQUMsaUJBQVUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDL0IsQ0FBQztRQUVELE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWTthQUNuQixXQUFXLEVBQUU7YUFDYixPQUFPLENBQUMsVUFBQSxTQUFTO1lBRWQsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFDWixNQUFNLENBQUMsaUJBQVUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDL0IsQ0FBQztZQUlELEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLEtBQUssdUJBQXVCLENBQUMsQ0FBQyxDQUFDO2dCQUN4QyxLQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQztnQkFDL0MsTUFBTSxDQUFDLGlCQUFVLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2hDLENBQUM7WUFHRCxNQUFNLENBQUMsaUJBQVUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDL0IsQ0FBQyxDQUFDO2FBQ0QsS0FBSyxDQUFDLFVBQUEsR0FBRztZQUNOLEtBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUNoQyxNQUFNLENBQUMsaUJBQVUsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDaEMsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRUQsaURBQWdCLEdBQWhCLFVBQWlCLEtBQTZCLEVBQUUsS0FBMEI7UUFDdEUsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzFDLENBQUM7SUF0Q0w7UUFBQyxpQkFBVSxFQUFFOzs4QkFBQTtJQXVDYiw2QkFBQztBQUFELENBdENBLEFBc0NDLElBQUE7QUF0Q1ksOEJBQXNCLHlCQXNDbEMsQ0FBQSIsImZpbGUiOiJhcHAvc2hhcmVkL25hdmlnYXRpb25HdWFyZC9uYXZpZ2F0aW9uR3VhcmQuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQge1xyXG4gICAgQ2FuQWN0aXZhdGUsIENhbkFjdGl2YXRlQ2hpbGQsIFJvdXRlcixcclxuICAgIEFjdGl2YXRlZFJvdXRlU25hcHNob3QsXHJcbiAgICBSb3V0ZXJTdGF0ZVNuYXBzaG90XHJcbn0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuXHJcbmltcG9ydCB7IENvbmZpZyB9IGZyb20gJy4uLy4uL3NoYXJlZC9pbmRleCc7XHJcbmltcG9ydCB7IENlbnRyZVVzZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vY29yZS9pbmRleCc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBOYXZpZ2F0aW9uR3VhcmRTZXJ2aWNlIGltcGxlbWVudHMgQ2FuQWN0aXZhdGUsIENhbkFjdGl2YXRlQ2hpbGQge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX3VzZXJTZXJ2aWNlOiBDZW50cmVVc2VyU2VydmljZSwgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcikge1xyXG4gICAgfVxyXG5cclxuICAgIGNhbkFjdGl2YXRlKHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBzdGF0ZTogUm91dGVyU3RhdGVTbmFwc2hvdCk6IE9ic2VydmFibGU8Ym9vbGVhbj4ge1xyXG5cclxuICAgICAgICBpZiAoQ29uZmlnLkVOQUJMRV9BVVRIT1JJU0FUSU9OID09PSAnZmFsc2UnKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBPYnNlcnZhYmxlLm9mKHRydWUpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3VzZXJTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5oYXNDZW50cmVzJCgpXHJcbiAgICAgICAgICAgIC5mbGF0TWFwKGhhc0NlbnRyZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAvLyBVc2VyIGhhcyBjZW50cmUsIGNhbiBuYXZpZ2F0ZSBhY3Jvc3MgdGhlIGFwcFxyXG4gICAgICAgICAgICAgICAgaWYgKGhhc0NlbnRyZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBPYnNlcnZhYmxlLm9mKHRydWUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIC8vIElmIHVzZXIgaGFzIG5vIGNlbnRyZSwgYW5kIGlzIG5vdCBhbHJlYWR5IG9uIGNlbnRyZSBwYWdlLFxyXG4gICAgICAgICAgICAgICAgLy8gcmVkaXJlY3QgdG8gY2VudHJlIHBhZ2VcclxuICAgICAgICAgICAgICAgIGlmIChzdGF0ZS51cmwgIT09ICcvY2VudHJlLXBvcnRhbC9jZW50cmUnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWydjZW50cmUtcG9ydGFsL2NlbnRyZSddKTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gT2JzZXJ2YWJsZS5vZihmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gdXNlciBoYXMgbm8gY2VudHJlLCBidXQgaXMgYWxyZWFkeSBvbiBjZW50cmUgcGFnZVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIE9ic2VydmFibGUub2YodHJ1ZSk7XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIC5jYXRjaChlcnIgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWydsb2dpbiddKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBPYnNlcnZhYmxlLm9mKGZhbHNlKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgY2FuQWN0aXZhdGVDaGlsZChyb3V0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgc3RhdGU6IFJvdXRlclN0YXRlU25hcHNob3QpOiBPYnNlcnZhYmxlPGJvb2xlYW4+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jYW5BY3RpdmF0ZShyb3V0ZSwgc3RhdGUpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
