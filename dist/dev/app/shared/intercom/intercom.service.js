"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var IntercomService = (function () {
    function IntercomService() {
        this.isInit = false;
    }
    IntercomService.prototype.initUserNotLoggedIn = function (appId) {
        if (!this.isInit) {
            this.init(appId, {
                app_id: appId
            });
            this.isInit = true;
        }
        window.Intercom('boot');
    };
    IntercomService.prototype.initUserLoggedIn = function (appId, userEmail) {
        var intercomCmd = 'update';
        if (!this.isInit) {
            this.init(appId, {
                app_id: appId,
                email: userEmail
            });
            intercomCmd = 'boot';
            this.isInit = true;
        }
        window.Intercom(intercomCmd, {
            app_id: appId,
            email: userEmail
        });
    };
    IntercomService.prototype.userLoggedOut = function (appId) {
        this.isInit = false;
        window.Intercom('shutdown');
    };
    IntercomService.prototype.init = function (appId, intercomSettings) {
        var w = window;
        var ic = w.Intercom;
        if (typeof ic === 'function') {
            ic('reattach_activator');
            ic('update', intercomSettings);
        }
        else {
            var d = document;
            var i = function () {
                i.c(arguments);
            };
            i.q = [];
            i.c = function (args) {
                i.q.push(args);
            };
            w.Intercom = i;
            var l = function () {
                var s = d.createElement('script');
                s.type = 'text/javascript';
                s.async = true;
                s.src = 'https://widget.intercom.io/widget/' + appId;
                var x = d.getElementsByTagName('script')[0];
                x.parentNode.insertBefore(s, x);
            };
            if (w.attachEvent) {
                w.attachEvent('onload', l);
            }
            else {
                w.addEventListener('load', l, false);
            }
        }
    };
    IntercomService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], IntercomService);
    return IntercomService;
}());
exports.IntercomService = IntercomService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zaGFyZWQvaW50ZXJjb20vaW50ZXJjb20uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQTJCLGVBQWUsQ0FBQyxDQUFBO0FBTTNDO0lBQUE7UUFFWSxXQUFNLEdBQVksS0FBSyxDQUFDO0lBcUVwQyxDQUFDO0lBbkVVLDZDQUFtQixHQUExQixVQUEyQixLQUFhO1FBRXBDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDZixJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRTtnQkFDYixNQUFNLEVBQUUsS0FBSzthQUNoQixDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUN2QixDQUFDO1FBQ0QsTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUM1QixDQUFDO0lBRU0sMENBQWdCLEdBQXZCLFVBQXdCLEtBQWEsRUFBRSxTQUFpQjtRQUVwRCxJQUFJLFdBQVcsR0FBRyxRQUFRLENBQUM7UUFDM0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUNmLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUNYO2dCQUNJLE1BQU0sRUFBRSxLQUFLO2dCQUNiLEtBQUssRUFBRSxTQUFTO2FBQ25CLENBQUMsQ0FBQztZQUNQLFdBQVcsR0FBRyxNQUFNLENBQUM7WUFDckIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDdkIsQ0FBQztRQUVELE1BQU0sQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUN2QjtZQUNJLE1BQU0sRUFBRSxLQUFLO1lBQ2IsS0FBSyxFQUFFLFNBQVM7U0FDbkIsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVNLHVDQUFhLEdBQXBCLFVBQXFCLEtBQWE7UUFDOUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsTUFBTSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUNoQyxDQUFDO0lBRU8sOEJBQUksR0FBWixVQUFhLEtBQWEsRUFBRSxnQkFBcUI7UUFDN0MsSUFBSSxDQUFDLEdBQUcsTUFBTSxDQUFDO1FBQ2YsSUFBSSxFQUFFLEdBQUcsQ0FBQyxDQUFDLFFBQVEsQ0FBQztRQUNwQixFQUFFLENBQUMsQ0FBQyxPQUFPLEVBQUUsS0FBSyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQzNCLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1lBQ3pCLEVBQUUsQ0FBQyxRQUFRLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztRQUNuQyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixJQUFJLENBQUMsR0FBRyxRQUFRLENBQUM7WUFDakIsSUFBSSxDQUFDLEdBQVE7Z0JBQ1QsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNuQixDQUFDLENBQUM7WUFDRixDQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUNULENBQUMsQ0FBQyxDQUFDLEdBQUcsVUFBVSxJQUFTO2dCQUNyQixDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNuQixDQUFDLENBQUM7WUFDRixDQUFDLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQztZQUNmLElBQUksQ0FBQyxHQUFHO2dCQUNKLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ2xDLENBQUMsQ0FBQyxJQUFJLEdBQUcsaUJBQWlCLENBQUM7Z0JBQzNCLENBQUMsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO2dCQUNmLENBQUMsQ0FBQyxHQUFHLEdBQUcsb0NBQW9DLEdBQUcsS0FBSyxDQUFDO2dCQUNyRCxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzVDLENBQUMsQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNwQyxDQUFDLENBQUM7WUFDRixFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztnQkFDaEIsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDL0IsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQ3pDLENBQUM7UUFDTCxDQUFDO0lBQ0wsQ0FBQztJQXZFTDtRQUFDLGlCQUFVLEVBQUU7O3VCQUFBO0lBd0ViLHNCQUFDO0FBQUQsQ0F2RUEsQUF1RUMsSUFBQTtBQXZFWSx1QkFBZSxrQkF1RTNCLENBQUEiLCJmaWxlIjoiYXBwL3NoYXJlZC9pbnRlcmNvbS9pbnRlcmNvbS5zZXJ2aWNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuZGVjbGFyZSB2YXIgd2luZG93OiBhbnk7XHJcbmRlY2xhcmUgdmFyIGludGVyY29tU2V0dGluZ3M6IGFueTtcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIEludGVyY29tU2VydmljZSB7XHJcblxyXG4gICAgcHJpdmF0ZSBpc0luaXQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICBwdWJsaWMgaW5pdFVzZXJOb3RMb2dnZWRJbihhcHBJZDogc3RyaW5nKTogdm9pZCB7XHJcblxyXG4gICAgICAgIGlmICghdGhpcy5pc0luaXQpIHtcclxuICAgICAgICAgICAgdGhpcy5pbml0KGFwcElkLCB7XHJcbiAgICAgICAgICAgICAgICBhcHBfaWQ6IGFwcElkXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB0aGlzLmlzSW5pdCA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHdpbmRvdy5JbnRlcmNvbSgnYm9vdCcpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBpbml0VXNlckxvZ2dlZEluKGFwcElkOiBzdHJpbmcsIHVzZXJFbWFpbDogc3RyaW5nKTogdm9pZCB7XHJcblxyXG4gICAgICAgIGxldCBpbnRlcmNvbUNtZCA9ICd1cGRhdGUnO1xyXG4gICAgICAgIGlmICghdGhpcy5pc0luaXQpIHtcclxuICAgICAgICAgICAgdGhpcy5pbml0KGFwcElkLFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIGFwcF9pZDogYXBwSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgZW1haWw6IHVzZXJFbWFpbFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGludGVyY29tQ21kID0gJ2Jvb3QnO1xyXG4gICAgICAgICAgICB0aGlzLmlzSW5pdCA9IHRydWU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB3aW5kb3cuSW50ZXJjb20oaW50ZXJjb21DbWQsXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGFwcF9pZDogYXBwSWQsXHJcbiAgICAgICAgICAgICAgICBlbWFpbDogdXNlckVtYWlsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyB1c2VyTG9nZ2VkT3V0KGFwcElkOiBzdHJpbmcpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmlzSW5pdCA9IGZhbHNlO1xyXG4gICAgICAgIHdpbmRvdy5JbnRlcmNvbSgnc2h1dGRvd24nKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGluaXQoYXBwSWQ6IHN0cmluZywgaW50ZXJjb21TZXR0aW5nczogYW55KTogdm9pZCB7XHJcbiAgICAgICAgdmFyIHcgPSB3aW5kb3c7XHJcbiAgICAgICAgdmFyIGljID0gdy5JbnRlcmNvbTtcclxuICAgICAgICBpZiAodHlwZW9mIGljID09PSAnZnVuY3Rpb24nKSB7XHJcbiAgICAgICAgICAgIGljKCdyZWF0dGFjaF9hY3RpdmF0b3InKTtcclxuICAgICAgICAgICAgaWMoJ3VwZGF0ZScsIGludGVyY29tU2V0dGluZ3MpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHZhciBkID0gZG9jdW1lbnQ7XHJcbiAgICAgICAgICAgIHZhciBpOiBhbnkgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICBpLmMoYXJndW1lbnRzKTtcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgaS5xID0gW107XHJcbiAgICAgICAgICAgIGkuYyA9IGZ1bmN0aW9uIChhcmdzOiBhbnkpIHtcclxuICAgICAgICAgICAgICAgIGkucS5wdXNoKGFyZ3MpO1xyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB3LkludGVyY29tID0gaTtcclxuICAgICAgICAgICAgbGV0IGwgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgcyA9IGQuY3JlYXRlRWxlbWVudCgnc2NyaXB0Jyk7XHJcbiAgICAgICAgICAgICAgICBzLnR5cGUgPSAndGV4dC9qYXZhc2NyaXB0JztcclxuICAgICAgICAgICAgICAgIHMuYXN5bmMgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgcy5zcmMgPSAnaHR0cHM6Ly93aWRnZXQuaW50ZXJjb20uaW8vd2lkZ2V0LycgKyBhcHBJZDtcclxuICAgICAgICAgICAgICAgIHZhciB4ID0gZC5nZXRFbGVtZW50c0J5VGFnTmFtZSgnc2NyaXB0JylbMF07XHJcbiAgICAgICAgICAgICAgICB4LnBhcmVudE5vZGUuaW5zZXJ0QmVmb3JlKHMsIHgpO1xyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICBpZiAody5hdHRhY2hFdmVudCkge1xyXG4gICAgICAgICAgICAgICAgdy5hdHRhY2hFdmVudCgnb25sb2FkJywgbCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB3LmFkZEV2ZW50TGlzdGVuZXIoJ2xvYWQnLCBsLCBmYWxzZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19
