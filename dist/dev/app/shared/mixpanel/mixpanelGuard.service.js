"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('./index');
var MixpanelGuard = (function () {
    function MixpanelGuard(_mixpanelService) {
        this._mixpanelService = _mixpanelService;
    }
    MixpanelGuard.prototype.canActivate = function (route, state) {
        this._mixpanelService.track(state.url);
        return true;
    };
    MixpanelGuard.prototype.canActivateChild = function (route, state) {
        return this.canActivate(route, state);
    };
    MixpanelGuard = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.MixpanelService])
    ], MixpanelGuard);
    return MixpanelGuard;
}());
exports.MixpanelGuard = MixpanelGuard;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zaGFyZWQvbWl4cGFuZWwvbWl4cGFuZWxHdWFyZC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBMkIsZUFBZSxDQUFDLENBQUE7QUFNM0Msc0JBQWdDLFNBQVMsQ0FBQyxDQUFBO0FBRzFDO0lBRUksdUJBQW9CLGdCQUFpQztRQUFqQyxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWlCO0lBQ3JELENBQUM7SUFFRCxtQ0FBVyxHQUFYLFVBQVksS0FBNkIsRUFBRSxLQUEwQjtRQUVqRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN2QyxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRCx3Q0FBZ0IsR0FBaEIsVUFBaUIsS0FBNkIsRUFBRSxLQUEwQjtRQUN0RSxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDMUMsQ0FBQztJQWRMO1FBQUMsaUJBQVUsRUFBRTs7cUJBQUE7SUFlYixvQkFBQztBQUFELENBZEEsQUFjQyxJQUFBO0FBZFkscUJBQWEsZ0JBY3pCLENBQUEiLCJmaWxlIjoiYXBwL3NoYXJlZC9taXhwYW5lbC9taXhwYW5lbEd1YXJkLnNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7XHJcbiAgICBDYW5BY3RpdmF0ZSwgQ2FuQWN0aXZhdGVDaGlsZCxcclxuICAgIEFjdGl2YXRlZFJvdXRlU25hcHNob3QsIFJvdXRlclN0YXRlU25hcHNob3RcclxufSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5cclxuaW1wb3J0IHsgTWl4cGFuZWxTZXJ2aWNlIH0gZnJvbSAnLi9pbmRleCc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBNaXhwYW5lbEd1YXJkIGltcGxlbWVudHMgQ2FuQWN0aXZhdGUsIENhbkFjdGl2YXRlQ2hpbGQge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX21peHBhbmVsU2VydmljZTogTWl4cGFuZWxTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgY2FuQWN0aXZhdGUocm91dGU6IEFjdGl2YXRlZFJvdXRlU25hcHNob3QsIHN0YXRlOiBSb3V0ZXJTdGF0ZVNuYXBzaG90KSB7XHJcblxyXG4gICAgICAgIHRoaXMuX21peHBhbmVsU2VydmljZS50cmFjayhzdGF0ZS51cmwpO1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG5cclxuICAgIGNhbkFjdGl2YXRlQ2hpbGQocm91dGU6IEFjdGl2YXRlZFJvdXRlU25hcHNob3QsIHN0YXRlOiBSb3V0ZXJTdGF0ZVNuYXBzaG90KTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY2FuQWN0aXZhdGUocm91dGUsIHN0YXRlKTtcclxuICAgIH1cclxufVxyXG4iXX0=
