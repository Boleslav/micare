"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../index');
var MixpanelService = (function () {
    function MixpanelService() {
    }
    MixpanelService.prototype.init = function (token) {
        mixpanel.init(token);
    };
    MixpanelService.prototype.track = function (message) {
        if (index_1.Config.ENV === 'Prod')
            mixpanel.track(message, { Environment: index_1.Config.ENV });
    };
    MixpanelService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], MixpanelService);
    return MixpanelService;
}());
exports.MixpanelService = MixpanelService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zaGFyZWQvbWl4cGFuZWwvbWl4cGFuZWwuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQTJCLGVBQWUsQ0FBQyxDQUFBO0FBQzNDLHNCQUF1QixVQUFVLENBQUMsQ0FBQTtBQUtsQztJQUFBO0lBVUEsQ0FBQztJQVJVLDhCQUFJLEdBQVgsVUFBWSxLQUFhO1FBQ3JCLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDekIsQ0FBQztJQUVNLCtCQUFLLEdBQVosVUFBYSxPQUFlO1FBQ3hCLEVBQUUsQ0FBQyxDQUFDLGNBQU0sQ0FBQyxHQUFHLEtBQUssTUFBTSxDQUFDO1lBQ3RCLFFBQVEsQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFLEVBQUUsV0FBVyxFQUFFLGNBQU0sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDO0lBQzdELENBQUM7SUFWTDtRQUFDLGlCQUFVLEVBQUU7O3VCQUFBO0lBV2Isc0JBQUM7QUFBRCxDQVZBLEFBVUMsSUFBQTtBQVZZLHVCQUFlLGtCQVUzQixDQUFBIiwiZmlsZSI6ImFwcC9zaGFyZWQvbWl4cGFuZWwvbWl4cGFuZWwuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29uZmlnIH0gZnJvbSAnLi4vaW5kZXgnO1xyXG5cclxuZGVjbGFyZSB2YXIgbWl4cGFuZWw6IGFueTtcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIE1peHBhbmVsU2VydmljZSB7XHJcblxyXG4gICAgcHVibGljIGluaXQodG9rZW46IHN0cmluZyk6IHZvaWQge1xyXG4gICAgICAgIG1peHBhbmVsLmluaXQodG9rZW4pO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyB0cmFjayhtZXNzYWdlOiBzdHJpbmcpOiB2b2lkIHtcclxuICAgICAgICBpZiAoQ29uZmlnLkVOViA9PT0gJ1Byb2QnKVxyXG4gICAgICAgICAgICBtaXhwYW5lbC50cmFjayhtZXNzYWdlLCB7IEVudmlyb25tZW50OiBDb25maWcuRU5WIH0pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
