"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ngx_uploader_1 = require('ngx-uploader');
var PictureUploaderComponent = (function () {
    function PictureUploaderComponent(renderer) {
        this.renderer = renderer;
        this.title = '';
        this.defaultPicture = '';
        this.picture = '';
        this.uploaderOptions = { url: '' };
        this.canDelete = true;
        this.onUpload = new core_1.EventEmitter();
        this.onUploadCompleted = new core_1.EventEmitter();
    }
    PictureUploaderComponent.prototype.beforeUpload = function (uploadingFile) {
        var files = this._fileUpload.nativeElement.files;
        if (files.length) {
            var file = files[0];
            this._changePicture(file);
            if (!this._canUploadOnServer()) {
                uploadingFile.setAbort();
            }
            else {
                this.uploadInProgress = true;
            }
        }
    };
    PictureUploaderComponent.prototype.bringFileSelector = function () {
        this.renderer.invokeElementMethod(this._fileUpload.nativeElement, 'click');
        return false;
    };
    PictureUploaderComponent.prototype.removePicture = function () {
        this.picture = '';
        return false;
    };
    PictureUploaderComponent.prototype._changePicture = function (file) {
        var _this = this;
        var reader = new FileReader();
        reader.addEventListener('load', function (event) {
            _this.picture = event.target.result;
        }, false);
        reader.readAsDataURL(file);
    };
    PictureUploaderComponent.prototype._onUpload = function (data) {
        if (data['done'] || data['abort'] || data['error']) {
            this._onUploadCompleted(data);
        }
        else {
            this.onUpload.emit(data);
        }
    };
    PictureUploaderComponent.prototype._onUploadCompleted = function (data) {
        this.uploadInProgress = false;
        this.onUploadCompleted.emit(data);
    };
    PictureUploaderComponent.prototype._canUploadOnServer = function () {
        return !!this.uploaderOptions['url'];
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], PictureUploaderComponent.prototype, "title", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], PictureUploaderComponent.prototype, "defaultPicture", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], PictureUploaderComponent.prototype, "picture", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', ngx_uploader_1.NgUploaderOptions)
    ], PictureUploaderComponent.prototype, "uploaderOptions", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], PictureUploaderComponent.prototype, "canDelete", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], PictureUploaderComponent.prototype, "onUpload", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], PictureUploaderComponent.prototype, "onUploadCompleted", void 0);
    __decorate([
        core_1.ViewChild('fileUpload'), 
        __metadata('design:type', core_1.ElementRef)
    ], PictureUploaderComponent.prototype, "_fileUpload", void 0);
    PictureUploaderComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'picture-uploader-cmp',
            styleUrls: ['picture-uploader.component.css'],
            templateUrl: 'picture-uploader.component.html',
        }), 
        __metadata('design:paramtypes', [core_1.Renderer])
    ], PictureUploaderComponent);
    return PictureUploaderComponent;
}());
exports.PictureUploaderComponent = PictureUploaderComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zaGFyZWQvcGljdHVyZS11cGxvYWRlci9waWN0dXJlLXVwbG9hZGVyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQXdGLGVBQWUsQ0FBQyxDQUFBO0FBQ3hHLDZCQUFrQyxjQUFjLENBQUMsQ0FBQTtBQVFqRDtJQWdCSSxrQ0FBb0IsUUFBa0I7UUFBbEIsYUFBUSxHQUFSLFFBQVEsQ0FBVTtRQWQ3QixVQUFLLEdBQVcsRUFBRSxDQUFDO1FBQ25CLG1CQUFjLEdBQVcsRUFBRSxDQUFDO1FBQzVCLFlBQU8sR0FBVyxFQUFFLENBQUM7UUFFckIsb0JBQWUsR0FBc0IsRUFBRSxHQUFHLEVBQUUsRUFBRSxFQUFFLENBQUM7UUFDakQsY0FBUyxHQUFZLElBQUksQ0FBQztRQUV6QixhQUFRLEdBQUcsSUFBSSxtQkFBWSxFQUFPLENBQUM7UUFDbkMsc0JBQWlCLEdBQUcsSUFBSSxtQkFBWSxFQUFPLENBQUM7SUFPdEQsQ0FBQztJQUVELCtDQUFZLEdBQVosVUFBYSxhQUFrQjtRQUMzQixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUM7UUFFakQsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDZixJQUFNLElBQUksR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUUxQixFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDN0IsYUFBYSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQzdCLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDSixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO1lBQ2pDLENBQUM7UUFDTCxDQUFDO0lBQ0wsQ0FBQztJQUVELG9EQUFpQixHQUFqQjtRQUNJLElBQUksQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDM0UsTUFBTSxDQUFDLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRUQsZ0RBQWEsR0FBYjtRQUNJLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO1FBQ2xCLE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFDakIsQ0FBQztJQUVELGlEQUFjLEdBQWQsVUFBZSxJQUFVO1FBQXpCLGlCQU9DO1FBTEcsSUFBTSxNQUFNLEdBQUcsSUFBSSxVQUFVLEVBQUUsQ0FBQztRQUNoQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFLFVBQUMsS0FBWTtZQUN6QyxLQUFJLENBQUMsT0FBTyxHQUFTLEtBQUssQ0FBQyxNQUFPLENBQUMsTUFBTSxDQUFDO1FBQzlDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUNWLE1BQU0sQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDL0IsQ0FBQztJQUVELDRDQUFTLEdBQVQsVUFBVSxJQUFTO1FBRWYsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2pELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNsQyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM3QixDQUFDO0lBQ0wsQ0FBQztJQUVELHFEQUFrQixHQUFsQixVQUFtQixJQUFTO1FBQ3hCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7UUFDOUIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBRUQscURBQWtCLEdBQWxCO1FBQ0ksTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3pDLENBQUM7SUFuRUQ7UUFBQyxZQUFLLEVBQUU7OzJEQUFBO0lBQ1I7UUFBQyxZQUFLLEVBQUU7O29FQUFBO0lBQ1I7UUFBQyxZQUFLLEVBQUU7OzZEQUFBO0lBRVI7UUFBQyxZQUFLLEVBQUU7O3FFQUFBO0lBQ1I7UUFBQyxZQUFLLEVBQUU7OytEQUFBO0lBRVI7UUFBQyxhQUFNLEVBQUU7OzhEQUFBO0lBQ1Q7UUFBQyxhQUFNLEVBQUU7O3VFQUFBO0lBRVQ7UUFBQyxnQkFBUyxDQUFDLFlBQVksQ0FBQzs7aUVBQUE7SUFsQjVCO1FBQUMsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsc0JBQXNCO1lBQ2hDLFNBQVMsRUFBRSxDQUFDLGdDQUFnQyxDQUFDO1lBQzdDLFdBQVcsRUFBRSxpQ0FBaUM7U0FDakQsQ0FBQzs7Z0NBQUE7SUF1RUYsK0JBQUM7QUFBRCxDQXRFQSxBQXNFQyxJQUFBO0FBdEVZLGdDQUF3QiwyQkFzRXBDLENBQUEiLCJmaWxlIjoiYXBwL3NoYXJlZC9waWN0dXJlLXVwbG9hZGVyL3BpY3R1cmUtdXBsb2FkZXIuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBWaWV3Q2hpbGQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciwgRWxlbWVudFJlZiwgUmVuZGVyZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTmdVcGxvYWRlck9wdGlvbnMgfSBmcm9tICduZ3gtdXBsb2FkZXInO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgc2VsZWN0b3I6ICdwaWN0dXJlLXVwbG9hZGVyLWNtcCcsXHJcbiAgICBzdHlsZVVybHM6IFsncGljdHVyZS11cGxvYWRlci5jb21wb25lbnQuY3NzJ10sXHJcbiAgICB0ZW1wbGF0ZVVybDogJ3BpY3R1cmUtdXBsb2FkZXIuY29tcG9uZW50Lmh0bWwnLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgUGljdHVyZVVwbG9hZGVyQ29tcG9uZW50IHtcclxuXHJcbiAgICBASW5wdXQoKSB0aXRsZTogc3RyaW5nID0gJyc7XHJcbiAgICBASW5wdXQoKSBkZWZhdWx0UGljdHVyZTogc3RyaW5nID0gJyc7XHJcbiAgICBASW5wdXQoKSBwaWN0dXJlOiBzdHJpbmcgPSAnJztcclxuXHJcbiAgICBASW5wdXQoKSB1cGxvYWRlck9wdGlvbnM6IE5nVXBsb2FkZXJPcHRpb25zID0geyB1cmw6ICcnIH07XHJcbiAgICBASW5wdXQoKSBjYW5EZWxldGU6IGJvb2xlYW4gPSB0cnVlO1xyXG5cclxuICAgIEBPdXRwdXQoKSBvblVwbG9hZCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIG9uVXBsb2FkQ29tcGxldGVkID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcblxyXG4gICAgQFZpZXdDaGlsZCgnZmlsZVVwbG9hZCcpIHB1YmxpYyBfZmlsZVVwbG9hZDogRWxlbWVudFJlZjtcclxuXHJcbiAgICBwdWJsaWMgdXBsb2FkSW5Qcm9ncmVzczogYm9vbGVhbjtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJlbmRlcmVyOiBSZW5kZXJlcikge1xyXG4gICAgfVxyXG5cclxuICAgIGJlZm9yZVVwbG9hZCh1cGxvYWRpbmdGaWxlOiBhbnkpOiB2b2lkIHtcclxuICAgICAgICBsZXQgZmlsZXMgPSB0aGlzLl9maWxlVXBsb2FkLm5hdGl2ZUVsZW1lbnQuZmlsZXM7XHJcblxyXG4gICAgICAgIGlmIChmaWxlcy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgY29uc3QgZmlsZSA9IGZpbGVzWzBdO1xyXG4gICAgICAgICAgICB0aGlzLl9jaGFuZ2VQaWN0dXJlKGZpbGUpO1xyXG5cclxuICAgICAgICAgICAgaWYgKCF0aGlzLl9jYW5VcGxvYWRPblNlcnZlcigpKSB7XHJcbiAgICAgICAgICAgICAgICB1cGxvYWRpbmdGaWxlLnNldEFib3J0KCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnVwbG9hZEluUHJvZ3Jlc3MgPSB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGJyaW5nRmlsZVNlbGVjdG9yKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHRoaXMucmVuZGVyZXIuaW52b2tlRWxlbWVudE1ldGhvZCh0aGlzLl9maWxlVXBsb2FkLm5hdGl2ZUVsZW1lbnQsICdjbGljaycpO1xyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICByZW1vdmVQaWN0dXJlKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHRoaXMucGljdHVyZSA9ICcnO1xyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBfY2hhbmdlUGljdHVyZShmaWxlOiBGaWxlKTogdm9pZCB7XHJcblxyXG4gICAgICAgIGNvbnN0IHJlYWRlciA9IG5ldyBGaWxlUmVhZGVyKCk7XHJcbiAgICAgICAgcmVhZGVyLmFkZEV2ZW50TGlzdGVuZXIoJ2xvYWQnLCAoZXZlbnQ6IEV2ZW50KSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMucGljdHVyZSA9ICg8YW55PmV2ZW50LnRhcmdldCkucmVzdWx0O1xyXG4gICAgICAgIH0sIGZhbHNlKTtcclxuICAgICAgICByZWFkZXIucmVhZEFzRGF0YVVSTChmaWxlKTtcclxuICAgIH1cclxuXHJcbiAgICBfb25VcGxvYWQoZGF0YTogYW55KTogdm9pZCB7XHJcblxyXG4gICAgICAgIGlmIChkYXRhWydkb25lJ10gfHwgZGF0YVsnYWJvcnQnXSB8fCBkYXRhWydlcnJvciddKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX29uVXBsb2FkQ29tcGxldGVkKGRhdGEpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMub25VcGxvYWQuZW1pdChkYXRhKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgX29uVXBsb2FkQ29tcGxldGVkKGRhdGE6IGFueSk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMudXBsb2FkSW5Qcm9ncmVzcyA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMub25VcGxvYWRDb21wbGV0ZWQuZW1pdChkYXRhKTtcclxuICAgIH1cclxuXHJcbiAgICBfY2FuVXBsb2FkT25TZXJ2ZXIoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuICEhdGhpcy51cGxvYWRlck9wdGlvbnNbJ3VybCddO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
