"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var PageHeadingParentPortalComponent = (function () {
    function PageHeadingParentPortalComponent() {
    }
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], PageHeadingParentPortalComponent.prototype, "pageDisplayName", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], PageHeadingParentPortalComponent.prototype, "breadcrumbs", void 0);
    PageHeadingParentPortalComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'page-heading-parent-portal-cmp',
            templateUrl: 'page-heading-parent-portal.html',
            styleUrls: ['./page-heading-parent-portal.css']
        }), 
        __metadata('design:paramtypes', [])
    ], PageHeadingParentPortalComponent);
    return PageHeadingParentPortalComponent;
}());
exports.PageHeadingParentPortalComponent = PageHeadingParentPortalComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zaGFyZWQvcGFnZS1oZWFkaW5nLXBhcmVudC1wb3J0YWwvcGFnZS1oZWFkaW5nLXBhcmVudC1wb3J0YWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUFpQyxlQUFlLENBQUMsQ0FBQTtBQVNqRDtJQUFBO0lBR0EsQ0FBQztJQUZBO1FBQUMsWUFBSyxFQUFFOzs2RUFBQTtJQUNSO1FBQUMsWUFBSyxFQUFFOzt5RUFBQTtJQVRUO1FBQUMsZ0JBQVMsQ0FBQztZQUNWLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsZ0NBQWdDO1lBQzFDLFdBQVcsRUFBRSxpQ0FBaUM7WUFDOUMsU0FBUyxFQUFFLENBQUMsa0NBQWtDLENBQUM7U0FDL0MsQ0FBQzs7d0NBQUE7SUFLRix1Q0FBQztBQUFELENBSEEsQUFHQyxJQUFBO0FBSFksd0NBQWdDLG1DQUc1QyxDQUFBIiwiZmlsZSI6ImFwcC9zaGFyZWQvcGFnZS1oZWFkaW5nLXBhcmVudC1wb3J0YWwvcGFnZS1oZWFkaW5nLXBhcmVudC1wb3J0YWwuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuXHRtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG5cdHNlbGVjdG9yOiAncGFnZS1oZWFkaW5nLXBhcmVudC1wb3J0YWwtY21wJyxcclxuXHR0ZW1wbGF0ZVVybDogJ3BhZ2UtaGVhZGluZy1wYXJlbnQtcG9ydGFsLmh0bWwnLFxyXG5cdHN0eWxlVXJsczogWycuL3BhZ2UtaGVhZGluZy1wYXJlbnQtcG9ydGFsLmNzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgUGFnZUhlYWRpbmdQYXJlbnRQb3J0YWxDb21wb25lbnQge1xyXG5cdEBJbnB1dCgpIHBhZ2VEaXNwbGF5TmFtZTogc3RyaW5nO1xyXG5cdEBJbnB1dCgpIGJyZWFkY3J1bWJzOiBzdHJpbmdbXTtcclxufVxyXG4iXX0=
