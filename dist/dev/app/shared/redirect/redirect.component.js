"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var RedirectComponent = (function () {
    function RedirectComponent(_router, _route) {
        this._router = _router;
        this._route = _route;
    }
    RedirectComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route.params.subscribe(function (params) {
            var redirectTo = params['redirectTo'];
            _this._router.navigate([redirectTo]);
        });
    };
    RedirectComponent = __decorate([
        core_1.Component({
            selector: '',
            template: ''
        }), 
        __metadata('design:paramtypes', [router_1.Router, router_1.ActivatedRoute])
    ], RedirectComponent);
    return RedirectComponent;
}());
exports.RedirectComponent = RedirectComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zaGFyZWQvcmVkaXJlY3QvcmVkaXJlY3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBa0MsZUFBZSxDQUFDLENBQUE7QUFDbEQsdUJBQStDLGlCQUFpQixDQUFDLENBQUE7QUFNakU7SUFRSSwyQkFBb0IsT0FBZSxFQUN2QixNQUFzQjtRQURkLFlBQU8sR0FBUCxPQUFPLENBQVE7UUFDdkIsV0FBTSxHQUFOLE1BQU0sQ0FBZ0I7SUFFbEMsQ0FBQztJQUVELG9DQUFRLEdBQVI7UUFBQSxpQkFLQztRQUpHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxVQUFDLE1BQWM7WUFDeEMsSUFBSSxVQUFVLEdBQVcsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQzlDLEtBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztRQUN4QyxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUF0Qkw7UUFBQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLEVBQUU7WUFDWixRQUFRLEVBQUUsRUFBRTtTQUNmLENBQUM7O3lCQUFBO0lBb0JGLHdCQUFDO0FBQUQsQ0FuQkEsQUFtQkMsSUFBQTtBQW5CWSx5QkFBaUIsb0JBbUI3QixDQUFBIiwiZmlsZSI6ImFwcC9zaGFyZWQvcmVkaXJlY3QvcmVkaXJlY3QuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgUm91dGVyLCBBY3RpdmF0ZWRSb3V0ZSwgUGFyYW1zIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICcnLFxyXG4gICAgdGVtcGxhdGU6ICcnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBSZWRpcmVjdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBbmd1bGFyIDIgcmUtdXNlcyBjb21wb25lbnRzLCB0aGVyZWZvcmUgc3dpdGNoaW5nIGJldHdlZW4gY2VudHJlc1xyXG4gICAgICogbWVhbnMgdGhlIGNvbXBvbmVudCB3aWxsIG5vdCByZWxvYWQgdGhlIGNlbnRyZSBpbmZvcm1hdGlvbi5cclxuICAgICAqIEN1cnJlbnQgcHJvcG9zZWQgc29sdXRpb24gaXMgdG8gdXNlIGEgcmVkaXJlY3QgY29tcG9uZW50IHRvIGZvcmNlIGEgcGFnZSByZWZyZXNoXHJcbiAgICAgKiBTZWUgaHR0cHM6Ly9naXRodWIuY29tL2FuZ3VsYXIvYW5ndWxhci9pc3N1ZXMvOTgxMVxyXG4gICAgICovXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9yb3V0ZXI6IFJvdXRlcixcclxuICAgICAgICBwcml2YXRlIF9yb3V0ZTogQWN0aXZhdGVkUm91dGUpIHtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5fcm91dGUucGFyYW1zLnN1YnNjcmliZSgocGFyYW1zOiBQYXJhbXMpID0+IHtcclxuICAgICAgICAgICAgbGV0IHJlZGlyZWN0VG86IHN0cmluZyA9IHBhcmFtc1sncmVkaXJlY3RUbyddO1xyXG4gICAgICAgICAgICB0aGlzLl9yb3V0ZXIubmF2aWdhdGUoW3JlZGlyZWN0VG9dKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG4iXX0=
