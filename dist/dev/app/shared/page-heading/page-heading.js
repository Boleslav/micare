"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var PageHeadingComponent = (function () {
    function PageHeadingComponent() {
    }
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], PageHeadingComponent.prototype, "pageDisplayName", void 0);
    PageHeadingComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'page-heading-cmp',
            templateUrl: 'page-heading.html'
        }), 
        __metadata('design:paramtypes', [])
    ], PageHeadingComponent);
    return PageHeadingComponent;
}());
exports.PageHeadingComponent = PageHeadingComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zaGFyZWQvcGFnZS1oZWFkaW5nL3BhZ2UtaGVhZGluZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQWlDLGVBQWUsQ0FBQyxDQUFBO0FBUWpEO0lBQUE7SUFFQSxDQUFDO0lBREc7UUFBQyxZQUFLLEVBQUU7O2lFQUFBO0lBUFo7UUFBQyxnQkFBUyxDQUFDO1lBQ1YsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxrQkFBa0I7WUFDNUIsV0FBVyxFQUFFLG1CQUFtQjtTQUNoQyxDQUFDOzs0QkFBQTtJQUlGLDJCQUFDO0FBQUQsQ0FGQSxBQUVDLElBQUE7QUFGWSw0QkFBb0IsdUJBRWhDLENBQUEiLCJmaWxlIjoiYXBwL3NoYXJlZC9wYWdlLWhlYWRpbmcvcGFnZS1oZWFkaW5nLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcblx0bW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuXHRzZWxlY3RvcjogJ3BhZ2UtaGVhZGluZy1jbXAnLFxyXG5cdHRlbXBsYXRlVXJsOiAncGFnZS1oZWFkaW5nLmh0bWwnXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgUGFnZUhlYWRpbmdDb21wb25lbnQge1xyXG4gICAgQElucHV0KCkgcGFnZURpc3BsYXlOYW1lOiBzdHJpbmc7XHJcbn1cclxuIl19
