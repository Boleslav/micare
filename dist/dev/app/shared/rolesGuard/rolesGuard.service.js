"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var rxjs_1 = require('rxjs');
var router_1 = require('@angular/router');
var angular2_toaster_1 = require('angular2-toaster');
var index_1 = require('../../core/index');
var RolesGuardService = (function () {
    function RolesGuardService(_userService, _toasterService, router) {
        this._userService = _userService;
        this._toasterService = _toasterService;
        this.router = router;
    }
    RolesGuardService.prototype.canActivate = function (route, state) {
        var _this = this;
        return rxjs_1.Observable.forkJoin([
            this._userService.IsSysAdmin$(),
            this._userService.IsCentreManager$()
        ])
            .flatMap(function (res) {
            var isSysAdmin = res[0];
            var isCentreAdmin = res[1];
            if (isSysAdmin) {
                return _this.allowRedirection();
            }
            else if (isCentreAdmin) {
                return _this.allowRedirection();
            }
            else {
                return _this.redirectToLogin$();
            }
        }).catch(function (err) {
            return _this.redirectToLogin$();
        });
    };
    RolesGuardService.prototype.allowRedirection = function () {
        this._toasterService.clear();
        return rxjs_1.Observable.of(true);
    };
    RolesGuardService.prototype.redirectToLogin$ = function () {
        this._toasterService.clear();
        this._toasterService.pop('error', '', 'Not authorised to access the Centre Admin Portal');
        this._userService.Clear();
        this.router.navigate(['login']);
        return rxjs_1.Observable.of(false);
    };
    RolesGuardService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.CentreUserService, angular2_toaster_1.ToasterService, router_1.Router])
    ], RolesGuardService);
    return RolesGuardService;
}());
exports.RolesGuardService = RolesGuardService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zaGFyZWQvcm9sZXNHdWFyZC9yb2xlc0d1YXJkLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUEyQixlQUFlLENBQUMsQ0FBQTtBQUMzQyxxQkFBMkIsTUFBTSxDQUFDLENBQUE7QUFDbEMsdUJBSU8saUJBQWlCLENBQUMsQ0FBQTtBQUN6QixpQ0FBK0Isa0JBQWtCLENBQUMsQ0FBQTtBQUNsRCxzQkFBa0Msa0JBQWtCLENBQUMsQ0FBQTtBQUdyRDtJQUVJLDJCQUFvQixZQUErQixFQUFVLGVBQStCLEVBQVUsTUFBYztRQUFoRyxpQkFBWSxHQUFaLFlBQVksQ0FBbUI7UUFBVSxvQkFBZSxHQUFmLGVBQWUsQ0FBZ0I7UUFBVSxXQUFNLEdBQU4sTUFBTSxDQUFRO0lBQ3BILENBQUM7SUFFRCx1Q0FBVyxHQUFYLFVBQVksS0FBNkIsRUFBRSxLQUEwQjtRQUFyRSxpQkF5QkM7UUF2QkcsTUFBTSxDQUFDLGlCQUFVLENBQUMsUUFBUSxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFO1lBQy9CLElBQUksQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLEVBQUU7U0FDdkMsQ0FBQzthQUNHLE9BQU8sQ0FBQyxVQUFBLEdBQUc7WUFDUixJQUFJLFVBQVUsR0FBWSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDakMsSUFBSSxhQUFhLEdBQVksR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRXBDLEVBQUUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7Z0JBS1QsTUFBTSxDQUFDLEtBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1lBRXZDLENBQUM7WUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztnQkFDdkIsTUFBTSxDQUFDLEtBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1lBQ25DLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDSixNQUFNLENBQUMsS0FBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7WUFDbkMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFBLEdBQUc7WUFDUixNQUFNLENBQUMsS0FBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDbkMsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRU8sNENBQWdCLEdBQXhCO1FBQ0ksSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUM3QixNQUFNLENBQUMsaUJBQVUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDL0IsQ0FBQztJQUVPLDRDQUFnQixHQUF4QjtRQUNJLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDN0IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLEVBQUUsRUFBRSxrREFBa0QsQ0FBQyxDQUFDO1FBQzFGLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDMUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1FBQ2hDLE1BQU0sQ0FBQyxpQkFBVSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNoQyxDQUFDO0lBNUNMO1FBQUMsaUJBQVUsRUFBRTs7eUJBQUE7SUE2Q2Isd0JBQUM7QUFBRCxDQTVDQSxBQTRDQyxJQUFBO0FBNUNZLHlCQUFpQixvQkE0QzdCLENBQUEiLCJmaWxlIjoiYXBwL3NoYXJlZC9yb2xlc0d1YXJkL3JvbGVzR3VhcmQuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7XG4gICAgQ2FuQWN0aXZhdGUsIFJvdXRlcixcbiAgICBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LFxuICAgIFJvdXRlclN0YXRlU25hcHNob3Rcbn0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IFRvYXN0ZXJTZXJ2aWNlIH0gZnJvbSAnYW5ndWxhcjItdG9hc3Rlcic7XG5pbXBvcnQgeyBDZW50cmVVc2VyU2VydmljZSB9IGZyb20gJy4uLy4uL2NvcmUvaW5kZXgnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgUm9sZXNHdWFyZFNlcnZpY2UgaW1wbGVtZW50cyBDYW5BY3RpdmF0ZSB7XG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF91c2VyU2VydmljZTogQ2VudHJlVXNlclNlcnZpY2UsIHByaXZhdGUgX3RvYXN0ZXJTZXJ2aWNlOiBUb2FzdGVyU2VydmljZSwgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcikge1xuICAgIH1cblxuICAgIGNhbkFjdGl2YXRlKHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBzdGF0ZTogUm91dGVyU3RhdGVTbmFwc2hvdCk6IE9ic2VydmFibGU8Ym9vbGVhbj4ge1xuXG4gICAgICAgIHJldHVybiBPYnNlcnZhYmxlLmZvcmtKb2luKFtcbiAgICAgICAgICAgIHRoaXMuX3VzZXJTZXJ2aWNlLklzU3lzQWRtaW4kKCksXG4gICAgICAgICAgICB0aGlzLl91c2VyU2VydmljZS5Jc0NlbnRyZU1hbmFnZXIkKClcbiAgICAgICAgXSlcbiAgICAgICAgICAgIC5mbGF0TWFwKHJlcyA9PiB7XG4gICAgICAgICAgICAgICAgbGV0IGlzU3lzQWRtaW46IGJvb2xlYW4gPSByZXNbMF07XG4gICAgICAgICAgICAgICAgbGV0IGlzQ2VudHJlQWRtaW46IGJvb2xlYW4gPSByZXNbMV07XG5cbiAgICAgICAgICAgICAgICBpZiAoaXNTeXNBZG1pbikge1xuICAgICAgICAgICAgICAgICAgICAvLyBpZiAoc3RhdGUudXJsICE9PSAnL2NlbnRyZS1wb3J0YWwvYWRtaW4tZGFzaGJvYXJkJykge1xuICAgICAgICAgICAgICAgICAgICAvLyAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWydjZW50cmUtcG9ydGFsL2FkbWluLWRhc2hib2FyZCddKTtcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgIHJldHVybiBPYnNlcnZhYmxlLm9mKGZhbHNlKTtcbiAgICAgICAgICAgICAgICAgICAgLy8gfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmFsbG93UmVkaXJlY3Rpb24oKTtcbiAgICAgICAgICAgICAgICAgICAgLy8gfVxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoaXNDZW50cmVBZG1pbikge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5hbGxvd1JlZGlyZWN0aW9uKCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMucmVkaXJlY3RUb0xvZ2luJCgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pLmNhdGNoKGVyciA9PiB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMucmVkaXJlY3RUb0xvZ2luJCgpO1xuICAgICAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBhbGxvd1JlZGlyZWN0aW9uKCk6IE9ic2VydmFibGU8Ym9vbGVhbj4ge1xuICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcigpO1xuICAgICAgICByZXR1cm4gT2JzZXJ2YWJsZS5vZih0cnVlKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIHJlZGlyZWN0VG9Mb2dpbiQoKTogT2JzZXJ2YWJsZTxib29sZWFuPiB7XG4gICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLmNsZWFyKCk7XG4gICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLnBvcCgnZXJyb3InLCAnJywgJ05vdCBhdXRob3Jpc2VkIHRvIGFjY2VzcyB0aGUgQ2VudHJlIEFkbWluIFBvcnRhbCcpO1xuICAgICAgICB0aGlzLl91c2VyU2VydmljZS5DbGVhcigpO1xuICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJ2xvZ2luJ10pO1xuICAgICAgICByZXR1cm4gT2JzZXJ2YWJsZS5vZihmYWxzZSk7XG4gICAgfVxufVxuIl19
