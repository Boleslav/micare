"use strict";
var angular2_toaster_1 = require('angular2-toaster');
var index_1 = require('../index');
var SpinningToast = (function () {
    function SpinningToast(message) {
        this.type = 'info';
        this.title = 'Saving';
        this.timeout = 0;
        this.body = index_1.SpinnerComponent;
        this.bodyOutputType = angular2_toaster_1.BodyOutputType.Component;
        if (message !== undefined)
            this.title = message;
    }
    return SpinningToast;
}());
exports.SpinningToast = SpinningToast;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zaGFyZWQvc3Bpbm5pbmctdG9hc3Qvc3Bpbm5pbmctdG9hc3QubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLGlDQUFzQyxrQkFBa0IsQ0FBQyxDQUFBO0FBQ3pELHNCQUFpQyxVQUFVLENBQUMsQ0FBQTtBQUc1QztJQU9JLHVCQUFZLE9BQWdCO1FBTnJCLFNBQUksR0FBVyxNQUFNLENBQUM7UUFDdEIsVUFBSyxHQUFXLFFBQVEsQ0FBQztRQUN6QixZQUFPLEdBQVcsQ0FBQyxDQUFDO1FBQ3BCLFNBQUksR0FBUSx3QkFBZ0IsQ0FBQztRQUM3QixtQkFBYyxHQUFtQixpQ0FBYyxDQUFDLFNBQVMsQ0FBQztRQUc3RCxFQUFFLENBQUMsQ0FBQyxPQUFPLEtBQUssU0FBUyxDQUFDO1lBQ3RCLElBQUksQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDO0lBQzdCLENBQUM7SUFDTCxvQkFBQztBQUFELENBWEEsQUFXQyxJQUFBO0FBWFkscUJBQWEsZ0JBV3pCLENBQUEiLCJmaWxlIjoiYXBwL3NoYXJlZC9zcGlubmluZy10b2FzdC9zcGlubmluZy10b2FzdC5tb2RlbC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFRvYXN0LCBCb2R5T3V0cHV0VHlwZSB9IGZyb20gJ2FuZ3VsYXIyLXRvYXN0ZXInO1xyXG5pbXBvcnQgeyBTcGlubmVyQ29tcG9uZW50IH0gZnJvbSAnLi4vaW5kZXgnO1xyXG5cclxuXHJcbmV4cG9ydCBjbGFzcyBTcGlubmluZ1RvYXN0IGltcGxlbWVudHMgVG9hc3Qge1xyXG4gICAgcHVibGljIHR5cGU6IHN0cmluZyA9ICdpbmZvJztcclxuICAgIHB1YmxpYyB0aXRsZTogc3RyaW5nID0gJ1NhdmluZyc7XHJcbiAgICBwdWJsaWMgdGltZW91dDogbnVtYmVyID0gMDtcclxuICAgIHB1YmxpYyBib2R5OiBhbnkgPSBTcGlubmVyQ29tcG9uZW50O1xyXG4gICAgcHVibGljIGJvZHlPdXRwdXRUeXBlOiBCb2R5T3V0cHV0VHlwZSA9IEJvZHlPdXRwdXRUeXBlLkNvbXBvbmVudDtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihtZXNzYWdlPzogc3RyaW5nKSB7XHJcbiAgICAgICAgaWYgKG1lc3NhZ2UgIT09IHVuZGVmaW5lZClcclxuICAgICAgICAgICAgdGhpcy50aXRsZSA9IG1lc3NhZ2U7XHJcbiAgICB9XHJcbn1cclxuIl19
