"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var cell_1 = require('../../../lib/data-set/cell');
var DefaultEditor = (function () {
    function DefaultEditor() {
        this.onStopEditing = new core_1.EventEmitter();
        this.onEdited = new core_1.EventEmitter();
        this.onClick = new core_1.EventEmitter();
    }
    __decorate([
        core_1.Input(), 
        __metadata('design:type', cell_1.Cell)
    ], DefaultEditor.prototype, "cell", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], DefaultEditor.prototype, "inputClass", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], DefaultEditor.prototype, "onStopEditing", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], DefaultEditor.prototype, "onEdited", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], DefaultEditor.prototype, "onClick", void 0);
    return DefaultEditor;
}());
exports.DefaultEditor = DefaultEditor;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zaGFyZWQvbmcyLXNtYXJ0LXRhYmxlL25nMi1zbWFydC10YWJsZS9jb21wb25lbnRzL2NlbGwvY2VsbC1lZGl0b3JzL2RlZmF1bHQtZWRpdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBNEMsZUFBZSxDQUFDLENBQUE7QUFFNUQscUJBQXFCLDRCQUE0QixDQUFDLENBQUE7QUFFbEQ7SUFBQTtRQUlZLGtCQUFhLEdBQUcsSUFBSSxtQkFBWSxFQUFPLENBQUM7UUFDeEMsYUFBUSxHQUFHLElBQUksbUJBQVksRUFBTyxDQUFDO1FBQ25DLFlBQU8sR0FBRyxJQUFJLG1CQUFZLEVBQU8sQ0FBQztJQUM5QyxDQUFDO0lBTkM7UUFBQyxZQUFLLEVBQUU7OytDQUFBO0lBQ1I7UUFBQyxZQUFLLEVBQUU7O3FEQUFBO0lBRVI7UUFBQyxhQUFNLEVBQUU7O3dEQUFBO0lBQ1Q7UUFBQyxhQUFNLEVBQUU7O21EQUFBO0lBQ1Q7UUFBQyxhQUFNLEVBQUU7O2tEQUFBO0lBQ1gsb0JBQUM7QUFBRCxDQVBBLEFBT0MsSUFBQTtBQVBZLHFCQUFhLGdCQU96QixDQUFBIiwiZmlsZSI6ImFwcC9zaGFyZWQvbmcyLXNtYXJ0LXRhYmxlL25nMi1zbWFydC10YWJsZS9jb21wb25lbnRzL2NlbGwvY2VsbC1lZGl0b3JzL2RlZmF1bHQtZWRpdG9yLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBDZWxsIH0gZnJvbSAnLi4vLi4vLi4vbGliL2RhdGEtc2V0L2NlbGwnO1xyXG5cclxuZXhwb3J0IGNsYXNzIERlZmF1bHRFZGl0b3IgaW1wbGVtZW50cyBFZGl0b3Ige1xyXG4gIEBJbnB1dCgpIGNlbGw6IENlbGw7XHJcbiAgQElucHV0KCkgaW5wdXRDbGFzczogc3RyaW5nO1xyXG5cclxuICBAT3V0cHV0KCkgb25TdG9wRWRpdGluZyA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIEBPdXRwdXQoKSBvbkVkaXRlZCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIEBPdXRwdXQoKSBvbkNsaWNrID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbn1cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgRWRpdG9yIHtcclxuICBjZWxsOiBDZWxsO1xyXG4gIGlucHV0Q2xhc3M6IHN0cmluZztcclxuICBvblN0b3BFZGl0aW5nOiBFdmVudEVtaXR0ZXI8YW55PjtcclxuICBvbkVkaXRlZDogRXZlbnRFbWl0dGVyPGFueT47XHJcbiAgb25DbGljazogRXZlbnRFbWl0dGVyPGFueT47XHJcbn1cclxuIl19
