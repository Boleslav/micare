"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var grid_1 = require('../../../lib/grid');
var row_1 = require('../../../lib/data-set/row');
var TbodyEditDeleteComponent = (function () {
    function TbodyEditDeleteComponent() {
        this.edit = new core_1.EventEmitter();
        this.delete = new core_1.EventEmitter();
        this.editRowSelect = new core_1.EventEmitter();
    }
    TbodyEditDeleteComponent.prototype.onEdit = function (event) {
        event.preventDefault();
        event.stopPropagation();
        this.editRowSelect.emit(this.row);
        if (this.grid.getSetting('mode') === 'external') {
            this.edit.emit({
                data: this.row.getData(),
                source: this.source
            });
        }
        else {
            this.grid.edit(this.row);
        }
    };
    TbodyEditDeleteComponent.prototype.onDelete = function (event) {
        event.preventDefault();
        event.stopPropagation();
        if (this.grid.getSetting('mode') === 'external') {
            this.delete.emit({
                data: this.row.getData(),
                source: this.source
            });
        }
        else {
            this.grid.delete(this.row, this.deleteConfirm);
        }
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', grid_1.Grid)
    ], TbodyEditDeleteComponent.prototype, "grid", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', row_1.Row)
    ], TbodyEditDeleteComponent.prototype, "row", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], TbodyEditDeleteComponent.prototype, "source", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', core_1.EventEmitter)
    ], TbodyEditDeleteComponent.prototype, "deleteConfirm", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', core_1.EventEmitter)
    ], TbodyEditDeleteComponent.prototype, "editConfirm", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], TbodyEditDeleteComponent.prototype, "edit", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], TbodyEditDeleteComponent.prototype, "delete", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], TbodyEditDeleteComponent.prototype, "editRowSelect", void 0);
    TbodyEditDeleteComponent = __decorate([
        core_1.Component({
            selector: 'ng2-st-tbody-edit-delete',
            template: "\n    <a href=\"#\" *ngIf=\"grid.getSetting('actions.edit')\" class=\"ng2-smart-action ng2-smart-action-edit-edit btn btn-sm btn-info btn-block\"\n        [innerHTML]=\"grid.getSetting('edit.editButtonContent')\" (click)=\"onEdit($event)\"></a>\n    <a href=\"#\" *ngIf=\"grid.getSetting('actions.delete')\" \n        class=\"ng2-smart-action ng2-smart-action-delete-delete btn-sm btn btn-danger btn-block\"\n        [innerHTML]=\"grid.getSetting('delete.deleteButtonContent')\" (click)=\"onDelete($event)\"></a>\n  "
        }), 
        __metadata('design:paramtypes', [])
    ], TbodyEditDeleteComponent);
    return TbodyEditDeleteComponent;
}());
exports.TbodyEditDeleteComponent = TbodyEditDeleteComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zaGFyZWQvbmcyLXNtYXJ0LXRhYmxlL25nMi1zbWFydC10YWJsZS9jb21wb25lbnRzL3Rib2R5L2NlbGxzL2VkaXQtZGVsZXRlLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQXVELGVBQWUsQ0FBQyxDQUFBO0FBRXZFLHFCQUFxQixtQkFBbUIsQ0FBQyxDQUFBO0FBQ3pDLG9CQUFvQiwyQkFBMkIsQ0FBQyxDQUFBO0FBWWhEO0lBQUE7UUFRWSxTQUFJLEdBQUcsSUFBSSxtQkFBWSxFQUFPLENBQUM7UUFDL0IsV0FBTSxHQUFHLElBQUksbUJBQVksRUFBTyxDQUFDO1FBQ2pDLGtCQUFhLEdBQUcsSUFBSSxtQkFBWSxFQUFPLENBQUM7SUErQnBELENBQUM7SUE3QkMseUNBQU0sR0FBTixVQUFPLEtBQVU7UUFDZixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdkIsS0FBSyxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBRXhCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUVsQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsS0FBSyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQ2hELElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO2dCQUNiLElBQUksRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRTtnQkFDeEIsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNO2FBQ3BCLENBQUMsQ0FBQztRQUNMLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMzQixDQUFDO0lBQ0gsQ0FBQztJQUVELDJDQUFRLEdBQVIsVUFBUyxLQUFVO1FBQ2pCLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN2QixLQUFLLENBQUMsZUFBZSxFQUFFLENBQUM7UUFFeEIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLEtBQUssVUFBVSxDQUFDLENBQUMsQ0FBQztZQUNoRCxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztnQkFDZixJQUFJLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUU7Z0JBQ3hCLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTTthQUNwQixDQUFDLENBQUM7UUFDTCxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNqRCxDQUFDO0lBQ0gsQ0FBQztJQXRDRDtRQUFDLFlBQUssRUFBRTs7MERBQUE7SUFDUjtRQUFDLFlBQUssRUFBRTs7eURBQUE7SUFDUjtRQUFDLFlBQUssRUFBRTs7NERBQUE7SUFDUjtRQUFDLFlBQUssRUFBRTs7bUVBQUE7SUFDUjtRQUFDLFlBQUssRUFBRTs7aUVBQUE7SUFFUjtRQUFDLGFBQU0sRUFBRTs7MERBQUE7SUFDVDtRQUFDLGFBQU0sRUFBRTs7NERBQUE7SUFDVDtRQUFDLGFBQU0sRUFBRTs7bUVBQUE7SUFwQlg7UUFBQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLDBCQUEwQjtZQUNwQyxRQUFRLEVBQUUsc2dCQU1UO1NBQ0YsQ0FBQzs7Z0NBQUE7SUEwQ0YsK0JBQUM7QUFBRCxDQXpDQSxBQXlDQyxJQUFBO0FBekNZLGdDQUF3QiwyQkF5Q3BDLENBQUEiLCJmaWxlIjoiYXBwL3NoYXJlZC9uZzItc21hcnQtdGFibGUvbmcyLXNtYXJ0LXRhYmxlL2NvbXBvbmVudHMvdGJvZHkvY2VsbHMvZWRpdC1kZWxldGUuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmltcG9ydCB7IEdyaWQgfSBmcm9tICcuLi8uLi8uLi9saWIvZ3JpZCc7XHJcbmltcG9ydCB7IFJvdyB9IGZyb20gJy4uLy4uLy4uL2xpYi9kYXRhLXNldC9yb3cnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICduZzItc3QtdGJvZHktZWRpdC1kZWxldGUnLFxyXG4gIHRlbXBsYXRlOiBgXHJcbiAgICA8YSBocmVmPVwiI1wiICpuZ0lmPVwiZ3JpZC5nZXRTZXR0aW5nKCdhY3Rpb25zLmVkaXQnKVwiIGNsYXNzPVwibmcyLXNtYXJ0LWFjdGlvbiBuZzItc21hcnQtYWN0aW9uLWVkaXQtZWRpdCBidG4gYnRuLXNtIGJ0bi1pbmZvIGJ0bi1ibG9ja1wiXHJcbiAgICAgICAgW2lubmVySFRNTF09XCJncmlkLmdldFNldHRpbmcoJ2VkaXQuZWRpdEJ1dHRvbkNvbnRlbnQnKVwiIChjbGljayk9XCJvbkVkaXQoJGV2ZW50KVwiPjwvYT5cclxuICAgIDxhIGhyZWY9XCIjXCIgKm5nSWY9XCJncmlkLmdldFNldHRpbmcoJ2FjdGlvbnMuZGVsZXRlJylcIiBcclxuICAgICAgICBjbGFzcz1cIm5nMi1zbWFydC1hY3Rpb24gbmcyLXNtYXJ0LWFjdGlvbi1kZWxldGUtZGVsZXRlIGJ0bi1zbSBidG4gYnRuLWRhbmdlciBidG4tYmxvY2tcIlxyXG4gICAgICAgIFtpbm5lckhUTUxdPVwiZ3JpZC5nZXRTZXR0aW5nKCdkZWxldGUuZGVsZXRlQnV0dG9uQ29udGVudCcpXCIgKGNsaWNrKT1cIm9uRGVsZXRlKCRldmVudClcIj48L2E+XHJcbiAgYFxyXG59KVxyXG5leHBvcnQgY2xhc3MgVGJvZHlFZGl0RGVsZXRlQ29tcG9uZW50IHtcclxuXHJcbiAgQElucHV0KCkgZ3JpZDogR3JpZDtcclxuICBASW5wdXQoKSByb3c6IFJvdztcclxuICBASW5wdXQoKSBzb3VyY2U6IGFueTtcclxuICBASW5wdXQoKSBkZWxldGVDb25maXJtOiBFdmVudEVtaXR0ZXI8YW55PjtcclxuICBASW5wdXQoKSBlZGl0Q29uZmlybTogRXZlbnRFbWl0dGVyPGFueT47XHJcblxyXG4gIEBPdXRwdXQoKSBlZGl0ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgQE91dHB1dCgpIGRlbGV0ZSA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIEBPdXRwdXQoKSBlZGl0Um93U2VsZWN0ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcblxyXG4gIG9uRWRpdChldmVudDogYW55KSB7XHJcbiAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XHJcblxyXG4gICAgdGhpcy5lZGl0Um93U2VsZWN0LmVtaXQodGhpcy5yb3cpO1xyXG5cclxuICAgIGlmICh0aGlzLmdyaWQuZ2V0U2V0dGluZygnbW9kZScpID09PSAnZXh0ZXJuYWwnKSB7XHJcbiAgICAgIHRoaXMuZWRpdC5lbWl0KHtcclxuICAgICAgICBkYXRhOiB0aGlzLnJvdy5nZXREYXRhKCksXHJcbiAgICAgICAgc291cmNlOiB0aGlzLnNvdXJjZVxyXG4gICAgICB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuZ3JpZC5lZGl0KHRoaXMucm93KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG9uRGVsZXRlKGV2ZW50OiBhbnkpIHtcclxuICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcclxuXHJcbiAgICBpZiAodGhpcy5ncmlkLmdldFNldHRpbmcoJ21vZGUnKSA9PT0gJ2V4dGVybmFsJykge1xyXG4gICAgICB0aGlzLmRlbGV0ZS5lbWl0KHtcclxuICAgICAgICBkYXRhOiB0aGlzLnJvdy5nZXREYXRhKCksXHJcbiAgICAgICAgc291cmNlOiB0aGlzLnNvdXJjZVxyXG4gICAgICB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuZ3JpZC5kZWxldGUodGhpcy5yb3csIHRoaXMuZGVsZXRlQ29uZmlybSk7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ==
