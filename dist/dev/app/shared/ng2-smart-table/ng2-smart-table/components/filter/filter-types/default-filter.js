"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var column_1 = require('../../../lib/data-set/column');
var DefaultFilter = (function () {
    function DefaultFilter() {
        this.delay = 300;
        this.filter = new core_1.EventEmitter();
    }
    DefaultFilter.prototype.ngOnDestroy = function () {
        if (this.changesSubscription)
            this.changesSubscription.unsubscribe();
    };
    DefaultFilter.prototype.setFilter = function () {
        this.filter.emit(this.query);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], DefaultFilter.prototype, "query", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], DefaultFilter.prototype, "inputClass", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', column_1.Column)
    ], DefaultFilter.prototype, "column", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], DefaultFilter.prototype, "filter", void 0);
    return DefaultFilter;
}());
exports.DefaultFilter = DefaultFilter;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zaGFyZWQvbmcyLXNtYXJ0LXRhYmxlL25nMi1zbWFydC10YWJsZS9jb21wb25lbnRzL2ZpbHRlci9maWx0ZXItdHlwZXMvZGVmYXVsdC1maWx0ZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUF1RCxlQUFlLENBQUMsQ0FBQTtBQUd2RSx1QkFBdUIsOEJBQThCLENBQUMsQ0FBQTtBQUV0RDtJQUFBO1FBRUUsVUFBSyxHQUFXLEdBQUcsQ0FBQztRQUtWLFdBQU0sR0FBRyxJQUFJLG1CQUFZLEVBQVUsQ0FBQztJQVVoRCxDQUFDO0lBUkMsbUNBQVcsR0FBWDtRQUNFLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztZQUMzQixJQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDM0MsQ0FBQztJQUVELGlDQUFTLEdBQVQ7UUFDRSxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDL0IsQ0FBQztJQVpEO1FBQUMsWUFBSyxFQUFFOztnREFBQTtJQUNSO1FBQUMsWUFBSyxFQUFFOztxREFBQTtJQUNSO1FBQUMsWUFBSyxFQUFFOztpREFBQTtJQUNSO1FBQUMsYUFBTSxFQUFFOztpREFBQTtJQVVYLG9CQUFDO0FBQUQsQ0FqQkEsQUFpQkMsSUFBQTtBQWpCWSxxQkFBYSxnQkFpQnpCLENBQUEiLCJmaWxlIjoiYXBwL3NoYXJlZC9uZzItc21hcnQtdGFibGUvbmcyLXNtYXJ0LXRhYmxlL2NvbXBvbmVudHMvZmlsdGVyL2ZpbHRlci10eXBlcy9kZWZhdWx0LWZpbHRlci5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciwgT25EZXN0cm95IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMvU3Vic2NyaXB0aW9uJztcclxuXHJcbmltcG9ydCB7IENvbHVtbiB9IGZyb20gJy4uLy4uLy4uL2xpYi9kYXRhLXNldC9jb2x1bW4nO1xyXG5cclxuZXhwb3J0IGNsYXNzIERlZmF1bHRGaWx0ZXIgaW1wbGVtZW50cyBGaWx0ZXIsIE9uRGVzdHJveSB7XHJcblxyXG4gIGRlbGF5OiBudW1iZXIgPSAzMDA7XHJcbiAgY2hhbmdlc1N1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xyXG4gIEBJbnB1dCgpIHF1ZXJ5OiBzdHJpbmc7XHJcbiAgQElucHV0KCkgaW5wdXRDbGFzczogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIGNvbHVtbjogQ29sdW1uO1xyXG4gIEBPdXRwdXQoKSBmaWx0ZXIgPSBuZXcgRXZlbnRFbWl0dGVyPHN0cmluZz4oKTtcclxuXHJcbiAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICBpZiAodGhpcy5jaGFuZ2VzU3Vic2NyaXB0aW9uKVxyXG4gICAgICB0aGlzLmNoYW5nZXNTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICB9XHJcblxyXG4gIHNldEZpbHRlcigpIHtcclxuICAgIHRoaXMuZmlsdGVyLmVtaXQodGhpcy5xdWVyeSk7XHJcbiAgfVxyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIEZpbHRlciB7XHJcblxyXG4gIGRlbGF5PzogbnVtYmVyO1xyXG4gIGNoYW5nZXNTdWJzY3JpcHRpb24/OiBTdWJzY3JpcHRpb247XHJcbiAgcXVlcnk6IHN0cmluZztcclxuICBpbnB1dENsYXNzOiBzdHJpbmc7XHJcbiAgY29sdW1uOiBDb2x1bW47XHJcbiAgZmlsdGVyOiBFdmVudEVtaXR0ZXI8c3RyaW5nPjtcclxufVxyXG4iXX0=
