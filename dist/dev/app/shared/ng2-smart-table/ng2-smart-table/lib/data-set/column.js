"use strict";
var Column = (function () {
    function Column(id, settings, dataSet) {
        this.id = id;
        this.settings = settings;
        this.dataSet = dataSet;
        this.title = '';
        this.type = '';
        this.class = '';
        this.isSortable = false;
        this.isEditable = true;
        this.isFilterable = false;
        this.sortDirection = '';
        this.defaultSortDirection = '';
        this.editor = { type: '', config: {}, component: null };
        this.filter = { type: '', config: {} };
        this.renderComponent = null;
        this.process();
    }
    Column.prototype.getCompareFunction = function () {
        return this.compareFunction;
    };
    Column.prototype.getValuePrepareFunction = function () {
        return this.valuePrepareFunction;
    };
    Column.prototype.getFilterFunction = function () {
        return this.filterFunction;
    };
    Column.prototype.getConfig = function () {
        return this.editor.config;
    };
    Column.prototype.getFilterType = function () {
        return this.filter && this.filter.type;
    };
    Column.prototype.getFilterConfig = function () {
        return this.filter && this.filter.config;
    };
    Column.prototype.process = function () {
        this.title = this.settings['title'];
        this.class = this.settings['class'];
        this.type = this.prepareType();
        this.editor = this.settings['editor'];
        this.filter = this.settings['filter'];
        this.renderComponent = this.settings['renderComponent'];
        this.isFilterable = typeof this.settings['filter'] === 'undefined' ? true : !!this.settings['filter'];
        this.defaultSortDirection = ['asc', 'desc'].indexOf(this.settings['sortDirection']) !== -1 ? this.settings['sortDirection'] : '';
        this.isSortable = typeof this.settings['sort'] === 'undefined' ? true : !!this.settings['sort'];
        this.isEditable = typeof this.settings['editable'] === 'undefined' ? true : !!this.settings['editable'];
        this.sortDirection = this.prepareSortDirection();
        this.compareFunction = this.settings['compareFunction'];
        this.valuePrepareFunction = this.settings['valuePrepareFunction'];
        this.filterFunction = this.settings['filterFunction'];
    };
    Column.prototype.prepareType = function () {
        return this.settings['type'] || this.determineType();
    };
    Column.prototype.prepareSortDirection = function () {
        return this.settings['sort'] === 'desc' ? 'desc' : 'asc';
    };
    Column.prototype.determineType = function () {
        return 'text';
    };
    return Column;
}());
exports.Column = Column;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zaGFyZWQvbmcyLXNtYXJ0LXRhYmxlL25nMi1zbWFydC10YWJsZS9saWIvZGF0YS1zZXQvY29sdW1uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFFQTtJQWlCRSxnQkFBbUIsRUFBVSxFQUFZLFFBQWEsRUFBWSxPQUFnQjtRQUEvRCxPQUFFLEdBQUYsRUFBRSxDQUFRO1FBQVksYUFBUSxHQUFSLFFBQVEsQ0FBSztRQUFZLFlBQU8sR0FBUCxPQUFPLENBQVM7UUFmM0UsVUFBSyxHQUFXLEVBQUUsQ0FBQztRQUNuQixTQUFJLEdBQVcsRUFBRSxDQUFDO1FBQ2xCLFVBQUssR0FBVyxFQUFFLENBQUM7UUFDbkIsZUFBVSxHQUFZLEtBQUssQ0FBQztRQUM1QixlQUFVLEdBQVksSUFBSSxDQUFDO1FBQzNCLGlCQUFZLEdBQVksS0FBSyxDQUFDO1FBQzlCLGtCQUFhLEdBQVcsRUFBRSxDQUFDO1FBQzNCLHlCQUFvQixHQUFXLEVBQUUsQ0FBQztRQUNsQyxXQUFNLEdBQWtELEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxNQUFNLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsQ0FBQztRQUNsRyxXQUFNLEdBQWtDLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxNQUFNLEVBQUUsRUFBRSxFQUFFLENBQUM7UUFDakUsb0JBQWUsR0FBUSxJQUFJLENBQUM7UUFNakMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ2pCLENBQUM7SUFFTSxtQ0FBa0IsR0FBekI7UUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQztJQUM5QixDQUFDO0lBRU0sd0NBQXVCLEdBQTlCO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQztJQUNuQyxDQUFDO0lBRU0sa0NBQWlCLEdBQXhCO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUM7SUFDN0IsQ0FBQztJQUVNLDBCQUFTLEdBQWhCO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDO0lBQzVCLENBQUM7SUFFRCw4QkFBYSxHQUFiO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDekMsQ0FBQztJQUVELGdDQUFlLEdBQWY7UUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQztJQUMzQyxDQUFDO0lBRVMsd0JBQU8sR0FBakI7UUFDRSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDcEMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQy9CLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN0QyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLENBQUM7UUFFeEQsSUFBSSxDQUFDLFlBQVksR0FBRyxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssV0FBVyxHQUFHLElBQUksR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN0RyxJQUFJLENBQUMsb0JBQW9CLEdBQUcsQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUNqSSxJQUFJLENBQUMsVUFBVSxHQUFHLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxXQUFXLEdBQUcsSUFBSSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ2hHLElBQUksQ0FBQyxVQUFVLEdBQUcsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxLQUFLLFdBQVcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDeEcsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztRQUVqRCxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUN4RCxJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1FBQ2xFLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO0lBQ3hELENBQUM7SUFFRCw0QkFBVyxHQUFYO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3ZELENBQUM7SUFFRCxxQ0FBb0IsR0FBcEI7UUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxNQUFNLEdBQUcsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUMzRCxDQUFDO0lBRUQsOEJBQWEsR0FBYjtRQUVFLE1BQU0sQ0FBQyxNQUFNLENBQUM7SUFDaEIsQ0FBQztJQUNILGFBQUM7QUFBRCxDQTVFQSxBQTRFQyxJQUFBO0FBNUVZLGNBQU0sU0E0RWxCLENBQUEiLCJmaWxlIjoiYXBwL3NoYXJlZC9uZzItc21hcnQtdGFibGUvbmcyLXNtYXJ0LXRhYmxlL2xpYi9kYXRhLXNldC9jb2x1bW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEYXRhU2V0IH0gZnJvbSAnLi9kYXRhLXNldCc7XHJcblxyXG5leHBvcnQgY2xhc3MgQ29sdW1uIHtcclxuXHJcbiAgcHVibGljIHRpdGxlOiBzdHJpbmcgPSAnJztcclxuICBwdWJsaWMgdHlwZTogc3RyaW5nID0gJyc7XHJcbiAgcHVibGljIGNsYXNzOiBzdHJpbmcgPSAnJztcclxuICBwdWJsaWMgaXNTb3J0YWJsZTogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIHB1YmxpYyBpc0VkaXRhYmxlOiBib29sZWFuID0gdHJ1ZTtcclxuICBwdWJsaWMgaXNGaWx0ZXJhYmxlOiBib29sZWFuID0gZmFsc2U7XHJcbiAgcHVibGljIHNvcnREaXJlY3Rpb246IHN0cmluZyA9ICcnO1xyXG4gIHB1YmxpYyBkZWZhdWx0U29ydERpcmVjdGlvbjogc3RyaW5nID0gJyc7XHJcbiAgcHVibGljIGVkaXRvcjogeyB0eXBlOiBzdHJpbmcsIGNvbmZpZzogYW55LCBjb21wb25lbnQ6IGFueSB9ID0geyB0eXBlOiAnJywgY29uZmlnOiB7fSwgY29tcG9uZW50OiBudWxsIH07XHJcbiAgcHVibGljIGZpbHRlcjogeyB0eXBlOiBzdHJpbmcsIGNvbmZpZzogYW55IH0gPSB7IHR5cGU6ICcnLCBjb25maWc6IHt9IH07XHJcbiAgcHVibGljIHJlbmRlckNvbXBvbmVudDogYW55ID0gbnVsbDtcclxuICBjb21wYXJlRnVuY3Rpb246IEZ1bmN0aW9uO1xyXG4gIHZhbHVlUHJlcGFyZUZ1bmN0aW9uOiBGdW5jdGlvbjtcclxuICBmaWx0ZXJGdW5jdGlvbjogRnVuY3Rpb247XHJcblxyXG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBpZDogc3RyaW5nLCBwcm90ZWN0ZWQgc2V0dGluZ3M6IGFueSwgcHJvdGVjdGVkIGRhdGFTZXQ6IERhdGFTZXQpIHtcclxuICAgIHRoaXMucHJvY2VzcygpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGdldENvbXBhcmVGdW5jdGlvbigpOiBGdW5jdGlvbiB7XHJcbiAgICByZXR1cm4gdGhpcy5jb21wYXJlRnVuY3Rpb247XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZ2V0VmFsdWVQcmVwYXJlRnVuY3Rpb24oKTogRnVuY3Rpb24ge1xyXG4gICAgcmV0dXJuIHRoaXMudmFsdWVQcmVwYXJlRnVuY3Rpb247XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZ2V0RmlsdGVyRnVuY3Rpb24oKTogRnVuY3Rpb24ge1xyXG4gICAgcmV0dXJuIHRoaXMuZmlsdGVyRnVuY3Rpb247XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZ2V0Q29uZmlnKCk6IGFueSB7XHJcbiAgICByZXR1cm4gdGhpcy5lZGl0b3IuY29uZmlnO1xyXG4gIH1cclxuXHJcbiAgZ2V0RmlsdGVyVHlwZSgpOiBhbnkge1xyXG4gICAgcmV0dXJuIHRoaXMuZmlsdGVyICYmIHRoaXMuZmlsdGVyLnR5cGU7XHJcbiAgfVxyXG5cclxuICBnZXRGaWx0ZXJDb25maWcoKTogYW55IHtcclxuICAgIHJldHVybiB0aGlzLmZpbHRlciAmJiB0aGlzLmZpbHRlci5jb25maWc7XHJcbiAgfVxyXG5cclxuICBwcm90ZWN0ZWQgcHJvY2VzcygpOiB2b2lkIHtcclxuICAgIHRoaXMudGl0bGUgPSB0aGlzLnNldHRpbmdzWyd0aXRsZSddO1xyXG4gICAgdGhpcy5jbGFzcyA9IHRoaXMuc2V0dGluZ3NbJ2NsYXNzJ107XHJcbiAgICB0aGlzLnR5cGUgPSB0aGlzLnByZXBhcmVUeXBlKCk7XHJcbiAgICB0aGlzLmVkaXRvciA9IHRoaXMuc2V0dGluZ3NbJ2VkaXRvciddO1xyXG4gICAgdGhpcy5maWx0ZXIgPSB0aGlzLnNldHRpbmdzWydmaWx0ZXInXTtcclxuICAgIHRoaXMucmVuZGVyQ29tcG9uZW50ID0gdGhpcy5zZXR0aW5nc1sncmVuZGVyQ29tcG9uZW50J107XHJcblxyXG4gICAgdGhpcy5pc0ZpbHRlcmFibGUgPSB0eXBlb2YgdGhpcy5zZXR0aW5nc1snZmlsdGVyJ10gPT09ICd1bmRlZmluZWQnID8gdHJ1ZSA6ICEhdGhpcy5zZXR0aW5nc1snZmlsdGVyJ107XHJcbiAgICB0aGlzLmRlZmF1bHRTb3J0RGlyZWN0aW9uID0gWydhc2MnLCAnZGVzYyddLmluZGV4T2YodGhpcy5zZXR0aW5nc1snc29ydERpcmVjdGlvbiddKSAhPT0gLTEgPyB0aGlzLnNldHRpbmdzWydzb3J0RGlyZWN0aW9uJ10gOiAnJztcclxuICAgIHRoaXMuaXNTb3J0YWJsZSA9IHR5cGVvZiB0aGlzLnNldHRpbmdzWydzb3J0J10gPT09ICd1bmRlZmluZWQnID8gdHJ1ZSA6ICEhdGhpcy5zZXR0aW5nc1snc29ydCddO1xyXG4gICAgdGhpcy5pc0VkaXRhYmxlID0gdHlwZW9mIHRoaXMuc2V0dGluZ3NbJ2VkaXRhYmxlJ10gPT09ICd1bmRlZmluZWQnID8gdHJ1ZSA6ICEhdGhpcy5zZXR0aW5nc1snZWRpdGFibGUnXTtcclxuICAgIHRoaXMuc29ydERpcmVjdGlvbiA9IHRoaXMucHJlcGFyZVNvcnREaXJlY3Rpb24oKTtcclxuXHJcbiAgICB0aGlzLmNvbXBhcmVGdW5jdGlvbiA9IHRoaXMuc2V0dGluZ3NbJ2NvbXBhcmVGdW5jdGlvbiddO1xyXG4gICAgdGhpcy52YWx1ZVByZXBhcmVGdW5jdGlvbiA9IHRoaXMuc2V0dGluZ3NbJ3ZhbHVlUHJlcGFyZUZ1bmN0aW9uJ107XHJcbiAgICB0aGlzLmZpbHRlckZ1bmN0aW9uID0gdGhpcy5zZXR0aW5nc1snZmlsdGVyRnVuY3Rpb24nXTtcclxuICB9XHJcblxyXG4gIHByZXBhcmVUeXBlKCk6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gdGhpcy5zZXR0aW5nc1sndHlwZSddIHx8IHRoaXMuZGV0ZXJtaW5lVHlwZSgpO1xyXG4gIH1cclxuXHJcbiAgcHJlcGFyZVNvcnREaXJlY3Rpb24oKTogc3RyaW5nIHtcclxuICAgIHJldHVybiB0aGlzLnNldHRpbmdzWydzb3J0J10gPT09ICdkZXNjJyA/ICdkZXNjJyA6ICdhc2MnO1xyXG4gIH1cclxuXHJcbiAgZGV0ZXJtaW5lVHlwZSgpOiBzdHJpbmcge1xyXG4gICAgLy8gVE9ETzogZGV0ZXJtaW5lIHR5cGUgYnkgZGF0YVxyXG4gICAgcmV0dXJuICd0ZXh0JztcclxuICB9XHJcbn1cclxuIl19
