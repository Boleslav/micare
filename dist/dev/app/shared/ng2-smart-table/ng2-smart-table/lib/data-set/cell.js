"use strict";
var Cell = (function () {
    function Cell(value, row, column, dataSet) {
        this.value = value;
        this.row = row;
        this.column = column;
        this.dataSet = dataSet;
        this.newValue = '';
        this.newValue = value;
    }
    Cell.prototype.getColumn = function () {
        return this.column;
    };
    Cell.prototype.getRow = function () {
        return this.row;
    };
    Cell.prototype.getValue = function () {
        var valid = this.column.getValuePrepareFunction() instanceof Function;
        var prepare = valid ? this.column.getValuePrepareFunction() : Cell.PREPARE;
        return prepare.call(null, this.value, this.row.getData());
    };
    Cell.prototype.setValue = function (value) {
        this.newValue = value;
    };
    Cell.prototype.getId = function () {
        return this.getColumn().id;
    };
    Cell.prototype.getTitle = function () {
        return this.getColumn().title;
    };
    Cell.prototype.isEditable = function () {
        return this.getColumn().isEditable;
    };
    Cell.PREPARE = function (value) { return value; };
    return Cell;
}());
exports.Cell = Cell;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zaGFyZWQvbmcyLXNtYXJ0LXRhYmxlL25nMi1zbWFydC10YWJsZS9saWIvZGF0YS1zZXQvY2VsbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBSUE7SUFLRSxjQUFzQixLQUFVLEVBQVksR0FBUSxFQUFZLE1BQVcsRUFBWSxPQUFnQjtRQUFqRixVQUFLLEdBQUwsS0FBSyxDQUFLO1FBQVksUUFBRyxHQUFILEdBQUcsQ0FBSztRQUFZLFdBQU0sR0FBTixNQUFNLENBQUs7UUFBWSxZQUFPLEdBQVAsT0FBTyxDQUFTO1FBSHZHLGFBQVEsR0FBUSxFQUFFLENBQUM7UUFJakIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7SUFDeEIsQ0FBQztJQUVELHdCQUFTLEdBQVQ7UUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUNyQixDQUFDO0lBRUQscUJBQU0sR0FBTjtRQUNFLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO0lBQ2xCLENBQUM7SUFFRCx1QkFBUSxHQUFSO1FBQ0UsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyx1QkFBdUIsRUFBRSxZQUFZLFFBQVEsQ0FBQztRQUN0RSxJQUFJLE9BQU8sR0FBRyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyx1QkFBdUIsRUFBRSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDM0UsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO0lBQzVELENBQUM7SUFFRCx1QkFBUSxHQUFSLFVBQVMsS0FBVTtRQUNqQixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztJQUN4QixDQUFDO0lBRUQsb0JBQUssR0FBTDtRQUNFLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUMsRUFBRSxDQUFDO0lBQzdCLENBQUM7SUFFRCx1QkFBUSxHQUFSO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxLQUFLLENBQUM7SUFDaEMsQ0FBQztJQUVELHlCQUFVLEdBQVY7UUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLFVBQVUsQ0FBQztJQUNyQyxDQUFDO0lBbENnQixZQUFPLEdBQUcsVUFBQyxLQUFVLElBQUssT0FBQSxLQUFLLEVBQUwsQ0FBSyxDQUFDO0lBbUNuRCxXQUFDO0FBQUQsQ0F0Q0EsQUFzQ0MsSUFBQTtBQXRDWSxZQUFJLE9Bc0NoQixDQUFBIiwiZmlsZSI6ImFwcC9zaGFyZWQvbmcyLXNtYXJ0LXRhYmxlL25nMi1zbWFydC10YWJsZS9saWIvZGF0YS1zZXQvY2VsbC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbHVtbiB9IGZyb20gJy4vY29sdW1uJztcclxuaW1wb3J0IHsgRGF0YVNldCB9IGZyb20gJy4vZGF0YS1zZXQnO1xyXG5pbXBvcnQgeyBSb3cgfSBmcm9tICcuL3Jvdyc7XHJcblxyXG5leHBvcnQgY2xhc3MgQ2VsbCB7XHJcblxyXG4gIG5ld1ZhbHVlOiBhbnkgPSAnJztcclxuICBwcm90ZWN0ZWQgc3RhdGljIFBSRVBBUkUgPSAodmFsdWU6IGFueSkgPT4gdmFsdWU7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByb3RlY3RlZCB2YWx1ZTogYW55LCBwcm90ZWN0ZWQgcm93OiBSb3csIHByb3RlY3RlZCBjb2x1bW46IGFueSwgcHJvdGVjdGVkIGRhdGFTZXQ6IERhdGFTZXQpIHtcclxuICAgIHRoaXMubmV3VmFsdWUgPSB2YWx1ZTtcclxuICB9XHJcblxyXG4gIGdldENvbHVtbigpOiBDb2x1bW4ge1xyXG4gICAgcmV0dXJuIHRoaXMuY29sdW1uO1xyXG4gIH1cclxuXHJcbiAgZ2V0Um93KCk6IFJvdyB7XHJcbiAgICByZXR1cm4gdGhpcy5yb3c7XHJcbiAgfVxyXG5cclxuICBnZXRWYWx1ZSgpOiBhbnkge1xyXG4gICAgbGV0IHZhbGlkID0gdGhpcy5jb2x1bW4uZ2V0VmFsdWVQcmVwYXJlRnVuY3Rpb24oKSBpbnN0YW5jZW9mIEZ1bmN0aW9uO1xyXG4gICAgbGV0IHByZXBhcmUgPSB2YWxpZCA/IHRoaXMuY29sdW1uLmdldFZhbHVlUHJlcGFyZUZ1bmN0aW9uKCkgOiBDZWxsLlBSRVBBUkU7XHJcbiAgICByZXR1cm4gcHJlcGFyZS5jYWxsKG51bGwsIHRoaXMudmFsdWUsIHRoaXMucm93LmdldERhdGEoKSk7XHJcbiAgfVxyXG5cclxuICBzZXRWYWx1ZSh2YWx1ZTogYW55KTogYW55IHtcclxuICAgIHRoaXMubmV3VmFsdWUgPSB2YWx1ZTtcclxuICB9XHJcblxyXG4gIGdldElkKCk6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gdGhpcy5nZXRDb2x1bW4oKS5pZDtcclxuICB9XHJcblxyXG4gIGdldFRpdGxlKCk6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gdGhpcy5nZXRDb2x1bW4oKS50aXRsZTtcclxuICB9XHJcblxyXG4gIGlzRWRpdGFibGUoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gdGhpcy5nZXRDb2x1bW4oKS5pc0VkaXRhYmxlO1xyXG4gIH1cclxufVxyXG4iXX0=
