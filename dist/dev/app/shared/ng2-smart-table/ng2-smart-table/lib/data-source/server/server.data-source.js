"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var local_data_source_1 = require('../local/local.data-source');
var url_search_params_1 = require('@angular/http/src/url_search_params');
var server_source_conf_1 = require('./server-source.conf');
var helpers_1 = require('../../helpers');
var ServerDataSource = (function (_super) {
    __extends(ServerDataSource, _super);
    function ServerDataSource(http, conf) {
        if (conf === void 0) { conf = {}; }
        _super.call(this);
        this.http = http;
        this.lastRequestCount = 0;
        this.conf = new server_source_conf_1.ServerSourceConf(conf);
        if (!this.conf.endPoint) {
            throw new Error('At least endPoint must be specified as a configuration of the server data source.');
        }
    }
    ServerDataSource.prototype.count = function () {
        return this.lastRequestCount;
    };
    ServerDataSource.prototype.getElements = function () {
        var _this = this;
        return this.requestElements().map(function (res) {
            _this.lastRequestCount = _this.extractTotalFromResponse(res);
            _this.data = _this.extractDataFromResponse(res);
            return _this.data;
        }).toPromise();
    };
    ServerDataSource.prototype.extractDataFromResponse = function (res) {
        var rawData = res.json();
        var data = !!this.conf.dataKey ? helpers_1.getDeepFromObject(rawData, this.conf.dataKey, []) : rawData;
        if (data instanceof Array) {
            return data;
        }
        throw new Error("Data must be an array. Please check \n      that data extracted from the server response by the key '" + this.conf.dataKey + "' exists and is array.");
    };
    ServerDataSource.prototype.extractTotalFromResponse = function (res) {
        if (res.headers.has(this.conf.totalKey)) {
            return +res.headers.get(this.conf.totalKey);
        }
        else {
            var rawData = res.json();
            return helpers_1.getDeepFromObject(rawData, this.conf.totalKey, 0);
        }
    };
    ServerDataSource.prototype.requestElements = function () {
        return this.http.get(this.conf.endPoint, this.createRequestOptions());
    };
    ServerDataSource.prototype.createRequestOptions = function () {
        var requestOptions = {};
        requestOptions.search = new url_search_params_1.URLSearchParams();
        requestOptions = this.addSortRequestOptions(requestOptions);
        requestOptions = this.addFilterRequestOptions(requestOptions);
        return this.addPagerRequestOptions(requestOptions);
    };
    ServerDataSource.prototype.addSortRequestOptions = function (requestOptions) {
        var _this = this;
        var searchParams = requestOptions.search;
        if (this.sortConf) {
            this.sortConf.forEach(function (fieldConf) {
                searchParams.set(_this.conf.sortFieldKey, fieldConf.field);
                searchParams.set(_this.conf.sortDirKey, fieldConf.direction.toUpperCase());
            });
        }
        return requestOptions;
    };
    ServerDataSource.prototype.addFilterRequestOptions = function (requestOptions) {
        var _this = this;
        var searchParams = requestOptions.search;
        if (this.filterConf.filters) {
            this.filterConf.filters.forEach(function (fieldConf) {
                if (fieldConf['search']) {
                    searchParams.set(_this.conf.filterFieldKey.replace('#field#', fieldConf['field']), fieldConf['search']);
                }
            });
        }
        return requestOptions;
    };
    ServerDataSource.prototype.addPagerRequestOptions = function (requestOptions) {
        var searchParams = requestOptions.search;
        if (this.pagingConf && this.pagingConf['page'] && this.pagingConf['perPage']) {
            searchParams.set(this.conf.pagerPageKey, this.pagingConf['page']);
            searchParams.set(this.conf.pagerLimitKey, this.pagingConf['perPage']);
        }
        return requestOptions;
    };
    return ServerDataSource;
}(local_data_source_1.LocalDataSource));
exports.ServerDataSource = ServerDataSource;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zaGFyZWQvbmcyLXNtYXJ0LXRhYmxlL25nMi1zbWFydC10YWJsZS9saWIvZGF0YS1zb3VyY2Uvc2VydmVyL3NlcnZlci5kYXRhLXNvdXJjZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFDQSxrQ0FBZ0MsNEJBQTRCLENBQUMsQ0FBQTtBQUU3RCxrQ0FBZ0MscUNBQXFDLENBQUMsQ0FBQTtBQUV0RSxtQ0FBaUMsc0JBQXNCLENBQUMsQ0FBQTtBQUN4RCx3QkFBa0MsZUFBZSxDQUFDLENBQUE7QUFFbEQ7SUFBc0Msb0NBQWU7SUFNbkQsMEJBQXNCLElBQVUsRUFBRSxJQUE4QjtRQUE5QixvQkFBOEIsR0FBOUIsU0FBOEI7UUFDOUQsaUJBQU8sQ0FBQztRQURZLFNBQUksR0FBSixJQUFJLENBQU07UUFGdEIscUJBQWdCLEdBQVcsQ0FBQyxDQUFDO1FBS3JDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxxQ0FBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUV2QyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUN4QixNQUFNLElBQUksS0FBSyxDQUFDLG1GQUFtRixDQUFDLENBQUM7UUFDdkcsQ0FBQztJQUNILENBQUM7SUFFRCxnQ0FBSyxHQUFMO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztJQUMvQixDQUFDO0lBRUQsc0NBQVcsR0FBWDtRQUFBLGlCQU9DO1FBTkMsTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQyxHQUFHLENBQUMsVUFBQSxHQUFHO1lBQ25DLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFJLENBQUMsd0JBQXdCLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDM0QsS0FBSSxDQUFDLElBQUksR0FBRyxLQUFJLENBQUMsdUJBQXVCLENBQUMsR0FBRyxDQUFDLENBQUM7WUFFOUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUM7UUFDbkIsQ0FBQyxDQUFDLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDakIsQ0FBQztJQU9TLGtEQUF1QixHQUFqQyxVQUFrQyxHQUFRO1FBQ3hDLElBQU0sT0FBTyxHQUFHLEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMzQixJQUFJLElBQUksR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsMkJBQWlCLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLEVBQUUsQ0FBQyxHQUFHLE9BQU8sQ0FBQztRQUU3RixFQUFFLENBQUMsQ0FBQyxJQUFJLFlBQVksS0FBSyxDQUFDLENBQUMsQ0FBQztZQUMxQixNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2QsQ0FBQztRQUVELE1BQU0sSUFBSSxLQUFLLENBQUMsMEdBQzZDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTywyQkFBd0IsQ0FBQyxDQUFDO0lBQzFHLENBQUM7SUFRUyxtREFBd0IsR0FBbEMsVUFBbUMsR0FBUTtRQUN6QyxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN4QyxNQUFNLENBQUMsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzlDLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLElBQU0sT0FBTyxHQUFHLEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMzQixNQUFNLENBQUMsMkJBQWlCLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzNELENBQUM7SUFDSCxDQUFDO0lBRVMsMENBQWUsR0FBekI7UUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUMsQ0FBQztJQUN4RSxDQUFDO0lBRVMsK0NBQW9CLEdBQTlCO1FBQ0UsSUFBSSxjQUFjLEdBQXVCLEVBQUUsQ0FBQztRQUM1QyxjQUFjLENBQUMsTUFBTSxHQUFHLElBQUksbUNBQWUsRUFBRSxDQUFDO1FBRTlDLGNBQWMsR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDNUQsY0FBYyxHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUM5RCxNQUFNLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQ3JELENBQUM7SUFFUyxnREFBcUIsR0FBL0IsVUFBZ0MsY0FBa0M7UUFBbEUsaUJBV0M7UUFWQyxJQUFJLFlBQVksR0FBc0MsY0FBYyxDQUFDLE1BQU0sQ0FBQztRQUU1RSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUNsQixJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFDLFNBQVM7Z0JBQzlCLFlBQVksQ0FBQyxHQUFHLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUMxRCxZQUFZLENBQUMsR0FBRyxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLFNBQVMsQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztZQUM1RSxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUM7UUFFRCxNQUFNLENBQUMsY0FBYyxDQUFDO0lBQ3hCLENBQUM7SUFFUyxrREFBdUIsR0FBakMsVUFBa0MsY0FBa0M7UUFBcEUsaUJBWUM7UUFYQyxJQUFJLFlBQVksR0FBc0MsY0FBYyxDQUFDLE1BQU0sQ0FBQztRQUU1RSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDNUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLFVBQUMsU0FBUztnQkFDeEMsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDeEIsWUFBWSxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxFQUFFLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUN6RyxDQUFDO1lBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDO1FBRUQsTUFBTSxDQUFDLGNBQWMsQ0FBQztJQUN4QixDQUFDO0lBRVMsaURBQXNCLEdBQWhDLFVBQWlDLGNBQWtDO1FBQ2pFLElBQUksWUFBWSxHQUFzQyxjQUFjLENBQUMsTUFBTSxDQUFDO1FBRTVFLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM3RSxZQUFZLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUNsRSxZQUFZLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztRQUN4RSxDQUFDO1FBRUQsTUFBTSxDQUFDLGNBQWMsQ0FBQztJQUN4QixDQUFDO0lBQ0gsdUJBQUM7QUFBRCxDQS9HQSxBQStHQyxDQS9HcUMsbUNBQWUsR0ErR3BEO0FBL0dZLHdCQUFnQixtQkErRzVCLENBQUEiLCJmaWxlIjoiYXBwL3NoYXJlZC9uZzItc21hcnQtdGFibGUvbmcyLXNtYXJ0LXRhYmxlL2xpYi9kYXRhLXNvdXJjZS9zZXJ2ZXIvc2VydmVyLmRhdGEtc291cmNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSHR0cCB9IGZyb20gJ0Bhbmd1bGFyL2h0dHAnO1xyXG5pbXBvcnQgeyBMb2NhbERhdGFTb3VyY2UgfSBmcm9tICcuLi9sb2NhbC9sb2NhbC5kYXRhLXNvdXJjZSc7XHJcbmltcG9ydCB7IFJlcXVlc3RPcHRpb25zQXJncyB9IGZyb20gJ0Bhbmd1bGFyL2h0dHAvc3JjL2ludGVyZmFjZXMnO1xyXG5pbXBvcnQgeyBVUkxTZWFyY2hQYXJhbXMgfSBmcm9tICdAYW5ndWxhci9odHRwL3NyYy91cmxfc2VhcmNoX3BhcmFtcyc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgU2VydmVyU291cmNlQ29uZiB9IGZyb20gJy4vc2VydmVyLXNvdXJjZS5jb25mJztcclxuaW1wb3J0IHsgZ2V0RGVlcEZyb21PYmplY3QgfSBmcm9tICcuLi8uLi9oZWxwZXJzJztcclxuXHJcbmV4cG9ydCBjbGFzcyBTZXJ2ZXJEYXRhU291cmNlIGV4dGVuZHMgTG9jYWxEYXRhU291cmNlIHtcclxuXHJcbiAgcHJvdGVjdGVkIGNvbmY6IFNlcnZlclNvdXJjZUNvbmY7XHJcblxyXG4gIHByb3RlY3RlZCBsYXN0UmVxdWVzdENvdW50OiBudW1iZXIgPSAwO1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcm90ZWN0ZWQgaHR0cDogSHR0cCwgY29uZjogU2VydmVyU291cmNlQ29uZnx7fSA9IHt9KSB7XHJcbiAgICBzdXBlcigpO1xyXG5cclxuICAgIHRoaXMuY29uZiA9IG5ldyBTZXJ2ZXJTb3VyY2VDb25mKGNvbmYpO1xyXG5cclxuICAgIGlmICghdGhpcy5jb25mLmVuZFBvaW50KSB7XHJcbiAgICAgIHRocm93IG5ldyBFcnJvcignQXQgbGVhc3QgZW5kUG9pbnQgbXVzdCBiZSBzcGVjaWZpZWQgYXMgYSBjb25maWd1cmF0aW9uIG9mIHRoZSBzZXJ2ZXIgZGF0YSBzb3VyY2UuJyk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBjb3VudCgpOiBudW1iZXIge1xyXG4gICAgcmV0dXJuIHRoaXMubGFzdFJlcXVlc3RDb3VudDtcclxuICB9XHJcblxyXG4gIGdldEVsZW1lbnRzKCk6IFByb21pc2U8YW55PiB7XHJcbiAgICByZXR1cm4gdGhpcy5yZXF1ZXN0RWxlbWVudHMoKS5tYXAocmVzID0+IHtcclxuICAgICAgdGhpcy5sYXN0UmVxdWVzdENvdW50ID0gdGhpcy5leHRyYWN0VG90YWxGcm9tUmVzcG9uc2UocmVzKTtcclxuICAgICAgdGhpcy5kYXRhID0gdGhpcy5leHRyYWN0RGF0YUZyb21SZXNwb25zZShyZXMpO1xyXG5cclxuICAgICAgcmV0dXJuIHRoaXMuZGF0YTtcclxuICAgIH0pLnRvUHJvbWlzZSgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogRXh0cmFjdHMgYXJyYXkgb2YgZGF0YSBmcm9tIHNlcnZlciByZXNwb25zZVxyXG4gICAqIEBwYXJhbSByZXNcclxuICAgKiBAcmV0dXJucyB7YW55fVxyXG4gICAqL1xyXG4gIHByb3RlY3RlZCBleHRyYWN0RGF0YUZyb21SZXNwb25zZShyZXM6IGFueSk6IEFycmF5PGFueT4ge1xyXG4gICAgY29uc3QgcmF3RGF0YSA9IHJlcy5qc29uKCk7XHJcbiAgICBsZXQgZGF0YSA9ICEhdGhpcy5jb25mLmRhdGFLZXkgPyBnZXREZWVwRnJvbU9iamVjdChyYXdEYXRhLCB0aGlzLmNvbmYuZGF0YUtleSwgW10pIDogcmF3RGF0YTtcclxuXHJcbiAgICBpZiAoZGF0YSBpbnN0YW5jZW9mIEFycmF5KSB7XHJcbiAgICAgIHJldHVybiBkYXRhO1xyXG4gICAgfVxyXG5cclxuICAgIHRocm93IG5ldyBFcnJvcihgRGF0YSBtdXN0IGJlIGFuIGFycmF5LiBQbGVhc2UgY2hlY2sgXHJcbiAgICAgIHRoYXQgZGF0YSBleHRyYWN0ZWQgZnJvbSB0aGUgc2VydmVyIHJlc3BvbnNlIGJ5IHRoZSBrZXkgJyR7dGhpcy5jb25mLmRhdGFLZXl9JyBleGlzdHMgYW5kIGlzIGFycmF5LmApO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogRXh0cmFjdHMgdG90YWwgcm93cyBjb3VudCBmcm9tIHRoZSBzZXJ2ZXIgcmVzcG9uc2VcclxuICAgKiBMb29rcyBmb3IgdGhlIGNvdW50IGluIHRoZSBoZWRlcnMgZmlyc3QsIHRoZW4gaW4gdGhlIHJlc3BvbnNlIGJvZHlcclxuICAgKiBAcGFyYW0gcmVzXHJcbiAgICogQHJldHVybnMge2FueX1cclxuICAgKi9cclxuICBwcm90ZWN0ZWQgZXh0cmFjdFRvdGFsRnJvbVJlc3BvbnNlKHJlczogYW55KTogbnVtYmVyIHtcclxuICAgIGlmIChyZXMuaGVhZGVycy5oYXModGhpcy5jb25mLnRvdGFsS2V5KSkge1xyXG4gICAgICByZXR1cm4gK3Jlcy5oZWFkZXJzLmdldCh0aGlzLmNvbmYudG90YWxLZXkpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgY29uc3QgcmF3RGF0YSA9IHJlcy5qc29uKCk7XHJcbiAgICAgIHJldHVybiBnZXREZWVwRnJvbU9iamVjdChyYXdEYXRhLCB0aGlzLmNvbmYudG90YWxLZXksIDApO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJvdGVjdGVkIHJlcXVlc3RFbGVtZW50cygpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuaHR0cC5nZXQodGhpcy5jb25mLmVuZFBvaW50LCB0aGlzLmNyZWF0ZVJlcXVlc3RPcHRpb25zKCkpO1xyXG4gIH1cclxuXHJcbiAgcHJvdGVjdGVkIGNyZWF0ZVJlcXVlc3RPcHRpb25zKCk6IFJlcXVlc3RPcHRpb25zQXJncyB7XHJcbiAgICBsZXQgcmVxdWVzdE9wdGlvbnM6IFJlcXVlc3RPcHRpb25zQXJncyA9IHt9O1xyXG4gICAgcmVxdWVzdE9wdGlvbnMuc2VhcmNoID0gbmV3IFVSTFNlYXJjaFBhcmFtcygpO1xyXG5cclxuICAgIHJlcXVlc3RPcHRpb25zID0gdGhpcy5hZGRTb3J0UmVxdWVzdE9wdGlvbnMocmVxdWVzdE9wdGlvbnMpO1xyXG4gICAgcmVxdWVzdE9wdGlvbnMgPSB0aGlzLmFkZEZpbHRlclJlcXVlc3RPcHRpb25zKHJlcXVlc3RPcHRpb25zKTtcclxuICAgIHJldHVybiB0aGlzLmFkZFBhZ2VyUmVxdWVzdE9wdGlvbnMocmVxdWVzdE9wdGlvbnMpO1xyXG4gIH1cclxuXHJcbiAgcHJvdGVjdGVkIGFkZFNvcnRSZXF1ZXN0T3B0aW9ucyhyZXF1ZXN0T3B0aW9uczogUmVxdWVzdE9wdGlvbnNBcmdzKTogUmVxdWVzdE9wdGlvbnNBcmdzIHtcclxuICAgIGxldCBzZWFyY2hQYXJhbXM6IFVSTFNlYXJjaFBhcmFtcyA9IDxVUkxTZWFyY2hQYXJhbXM+IHJlcXVlc3RPcHRpb25zLnNlYXJjaDtcclxuXHJcbiAgICBpZiAodGhpcy5zb3J0Q29uZikge1xyXG4gICAgICB0aGlzLnNvcnRDb25mLmZvckVhY2goKGZpZWxkQ29uZikgPT4ge1xyXG4gICAgICAgIHNlYXJjaFBhcmFtcy5zZXQodGhpcy5jb25mLnNvcnRGaWVsZEtleSwgZmllbGRDb25mLmZpZWxkKTtcclxuICAgICAgICBzZWFyY2hQYXJhbXMuc2V0KHRoaXMuY29uZi5zb3J0RGlyS2V5LCBmaWVsZENvbmYuZGlyZWN0aW9uLnRvVXBwZXJDYXNlKCkpO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gcmVxdWVzdE9wdGlvbnM7XHJcbiAgfVxyXG5cclxuICBwcm90ZWN0ZWQgYWRkRmlsdGVyUmVxdWVzdE9wdGlvbnMocmVxdWVzdE9wdGlvbnM6IFJlcXVlc3RPcHRpb25zQXJncyk6IFJlcXVlc3RPcHRpb25zQXJncyB7XHJcbiAgICBsZXQgc2VhcmNoUGFyYW1zOiBVUkxTZWFyY2hQYXJhbXMgPSA8VVJMU2VhcmNoUGFyYW1zPiByZXF1ZXN0T3B0aW9ucy5zZWFyY2g7XHJcblxyXG4gICAgaWYgKHRoaXMuZmlsdGVyQ29uZi5maWx0ZXJzKSB7XHJcbiAgICAgIHRoaXMuZmlsdGVyQ29uZi5maWx0ZXJzLmZvckVhY2goKGZpZWxkQ29uZikgPT4ge1xyXG4gICAgICAgIGlmIChmaWVsZENvbmZbJ3NlYXJjaCddKSB7XHJcbiAgICAgICAgICBzZWFyY2hQYXJhbXMuc2V0KHRoaXMuY29uZi5maWx0ZXJGaWVsZEtleS5yZXBsYWNlKCcjZmllbGQjJywgZmllbGRDb25mWydmaWVsZCddKSwgZmllbGRDb25mWydzZWFyY2gnXSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gcmVxdWVzdE9wdGlvbnM7XHJcbiAgfVxyXG5cclxuICBwcm90ZWN0ZWQgYWRkUGFnZXJSZXF1ZXN0T3B0aW9ucyhyZXF1ZXN0T3B0aW9uczogUmVxdWVzdE9wdGlvbnNBcmdzKTogUmVxdWVzdE9wdGlvbnNBcmdzIHtcclxuICAgIGxldCBzZWFyY2hQYXJhbXM6IFVSTFNlYXJjaFBhcmFtcyA9IDxVUkxTZWFyY2hQYXJhbXM+IHJlcXVlc3RPcHRpb25zLnNlYXJjaDtcclxuXHJcbiAgICBpZiAodGhpcy5wYWdpbmdDb25mICYmIHRoaXMucGFnaW5nQ29uZlsncGFnZSddICYmIHRoaXMucGFnaW5nQ29uZlsncGVyUGFnZSddKSB7XHJcbiAgICAgIHNlYXJjaFBhcmFtcy5zZXQodGhpcy5jb25mLnBhZ2VyUGFnZUtleSwgdGhpcy5wYWdpbmdDb25mWydwYWdlJ10pO1xyXG4gICAgICBzZWFyY2hQYXJhbXMuc2V0KHRoaXMuY29uZi5wYWdlckxpbWl0S2V5LCB0aGlzLnBhZ2luZ0NvbmZbJ3BlclBhZ2UnXSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHJlcXVlc3RPcHRpb25zO1xyXG4gIH1cclxufVxyXG4iXX0=
