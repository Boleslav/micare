"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var local_sorter_1 = require('./local.sorter');
var local_filter_1 = require('./local.filter');
var local_pager_1 = require('./local.pager');
var data_source_1 = require('../data-source');
var helpers_1 = require('../../helpers');
var LocalDataSource = (function (_super) {
    __extends(LocalDataSource, _super);
    function LocalDataSource(data) {
        if (data === void 0) { data = []; }
        _super.call(this);
        this.data = [];
        this.filteredAndSorted = [];
        this.sortConf = [];
        this.filterConf = {
            filters: [],
            andOperator: true
        };
        this.pagingConf = {};
        this.data = data;
    }
    LocalDataSource.prototype.load = function (data) {
        this.data = data;
        return _super.prototype.load.call(this, data);
    };
    LocalDataSource.prototype.prepend = function (element) {
        this.reset(true);
        this.data.unshift(element);
        return _super.prototype.prepend.call(this, element);
    };
    LocalDataSource.prototype.append = function (element) {
        this.reset(true);
        this.data.push(element);
        return _super.prototype.append.call(this, element);
    };
    LocalDataSource.prototype.add = function (element) {
        this.data.push(element);
        return _super.prototype.add.call(this, element);
    };
    LocalDataSource.prototype.remove = function (element) {
        this.data = this.data.filter(function (el) { return el !== element; });
        return _super.prototype.remove.call(this, element);
    };
    LocalDataSource.prototype.update = function (element, values) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.find(element).then(function (found) {
                found = helpers_1.deepExtend(found, values);
                _super.prototype.update.call(_this, found, values).then(resolve).catch(reject);
            }).catch(reject);
        });
    };
    LocalDataSource.prototype.find = function (element) {
        var found = this.data.find(function (el) { return el === element; });
        if (found) {
            return Promise.resolve(found);
        }
        return Promise.reject(new Error('Element was not found in the dataset'));
    };
    LocalDataSource.prototype.getElements = function () {
        var data = this.data.slice(0);
        return Promise.resolve(this.prepareData(data));
    };
    LocalDataSource.prototype.getAll = function () {
        var data = this.data.slice(0);
        return Promise.resolve(data);
    };
    LocalDataSource.prototype.reset = function (silent) {
        if (silent === void 0) { silent = false; }
        if (silent) {
            this.filterConf = {
                filters: [],
                andOperator: true
            };
            this.sortConf = [];
            this.pagingConf['page'] = 1;
        }
        else {
            this.setFilter([], true, false);
            this.setSort([], false);
            this.setPage(1);
        }
    };
    LocalDataSource.prototype.empty = function () {
        this.data = [];
        return _super.prototype.empty.call(this);
    };
    LocalDataSource.prototype.count = function () {
        return this.filteredAndSorted.length;
    };
    LocalDataSource.prototype.setSort = function (conf, doEmit) {
        if (doEmit === void 0) { doEmit = true; }
        if (conf !== null) {
            conf.forEach(function (fieldConf) {
                if (!fieldConf['field'] || typeof fieldConf['direction'] === 'undefined') {
                    throw new Error('Sort configuration object is not valid');
                }
            });
            this.sortConf = conf;
        }
        _super.prototype.setSort.call(this, conf, doEmit);
        return this;
    };
    LocalDataSource.prototype.setFilter = function (conf, andOperator, doEmit) {
        var _this = this;
        if (andOperator === void 0) { andOperator = true; }
        if (doEmit === void 0) { doEmit = true; }
        if (conf && conf.length > 0) {
            conf.forEach(function (fieldConf) {
                _this.addFilter(fieldConf, andOperator, false);
            });
        }
        else {
            this.filterConf = {
                filters: [],
                andOperator: true
            };
        }
        this.filterConf.andOperator = andOperator;
        this.pagingConf['page'] = 1;
        _super.prototype.setFilter.call(this, conf, andOperator, doEmit);
        return this;
    };
    LocalDataSource.prototype.addFilter = function (fieldConf, andOperator, doEmit) {
        var _this = this;
        if (andOperator === void 0) { andOperator = true; }
        if (doEmit === void 0) { doEmit = true; }
        if (!fieldConf['field'] || typeof fieldConf['search'] === 'undefined') {
            throw new Error('Filter configuration object is not valid');
        }
        var found = false;
        this.filterConf.filters.forEach(function (currentFieldConf, index) {
            if (currentFieldConf['field'] === fieldConf['field']) {
                _this.filterConf.filters[index] = fieldConf;
                found = true;
            }
        });
        if (!found) {
            this.filterConf.filters.push(fieldConf);
        }
        this.filterConf.andOperator = andOperator;
        _super.prototype.addFilter.call(this, fieldConf, andOperator, doEmit);
        return this;
    };
    LocalDataSource.prototype.setPaging = function (page, perPage, doEmit) {
        if (doEmit === void 0) { doEmit = true; }
        this.pagingConf['page'] = page;
        this.pagingConf['perPage'] = perPage;
        _super.prototype.setPaging.call(this, page, perPage, doEmit);
        return this;
    };
    LocalDataSource.prototype.setPage = function (page, doEmit) {
        if (doEmit === void 0) { doEmit = true; }
        this.pagingConf['page'] = page;
        _super.prototype.setPage.call(this, page, doEmit);
        return this;
    };
    LocalDataSource.prototype.getSort = function () {
        return this.sortConf;
    };
    LocalDataSource.prototype.getFilter = function () {
        return this.filterConf;
    };
    LocalDataSource.prototype.getPaging = function () {
        return this.pagingConf;
    };
    LocalDataSource.prototype.prepareData = function (data) {
        data = this.filter(data);
        data = this.sort(data);
        this.filteredAndSorted = data.slice(0);
        return this.paginate(data);
    };
    LocalDataSource.prototype.sort = function (data) {
        if (this.sortConf) {
            this.sortConf.forEach(function (fieldConf) {
                data = local_sorter_1.LocalSorter
                    .sort(data, fieldConf['field'], fieldConf['direction'], fieldConf['compare']);
            });
        }
        return data;
    };
    LocalDataSource.prototype.filter = function (data) {
        if (this.filterConf.filters) {
            if (this.filterConf.andOperator) {
                this.filterConf.filters.forEach(function (fieldConf) {
                    data = local_filter_1.LocalFilter
                        .filter(data, fieldConf['field'], fieldConf['search'], fieldConf['filter']);
                });
            }
            else {
                var mergedData_1 = [];
                this.filterConf.filters.forEach(function (fieldConf) {
                    mergedData_1 = mergedData_1.concat(local_filter_1.LocalFilter
                        .filter(data, fieldConf['field'], fieldConf['search'], fieldConf['filter']));
                });
                data = mergedData_1.filter(function (elem, pos, arr) {
                    return arr.indexOf(elem) === pos;
                });
            }
        }
        return data;
    };
    LocalDataSource.prototype.paginate = function (data) {
        if (this.pagingConf && this.pagingConf['page'] && this.pagingConf['perPage']) {
            data = local_pager_1.LocalPager.paginate(data, this.pagingConf['page'], this.pagingConf['perPage']);
        }
        return data;
    };
    return LocalDataSource;
}(data_source_1.DataSource));
exports.LocalDataSource = LocalDataSource;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zaGFyZWQvbmcyLXNtYXJ0LXRhYmxlL25nMi1zbWFydC10YWJsZS9saWIvZGF0YS1zb3VyY2UvbG9jYWwvbG9jYWwuZGF0YS1zb3VyY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsNkJBQTRCLGdCQUFnQixDQUFDLENBQUE7QUFDN0MsNkJBQTRCLGdCQUFnQixDQUFDLENBQUE7QUFDN0MsNEJBQTJCLGVBQWUsQ0FBQyxDQUFBO0FBQzNDLDRCQUEyQixnQkFBZ0IsQ0FBQyxDQUFBO0FBQzVDLHdCQUEyQixlQUFlLENBQUMsQ0FBQTtBQUUzQztJQUFxQyxtQ0FBVTtJQVc3Qyx5QkFBWSxJQUFxQjtRQUFyQixvQkFBcUIsR0FBckIsU0FBcUI7UUFDL0IsaUJBQU8sQ0FBQztRQVZBLFNBQUksR0FBZSxFQUFFLENBQUM7UUFDdEIsc0JBQWlCLEdBQWUsRUFBRSxDQUFDO1FBQ25DLGFBQVEsR0FBZSxFQUFFLENBQUM7UUFDMUIsZUFBVSxHQUFHO1lBQ3JCLE9BQU8sRUFBUyxFQUFFO1lBQ2xCLFdBQVcsRUFBRSxJQUFJO1NBQ2xCLENBQUM7UUFDUSxlQUFVLEdBQVEsRUFBRSxDQUFDO1FBSzdCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO0lBQ25CLENBQUM7SUFFRCw4QkFBSSxHQUFKLFVBQUssSUFBZ0I7UUFDbkIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFFakIsTUFBTSxDQUFDLGdCQUFLLENBQUMsSUFBSSxZQUFDLElBQUksQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUFFRCxpQ0FBTyxHQUFQLFVBQVEsT0FBWTtRQUNsQixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRWpCLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzNCLE1BQU0sQ0FBQyxnQkFBSyxDQUFDLE9BQU8sWUFBQyxPQUFPLENBQUMsQ0FBQztJQUNoQyxDQUFDO0lBRUQsZ0NBQU0sR0FBTixVQUFPLE9BQVk7UUFDakIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUVqQixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN4QixNQUFNLENBQUMsZ0JBQUssQ0FBQyxNQUFNLFlBQUMsT0FBTyxDQUFDLENBQUM7SUFDL0IsQ0FBQztJQUVELDZCQUFHLEdBQUgsVUFBSSxPQUFZO1FBQ2QsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFFeEIsTUFBTSxDQUFDLGdCQUFLLENBQUMsR0FBRyxZQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQzVCLENBQUM7SUFFRCxnQ0FBTSxHQUFOLFVBQU8sT0FBWTtRQUNqQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQUEsRUFBRSxJQUFJLE9BQUEsRUFBRSxLQUFLLE9BQU8sRUFBZCxDQUFjLENBQUMsQ0FBQztRQUVuRCxNQUFNLENBQUMsZ0JBQUssQ0FBQyxNQUFNLFlBQUMsT0FBTyxDQUFDLENBQUM7SUFDL0IsQ0FBQztJQUVELGdDQUFNLEdBQU4sVUFBTyxPQUFZLEVBQUUsTUFBVztRQUFoQyxpQkFPQztRQU5DLE1BQU0sQ0FBQyxJQUFJLE9BQU8sQ0FBQyxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBQ2pDLEtBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsS0FBSztnQkFDNUIsS0FBSyxHQUFHLG9CQUFVLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDO2dCQUNsQyxnQkFBSyxDQUFDLE1BQU0sYUFBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUMxRCxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDbkIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsOEJBQUksR0FBSixVQUFLLE9BQVk7UUFDZixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFBLEVBQUUsSUFBSSxPQUFBLEVBQUUsS0FBSyxPQUFPLEVBQWQsQ0FBYyxDQUFDLENBQUM7UUFDakQsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNWLE1BQU0sQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2hDLENBQUM7UUFFRCxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEtBQUssQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFDLENBQUM7SUFDM0UsQ0FBQztJQUVELHFDQUFXLEdBQVg7UUFDRSxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUM5QixNQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUVELGdDQUFNLEdBQU47UUFDRSxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUM5QixNQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMvQixDQUFDO0lBRUQsK0JBQUssR0FBTCxVQUFNLE1BQWM7UUFBZCxzQkFBYyxHQUFkLGNBQWM7UUFDbEIsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUNYLElBQUksQ0FBQyxVQUFVLEdBQUc7Z0JBQ2hCLE9BQU8sRUFBRSxFQUFFO2dCQUNYLFdBQVcsRUFBRSxJQUFJO2FBQ2xCLENBQUM7WUFDRixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztZQUNuQixJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUM5QixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDaEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDeEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNsQixDQUFDO0lBQ0gsQ0FBQztJQUVELCtCQUFLLEdBQUw7UUFDRSxJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQztRQUVmLE1BQU0sQ0FBQyxnQkFBSyxDQUFDLEtBQUssV0FBRSxDQUFDO0lBQ3ZCLENBQUM7SUFFRCwrQkFBSyxHQUFMO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUM7SUFDdkMsQ0FBQztJQVlELGlDQUFPLEdBQVAsVUFBUSxJQUFnQixFQUFFLE1BQWE7UUFBYixzQkFBYSxHQUFiLGFBQWE7UUFDckMsRUFBRSxDQUFDLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7WUFFbEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFDLFNBQVM7Z0JBQ3JCLEVBQUUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxJQUFJLE9BQU8sU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLFdBQVcsQ0FBQyxDQUFDLENBQUM7b0JBQ3pFLE1BQU0sSUFBSSxLQUFLLENBQUMsd0NBQXdDLENBQUMsQ0FBQztnQkFDNUQsQ0FBQztZQUNILENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDdkIsQ0FBQztRQUVELGdCQUFLLENBQUMsT0FBTyxZQUFDLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQztRQUM1QixNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQWFELG1DQUFTLEdBQVQsVUFBVSxJQUFnQixFQUFFLFdBQWtCLEVBQUUsTUFBYTtRQUE3RCxpQkFnQkM7UUFoQjJCLDJCQUFrQixHQUFsQixrQkFBa0I7UUFBRSxzQkFBYSxHQUFiLGFBQWE7UUFDM0QsRUFBRSxDQUFDLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM1QixJQUFJLENBQUMsT0FBTyxDQUFDLFVBQUMsU0FBUztnQkFDckIsS0FBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEVBQUUsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQ2hELENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sSUFBSSxDQUFDLFVBQVUsR0FBRztnQkFDaEIsT0FBTyxFQUFFLEVBQUU7Z0JBQ1gsV0FBVyxFQUFFLElBQUk7YUFDbEIsQ0FBQztRQUNKLENBQUM7UUFDRCxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUM7UUFDMUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7UUFFNUIsZ0JBQUssQ0FBQyxTQUFTLFlBQUMsSUFBSSxFQUFFLFdBQVcsRUFBRSxNQUFNLENBQUMsQ0FBQztRQUMzQyxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVELG1DQUFTLEdBQVQsVUFBVSxTQUFjLEVBQUUsV0FBa0IsRUFBRSxNQUFzQjtRQUFwRSxpQkFrQkM7UUFsQnlCLDJCQUFrQixHQUFsQixrQkFBa0I7UUFBRSxzQkFBc0IsR0FBdEIsYUFBc0I7UUFDbEUsRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLElBQUksT0FBTyxTQUFTLENBQUMsUUFBUSxDQUFDLEtBQUssV0FBVyxDQUFDLENBQUMsQ0FBQztZQUN0RSxNQUFNLElBQUksS0FBSyxDQUFDLDBDQUEwQyxDQUFDLENBQUM7UUFDOUQsQ0FBQztRQUVELElBQUksS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNsQixJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsVUFBQyxnQkFBZ0IsRUFBRSxLQUFLO1lBQ3RELEVBQUUsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxLQUFLLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JELEtBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLFNBQVMsQ0FBQztnQkFDM0MsS0FBSyxHQUFHLElBQUksQ0FBQztZQUNmLENBQUM7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUNILEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNYLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUMxQyxDQUFDO1FBQ0QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDO1FBQzFDLGdCQUFLLENBQUMsU0FBUyxZQUFDLFNBQVMsRUFBRSxXQUFXLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDaEQsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNkLENBQUM7SUFFRCxtQ0FBUyxHQUFULFVBQVUsSUFBWSxFQUFFLE9BQWUsRUFBRSxNQUFzQjtRQUF0QixzQkFBc0IsR0FBdEIsYUFBc0I7UUFDN0QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsR0FBRyxJQUFJLENBQUM7UUFDL0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxPQUFPLENBQUM7UUFFckMsZ0JBQUssQ0FBQyxTQUFTLFlBQUMsSUFBSSxFQUFFLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztRQUN2QyxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVELGlDQUFPLEdBQVAsVUFBUSxJQUFZLEVBQUUsTUFBc0I7UUFBdEIsc0JBQXNCLEdBQXRCLGFBQXNCO1FBQzFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDO1FBQy9CLGdCQUFLLENBQUMsT0FBTyxZQUFDLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQztRQUM1QixNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVELGlDQUFPLEdBQVA7UUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUN2QixDQUFDO0lBRUQsbUNBQVMsR0FBVDtRQUNFLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDO0lBQ3pCLENBQUM7SUFFRCxtQ0FBUyxHQUFUO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDekIsQ0FBQztJQUVTLHFDQUFXLEdBQXJCLFVBQXNCLElBQWdCO1FBQ3BDLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3pCLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRXZDLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzdCLENBQUM7SUFFUyw4QkFBSSxHQUFkLFVBQWUsSUFBZ0I7UUFDN0IsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDbEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBQyxTQUFTO2dCQUM5QixJQUFJLEdBQUcsMEJBQVc7cUJBQ2YsSUFBSSxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsT0FBTyxDQUFDLEVBQUUsU0FBUyxDQUFDLFdBQVcsQ0FBQyxFQUFFLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ2xGLENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQztRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBR1MsZ0NBQU0sR0FBaEIsVUFBaUIsSUFBZ0I7UUFDL0IsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQzVCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztnQkFDaEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLFVBQUMsU0FBUztvQkFDeEMsSUFBSSxHQUFHLDBCQUFXO3lCQUNmLE1BQU0sQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLE9BQU8sQ0FBQyxFQUFFLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRSxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDaEYsQ0FBQyxDQUFDLENBQUM7WUFDTCxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ04sSUFBSSxZQUFVLEdBQVUsRUFBRSxDQUFDO2dCQUMzQixJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsVUFBQyxTQUFTO29CQUN4QyxZQUFVLEdBQUcsWUFBVSxDQUFDLE1BQU0sQ0FBQywwQkFBVzt5QkFDdkMsTUFBTSxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsT0FBTyxDQUFDLEVBQUUsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pGLENBQUMsQ0FBQyxDQUFDO2dCQUVILElBQUksR0FBRyxZQUFVLENBQUMsTUFBTSxDQUFDLFVBQUMsSUFBSSxFQUFFLEdBQUcsRUFBRSxHQUFHO29CQUN0QyxNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUM7Z0JBQ25DLENBQUMsQ0FBQyxDQUFDO1lBQ0wsQ0FBQztRQUNILENBQUM7UUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVTLGtDQUFRLEdBQWxCLFVBQW1CLElBQWdCO1FBQ2pDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM3RSxJQUFJLEdBQUcsd0JBQVUsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1FBQ3hGLENBQUM7UUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUNILHNCQUFDO0FBQUQsQ0F6UEEsQUF5UEMsQ0F6UG9DLHdCQUFVLEdBeVA5QztBQXpQWSx1QkFBZSxrQkF5UDNCLENBQUEiLCJmaWxlIjoiYXBwL3NoYXJlZC9uZzItc21hcnQtdGFibGUvbmcyLXNtYXJ0LXRhYmxlL2xpYi9kYXRhLXNvdXJjZS9sb2NhbC9sb2NhbC5kYXRhLXNvdXJjZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IExvY2FsU29ydGVyIH0gZnJvbSAnLi9sb2NhbC5zb3J0ZXInO1xyXG5pbXBvcnQgeyBMb2NhbEZpbHRlciB9IGZyb20gJy4vbG9jYWwuZmlsdGVyJztcclxuaW1wb3J0IHsgTG9jYWxQYWdlciB9IGZyb20gJy4vbG9jYWwucGFnZXInO1xyXG5pbXBvcnQgeyBEYXRhU291cmNlIH0gZnJvbSAnLi4vZGF0YS1zb3VyY2UnO1xyXG5pbXBvcnQgeyBkZWVwRXh0ZW5kIH0gZnJvbSAnLi4vLi4vaGVscGVycyc7XHJcblxyXG5leHBvcnQgY2xhc3MgTG9jYWxEYXRhU291cmNlIGV4dGVuZHMgRGF0YVNvdXJjZSB7XHJcblxyXG4gIHByb3RlY3RlZCBkYXRhOiBBcnJheTxhbnk+ID0gW107XHJcbiAgcHJvdGVjdGVkIGZpbHRlcmVkQW5kU29ydGVkOiBBcnJheTxhbnk+ID0gW107XHJcbiAgcHJvdGVjdGVkIHNvcnRDb25mOiBBcnJheTxhbnk+ID0gW107XHJcbiAgcHJvdGVjdGVkIGZpbHRlckNvbmYgPSB7XHJcbiAgICBmaWx0ZXJzOiA8YW55W10+W10sXHJcbiAgICBhbmRPcGVyYXRvcjogdHJ1ZVxyXG4gIH07XHJcbiAgcHJvdGVjdGVkIHBhZ2luZ0NvbmY6IGFueSA9IHt9O1xyXG5cclxuICBjb25zdHJ1Y3RvcihkYXRhOiBBcnJheTxhbnk+ID0gW10pIHtcclxuICAgIHN1cGVyKCk7XHJcblxyXG4gICAgdGhpcy5kYXRhID0gZGF0YTtcclxuICB9XHJcblxyXG4gIGxvYWQoZGF0YTogQXJyYXk8YW55Pik6IFByb21pc2U8YW55PiB7XHJcbiAgICB0aGlzLmRhdGEgPSBkYXRhO1xyXG5cclxuICAgIHJldHVybiBzdXBlci5sb2FkKGRhdGEpO1xyXG4gIH1cclxuXHJcbiAgcHJlcGVuZChlbGVtZW50OiBhbnkpOiBQcm9taXNlPGFueT4ge1xyXG4gICAgdGhpcy5yZXNldCh0cnVlKTtcclxuXHJcbiAgICB0aGlzLmRhdGEudW5zaGlmdChlbGVtZW50KTtcclxuICAgIHJldHVybiBzdXBlci5wcmVwZW5kKGVsZW1lbnQpO1xyXG4gIH1cclxuXHJcbiAgYXBwZW5kKGVsZW1lbnQ6IGFueSk6IFByb21pc2U8YW55PiB7XHJcbiAgICB0aGlzLnJlc2V0KHRydWUpO1xyXG5cclxuICAgIHRoaXMuZGF0YS5wdXNoKGVsZW1lbnQpO1xyXG4gICAgcmV0dXJuIHN1cGVyLmFwcGVuZChlbGVtZW50KTtcclxuICB9XHJcblxyXG4gIGFkZChlbGVtZW50OiBhbnkpOiBQcm9taXNlPGFueT4ge1xyXG4gICAgdGhpcy5kYXRhLnB1c2goZWxlbWVudCk7XHJcblxyXG4gICAgcmV0dXJuIHN1cGVyLmFkZChlbGVtZW50KTtcclxuICB9XHJcblxyXG4gIHJlbW92ZShlbGVtZW50OiBhbnkpOiBQcm9taXNlPGFueT4ge1xyXG4gICAgdGhpcy5kYXRhID0gdGhpcy5kYXRhLmZpbHRlcihlbCA9PiBlbCAhPT0gZWxlbWVudCk7XHJcblxyXG4gICAgcmV0dXJuIHN1cGVyLnJlbW92ZShlbGVtZW50KTtcclxuICB9XHJcblxyXG4gIHVwZGF0ZShlbGVtZW50OiBhbnksIHZhbHVlczogYW55KTogUHJvbWlzZTxhbnk+IHtcclxuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgIHRoaXMuZmluZChlbGVtZW50KS50aGVuKChmb3VuZCkgPT4ge1xyXG4gICAgICAgIGZvdW5kID0gZGVlcEV4dGVuZChmb3VuZCwgdmFsdWVzKTtcclxuICAgICAgICBzdXBlci51cGRhdGUoZm91bmQsIHZhbHVlcykudGhlbihyZXNvbHZlKS5jYXRjaChyZWplY3QpO1xyXG4gICAgICB9KS5jYXRjaChyZWplY3QpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBmaW5kKGVsZW1lbnQ6IGFueSk6IFByb21pc2U8YW55PiB7XHJcbiAgICBsZXQgZm91bmQgPSB0aGlzLmRhdGEuZmluZChlbCA9PiBlbCA9PT0gZWxlbWVudCk7XHJcbiAgICBpZiAoZm91bmQpIHtcclxuICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShmb3VuZCk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIFByb21pc2UucmVqZWN0KG5ldyBFcnJvcignRWxlbWVudCB3YXMgbm90IGZvdW5kIGluIHRoZSBkYXRhc2V0JykpO1xyXG4gIH1cclxuXHJcbiAgZ2V0RWxlbWVudHMoKTogUHJvbWlzZTxhbnk+IHtcclxuICAgIGxldCBkYXRhID0gdGhpcy5kYXRhLnNsaWNlKDApO1xyXG4gICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZSh0aGlzLnByZXBhcmVEYXRhKGRhdGEpKTtcclxuICB9XHJcblxyXG4gIGdldEFsbCgpOiBQcm9taXNlPGFueT4ge1xyXG4gICAgbGV0IGRhdGEgPSB0aGlzLmRhdGEuc2xpY2UoMCk7XHJcbiAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKGRhdGEpO1xyXG4gIH1cclxuXHJcbiAgcmVzZXQoc2lsZW50ID0gZmFsc2UpOiB2b2lkIHtcclxuICAgIGlmIChzaWxlbnQpIHtcclxuICAgICAgdGhpcy5maWx0ZXJDb25mID0ge1xyXG4gICAgICAgIGZpbHRlcnM6IFtdLFxyXG4gICAgICAgIGFuZE9wZXJhdG9yOiB0cnVlXHJcbiAgICAgIH07XHJcbiAgICAgIHRoaXMuc29ydENvbmYgPSBbXTtcclxuICAgICAgdGhpcy5wYWdpbmdDb25mWydwYWdlJ10gPSAxO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5zZXRGaWx0ZXIoW10sIHRydWUsIGZhbHNlKTtcclxuICAgICAgdGhpcy5zZXRTb3J0KFtdLCBmYWxzZSk7XHJcbiAgICAgIHRoaXMuc2V0UGFnZSgxKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGVtcHR5KCk6IFByb21pc2U8YW55PiB7XHJcbiAgICB0aGlzLmRhdGEgPSBbXTtcclxuXHJcbiAgICByZXR1cm4gc3VwZXIuZW1wdHkoKTtcclxuICB9XHJcblxyXG4gIGNvdW50KCk6IG51bWJlciB7XHJcbiAgICByZXR1cm4gdGhpcy5maWx0ZXJlZEFuZFNvcnRlZC5sZW5ndGg7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKlxyXG4gICAqIEFycmF5IG9mIGNvbmYgb2JqZWN0c1xyXG4gICAqIFtcclxuICAgKiAge2ZpZWxkOiBzdHJpbmcsIGRpcmVjdGlvbjogYXNjfGRlc2N8bnVsbCwgY29tcGFyZTogRnVuY3Rpb258bnVsbH0sXHJcbiAgICogXVxyXG4gICAqIEBwYXJhbSBjb25mXHJcbiAgICogQHBhcmFtIGRvRW1pdFxyXG4gICAqIEByZXR1cm5zIHtMb2NhbERhdGFTb3VyY2V9XHJcbiAgICovXHJcbiAgc2V0U29ydChjb25mOiBBcnJheTxhbnk+LCBkb0VtaXQgPSB0cnVlKTogTG9jYWxEYXRhU291cmNlIHtcclxuICAgIGlmIChjb25mICE9PSBudWxsKSB7XHJcblxyXG4gICAgICBjb25mLmZvckVhY2goKGZpZWxkQ29uZikgPT4ge1xyXG4gICAgICAgIGlmICghZmllbGRDb25mWydmaWVsZCddIHx8IHR5cGVvZiBmaWVsZENvbmZbJ2RpcmVjdGlvbiddID09PSAndW5kZWZpbmVkJykge1xyXG4gICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdTb3J0IGNvbmZpZ3VyYXRpb24gb2JqZWN0IGlzIG5vdCB2YWxpZCcpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICAgIHRoaXMuc29ydENvbmYgPSBjb25mO1xyXG4gICAgfVxyXG5cclxuICAgIHN1cGVyLnNldFNvcnQoY29uZiwgZG9FbWl0KTtcclxuICAgIHJldHVybiB0aGlzO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICpcclxuICAgKiBBcnJheSBvZiBjb25mIG9iamVjdHNcclxuICAgKiBbXHJcbiAgICogIHtmaWVsZDogc3RyaW5nLCBzZWFyY2g6IHN0cmluZywgZmlsdGVyOiBGdW5jdGlvbnxudWxsfSxcclxuICAgKiBdXHJcbiAgICogQHBhcmFtIGNvbmZcclxuICAgKiBAcGFyYW0gYW5kT3BlcmF0b3JcclxuICAgKiBAcGFyYW0gZG9FbWl0XHJcbiAgICogQHJldHVybnMge0xvY2FsRGF0YVNvdXJjZX1cclxuICAgKi9cclxuICBzZXRGaWx0ZXIoY29uZjogQXJyYXk8YW55PiwgYW5kT3BlcmF0b3IgPSB0cnVlLCBkb0VtaXQgPSB0cnVlKTogTG9jYWxEYXRhU291cmNlIHtcclxuICAgIGlmIChjb25mICYmIGNvbmYubGVuZ3RoID4gMCkge1xyXG4gICAgICBjb25mLmZvckVhY2goKGZpZWxkQ29uZikgPT4ge1xyXG4gICAgICAgIHRoaXMuYWRkRmlsdGVyKGZpZWxkQ29uZiwgYW5kT3BlcmF0b3IsIGZhbHNlKTtcclxuICAgICAgfSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmZpbHRlckNvbmYgPSB7XHJcbiAgICAgICAgZmlsdGVyczogW10sXHJcbiAgICAgICAgYW5kT3BlcmF0b3I6IHRydWVcclxuICAgICAgfTtcclxuICAgIH1cclxuICAgIHRoaXMuZmlsdGVyQ29uZi5hbmRPcGVyYXRvciA9IGFuZE9wZXJhdG9yO1xyXG4gICAgdGhpcy5wYWdpbmdDb25mWydwYWdlJ10gPSAxO1xyXG5cclxuICAgIHN1cGVyLnNldEZpbHRlcihjb25mLCBhbmRPcGVyYXRvciwgZG9FbWl0KTtcclxuICAgIHJldHVybiB0aGlzO1xyXG4gIH1cclxuXHJcbiAgYWRkRmlsdGVyKGZpZWxkQ29uZjogYW55LCBhbmRPcGVyYXRvciA9IHRydWUsIGRvRW1pdDogYm9vbGVhbiA9IHRydWUpOiBMb2NhbERhdGFTb3VyY2Uge1xyXG4gICAgaWYgKCFmaWVsZENvbmZbJ2ZpZWxkJ10gfHwgdHlwZW9mIGZpZWxkQ29uZlsnc2VhcmNoJ10gPT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgIHRocm93IG5ldyBFcnJvcignRmlsdGVyIGNvbmZpZ3VyYXRpb24gb2JqZWN0IGlzIG5vdCB2YWxpZCcpO1xyXG4gICAgfVxyXG5cclxuICAgIGxldCBmb3VuZCA9IGZhbHNlO1xyXG4gICAgdGhpcy5maWx0ZXJDb25mLmZpbHRlcnMuZm9yRWFjaCgoY3VycmVudEZpZWxkQ29uZiwgaW5kZXgpID0+IHtcclxuICAgICAgaWYgKGN1cnJlbnRGaWVsZENvbmZbJ2ZpZWxkJ10gPT09IGZpZWxkQ29uZlsnZmllbGQnXSkge1xyXG4gICAgICAgIHRoaXMuZmlsdGVyQ29uZi5maWx0ZXJzW2luZGV4XSA9IGZpZWxkQ29uZjtcclxuICAgICAgICBmb3VuZCA9IHRydWU7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gICAgaWYgKCFmb3VuZCkge1xyXG4gICAgICB0aGlzLmZpbHRlckNvbmYuZmlsdGVycy5wdXNoKGZpZWxkQ29uZik7XHJcbiAgICB9XHJcbiAgICB0aGlzLmZpbHRlckNvbmYuYW5kT3BlcmF0b3IgPSBhbmRPcGVyYXRvcjtcclxuICAgIHN1cGVyLmFkZEZpbHRlcihmaWVsZENvbmYsIGFuZE9wZXJhdG9yLCBkb0VtaXQpO1xyXG4gICAgcmV0dXJuIHRoaXM7XHJcbiAgfVxyXG5cclxuICBzZXRQYWdpbmcocGFnZTogbnVtYmVyLCBwZXJQYWdlOiBudW1iZXIsIGRvRW1pdDogYm9vbGVhbiA9IHRydWUpOiBMb2NhbERhdGFTb3VyY2Uge1xyXG4gICAgdGhpcy5wYWdpbmdDb25mWydwYWdlJ10gPSBwYWdlO1xyXG4gICAgdGhpcy5wYWdpbmdDb25mWydwZXJQYWdlJ10gPSBwZXJQYWdlO1xyXG5cclxuICAgIHN1cGVyLnNldFBhZ2luZyhwYWdlLCBwZXJQYWdlLCBkb0VtaXQpO1xyXG4gICAgcmV0dXJuIHRoaXM7XHJcbiAgfVxyXG5cclxuICBzZXRQYWdlKHBhZ2U6IG51bWJlciwgZG9FbWl0OiBib29sZWFuID0gdHJ1ZSk6IExvY2FsRGF0YVNvdXJjZSB7XHJcbiAgICB0aGlzLnBhZ2luZ0NvbmZbJ3BhZ2UnXSA9IHBhZ2U7XHJcbiAgICBzdXBlci5zZXRQYWdlKHBhZ2UsIGRvRW1pdCk7XHJcbiAgICByZXR1cm4gdGhpcztcclxuICB9XHJcblxyXG4gIGdldFNvcnQoKTogYW55IHtcclxuICAgIHJldHVybiB0aGlzLnNvcnRDb25mO1xyXG4gIH1cclxuXHJcbiAgZ2V0RmlsdGVyKCk6IGFueSB7XHJcbiAgICByZXR1cm4gdGhpcy5maWx0ZXJDb25mO1xyXG4gIH1cclxuXHJcbiAgZ2V0UGFnaW5nKCk6IGFueSB7XHJcbiAgICByZXR1cm4gdGhpcy5wYWdpbmdDb25mO1xyXG4gIH1cclxuXHJcbiAgcHJvdGVjdGVkIHByZXBhcmVEYXRhKGRhdGE6IEFycmF5PGFueT4pOiBBcnJheTxhbnk+IHtcclxuICAgIGRhdGEgPSB0aGlzLmZpbHRlcihkYXRhKTtcclxuICAgIGRhdGEgPSB0aGlzLnNvcnQoZGF0YSk7XHJcbiAgICB0aGlzLmZpbHRlcmVkQW5kU29ydGVkID0gZGF0YS5zbGljZSgwKTtcclxuXHJcbiAgICByZXR1cm4gdGhpcy5wYWdpbmF0ZShkYXRhKTtcclxuICB9XHJcblxyXG4gIHByb3RlY3RlZCBzb3J0KGRhdGE6IEFycmF5PGFueT4pOiBBcnJheTxhbnk+IHtcclxuICAgIGlmICh0aGlzLnNvcnRDb25mKSB7XHJcbiAgICAgIHRoaXMuc29ydENvbmYuZm9yRWFjaCgoZmllbGRDb25mKSA9PiB7XHJcbiAgICAgICAgZGF0YSA9IExvY2FsU29ydGVyXHJcbiAgICAgICAgICAuc29ydChkYXRhLCBmaWVsZENvbmZbJ2ZpZWxkJ10sIGZpZWxkQ29uZlsnZGlyZWN0aW9uJ10sIGZpZWxkQ29uZlsnY29tcGFyZSddKTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gZGF0YTtcclxuICB9XHJcblxyXG4gIC8vIFRPRE86IHJlZmFjdG9yP1xyXG4gIHByb3RlY3RlZCBmaWx0ZXIoZGF0YTogQXJyYXk8YW55Pik6IEFycmF5PGFueT4ge1xyXG4gICAgaWYgKHRoaXMuZmlsdGVyQ29uZi5maWx0ZXJzKSB7XHJcbiAgICAgIGlmICh0aGlzLmZpbHRlckNvbmYuYW5kT3BlcmF0b3IpIHtcclxuICAgICAgICB0aGlzLmZpbHRlckNvbmYuZmlsdGVycy5mb3JFYWNoKChmaWVsZENvbmYpID0+IHtcclxuICAgICAgICAgIGRhdGEgPSBMb2NhbEZpbHRlclxyXG4gICAgICAgICAgICAuZmlsdGVyKGRhdGEsIGZpZWxkQ29uZlsnZmllbGQnXSwgZmllbGRDb25mWydzZWFyY2gnXSwgZmllbGRDb25mWydmaWx0ZXInXSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgbGV0IG1lcmdlZERhdGE6IGFueVtdID0gW107XHJcbiAgICAgICAgdGhpcy5maWx0ZXJDb25mLmZpbHRlcnMuZm9yRWFjaCgoZmllbGRDb25mKSA9PiB7XHJcbiAgICAgICAgICBtZXJnZWREYXRhID0gbWVyZ2VkRGF0YS5jb25jYXQoTG9jYWxGaWx0ZXJcclxuICAgICAgICAgICAgLmZpbHRlcihkYXRhLCBmaWVsZENvbmZbJ2ZpZWxkJ10sIGZpZWxkQ29uZlsnc2VhcmNoJ10sIGZpZWxkQ29uZlsnZmlsdGVyJ10pKTtcclxuICAgICAgICB9KTtcclxuICAgICAgICAvLyByZW1vdmUgbm9uIHVuaXF1ZSBpdGVtc1xyXG4gICAgICAgIGRhdGEgPSBtZXJnZWREYXRhLmZpbHRlcigoZWxlbSwgcG9zLCBhcnIpID0+IHtcclxuICAgICAgICAgIHJldHVybiBhcnIuaW5kZXhPZihlbGVtKSA9PT0gcG9zO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gZGF0YTtcclxuICB9XHJcblxyXG4gIHByb3RlY3RlZCBwYWdpbmF0ZShkYXRhOiBBcnJheTxhbnk+KTogQXJyYXk8YW55PiB7XHJcbiAgICBpZiAodGhpcy5wYWdpbmdDb25mICYmIHRoaXMucGFnaW5nQ29uZlsncGFnZSddICYmIHRoaXMucGFnaW5nQ29uZlsncGVyUGFnZSddKSB7XHJcbiAgICAgIGRhdGEgPSBMb2NhbFBhZ2VyLnBhZ2luYXRlKGRhdGEsIHRoaXMucGFnaW5nQ29uZlsncGFnZSddLCB0aGlzLnBhZ2luZ0NvbmZbJ3BlclBhZ2UnXSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gZGF0YTtcclxuICB9XHJcbn1cclxuIl19
