"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var forms_1 = require('@angular/forms');
var ng2_completer_1 = require('ng2-completer');
var ng2_smart_table_directives_1 = require('./ng2-smart-table.directives');
var cell_component_1 = require('./ng2-smart-table/components/cell/cell.component');
var index_1 = require('./ng2-smart-table/components/cell/cell-view-mode/index');
var index_2 = require('./ng2-smart-table/components/cell/cell-edit-mode/index');
var index_3 = require('./ng2-smart-table/components/cell/cell-editors/index');
var filter_component_1 = require('./ng2-smart-table/components/filter/filter.component');
var index_4 = require('./ng2-smart-table/components/filter/filter-types/index');
var pager_component_1 = require('./ng2-smart-table/components/pager/pager.component');
var thead_directives_1 = require('./ng2-smart-table/components/thead/thead.directives');
var tbody_directives_1 = require('./ng2-smart-table/components/tbody/tbody.directives');
var Ng2SmartTableModule = (function () {
    function Ng2SmartTableModule() {
    }
    Ng2SmartTableModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                ng2_completer_1.Ng2CompleterModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule
            ],
            declarations: [
                cell_component_1.CellComponent,
                index_1.ViewCellComponent,
                index_2.DefaultEditComponent,
                index_2.CustomEditComponent,
                index_1.CustomViewComponent,
                index_2.EditCellComponent,
                index_3.CompleterEditorComponent,
                index_3.InputEditorComponent,
                index_3.SelectEditorComponent,
                index_3.TextareaEditorComponent,
                index_3.CheckboxEditorComponent,
                filter_component_1.FilterComponent,
                index_4.InputFilterComponent,
                index_4.SelectFilterComponent,
                index_4.CheckboxFilterComponent,
                index_4.CompleterFilterComponent,
                pager_component_1.PagerComponent
            ].concat(thead_directives_1.NG2_SMART_TABLE_THEAD_DIRECTIVES, tbody_directives_1.NG2_SMART_TABLE_TBODY_DIRECTIVES, ng2_smart_table_directives_1.NG2_SMART_TABLE_DIRECTIVES),
            exports: ng2_smart_table_directives_1.NG2_SMART_TABLE_DIRECTIVES.slice()
        }), 
        __metadata('design:paramtypes', [])
    ], Ng2SmartTableModule);
    return Ng2SmartTableModule;
}());
exports.Ng2SmartTableModule = Ng2SmartTableModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zaGFyZWQvbmcyLXNtYXJ0LXRhYmxlL25nMi1zbWFydC10YWJsZS5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUF5QixlQUFlLENBQUMsQ0FBQTtBQUN6Qyx1QkFBNkIsaUJBQWlCLENBQUMsQ0FBQTtBQUMvQyxzQkFBaUQsZ0JBQWdCLENBQUMsQ0FBQTtBQUNsRSw4QkFBbUMsZUFBZSxDQUFDLENBQUE7QUFFbkQsMkNBQTJDLDhCQUE4QixDQUFDLENBQUE7QUFDMUUsK0JBQThCLGtEQUFrRCxDQUFDLENBQUE7QUFDakYsc0JBRTZCLHdEQUF3RCxDQUFDLENBQUE7QUFDdEYsc0JBRzZCLHdEQUF3RCxDQUFDLENBQUE7QUFDdEYsc0JBS2lDLHNEQUFzRCxDQUFDLENBQUE7QUFDeEYsaUNBQWdDLHNEQUFzRCxDQUFDLENBQUE7QUFDdkYsc0JBSWtDLHdEQUF3RCxDQUFDLENBQUE7QUFDM0YsZ0NBQStCLG9EQUFvRCxDQUFDLENBQUE7QUFDcEYsaUNBQWlELHFEQUFxRCxDQUFDLENBQUE7QUFDdkcsaUNBQWlELHFEQUFxRCxDQUFDLENBQUE7QUFtQ3ZHO0lBQUE7SUFDQSxDQUFDO0lBbENEO1FBQUMsZUFBUSxDQUFDO1lBQ1IsT0FBTyxFQUFFO2dCQUNQLHFCQUFZO2dCQUNaLGtDQUFrQjtnQkFDbEIsbUJBQVc7Z0JBQ1gsMkJBQW1CO2FBQ3BCO1lBQ0QsWUFBWSxFQUFFO2dCQUNaLDhCQUFhO2dCQUNiLHlCQUFpQjtnQkFDakIsNEJBQW9CO2dCQUNwQiwyQkFBbUI7Z0JBQ25CLDJCQUFtQjtnQkFDbkIseUJBQWlCO2dCQUNqQixnQ0FBd0I7Z0JBQ3hCLDRCQUFvQjtnQkFDcEIsNkJBQXFCO2dCQUNyQiwrQkFBdUI7Z0JBQ3ZCLCtCQUF1QjtnQkFDdkIsa0NBQWU7Z0JBQ2YsNEJBQW9CO2dCQUNwQiw2QkFBcUI7Z0JBQ3JCLCtCQUF1QjtnQkFDdkIsZ0NBQXdCO2dCQUN4QixnQ0FBYztxQkFDWCxtREFBZ0MsRUFDaEMsbURBQWdDLEVBQ2hDLHVEQUEwQixDQUM5QjtZQUNELE9BQU8sRUFDRix1REFBMEIsUUFDOUI7U0FDRixDQUFDOzsyQkFBQTtJQUVGLDBCQUFDO0FBQUQsQ0FEQSxBQUNDLElBQUE7QUFEWSwyQkFBbUIsc0JBQy9CLENBQUEiLCJmaWxlIjoiYXBwL3NoYXJlZC9uZzItc21hcnQtdGFibGUvbmcyLXNtYXJ0LXRhYmxlLm1vZHVsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IEZvcm1zTW9kdWxlLCBSZWFjdGl2ZUZvcm1zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBOZzJDb21wbGV0ZXJNb2R1bGUgfSBmcm9tICduZzItY29tcGxldGVyJztcclxuXHJcbmltcG9ydCB7IE5HMl9TTUFSVF9UQUJMRV9ESVJFQ1RJVkVTIH0gZnJvbSAnLi9uZzItc21hcnQtdGFibGUuZGlyZWN0aXZlcyc7XHJcbmltcG9ydCB7IENlbGxDb21wb25lbnQgfSBmcm9tICcuL25nMi1zbWFydC10YWJsZS9jb21wb25lbnRzL2NlbGwvY2VsbC5jb21wb25lbnQnO1xyXG5pbXBvcnQge1xyXG4gIFZpZXdDZWxsQ29tcG9uZW50LFxyXG4gIEN1c3RvbVZpZXdDb21wb25lbnQgfSBmcm9tICcuL25nMi1zbWFydC10YWJsZS9jb21wb25lbnRzL2NlbGwvY2VsbC12aWV3LW1vZGUvaW5kZXgnO1xyXG5pbXBvcnQge1xyXG4gIEVkaXRDZWxsQ29tcG9uZW50LFxyXG4gIERlZmF1bHRFZGl0Q29tcG9uZW50LFxyXG4gIEN1c3RvbUVkaXRDb21wb25lbnQgfSBmcm9tICcuL25nMi1zbWFydC10YWJsZS9jb21wb25lbnRzL2NlbGwvY2VsbC1lZGl0LW1vZGUvaW5kZXgnO1xyXG5pbXBvcnQge1xyXG4gIENvbXBsZXRlckVkaXRvckNvbXBvbmVudCxcclxuICBJbnB1dEVkaXRvckNvbXBvbmVudCxcclxuICBTZWxlY3RFZGl0b3JDb21wb25lbnQsXHJcbiAgVGV4dGFyZWFFZGl0b3JDb21wb25lbnQsXHJcbiAgQ2hlY2tib3hFZGl0b3JDb21wb25lbnQgfSBmcm9tICcuL25nMi1zbWFydC10YWJsZS9jb21wb25lbnRzL2NlbGwvY2VsbC1lZGl0b3JzL2luZGV4JztcclxuaW1wb3J0IHsgRmlsdGVyQ29tcG9uZW50IH0gZnJvbSAnLi9uZzItc21hcnQtdGFibGUvY29tcG9uZW50cy9maWx0ZXIvZmlsdGVyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7XHJcbiAgSW5wdXRGaWx0ZXJDb21wb25lbnQsXHJcbiAgU2VsZWN0RmlsdGVyQ29tcG9uZW50LFxyXG4gIENoZWNrYm94RmlsdGVyQ29tcG9uZW50LFxyXG4gIENvbXBsZXRlckZpbHRlckNvbXBvbmVudCB9IGZyb20gJy4vbmcyLXNtYXJ0LXRhYmxlL2NvbXBvbmVudHMvZmlsdGVyL2ZpbHRlci10eXBlcy9pbmRleCc7XHJcbmltcG9ydCB7IFBhZ2VyQ29tcG9uZW50IH0gZnJvbSAnLi9uZzItc21hcnQtdGFibGUvY29tcG9uZW50cy9wYWdlci9wYWdlci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBORzJfU01BUlRfVEFCTEVfVEhFQURfRElSRUNUSVZFUyB9IGZyb20gJy4vbmcyLXNtYXJ0LXRhYmxlL2NvbXBvbmVudHMvdGhlYWQvdGhlYWQuZGlyZWN0aXZlcyc7XHJcbmltcG9ydCB7IE5HMl9TTUFSVF9UQUJMRV9UQk9EWV9ESVJFQ1RJVkVTIH0gZnJvbSAnLi9uZzItc21hcnQtdGFibGUvY29tcG9uZW50cy90Ym9keS90Ym9keS5kaXJlY3RpdmVzJztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgaW1wb3J0czogW1xyXG4gICAgQ29tbW9uTW9kdWxlLFxyXG4gICAgTmcyQ29tcGxldGVyTW9kdWxlLFxyXG4gICAgRm9ybXNNb2R1bGUsXHJcbiAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlXHJcbiAgXSxcclxuICBkZWNsYXJhdGlvbnM6IFtcclxuICAgIENlbGxDb21wb25lbnQsXHJcbiAgICBWaWV3Q2VsbENvbXBvbmVudCxcclxuICAgIERlZmF1bHRFZGl0Q29tcG9uZW50LFxyXG4gICAgQ3VzdG9tRWRpdENvbXBvbmVudCxcclxuICAgIEN1c3RvbVZpZXdDb21wb25lbnQsXHJcbiAgICBFZGl0Q2VsbENvbXBvbmVudCxcclxuICAgIENvbXBsZXRlckVkaXRvckNvbXBvbmVudCxcclxuICAgIElucHV0RWRpdG9yQ29tcG9uZW50LFxyXG4gICAgU2VsZWN0RWRpdG9yQ29tcG9uZW50LFxyXG4gICAgVGV4dGFyZWFFZGl0b3JDb21wb25lbnQsXHJcbiAgICBDaGVja2JveEVkaXRvckNvbXBvbmVudCxcclxuICAgIEZpbHRlckNvbXBvbmVudCxcclxuICAgIElucHV0RmlsdGVyQ29tcG9uZW50LFxyXG4gICAgU2VsZWN0RmlsdGVyQ29tcG9uZW50LFxyXG4gICAgQ2hlY2tib3hGaWx0ZXJDb21wb25lbnQsXHJcbiAgICBDb21wbGV0ZXJGaWx0ZXJDb21wb25lbnQsXHJcbiAgICBQYWdlckNvbXBvbmVudCxcclxuICAgIC4uLk5HMl9TTUFSVF9UQUJMRV9USEVBRF9ESVJFQ1RJVkVTLFxyXG4gICAgLi4uTkcyX1NNQVJUX1RBQkxFX1RCT0RZX0RJUkVDVElWRVMsXHJcbiAgICAuLi5ORzJfU01BUlRfVEFCTEVfRElSRUNUSVZFU1xyXG4gIF0sXHJcbiAgZXhwb3J0czogW1xyXG4gICAgLi4uTkcyX1NNQVJUX1RBQkxFX0RJUkVDVElWRVNcclxuICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZzJTbWFydFRhYmxlTW9kdWxlIHtcclxufVxyXG4iXX0=
