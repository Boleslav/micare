"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var forms_1 = require('@angular/forms');
var router_1 = require('@angular/router');
var ng2_bootstrap_1 = require('ng2-bootstrap');
var ngx_uploader_1 = require('ngx-uploader');
var navigationGuard_service_1 = require('./navigationGuard/navigationGuard.service');
var rolesGuard_service_1 = require('./rolesGuard/rolesGuard.service');
var index_1 = require('./name-list/index');
var angular2_toaster_1 = require('angular2-toaster');
var ng2_smart_table_module_1 = require('./ng2-smart-table/ng2-smart-table.module');
var index_2 = require('./spinner/index');
var index_3 = require('./redirect/index');
var index_4 = require('./page-heading/index');
var index_5 = require('./page-heading-parent-portal/index');
var picture_uploader_component_1 = require('./picture-uploader/picture-uploader.component');
var angular_date_value_accessor_1 = require('angular-date-value-accessor');
var SharedModule = (function () {
    function SharedModule() {
    }
    SharedModule.forRoot = function () {
        return {
            ngModule: SharedModule,
            providers: [index_1.NameListService, navigationGuard_service_1.NavigationGuardService, rolesGuard_service_1.RolesGuardService]
        };
    };
    SharedModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                router_1.RouterModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                ng2_bootstrap_1.BsDropdownModule.forRoot(),
                ng2_bootstrap_1.DatepickerModule.forRoot(),
                ng2_bootstrap_1.ModalModule.forRoot(),
                ng2_bootstrap_1.AccordionModule.forRoot(),
                ng2_bootstrap_1.TypeaheadModule.forRoot(),
                ngx_uploader_1.NgUploaderModule
            ],
            declarations: [index_2.SpinnerComponent, index_3.RedirectComponent, index_4.PageHeadingComponent, index_5.PageHeadingParentPortalComponent, picture_uploader_component_1.PictureUploaderComponent],
            entryComponents: [index_2.SpinnerComponent],
            exports: [
                common_1.CommonModule,
                router_1.RouterModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                ng2_bootstrap_1.BsDropdownModule,
                ng2_bootstrap_1.DatepickerModule,
                ng2_bootstrap_1.ModalModule,
                ng2_bootstrap_1.AccordionModule,
                ng2_bootstrap_1.TypeaheadModule,
                angular2_toaster_1.ToasterModule,
                ng2_smart_table_module_1.Ng2SmartTableModule,
                index_2.SpinnerComponent,
                index_3.RedirectComponent,
                index_4.PageHeadingComponent,
                index_5.PageHeadingParentPortalComponent,
                picture_uploader_component_1.PictureUploaderComponent,
                ngx_uploader_1.NgUploaderModule,
                angular_date_value_accessor_1.DateValueAccessorModule
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], SharedModule);
    return SharedModule;
}());
exports.SharedModule = SharedModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zaGFyZWQvc2hhcmVkLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQThDLGVBQWUsQ0FBQyxDQUFBO0FBQzlELHVCQUE2QixpQkFBaUIsQ0FBQyxDQUFBO0FBQy9DLHNCQUFpRCxnQkFBZ0IsQ0FBQyxDQUFBO0FBQ2xFLHVCQUE2QixpQkFBaUIsQ0FBQyxDQUFBO0FBQy9DLDhCQUFrRyxlQUFlLENBQUMsQ0FBQTtBQUNsSCw2QkFBaUMsY0FBYyxDQUFDLENBQUE7QUFFaEQsd0NBQXVDLDJDQUEyQyxDQUFDLENBQUE7QUFDbkYsbUNBQWtDLGlDQUFpQyxDQUFDLENBQUE7QUFDcEUsc0JBQWdDLG1CQUFtQixDQUFDLENBQUE7QUFDcEQsaUNBQThCLGtCQUFrQixDQUFDLENBQUE7QUFDakQsdUNBQW9DLDBDQUEwQyxDQUFDLENBQUE7QUFDL0Usc0JBQWlDLGlCQUFpQixDQUFDLENBQUE7QUFDbkQsc0JBQWtDLGtCQUFrQixDQUFDLENBQUE7QUFDckQsc0JBQXFDLHNCQUFzQixDQUFDLENBQUE7QUFDNUQsc0JBQWlELG9DQUFvQyxDQUFDLENBQUE7QUFDdEYsMkNBQXlDLCtDQUErQyxDQUFDLENBQUE7QUFDekYsNENBQXdDLDZCQUE2QixDQUFDLENBQUE7QUEyQ3RFO0lBQUE7SUFPQSxDQUFDO0lBTlUsb0JBQU8sR0FBZDtRQUNJLE1BQU0sQ0FBQztZQUNILFFBQVEsRUFBRSxZQUFZO1lBQ3RCLFNBQVMsRUFBRSxDQUFDLHVCQUFlLEVBQUUsZ0RBQXNCLEVBQUUsc0NBQWlCLENBQUM7U0FDMUUsQ0FBQztJQUNOLENBQUM7SUEzQ0w7UUFBQyxlQUFRLENBQUM7WUFDTixPQUFPLEVBQUU7Z0JBQ0wscUJBQVk7Z0JBQ1oscUJBQVk7Z0JBQ1osbUJBQVc7Z0JBQ1gsMkJBQW1CO2dCQUNuQixnQ0FBZ0IsQ0FBQyxPQUFPLEVBQUU7Z0JBQzFCLGdDQUFnQixDQUFDLE9BQU8sRUFBRTtnQkFDMUIsMkJBQVcsQ0FBQyxPQUFPLEVBQUU7Z0JBQ3JCLCtCQUFlLENBQUMsT0FBTyxFQUFFO2dCQUN6QiwrQkFBZSxDQUFDLE9BQU8sRUFBRTtnQkFDekIsK0JBQWdCO2FBQ25CO1lBQ0QsWUFBWSxFQUFFLENBQUMsd0JBQWdCLEVBQUUseUJBQWlCLEVBQUUsNEJBQW9CLEVBQUUsd0NBQWdDLEVBQUUscURBQXdCLENBQUM7WUFDckksZUFBZSxFQUFFLENBQUMsd0JBQWdCLENBQUM7WUFDbkMsT0FBTyxFQUFFO2dCQUNMLHFCQUFZO2dCQUNaLHFCQUFZO2dCQUNaLG1CQUFXO2dCQUNYLDJCQUFtQjtnQkFDbkIsZ0NBQWdCO2dCQUNoQixnQ0FBZ0I7Z0JBQ2hCLDJCQUFXO2dCQUNYLCtCQUFlO2dCQUNmLCtCQUFlO2dCQUNmLGdDQUFhO2dCQUNiLDRDQUFtQjtnQkFDbkIsd0JBQWdCO2dCQUNoQix5QkFBaUI7Z0JBQ2pCLDRCQUFvQjtnQkFDcEIsd0NBQWdDO2dCQUNoQyxxREFBd0I7Z0JBQ3hCLCtCQUFnQjtnQkFDaEIscURBQXVCO2FBQzFCO1NBQ0osQ0FBQzs7b0JBQUE7SUFTRixtQkFBQztBQUFELENBUEEsQUFPQyxJQUFBO0FBUFksb0JBQVksZUFPeEIsQ0FBQSIsImZpbGUiOiJhcHAvc2hhcmVkL3NoYXJlZC5tb2R1bGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSwgTW9kdWxlV2l0aFByb3ZpZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBGb3Jtc01vZHVsZSwgUmVhY3RpdmVGb3Jtc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgUm91dGVyTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgQnNEcm9wZG93bk1vZHVsZSwgRGF0ZXBpY2tlck1vZHVsZSwgTW9kYWxNb2R1bGUsIEFjY29yZGlvbk1vZHVsZSwgVHlwZWFoZWFkTW9kdWxlIH0gZnJvbSAnbmcyLWJvb3RzdHJhcCc7XHJcbmltcG9ydCB7IE5nVXBsb2FkZXJNb2R1bGUgfSBmcm9tICduZ3gtdXBsb2FkZXInO1xyXG5cclxuaW1wb3J0IHsgTmF2aWdhdGlvbkd1YXJkU2VydmljZSB9IGZyb20gJy4vbmF2aWdhdGlvbkd1YXJkL25hdmlnYXRpb25HdWFyZC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUm9sZXNHdWFyZFNlcnZpY2UgfSBmcm9tICcuL3JvbGVzR3VhcmQvcm9sZXNHdWFyZC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTmFtZUxpc3RTZXJ2aWNlIH0gZnJvbSAnLi9uYW1lLWxpc3QvaW5kZXgnO1xyXG5pbXBvcnQgeyBUb2FzdGVyTW9kdWxlIH0gZnJvbSAnYW5ndWxhcjItdG9hc3Rlcic7XHJcbmltcG9ydCB7IE5nMlNtYXJ0VGFibGVNb2R1bGUgfSBmcm9tICcuL25nMi1zbWFydC10YWJsZS9uZzItc21hcnQtdGFibGUubW9kdWxlJztcclxuaW1wb3J0IHsgU3Bpbm5lckNvbXBvbmVudCB9IGZyb20gJy4vc3Bpbm5lci9pbmRleCc7XHJcbmltcG9ydCB7IFJlZGlyZWN0Q29tcG9uZW50IH0gZnJvbSAnLi9yZWRpcmVjdC9pbmRleCc7XHJcbmltcG9ydCB7IFBhZ2VIZWFkaW5nQ29tcG9uZW50IH0gZnJvbSAnLi9wYWdlLWhlYWRpbmcvaW5kZXgnO1xyXG5pbXBvcnQgeyBQYWdlSGVhZGluZ1BhcmVudFBvcnRhbENvbXBvbmVudCB9IGZyb20gJy4vcGFnZS1oZWFkaW5nLXBhcmVudC1wb3J0YWwvaW5kZXgnO1xyXG5pbXBvcnQgeyBQaWN0dXJlVXBsb2FkZXJDb21wb25lbnQgfSBmcm9tICcuL3BpY3R1cmUtdXBsb2FkZXIvcGljdHVyZS11cGxvYWRlci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBEYXRlVmFsdWVBY2Nlc3Nvck1vZHVsZSB9IGZyb20gJ2FuZ3VsYXItZGF0ZS12YWx1ZS1hY2Nlc3Nvcic7XHJcblxyXG4vKipcclxuKiBEbyBub3Qgc3BlY2lmeSBwcm92aWRlcnMgZm9yIG1vZHVsZXMgdGhhdCBtaWdodCBiZSBpbXBvcnRlZCBieSBhIGxhenkgbG9hZGVkIG1vZHVsZS5cclxuKi9cclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiBbXHJcbiAgICAgICAgQ29tbW9uTW9kdWxlLFxyXG4gICAgICAgIFJvdXRlck1vZHVsZSxcclxuICAgICAgICBGb3Jtc01vZHVsZSxcclxuICAgICAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlLFxyXG4gICAgICAgIEJzRHJvcGRvd25Nb2R1bGUuZm9yUm9vdCgpLFxyXG4gICAgICAgIERhdGVwaWNrZXJNb2R1bGUuZm9yUm9vdCgpLFxyXG4gICAgICAgIE1vZGFsTW9kdWxlLmZvclJvb3QoKSxcclxuICAgICAgICBBY2NvcmRpb25Nb2R1bGUuZm9yUm9vdCgpLFxyXG4gICAgICAgIFR5cGVhaGVhZE1vZHVsZS5mb3JSb290KCksXHJcbiAgICAgICAgTmdVcGxvYWRlck1vZHVsZVxyXG4gICAgXSxcclxuICAgIGRlY2xhcmF0aW9uczogW1NwaW5uZXJDb21wb25lbnQsIFJlZGlyZWN0Q29tcG9uZW50LCBQYWdlSGVhZGluZ0NvbXBvbmVudCwgUGFnZUhlYWRpbmdQYXJlbnRQb3J0YWxDb21wb25lbnQsIFBpY3R1cmVVcGxvYWRlckNvbXBvbmVudF0sXHJcbiAgICBlbnRyeUNvbXBvbmVudHM6IFtTcGlubmVyQ29tcG9uZW50XSxcclxuICAgIGV4cG9ydHM6IFtcclxuICAgICAgICBDb21tb25Nb2R1bGUsXHJcbiAgICAgICAgUm91dGVyTW9kdWxlLFxyXG4gICAgICAgIEZvcm1zTW9kdWxlLFxyXG4gICAgICAgIFJlYWN0aXZlRm9ybXNNb2R1bGUsXHJcbiAgICAgICAgQnNEcm9wZG93bk1vZHVsZSxcclxuICAgICAgICBEYXRlcGlja2VyTW9kdWxlLFxyXG4gICAgICAgIE1vZGFsTW9kdWxlLFxyXG4gICAgICAgIEFjY29yZGlvbk1vZHVsZSxcclxuICAgICAgICBUeXBlYWhlYWRNb2R1bGUsXHJcbiAgICAgICAgVG9hc3Rlck1vZHVsZSxcclxuICAgICAgICBOZzJTbWFydFRhYmxlTW9kdWxlLFxyXG4gICAgICAgIFNwaW5uZXJDb21wb25lbnQsXHJcbiAgICAgICAgUmVkaXJlY3RDb21wb25lbnQsXHJcbiAgICAgICAgUGFnZUhlYWRpbmdDb21wb25lbnQsXHJcbiAgICAgICAgUGFnZUhlYWRpbmdQYXJlbnRQb3J0YWxDb21wb25lbnQsXHJcbiAgICAgICAgUGljdHVyZVVwbG9hZGVyQ29tcG9uZW50LFxyXG4gICAgICAgIE5nVXBsb2FkZXJNb2R1bGUsXHJcbiAgICAgICAgRGF0ZVZhbHVlQWNjZXNzb3JNb2R1bGVcclxuICAgIF1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBTaGFyZWRNb2R1bGUge1xyXG4gICAgc3RhdGljIGZvclJvb3QoKTogTW9kdWxlV2l0aFByb3ZpZGVycyB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgbmdNb2R1bGU6IFNoYXJlZE1vZHVsZSxcclxuICAgICAgICAgICAgcHJvdmlkZXJzOiBbTmFtZUxpc3RTZXJ2aWNlLCBOYXZpZ2F0aW9uR3VhcmRTZXJ2aWNlLCBSb2xlc0d1YXJkU2VydmljZV1cclxuICAgICAgICB9O1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
