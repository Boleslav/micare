"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../shared/index');
var index_2 = require('../core/index');
var index_3 = require('./index');
var AuthModule = (function () {
    function AuthModule() {
    }
    AuthModule.forRoot = function () {
        return {
            ngModule: index_1.SharedModule,
            providers: [index_3.SysAdminGuardService,
                index_3.CentreAuthGuardService, index_3.ParentAuthGuardService]
        };
    };
    AuthModule = __decorate([
        core_1.NgModule({
            imports: [
                index_1.SharedModule,
                index_2.CoreModule
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], AuthModule);
    return AuthModule;
}());
exports.AuthModule = AuthModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9hdXRoL2F1dGgubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBOEMsZUFBZSxDQUFDLENBQUE7QUFDOUQsc0JBQTZCLGlCQUFpQixDQUFDLENBQUE7QUFDL0Msc0JBQTJCLGVBQWUsQ0FBQyxDQUFBO0FBRTNDLHNCQUVPLFNBQVMsQ0FBQyxDQUFBO0FBU2pCO0lBQUE7SUFRQSxDQUFDO0lBUFUsa0JBQU8sR0FBZDtRQUNJLE1BQU0sQ0FBQztZQUNILFFBQVEsRUFBRSxvQkFBWTtZQUN0QixTQUFTLEVBQUUsQ0FBQyw0QkFBb0I7Z0JBQzVCLDhCQUFzQixFQUFFLDhCQUFzQixDQUFDO1NBQ3RELENBQUM7SUFDTixDQUFDO0lBZEw7UUFBQyxlQUFRLENBQUM7WUFDTixPQUFPLEVBQUU7Z0JBQ0wsb0JBQVk7Z0JBQ1osa0JBQVU7YUFDYjtTQUNKLENBQUM7O2tCQUFBO0lBVUYsaUJBQUM7QUFBRCxDQVJBLEFBUUMsSUFBQTtBQVJZLGtCQUFVLGFBUXRCLENBQUEiLCJmaWxlIjoiYXBwL2F1dGgvYXV0aC5tb2R1bGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSwgTW9kdWxlV2l0aFByb3ZpZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTaGFyZWRNb2R1bGUgfSBmcm9tICcuLi9zaGFyZWQvaW5kZXgnO1xyXG5pbXBvcnQgeyBDb3JlTW9kdWxlIH0gZnJvbSAnLi4vY29yZS9pbmRleCc7XHJcblxyXG5pbXBvcnQge1xyXG4gICAgU3lzQWRtaW5HdWFyZFNlcnZpY2UsIENlbnRyZUF1dGhHdWFyZFNlcnZpY2UsIFBhcmVudEF1dGhHdWFyZFNlcnZpY2VcclxufSBmcm9tICcuL2luZGV4JztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiBbXHJcbiAgICAgICAgU2hhcmVkTW9kdWxlLFxyXG4gICAgICAgIENvcmVNb2R1bGVcclxuICAgIF1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBBdXRoTW9kdWxlIHtcclxuICAgIHN0YXRpYyBmb3JSb290KCk6IE1vZHVsZVdpdGhQcm92aWRlcnMge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIG5nTW9kdWxlOiBTaGFyZWRNb2R1bGUsXHJcbiAgICAgICAgICAgIHByb3ZpZGVyczogW1N5c0FkbWluR3VhcmRTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgQ2VudHJlQXV0aEd1YXJkU2VydmljZSwgUGFyZW50QXV0aEd1YXJkU2VydmljZV1cclxuICAgICAgICB9O1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
