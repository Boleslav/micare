"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var index_1 = require('../../../core/index');
var index_2 = require('../../../shared/index');
var CentreAuthGuardService = (function () {
    function CentreAuthGuardService(_authService, router) {
        this._authService = _authService;
        this.router = router;
    }
    CentreAuthGuardService.prototype.canActivate = function (route, state) {
        if (index_2.Config.ENABLE_AUTHORISATION === 'false') {
            return true;
        }
        if (this._authService.hasValidAccessToken()) {
            return true;
        }
        this.router.navigate(['login']);
        return false;
    };
    CentreAuthGuardService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.AccessTokenService, router_1.Router])
    ], CentreAuthGuardService);
    return CentreAuthGuardService;
}());
exports.CentreAuthGuardService = CentreAuthGuardService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9hdXRoL2NvbXBvbmVudHMvYXV0aEd1YXJkU2VydmljZS9jZW50cmVBdXRoR3VhcmQuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQTJCLGVBQWUsQ0FBQyxDQUFBO0FBQzNDLHVCQUlPLGlCQUFpQixDQUFDLENBQUE7QUFFekIsc0JBQW1DLHFCQUFxQixDQUFDLENBQUE7QUFDekQsc0JBQXVCLHVCQUF1QixDQUFDLENBQUE7QUFHL0M7SUFFSSxnQ0FBb0IsWUFBZ0MsRUFBVSxNQUFjO1FBQXhELGlCQUFZLEdBQVosWUFBWSxDQUFvQjtRQUFVLFdBQU0sR0FBTixNQUFNLENBQVE7SUFFNUUsQ0FBQztJQUVELDRDQUFXLEdBQVgsVUFBWSxLQUE2QixFQUFFLEtBQTBCO1FBRWpFLEVBQUUsQ0FBQyxDQUFDLGNBQU0sQ0FBQyxvQkFBb0IsS0FBSyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQzFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsbUJBQW1CLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDMUMsTUFBTSxDQUFDLElBQUksQ0FBQztRQUNoQixDQUFDO1FBR0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1FBQ2hDLE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFDakIsQ0FBQztJQXBCTDtRQUFDLGlCQUFVLEVBQUU7OzhCQUFBO0lBcUJiLDZCQUFDO0FBQUQsQ0FwQkEsQUFvQkMsSUFBQTtBQXBCWSw4QkFBc0IseUJBb0JsQyxDQUFBIiwiZmlsZSI6ImFwcC9hdXRoL2NvbXBvbmVudHMvYXV0aEd1YXJkU2VydmljZS9jZW50cmVBdXRoR3VhcmQuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHtcclxuICAgIENhbkFjdGl2YXRlLCBSb3V0ZXIsXHJcbiAgICBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LFxyXG4gICAgUm91dGVyU3RhdGVTbmFwc2hvdFxyXG59IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcblxyXG5pbXBvcnQgeyBBY2Nlc3NUb2tlblNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9jb3JlL2luZGV4JztcclxuaW1wb3J0IHsgQ29uZmlnIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL2luZGV4JztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIENlbnRyZUF1dGhHdWFyZFNlcnZpY2UgaW1wbGVtZW50cyBDYW5BY3RpdmF0ZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfYXV0aFNlcnZpY2U6IEFjY2Vzc1Rva2VuU2VydmljZSwgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcikge1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBjYW5BY3RpdmF0ZShyb3V0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgc3RhdGU6IFJvdXRlclN0YXRlU25hcHNob3QpIHtcclxuXHJcbiAgICAgICAgaWYgKENvbmZpZy5FTkFCTEVfQVVUSE9SSVNBVElPTiA9PT0gJ2ZhbHNlJykge1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0aGlzLl9hdXRoU2VydmljZS5oYXNWYWxpZEFjY2Vzc1Rva2VuKCkpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBOYXZpZ2F0ZSB0byB0aGUgbG9naW4gcGFnZVxyXG4gICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnbG9naW4nXSk7XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
