"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var rxjs_1 = require('rxjs');
var router_1 = require('@angular/router');
var index_1 = require('../../../shared/index');
var index_2 = require('../../../core/index');
var SysAdminGuardService = (function () {
    function SysAdminGuardService(_userService, router) {
        this._userService = _userService;
        this.router = router;
    }
    SysAdminGuardService.prototype.canActivate = function (route, state) {
        if (index_1.Config.ENABLE_AUTHORISATION === 'false') {
            return rxjs_1.Observable.of(true);
        }
        return this._userService
            .IsSysAdmin$()
            .flatMap(function (isAdmin) {
            if (isAdmin === true) {
                return rxjs_1.Observable.of(true);
            }
            else {
                return rxjs_1.Observable.of(false);
            }
        });
    };
    SysAdminGuardService.prototype.canActivateChild = function (route, state) {
        return this.canActivate(route, state);
    };
    SysAdminGuardService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_2.CentreUserService, router_1.Router])
    ], SysAdminGuardService);
    return SysAdminGuardService;
}());
exports.SysAdminGuardService = SysAdminGuardService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9hdXRoL2NvbXBvbmVudHMvYXV0aEd1YXJkU2VydmljZS9zeXNBZG1pbkd1YXJkLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUEyQixlQUFlLENBQUMsQ0FBQTtBQUMzQyxxQkFBMkIsTUFBTSxDQUFDLENBQUE7QUFDbEMsdUJBSU8saUJBQWlCLENBQUMsQ0FBQTtBQUV6QixzQkFBdUIsdUJBQXVCLENBQUMsQ0FBQTtBQUMvQyxzQkFBa0MscUJBQXFCLENBQUMsQ0FBQTtBQUd4RDtJQUVJLDhCQUFvQixZQUErQixFQUFVLE1BQWM7UUFBdkQsaUJBQVksR0FBWixZQUFZLENBQW1CO1FBQVUsV0FBTSxHQUFOLE1BQU0sQ0FBUTtJQUUzRSxDQUFDO0lBRUQsMENBQVcsR0FBWCxVQUFZLEtBQTZCLEVBQUUsS0FBMEI7UUFFakUsRUFBRSxDQUFDLENBQUMsY0FBTSxDQUFDLG9CQUFvQixLQUFLLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDMUMsTUFBTSxDQUFDLGlCQUFVLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQy9CLENBQUM7UUFFRCxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVk7YUFDbkIsV0FBVyxFQUFFO2FBQ2IsT0FBTyxDQUFDLFVBQUEsT0FBTztZQUNaLEVBQUUsQ0FBQyxDQUFDLE9BQU8sS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUNuQixNQUFNLENBQUMsaUJBQVUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDL0IsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLE1BQU0sQ0FBQyxpQkFBVSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNoQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRUQsK0NBQWdCLEdBQWhCLFVBQWlCLEtBQTZCLEVBQUUsS0FBMEI7UUFDdEUsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzFDLENBQUM7SUExQkw7UUFBQyxpQkFBVSxFQUFFOzs0QkFBQTtJQTJCYiwyQkFBQztBQUFELENBMUJBLEFBMEJDLElBQUE7QUExQlksNEJBQW9CLHVCQTBCaEMsQ0FBQSIsImZpbGUiOiJhcHAvYXV0aC9jb21wb25lbnRzL2F1dGhHdWFyZFNlcnZpY2Uvc3lzQWRtaW5HdWFyZC5zZXJ2aWNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7XHJcbiAgICBDYW5BY3RpdmF0ZSwgQ2FuQWN0aXZhdGVDaGlsZCxcclxuICAgIFJvdXRlciwgQWN0aXZhdGVkUm91dGVTbmFwc2hvdCxcclxuICAgIFJvdXRlclN0YXRlU25hcHNob3RcclxufSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5cclxuaW1wb3J0IHsgQ29uZmlnIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL2luZGV4JztcclxuaW1wb3J0IHsgQ2VudHJlVXNlclNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9jb3JlL2luZGV4JztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIFN5c0FkbWluR3VhcmRTZXJ2aWNlIGltcGxlbWVudHMgQ2FuQWN0aXZhdGUsIENhbkFjdGl2YXRlQ2hpbGQge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX3VzZXJTZXJ2aWNlOiBDZW50cmVVc2VyU2VydmljZSwgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcikge1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBjYW5BY3RpdmF0ZShyb3V0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgc3RhdGU6IFJvdXRlclN0YXRlU25hcHNob3QpOiBPYnNlcnZhYmxlPGJvb2xlYW4+IHtcclxuXHJcbiAgICAgICAgaWYgKENvbmZpZy5FTkFCTEVfQVVUSE9SSVNBVElPTiA9PT0gJ2ZhbHNlJykge1xyXG4gICAgICAgICAgICByZXR1cm4gT2JzZXJ2YWJsZS5vZih0cnVlKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLl91c2VyU2VydmljZVxyXG4gICAgICAgICAgICAuSXNTeXNBZG1pbiQoKVxyXG4gICAgICAgICAgICAuZmxhdE1hcChpc0FkbWluID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChpc0FkbWluID09PSB0cnVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIE9ic2VydmFibGUub2YodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBPYnNlcnZhYmxlLm9mKGZhbHNlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgY2FuQWN0aXZhdGVDaGlsZChyb3V0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgc3RhdGU6IFJvdXRlclN0YXRlU25hcHNob3QpOiBPYnNlcnZhYmxlPGJvb2xlYW4+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jYW5BY3RpdmF0ZShyb3V0ZSwgc3RhdGUpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
