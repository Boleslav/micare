"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var forms_1 = require('@angular/forms');
var common_1 = require('@angular/common');
var router_1 = require('@angular/router');
var http_1 = require('@angular/http');
var app_component_1 = require('./app.component');
var app_routes_1 = require('./app.routes');
var centre_portal_module_1 = require('./pages/centre-portal/centre-portal.module');
var parents_portal_module_1 = require('./pages/parents-portal/parents-portal.module');
var index_1 = require('./pages/public/index');
var index_2 = require('./shared/index');
var core_module_1 = require('./core/core.module');
var index_3 = require('./auth/index');
var index_4 = require('./api/index');
var angular_2_local_storage_1 = require('angular-2-local-storage');
var index_5 = require('./shared/mixpanel/index');
var index_6 = require('./shared/intercom/index');
var index_7 = require('./core/index');
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                forms_1.FormsModule,
                http_1.HttpModule,
                router_1.RouterModule.forRoot(app_routes_1.routes),
                core_module_1.CoreModule,
                index_2.SharedModule.forRoot(),
                index_3.AuthModule.forRoot(),
                index_4.ApiModule.forRoot(),
                centre_portal_module_1.CentrePortalModule,
                parents_portal_module_1.ParentsPortalModule,
                index_1.PublicModule
            ],
            declarations: [app_component_1.AppComponent],
            providers: [
                {
                    provide: common_1.APP_BASE_HREF,
                    useValue: '/'
                },
                angular_2_local_storage_1.LocalStorageService,
                {
                    provide: angular_2_local_storage_1.LOCAL_STORAGE_SERVICE_CONFIG,
                    useValue: { prefix: 'miswap' }
                },
                {
                    provide: 'hangfireUrlRedirectResolver',
                    useFactory: function (userService) {
                        userService.HangfireUrl$()
                            .subscribe(function (url) {
                            window.location.href = url;
                        });
                        return '';
                    },
                    deps: [index_7.CentreUserService]
                },
                {
                    provide: 'swaggerUrlRedirectResolver',
                    useFactory: function (userService) {
                        userService.SwaggerUrl$()
                            .subscribe(function (url) {
                            window.location.href = url;
                        });
                        return '';
                    },
                    deps: [index_7.CentreUserService]
                },
                index_5.MixpanelService,
                index_5.MixpanelGuard,
                index_6.IntercomService
            ],
            bootstrap: [app_component_1.AppComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9hcHAubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBeUIsZUFBZSxDQUFDLENBQUE7QUFDekMsaUNBQThCLDJCQUEyQixDQUFDLENBQUE7QUFDMUQsc0JBQTRCLGdCQUFnQixDQUFDLENBQUE7QUFDN0MsdUJBQThCLGlCQUFpQixDQUFDLENBQUE7QUFDaEQsdUJBQTZCLGlCQUFpQixDQUFDLENBQUE7QUFDL0MscUJBQTJCLGVBQWUsQ0FBQyxDQUFBO0FBQzNDLDhCQUE2QixpQkFBaUIsQ0FBQyxDQUFBO0FBQy9DLDJCQUF1QixjQUFjLENBQUMsQ0FBQTtBQUV0QyxxQ0FBbUMsNENBQTRDLENBQUMsQ0FBQTtBQUNoRixzQ0FBb0MsOENBQThDLENBQUMsQ0FBQTtBQUNuRixzQkFBNkIsc0JBQXNCLENBQUMsQ0FBQTtBQUNwRCxzQkFBNkIsZ0JBQWdCLENBQUMsQ0FBQTtBQUM5Qyw0QkFBMkIsb0JBQW9CLENBQUMsQ0FBQTtBQUNoRCxzQkFBMkIsY0FBYyxDQUFDLENBQUE7QUFDMUMsc0JBQTBCLGFBQWEsQ0FBQyxDQUFBO0FBRXhDLHdDQUFrRSx5QkFBeUIsQ0FBQyxDQUFBO0FBQzVGLHNCQUErQyx5QkFBeUIsQ0FBQyxDQUFBO0FBQ3pFLHNCQUFnQyx5QkFBeUIsQ0FBQyxDQUFBO0FBQzFELHNCQUFrQyxjQUFjLENBQUMsQ0FBQTtBQXlEakQ7SUFBQTtJQUF5QixDQUFDO0lBdkQxQjtRQUFDLGVBQVEsQ0FBQztZQUNULE9BQU8sRUFBRTtnQkFDUixnQ0FBYTtnQkFDYixtQkFBVztnQkFDWCxpQkFBVTtnQkFDVixxQkFBWSxDQUFDLE9BQU8sQ0FBQyxtQkFBTSxDQUFDO2dCQUM1Qix3QkFBVTtnQkFDVixvQkFBWSxDQUFDLE9BQU8sRUFBRTtnQkFDdEIsa0JBQVUsQ0FBQyxPQUFPLEVBQUU7Z0JBQ3BCLGlCQUFTLENBQUMsT0FBTyxFQUFFO2dCQUNuQix5Q0FBa0I7Z0JBQ2xCLDJDQUFtQjtnQkFDbkIsb0JBQVk7YUFDWjtZQUNELFlBQVksRUFBRSxDQUFDLDRCQUFZLENBQUM7WUFDNUIsU0FBUyxFQUFFO2dCQUNWO29CQUNDLE9BQU8sRUFBRSxzQkFBYTtvQkFDdEIsUUFBUSxFQUFFLGlCQUFpQjtpQkFDM0I7Z0JBQ0QsNkNBQW1CO2dCQUNuQjtvQkFDQyxPQUFPLEVBQUUsc0RBQTRCO29CQUNyQyxRQUFRLEVBQUUsRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFO2lCQUM5QjtnQkFDRDtvQkFDQyxPQUFPLEVBQUUsNkJBQTZCO29CQUN0QyxVQUFVLEVBQUUsVUFBQyxXQUE4Qjt3QkFDMUMsV0FBVyxDQUFDLFlBQVksRUFBRTs2QkFDeEIsU0FBUyxDQUFDLFVBQUMsR0FBRzs0QkFDZCxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksR0FBRyxHQUFHLENBQUM7d0JBQzVCLENBQUMsQ0FBQyxDQUFDO3dCQUNKLE1BQU0sQ0FBQyxFQUFFLENBQUM7b0JBQ1gsQ0FBQztvQkFDRCxJQUFJLEVBQUUsQ0FBQyx5QkFBaUIsQ0FBQztpQkFDekI7Z0JBQ0Q7b0JBQ0MsT0FBTyxFQUFFLDRCQUE0QjtvQkFDckMsVUFBVSxFQUFFLFVBQUMsV0FBOEI7d0JBQzFDLFdBQVcsQ0FBQyxXQUFXLEVBQUU7NkJBQ3ZCLFNBQVMsQ0FBQyxVQUFDLEdBQUc7NEJBQ2QsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDO3dCQUM1QixDQUFDLENBQUMsQ0FBQzt3QkFDSixNQUFNLENBQUMsRUFBRSxDQUFDO29CQUNYLENBQUM7b0JBQ0QsSUFBSSxFQUFFLENBQUMseUJBQWlCLENBQUM7aUJBQ3pCO2dCQUNELHVCQUFlO2dCQUNmLHFCQUFhO2dCQUNiLHVCQUFlO2FBQ2Y7WUFDRCxTQUFTLEVBQUUsQ0FBQyw0QkFBWSxDQUFDO1NBRXpCLENBQUM7O2lCQUFBO0lBRXVCLGdCQUFDO0FBQUQsQ0FBekIsQUFBMEIsSUFBQTtBQUFiLGlCQUFTLFlBQUksQ0FBQSIsImZpbGUiOiJhcHAvYXBwLm1vZHVsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEJyb3dzZXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyJztcclxuaW1wb3J0IHsgRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IEFQUF9CQVNFX0hSRUYgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBSb3V0ZXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBIdHRwTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvaHR0cCc7XHJcbmltcG9ydCB7IEFwcENvbXBvbmVudCB9IGZyb20gJy4vYXBwLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IHJvdXRlcyB9IGZyb20gJy4vYXBwLnJvdXRlcyc7XHJcblxyXG5pbXBvcnQgeyBDZW50cmVQb3J0YWxNb2R1bGUgfSBmcm9tICcuL3BhZ2VzL2NlbnRyZS1wb3J0YWwvY2VudHJlLXBvcnRhbC5tb2R1bGUnO1xyXG5pbXBvcnQgeyBQYXJlbnRzUG9ydGFsTW9kdWxlIH0gZnJvbSAnLi9wYWdlcy9wYXJlbnRzLXBvcnRhbC9wYXJlbnRzLXBvcnRhbC5tb2R1bGUnO1xyXG5pbXBvcnQgeyBQdWJsaWNNb2R1bGUgfSBmcm9tICcuL3BhZ2VzL3B1YmxpYy9pbmRleCc7XHJcbmltcG9ydCB7IFNoYXJlZE1vZHVsZSB9IGZyb20gJy4vc2hhcmVkL2luZGV4JztcclxuaW1wb3J0IHsgQ29yZU1vZHVsZSB9IGZyb20gJy4vY29yZS9jb3JlLm1vZHVsZSc7XHJcbmltcG9ydCB7IEF1dGhNb2R1bGUgfSBmcm9tICcuL2F1dGgvaW5kZXgnO1xyXG5pbXBvcnQgeyBBcGlNb2R1bGUgfSBmcm9tICcuL2FwaS9pbmRleCc7XHJcblxyXG5pbXBvcnQgeyBMb2NhbFN0b3JhZ2VTZXJ2aWNlLCBMT0NBTF9TVE9SQUdFX1NFUlZJQ0VfQ09ORklHIH0gZnJvbSAnYW5ndWxhci0yLWxvY2FsLXN0b3JhZ2UnO1xyXG5pbXBvcnQgeyBNaXhwYW5lbFNlcnZpY2UsIE1peHBhbmVsR3VhcmQgfSBmcm9tICcuL3NoYXJlZC9taXhwYW5lbC9pbmRleCc7XHJcbmltcG9ydCB7IEludGVyY29tU2VydmljZSB9IGZyb20gJy4vc2hhcmVkL2ludGVyY29tL2luZGV4JztcclxuaW1wb3J0IHsgQ2VudHJlVXNlclNlcnZpY2UgfSBmcm9tICcuL2NvcmUvaW5kZXgnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuXHRpbXBvcnRzOiBbXHJcblx0XHRCcm93c2VyTW9kdWxlLFxyXG5cdFx0Rm9ybXNNb2R1bGUsXHJcblx0XHRIdHRwTW9kdWxlLFxyXG5cdFx0Um91dGVyTW9kdWxlLmZvclJvb3Qocm91dGVzKSxcclxuXHRcdENvcmVNb2R1bGUsXHJcblx0XHRTaGFyZWRNb2R1bGUuZm9yUm9vdCgpLFxyXG5cdFx0QXV0aE1vZHVsZS5mb3JSb290KCksXHJcblx0XHRBcGlNb2R1bGUuZm9yUm9vdCgpLFxyXG5cdFx0Q2VudHJlUG9ydGFsTW9kdWxlLFxyXG5cdFx0UGFyZW50c1BvcnRhbE1vZHVsZSxcclxuXHRcdFB1YmxpY01vZHVsZVxyXG5cdF0sXHJcblx0ZGVjbGFyYXRpb25zOiBbQXBwQ29tcG9uZW50XSxcclxuXHRwcm92aWRlcnM6IFtcclxuXHRcdHtcclxuXHRcdFx0cHJvdmlkZTogQVBQX0JBU0VfSFJFRixcclxuXHRcdFx0dXNlVmFsdWU6ICc8JT0gQVBQX0JBU0UgJT4nXHJcblx0XHR9LFxyXG5cdFx0TG9jYWxTdG9yYWdlU2VydmljZSxcclxuXHRcdHtcclxuXHRcdFx0cHJvdmlkZTogTE9DQUxfU1RPUkFHRV9TRVJWSUNFX0NPTkZJRyxcclxuXHRcdFx0dXNlVmFsdWU6IHsgcHJlZml4OiAnbWlzd2FwJyB9XHJcblx0XHR9LFxyXG5cdFx0e1xyXG5cdFx0XHRwcm92aWRlOiAnaGFuZ2ZpcmVVcmxSZWRpcmVjdFJlc29sdmVyJyxcclxuXHRcdFx0dXNlRmFjdG9yeTogKHVzZXJTZXJ2aWNlOiBDZW50cmVVc2VyU2VydmljZSkgPT4ge1xyXG5cdFx0XHRcdHVzZXJTZXJ2aWNlLkhhbmdmaXJlVXJsJCgpXHJcblx0XHRcdFx0XHQuc3Vic2NyaWJlKCh1cmwpID0+IHtcclxuXHRcdFx0XHRcdFx0d2luZG93LmxvY2F0aW9uLmhyZWYgPSB1cmw7XHJcblx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHRyZXR1cm4gJyc7XHJcblx0XHRcdH0sXHJcblx0XHRcdGRlcHM6IFtDZW50cmVVc2VyU2VydmljZV1cclxuXHRcdH0sXHJcblx0XHR7XHJcblx0XHRcdHByb3ZpZGU6ICdzd2FnZ2VyVXJsUmVkaXJlY3RSZXNvbHZlcicsXHJcblx0XHRcdHVzZUZhY3Rvcnk6ICh1c2VyU2VydmljZTogQ2VudHJlVXNlclNlcnZpY2UpID0+IHtcclxuXHRcdFx0XHR1c2VyU2VydmljZS5Td2FnZ2VyVXJsJCgpXHJcblx0XHRcdFx0XHQuc3Vic2NyaWJlKCh1cmwpID0+IHtcclxuXHRcdFx0XHRcdFx0d2luZG93LmxvY2F0aW9uLmhyZWYgPSB1cmw7XHJcblx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHRyZXR1cm4gJyc7XHJcblx0XHRcdH0sXHJcblx0XHRcdGRlcHM6IFtDZW50cmVVc2VyU2VydmljZV1cclxuXHRcdH0sXHJcblx0XHRNaXhwYW5lbFNlcnZpY2UsXHJcblx0XHRNaXhwYW5lbEd1YXJkLFxyXG5cdFx0SW50ZXJjb21TZXJ2aWNlXHJcblx0XSxcclxuXHRib290c3RyYXA6IFtBcHBDb21wb25lbnRdXHJcblxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEFwcE1vZHVsZSB7IH1cclxuIl19
