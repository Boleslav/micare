"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var rxjs_1 = require('rxjs');
var index_1 = require('./index');
var CentreLogoResolver = (function () {
    function CentreLogoResolver(_centreService) {
        this._centreService = _centreService;
    }
    CentreLogoResolver.prototype.resolve = function (route, state) {
        var centreName = route.params['centreName'];
        if (centreName !== undefined) {
            return this._centreService.getCentreLogo$(centreName);
        }
        else {
            return rxjs_1.Observable.of(null);
        }
    };
    CentreLogoResolver = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.CentreService])
    ], CentreLogoResolver);
    return CentreLogoResolver;
}());
exports.CentreLogoResolver = CentreLogoResolver;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wdWJsaWMvY29tcG9uZW50cy9jZW50cmUtbG9nby1yZXNvbHZlci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBMkIsZUFBZSxDQUFDLENBQUE7QUFFM0MscUJBQTJCLE1BQU0sQ0FBQyxDQUFBO0FBRWxDLHNCQUE4QixTQUFTLENBQUMsQ0FBQTtBQUl4QztJQUNJLDRCQUFvQixjQUE2QjtRQUE3QixtQkFBYyxHQUFkLGNBQWMsQ0FBZTtJQUFJLENBQUM7SUFDdEQsb0NBQU8sR0FBUCxVQUNJLEtBQTZCLEVBQzdCLEtBQTBCO1FBRTFCLElBQUksVUFBVSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDNUMsRUFBRSxDQUFDLENBQUMsVUFBVSxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDM0IsTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzFELENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLE1BQU0sQ0FBQyxpQkFBVSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMvQixDQUFDO0lBQ0wsQ0FBQztJQWJMO1FBQUMsaUJBQVUsRUFBRTs7MEJBQUE7SUFjYix5QkFBQztBQUFELENBYkEsQUFhQyxJQUFBO0FBYlksMEJBQWtCLHFCQWE5QixDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9wdWJsaWMvY29tcG9uZW50cy9jZW50cmUtbG9nby1yZXNvbHZlci5zZXJ2aWNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBSZXNvbHZlLCBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBSb3V0ZXJTdGF0ZVNuYXBzaG90IH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgQ2VudHJlU2VydmljZSB9IGZyb20gJy4vaW5kZXgnO1xyXG5pbXBvcnQgeyBDZW50cmVMb2dvRHRvIH0gZnJvbSAnLi4vLi4vLi4vYXBpL2luZGV4JztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIENlbnRyZUxvZ29SZXNvbHZlciBpbXBsZW1lbnRzIFJlc29sdmU8Q2VudHJlTG9nb0R0bz4ge1xyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfY2VudHJlU2VydmljZTogQ2VudHJlU2VydmljZSkgeyB9XHJcbiAgICByZXNvbHZlKFxyXG4gICAgICAgIHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LFxyXG4gICAgICAgIHN0YXRlOiBSb3V0ZXJTdGF0ZVNuYXBzaG90XHJcbiAgICApOiBPYnNlcnZhYmxlPENlbnRyZUxvZ29EdG8+IHtcclxuICAgICAgICBsZXQgY2VudHJlTmFtZSA9IHJvdXRlLnBhcmFtc1snY2VudHJlTmFtZSddO1xyXG4gICAgICAgIGlmIChjZW50cmVOYW1lICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuX2NlbnRyZVNlcnZpY2UuZ2V0Q2VudHJlTG9nbyQoY2VudHJlTmFtZSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIE9ic2VydmFibGUub2YobnVsbCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==
