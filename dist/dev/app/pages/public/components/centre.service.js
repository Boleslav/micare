"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../api/index');
var CentreService = (function () {
    function CentreService(_apiClient) {
        this._apiClient = _apiClient;
        this.ImageEncoding = 'data:image/png;base64,';
    }
    CentreService.prototype.getCentreLogo$ = function (uniqueName) {
        var _this = this;
        return this._apiClient
            .centre_GetCentreLogoByUniqueName(uniqueName)
            .map(function (res) {
            var logoDto = res;
            logoDto.imageBase64 = _this.ImageEncoding + logoDto.imageBase64;
            return logoDto;
        });
    };
    CentreService.prototype.doesCentreExist$ = function (uniqueName) {
        return this
            ._apiClient
            .centre_CentreExistsByUniqueName(uniqueName);
    };
    CentreService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.ApiClientService])
    ], CentreService);
    return CentreService;
}());
exports.CentreService = CentreService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wdWJsaWMvY29tcG9uZW50cy9jZW50cmUuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQTJCLGVBQWUsQ0FBQyxDQUFBO0FBRzNDLHNCQUFnRCxvQkFBb0IsQ0FBQyxDQUFBO0FBR3JFO0lBSUksdUJBQW9CLFVBQTRCO1FBQTVCLGVBQVUsR0FBVixVQUFVLENBQWtCO1FBRi9CLGtCQUFhLEdBQVcsd0JBQXdCLENBQUM7SUFFZCxDQUFDO0lBRTlDLHNDQUFjLEdBQXJCLFVBQXNCLFVBQWtCO1FBQXhDLGlCQVNDO1FBUEcsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVO2FBQ2pCLGdDQUFnQyxDQUFDLFVBQVUsQ0FBQzthQUM1QyxHQUFHLENBQUMsVUFBQSxHQUFHO1lBQ0osSUFBSSxPQUFPLEdBQWtCLEdBQUcsQ0FBQztZQUNqQyxPQUFPLENBQUMsV0FBVyxHQUFHLEtBQUksQ0FBQyxhQUFhLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQztZQUMvRCxNQUFNLENBQUMsT0FBTyxDQUFDO1FBQ25CLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVNLHdDQUFnQixHQUF2QixVQUF3QixVQUFrQjtRQUN0QyxNQUFNLENBQUMsSUFBSTthQUNOLFVBQVU7YUFDViwrQkFBK0IsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBdEJMO1FBQUMsaUJBQVUsRUFBRTs7cUJBQUE7SUF1QmIsb0JBQUM7QUFBRCxDQXRCQSxBQXNCQyxJQUFBO0FBdEJZLHFCQUFhLGdCQXNCekIsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvcHVibGljL2NvbXBvbmVudHMvY2VudHJlLnNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuXHJcbmltcG9ydCB7IEFwaUNsaWVudFNlcnZpY2UsIENlbnRyZUxvZ29EdG8gfSBmcm9tICcuLi8uLi8uLi9hcGkvaW5kZXgnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgQ2VudHJlU2VydmljZSB7XHJcblxyXG4gICAgcHJpdmF0ZSByZWFkb25seSBJbWFnZUVuY29kaW5nOiBzdHJpbmcgPSAnZGF0YTppbWFnZS9wbmc7YmFzZTY0LCc7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfYXBpQ2xpZW50OiBBcGlDbGllbnRTZXJ2aWNlKSB7IH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0Q2VudHJlTG9nbyQodW5pcXVlTmFtZTogc3RyaW5nKTogT2JzZXJ2YWJsZTxDZW50cmVMb2dvRHRvPiB7XHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLl9hcGlDbGllbnRcclxuICAgICAgICAgICAgLmNlbnRyZV9HZXRDZW50cmVMb2dvQnlVbmlxdWVOYW1lKHVuaXF1ZU5hbWUpXHJcbiAgICAgICAgICAgIC5tYXAocmVzID0+IHtcclxuICAgICAgICAgICAgICAgIGxldCBsb2dvRHRvOiBDZW50cmVMb2dvRHRvID0gcmVzO1xyXG4gICAgICAgICAgICAgICAgbG9nb0R0by5pbWFnZUJhc2U2NCA9IHRoaXMuSW1hZ2VFbmNvZGluZyArIGxvZ29EdG8uaW1hZ2VCYXNlNjQ7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbG9nb0R0bztcclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGRvZXNDZW50cmVFeGlzdCQodW5pcXVlTmFtZTogc3RyaW5nKTogT2JzZXJ2YWJsZTxib29sZWFuPiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXNcclxuICAgICAgICAgICAgLl9hcGlDbGllbnRcclxuICAgICAgICAgICAgLmNlbnRyZV9DZW50cmVFeGlzdHNCeVVuaXF1ZU5hbWUodW5pcXVlTmFtZSk7XHJcbiAgICB9XHJcbn1cclxuIl19
