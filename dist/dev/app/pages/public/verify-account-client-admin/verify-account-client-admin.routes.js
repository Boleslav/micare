"use strict";
var index_1 = require('./index');
exports.VerifyAccountClientAdminRoutes = [
    {
        path: 'verify-account-client-admin/:token', component: index_1.VerifyAccountClientAdminComponent
    }
];

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wdWJsaWMvdmVyaWZ5LWFjY291bnQtY2xpZW50LWFkbWluL3ZlcmlmeS1hY2NvdW50LWNsaWVudC1hZG1pbi5yb3V0ZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBLHNCQUFrRCxTQUFTLENBQUMsQ0FBQTtBQUUvQyxzQ0FBOEIsR0FBWTtJQUN0RDtRQUNDLElBQUksRUFBRSxvQ0FBb0MsRUFBRSxTQUFTLEVBQUUseUNBQWlDO0tBQ3hGO0NBQ0QsQ0FBQyIsImZpbGUiOiJhcHAvcGFnZXMvcHVibGljL3ZlcmlmeS1hY2NvdW50LWNsaWVudC1hZG1pbi92ZXJpZnktYWNjb3VudC1jbGllbnQtYWRtaW4ucm91dGVzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBWZXJpZnlBY2NvdW50Q2xpZW50QWRtaW5Db21wb25lbnQgfSBmcm9tICcuL2luZGV4JztcclxuXHJcbmV4cG9ydCBjb25zdCBWZXJpZnlBY2NvdW50Q2xpZW50QWRtaW5Sb3V0ZXM6IFJvdXRlW10gPSBbXHJcblx0e1xyXG5cdFx0cGF0aDogJ3ZlcmlmeS1hY2NvdW50LWNsaWVudC1hZG1pbi86dG9rZW4nLCBjb21wb25lbnQ6IFZlcmlmeUFjY291bnRDbGllbnRBZG1pbkNvbXBvbmVudFxyXG5cdH1cclxuXTtcclxuIl19
