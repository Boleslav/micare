"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../shared/index');
var index_2 = require('./index');
var VerifyAccountClientAdminModule = (function () {
    function VerifyAccountClientAdminModule() {
    }
    VerifyAccountClientAdminModule = __decorate([
        core_1.NgModule({
            imports: [index_1.SharedModule],
            declarations: [index_2.VerifyAccountClientAdminComponent],
            providers: []
        }), 
        __metadata('design:paramtypes', [])
    ], VerifyAccountClientAdminModule);
    return VerifyAccountClientAdminModule;
}());
exports.VerifyAccountClientAdminModule = VerifyAccountClientAdminModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wdWJsaWMvdmVyaWZ5LWFjY291bnQtY2xpZW50LWFkbWluL3ZlcmlmeS1hY2NvdW50LWNsaWVudC1hZG1pbi5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUF5QixlQUFlLENBQUMsQ0FBQTtBQUN6QyxzQkFBNkIsdUJBQXVCLENBQUMsQ0FBQTtBQUVyRCxzQkFBa0QsU0FBUyxDQUFDLENBQUE7QUFRNUQ7SUFBQTtJQUE4QyxDQUFDO0lBTi9DO1FBQUMsZUFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFLENBQUMsb0JBQVksQ0FBQztZQUN2QixZQUFZLEVBQUUsQ0FBQyx5Q0FBaUMsQ0FBQztZQUNqRCxTQUFTLEVBQUUsRUFBRTtTQUNoQixDQUFDOztzQ0FBQTtJQUU0QyxxQ0FBQztBQUFELENBQTlDLEFBQStDLElBQUE7QUFBbEMsc0NBQThCLGlDQUFJLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL3B1YmxpYy92ZXJpZnktYWNjb3VudC1jbGllbnQtYWRtaW4vdmVyaWZ5LWFjY291bnQtY2xpZW50LWFkbWluLm1vZHVsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFNoYXJlZE1vZHVsZSB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9pbmRleCc7XHJcblxyXG5pbXBvcnQgeyBWZXJpZnlBY2NvdW50Q2xpZW50QWRtaW5Db21wb25lbnQgfSBmcm9tICcuL2luZGV4JztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiBbU2hhcmVkTW9kdWxlXSxcclxuICAgIGRlY2xhcmF0aW9uczogW1ZlcmlmeUFjY291bnRDbGllbnRBZG1pbkNvbXBvbmVudF0sXHJcbiAgICBwcm92aWRlcnM6IFtdXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgVmVyaWZ5QWNjb3VudENsaWVudEFkbWluTW9kdWxlIHsgfVxyXG4iXX0=
