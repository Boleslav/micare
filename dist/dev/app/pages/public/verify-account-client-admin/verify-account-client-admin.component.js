"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var index_1 = require('../../../api/index');
var VerifyAccountClientAdminComponent = (function () {
    function VerifyAccountClientAdminComponent(_apiClient, _route) {
        this._apiClient = _apiClient;
        this._route = _route;
        this.verificationCompleted = undefined;
        this.token = undefined;
    }
    VerifyAccountClientAdminComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route
            .params
            .switchMap(function (params) {
            _this.token = params['token'];
            return _this._apiClient.account_VerifyEmail(_this.token);
        })
            .subscribe(function (res) { return _this.verificationCompleted = true; });
    };
    VerifyAccountClientAdminComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'verify-account-client-admin-cmp',
            templateUrl: 'verify-account-client-admin.component.html'
        }), 
        __metadata('design:paramtypes', [index_1.ApiClientService, router_1.ActivatedRoute])
    ], VerifyAccountClientAdminComponent);
    return VerifyAccountClientAdminComponent;
}());
exports.VerifyAccountClientAdminComponent = VerifyAccountClientAdminComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wdWJsaWMvdmVyaWZ5LWFjY291bnQtY2xpZW50LWFkbWluL3ZlcmlmeS1hY2NvdW50LWNsaWVudC1hZG1pbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUFrQyxlQUFlLENBQUMsQ0FBQTtBQUNsRCx1QkFBdUMsaUJBQWlCLENBQUMsQ0FBQTtBQUV6RCxzQkFBaUMsb0JBQW9CLENBQUMsQ0FBQTtBQVF0RDtJQUtFLDJDQUFvQixVQUE0QixFQUFVLE1BQXNCO1FBQTVELGVBQVUsR0FBVixVQUFVLENBQWtCO1FBQVUsV0FBTSxHQUFOLE1BQU0sQ0FBZ0I7UUFIeEUsMEJBQXFCLEdBQVksU0FBUyxDQUFDO1FBQzNDLFVBQUssR0FBVyxTQUFTLENBQUM7SUFHbEMsQ0FBQztJQUVELG9EQUFRLEdBQVI7UUFBQSxpQkFTQztRQVBDLElBQUksQ0FBQyxNQUFNO2FBQ1IsTUFBTTthQUNOLFNBQVMsQ0FBQyxVQUFDLE1BQWM7WUFDeEIsS0FBSSxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDN0IsTUFBTSxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsbUJBQW1CLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3pELENBQUMsQ0FBQzthQUNELFNBQVMsQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEtBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLEVBQWpDLENBQWlDLENBQUMsQ0FBQztJQUN6RCxDQUFDO0lBdkJIO1FBQUMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsaUNBQWlDO1lBQzNDLFdBQVcsRUFBRSw0Q0FBNEM7U0FDMUQsQ0FBQzs7eUNBQUE7SUFvQkYsd0NBQUM7QUFBRCxDQWxCQSxBQWtCQyxJQUFBO0FBbEJZLHlDQUFpQyxvQ0FrQjdDLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL3B1YmxpYy92ZXJpZnktYWNjb3VudC1jbGllbnQtYWRtaW4vdmVyaWZ5LWFjY291bnQtY2xpZW50LWFkbWluLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlLCBQYXJhbXMgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5cclxuaW1wb3J0IHsgQXBpQ2xpZW50U2VydmljZSB9IGZyb20gJy4uLy4uLy4uL2FwaS9pbmRleCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gIHNlbGVjdG9yOiAndmVyaWZ5LWFjY291bnQtY2xpZW50LWFkbWluLWNtcCcsXHJcbiAgdGVtcGxhdGVVcmw6ICd2ZXJpZnktYWNjb3VudC1jbGllbnQtYWRtaW4uY29tcG9uZW50Lmh0bWwnXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgVmVyaWZ5QWNjb3VudENsaWVudEFkbWluQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgcHJpdmF0ZSB2ZXJpZmljYXRpb25Db21wbGV0ZWQ6IGJvb2xlYW4gPSB1bmRlZmluZWQ7XHJcbiAgcHJpdmF0ZSB0b2tlbjogc3RyaW5nID0gdW5kZWZpbmVkO1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9hcGlDbGllbnQ6IEFwaUNsaWVudFNlcnZpY2UsIHByaXZhdGUgX3JvdXRlOiBBY3RpdmF0ZWRSb3V0ZSkge1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcblxyXG4gICAgdGhpcy5fcm91dGVcclxuICAgICAgLnBhcmFtc1xyXG4gICAgICAuc3dpdGNoTWFwKChwYXJhbXM6IFBhcmFtcykgPT4ge1xyXG4gICAgICAgIHRoaXMudG9rZW4gPSBwYXJhbXNbJ3Rva2VuJ107XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2FwaUNsaWVudC5hY2NvdW50X1ZlcmlmeUVtYWlsKHRoaXMudG9rZW4pO1xyXG4gICAgICB9KVxyXG4gICAgICAuc3Vic2NyaWJlKHJlcyA9PiB0aGlzLnZlcmlmaWNhdGlvbkNvbXBsZXRlZCA9IHRydWUpO1xyXG4gIH1cclxufVxyXG4iXX0=
