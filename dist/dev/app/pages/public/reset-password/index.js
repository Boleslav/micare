"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
__export(require('./reset-password.component'));
__export(require('./reset-password.routes'));
__export(require('./reset-password.module'));

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wdWJsaWMvcmVzZXQtcGFzc3dvcmQvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLGlCQUFjLDRCQUE0QixDQUFDLEVBQUE7QUFDM0MsaUJBQWMseUJBQXlCLENBQUMsRUFBQTtBQUN4QyxpQkFBYyx5QkFBeUIsQ0FBQyxFQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9wdWJsaWMvcmVzZXQtcGFzc3dvcmQvaW5kZXguanMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgKiBmcm9tICcuL3Jlc2V0LXBhc3N3b3JkLmNvbXBvbmVudCc7XHJcbmV4cG9ydCAqIGZyb20gJy4vcmVzZXQtcGFzc3dvcmQucm91dGVzJztcclxuZXhwb3J0ICogZnJvbSAnLi9yZXNldC1wYXNzd29yZC5tb2R1bGUnO1xyXG4iXX0=
