"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../shared/index');
var index_2 = require('./index');
var ResetPasswordModule = (function () {
    function ResetPasswordModule() {
    }
    ResetPasswordModule = __decorate([
        core_1.NgModule({
            imports: [index_1.SharedModule],
            declarations: [index_2.ResetPasswordComponent],
            providers: []
        }), 
        __metadata('design:paramtypes', [])
    ], ResetPasswordModule);
    return ResetPasswordModule;
}());
exports.ResetPasswordModule = ResetPasswordModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wdWJsaWMvcmVzZXQtcGFzc3dvcmQvcmVzZXQtcGFzc3dvcmQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBeUIsZUFBZSxDQUFDLENBQUE7QUFDekMsc0JBQTZCLHVCQUF1QixDQUFDLENBQUE7QUFFckQsc0JBQXVDLFNBQVMsQ0FBQyxDQUFBO0FBUWpEO0lBQUE7SUFBbUMsQ0FBQztJQU5wQztRQUFDLGVBQVEsQ0FBQztZQUNOLE9BQU8sRUFBRSxDQUFDLG9CQUFZLENBQUM7WUFDdkIsWUFBWSxFQUFFLENBQUMsOEJBQXNCLENBQUM7WUFDdEMsU0FBUyxFQUFFLEVBQUU7U0FDaEIsQ0FBQzs7MkJBQUE7SUFFaUMsMEJBQUM7QUFBRCxDQUFuQyxBQUFvQyxJQUFBO0FBQXZCLDJCQUFtQixzQkFBSSxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9wdWJsaWMvcmVzZXQtcGFzc3dvcmQvcmVzZXQtcGFzc3dvcmQubW9kdWxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU2hhcmVkTW9kdWxlIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL2luZGV4JztcclxuXHJcbmltcG9ydCB7IFJlc2V0UGFzc3dvcmRDb21wb25lbnQgfSBmcm9tICcuL2luZGV4JztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiBbU2hhcmVkTW9kdWxlXSxcclxuICAgIGRlY2xhcmF0aW9uczogW1Jlc2V0UGFzc3dvcmRDb21wb25lbnRdLFxyXG4gICAgcHJvdmlkZXJzOiBbXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFJlc2V0UGFzc3dvcmRNb2R1bGUgeyB9XHJcbiJdfQ==
