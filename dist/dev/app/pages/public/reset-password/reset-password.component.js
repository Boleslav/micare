"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var forms_1 = require('@angular/forms');
var index_1 = require('../../../api/index');
var index_2 = require('../../../validators/index');
var ResetPasswordComponent = (function () {
    function ResetPasswordComponent(fb, _apiClient, _route) {
        this._apiClient = _apiClient;
        this._route = _route;
        this.emailStr = null;
        this.resetPasswordSubmitted = false;
        this.resetPasswordConfirmed = false;
        this.form = fb.group({
            'email': ['', forms_1.Validators.compose([forms_1.Validators.required, index_2.EmailValidator.validate])]
        });
        this.email = this.form.controls['email'];
    }
    ResetPasswordComponent.prototype.resetPassword = function () {
        var _this = this;
        this.resetPasswordSubmitted = true;
        var emailModel = new index_1.EmailModel();
        emailModel.init({
            Email: this.emailStr
        });
        this._apiClient
            .account_RequestResetPassword(emailModel)
            .subscribe(function (res) {
            _this.resetPasswordConfirmed = true;
        });
    };
    ResetPasswordComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'reset-password-cmp',
            templateUrl: 'reset-password.component.html'
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, index_1.ApiClientService, router_1.ActivatedRoute])
    ], ResetPasswordComponent);
    return ResetPasswordComponent;
}());
exports.ResetPasswordComponent = ResetPasswordComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wdWJsaWMvcmVzZXQtcGFzc3dvcmQvcmVzZXQtcGFzc3dvcmQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBMEIsZUFBZSxDQUFDLENBQUE7QUFDMUMsdUJBQStCLGlCQUFpQixDQUFDLENBQUE7QUFDakQsc0JBQW9FLGdCQUFnQixDQUFDLENBQUE7QUFDckYsc0JBQTZDLG9CQUFvQixDQUFDLENBQUE7QUFDbEUsc0JBQStCLDJCQUEyQixDQUFDLENBQUE7QUFPM0Q7SUFRSSxnQ0FBWSxFQUFlLEVBQVUsVUFBNEIsRUFBVSxNQUFzQjtRQUE1RCxlQUFVLEdBQVYsVUFBVSxDQUFrQjtRQUFVLFdBQU0sR0FBTixNQUFNLENBQWdCO1FBSnpGLGFBQVEsR0FBVyxJQUFJLENBQUM7UUFDeEIsMkJBQXNCLEdBQVksS0FBSyxDQUFDO1FBQ3hDLDJCQUFzQixHQUFZLEtBQUssQ0FBQztRQUc1QyxJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQyxLQUFLLENBQUM7WUFDakIsT0FBTyxFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsa0JBQVUsQ0FBQyxRQUFRLEVBQUUsc0JBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1NBQ3BGLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDN0MsQ0FBQztJQUVELDhDQUFhLEdBQWI7UUFBQSxpQkFZQztRQVZHLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUM7UUFDbkMsSUFBSSxVQUFVLEdBQUcsSUFBSSxrQkFBVSxFQUFFLENBQUM7UUFDbEMsVUFBVSxDQUFDLElBQUksQ0FBQztZQUNaLEtBQUssRUFBRSxJQUFJLENBQUMsUUFBUTtTQUN2QixDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsVUFBVTthQUNWLDRCQUE0QixDQUFDLFVBQVUsQ0FBQzthQUN4QyxTQUFTLENBQUMsVUFBQSxHQUFHO1lBQ1YsS0FBSSxDQUFDLHNCQUFzQixHQUFHLElBQUksQ0FBQztRQUN2QyxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFoQ0w7UUFBQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxvQkFBb0I7WUFDOUIsV0FBVyxFQUFFLCtCQUErQjtTQUMvQyxDQUFDOzs4QkFBQTtJQTZCRiw2QkFBQztBQUFELENBNUJBLEFBNEJDLElBQUE7QUE1QlksOEJBQXNCLHlCQTRCbEMsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvcHVibGljL3Jlc2V0LXBhc3N3b3JkL3Jlc2V0LXBhc3N3b3JkLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IEZvcm1Hcm91cCwgQWJzdHJhY3RDb250cm9sLCBGb3JtQnVpbGRlciwgVmFsaWRhdG9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgQXBpQ2xpZW50U2VydmljZSwgRW1haWxNb2RlbCB9IGZyb20gJy4uLy4uLy4uL2FwaS9pbmRleCc7XHJcbmltcG9ydCB7IEVtYWlsVmFsaWRhdG9yIH0gZnJvbSAnLi4vLi4vLi4vdmFsaWRhdG9ycy9pbmRleCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICBzZWxlY3RvcjogJ3Jlc2V0LXBhc3N3b3JkLWNtcCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJ3Jlc2V0LXBhc3N3b3JkLmNvbXBvbmVudC5odG1sJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgUmVzZXRQYXNzd29yZENvbXBvbmVudCB7XHJcblxyXG4gICAgcHVibGljIGZvcm06IEZvcm1Hcm91cDtcclxuICAgIHB1YmxpYyBlbWFpbDogQWJzdHJhY3RDb250cm9sO1xyXG4gICAgcHJpdmF0ZSBlbWFpbFN0cjogc3RyaW5nID0gbnVsbDtcclxuICAgIHByaXZhdGUgcmVzZXRQYXNzd29yZFN1Ym1pdHRlZDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgcHJpdmF0ZSByZXNldFBhc3N3b3JkQ29uZmlybWVkOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgY29uc3RydWN0b3IoZmI6IEZvcm1CdWlsZGVyLCBwcml2YXRlIF9hcGlDbGllbnQ6IEFwaUNsaWVudFNlcnZpY2UsIHByaXZhdGUgX3JvdXRlOiBBY3RpdmF0ZWRSb3V0ZSkge1xyXG4gICAgICAgIHRoaXMuZm9ybSA9IGZiLmdyb3VwKHtcclxuICAgICAgICAgICAgJ2VtYWlsJzogWycnLCBWYWxpZGF0b3JzLmNvbXBvc2UoW1ZhbGlkYXRvcnMucmVxdWlyZWQsIEVtYWlsVmFsaWRhdG9yLnZhbGlkYXRlXSldXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5lbWFpbCA9IHRoaXMuZm9ybS5jb250cm9sc1snZW1haWwnXTtcclxuICAgIH1cclxuXHJcbiAgICByZXNldFBhc3N3b3JkKCk6IHZvaWQge1xyXG5cclxuICAgICAgICB0aGlzLnJlc2V0UGFzc3dvcmRTdWJtaXR0ZWQgPSB0cnVlO1xyXG4gICAgICAgIGxldCBlbWFpbE1vZGVsID0gbmV3IEVtYWlsTW9kZWwoKTtcclxuICAgICAgICBlbWFpbE1vZGVsLmluaXQoe1xyXG4gICAgICAgICAgICBFbWFpbDogdGhpcy5lbWFpbFN0clxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuX2FwaUNsaWVudFxyXG4gICAgICAgICAgICAuYWNjb3VudF9SZXF1ZXN0UmVzZXRQYXNzd29yZChlbWFpbE1vZGVsKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKHJlcyA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJlc2V0UGFzc3dvcmRDb25maXJtZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG4iXX0=
