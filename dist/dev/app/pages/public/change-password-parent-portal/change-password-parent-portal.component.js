"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var router_1 = require('@angular/router');
var angular2_toaster_1 = require('angular2-toaster');
var index_1 = require('../../../shared/index');
var index_2 = require('../../../api/index');
var index_3 = require('../../../validators/index');
var ChangePasswordParentPortalComponent = (function () {
    function ChangePasswordParentPortalComponent(fb, _apiClient, _toasterService, _route) {
        this._apiClient = _apiClient;
        this._toasterService = _toasterService;
        this._route = _route;
        this.submitted = false;
        this.logo = null;
        this.centreName = null;
        this.token = null;
        this.passwordChanged = false;
        this.form = fb.group({
            'password': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(8), index_3.ComplexPasswordValidator.validate])],
            'confirmPassword': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(8)])]
        }, { validator: index_3.EqualPasswordsValidator.validate('password', 'confirmPassword') });
        this.password = this.form.controls['password'];
        this.confirmPassword = this.form.controls['confirmPassword'];
    }
    ChangePasswordParentPortalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route
            .params
            .subscribe(function (params) {
            var tokenParam = params['token'];
            _this.token = tokenParam === undefined ? '' : tokenParam;
            _this.centreName = params['centreName'];
            return _this.getLogo$(_this._route);
        });
    };
    ChangePasswordParentPortalComponent.prototype.getLogo$ = function (route) {
        var _this = this;
        route.data
            .subscribe(function (data) {
            if (data.centreLogo && data.centreLogo.imageBase64) {
                _this.logo = data.centreLogo.imageBase64;
            }
        });
    };
    ChangePasswordParentPortalComponent.prototype.onSubmit = function () {
        var _this = this;
        this.submitted = true;
        var savingToast = this._toasterService.pop(new index_1.SpinningToast());
        var changePwModel = new index_2.ChangePwdModel();
        changePwModel.init({
            NewPassword: this.password.value,
            ConfirmNewPassword: this.confirmPassword.value,
            Token: this.token
        });
        this._apiClient
            .account_ChangePassword(changePwModel)
            .subscribe(function (res) {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this.passwordChanged = true;
        }, function (err) { return _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId); }, function () {
            _this.submitted = false;
        });
    };
    ChangePasswordParentPortalComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'change-password-parent-portal-cmp',
            templateUrl: 'change-password-parent-portal.component.html'
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, index_2.ApiClientService, angular2_toaster_1.ToasterService, router_1.ActivatedRoute])
    ], ChangePasswordParentPortalComponent);
    return ChangePasswordParentPortalComponent;
}());
exports.ChangePasswordParentPortalComponent = ChangePasswordParentPortalComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wdWJsaWMvY2hhbmdlLXBhc3N3b3JkLXBhcmVudC1wb3J0YWwvY2hhbmdlLXBhc3N3b3JkLXBhcmVudC1wb3J0YWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBa0MsZUFBZSxDQUFDLENBQUE7QUFDbEQsc0JBQW9FLGdCQUFnQixDQUFDLENBQUE7QUFDckYsdUJBQXVDLGlCQUFpQixDQUFDLENBQUE7QUFFekQsaUNBQStCLGtCQUFrQixDQUFDLENBQUE7QUFDbEQsc0JBQThCLHVCQUF1QixDQUFDLENBQUE7QUFDdEQsc0JBQWdFLG9CQUFvQixDQUFDLENBQUE7QUFDckYsc0JBQWtFLDJCQUEyQixDQUFDLENBQUE7QUFPOUY7SUFZSSw2Q0FBWSxFQUFlLEVBQ2YsVUFBNEIsRUFDNUIsZUFBK0IsRUFDL0IsTUFBc0I7UUFGdEIsZUFBVSxHQUFWLFVBQVUsQ0FBa0I7UUFDNUIsb0JBQWUsR0FBZixlQUFlLENBQWdCO1FBQy9CLFdBQU0sR0FBTixNQUFNLENBQWdCO1FBVDFCLGNBQVMsR0FBWSxLQUFLLENBQUM7UUFDM0IsU0FBSSxHQUFXLElBQUksQ0FBQztRQUNwQixlQUFVLEdBQVcsSUFBSSxDQUFDO1FBQzFCLFVBQUssR0FBVyxJQUFJLENBQUM7UUFDckIsb0JBQWUsR0FBWSxLQUFLLENBQUM7UUFPckMsSUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDO1lBQ2pCLFVBQVUsRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLGtCQUFVLENBQUMsUUFBUSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFLGdDQUF3QixDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDdkgsaUJBQWlCLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDOUYsRUFBRSxFQUFFLFNBQVMsRUFBRSwrQkFBdUIsQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLGlCQUFpQixDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBRW5GLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDL0MsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0lBQ2pFLENBQUM7SUFFRCxzREFBUSxHQUFSO1FBQUEsaUJBV0M7UUFURyxJQUFJLENBQUMsTUFBTTthQUNOLE1BQU07YUFDTixTQUFTLENBQUMsVUFBQyxNQUFjO1lBQ3RCLElBQUksVUFBVSxHQUFXLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUN6QyxLQUFJLENBQUMsS0FBSyxHQUFHLFVBQVUsS0FBSyxTQUFTLEdBQUcsRUFBRSxHQUFHLFVBQVUsQ0FBQztZQUV4RCxLQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUN2QyxNQUFNLENBQUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDdEMsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRUQsc0RBQVEsR0FBUixVQUFTLEtBQXFCO1FBQTlCLGlCQU9DO1FBTkcsS0FBSyxDQUFDLElBQUk7YUFDTCxTQUFTLENBQUMsVUFBQyxJQUFtQztZQUMzQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztnQkFDakQsS0FBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQztZQUM1QyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRUQsc0RBQVEsR0FBUjtRQUFBLGlCQW9CQztRQW5CRyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUN0QixJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxJQUFJLHFCQUFhLEVBQUUsQ0FBQyxDQUFDO1FBQ2hFLElBQUksYUFBYSxHQUFHLElBQUksc0JBQWMsRUFBRSxDQUFDO1FBQ3pDLGFBQWEsQ0FBQyxJQUFJLENBQUM7WUFDZixXQUFXLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLO1lBQ2hDLGtCQUFrQixFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSztZQUM5QyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7U0FDcEIsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLFVBQVU7YUFDVixzQkFBc0IsQ0FBQyxhQUFhLENBQUM7YUFDckMsU0FBUyxDQUFDLFVBQUEsR0FBRztZQUNWLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDOUUsS0FBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7UUFDaEMsQ0FBQyxFQUNELFVBQUEsR0FBRyxJQUFJLE9BQUEsS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxXQUFXLENBQUMsZ0JBQWdCLENBQUMsRUFBN0UsQ0FBNkUsRUFDcEY7WUFFSSxLQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztRQUMzQixDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUF6RUw7UUFBQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxtQ0FBbUM7WUFDN0MsV0FBVyxFQUFFLDhDQUE4QztTQUM5RCxDQUFDOzsyQ0FBQTtJQXNFRiwwQ0FBQztBQUFELENBckVBLEFBcUVDLElBQUE7QUFyRVksMkNBQW1DLHNDQXFFL0MsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvcHVibGljL2NoYW5nZS1wYXNzd29yZC1wYXJlbnQtcG9ydGFsL2NoYW5nZS1wYXNzd29yZC1wYXJlbnQtcG9ydGFsLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1Hcm91cCwgQWJzdHJhY3RDb250cm9sLCBGb3JtQnVpbGRlciwgVmFsaWRhdG9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUsIFBhcmFtcyB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcblxyXG5pbXBvcnQgeyBUb2FzdGVyU2VydmljZSB9IGZyb20gJ2FuZ3VsYXIyLXRvYXN0ZXInO1xyXG5pbXBvcnQgeyBTcGlubmluZ1RvYXN0IH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL2luZGV4JztcclxuaW1wb3J0IHsgQXBpQ2xpZW50U2VydmljZSwgQ2hhbmdlUHdkTW9kZWwsIENlbnRyZUxvZ29EdG8gfSBmcm9tICcuLi8uLi8uLi9hcGkvaW5kZXgnO1xyXG5pbXBvcnQgeyBDb21wbGV4UGFzc3dvcmRWYWxpZGF0b3IsIEVxdWFsUGFzc3dvcmRzVmFsaWRhdG9yIH0gZnJvbSAnLi4vLi4vLi4vdmFsaWRhdG9ycy9pbmRleCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICBzZWxlY3RvcjogJ2NoYW5nZS1wYXNzd29yZC1wYXJlbnQtcG9ydGFsLWNtcCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJ2NoYW5nZS1wYXNzd29yZC1wYXJlbnQtcG9ydGFsLmNvbXBvbmVudC5odG1sJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgQ2hhbmdlUGFzc3dvcmRQYXJlbnRQb3J0YWxDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIHB1YmxpYyBmb3JtOiBGb3JtR3JvdXA7XHJcbiAgICBwdWJsaWMgcGFzc3dvcmQ6IEFic3RyYWN0Q29udHJvbDtcclxuICAgIHB1YmxpYyBjb25maXJtUGFzc3dvcmQ6IEFic3RyYWN0Q29udHJvbDtcclxuXHJcbiAgICBwcml2YXRlIHN1Ym1pdHRlZDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgcHJpdmF0ZSBsb2dvOiBzdHJpbmcgPSBudWxsO1xyXG4gICAgcHJpdmF0ZSBjZW50cmVOYW1lOiBzdHJpbmcgPSBudWxsO1xyXG4gICAgcHJpdmF0ZSB0b2tlbjogc3RyaW5nID0gbnVsbDtcclxuICAgIHByaXZhdGUgcGFzc3dvcmRDaGFuZ2VkOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgY29uc3RydWN0b3IoZmI6IEZvcm1CdWlsZGVyLFxyXG4gICAgICAgIHByaXZhdGUgX2FwaUNsaWVudDogQXBpQ2xpZW50U2VydmljZSxcclxuICAgICAgICBwcml2YXRlIF90b2FzdGVyU2VydmljZTogVG9hc3RlclNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBfcm91dGU6IEFjdGl2YXRlZFJvdXRlKSB7XHJcblxyXG4gICAgICAgIHRoaXMuZm9ybSA9IGZiLmdyb3VwKHtcclxuICAgICAgICAgICAgJ3Bhc3N3b3JkJzogWycnLCBWYWxpZGF0b3JzLmNvbXBvc2UoW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWluTGVuZ3RoKDgpLCBDb21wbGV4UGFzc3dvcmRWYWxpZGF0b3IudmFsaWRhdGVdKV0sXHJcbiAgICAgICAgICAgICdjb25maXJtUGFzc3dvcmQnOiBbJycsIFZhbGlkYXRvcnMuY29tcG9zZShbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5taW5MZW5ndGgoOCldKV1cclxuICAgICAgICB9LCB7IHZhbGlkYXRvcjogRXF1YWxQYXNzd29yZHNWYWxpZGF0b3IudmFsaWRhdGUoJ3Bhc3N3b3JkJywgJ2NvbmZpcm1QYXNzd29yZCcpIH0pO1xyXG5cclxuICAgICAgICB0aGlzLnBhc3N3b3JkID0gdGhpcy5mb3JtLmNvbnRyb2xzWydwYXNzd29yZCddO1xyXG4gICAgICAgIHRoaXMuY29uZmlybVBhc3N3b3JkID0gdGhpcy5mb3JtLmNvbnRyb2xzWydjb25maXJtUGFzc3dvcmQnXTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuXHJcbiAgICAgICAgdGhpcy5fcm91dGVcclxuICAgICAgICAgICAgLnBhcmFtc1xyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKChwYXJhbXM6IFBhcmFtcykgPT4ge1xyXG4gICAgICAgICAgICAgICAgbGV0IHRva2VuUGFyYW06IHN0cmluZyA9IHBhcmFtc1sndG9rZW4nXTtcclxuICAgICAgICAgICAgICAgIHRoaXMudG9rZW4gPSB0b2tlblBhcmFtID09PSB1bmRlZmluZWQgPyAnJyA6IHRva2VuUGFyYW07XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5jZW50cmVOYW1lID0gcGFyYW1zWydjZW50cmVOYW1lJ107XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5nZXRMb2dvJCh0aGlzLl9yb3V0ZSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldExvZ28kKHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSkge1xyXG4gICAgICAgIHJvdXRlLmRhdGFcclxuICAgICAgICAgICAgLnN1YnNjcmliZSgoZGF0YTogeyBjZW50cmVMb2dvOiBDZW50cmVMb2dvRHRvIH0pID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChkYXRhLmNlbnRyZUxvZ28gJiYgZGF0YS5jZW50cmVMb2dvLmltYWdlQmFzZTY0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2dvID0gZGF0YS5jZW50cmVMb2dvLmltYWdlQmFzZTY0O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBvblN1Ym1pdCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLnN1Ym1pdHRlZCA9IHRydWU7XHJcbiAgICAgICAgbGV0IHNhdmluZ1RvYXN0ID0gdGhpcy5fdG9hc3RlclNlcnZpY2UucG9wKG5ldyBTcGlubmluZ1RvYXN0KCkpO1xyXG4gICAgICAgIGxldCBjaGFuZ2VQd01vZGVsID0gbmV3IENoYW5nZVB3ZE1vZGVsKCk7XHJcbiAgICAgICAgY2hhbmdlUHdNb2RlbC5pbml0KHtcclxuICAgICAgICAgICAgTmV3UGFzc3dvcmQ6IHRoaXMucGFzc3dvcmQudmFsdWUsXHJcbiAgICAgICAgICAgIENvbmZpcm1OZXdQYXNzd29yZDogdGhpcy5jb25maXJtUGFzc3dvcmQudmFsdWUsXHJcbiAgICAgICAgICAgIFRva2VuOiB0aGlzLnRva2VuXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5fYXBpQ2xpZW50XHJcbiAgICAgICAgICAgIC5hY2NvdW50X0NoYW5nZVBhc3N3b3JkKGNoYW5nZVB3TW9kZWwpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLmNsZWFyKHNhdmluZ1RvYXN0LnRvYXN0SWQsIHNhdmluZ1RvYXN0LnRvYXN0Q29udGFpbmVySWQpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wYXNzd29yZENoYW5nZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBlcnIgPT4gdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoc2F2aW5nVG9hc3QudG9hc3RJZCwgc2F2aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCksXHJcbiAgICAgICAgICAgICgpID0+IHtcclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLnN1Ym1pdHRlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG4iXX0=
