"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../shared/index');
var index_2 = require('./index');
var ChangePasswordParentPortalModule = (function () {
    function ChangePasswordParentPortalModule() {
    }
    ChangePasswordParentPortalModule = __decorate([
        core_1.NgModule({
            imports: [index_1.SharedModule],
            declarations: [index_2.ChangePasswordParentPortalComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], ChangePasswordParentPortalModule);
    return ChangePasswordParentPortalModule;
}());
exports.ChangePasswordParentPortalModule = ChangePasswordParentPortalModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wdWJsaWMvY2hhbmdlLXBhc3N3b3JkLXBhcmVudC1wb3J0YWwvY2hhbmdlLXBhc3N3b3JkLXBhcmVudC1wb3J0YWwubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBeUIsZUFBZSxDQUFDLENBQUE7QUFDekMsc0JBQTZCLHVCQUF1QixDQUFDLENBQUE7QUFFckQsc0JBQW9ELFNBQVMsQ0FBQyxDQUFBO0FBTzlEO0lBQUE7SUFBZ0QsQ0FBQztJQUxqRDtRQUFDLGVBQVEsQ0FBQztZQUNOLE9BQU8sRUFBRSxDQUFDLG9CQUFZLENBQUM7WUFDdkIsWUFBWSxFQUFFLENBQUMsMkNBQW1DLENBQUM7U0FDdEQsQ0FBQzs7d0NBQUE7SUFFOEMsdUNBQUM7QUFBRCxDQUFoRCxBQUFpRCxJQUFBO0FBQXBDLHdDQUFnQyxtQ0FBSSxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9wdWJsaWMvY2hhbmdlLXBhc3N3b3JkLXBhcmVudC1wb3J0YWwvY2hhbmdlLXBhc3N3b3JkLXBhcmVudC1wb3J0YWwubW9kdWxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU2hhcmVkTW9kdWxlIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL2luZGV4JztcclxuXHJcbmltcG9ydCB7IENoYW5nZVBhc3N3b3JkUGFyZW50UG9ydGFsQ29tcG9uZW50IH0gZnJvbSAnLi9pbmRleCc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgaW1wb3J0czogW1NoYXJlZE1vZHVsZV0sXHJcbiAgICBkZWNsYXJhdGlvbnM6IFtDaGFuZ2VQYXNzd29yZFBhcmVudFBvcnRhbENvbXBvbmVudF1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBDaGFuZ2VQYXNzd29yZFBhcmVudFBvcnRhbE1vZHVsZSB7IH1cclxuIl19
