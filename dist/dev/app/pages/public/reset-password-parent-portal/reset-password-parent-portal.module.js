"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../shared/index');
var index_2 = require('./index');
var ResetPasswordParentPortalModule = (function () {
    function ResetPasswordParentPortalModule() {
    }
    ResetPasswordParentPortalModule = __decorate([
        core_1.NgModule({
            imports: [index_1.SharedModule],
            declarations: [index_2.ResetPasswordParentPortalComponent],
            providers: []
        }), 
        __metadata('design:paramtypes', [])
    ], ResetPasswordParentPortalModule);
    return ResetPasswordParentPortalModule;
}());
exports.ResetPasswordParentPortalModule = ResetPasswordParentPortalModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wdWJsaWMvcmVzZXQtcGFzc3dvcmQtcGFyZW50LXBvcnRhbC9yZXNldC1wYXNzd29yZC1wYXJlbnQtcG9ydGFsLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQXlCLGVBQWUsQ0FBQyxDQUFBO0FBQ3pDLHNCQUE2Qix1QkFBdUIsQ0FBQyxDQUFBO0FBRXJELHNCQUFtRCxTQUFTLENBQUMsQ0FBQTtBQVE3RDtJQUFBO0lBQStDLENBQUM7SUFOaEQ7UUFBQyxlQUFRLENBQUM7WUFDTixPQUFPLEVBQUUsQ0FBQyxvQkFBWSxDQUFDO1lBQ3ZCLFlBQVksRUFBRSxDQUFDLDBDQUFrQyxDQUFDO1lBQ2xELFNBQVMsRUFBRSxFQUFFO1NBQ2hCLENBQUM7O3VDQUFBO0lBRTZDLHNDQUFDO0FBQUQsQ0FBL0MsQUFBZ0QsSUFBQTtBQUFuQyx1Q0FBK0Isa0NBQUksQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvcHVibGljL3Jlc2V0LXBhc3N3b3JkLXBhcmVudC1wb3J0YWwvcmVzZXQtcGFzc3dvcmQtcGFyZW50LXBvcnRhbC5tb2R1bGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTaGFyZWRNb2R1bGUgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvaW5kZXgnO1xyXG5cclxuaW1wb3J0IHsgUmVzZXRQYXNzd29yZFBhcmVudFBvcnRhbENvbXBvbmVudCB9IGZyb20gJy4vaW5kZXgnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6IFtTaGFyZWRNb2R1bGVdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBbUmVzZXRQYXNzd29yZFBhcmVudFBvcnRhbENvbXBvbmVudF0sXHJcbiAgICBwcm92aWRlcnM6IFtdXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgUmVzZXRQYXNzd29yZFBhcmVudFBvcnRhbE1vZHVsZSB7IH1cclxuIl19
