"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var forms_1 = require('@angular/forms');
var index_1 = require('../../../api/index');
var index_2 = require('../../../validators/index');
var ResetPasswordParentPortalComponent = (function () {
    function ResetPasswordParentPortalComponent(fb, _apiClient, _route) {
        this._apiClient = _apiClient;
        this._route = _route;
        this.logo = null;
        this.centreName = null;
        this.emailStr = null;
        this.resetPasswordSubmitted = false;
        this.resetPasswordConfirmed = false;
        this.form = fb.group({
            'email': ['', forms_1.Validators.compose([forms_1.Validators.required, index_2.EmailValidator.validate])]
        });
        this.email = this.form.controls['email'];
    }
    ResetPasswordParentPortalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route
            .params
            .subscribe(function (params) {
            _this.centreName = params['centreName'];
            return _this.getLogo$(_this._route);
        });
    };
    ResetPasswordParentPortalComponent.prototype.getLogo$ = function (route) {
        var _this = this;
        route.data
            .subscribe(function (data) {
            if (data.centreLogo && data.centreLogo.imageBase64) {
                _this.logo = data.centreLogo.imageBase64;
            }
        });
    };
    ResetPasswordParentPortalComponent.prototype.resetPassword = function () {
        var _this = this;
        this.resetPasswordSubmitted = true;
        var emailModel = new index_1.EmailModel();
        emailModel.init({
            Email: this.emailStr
        });
        this._apiClient
            .account_RequestResetPasswordParentPortal(emailModel, this.centreName)
            .subscribe(function (res) {
            _this.resetPasswordConfirmed = true;
        });
    };
    ResetPasswordParentPortalComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'reset-password-parent-portal-cmp',
            templateUrl: 'reset-password-parent-portal.component.html'
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, index_1.ApiClientService, router_1.ActivatedRoute])
    ], ResetPasswordParentPortalComponent);
    return ResetPasswordParentPortalComponent;
}());
exports.ResetPasswordParentPortalComponent = ResetPasswordParentPortalComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wdWJsaWMvcmVzZXQtcGFzc3dvcmQtcGFyZW50LXBvcnRhbC9yZXNldC1wYXNzd29yZC1wYXJlbnQtcG9ydGFsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQWtDLGVBQWUsQ0FBQyxDQUFBO0FBQ2xELHVCQUF1QyxpQkFBaUIsQ0FBQyxDQUFBO0FBQ3pELHNCQUFvRSxnQkFBZ0IsQ0FBQyxDQUFBO0FBQ3JGLHNCQUE0RCxvQkFBb0IsQ0FBQyxDQUFBO0FBQ2pGLHNCQUErQiwyQkFBMkIsQ0FBQyxDQUFBO0FBTzNEO0lBVUksNENBQVksRUFBZSxFQUFVLFVBQTRCLEVBQVUsTUFBc0I7UUFBNUQsZUFBVSxHQUFWLFVBQVUsQ0FBa0I7UUFBVSxXQUFNLEdBQU4sTUFBTSxDQUFnQjtRQU56RixTQUFJLEdBQVcsSUFBSSxDQUFDO1FBQ3BCLGVBQVUsR0FBVyxJQUFJLENBQUM7UUFDMUIsYUFBUSxHQUFXLElBQUksQ0FBQztRQUN4QiwyQkFBc0IsR0FBWSxLQUFLLENBQUM7UUFDeEMsMkJBQXNCLEdBQVksS0FBSyxDQUFDO1FBRzVDLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQztZQUNqQixPQUFPLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsRUFBRSxzQkFBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7U0FDcEYsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUM3QyxDQUFDO0lBRUQscURBQVEsR0FBUjtRQUFBLGlCQVFDO1FBTkcsSUFBSSxDQUFDLE1BQU07YUFDTixNQUFNO2FBQ04sU0FBUyxDQUFDLFVBQUMsTUFBYztZQUN0QixLQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUN2QyxNQUFNLENBQUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDdEMsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRUQscURBQVEsR0FBUixVQUFTLEtBQXFCO1FBQTlCLGlCQU9DO1FBTkcsS0FBSyxDQUFDLElBQUk7YUFDTCxTQUFTLENBQUMsVUFBQyxJQUFtQztZQUMzQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztnQkFDakQsS0FBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQztZQUM1QyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRUQsMERBQWEsR0FBYjtRQUFBLGlCQVlDO1FBVkcsSUFBSSxDQUFDLHNCQUFzQixHQUFHLElBQUksQ0FBQztRQUNuQyxJQUFJLFVBQVUsR0FBRyxJQUFJLGtCQUFVLEVBQUUsQ0FBQztRQUNsQyxVQUFVLENBQUMsSUFBSSxDQUFDO1lBQ1osS0FBSyxFQUFFLElBQUksQ0FBQyxRQUFRO1NBQ3ZCLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxVQUFVO2FBQ1Ysd0NBQXdDLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUM7YUFDckUsU0FBUyxDQUFDLFVBQUEsR0FBRztZQUNWLEtBQUksQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUM7UUFDdkMsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBckRMO1FBQUMsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsa0NBQWtDO1lBQzVDLFdBQVcsRUFBRSw2Q0FBNkM7U0FDN0QsQ0FBQzs7MENBQUE7SUFrREYseUNBQUM7QUFBRCxDQWpEQSxBQWlEQyxJQUFBO0FBakRZLDBDQUFrQyxxQ0FpRDlDLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL3B1YmxpYy9yZXNldC1wYXNzd29yZC1wYXJlbnQtcG9ydGFsL3Jlc2V0LXBhc3N3b3JkLXBhcmVudC1wb3J0YWwuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUsIFBhcmFtcyB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IEZvcm1Hcm91cCwgQWJzdHJhY3RDb250cm9sLCBGb3JtQnVpbGRlciwgVmFsaWRhdG9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgQXBpQ2xpZW50U2VydmljZSwgRW1haWxNb2RlbCwgQ2VudHJlTG9nb0R0byB9IGZyb20gJy4uLy4uLy4uL2FwaS9pbmRleCc7XHJcbmltcG9ydCB7IEVtYWlsVmFsaWRhdG9yIH0gZnJvbSAnLi4vLi4vLi4vdmFsaWRhdG9ycy9pbmRleCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICBzZWxlY3RvcjogJ3Jlc2V0LXBhc3N3b3JkLXBhcmVudC1wb3J0YWwtY21wJyxcclxuICAgIHRlbXBsYXRlVXJsOiAncmVzZXQtcGFzc3dvcmQtcGFyZW50LXBvcnRhbC5jb21wb25lbnQuaHRtbCdcclxufSlcclxuZXhwb3J0IGNsYXNzIFJlc2V0UGFzc3dvcmRQYXJlbnRQb3J0YWxDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIHB1YmxpYyBmb3JtOiBGb3JtR3JvdXA7XHJcbiAgICBwdWJsaWMgZW1haWw6IEFic3RyYWN0Q29udHJvbDtcclxuICAgIHByaXZhdGUgbG9nbzogc3RyaW5nID0gbnVsbDtcclxuICAgIHByaXZhdGUgY2VudHJlTmFtZTogc3RyaW5nID0gbnVsbDtcclxuICAgIHByaXZhdGUgZW1haWxTdHI6IHN0cmluZyA9IG51bGw7XHJcbiAgICBwcml2YXRlIHJlc2V0UGFzc3dvcmRTdWJtaXR0ZWQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIHByaXZhdGUgcmVzZXRQYXNzd29yZENvbmZpcm1lZDogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGZiOiBGb3JtQnVpbGRlciwgcHJpdmF0ZSBfYXBpQ2xpZW50OiBBcGlDbGllbnRTZXJ2aWNlLCBwcml2YXRlIF9yb3V0ZTogQWN0aXZhdGVkUm91dGUpIHtcclxuICAgICAgICB0aGlzLmZvcm0gPSBmYi5ncm91cCh7XHJcbiAgICAgICAgICAgICdlbWFpbCc6IFsnJywgVmFsaWRhdG9ycy5jb21wb3NlKFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBFbWFpbFZhbGlkYXRvci52YWxpZGF0ZV0pXVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuZW1haWwgPSB0aGlzLmZvcm0uY29udHJvbHNbJ2VtYWlsJ107XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcblxyXG4gICAgICAgIHRoaXMuX3JvdXRlXHJcbiAgICAgICAgICAgIC5wYXJhbXNcclxuICAgICAgICAgICAgLnN1YnNjcmliZSgocGFyYW1zOiBQYXJhbXMpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY2VudHJlTmFtZSA9IHBhcmFtc1snY2VudHJlTmFtZSddO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuZ2V0TG9nbyQodGhpcy5fcm91dGUpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRMb2dvJChyb3V0ZTogQWN0aXZhdGVkUm91dGUpIHtcclxuICAgICAgICByb3V0ZS5kYXRhXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoKGRhdGE6IHsgY2VudHJlTG9nbzogQ2VudHJlTG9nb0R0byB9KSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZGF0YS5jZW50cmVMb2dvICYmIGRhdGEuY2VudHJlTG9nby5pbWFnZUJhc2U2NCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9nbyA9IGRhdGEuY2VudHJlTG9nby5pbWFnZUJhc2U2NDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVzZXRQYXNzd29yZCgpOiB2b2lkIHtcclxuXHJcbiAgICAgICAgdGhpcy5yZXNldFBhc3N3b3JkU3VibWl0dGVkID0gdHJ1ZTtcclxuICAgICAgICBsZXQgZW1haWxNb2RlbCA9IG5ldyBFbWFpbE1vZGVsKCk7XHJcbiAgICAgICAgZW1haWxNb2RlbC5pbml0KHtcclxuICAgICAgICAgICAgRW1haWw6IHRoaXMuZW1haWxTdHJcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLl9hcGlDbGllbnRcclxuICAgICAgICAgICAgLmFjY291bnRfUmVxdWVzdFJlc2V0UGFzc3dvcmRQYXJlbnRQb3J0YWwoZW1haWxNb2RlbCwgdGhpcy5jZW50cmVOYW1lKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKHJlcyA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJlc2V0UGFzc3dvcmRDb25maXJtZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG4iXX0=
