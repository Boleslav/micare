"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var router_1 = require('@angular/router');
var angular2_toaster_1 = require('angular2-toaster');
var index_1 = require('../../../shared/index');
var index_2 = require('../../../api/index');
var index_3 = require('../../../validators/index');
var ChangePasswordComponent = (function () {
    function ChangePasswordComponent(fb, _apiClient, _toasterService, _route) {
        this._apiClient = _apiClient;
        this._toasterService = _toasterService;
        this._route = _route;
        this.submitted = false;
        this.token = null;
        this.passwordChanged = false;
        this.form = fb.group({
            'password': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(8), index_3.ComplexPasswordValidator.validate])],
            'confirmPassword': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(8)])]
        }, { validator: index_3.EqualPasswordsValidator.validate('password', 'confirmPassword') });
        this.password = this.form.controls['password'];
        this.confirmPassword = this.form.controls['confirmPassword'];
    }
    ChangePasswordComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route
            .params
            .subscribe(function (params) {
            var tokenParam = params['token'];
            _this.token = tokenParam === undefined ? '' : tokenParam;
        });
    };
    ChangePasswordComponent.prototype.onSubmit = function () {
        var _this = this;
        this.submitted = true;
        var savingToast = this._toasterService.pop(new index_1.SpinningToast());
        var changePwModel = new index_2.ChangePwdModel();
        changePwModel.init({
            NewPassword: this.password.value,
            ConfirmNewPassword: this.confirmPassword.value,
            Token: this.token
        });
        this._apiClient
            .account_ChangePassword(changePwModel)
            .subscribe(function (res) {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this.passwordChanged = true;
        }, function (err) { return _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId); }, function () {
            _this.submitted = false;
        });
    };
    ChangePasswordComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'change-password-cmp',
            templateUrl: 'change-password.component.html'
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, index_2.ApiClientService, angular2_toaster_1.ToasterService, router_1.ActivatedRoute])
    ], ChangePasswordComponent);
    return ChangePasswordComponent;
}());
exports.ChangePasswordComponent = ChangePasswordComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wdWJsaWMvY2hhbmdlLXBhc3N3b3JkL2NoYW5nZS1wYXNzd29yZC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUFrQyxlQUFlLENBQUMsQ0FBQTtBQUNsRCxzQkFBb0UsZ0JBQWdCLENBQUMsQ0FBQTtBQUNyRix1QkFBdUMsaUJBQWlCLENBQUMsQ0FBQTtBQUV6RCxpQ0FBK0Isa0JBQWtCLENBQUMsQ0FBQTtBQUNsRCxzQkFBOEIsdUJBQXVCLENBQUMsQ0FBQTtBQUN0RCxzQkFBaUQsb0JBQW9CLENBQUMsQ0FBQTtBQUN0RSxzQkFBa0UsMkJBQTJCLENBQUMsQ0FBQTtBQU85RjtJQVVJLGlDQUFZLEVBQWUsRUFDZixVQUE0QixFQUM1QixlQUErQixFQUMvQixNQUFzQjtRQUZ0QixlQUFVLEdBQVYsVUFBVSxDQUFrQjtRQUM1QixvQkFBZSxHQUFmLGVBQWUsQ0FBZ0I7UUFDL0IsV0FBTSxHQUFOLE1BQU0sQ0FBZ0I7UUFQMUIsY0FBUyxHQUFZLEtBQUssQ0FBQztRQUMzQixVQUFLLEdBQVcsSUFBSSxDQUFDO1FBQ3JCLG9CQUFlLEdBQVksS0FBSyxDQUFDO1FBT3JDLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQztZQUNqQixVQUFVLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxnQ0FBd0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ3ZILGlCQUFpQixFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsa0JBQVUsQ0FBQyxRQUFRLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQzlGLEVBQUUsRUFBRSxTQUFTLEVBQUUsK0JBQXVCLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSxpQkFBaUIsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUVuRixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQy9DLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMsQ0FBQztJQUNqRSxDQUFDO0lBRUQsMENBQVEsR0FBUjtRQUFBLGlCQVFDO1FBTkcsSUFBSSxDQUFDLE1BQU07YUFDTixNQUFNO2FBQ04sU0FBUyxDQUFDLFVBQUMsTUFBYztZQUN0QixJQUFJLFVBQVUsR0FBVyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDekMsS0FBSSxDQUFDLEtBQUssR0FBRyxVQUFVLEtBQUssU0FBUyxHQUFHLEVBQUUsR0FBRyxVQUFVLENBQUM7UUFDNUQsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRUQsMENBQVEsR0FBUjtRQUFBLGlCQW9CQztRQW5CRyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUN0QixJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxJQUFJLHFCQUFhLEVBQUUsQ0FBQyxDQUFDO1FBQ2hFLElBQUksYUFBYSxHQUFHLElBQUksc0JBQWMsRUFBRSxDQUFDO1FBQ3pDLGFBQWEsQ0FBQyxJQUFJLENBQUM7WUFDZixXQUFXLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLO1lBQ2hDLGtCQUFrQixFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSztZQUM5QyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7U0FDcEIsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLFVBQVU7YUFDVixzQkFBc0IsQ0FBQyxhQUFhLENBQUM7YUFDckMsU0FBUyxDQUFDLFVBQUEsR0FBRztZQUNWLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDOUUsS0FBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7UUFDaEMsQ0FBQyxFQUNELFVBQUEsR0FBRyxJQUFJLE9BQUEsS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxXQUFXLENBQUMsZ0JBQWdCLENBQUMsRUFBN0UsQ0FBNkUsRUFDcEY7WUFFSSxLQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztRQUMzQixDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUEzREw7UUFBQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxxQkFBcUI7WUFDL0IsV0FBVyxFQUFFLGdDQUFnQztTQUNoRCxDQUFDOzsrQkFBQTtJQXdERiw4QkFBQztBQUFELENBdkRBLEFBdURDLElBQUE7QUF2RFksK0JBQXVCLDBCQXVEbkMsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvcHVibGljL2NoYW5nZS1wYXNzd29yZC9jaGFuZ2UtcGFzc3dvcmQuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybUdyb3VwLCBBYnN0cmFjdENvbnRyb2wsIEZvcm1CdWlsZGVyLCBWYWxpZGF0b3JzIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZSwgUGFyYW1zIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuXHJcbmltcG9ydCB7IFRvYXN0ZXJTZXJ2aWNlIH0gZnJvbSAnYW5ndWxhcjItdG9hc3Rlcic7XHJcbmltcG9ydCB7IFNwaW5uaW5nVG9hc3QgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvaW5kZXgnO1xyXG5pbXBvcnQgeyBBcGlDbGllbnRTZXJ2aWNlLCBDaGFuZ2VQd2RNb2RlbCB9IGZyb20gJy4uLy4uLy4uL2FwaS9pbmRleCc7XHJcbmltcG9ydCB7IENvbXBsZXhQYXNzd29yZFZhbGlkYXRvciwgRXF1YWxQYXNzd29yZHNWYWxpZGF0b3IgfSBmcm9tICcuLi8uLi8uLi92YWxpZGF0b3JzL2luZGV4JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHNlbGVjdG9yOiAnY2hhbmdlLXBhc3N3b3JkLWNtcCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJ2NoYW5nZS1wYXNzd29yZC5jb21wb25lbnQuaHRtbCdcclxufSlcclxuZXhwb3J0IGNsYXNzIENoYW5nZVBhc3N3b3JkQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBwdWJsaWMgZm9ybTogRm9ybUdyb3VwO1xyXG4gICAgcHVibGljIHBhc3N3b3JkOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgICBwdWJsaWMgY29uZmlybVBhc3N3b3JkOiBBYnN0cmFjdENvbnRyb2w7XHJcblxyXG4gICAgcHJpdmF0ZSBzdWJtaXR0ZWQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIHByaXZhdGUgdG9rZW46IHN0cmluZyA9IG51bGw7XHJcbiAgICBwcml2YXRlIHBhc3N3b3JkQ2hhbmdlZDogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGZiOiBGb3JtQnVpbGRlcixcclxuICAgICAgICBwcml2YXRlIF9hcGlDbGllbnQ6IEFwaUNsaWVudFNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBfdG9hc3RlclNlcnZpY2U6IFRvYXN0ZXJTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgX3JvdXRlOiBBY3RpdmF0ZWRSb3V0ZSkge1xyXG5cclxuICAgICAgICB0aGlzLmZvcm0gPSBmYi5ncm91cCh7XHJcbiAgICAgICAgICAgICdwYXNzd29yZCc6IFsnJywgVmFsaWRhdG9ycy5jb21wb3NlKFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1pbkxlbmd0aCg4KSwgQ29tcGxleFBhc3N3b3JkVmFsaWRhdG9yLnZhbGlkYXRlXSldLFxyXG4gICAgICAgICAgICAnY29uZmlybVBhc3N3b3JkJzogWycnLCBWYWxpZGF0b3JzLmNvbXBvc2UoW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWluTGVuZ3RoKDgpXSldXHJcbiAgICAgICAgfSwgeyB2YWxpZGF0b3I6IEVxdWFsUGFzc3dvcmRzVmFsaWRhdG9yLnZhbGlkYXRlKCdwYXNzd29yZCcsICdjb25maXJtUGFzc3dvcmQnKSB9KTtcclxuXHJcbiAgICAgICAgdGhpcy5wYXNzd29yZCA9IHRoaXMuZm9ybS5jb250cm9sc1sncGFzc3dvcmQnXTtcclxuICAgICAgICB0aGlzLmNvbmZpcm1QYXNzd29yZCA9IHRoaXMuZm9ybS5jb250cm9sc1snY29uZmlybVBhc3N3b3JkJ107XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcblxyXG4gICAgICAgIHRoaXMuX3JvdXRlXHJcbiAgICAgICAgICAgIC5wYXJhbXNcclxuICAgICAgICAgICAgLnN1YnNjcmliZSgocGFyYW1zOiBQYXJhbXMpID0+IHtcclxuICAgICAgICAgICAgICAgIGxldCB0b2tlblBhcmFtOiBzdHJpbmcgPSBwYXJhbXNbJ3Rva2VuJ107XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRva2VuID0gdG9rZW5QYXJhbSA9PT0gdW5kZWZpbmVkID8gJycgOiB0b2tlblBhcmFtO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBvblN1Ym1pdCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLnN1Ym1pdHRlZCA9IHRydWU7XHJcbiAgICAgICAgbGV0IHNhdmluZ1RvYXN0ID0gdGhpcy5fdG9hc3RlclNlcnZpY2UucG9wKG5ldyBTcGlubmluZ1RvYXN0KCkpO1xyXG4gICAgICAgIGxldCBjaGFuZ2VQd01vZGVsID0gbmV3IENoYW5nZVB3ZE1vZGVsKCk7XHJcbiAgICAgICAgY2hhbmdlUHdNb2RlbC5pbml0KHtcclxuICAgICAgICAgICAgTmV3UGFzc3dvcmQ6IHRoaXMucGFzc3dvcmQudmFsdWUsXHJcbiAgICAgICAgICAgIENvbmZpcm1OZXdQYXNzd29yZDogdGhpcy5jb25maXJtUGFzc3dvcmQudmFsdWUsXHJcbiAgICAgICAgICAgIFRva2VuOiB0aGlzLnRva2VuXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5fYXBpQ2xpZW50XHJcbiAgICAgICAgICAgIC5hY2NvdW50X0NoYW5nZVBhc3N3b3JkKGNoYW5nZVB3TW9kZWwpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLmNsZWFyKHNhdmluZ1RvYXN0LnRvYXN0SWQsIHNhdmluZ1RvYXN0LnRvYXN0Q29udGFpbmVySWQpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wYXNzd29yZENoYW5nZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBlcnIgPT4gdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoc2F2aW5nVG9hc3QudG9hc3RJZCwgc2F2aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCksXHJcbiAgICAgICAgICAgICgpID0+IHtcclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLnN1Ym1pdHRlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG4iXX0=
