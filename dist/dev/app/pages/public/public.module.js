"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../shared/index');
var public_component_1 = require('./public.component');
var index_2 = require('./change-password/index');
var index_3 = require('./change-password-parent-portal/index');
var index_4 = require('./reset-password/index');
var index_5 = require('./reset-password-parent-portal/index');
var index_6 = require('./verify-account-client-admin/index');
var index_7 = require('./verify-account-parent/index');
var index_8 = require('./verify-account-parent-portal/index');
var index_9 = require('./components/index');
var PublicModule = (function () {
    function PublicModule() {
    }
    PublicModule = __decorate([
        core_1.NgModule({
            imports: [
                index_2.ChangePasswordModule,
                index_3.ChangePasswordParentPortalModule,
                index_4.ResetPasswordModule,
                index_5.ResetPasswordParentPortalModule,
                index_6.VerifyAccountClientAdminModule,
                index_7.VerifyAccountParentModule,
                index_8.VerifyAccountParentPortalModule,
                index_1.SharedModule
            ],
            providers: [index_9.CentreLogoResolver, index_9.CentreService],
            declarations: [
                public_component_1.PublicComponent
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], PublicModule);
    return PublicModule;
}());
exports.PublicModule = PublicModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wdWJsaWMvcHVibGljLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQXlCLGVBQWUsQ0FBQyxDQUFBO0FBQ3pDLHNCQUE2QixvQkFBb0IsQ0FBQyxDQUFBO0FBRWxELGlDQUFnQyxvQkFBb0IsQ0FBQyxDQUFBO0FBQ3JELHNCQUFxQyx5QkFBeUIsQ0FBQyxDQUFBO0FBQy9ELHNCQUFpRCx1Q0FBdUMsQ0FBQyxDQUFBO0FBQ3pGLHNCQUFvQyx3QkFBd0IsQ0FBQyxDQUFBO0FBQzdELHNCQUFnRCxzQ0FBc0MsQ0FBQyxDQUFBO0FBQ3ZGLHNCQUErQyxxQ0FBcUMsQ0FBQyxDQUFBO0FBQ3JGLHNCQUEwQywrQkFBK0IsQ0FBQyxDQUFBO0FBQzFFLHNCQUFnRCxzQ0FBc0MsQ0FBQyxDQUFBO0FBQ3ZGLHNCQUFrRCxvQkFBb0IsQ0FBQyxDQUFBO0FBa0J2RTtJQUFBO0lBQTRCLENBQUM7SUFoQjdCO1FBQUMsZUFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFO2dCQUNMLDRCQUFvQjtnQkFDcEIsd0NBQWdDO2dCQUNoQywyQkFBbUI7Z0JBQ25CLHVDQUErQjtnQkFDL0Isc0NBQThCO2dCQUM5QixpQ0FBeUI7Z0JBQ3pCLHVDQUErQjtnQkFDL0Isb0JBQVk7YUFDZjtZQUNELFNBQVMsRUFBRSxDQUFDLDBCQUFrQixFQUFFLHFCQUFhLENBQUM7WUFDOUMsWUFBWSxFQUFFO2dCQUNWLGtDQUFlO2FBQ2xCO1NBQ0osQ0FBQzs7b0JBQUE7SUFDMEIsbUJBQUM7QUFBRCxDQUE1QixBQUE2QixJQUFBO0FBQWhCLG9CQUFZLGVBQUksQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvcHVibGljL3B1YmxpYy5tb2R1bGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTaGFyZWRNb2R1bGUgfSBmcm9tICcuLi8uLi9zaGFyZWQvaW5kZXgnO1xyXG5cclxuaW1wb3J0IHsgUHVibGljQ29tcG9uZW50IH0gZnJvbSAnLi9wdWJsaWMuY29tcG9uZW50JztcclxuaW1wb3J0IHsgQ2hhbmdlUGFzc3dvcmRNb2R1bGUgfSBmcm9tICcuL2NoYW5nZS1wYXNzd29yZC9pbmRleCc7XHJcbmltcG9ydCB7IENoYW5nZVBhc3N3b3JkUGFyZW50UG9ydGFsTW9kdWxlIH0gZnJvbSAnLi9jaGFuZ2UtcGFzc3dvcmQtcGFyZW50LXBvcnRhbC9pbmRleCc7XHJcbmltcG9ydCB7IFJlc2V0UGFzc3dvcmRNb2R1bGUgfSBmcm9tICcuL3Jlc2V0LXBhc3N3b3JkL2luZGV4JztcclxuaW1wb3J0IHsgUmVzZXRQYXNzd29yZFBhcmVudFBvcnRhbE1vZHVsZSB9IGZyb20gJy4vcmVzZXQtcGFzc3dvcmQtcGFyZW50LXBvcnRhbC9pbmRleCc7XHJcbmltcG9ydCB7IFZlcmlmeUFjY291bnRDbGllbnRBZG1pbk1vZHVsZSB9IGZyb20gJy4vdmVyaWZ5LWFjY291bnQtY2xpZW50LWFkbWluL2luZGV4JztcclxuaW1wb3J0IHsgVmVyaWZ5QWNjb3VudFBhcmVudE1vZHVsZSB9IGZyb20gJy4vdmVyaWZ5LWFjY291bnQtcGFyZW50L2luZGV4JztcclxuaW1wb3J0IHsgVmVyaWZ5QWNjb3VudFBhcmVudFBvcnRhbE1vZHVsZSB9IGZyb20gJy4vdmVyaWZ5LWFjY291bnQtcGFyZW50LXBvcnRhbC9pbmRleCc7XHJcbmltcG9ydCB7IENlbnRyZVNlcnZpY2UsIENlbnRyZUxvZ29SZXNvbHZlciB9IGZyb20gJy4vY29tcG9uZW50cy9pbmRleCc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgaW1wb3J0czogW1xyXG4gICAgICAgIENoYW5nZVBhc3N3b3JkTW9kdWxlLFxyXG4gICAgICAgIENoYW5nZVBhc3N3b3JkUGFyZW50UG9ydGFsTW9kdWxlLFxyXG4gICAgICAgIFJlc2V0UGFzc3dvcmRNb2R1bGUsXHJcbiAgICAgICAgUmVzZXRQYXNzd29yZFBhcmVudFBvcnRhbE1vZHVsZSxcclxuICAgICAgICBWZXJpZnlBY2NvdW50Q2xpZW50QWRtaW5Nb2R1bGUsXHJcbiAgICAgICAgVmVyaWZ5QWNjb3VudFBhcmVudE1vZHVsZSxcclxuICAgICAgICBWZXJpZnlBY2NvdW50UGFyZW50UG9ydGFsTW9kdWxlLFxyXG4gICAgICAgIFNoYXJlZE1vZHVsZVxyXG4gICAgXSxcclxuICAgIHByb3ZpZGVyczogW0NlbnRyZUxvZ29SZXNvbHZlciwgQ2VudHJlU2VydmljZV0sXHJcbiAgICBkZWNsYXJhdGlvbnM6IFtcclxuICAgICAgICBQdWJsaWNDb21wb25lbnRcclxuICAgIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIFB1YmxpY01vZHVsZSB7IH1cclxuIl19
