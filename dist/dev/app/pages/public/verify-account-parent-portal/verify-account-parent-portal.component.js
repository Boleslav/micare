"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rxjs_1 = require('rxjs');
var index_1 = require('../../../api/index');
var VerifyAccountParentPortalComponent = (function () {
    function VerifyAccountParentPortalComponent(_apiClient, _route) {
        this._apiClient = _apiClient;
        this._route = _route;
        this.verificationCompleted = undefined;
        this.token = undefined;
        this.centreName = undefined;
        this.logo = null;
    }
    VerifyAccountParentPortalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route
            .params
            .flatMap(function (params) { return _this.getLogo$(_this._route); })
            .flatMap(function () {
            return _this._route
                .params
                .switchMap(function (params) {
                _this.token = params['token'];
                _this.centreName = params['centreName'];
                return _this._apiClient.account_VerifyEmail(_this.token);
            });
        }).subscribe(function (res) { return _this.verificationCompleted = true; });
    };
    VerifyAccountParentPortalComponent.prototype.getLogo$ = function (route) {
        var _this = this;
        return route
            .data
            .flatMap(function (data) {
            if (data.centreLogo && data.centreLogo.imageBase64) {
                _this.logo = data.centreLogo.imageBase64;
            }
            return rxjs_1.Observable.of(null);
        });
    };
    VerifyAccountParentPortalComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'verify-account-parent-portal-cmp',
            templateUrl: 'verify-account-parent-portal.component.html'
        }), 
        __metadata('design:paramtypes', [index_1.ApiClientService, router_1.ActivatedRoute])
    ], VerifyAccountParentPortalComponent);
    return VerifyAccountParentPortalComponent;
}());
exports.VerifyAccountParentPortalComponent = VerifyAccountParentPortalComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wdWJsaWMvdmVyaWZ5LWFjY291bnQtcGFyZW50LXBvcnRhbC92ZXJpZnktYWNjb3VudC1wYXJlbnQtcG9ydGFsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQWtDLGVBQWUsQ0FBQyxDQUFBO0FBQ2xELHVCQUF1QyxpQkFBaUIsQ0FBQyxDQUFBO0FBQ3pELHFCQUEyQixNQUFNLENBQUMsQ0FBQTtBQUVsQyxzQkFBZ0Qsb0JBQW9CLENBQUMsQ0FBQTtBQVFyRTtJQU9FLDRDQUNVLFVBQTRCLEVBQzVCLE1BQXNCO1FBRHRCLGVBQVUsR0FBVixVQUFVLENBQWtCO1FBQzVCLFdBQU0sR0FBTixNQUFNLENBQWdCO1FBUHhCLDBCQUFxQixHQUFZLFNBQVMsQ0FBQztRQUMzQyxVQUFLLEdBQVcsU0FBUyxDQUFDO1FBQzFCLGVBQVUsR0FBVyxTQUFTLENBQUM7UUFDL0IsU0FBSSxHQUFXLElBQUksQ0FBQztJQUs1QixDQUFDO0lBRUQscURBQVEsR0FBUjtRQUFBLGlCQWNDO1FBWkMsSUFBSSxDQUFDLE1BQU07YUFDUixNQUFNO2FBQ04sT0FBTyxDQUFDLFVBQUMsTUFBYyxJQUFLLE9BQUEsS0FBSSxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLEVBQTFCLENBQTBCLENBQUM7YUFDdkQsT0FBTyxDQUFDO1lBQ1AsTUFBTSxDQUFDLEtBQUksQ0FBQyxNQUFNO2lCQUNmLE1BQU07aUJBQ04sU0FBUyxDQUFDLFVBQUMsTUFBYztnQkFDeEIsS0FBSSxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQzdCLEtBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUN2QyxNQUFNLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDekQsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxLQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxFQUFqQyxDQUFpQyxDQUFDLENBQUM7SUFDM0QsQ0FBQztJQUVELHFEQUFRLEdBQVIsVUFBUyxLQUFxQjtRQUE5QixpQkFTQztRQVJDLE1BQU0sQ0FBQyxLQUFLO2FBQ1QsSUFBSTthQUNKLE9BQU8sQ0FBQyxVQUFDLElBQW1DO1lBQzNDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO2dCQUNuRCxLQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDO1lBQzFDLENBQUM7WUFDRCxNQUFNLENBQUMsaUJBQVUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDN0IsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBM0NIO1FBQUMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsa0NBQWtDO1lBQzVDLFdBQVcsRUFBRSw2Q0FBNkM7U0FDM0QsQ0FBQzs7MENBQUE7SUF3Q0YseUNBQUM7QUFBRCxDQXRDQSxBQXNDQyxJQUFBO0FBdENZLDBDQUFrQyxxQ0FzQzlDLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL3B1YmxpYy92ZXJpZnktYWNjb3VudC1wYXJlbnQtcG9ydGFsL3ZlcmlmeS1hY2NvdW50LXBhcmVudC1wb3J0YWwuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUsIFBhcmFtcyB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuXHJcbmltcG9ydCB7IEFwaUNsaWVudFNlcnZpY2UsIENlbnRyZUxvZ29EdG8gfSBmcm9tICcuLi8uLi8uLi9hcGkvaW5kZXgnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICBzZWxlY3RvcjogJ3ZlcmlmeS1hY2NvdW50LXBhcmVudC1wb3J0YWwtY21wJyxcclxuICB0ZW1wbGF0ZVVybDogJ3ZlcmlmeS1hY2NvdW50LXBhcmVudC1wb3J0YWwuY29tcG9uZW50Lmh0bWwnXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgVmVyaWZ5QWNjb3VudFBhcmVudFBvcnRhbENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIHByaXZhdGUgdmVyaWZpY2F0aW9uQ29tcGxldGVkOiBib29sZWFuID0gdW5kZWZpbmVkO1xyXG4gIHByaXZhdGUgdG9rZW46IHN0cmluZyA9IHVuZGVmaW5lZDtcclxuICBwcml2YXRlIGNlbnRyZU5hbWU6IHN0cmluZyA9IHVuZGVmaW5lZDtcclxuICBwcml2YXRlIGxvZ286IHN0cmluZyA9IG51bGw7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBfYXBpQ2xpZW50OiBBcGlDbGllbnRTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBfcm91dGU6IEFjdGl2YXRlZFJvdXRlKSB7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuXHJcbiAgICB0aGlzLl9yb3V0ZVxyXG4gICAgICAucGFyYW1zXHJcbiAgICAgIC5mbGF0TWFwKChwYXJhbXM6IFBhcmFtcykgPT4gdGhpcy5nZXRMb2dvJCh0aGlzLl9yb3V0ZSkpXHJcbiAgICAgIC5mbGF0TWFwKCgpID0+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fcm91dGVcclxuICAgICAgICAgIC5wYXJhbXNcclxuICAgICAgICAgIC5zd2l0Y2hNYXAoKHBhcmFtczogUGFyYW1zKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMudG9rZW4gPSBwYXJhbXNbJ3Rva2VuJ107XHJcbiAgICAgICAgICAgIHRoaXMuY2VudHJlTmFtZSA9IHBhcmFtc1snY2VudHJlTmFtZSddO1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5fYXBpQ2xpZW50LmFjY291bnRfVmVyaWZ5RW1haWwodGhpcy50b2tlbik7XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgfSkuc3Vic2NyaWJlKHJlcyA9PiB0aGlzLnZlcmlmaWNhdGlvbkNvbXBsZXRlZCA9IHRydWUpO1xyXG4gIH1cclxuXHJcbiAgZ2V0TG9nbyQocm91dGU6IEFjdGl2YXRlZFJvdXRlKTogT2JzZXJ2YWJsZTx2b2lkPiB7XHJcbiAgICByZXR1cm4gcm91dGVcclxuICAgICAgLmRhdGFcclxuICAgICAgLmZsYXRNYXAoKGRhdGE6IHsgY2VudHJlTG9nbzogQ2VudHJlTG9nb0R0byB9KSA9PiB7XHJcbiAgICAgICAgaWYgKGRhdGEuY2VudHJlTG9nbyAmJiBkYXRhLmNlbnRyZUxvZ28uaW1hZ2VCYXNlNjQpIHtcclxuICAgICAgICAgIHRoaXMubG9nbyA9IGRhdGEuY2VudHJlTG9nby5pbWFnZUJhc2U2NDtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIE9ic2VydmFibGUub2YobnVsbCk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxufVxyXG4iXX0=
