"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
__export(require('./verify-account-parent.component'));
__export(require('./verify-account-parent.routes'));
__export(require('./verify-account-parent.module'));

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wdWJsaWMvdmVyaWZ5LWFjY291bnQtcGFyZW50L2luZGV4LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxpQkFBYyxtQ0FBbUMsQ0FBQyxFQUFBO0FBQ2xELGlCQUFjLGdDQUFnQyxDQUFDLEVBQUE7QUFDL0MsaUJBQWMsZ0NBQWdDLENBQUMsRUFBQSIsImZpbGUiOiJhcHAvcGFnZXMvcHVibGljL3ZlcmlmeS1hY2NvdW50LXBhcmVudC9pbmRleC5qcyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCAqIGZyb20gJy4vdmVyaWZ5LWFjY291bnQtcGFyZW50LmNvbXBvbmVudCc7XHJcbmV4cG9ydCAqIGZyb20gJy4vdmVyaWZ5LWFjY291bnQtcGFyZW50LnJvdXRlcyc7XHJcbmV4cG9ydCAqIGZyb20gJy4vdmVyaWZ5LWFjY291bnQtcGFyZW50Lm1vZHVsZSc7XHJcbiJdfQ==
