"use strict";
var index_1 = require('./index');
exports.VerifyAccountParentRoutes = [
    {
        path: 'verify-account-parent/:token', component: index_1.VerifyAccountParentComponent
    }
];

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wdWJsaWMvdmVyaWZ5LWFjY291bnQtcGFyZW50L3ZlcmlmeS1hY2NvdW50LXBhcmVudC5yb3V0ZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBLHNCQUE2QyxTQUFTLENBQUMsQ0FBQTtBQUUxQyxpQ0FBeUIsR0FBWTtJQUNqRDtRQUNDLElBQUksRUFBRSw4QkFBOEIsRUFBRSxTQUFTLEVBQUUsb0NBQTRCO0tBQzdFO0NBQ0QsQ0FBQyIsImZpbGUiOiJhcHAvcGFnZXMvcHVibGljL3ZlcmlmeS1hY2NvdW50LXBhcmVudC92ZXJpZnktYWNjb3VudC1wYXJlbnQucm91dGVzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBWZXJpZnlBY2NvdW50UGFyZW50Q29tcG9uZW50IH0gZnJvbSAnLi9pbmRleCc7XHJcblxyXG5leHBvcnQgY29uc3QgVmVyaWZ5QWNjb3VudFBhcmVudFJvdXRlczogUm91dGVbXSA9IFtcclxuXHR7XHJcblx0XHRwYXRoOiAndmVyaWZ5LWFjY291bnQtcGFyZW50Lzp0b2tlbicsIGNvbXBvbmVudDogVmVyaWZ5QWNjb3VudFBhcmVudENvbXBvbmVudFxyXG5cdH1cclxuXTtcclxuIl19
