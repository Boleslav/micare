"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../shared/index');
var index_2 = require('./index');
var VerifyAccountParentModule = (function () {
    function VerifyAccountParentModule() {
    }
    VerifyAccountParentModule = __decorate([
        core_1.NgModule({
            imports: [index_1.SharedModule],
            declarations: [index_2.VerifyAccountParentComponent],
            providers: []
        }), 
        __metadata('design:paramtypes', [])
    ], VerifyAccountParentModule);
    return VerifyAccountParentModule;
}());
exports.VerifyAccountParentModule = VerifyAccountParentModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wdWJsaWMvdmVyaWZ5LWFjY291bnQtcGFyZW50L3ZlcmlmeS1hY2NvdW50LXBhcmVudC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUF5QixlQUFlLENBQUMsQ0FBQTtBQUN6QyxzQkFBNkIsdUJBQXVCLENBQUMsQ0FBQTtBQUVyRCxzQkFBNkMsU0FBUyxDQUFDLENBQUE7QUFRdkQ7SUFBQTtJQUF5QyxDQUFDO0lBTjFDO1FBQUMsZUFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFLENBQUMsb0JBQVksQ0FBQztZQUN2QixZQUFZLEVBQUUsQ0FBQyxvQ0FBNEIsQ0FBQztZQUM1QyxTQUFTLEVBQUUsRUFBRTtTQUNoQixDQUFDOztpQ0FBQTtJQUV1QyxnQ0FBQztBQUFELENBQXpDLEFBQTBDLElBQUE7QUFBN0IsaUNBQXlCLDRCQUFJLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL3B1YmxpYy92ZXJpZnktYWNjb3VudC1wYXJlbnQvdmVyaWZ5LWFjY291bnQtcGFyZW50Lm1vZHVsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFNoYXJlZE1vZHVsZSB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9pbmRleCc7XHJcblxyXG5pbXBvcnQgeyBWZXJpZnlBY2NvdW50UGFyZW50Q29tcG9uZW50IH0gZnJvbSAnLi9pbmRleCc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgaW1wb3J0czogW1NoYXJlZE1vZHVsZV0sXHJcbiAgICBkZWNsYXJhdGlvbnM6IFtWZXJpZnlBY2NvdW50UGFyZW50Q29tcG9uZW50XSxcclxuICAgIHByb3ZpZGVyczogW11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBWZXJpZnlBY2NvdW50UGFyZW50TW9kdWxlIHsgfVxyXG4iXX0=
