"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var index_1 = require('../../../api/index');
var VerifyAccountParentComponent = (function () {
    function VerifyAccountParentComponent(_apiClient, _route) {
        this._apiClient = _apiClient;
        this._route = _route;
        this.verificationCompleted = undefined;
        this.token = undefined;
    }
    VerifyAccountParentComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route
            .params
            .switchMap(function (params) {
            _this.token = _this.token = params['token'];
            return _this._apiClient.account_VerifyEmail(_this.token);
        }).subscribe(function (res) {
            _this.verificationCompleted = true;
        });
    };
    VerifyAccountParentComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'verify-account-parent-cmp',
            templateUrl: 'verify-account-parent.component.html'
        }), 
        __metadata('design:paramtypes', [index_1.ApiClientService, router_1.ActivatedRoute])
    ], VerifyAccountParentComponent);
    return VerifyAccountParentComponent;
}());
exports.VerifyAccountParentComponent = VerifyAccountParentComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wdWJsaWMvdmVyaWZ5LWFjY291bnQtcGFyZW50L3ZlcmlmeS1hY2NvdW50LXBhcmVudC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUFrQyxlQUFlLENBQUMsQ0FBQTtBQUNsRCx1QkFBdUMsaUJBQWlCLENBQUMsQ0FBQTtBQUV6RCxzQkFBaUMsb0JBQW9CLENBQUMsQ0FBQTtBQVF0RDtJQUtFLHNDQUFvQixVQUE0QixFQUFVLE1BQXNCO1FBQTVELGVBQVUsR0FBVixVQUFVLENBQWtCO1FBQVUsV0FBTSxHQUFOLE1BQU0sQ0FBZ0I7UUFIeEUsMEJBQXFCLEdBQVksU0FBUyxDQUFDO1FBQzNDLFVBQUssR0FBVyxTQUFTLENBQUM7SUFHbEMsQ0FBQztJQUVELCtDQUFRLEdBQVI7UUFBQSxpQkFVQztRQVJDLElBQUksQ0FBQyxNQUFNO2FBQ1IsTUFBTTthQUNOLFNBQVMsQ0FBQyxVQUFDLE1BQWM7WUFDeEIsS0FBSSxDQUFDLEtBQUssR0FBRyxLQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMxQyxNQUFNLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDekQsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsR0FBRztZQUNkLEtBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUM7UUFDcEMsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBeEJIO1FBQUMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsMkJBQTJCO1lBQ3JDLFdBQVcsRUFBRSxzQ0FBc0M7U0FDcEQsQ0FBQzs7b0NBQUE7SUFxQkYsbUNBQUM7QUFBRCxDQW5CQSxBQW1CQyxJQUFBO0FBbkJZLG9DQUE0QiwrQkFtQnhDLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL3B1YmxpYy92ZXJpZnktYWNjb3VudC1wYXJlbnQvdmVyaWZ5LWFjY291bnQtcGFyZW50LmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlLCBQYXJhbXMgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5cclxuaW1wb3J0IHsgQXBpQ2xpZW50U2VydmljZSB9IGZyb20gJy4uLy4uLy4uL2FwaS9pbmRleCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gIHNlbGVjdG9yOiAndmVyaWZ5LWFjY291bnQtcGFyZW50LWNtcCcsXHJcbiAgdGVtcGxhdGVVcmw6ICd2ZXJpZnktYWNjb3VudC1wYXJlbnQuY29tcG9uZW50Lmh0bWwnXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgVmVyaWZ5QWNjb3VudFBhcmVudENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIHByaXZhdGUgdmVyaWZpY2F0aW9uQ29tcGxldGVkOiBib29sZWFuID0gdW5kZWZpbmVkO1xyXG4gIHByaXZhdGUgdG9rZW46IHN0cmluZyA9IHVuZGVmaW5lZDtcclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBfYXBpQ2xpZW50OiBBcGlDbGllbnRTZXJ2aWNlLCBwcml2YXRlIF9yb3V0ZTogQWN0aXZhdGVkUm91dGUpIHtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG5cclxuICAgIHRoaXMuX3JvdXRlXHJcbiAgICAgIC5wYXJhbXNcclxuICAgICAgLnN3aXRjaE1hcCgocGFyYW1zOiBQYXJhbXMpID0+IHtcclxuICAgICAgICB0aGlzLnRva2VuID0gdGhpcy50b2tlbiA9IHBhcmFtc1sndG9rZW4nXTtcclxuICAgICAgICByZXR1cm4gdGhpcy5fYXBpQ2xpZW50LmFjY291bnRfVmVyaWZ5RW1haWwodGhpcy50b2tlbik7XHJcbiAgICAgIH0pLnN1YnNjcmliZShyZXMgPT4ge1xyXG4gICAgICAgIHRoaXMudmVyaWZpY2F0aW9uQ29tcGxldGVkID0gdHJ1ZTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG59XHJcbiJdfQ==
