"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../../shared/index');
var index_2 = require('./index');
var EnrolmentContactEditModule = (function () {
    function EnrolmentContactEditModule() {
    }
    EnrolmentContactEditModule = __decorate([
        core_1.NgModule({
            imports: [index_1.SharedModule],
            declarations: [index_2.EnrolmentContactEditComponent],
            exports: [index_2.EnrolmentContactEditComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], EnrolmentContactEditModule);
    return EnrolmentContactEditModule;
}());
exports.EnrolmentContactEditModule = EnrolmentContactEditModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvY29udGFjdC1lZGl0LWVucm9sbWVudC9jb250YWN0LWVkaXQtZW5yb2xtZW50Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQXlCLGVBQWUsQ0FBQyxDQUFBO0FBQ3pDLHNCQUE2QiwwQkFBMEIsQ0FBQyxDQUFBO0FBRXhELHNCQUE4QyxTQUFTLENBQUMsQ0FBQTtBQVF4RDtJQUFBO0lBQTBDLENBQUM7SUFOM0M7UUFBQyxlQUFRLENBQUM7WUFDUixPQUFPLEVBQUUsQ0FBQyxvQkFBWSxDQUFDO1lBQ3ZCLFlBQVksRUFBRSxDQUFDLHFDQUE2QixDQUFDO1lBQzNDLE9BQU8sRUFBRSxDQUFDLHFDQUE2QixDQUFDO1NBQzNDLENBQUM7O2tDQUFBO0lBRXdDLGlDQUFDO0FBQUQsQ0FBMUMsQUFBMkMsSUFBQTtBQUE5QixrQ0FBMEIsNkJBQUksQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvcGFyZW50cy1wb3J0YWwvZW5yb2xtZW50L2NvbnRhY3QtZWRpdC1lbnJvbG1lbnQvY29udGFjdC1lZGl0LWVucm9sbWVudC5tb2R1bGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTaGFyZWRNb2R1bGUgfSBmcm9tICcuLi8uLi8uLi8uLi9zaGFyZWQvaW5kZXgnO1xyXG5cclxuaW1wb3J0IHsgRW5yb2xtZW50Q29udGFjdEVkaXRDb21wb25lbnQgfSBmcm9tICcuL2luZGV4JztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgaW1wb3J0czogW1NoYXJlZE1vZHVsZV0sXHJcbiAgZGVjbGFyYXRpb25zOiBbRW5yb2xtZW50Q29udGFjdEVkaXRDb21wb25lbnRdLFxyXG4gICAgZXhwb3J0czogW0Vucm9sbWVudENvbnRhY3RFZGl0Q29tcG9uZW50XVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEVucm9sbWVudENvbnRhY3RFZGl0TW9kdWxlIHsgfVxyXG4iXX0=
