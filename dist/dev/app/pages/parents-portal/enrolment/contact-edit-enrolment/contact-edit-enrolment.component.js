"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var router_1 = require('@angular/router');
var rxjs_1 = require('rxjs');
var index_1 = require('../index');
var index_2 = require('../../index');
var index_3 = require('../../../../api/index');
var angular2_toaster_1 = require('angular2-toaster');
var index_4 = require('../../../../shared/index');
var index_5 = require('../../../../validators/index');
var EnrolmentContactEditComponent = (function () {
    function EnrolmentContactEditComponent(_router, _route, _enrolmentService, _centreService, _toasterService, fb) {
        this._router = _router;
        this._route = _route;
        this._enrolmentService = _enrolmentService;
        this._centreService = _centreService;
        this._toasterService = _toasterService;
        this.maxDateOfBirth = null;
        this.contactDto = null;
        this.relationshipOptions = new Array();
        this.titles = new Array();
        this.genders = new Array();
        var today = new Date();
        this.maxDateOfBirth = new Date(today.getFullYear() - 16, today.getMonth()).toJSON().split('T')[0];
        this.form = fb.group({
            'title': [''],
            'firstname': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.maxLength(100)])],
            'lastname': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.maxLength(100)])],
            'mobile': ['', forms_1.Validators.compose([forms_1.Validators.required, index_5.MobileValidator.validate, forms_1.Validators.maxLength(100)])],
            'email': ['', forms_1.Validators.compose([forms_1.Validators.required, index_5.EmailValidator.validate, forms_1.Validators.maxLength(100)])],
            'homePhone': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.maxLength(100)])],
            'gender': [''],
            'relationship': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.maxLength(50)])],
            'streetAddress': ['', forms_1.Validators.maxLength(200)],
            'suburb': ['', forms_1.Validators.maxLength(50)],
            'postcode': ['', forms_1.Validators.pattern('(\\d{4})')],
            'state': ['', forms_1.Validators.maxLength(50)]
        });
        this.title = this.form.controls['title'];
        this.firstname = this.form.controls['firstname'];
        this.lastname = this.form.controls['lastname'];
        this.mobile = this.form.controls['mobile'];
        this.email = this.form.controls['email'];
        this.homePhone = this.form.controls['homePhone'];
        this.gender = this.form.controls['gender'];
        this.relationship = this.form.controls['relationship'];
        this.streetAddress = this.form.controls['streetAddress'];
        this.suburb = this.form.controls['suburb'];
        this.postcode = this.form.controls['postcode'];
        this.state = this.form.controls['state'];
        this.crn = this.form.controls['crn'];
    }
    EnrolmentContactEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route
            .queryParams
            .map(function (params) { return params['contactId']; })
            .flatMap(function (contactId) {
            return (contactId === null || contactId === undefined) ?
                rxjs_1.Observable.of(null) :
                _this._enrolmentService.getContact$(contactId);
        })
            .subscribe(function (dto) {
            if ((dto !== null && dto !== undefined)) {
                _this.contactDto = dto;
            }
            else {
                _this._route
                    .queryParams
                    .map(function (params) { return params['enrolmentFormId']; })
                    .subscribe(function (enrolmentFormId) {
                    if (enrolmentFormId === null || enrolmentFormId === undefined || enrolmentFormId === null)
                        throw new Error('Must provide enrolment form Id when creating new contact');
                    _this.contactDto = new index_3.ContactDto();
                    _this.contactDto.init({
                        EnrolmentFormId: enrolmentFormId
                    });
                });
            }
        });
        rxjs_1.Observable
            .forkJoin(this._enrolmentService.getRelationshipOptions$(), this._enrolmentService.getTitles$(), this._enrolmentService.getGenders$())
            .subscribe(function (res) {
            _this.relationshipOptions = _this._enrolmentService.convertEnumToArray(res[0]);
            _this.titles = _this._enrolmentService.convertEnumToArray(res[1]);
            _this.genders = _this._enrolmentService.convertEnumToArray(res[2]);
        });
    };
    EnrolmentContactEditComponent.prototype.saveChanges = function () {
        var _this = this;
        var savingToast = this._toasterService.pop(new index_4.SpinningToast());
        var updateContact = new index_3.CreateUpdateContactDto();
        updateContact.init(this.contactDto.toJSON());
        this._enrolmentService
            .addOrUpdateContact$(updateContact)
            .subscribe(function (res) {
            _this.contactDto = res;
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this._toasterService.popAsync('success', 'Saved', '');
            _this._router.navigate(['../enrolment-form'], { relativeTo: _this._route });
        }, function (error) {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
        });
    };
    EnrolmentContactEditComponent.prototype.cancelEdit = function () {
        this._router.navigate(['../enrolment-form'], { relativeTo: this._route });
    };
    EnrolmentContactEditComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            encapsulation: core_1.ViewEncapsulation.None,
            selector: 'contact-edit-enrolment-cmp',
            templateUrl: 'contact-edit-enrolment.component.html',
            styleUrls: ['contact-edit-enrolment.component.css']
        }), 
        __metadata('design:paramtypes', [router_1.Router, router_1.ActivatedRoute, index_1.EnrolmentService, index_2.CentreService, angular2_toaster_1.ToasterService, forms_1.FormBuilder])
    ], EnrolmentContactEditComponent);
    return EnrolmentContactEditComponent;
}());
exports.EnrolmentContactEditComponent = EnrolmentContactEditComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvY29udGFjdC1lZGl0LWVucm9sbWVudC9jb250YWN0LWVkaXQtZW5yb2xtZW50LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQXNELGVBQWUsQ0FBQyxDQUFBO0FBQ3RFLHNCQUFvRSxnQkFBZ0IsQ0FBQyxDQUFBO0FBQ3JGLHVCQUErQyxpQkFBaUIsQ0FBQyxDQUFBO0FBQ2pFLHFCQUEyQixNQUFNLENBQUMsQ0FBQTtBQUVsQyxzQkFBaUMsVUFBVSxDQUFDLENBQUE7QUFDNUMsc0JBQThCLGFBQWEsQ0FBQyxDQUFBO0FBQzVDLHNCQUFtRCx1QkFBdUIsQ0FBQyxDQUFBO0FBQzNFLGlDQUErQixrQkFBa0IsQ0FBQyxDQUFBO0FBQ2xELHNCQUE4QiwwQkFBMEIsQ0FBQyxDQUFBO0FBQ3pELHNCQUFnRCw4QkFBOEIsQ0FBQyxDQUFBO0FBVS9FO0lBdUJFLHVDQUFvQixPQUFlLEVBQ3pCLE1BQXNCLEVBQ3RCLGlCQUFtQyxFQUNuQyxjQUE2QixFQUM3QixlQUErQixFQUN2QyxFQUFlO1FBTEcsWUFBTyxHQUFQLE9BQU8sQ0FBUTtRQUN6QixXQUFNLEdBQU4sTUFBTSxDQUFnQjtRQUN0QixzQkFBaUIsR0FBakIsaUJBQWlCLENBQWtCO1FBQ25DLG1CQUFjLEdBQWQsY0FBYyxDQUFlO1FBQzdCLG9CQUFlLEdBQWYsZUFBZSxDQUFnQjtRQXpCbEMsbUJBQWMsR0FBVyxJQUFJLENBQUM7UUFDOUIsZUFBVSxHQUFlLElBQUksQ0FBQztRQUM5Qix3QkFBbUIsR0FBZ0MsSUFBSSxLQUFLLEVBQTZCLENBQUM7UUFDekYsV0FBTSxHQUFnQyxJQUFJLEtBQUssRUFBNkIsQ0FBQztRQUM3RSxZQUFPLEdBQWdDLElBQUksS0FBSyxFQUE2QixDQUFDO1FBd0JwRixJQUFJLEtBQUssR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRSxHQUFHLEVBQUUsRUFBRSxLQUFLLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFFbEcsSUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDO1lBQ25CLE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQztZQUNiLFdBQVcsRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLGtCQUFVLENBQUMsUUFBUSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN2RixVQUFVLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdEYsUUFBUSxFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsa0JBQVUsQ0FBQyxRQUFRLEVBQUUsdUJBQWUsQ0FBQyxRQUFRLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzlHLE9BQU8sRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLGtCQUFVLENBQUMsUUFBUSxFQUFFLHNCQUFjLENBQUMsUUFBUSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM1RyxXQUFXLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdkYsUUFBUSxFQUFFLENBQUMsRUFBRSxDQUFDO1lBQ2QsY0FBYyxFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsa0JBQVUsQ0FBQyxRQUFRLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3pGLGVBQWUsRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNoRCxRQUFRLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDeEMsVUFBVSxFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ2hELE9BQU8sRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQztTQUN4QyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDakQsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUMvQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzNDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDekMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNqRCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzNDLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDdkQsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUN6RCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzNDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDL0MsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFFRCxnREFBUSxHQUFSO1FBQUEsaUJBdUNDO1FBckNDLElBQUksQ0FBQyxNQUFNO2FBQ1IsV0FBVzthQUNYLEdBQUcsQ0FBQyxVQUFDLE1BQWMsSUFBSyxPQUFBLE1BQU0sQ0FBQyxXQUFXLENBQUMsRUFBbkIsQ0FBbUIsQ0FBQzthQUM1QyxPQUFPLENBQUMsVUFBQSxTQUFTO1lBQ2hCLE9BQUEsQ0FBQyxTQUFTLEtBQUssSUFBSSxJQUFJLFNBQVMsS0FBSyxTQUFTLENBQUM7Z0JBQzdDLGlCQUFVLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztnQkFDbkIsS0FBSSxDQUFDLGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUM7UUFGL0MsQ0FFK0MsQ0FBQzthQUNqRCxTQUFTLENBQUMsVUFBQSxHQUFHO1lBRVosRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLEtBQUssSUFBSSxJQUFJLEdBQUcsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hDLEtBQUksQ0FBQyxVQUFVLEdBQUcsR0FBRyxDQUFDO1lBQ3hCLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTixLQUFJLENBQUMsTUFBTTtxQkFDUixXQUFXO3FCQUNYLEdBQUcsQ0FBQyxVQUFDLE1BQWMsSUFBSyxPQUFBLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxFQUF6QixDQUF5QixDQUFDO3FCQUNsRCxTQUFTLENBQUMsVUFBQSxlQUFlO29CQUN4QixFQUFFLENBQUMsQ0FBQyxlQUFlLEtBQUssSUFBSSxJQUFJLGVBQWUsS0FBSyxTQUFTLElBQUksZUFBZSxLQUFLLElBQUksQ0FBQzt3QkFDeEYsTUFBTSxJQUFJLEtBQUssQ0FBQywwREFBMEQsQ0FBQyxDQUFDO29CQUM5RSxLQUFJLENBQUMsVUFBVSxHQUFHLElBQUksa0JBQVUsRUFBRSxDQUFDO29CQUNuQyxLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQzt3QkFDbkIsZUFBZSxFQUFFLGVBQWU7cUJBQ2pDLENBQUMsQ0FBQztnQkFDTCxDQUFDLENBQUMsQ0FBQztZQUNQLENBQUM7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUVMLGlCQUFVO2FBQ1AsUUFBUSxDQUNULElBQUksQ0FBQyxpQkFBaUIsQ0FBQyx1QkFBdUIsRUFBRSxFQUNoRCxJQUFJLENBQUMsaUJBQWlCLENBQUMsVUFBVSxFQUFFLEVBQ25DLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLEVBQUUsQ0FDbkM7YUFDQSxTQUFTLENBQUMsVUFBQSxHQUFHO1lBQ1osS0FBSSxDQUFDLG1CQUFtQixHQUFHLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM3RSxLQUFJLENBQUMsTUFBTSxHQUFHLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNoRSxLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNuRSxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxtREFBVyxHQUFYO1FBQUEsaUJBZUM7UUFkQyxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxJQUFJLHFCQUFhLEVBQUUsQ0FBQyxDQUFDO1FBQ2hFLElBQUksYUFBYSxHQUFHLElBQUksOEJBQXNCLEVBQUUsQ0FBQztRQUNqRCxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQztRQUM3QyxJQUFJLENBQUMsaUJBQWlCO2FBQ25CLG1CQUFtQixDQUFDLGFBQWEsQ0FBQzthQUNsQyxTQUFTLENBQUMsVUFBQSxHQUFHO1lBQ1osS0FBSSxDQUFDLFVBQVUsR0FBRyxHQUFHLENBQUM7WUFDdEIsS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxXQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUM5RSxLQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsT0FBTyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQ3RELEtBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsbUJBQW1CLENBQUMsRUFBRSxFQUFFLFVBQVUsRUFBRSxLQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQztRQUM1RSxDQUFDLEVBQ0QsVUFBQyxLQUFLO1lBQ0osS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxXQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUNoRixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxrREFBVSxHQUFWO1FBQ0UsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLEVBQUUsVUFBVSxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO0lBQzVFLENBQUM7SUFsSUg7UUFBQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLGFBQWEsRUFBRSx3QkFBaUIsQ0FBQyxJQUFJO1lBQ3JDLFFBQVEsRUFBRSw0QkFBNEI7WUFDdEMsV0FBVyxFQUFFLHVDQUF1QztZQUNwRCxTQUFTLEVBQUUsQ0FBQyxzQ0FBc0MsQ0FBQztTQUNwRCxDQUFDOztxQ0FBQTtJQTZIRixvQ0FBQztBQUFELENBM0hBLEFBMkhDLElBQUE7QUEzSFkscUNBQTZCLGdDQTJIekMsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvcGFyZW50cy1wb3J0YWwvZW5yb2xtZW50L2NvbnRhY3QtZWRpdC1lbnJvbG1lbnQvY29udGFjdC1lZGl0LWVucm9sbWVudC5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0VuY2Fwc3VsYXRpb24sIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1Hcm91cCwgQWJzdHJhY3RDb250cm9sLCBGb3JtQnVpbGRlciwgVmFsaWRhdG9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgUm91dGVyLCBBY3RpdmF0ZWRSb3V0ZSwgUGFyYW1zIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgRW5yb2xtZW50U2VydmljZSB9IGZyb20gJy4uL2luZGV4JztcclxuaW1wb3J0IHsgQ2VudHJlU2VydmljZSB9IGZyb20gJy4uLy4uL2luZGV4JztcclxuaW1wb3J0IHsgQ29udGFjdER0bywgQ3JlYXRlVXBkYXRlQ29udGFjdER0byB9IGZyb20gJy4uLy4uLy4uLy4uL2FwaS9pbmRleCc7XHJcbmltcG9ydCB7IFRvYXN0ZXJTZXJ2aWNlIH0gZnJvbSAnYW5ndWxhcjItdG9hc3Rlcic7XHJcbmltcG9ydCB7IFNwaW5uaW5nVG9hc3QgfSBmcm9tICcuLi8uLi8uLi8uLi9zaGFyZWQvaW5kZXgnO1xyXG5pbXBvcnQgeyBFbWFpbFZhbGlkYXRvciwgTW9iaWxlVmFsaWRhdG9yIH0gZnJvbSAnLi4vLi4vLi4vLi4vdmFsaWRhdG9ycy9pbmRleCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmUsXHJcbiAgc2VsZWN0b3I6ICdjb250YWN0LWVkaXQtZW5yb2xtZW50LWNtcCcsXHJcbiAgdGVtcGxhdGVVcmw6ICdjb250YWN0LWVkaXQtZW5yb2xtZW50LmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnY29udGFjdC1lZGl0LWVucm9sbWVudC5jb21wb25lbnQuY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBFbnJvbG1lbnRDb250YWN0RWRpdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIHB1YmxpYyBtYXhEYXRlT2ZCaXJ0aDogc3RyaW5nID0gbnVsbDtcclxuICBwdWJsaWMgY29udGFjdER0bzogQ29udGFjdER0byA9IG51bGw7XHJcbiAgcHVibGljIHJlbGF0aW9uc2hpcE9wdGlvbnM6IHsgW2tleTogc3RyaW5nXTogc3RyaW5nIH1bXSA9IG5ldyBBcnJheTx7IFtrZXk6IHN0cmluZ106IHN0cmluZyB9PigpO1xyXG4gIHByaXZhdGUgdGl0bGVzOiB7IFtrZXk6IHN0cmluZ106IHN0cmluZyB9W10gPSBuZXcgQXJyYXk8eyBba2V5OiBzdHJpbmddOiBzdHJpbmcgfT4oKTtcclxuICBwcml2YXRlIGdlbmRlcnM6IHsgW2tleTogc3RyaW5nXTogc3RyaW5nIH1bXSA9IG5ldyBBcnJheTx7IFtrZXk6IHN0cmluZ106IHN0cmluZyB9PigpO1xyXG5cclxuICBwcml2YXRlIGZvcm06IEZvcm1Hcm91cDtcclxuICBwcml2YXRlIHRpdGxlOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBmaXJzdG5hbWU6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIGxhc3RuYW1lOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBtb2JpbGU6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIGVtYWlsOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBob21lUGhvbmU6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIGdlbmRlcjogQWJzdHJhY3RDb250cm9sO1xyXG4gIHByaXZhdGUgcmVsYXRpb25zaGlwOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBzdHJlZXRBZGRyZXNzOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBzdWJ1cmI6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIHBvc3Rjb2RlOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBzdGF0ZTogQWJzdHJhY3RDb250cm9sO1xyXG4gIHByaXZhdGUgY3JuOiBBYnN0cmFjdENvbnRyb2w7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgX3JvdXRlcjogUm91dGVyLFxyXG4gICAgcHJpdmF0ZSBfcm91dGU6IEFjdGl2YXRlZFJvdXRlLFxyXG4gICAgcHJpdmF0ZSBfZW5yb2xtZW50U2VydmljZTogRW5yb2xtZW50U2VydmljZSxcclxuICAgIHByaXZhdGUgX2NlbnRyZVNlcnZpY2U6IENlbnRyZVNlcnZpY2UsXHJcbiAgICBwcml2YXRlIF90b2FzdGVyU2VydmljZTogVG9hc3RlclNlcnZpY2UsXHJcbiAgICBmYjogRm9ybUJ1aWxkZXIpIHtcclxuXHJcbiAgICBsZXQgdG9kYXkgPSBuZXcgRGF0ZSgpO1xyXG4gICAgdGhpcy5tYXhEYXRlT2ZCaXJ0aCA9IG5ldyBEYXRlKHRvZGF5LmdldEZ1bGxZZWFyKCkgLSAxNiwgdG9kYXkuZ2V0TW9udGgoKSkudG9KU09OKCkuc3BsaXQoJ1QnKVswXTtcclxuXHJcbiAgICB0aGlzLmZvcm0gPSBmYi5ncm91cCh7XHJcbiAgICAgICd0aXRsZSc6IFsnJ10sXHJcbiAgICAgICdmaXJzdG5hbWUnOiBbJycsIFZhbGlkYXRvcnMuY29tcG9zZShbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTAwKV0pXSxcclxuICAgICAgJ2xhc3RuYW1lJzogWycnLCBWYWxpZGF0b3JzLmNvbXBvc2UoW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDEwMCldKV0sXHJcbiAgICAgICdtb2JpbGUnOiBbJycsIFZhbGlkYXRvcnMuY29tcG9zZShbVmFsaWRhdG9ycy5yZXF1aXJlZCwgTW9iaWxlVmFsaWRhdG9yLnZhbGlkYXRlLCBWYWxpZGF0b3JzLm1heExlbmd0aCgxMDApXSldLFxyXG4gICAgICAnZW1haWwnOiBbJycsIFZhbGlkYXRvcnMuY29tcG9zZShbVmFsaWRhdG9ycy5yZXF1aXJlZCwgRW1haWxWYWxpZGF0b3IudmFsaWRhdGUsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDEwMCldKV0sXHJcbiAgICAgICdob21lUGhvbmUnOiBbJycsIFZhbGlkYXRvcnMuY29tcG9zZShbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTAwKV0pXSxcclxuICAgICAgJ2dlbmRlcic6IFsnJ10sXHJcbiAgICAgICdyZWxhdGlvbnNoaXAnOiBbJycsIFZhbGlkYXRvcnMuY29tcG9zZShbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoNTApXSldLFxyXG4gICAgICAnc3RyZWV0QWRkcmVzcyc6IFsnJywgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMjAwKV0sXHJcbiAgICAgICdzdWJ1cmInOiBbJycsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDUwKV0sXHJcbiAgICAgICdwb3N0Y29kZSc6IFsnJywgVmFsaWRhdG9ycy5wYXR0ZXJuKCcoXFxcXGR7NH0pJyldLFxyXG4gICAgICAnc3RhdGUnOiBbJycsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDUwKV1cclxuICAgIH0pO1xyXG4gICAgdGhpcy50aXRsZSA9IHRoaXMuZm9ybS5jb250cm9sc1sndGl0bGUnXTtcclxuICAgIHRoaXMuZmlyc3RuYW1lID0gdGhpcy5mb3JtLmNvbnRyb2xzWydmaXJzdG5hbWUnXTtcclxuICAgIHRoaXMubGFzdG5hbWUgPSB0aGlzLmZvcm0uY29udHJvbHNbJ2xhc3RuYW1lJ107XHJcbiAgICB0aGlzLm1vYmlsZSA9IHRoaXMuZm9ybS5jb250cm9sc1snbW9iaWxlJ107XHJcbiAgICB0aGlzLmVtYWlsID0gdGhpcy5mb3JtLmNvbnRyb2xzWydlbWFpbCddO1xyXG4gICAgdGhpcy5ob21lUGhvbmUgPSB0aGlzLmZvcm0uY29udHJvbHNbJ2hvbWVQaG9uZSddO1xyXG4gICAgdGhpcy5nZW5kZXIgPSB0aGlzLmZvcm0uY29udHJvbHNbJ2dlbmRlciddO1xyXG4gICAgdGhpcy5yZWxhdGlvbnNoaXAgPSB0aGlzLmZvcm0uY29udHJvbHNbJ3JlbGF0aW9uc2hpcCddO1xyXG4gICAgdGhpcy5zdHJlZXRBZGRyZXNzID0gdGhpcy5mb3JtLmNvbnRyb2xzWydzdHJlZXRBZGRyZXNzJ107XHJcbiAgICB0aGlzLnN1YnVyYiA9IHRoaXMuZm9ybS5jb250cm9sc1snc3VidXJiJ107XHJcbiAgICB0aGlzLnBvc3Rjb2RlID0gdGhpcy5mb3JtLmNvbnRyb2xzWydwb3N0Y29kZSddO1xyXG4gICAgdGhpcy5zdGF0ZSA9IHRoaXMuZm9ybS5jb250cm9sc1snc3RhdGUnXTtcclxuICAgIHRoaXMuY3JuID0gdGhpcy5mb3JtLmNvbnRyb2xzWydjcm4nXTtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG5cclxuICAgIHRoaXMuX3JvdXRlXHJcbiAgICAgIC5xdWVyeVBhcmFtc1xyXG4gICAgICAubWFwKChwYXJhbXM6IFBhcmFtcykgPT4gcGFyYW1zWydjb250YWN0SWQnXSlcclxuICAgICAgLmZsYXRNYXAoY29udGFjdElkID0+XHJcbiAgICAgICAgKGNvbnRhY3RJZCA9PT0gbnVsbCB8fCBjb250YWN0SWQgPT09IHVuZGVmaW5lZCkgP1xyXG4gICAgICAgICAgT2JzZXJ2YWJsZS5vZihudWxsKSA6XHJcbiAgICAgICAgICB0aGlzLl9lbnJvbG1lbnRTZXJ2aWNlLmdldENvbnRhY3QkKGNvbnRhY3RJZCkpXHJcbiAgICAgIC5zdWJzY3JpYmUoZHRvID0+IHtcclxuXHJcbiAgICAgICAgaWYgKChkdG8gIT09IG51bGwgJiYgZHRvICE9PSB1bmRlZmluZWQpKSB7XHJcbiAgICAgICAgICB0aGlzLmNvbnRhY3REdG8gPSBkdG87XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRoaXMuX3JvdXRlXHJcbiAgICAgICAgICAgIC5xdWVyeVBhcmFtc1xyXG4gICAgICAgICAgICAubWFwKChwYXJhbXM6IFBhcmFtcykgPT4gcGFyYW1zWydlbnJvbG1lbnRGb3JtSWQnXSlcclxuICAgICAgICAgICAgLnN1YnNjcmliZShlbnJvbG1lbnRGb3JtSWQgPT4ge1xyXG4gICAgICAgICAgICAgIGlmIChlbnJvbG1lbnRGb3JtSWQgPT09IG51bGwgfHwgZW5yb2xtZW50Rm9ybUlkID09PSB1bmRlZmluZWQgfHwgZW5yb2xtZW50Rm9ybUlkID09PSBudWxsKVxyXG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdNdXN0IHByb3ZpZGUgZW5yb2xtZW50IGZvcm0gSWQgd2hlbiBjcmVhdGluZyBuZXcgY29udGFjdCcpO1xyXG4gICAgICAgICAgICAgIHRoaXMuY29udGFjdER0byA9IG5ldyBDb250YWN0RHRvKCk7XHJcbiAgICAgICAgICAgICAgdGhpcy5jb250YWN0RHRvLmluaXQoe1xyXG4gICAgICAgICAgICAgICAgRW5yb2xtZW50Rm9ybUlkOiBlbnJvbG1lbnRGb3JtSWRcclxuICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuXHJcbiAgICBPYnNlcnZhYmxlXHJcbiAgICAgIC5mb3JrSm9pbihcclxuICAgICAgdGhpcy5fZW5yb2xtZW50U2VydmljZS5nZXRSZWxhdGlvbnNoaXBPcHRpb25zJCgpLFxyXG4gICAgICB0aGlzLl9lbnJvbG1lbnRTZXJ2aWNlLmdldFRpdGxlcyQoKSxcclxuICAgICAgdGhpcy5fZW5yb2xtZW50U2VydmljZS5nZXRHZW5kZXJzJCgpXHJcbiAgICAgIClcclxuICAgICAgLnN1YnNjcmliZShyZXMgPT4ge1xyXG4gICAgICAgIHRoaXMucmVsYXRpb25zaGlwT3B0aW9ucyA9IHRoaXMuX2Vucm9sbWVudFNlcnZpY2UuY29udmVydEVudW1Ub0FycmF5KHJlc1swXSk7XHJcbiAgICAgICAgdGhpcy50aXRsZXMgPSB0aGlzLl9lbnJvbG1lbnRTZXJ2aWNlLmNvbnZlcnRFbnVtVG9BcnJheShyZXNbMV0pO1xyXG4gICAgICAgIHRoaXMuZ2VuZGVycyA9IHRoaXMuX2Vucm9sbWVudFNlcnZpY2UuY29udmVydEVudW1Ub0FycmF5KHJlc1syXSk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgc2F2ZUNoYW5nZXMoKSB7XHJcbiAgICBsZXQgc2F2aW5nVG9hc3QgPSB0aGlzLl90b2FzdGVyU2VydmljZS5wb3AobmV3IFNwaW5uaW5nVG9hc3QoKSk7XHJcbiAgICBsZXQgdXBkYXRlQ29udGFjdCA9IG5ldyBDcmVhdGVVcGRhdGVDb250YWN0RHRvKCk7XHJcbiAgICB1cGRhdGVDb250YWN0LmluaXQodGhpcy5jb250YWN0RHRvLnRvSlNPTigpKTtcclxuICAgIHRoaXMuX2Vucm9sbWVudFNlcnZpY2VcclxuICAgICAgLmFkZE9yVXBkYXRlQ29udGFjdCQodXBkYXRlQ29udGFjdClcclxuICAgICAgLnN1YnNjcmliZShyZXMgPT4ge1xyXG4gICAgICAgIHRoaXMuY29udGFjdER0byA9IHJlcztcclxuICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcihzYXZpbmdUb2FzdC50b2FzdElkLCBzYXZpbmdUb2FzdC50b2FzdENvbnRhaW5lcklkKTtcclxuICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5wb3BBc3luYygnc3VjY2VzcycsICdTYXZlZCcsICcnKTtcclxuICAgICAgICB0aGlzLl9yb3V0ZXIubmF2aWdhdGUoWycuLi9lbnJvbG1lbnQtZm9ybSddLCB7IHJlbGF0aXZlVG86IHRoaXMuX3JvdXRlIH0pO1xyXG4gICAgICB9LFxyXG4gICAgICAoZXJyb3IpID0+IHtcclxuICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcihzYXZpbmdUb2FzdC50b2FzdElkLCBzYXZpbmdUb2FzdC50b2FzdENvbnRhaW5lcklkKTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBjYW5jZWxFZGl0KCkge1xyXG4gICAgdGhpcy5fcm91dGVyLm5hdmlnYXRlKFsnLi4vZW5yb2xtZW50LWZvcm0nXSwgeyByZWxhdGl2ZVRvOiB0aGlzLl9yb3V0ZSB9KTtcclxuICB9XHJcbn1cclxuIl19
