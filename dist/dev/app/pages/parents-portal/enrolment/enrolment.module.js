"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../shared/index');
var index_2 = require('./index');
var index_3 = require('./enrolment-form/index');
var index_4 = require('./parent-edit-enrolment/index');
var index_5 = require('./guardian-edit-enrolment/index');
var index_6 = require('./contact-edit-enrolment/index');
var index_7 = require('./kid-edit-enrolment/index');
var angular2_recaptcha_1 = require('angular2-recaptcha');
var EnrolmentModule = (function () {
    function EnrolmentModule() {
    }
    EnrolmentModule = __decorate([
        core_1.NgModule({
            imports: [
                index_1.SharedModule,
                angular2_recaptcha_1.ReCaptchaModule,
                index_3.EnrolmentFormModule,
                index_4.EnrolmentParentEditModule,
                index_5.EnrolmentGuardianEditModule,
                index_6.EnrolmentContactEditModule,
                index_7.EnrolmentKidEditModule
            ],
            declarations: [index_2.EnrolmentComponent],
            providers: [index_2.EnrolmentService, index_2.EnrolmentAccessGuardService, index_2.EnrolmentFormConsentResolver]
        }), 
        __metadata('design:paramtypes', [])
    ], EnrolmentModule);
    return EnrolmentModule;
}());
exports.EnrolmentModule = EnrolmentModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvZW5yb2xtZW50Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQXlCLGVBQWUsQ0FBQyxDQUFBO0FBQ3pDLHNCQUE2Qix1QkFBdUIsQ0FBQyxDQUFBO0FBRXJELHNCQUdPLFNBQVMsQ0FBQyxDQUFBO0FBQ2pCLHNCQUFvQyx3QkFBd0IsQ0FBQyxDQUFBO0FBQzdELHNCQUEwQywrQkFBK0IsQ0FBQyxDQUFBO0FBQzFFLHNCQUE0QyxpQ0FBaUMsQ0FBQyxDQUFBO0FBQzlFLHNCQUEyQyxnQ0FBZ0MsQ0FBQyxDQUFBO0FBQzVFLHNCQUF1Qyw0QkFBNEIsQ0FBQyxDQUFBO0FBQ3BFLG1DQUFnQyxvQkFBb0IsQ0FBQyxDQUFBO0FBaUJyRDtJQUFBO0lBQStCLENBQUM7SUFkaEM7UUFBQyxlQUFRLENBQUM7WUFDTixPQUFPLEVBQUU7Z0JBQ0wsb0JBQVk7Z0JBQ1osb0NBQWU7Z0JBQ2YsMkJBQW1CO2dCQUNuQixpQ0FBeUI7Z0JBQ3pCLG1DQUEyQjtnQkFDM0Isa0NBQTBCO2dCQUMxQiw4QkFBc0I7YUFDekI7WUFDRCxZQUFZLEVBQUUsQ0FBQywwQkFBa0IsQ0FBQztZQUNsQyxTQUFTLEVBQUUsQ0FBQyx3QkFBZ0IsRUFBRSxtQ0FBMkIsRUFBRSxvQ0FBNEIsQ0FBQztTQUMzRixDQUFDOzt1QkFBQTtJQUU2QixzQkFBQztBQUFELENBQS9CLEFBQWdDLElBQUE7QUFBbkIsdUJBQWUsa0JBQUksQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvcGFyZW50cy1wb3J0YWwvZW5yb2xtZW50L2Vucm9sbWVudC5tb2R1bGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTaGFyZWRNb2R1bGUgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvaW5kZXgnO1xyXG5cclxuaW1wb3J0IHtcclxuICAgIEVucm9sbWVudENvbXBvbmVudCwgRW5yb2xtZW50U2VydmljZSxcclxuICAgIEVucm9sbWVudEFjY2Vzc0d1YXJkU2VydmljZSwgRW5yb2xtZW50Rm9ybUNvbnNlbnRSZXNvbHZlclxyXG59IGZyb20gJy4vaW5kZXgnO1xyXG5pbXBvcnQgeyBFbnJvbG1lbnRGb3JtTW9kdWxlIH0gZnJvbSAnLi9lbnJvbG1lbnQtZm9ybS9pbmRleCc7XHJcbmltcG9ydCB7IEVucm9sbWVudFBhcmVudEVkaXRNb2R1bGUgfSBmcm9tICcuL3BhcmVudC1lZGl0LWVucm9sbWVudC9pbmRleCc7XHJcbmltcG9ydCB7IEVucm9sbWVudEd1YXJkaWFuRWRpdE1vZHVsZSB9IGZyb20gJy4vZ3VhcmRpYW4tZWRpdC1lbnJvbG1lbnQvaW5kZXgnO1xyXG5pbXBvcnQgeyBFbnJvbG1lbnRDb250YWN0RWRpdE1vZHVsZSB9IGZyb20gJy4vY29udGFjdC1lZGl0LWVucm9sbWVudC9pbmRleCc7XHJcbmltcG9ydCB7IEVucm9sbWVudEtpZEVkaXRNb2R1bGUgfSBmcm9tICcuL2tpZC1lZGl0LWVucm9sbWVudC9pbmRleCc7XHJcbmltcG9ydCB7IFJlQ2FwdGNoYU1vZHVsZSB9IGZyb20gJ2FuZ3VsYXIyLXJlY2FwdGNoYSc7XHJcblxyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBTaGFyZWRNb2R1bGUsXHJcbiAgICAgICAgUmVDYXB0Y2hhTW9kdWxlLFxyXG4gICAgICAgIEVucm9sbWVudEZvcm1Nb2R1bGUsXHJcbiAgICAgICAgRW5yb2xtZW50UGFyZW50RWRpdE1vZHVsZSxcclxuICAgICAgICBFbnJvbG1lbnRHdWFyZGlhbkVkaXRNb2R1bGUsXHJcbiAgICAgICAgRW5yb2xtZW50Q29udGFjdEVkaXRNb2R1bGUsXHJcbiAgICAgICAgRW5yb2xtZW50S2lkRWRpdE1vZHVsZVxyXG4gICAgXSxcclxuICAgIGRlY2xhcmF0aW9uczogW0Vucm9sbWVudENvbXBvbmVudF0sXHJcbiAgICBwcm92aWRlcnM6IFtFbnJvbG1lbnRTZXJ2aWNlLCBFbnJvbG1lbnRBY2Nlc3NHdWFyZFNlcnZpY2UsIEVucm9sbWVudEZvcm1Db25zZW50UmVzb2x2ZXJdXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgRW5yb2xtZW50TW9kdWxlIHsgfVxyXG4iXX0=
