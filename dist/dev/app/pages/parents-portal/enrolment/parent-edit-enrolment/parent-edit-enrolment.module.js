"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../../shared/index');
var index_2 = require('./index');
var EnrolmentParentEditModule = (function () {
    function EnrolmentParentEditModule() {
    }
    EnrolmentParentEditModule = __decorate([
        core_1.NgModule({
            imports: [index_1.SharedModule],
            declarations: [index_2.EnrolmentParentEditComponent],
            exports: [index_2.EnrolmentParentEditComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], EnrolmentParentEditModule);
    return EnrolmentParentEditModule;
}());
exports.EnrolmentParentEditModule = EnrolmentParentEditModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvcGFyZW50LWVkaXQtZW5yb2xtZW50L3BhcmVudC1lZGl0LWVucm9sbWVudC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUF5QixlQUFlLENBQUMsQ0FBQTtBQUN6QyxzQkFBNkIsMEJBQTBCLENBQUMsQ0FBQTtBQUV4RCxzQkFBNkMsU0FBUyxDQUFDLENBQUE7QUFRdkQ7SUFBQTtJQUF5QyxDQUFDO0lBTjFDO1FBQUMsZUFBUSxDQUFDO1lBQ1IsT0FBTyxFQUFFLENBQUMsb0JBQVksQ0FBQztZQUNyQixZQUFZLEVBQUUsQ0FBQyxvQ0FBNEIsQ0FBQztZQUM1QyxPQUFPLEVBQUUsQ0FBQyxvQ0FBNEIsQ0FBQztTQUMxQyxDQUFDOztpQ0FBQTtJQUV1QyxnQ0FBQztBQUFELENBQXpDLEFBQTBDLElBQUE7QUFBN0IsaUNBQXlCLDRCQUFJLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL3BhcmVudHMtcG9ydGFsL2Vucm9sbWVudC9wYXJlbnQtZWRpdC1lbnJvbG1lbnQvcGFyZW50LWVkaXQtZW5yb2xtZW50Lm1vZHVsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFNoYXJlZE1vZHVsZSB9IGZyb20gJy4uLy4uLy4uLy4uL3NoYXJlZC9pbmRleCc7XHJcblxyXG5pbXBvcnQgeyBFbnJvbG1lbnRQYXJlbnRFZGl0Q29tcG9uZW50IH0gZnJvbSAnLi9pbmRleCc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGltcG9ydHM6IFtTaGFyZWRNb2R1bGVdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBbRW5yb2xtZW50UGFyZW50RWRpdENvbXBvbmVudF0sXHJcbiAgICBleHBvcnRzOiBbRW5yb2xtZW50UGFyZW50RWRpdENvbXBvbmVudF1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBFbnJvbG1lbnRQYXJlbnRFZGl0TW9kdWxlIHsgfVxyXG4iXX0=
