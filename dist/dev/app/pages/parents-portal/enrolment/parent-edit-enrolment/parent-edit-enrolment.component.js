"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var router_1 = require('@angular/router');
var index_1 = require('../index');
var index_2 = require('../../index');
var index_3 = require('../../../../api/index');
var angular2_toaster_1 = require('angular2-toaster');
var index_4 = require('../../../../shared/index');
var EnrolmentParentEditComponent = (function () {
    function EnrolmentParentEditComponent(_router, _route, _enrolmentService, _centreService, _toasterService, fb) {
        this._router = _router;
        this._route = _route;
        this._enrolmentService = _enrolmentService;
        this._centreService = _centreService;
        this._toasterService = _toasterService;
        this.maxDateOfBirth = null;
        this.parentDto = null;
        var today = new Date();
        this.maxDateOfBirth = new Date(today.getFullYear() - 16, today.getMonth()).toJSON().split('T')[0];
        this.form = fb.group({
            'title': ['', forms_1.Validators.required],
            'homePhone': ['', forms_1.Validators.maxLength(12)],
            'gender': ['', forms_1.Validators.required],
            'dateOfBirth': ['', forms_1.Validators.required],
            'nationality': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.maxLength(50)])],
            'streetAddress': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.maxLength(200)])],
            'suburb': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.maxLength(50)])],
            'postcode': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.pattern('(\\d{4})')])],
            'state': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.maxLength(50)])],
            'crn': ['', forms_1.Validators.pattern('[0-9]{9}[a-zA-Z]')],
            'familyEthinicBackground': ['', forms_1.Validators.maxLength(1000)],
            'religiousOrCulturalPractices': ['', forms_1.Validators.maxLength(1000)],
            'predominantLanguagesAtHome': ['', forms_1.Validators.maxLength(1000)],
            'otherDetails': ['', forms_1.Validators.maxLength(1000)]
        });
        this.title = this.form.controls['title'];
        this.homePhone = this.form.controls['homePhone'];
        this.gender = this.form.controls['gender'];
        this.dateOfBirth = this.form.controls['dateOfBirth'];
        this.nationality = this.form.controls['nationality'];
        this.streetAddress = this.form.controls['streetAddress'];
        this.suburb = this.form.controls['suburb'];
        this.postcode = this.form.controls['postcode'];
        this.state = this.form.controls['state'];
        this.crn = this.form.controls['crn'];
        this.familyEthinicBackground = this.form.controls['familyEthinicBackground'];
        this.religiousOrCulturalPractices = this.form.controls['religiousOrCulturalPractices'];
        this.predominantLanguagesAtHome = this.form.controls['predominantLanguagesAtHome'];
        this.otherDetails = this.form.controls['otherDetails'];
    }
    EnrolmentParentEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._enrolmentService
            .getParent$()
            .subscribe(function (dto) {
            _this.parentDto = dto;
        });
    };
    EnrolmentParentEditComponent.prototype.saveChanges = function () {
        var _this = this;
        var savingToast = this._toasterService.pop(new index_4.SpinningToast());
        var updateParent = new index_3.UpdateParentDto();
        updateParent.init(this.parentDto.toJSON());
        this._enrolmentService
            .updateParent$(updateParent)
            .subscribe(function (res) {
            _this.parentDto = res;
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this._toasterService.popAsync('success', 'Saved', '');
            _this._router.navigate(['../../enrolment-form'], { relativeTo: _this._route });
        }, function (error) {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
        });
    };
    EnrolmentParentEditComponent.prototype.cancelEdit = function () {
        this._router.navigate(['../../enrolment-form'], { relativeTo: this._route });
    };
    EnrolmentParentEditComponent.prototype.onDateOfBirthChanged = function (date) {
        this.parentDto.dateOfBirth = new Date(date.setUTCMinutes(-date.getTimezoneOffset()));
    };
    EnrolmentParentEditComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            encapsulation: core_1.ViewEncapsulation.None,
            selector: 'parent-edit-enrolment-cmp',
            templateUrl: 'parent-edit-enrolment.component.html',
            styleUrls: ['parent-edit-enrolment.component.css']
        }), 
        __metadata('design:paramtypes', [router_1.Router, router_1.ActivatedRoute, index_1.EnrolmentService, index_2.CentreService, angular2_toaster_1.ToasterService, forms_1.FormBuilder])
    ], EnrolmentParentEditComponent);
    return EnrolmentParentEditComponent;
}());
exports.EnrolmentParentEditComponent = EnrolmentParentEditComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvcGFyZW50LWVkaXQtZW5yb2xtZW50L3BhcmVudC1lZGl0LWVucm9sbWVudC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUFxRCxlQUFlLENBQUMsQ0FBQTtBQUNyRSxzQkFBb0UsZ0JBQWdCLENBQUMsQ0FBQTtBQUNyRix1QkFBdUMsaUJBQWlCLENBQUMsQ0FBQTtBQUN6RCxzQkFBaUMsVUFBVSxDQUFDLENBQUE7QUFDNUMsc0JBQThCLGFBQWEsQ0FBQyxDQUFBO0FBQzVDLHNCQUEyQyx1QkFBdUIsQ0FBQyxDQUFBO0FBQ25FLGlDQUErQixrQkFBa0IsQ0FBQyxDQUFBO0FBQ2xELHNCQUE4QiwwQkFBMEIsQ0FBQyxDQUFBO0FBVXpEO0lBbUJFLHNDQUFvQixPQUFlLEVBQ3pCLE1BQXNCLEVBQ3RCLGlCQUFtQyxFQUNuQyxjQUE2QixFQUM3QixlQUErQixFQUN2QyxFQUFlO1FBTEcsWUFBTyxHQUFQLE9BQU8sQ0FBUTtRQUN6QixXQUFNLEdBQU4sTUFBTSxDQUFnQjtRQUN0QixzQkFBaUIsR0FBakIsaUJBQWlCLENBQWtCO1FBQ25DLG1CQUFjLEdBQWQsY0FBYyxDQUFlO1FBQzdCLG9CQUFlLEdBQWYsZUFBZSxDQUFnQjtRQXRCbEMsbUJBQWMsR0FBVyxJQUFJLENBQUM7UUFDOUIsY0FBUyxHQUFjLElBQUksQ0FBQztRQXdCakMsSUFBSSxLQUFLLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUN2QixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUUsR0FBRyxFQUFFLEVBQUUsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRWxHLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQztZQUNuQixPQUFPLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxRQUFRLENBQUM7WUFDbEMsV0FBVyxFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQzNDLFFBQVEsRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLFFBQVEsQ0FBQztZQUNuQyxhQUFhLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxRQUFRLENBQUM7WUFDeEMsYUFBYSxFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsa0JBQVUsQ0FBQyxRQUFRLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3hGLGVBQWUsRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLGtCQUFVLENBQUMsUUFBUSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMzRixRQUFRLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbkYsVUFBVSxFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsa0JBQVUsQ0FBQyxRQUFRLEVBQUUsa0JBQVUsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzNGLE9BQU8sRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLGtCQUFVLENBQUMsUUFBUSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNsRixLQUFLLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUMsQ0FBQztZQUNuRCx5QkFBeUIsRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMzRCw4QkFBOEIsRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNoRSw0QkFBNEIsRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM5RCxjQUFjLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDakQsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ2pELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDM0MsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNyRCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ3JELElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDekQsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMzQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQy9DLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDekMsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNyQyxJQUFJLENBQUMsdUJBQXVCLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMseUJBQXlCLENBQUMsQ0FBQztRQUM3RSxJQUFJLENBQUMsNEJBQTRCLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsOEJBQThCLENBQUMsQ0FBQztRQUN2RixJQUFJLENBQUMsMEJBQTBCLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsNEJBQTRCLENBQUMsQ0FBQztRQUNuRixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQ3pELENBQUM7SUFFRCwrQ0FBUSxHQUFSO1FBQUEsaUJBT0M7UUFMQyxJQUFJLENBQUMsaUJBQWlCO2FBQ25CLFVBQVUsRUFBRTthQUNaLFNBQVMsQ0FBQyxVQUFBLEdBQUc7WUFDWixLQUFJLENBQUMsU0FBUyxHQUFHLEdBQUcsQ0FBQztRQUN2QixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxrREFBVyxHQUFYO1FBQUEsaUJBZUM7UUFkQyxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxJQUFJLHFCQUFhLEVBQUUsQ0FBQyxDQUFDO1FBQ2hFLElBQUksWUFBWSxHQUFHLElBQUksdUJBQWUsRUFBRSxDQUFDO1FBQ3pDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO1FBQzNDLElBQUksQ0FBQyxpQkFBaUI7YUFDbkIsYUFBYSxDQUFDLFlBQVksQ0FBQzthQUMzQixTQUFTLENBQUMsVUFBQSxHQUFHO1lBQ1osS0FBSSxDQUFDLFNBQVMsR0FBRyxHQUFHLENBQUM7WUFDckIsS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxXQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUM5RSxLQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsT0FBTyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQ3RELEtBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsc0JBQXNCLENBQUMsRUFBRSxFQUFFLFVBQVUsRUFBRSxLQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQztRQUMvRSxDQUFDLEVBQ0QsVUFBQyxLQUFLO1lBQ0osS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxXQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUNoRixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxpREFBVSxHQUFWO1FBQ0UsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxzQkFBc0IsQ0FBQyxFQUFFLEVBQUUsVUFBVSxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO0lBQy9FLENBQUM7SUFFRCwyREFBb0IsR0FBcEIsVUFBcUIsSUFBVTtRQUM3QixJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsR0FBRyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ3ZGLENBQUM7SUFyR0g7UUFBQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLGFBQWEsRUFBRSx3QkFBaUIsQ0FBQyxJQUFJO1lBQ3JDLFFBQVEsRUFBRSwyQkFBMkI7WUFDckMsV0FBVyxFQUFFLHNDQUFzQztZQUNuRCxTQUFTLEVBQUUsQ0FBQyxxQ0FBcUMsQ0FBQztTQUNuRCxDQUFDOztvQ0FBQTtJQWdHRixtQ0FBQztBQUFELENBOUZBLEFBOEZDLElBQUE7QUE5Rlksb0NBQTRCLCtCQThGeEMsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvcGFyZW50cy1wb3J0YWwvZW5yb2xtZW50L3BhcmVudC1lZGl0LWVucm9sbWVudC9wYXJlbnQtZWRpdC1lbnJvbG1lbnQuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1Hcm91cCwgQWJzdHJhY3RDb250cm9sLCBGb3JtQnVpbGRlciwgVmFsaWRhdG9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgUm91dGVyLCBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IEVucm9sbWVudFNlcnZpY2UgfSBmcm9tICcuLi9pbmRleCc7XHJcbmltcG9ydCB7IENlbnRyZVNlcnZpY2UgfSBmcm9tICcuLi8uLi9pbmRleCc7XHJcbmltcG9ydCB7IFBhcmVudER0bywgVXBkYXRlUGFyZW50RHRvIH0gZnJvbSAnLi4vLi4vLi4vLi4vYXBpL2luZGV4JztcclxuaW1wb3J0IHsgVG9hc3RlclNlcnZpY2UgfSBmcm9tICdhbmd1bGFyMi10b2FzdGVyJztcclxuaW1wb3J0IHsgU3Bpbm5pbmdUb2FzdCB9IGZyb20gJy4uLy4uLy4uLy4uL3NoYXJlZC9pbmRleCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmUsXHJcbiAgc2VsZWN0b3I6ICdwYXJlbnQtZWRpdC1lbnJvbG1lbnQtY21wJyxcclxuICB0ZW1wbGF0ZVVybDogJ3BhcmVudC1lZGl0LWVucm9sbWVudC5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJ3BhcmVudC1lZGl0LWVucm9sbWVudC5jb21wb25lbnQuY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBFbnJvbG1lbnRQYXJlbnRFZGl0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBwdWJsaWMgbWF4RGF0ZU9mQmlydGg6IHN0cmluZyA9IG51bGw7XHJcbiAgcHVibGljIHBhcmVudER0bzogUGFyZW50RHRvID0gbnVsbDtcclxuICBwcml2YXRlIGZvcm06IEZvcm1Hcm91cDtcclxuICBwcml2YXRlIHRpdGxlOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBob21lUGhvbmU6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIGdlbmRlcjogQWJzdHJhY3RDb250cm9sO1xyXG4gIHByaXZhdGUgZGF0ZU9mQmlydGg6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIG5hdGlvbmFsaXR5OiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBzdHJlZXRBZGRyZXNzOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBzdWJ1cmI6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIHBvc3Rjb2RlOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBzdGF0ZTogQWJzdHJhY3RDb250cm9sO1xyXG4gIHByaXZhdGUgY3JuOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBmYW1pbHlFdGhpbmljQmFja2dyb3VuZDogQWJzdHJhY3RDb250cm9sO1xyXG4gIHByaXZhdGUgcmVsaWdpb3VzT3JDdWx0dXJhbFByYWN0aWNlczogQWJzdHJhY3RDb250cm9sO1xyXG4gIHByaXZhdGUgcHJlZG9taW5hbnRMYW5ndWFnZXNBdEhvbWU6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIG90aGVyRGV0YWlsczogQWJzdHJhY3RDb250cm9sO1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9yb3V0ZXI6IFJvdXRlcixcclxuICAgIHByaXZhdGUgX3JvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcclxuICAgIHByaXZhdGUgX2Vucm9sbWVudFNlcnZpY2U6IEVucm9sbWVudFNlcnZpY2UsXHJcbiAgICBwcml2YXRlIF9jZW50cmVTZXJ2aWNlOiBDZW50cmVTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBfdG9hc3RlclNlcnZpY2U6IFRvYXN0ZXJTZXJ2aWNlLFxyXG4gICAgZmI6IEZvcm1CdWlsZGVyKSB7XHJcblxyXG4gICAgbGV0IHRvZGF5ID0gbmV3IERhdGUoKTtcclxuICAgIHRoaXMubWF4RGF0ZU9mQmlydGggPSBuZXcgRGF0ZSh0b2RheS5nZXRGdWxsWWVhcigpIC0gMTYsIHRvZGF5LmdldE1vbnRoKCkpLnRvSlNPTigpLnNwbGl0KCdUJylbMF07XHJcblxyXG4gICAgdGhpcy5mb3JtID0gZmIuZ3JvdXAoe1xyXG4gICAgICAndGl0bGUnOiBbJycsIFZhbGlkYXRvcnMucmVxdWlyZWRdLFxyXG4gICAgICAnaG9tZVBob25lJzogWycnLCBWYWxpZGF0b3JzLm1heExlbmd0aCgxMildLFxyXG4gICAgICAnZ2VuZGVyJzogWycnLCBWYWxpZGF0b3JzLnJlcXVpcmVkXSxcclxuICAgICAgJ2RhdGVPZkJpcnRoJzogWycnLCBWYWxpZGF0b3JzLnJlcXVpcmVkXSxcclxuICAgICAgJ25hdGlvbmFsaXR5JzogWycnLCBWYWxpZGF0b3JzLmNvbXBvc2UoW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDUwKV0pXSxcclxuICAgICAgJ3N0cmVldEFkZHJlc3MnOiBbJycsIFZhbGlkYXRvcnMuY29tcG9zZShbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMjAwKV0pXSxcclxuICAgICAgJ3N1YnVyYic6IFsnJywgVmFsaWRhdG9ycy5jb21wb3NlKFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1heExlbmd0aCg1MCldKV0sXHJcbiAgICAgICdwb3N0Y29kZSc6IFsnJywgVmFsaWRhdG9ycy5jb21wb3NlKFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLnBhdHRlcm4oJyhcXFxcZHs0fSknKV0pXSxcclxuICAgICAgJ3N0YXRlJzogWycnLCBWYWxpZGF0b3JzLmNvbXBvc2UoW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDUwKV0pXSxcclxuICAgICAgJ2Nybic6IFsnJywgVmFsaWRhdG9ycy5wYXR0ZXJuKCdbMC05XXs5fVthLXpBLVpdJyldLFxyXG4gICAgICAnZmFtaWx5RXRoaW5pY0JhY2tncm91bmQnOiBbJycsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDEwMDApXSxcclxuICAgICAgJ3JlbGlnaW91c09yQ3VsdHVyYWxQcmFjdGljZXMnOiBbJycsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDEwMDApXSxcclxuICAgICAgJ3ByZWRvbWluYW50TGFuZ3VhZ2VzQXRIb21lJzogWycnLCBWYWxpZGF0b3JzLm1heExlbmd0aCgxMDAwKV0sXHJcbiAgICAgICdvdGhlckRldGFpbHMnOiBbJycsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDEwMDApXVxyXG4gICAgfSk7XHJcbiAgICB0aGlzLnRpdGxlID0gdGhpcy5mb3JtLmNvbnRyb2xzWyd0aXRsZSddO1xyXG4gICAgdGhpcy5ob21lUGhvbmUgPSB0aGlzLmZvcm0uY29udHJvbHNbJ2hvbWVQaG9uZSddO1xyXG4gICAgdGhpcy5nZW5kZXIgPSB0aGlzLmZvcm0uY29udHJvbHNbJ2dlbmRlciddO1xyXG4gICAgdGhpcy5kYXRlT2ZCaXJ0aCA9IHRoaXMuZm9ybS5jb250cm9sc1snZGF0ZU9mQmlydGgnXTtcclxuICAgIHRoaXMubmF0aW9uYWxpdHkgPSB0aGlzLmZvcm0uY29udHJvbHNbJ25hdGlvbmFsaXR5J107XHJcbiAgICB0aGlzLnN0cmVldEFkZHJlc3MgPSB0aGlzLmZvcm0uY29udHJvbHNbJ3N0cmVldEFkZHJlc3MnXTtcclxuICAgIHRoaXMuc3VidXJiID0gdGhpcy5mb3JtLmNvbnRyb2xzWydzdWJ1cmInXTtcclxuICAgIHRoaXMucG9zdGNvZGUgPSB0aGlzLmZvcm0uY29udHJvbHNbJ3Bvc3Rjb2RlJ107XHJcbiAgICB0aGlzLnN0YXRlID0gdGhpcy5mb3JtLmNvbnRyb2xzWydzdGF0ZSddO1xyXG4gICAgdGhpcy5jcm4gPSB0aGlzLmZvcm0uY29udHJvbHNbJ2NybiddO1xyXG4gICAgdGhpcy5mYW1pbHlFdGhpbmljQmFja2dyb3VuZCA9IHRoaXMuZm9ybS5jb250cm9sc1snZmFtaWx5RXRoaW5pY0JhY2tncm91bmQnXTtcclxuICAgIHRoaXMucmVsaWdpb3VzT3JDdWx0dXJhbFByYWN0aWNlcyA9IHRoaXMuZm9ybS5jb250cm9sc1sncmVsaWdpb3VzT3JDdWx0dXJhbFByYWN0aWNlcyddO1xyXG4gICAgdGhpcy5wcmVkb21pbmFudExhbmd1YWdlc0F0SG9tZSA9IHRoaXMuZm9ybS5jb250cm9sc1sncHJlZG9taW5hbnRMYW5ndWFnZXNBdEhvbWUnXTtcclxuICAgIHRoaXMub3RoZXJEZXRhaWxzID0gdGhpcy5mb3JtLmNvbnRyb2xzWydvdGhlckRldGFpbHMnXTtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG5cclxuICAgIHRoaXMuX2Vucm9sbWVudFNlcnZpY2VcclxuICAgICAgLmdldFBhcmVudCQoKVxyXG4gICAgICAuc3Vic2NyaWJlKGR0byA9PiB7XHJcbiAgICAgICAgdGhpcy5wYXJlbnREdG8gPSBkdG87XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgc2F2ZUNoYW5nZXMoKSB7XHJcbiAgICBsZXQgc2F2aW5nVG9hc3QgPSB0aGlzLl90b2FzdGVyU2VydmljZS5wb3AobmV3IFNwaW5uaW5nVG9hc3QoKSk7XHJcbiAgICBsZXQgdXBkYXRlUGFyZW50ID0gbmV3IFVwZGF0ZVBhcmVudER0bygpO1xyXG4gICAgdXBkYXRlUGFyZW50LmluaXQodGhpcy5wYXJlbnREdG8udG9KU09OKCkpO1xyXG4gICAgdGhpcy5fZW5yb2xtZW50U2VydmljZVxyXG4gICAgICAudXBkYXRlUGFyZW50JCh1cGRhdGVQYXJlbnQpXHJcbiAgICAgIC5zdWJzY3JpYmUocmVzID0+IHtcclxuICAgICAgICB0aGlzLnBhcmVudER0byA9IHJlcztcclxuICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcihzYXZpbmdUb2FzdC50b2FzdElkLCBzYXZpbmdUb2FzdC50b2FzdENvbnRhaW5lcklkKTtcclxuICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5wb3BBc3luYygnc3VjY2VzcycsICdTYXZlZCcsICcnKTtcclxuICAgICAgICB0aGlzLl9yb3V0ZXIubmF2aWdhdGUoWycuLi8uLi9lbnJvbG1lbnQtZm9ybSddLCB7IHJlbGF0aXZlVG86IHRoaXMuX3JvdXRlIH0pO1xyXG4gICAgICB9LFxyXG4gICAgICAoZXJyb3IpID0+IHtcclxuICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcihzYXZpbmdUb2FzdC50b2FzdElkLCBzYXZpbmdUb2FzdC50b2FzdENvbnRhaW5lcklkKTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBjYW5jZWxFZGl0KCkge1xyXG4gICAgdGhpcy5fcm91dGVyLm5hdmlnYXRlKFsnLi4vLi4vZW5yb2xtZW50LWZvcm0nXSwgeyByZWxhdGl2ZVRvOiB0aGlzLl9yb3V0ZSB9KTtcclxuICB9XHJcblxyXG4gIG9uRGF0ZU9mQmlydGhDaGFuZ2VkKGRhdGU6IERhdGUpOiB2b2lkIHtcclxuICAgIHRoaXMucGFyZW50RHRvLmRhdGVPZkJpcnRoID0gbmV3IERhdGUoZGF0ZS5zZXRVVENNaW51dGVzKC1kYXRlLmdldFRpbWV6b25lT2Zmc2V0KCkpKTtcclxuICB9XHJcbn1cclxuIl19
