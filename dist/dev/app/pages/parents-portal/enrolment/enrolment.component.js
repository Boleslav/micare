"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var ng2_bootstrap_1 = require('ng2-bootstrap');
var angular2_toaster_1 = require('angular2-toaster');
var index_1 = require('../../../shared/index');
var index_2 = require('./index');
var index_3 = require('../index');
var EnrolmentComponent = (function () {
    function EnrolmentComponent(_enrolmentService, _centreService, _toasterService, _route, _router) {
        this._enrolmentService = _enrolmentService;
        this._centreService = _centreService;
        this._toasterService = _toasterService;
        this._route = _route;
        this._router = _router;
        this.enrolmentFormConsent = null;
        this.centreName = null;
    }
    EnrolmentComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.centreName = this._centreService.SelectedCentreQualifiedName;
        this._route
            .data
            .subscribe(function (data) {
            if (data.enrolmentFormConsent) {
                _this.enrolmentFormConsent = true;
                _this._router.navigate(['enrolment-form'], { relativeTo: _this._route });
            }
            else {
                _this.enrolmentFormConsent = false;
            }
        });
    };
    EnrolmentComponent.prototype.ngAfterViewInit = function () {
        if (!this.enrolmentFormConsent)
            this.staticModal.show();
    };
    EnrolmentComponent.prototype.ngOnDestroy = function () {
        this._enrolmentService.clear();
    };
    EnrolmentComponent.prototype.grantAccess = function () {
        var _this = this;
        this._enrolmentService
            .grantEnrolmentFormAccess$(this._centreService.SelectedCentreId)
            .subscribe(function () {
            _this.staticModal.hide();
            _this._router.navigate(['enrolment-form'], { relativeTo: _this._route });
        });
    };
    EnrolmentComponent.prototype.denyAccess = function () {
        var _this = this;
        var savingToast = this._toasterService.pop(new index_1.SpinningToast('Logging out'));
        this._enrolmentService
            .denyEnrolmentFormAccess$(this._centreService.SelectedCentreId)
            .subscribe(function () {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this._router.navigate(['parent-logout', _this._centreService.SelectedCentreUniqueName]);
        });
    };
    __decorate([
        core_1.ViewChild('staticModal'), 
        __metadata('design:type', ng2_bootstrap_1.ModalDirective)
    ], EnrolmentComponent.prototype, "staticModal", void 0);
    EnrolmentComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'enrolment-cmp',
            templateUrl: 'enrolment.component.html',
            styleUrls: ['./enrolment.component.css']
        }), 
        __metadata('design:paramtypes', [index_2.EnrolmentService, index_3.CentreService, angular2_toaster_1.ToasterService, router_1.ActivatedRoute, router_1.Router])
    ], EnrolmentComponent);
    return EnrolmentComponent;
}());
exports.EnrolmentComponent = EnrolmentComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvZW5yb2xtZW50LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQXVFLGVBQWUsQ0FBQyxDQUFBO0FBQ3ZGLHVCQUF1QyxpQkFBaUIsQ0FBQyxDQUFBO0FBQ3pELDhCQUErQixlQUFlLENBQUMsQ0FBQTtBQUMvQyxpQ0FBK0Isa0JBQWtCLENBQUMsQ0FBQTtBQUNsRCxzQkFBOEIsdUJBQXVCLENBQUMsQ0FBQTtBQUN0RCxzQkFBaUMsU0FBUyxDQUFDLENBQUE7QUFDM0Msc0JBQThCLFVBQVUsQ0FBQyxDQUFBO0FBU3pDO0lBS0UsNEJBQ1UsaUJBQW1DLEVBQ25DLGNBQTZCLEVBQzdCLGVBQStCLEVBQy9CLE1BQXNCLEVBQ3RCLE9BQWU7UUFKZixzQkFBaUIsR0FBakIsaUJBQWlCLENBQWtCO1FBQ25DLG1CQUFjLEdBQWQsY0FBYyxDQUFlO1FBQzdCLG9CQUFlLEdBQWYsZUFBZSxDQUFnQjtRQUMvQixXQUFNLEdBQU4sTUFBTSxDQUFnQjtRQUN0QixZQUFPLEdBQVAsT0FBTyxDQUFRO1FBUGpCLHlCQUFvQixHQUFZLElBQUksQ0FBQztRQUNyQyxlQUFVLEdBQVcsSUFBSSxDQUFDO0lBUWxDLENBQUM7SUFFRCxxQ0FBUSxHQUFSO1FBQUEsaUJBWUM7UUFYQyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsMkJBQTJCLENBQUM7UUFDbEUsSUFBSSxDQUFDLE1BQU07YUFDUixJQUFJO2FBQ0osU0FBUyxDQUFDLFVBQUMsSUFBdUM7WUFDakQsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQztnQkFDOUIsS0FBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQztnQkFDakMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLEVBQUUsVUFBVSxFQUFFLEtBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO1lBQ3pFLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTixLQUFJLENBQUMsb0JBQW9CLEdBQUcsS0FBSyxDQUFDO1lBQ3BDLENBQUM7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCw0Q0FBZSxHQUFmO1FBQ0UsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUM7WUFDN0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUM1QixDQUFDO0lBRUQsd0NBQVcsR0FBWDtRQUNFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUNqQyxDQUFDO0lBRUQsd0NBQVcsR0FBWDtRQUFBLGlCQU9DO1FBTkMsSUFBSSxDQUFDLGlCQUFpQjthQUNuQix5QkFBeUIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDO2FBQy9ELFNBQVMsQ0FBQztZQUNULEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDeEIsS0FBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLEVBQUUsVUFBVSxFQUFFLEtBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO1FBQ3pFLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHVDQUFVLEdBQVY7UUFBQSxpQkFRQztRQVBDLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUkscUJBQWEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO1FBQzdFLElBQUksQ0FBQyxpQkFBaUI7YUFDbkIsd0JBQXdCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQzthQUM5RCxTQUFTLENBQUM7WUFDVCxLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1lBQzlFLEtBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsZUFBZSxFQUFFLEtBQUksQ0FBQyxjQUFjLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxDQUFDO1FBQ3pGLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQXBERDtRQUFDLGdCQUFTLENBQUMsYUFBYSxDQUFDOzsyREFBQTtJQVQzQjtRQUFDLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLGVBQWU7WUFDekIsV0FBVyxFQUFFLDBCQUEwQjtZQUN2QyxTQUFTLEVBQUUsQ0FBQywyQkFBMkIsQ0FBQztTQUN6QyxDQUFDOzswQkFBQTtJQXlERix5QkFBQztBQUFELENBdkRBLEFBdURDLElBQUE7QUF2RFksMEJBQWtCLHFCQXVEOUIsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvcGFyZW50cy1wb3J0YWwvZW5yb2xtZW50L2Vucm9sbWVudC5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgT25EZXN0cm95LCBWaWV3Q2hpbGQsIEFmdGVyVmlld0luaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUsIFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IE1vZGFsRGlyZWN0aXZlIH0gZnJvbSAnbmcyLWJvb3RzdHJhcCc7XHJcbmltcG9ydCB7IFRvYXN0ZXJTZXJ2aWNlIH0gZnJvbSAnYW5ndWxhcjItdG9hc3Rlcic7XHJcbmltcG9ydCB7IFNwaW5uaW5nVG9hc3QgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvaW5kZXgnO1xyXG5pbXBvcnQgeyBFbnJvbG1lbnRTZXJ2aWNlIH0gZnJvbSAnLi9pbmRleCc7XHJcbmltcG9ydCB7IENlbnRyZVNlcnZpY2UgfSBmcm9tICcuLi9pbmRleCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gIHNlbGVjdG9yOiAnZW5yb2xtZW50LWNtcCcsXHJcbiAgdGVtcGxhdGVVcmw6ICdlbnJvbG1lbnQuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2Vucm9sbWVudC5jb21wb25lbnQuY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBFbnJvbG1lbnRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSwgQWZ0ZXJWaWV3SW5pdCB7XHJcblxyXG4gIEBWaWV3Q2hpbGQoJ3N0YXRpY01vZGFsJykgcHVibGljIHN0YXRpY01vZGFsOiBNb2RhbERpcmVjdGl2ZTtcclxuICBwcml2YXRlIGVucm9sbWVudEZvcm1Db25zZW50OiBib29sZWFuID0gbnVsbDtcclxuICBwcml2YXRlIGNlbnRyZU5hbWU6IHN0cmluZyA9IG51bGw7XHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIF9lbnJvbG1lbnRTZXJ2aWNlOiBFbnJvbG1lbnRTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBfY2VudHJlU2VydmljZTogQ2VudHJlU2VydmljZSxcclxuICAgIHByaXZhdGUgX3RvYXN0ZXJTZXJ2aWNlOiBUb2FzdGVyU2VydmljZSxcclxuICAgIHByaXZhdGUgX3JvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcclxuICAgIHByaXZhdGUgX3JvdXRlcjogUm91dGVyKSB7XHJcblxyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLmNlbnRyZU5hbWUgPSB0aGlzLl9jZW50cmVTZXJ2aWNlLlNlbGVjdGVkQ2VudHJlUXVhbGlmaWVkTmFtZTtcclxuICAgIHRoaXMuX3JvdXRlXHJcbiAgICAgIC5kYXRhXHJcbiAgICAgIC5zdWJzY3JpYmUoKGRhdGE6IHsgZW5yb2xtZW50Rm9ybUNvbnNlbnQ6IGJvb2xlYW4gfSkgPT4ge1xyXG4gICAgICAgIGlmIChkYXRhLmVucm9sbWVudEZvcm1Db25zZW50KSB7XHJcbiAgICAgICAgICB0aGlzLmVucm9sbWVudEZvcm1Db25zZW50ID0gdHJ1ZTtcclxuICAgICAgICAgIHRoaXMuX3JvdXRlci5uYXZpZ2F0ZShbJ2Vucm9sbWVudC1mb3JtJ10sIHsgcmVsYXRpdmVUbzogdGhpcy5fcm91dGUgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRoaXMuZW5yb2xtZW50Rm9ybUNvbnNlbnQgPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgbmdBZnRlclZpZXdJbml0KCk6IHZvaWQge1xyXG4gICAgaWYgKCF0aGlzLmVucm9sbWVudEZvcm1Db25zZW50KVxyXG4gICAgICB0aGlzLnN0YXRpY01vZGFsLnNob3coKTtcclxuICB9XHJcblxyXG4gIG5nT25EZXN0cm95KCk6IHZvaWQge1xyXG4gICAgdGhpcy5fZW5yb2xtZW50U2VydmljZS5jbGVhcigpO1xyXG4gIH1cclxuXHJcbiAgZ3JhbnRBY2Nlc3MoKTogdm9pZCB7XHJcbiAgICB0aGlzLl9lbnJvbG1lbnRTZXJ2aWNlXHJcbiAgICAgIC5ncmFudEVucm9sbWVudEZvcm1BY2Nlc3MkKHRoaXMuX2NlbnRyZVNlcnZpY2UuU2VsZWN0ZWRDZW50cmVJZClcclxuICAgICAgLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zdGF0aWNNb2RhbC5oaWRlKCk7XHJcbiAgICAgICAgdGhpcy5fcm91dGVyLm5hdmlnYXRlKFsnZW5yb2xtZW50LWZvcm0nXSwgeyByZWxhdGl2ZVRvOiB0aGlzLl9yb3V0ZSB9KTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBkZW55QWNjZXNzKCk6IHZvaWQge1xyXG4gICAgbGV0IHNhdmluZ1RvYXN0ID0gdGhpcy5fdG9hc3RlclNlcnZpY2UucG9wKG5ldyBTcGlubmluZ1RvYXN0KCdMb2dnaW5nIG91dCcpKTtcclxuICAgIHRoaXMuX2Vucm9sbWVudFNlcnZpY2VcclxuICAgICAgLmRlbnlFbnJvbG1lbnRGb3JtQWNjZXNzJCh0aGlzLl9jZW50cmVTZXJ2aWNlLlNlbGVjdGVkQ2VudHJlSWQpXHJcbiAgICAgIC5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLmNsZWFyKHNhdmluZ1RvYXN0LnRvYXN0SWQsIHNhdmluZ1RvYXN0LnRvYXN0Q29udGFpbmVySWQpO1xyXG4gICAgICAgIHRoaXMuX3JvdXRlci5uYXZpZ2F0ZShbJ3BhcmVudC1sb2dvdXQnLCB0aGlzLl9jZW50cmVTZXJ2aWNlLlNlbGVjdGVkQ2VudHJlVW5pcXVlTmFtZV0pO1xyXG4gICAgICB9KTtcclxuICB9XHJcbn1cclxuIl19
