"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var forms_1 = require('@angular/forms');
var angular2_toaster_1 = require('angular2-toaster');
var index_1 = require('../../index');
var index_2 = require('../../../../../api/index');
var index_3 = require('../../../../../shared/index');
var index_4 = require('../../../index');
var EnrolmentConsentComponent = (function () {
    function EnrolmentConsentComponent(_router, _route, _enrolmentService, _centreService, _toasterService, fb) {
        this._router = _router;
        this._route = _route;
        this._enrolmentService = _enrolmentService;
        this._centreService = _centreService;
        this._toasterService = _toasterService;
        this.onEnrolmentFormId = new core_1.EventEmitter();
        this.enrolmentFormDto = null;
        this.form = fb.group({
            'consentPolices': [''],
            'consentSunHat': [''],
            'consentCollect': [''],
            'consentAbsentWhenSick': [''],
            'consentInformationShare': [''],
            'consentFees': [''],
            'consentTrueAndCorrect': [''],
            'consentIdentity': ['']
        });
        this.consentPolices = this.form.controls['consentPolices'];
        this.consentSunHat = this.form.controls['consentSunHat'];
        this.consentCollect = this.form.controls['consentCollect'];
        this.consentAbsentWhenSick = this.form.controls['consentAbsentWhenSick'];
        this.consentInformationShare = this.form.controls['consentInformationShare'];
        this.consentFees = this.form.controls['consentFees'];
        this.consentTrueAndCorrect = this.form.controls['consentTrueAndCorrect'];
        this.consentIdentity = this.form.controls['consentIdentity'];
    }
    EnrolmentConsentComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._enrolmentService
            .getEnrolmentForm$(this._centreService.SelectedCentreId)
            .subscribe(function (res) {
            _this.enrolmentFormDto = res;
        });
    };
    EnrolmentConsentComponent.prototype.saveChanges = function () {
        var _this = this;
        var savingToast = this._toasterService.pop(new index_3.SpinningToast());
        var updateEnrolmentForm = new index_2.UpdateEnrolmentFormDto();
        updateEnrolmentForm.init(this.enrolmentFormDto.toJSON());
        this._enrolmentService
            .updateEnrolmentForm$(updateEnrolmentForm)
            .subscribe(function (res) {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this._toasterService.popAsync('success', 'Saved', '');
            _this.enrolmentFormDto = res;
        }, function (error) {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
        });
    };
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], EnrolmentConsentComponent.prototype, "onEnrolmentFormId", void 0);
    EnrolmentConsentComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            encapsulation: core_1.ViewEncapsulation.None,
            selector: 'enrolment-consent-cmp',
            templateUrl: 'enrolment-consent.component.html',
            styleUrls: ['enrolment-consent.component.css']
        }), 
        __metadata('design:paramtypes', [router_1.Router, router_1.ActivatedRoute, index_1.EnrolmentService, index_4.CentreService, angular2_toaster_1.ToasterService, forms_1.FormBuilder])
    ], EnrolmentConsentComponent);
    return EnrolmentConsentComponent;
}());
exports.EnrolmentConsentComponent = EnrolmentConsentComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvZW5yb2xtZW50LWZvcm0vZW5yb2xtZW50LWNvbnNlbnQvZW5yb2xtZW50LWNvbnNlbnQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBMkUsZUFBZSxDQUFDLENBQUE7QUFDM0YsdUJBQXVDLGlCQUFpQixDQUFDLENBQUE7QUFDekQsc0JBQXdELGdCQUFnQixDQUFDLENBQUE7QUFDekUsaUNBQStCLGtCQUFrQixDQUFDLENBQUE7QUFFbEQsc0JBQWlDLGFBQWEsQ0FBQyxDQUFBO0FBQy9DLHNCQUF5RCwwQkFBMEIsQ0FBQyxDQUFBO0FBQ3BGLHNCQUE4Qiw2QkFBNkIsQ0FBQyxDQUFBO0FBQzVELHNCQUE4QixnQkFBZ0IsQ0FBQyxDQUFBO0FBVS9DO0lBY0UsbUNBQW9CLE9BQWUsRUFDekIsTUFBc0IsRUFDdEIsaUJBQW1DLEVBQ25DLGNBQTZCLEVBQzdCLGVBQStCLEVBQ3ZDLEVBQWU7UUFMRyxZQUFPLEdBQVAsT0FBTyxDQUFRO1FBQ3pCLFdBQU0sR0FBTixNQUFNLENBQWdCO1FBQ3RCLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBa0I7UUFDbkMsbUJBQWMsR0FBZCxjQUFjLENBQWU7UUFDN0Isb0JBQWUsR0FBZixlQUFlLENBQWdCO1FBaEIvQixzQkFBaUIsR0FBRyxJQUFJLG1CQUFZLEVBQVUsQ0FBQztRQUNqRCxxQkFBZ0IsR0FBcUIsSUFBSSxDQUFDO1FBa0JoRCxJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQyxLQUFLLENBQUM7WUFDbkIsZ0JBQWdCLEVBQUUsQ0FBQyxFQUFFLENBQUM7WUFDdEIsZUFBZSxFQUFFLENBQUMsRUFBRSxDQUFDO1lBQ3JCLGdCQUFnQixFQUFFLENBQUMsRUFBRSxDQUFDO1lBQ3RCLHVCQUF1QixFQUFFLENBQUMsRUFBRSxDQUFDO1lBQzdCLHlCQUF5QixFQUFFLENBQUMsRUFBRSxDQUFDO1lBQy9CLGFBQWEsRUFBRSxDQUFDLEVBQUUsQ0FBQztZQUNuQix1QkFBdUIsRUFBRSxDQUFDLEVBQUUsQ0FBQztZQUM3QixpQkFBaUIsRUFBRSxDQUFDLEVBQUUsQ0FBQztTQUN4QixDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDM0QsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUN6RCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDM0QsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLHVCQUF1QixDQUFDLENBQUM7UUFDekUsSUFBSSxDQUFDLHVCQUF1QixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLHlCQUF5QixDQUFDLENBQUM7UUFDN0UsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNyRCxJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsdUJBQXVCLENBQUMsQ0FBQztRQUN6RSxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUVELDRDQUFRLEdBQVI7UUFBQSxpQkFNQztRQUxDLElBQUksQ0FBQyxpQkFBaUI7YUFDbkIsaUJBQWlCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQzthQUN2RCxTQUFTLENBQUMsVUFBQyxHQUFxQjtZQUMvQixLQUFJLENBQUMsZ0JBQWdCLEdBQUcsR0FBRyxDQUFDO1FBQzlCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELCtDQUFXLEdBQVg7UUFBQSxpQkFlQztRQWJDLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUkscUJBQWEsRUFBRSxDQUFDLENBQUM7UUFDaEUsSUFBSSxtQkFBbUIsR0FBRyxJQUFJLDhCQUFzQixFQUFFLENBQUM7UUFDdkQsbUJBQW1CLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO1FBQ3pELElBQUksQ0FBQyxpQkFBaUI7YUFDbkIsb0JBQW9CLENBQUMsbUJBQW1CLENBQUM7YUFDekMsU0FBUyxDQUFDLFVBQUEsR0FBRztZQUNaLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDOUUsS0FBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLE9BQU8sRUFBRSxFQUFFLENBQUMsQ0FBQztZQUN0RCxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsR0FBRyxDQUFDO1FBQzlCLENBQUMsRUFDRCxVQUFDLEtBQUs7WUFDSixLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ2hGLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQS9ERDtRQUFDLGFBQU0sRUFBRTs7d0VBQUE7SUFWWDtRQUFDLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsYUFBYSxFQUFFLHdCQUFpQixDQUFDLElBQUk7WUFDckMsUUFBUSxFQUFFLHVCQUF1QjtZQUNqQyxXQUFXLEVBQUUsa0NBQWtDO1lBQy9DLFNBQVMsRUFBRSxDQUFDLGlDQUFpQyxDQUFDO1NBQy9DLENBQUM7O2lDQUFBO0lBb0VGLGdDQUFDO0FBQUQsQ0FsRUEsQUFrRUMsSUFBQTtBQWxFWSxpQ0FBeUIsNEJBa0VyQyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvZW5yb2xtZW50LWZvcm0vZW5yb2xtZW50LWNvbnNlbnQvZW5yb2xtZW50LWNvbnNlbnQuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBWaWV3RW5jYXBzdWxhdGlvbiwgT25Jbml0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBSb3V0ZXIsIEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgRm9ybUdyb3VwLCBBYnN0cmFjdENvbnRyb2wsIEZvcm1CdWlsZGVyIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBUb2FzdGVyU2VydmljZSB9IGZyb20gJ2FuZ3VsYXIyLXRvYXN0ZXInO1xyXG5cclxuaW1wb3J0IHsgRW5yb2xtZW50U2VydmljZSB9IGZyb20gJy4uLy4uL2luZGV4JztcclxuaW1wb3J0IHsgRW5yb2xtZW50Rm9ybUR0bywgVXBkYXRlRW5yb2xtZW50Rm9ybUR0byB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL2FwaS9pbmRleCc7XHJcbmltcG9ydCB7IFNwaW5uaW5nVG9hc3QgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9zaGFyZWQvaW5kZXgnO1xyXG5pbXBvcnQgeyBDZW50cmVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vaW5kZXgnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lLFxyXG4gIHNlbGVjdG9yOiAnZW5yb2xtZW50LWNvbnNlbnQtY21wJyxcclxuICB0ZW1wbGF0ZVVybDogJ2Vucm9sbWVudC1jb25zZW50LmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnZW5yb2xtZW50LWNvbnNlbnQuY29tcG9uZW50LmNzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgRW5yb2xtZW50Q29uc2VudENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIEBPdXRwdXQoKSBvbkVucm9sbWVudEZvcm1JZCA9IG5ldyBFdmVudEVtaXR0ZXI8c3RyaW5nPigpO1xyXG4gIHByaXZhdGUgZW5yb2xtZW50Rm9ybUR0bzogRW5yb2xtZW50Rm9ybUR0byA9IG51bGw7XHJcbiAgcHJpdmF0ZSBmb3JtOiBGb3JtR3JvdXA7XHJcbiAgcHJpdmF0ZSBjb25zZW50UG9saWNlczogQWJzdHJhY3RDb250cm9sO1xyXG4gIHByaXZhdGUgY29uc2VudFN1bkhhdDogQWJzdHJhY3RDb250cm9sO1xyXG4gIHByaXZhdGUgY29uc2VudENvbGxlY3Q6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIGNvbnNlbnRBYnNlbnRXaGVuU2ljazogQWJzdHJhY3RDb250cm9sO1xyXG4gIHByaXZhdGUgY29uc2VudEluZm9ybWF0aW9uU2hhcmU6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIGNvbnNlbnRGZWVzOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBjb25zZW50VHJ1ZUFuZENvcnJlY3Q6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIGNvbnNlbnRJZGVudGl0eTogQWJzdHJhY3RDb250cm9sO1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9yb3V0ZXI6IFJvdXRlcixcclxuICAgIHByaXZhdGUgX3JvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcclxuICAgIHByaXZhdGUgX2Vucm9sbWVudFNlcnZpY2U6IEVucm9sbWVudFNlcnZpY2UsXHJcbiAgICBwcml2YXRlIF9jZW50cmVTZXJ2aWNlOiBDZW50cmVTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBfdG9hc3RlclNlcnZpY2U6IFRvYXN0ZXJTZXJ2aWNlLFxyXG4gICAgZmI6IEZvcm1CdWlsZGVyKSB7XHJcblxyXG4gICAgdGhpcy5mb3JtID0gZmIuZ3JvdXAoe1xyXG4gICAgICAnY29uc2VudFBvbGljZXMnOiBbJyddLFxyXG4gICAgICAnY29uc2VudFN1bkhhdCc6IFsnJ10sXHJcbiAgICAgICdjb25zZW50Q29sbGVjdCc6IFsnJ10sXHJcbiAgICAgICdjb25zZW50QWJzZW50V2hlblNpY2snOiBbJyddLFxyXG4gICAgICAnY29uc2VudEluZm9ybWF0aW9uU2hhcmUnOiBbJyddLFxyXG4gICAgICAnY29uc2VudEZlZXMnOiBbJyddLFxyXG4gICAgICAnY29uc2VudFRydWVBbmRDb3JyZWN0JzogWycnXSxcclxuICAgICAgJ2NvbnNlbnRJZGVudGl0eSc6IFsnJ11cclxuICAgIH0pO1xyXG5cclxuICAgIHRoaXMuY29uc2VudFBvbGljZXMgPSB0aGlzLmZvcm0uY29udHJvbHNbJ2NvbnNlbnRQb2xpY2VzJ107XHJcbiAgICB0aGlzLmNvbnNlbnRTdW5IYXQgPSB0aGlzLmZvcm0uY29udHJvbHNbJ2NvbnNlbnRTdW5IYXQnXTtcclxuICAgIHRoaXMuY29uc2VudENvbGxlY3QgPSB0aGlzLmZvcm0uY29udHJvbHNbJ2NvbnNlbnRDb2xsZWN0J107XHJcbiAgICB0aGlzLmNvbnNlbnRBYnNlbnRXaGVuU2ljayA9IHRoaXMuZm9ybS5jb250cm9sc1snY29uc2VudEFic2VudFdoZW5TaWNrJ107XHJcbiAgICB0aGlzLmNvbnNlbnRJbmZvcm1hdGlvblNoYXJlID0gdGhpcy5mb3JtLmNvbnRyb2xzWydjb25zZW50SW5mb3JtYXRpb25TaGFyZSddO1xyXG4gICAgdGhpcy5jb25zZW50RmVlcyA9IHRoaXMuZm9ybS5jb250cm9sc1snY29uc2VudEZlZXMnXTtcclxuICAgIHRoaXMuY29uc2VudFRydWVBbmRDb3JyZWN0ID0gdGhpcy5mb3JtLmNvbnRyb2xzWydjb25zZW50VHJ1ZUFuZENvcnJlY3QnXTtcclxuICAgIHRoaXMuY29uc2VudElkZW50aXR5ID0gdGhpcy5mb3JtLmNvbnRyb2xzWydjb25zZW50SWRlbnRpdHknXTtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgdGhpcy5fZW5yb2xtZW50U2VydmljZVxyXG4gICAgICAuZ2V0RW5yb2xtZW50Rm9ybSQodGhpcy5fY2VudHJlU2VydmljZS5TZWxlY3RlZENlbnRyZUlkKVxyXG4gICAgICAuc3Vic2NyaWJlKChyZXM6IEVucm9sbWVudEZvcm1EdG8pID0+IHtcclxuICAgICAgICB0aGlzLmVucm9sbWVudEZvcm1EdG8gPSByZXM7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgc2F2ZUNoYW5nZXMoKTogdm9pZCB7XHJcblxyXG4gICAgbGV0IHNhdmluZ1RvYXN0ID0gdGhpcy5fdG9hc3RlclNlcnZpY2UucG9wKG5ldyBTcGlubmluZ1RvYXN0KCkpO1xyXG4gICAgbGV0IHVwZGF0ZUVucm9sbWVudEZvcm0gPSBuZXcgVXBkYXRlRW5yb2xtZW50Rm9ybUR0bygpO1xyXG4gICAgdXBkYXRlRW5yb2xtZW50Rm9ybS5pbml0KHRoaXMuZW5yb2xtZW50Rm9ybUR0by50b0pTT04oKSk7XHJcbiAgICB0aGlzLl9lbnJvbG1lbnRTZXJ2aWNlXHJcbiAgICAgIC51cGRhdGVFbnJvbG1lbnRGb3JtJCh1cGRhdGVFbnJvbG1lbnRGb3JtKVxyXG4gICAgICAuc3Vic2NyaWJlKHJlcyA9PiB7XHJcbiAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoc2F2aW5nVG9hc3QudG9hc3RJZCwgc2F2aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCk7XHJcbiAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UucG9wQXN5bmMoJ3N1Y2Nlc3MnLCAnU2F2ZWQnLCAnJyk7XHJcbiAgICAgICAgdGhpcy5lbnJvbG1lbnRGb3JtRHRvID0gcmVzO1xyXG4gICAgICB9LFxyXG4gICAgICAoZXJyb3IpID0+IHtcclxuICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcihzYXZpbmdUb2FzdC50b2FzdElkLCBzYXZpbmdUb2FzdC50b2FzdENvbnRhaW5lcklkKTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG59XHJcbiJdfQ==
