"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../../../shared/index');
var index_2 = require('./index');
var EnrolmentConsentModule = (function () {
    function EnrolmentConsentModule() {
    }
    EnrolmentConsentModule = __decorate([
        core_1.NgModule({
            imports: [index_1.SharedModule],
            declarations: [index_2.EnrolmentConsentComponent],
            providers: [],
            exports: [index_2.EnrolmentConsentComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], EnrolmentConsentModule);
    return EnrolmentConsentModule;
}());
exports.EnrolmentConsentModule = EnrolmentConsentModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvZW5yb2xtZW50LWZvcm0vZW5yb2xtZW50LWNvbnNlbnQvZW5yb2xtZW50LWNvbnNlbnQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBeUIsZUFBZSxDQUFDLENBQUE7QUFDekMsc0JBQTZCLDZCQUE2QixDQUFDLENBQUE7QUFFM0Qsc0JBQTBDLFNBQVMsQ0FBQyxDQUFBO0FBU3BEO0lBQUE7SUFBc0MsQ0FBQztJQVB2QztRQUFDLGVBQVEsQ0FBQztZQUNOLE9BQU8sRUFBRSxDQUFDLG9CQUFZLENBQUM7WUFDdkIsWUFBWSxFQUFFLENBQUMsaUNBQXlCLENBQUM7WUFDekMsU0FBUyxFQUFFLEVBQUU7WUFDYixPQUFPLEVBQUUsQ0FBQyxpQ0FBeUIsQ0FBQztTQUN2QyxDQUFDOzs4QkFBQTtJQUVvQyw2QkFBQztBQUFELENBQXRDLEFBQXVDLElBQUE7QUFBMUIsOEJBQXNCLHlCQUFJLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL3BhcmVudHMtcG9ydGFsL2Vucm9sbWVudC9lbnJvbG1lbnQtZm9ybS9lbnJvbG1lbnQtY29uc2VudC9lbnJvbG1lbnQtY29uc2VudC5tb2R1bGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTaGFyZWRNb2R1bGUgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9zaGFyZWQvaW5kZXgnO1xyXG5cclxuaW1wb3J0IHsgRW5yb2xtZW50Q29uc2VudENvbXBvbmVudCB9IGZyb20gJy4vaW5kZXgnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6IFtTaGFyZWRNb2R1bGVdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBbRW5yb2xtZW50Q29uc2VudENvbXBvbmVudF0sXHJcbiAgICBwcm92aWRlcnM6IFtdLFxyXG4gICAgZXhwb3J0czogW0Vucm9sbWVudENvbnNlbnRDb21wb25lbnRdXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgRW5yb2xtZW50Q29uc2VudE1vZHVsZSB7IH1cclxuIl19
