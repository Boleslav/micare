"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var index_1 = require('../../../../../../api/index');
var EnrolmentSingleContactComponent = (function () {
    function EnrolmentSingleContactComponent(_router, _route) {
        this._router = _router;
        this._route = _route;
        this.onDeleteContact = new core_1.EventEmitter();
    }
    EnrolmentSingleContactComponent.prototype.editContact = function (contactId) {
        var navigationExtras = {
            queryParams: { 'contactId': contactId },
            relativeTo: this._route
        };
        this._router.navigate(['../contact-edit-enrolment'], navigationExtras);
    };
    EnrolmentSingleContactComponent.prototype.deleteContact = function (contactId) {
        this.onDeleteContact.emit(contactId);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', index_1.ContactDto)
    ], EnrolmentSingleContactComponent.prototype, "contact", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], EnrolmentSingleContactComponent.prototype, "onDeleteContact", void 0);
    EnrolmentSingleContactComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            encapsulation: core_1.ViewEncapsulation.None,
            selector: 'enrolment-single-contact-cmp',
            templateUrl: 'enrolment-single-contact.component.html',
            styleUrls: ['enrolment-single-contact.component.css']
        }), 
        __metadata('design:paramtypes', [router_1.Router, router_1.ActivatedRoute])
    ], EnrolmentSingleContactComponent);
    return EnrolmentSingleContactComponent;
}());
exports.EnrolmentSingleContactComponent = EnrolmentSingleContactComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvZW5yb2xtZW50LWZvcm0vZW5yb2xtZW50LWNvbnRhY3RzL2Vucm9sbWVudC1zaW5nbGUtY29udGFjdC9lbnJvbG1lbnQtc2luZ2xlLWNvbnRhY3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBMEUsZUFBZSxDQUFDLENBQUE7QUFDMUYsdUJBQXlELGlCQUFpQixDQUFDLENBQUE7QUFDM0Usc0JBQTJCLDZCQUE2QixDQUFDLENBQUE7QUFVekQ7SUFLRSx5Q0FBb0IsT0FBZSxFQUN6QixNQUFzQjtRQURaLFlBQU8sR0FBUCxPQUFPLENBQVE7UUFDekIsV0FBTSxHQUFOLE1BQU0sQ0FBZ0I7UUFIdEIsb0JBQWUsR0FBRyxJQUFJLG1CQUFZLEVBQVUsQ0FBQztJQUl2RCxDQUFDO0lBRUQscURBQVcsR0FBWCxVQUFZLFNBQWlCO1FBQzNCLElBQUksZ0JBQWdCLEdBQXFCO1lBQ3ZDLFdBQVcsRUFBRSxFQUFFLFdBQVcsRUFBRSxTQUFTLEVBQUU7WUFDdkMsVUFBVSxFQUFFLElBQUksQ0FBQyxNQUFNO1NBQ3hCLENBQUM7UUFDRixJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLDJCQUEyQixDQUFDLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztJQUN6RSxDQUFDO0lBRUQsdURBQWEsR0FBYixVQUFjLFNBQWlCO1FBQzdCLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFqQkQ7UUFBQyxZQUFLLEVBQUU7O29FQUFBO0lBQ1I7UUFBQyxhQUFNLEVBQUU7OzRFQUFBO0lBWFg7UUFBQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLGFBQWEsRUFBRSx3QkFBaUIsQ0FBQyxJQUFJO1lBQ3JDLFFBQVEsRUFBRSw4QkFBOEI7WUFDeEMsV0FBVyxFQUFFLHlDQUF5QztZQUN0RCxTQUFTLEVBQUUsQ0FBQyx3Q0FBd0MsQ0FBQztTQUN0RCxDQUFDOzt1Q0FBQTtJQXNCRixzQ0FBQztBQUFELENBcEJBLEFBb0JDLElBQUE7QUFwQlksdUNBQStCLGtDQW9CM0MsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvcGFyZW50cy1wb3J0YWwvZW5yb2xtZW50L2Vucm9sbWVudC1mb3JtL2Vucm9sbWVudC1jb250YWN0cy9lbnJvbG1lbnQtc2luZ2xlLWNvbnRhY3QvZW5yb2xtZW50LXNpbmdsZS1jb250YWN0LmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgVmlld0VuY2Fwc3VsYXRpb24sIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBSb3V0ZXIsIEFjdGl2YXRlZFJvdXRlLCBOYXZpZ2F0aW9uRXh0cmFzIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgQ29udGFjdER0byB9IGZyb20gJy4uLy4uLy4uLy4uLy4uLy4uL2FwaS9pbmRleCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmUsXHJcbiAgc2VsZWN0b3I6ICdlbnJvbG1lbnQtc2luZ2xlLWNvbnRhY3QtY21wJyxcclxuICB0ZW1wbGF0ZVVybDogJ2Vucm9sbWVudC1zaW5nbGUtY29udGFjdC5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJ2Vucm9sbWVudC1zaW5nbGUtY29udGFjdC5jb21wb25lbnQuY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBFbnJvbG1lbnRTaW5nbGVDb250YWN0Q29tcG9uZW50IHtcclxuXHJcbiAgQElucHV0KCkgY29udGFjdDogQ29udGFjdER0bztcclxuICBAT3V0cHV0KCkgb25EZWxldGVDb250YWN0ID0gbmV3IEV2ZW50RW1pdHRlcjxzdHJpbmc+KCk7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgX3JvdXRlcjogUm91dGVyLFxyXG4gICAgcHJpdmF0ZSBfcm91dGU6IEFjdGl2YXRlZFJvdXRlKSB7XHJcbiAgfVxyXG5cclxuICBlZGl0Q29udGFjdChjb250YWN0SWQ6IHN0cmluZyk6IHZvaWQge1xyXG4gICAgbGV0IG5hdmlnYXRpb25FeHRyYXM6IE5hdmlnYXRpb25FeHRyYXMgPSB7XHJcbiAgICAgIHF1ZXJ5UGFyYW1zOiB7ICdjb250YWN0SWQnOiBjb250YWN0SWQgfSxcclxuICAgICAgcmVsYXRpdmVUbzogdGhpcy5fcm91dGVcclxuICAgIH07XHJcbiAgICB0aGlzLl9yb3V0ZXIubmF2aWdhdGUoWycuLi9jb250YWN0LWVkaXQtZW5yb2xtZW50J10sIG5hdmlnYXRpb25FeHRyYXMpO1xyXG4gIH1cclxuXHJcbiAgZGVsZXRlQ29udGFjdChjb250YWN0SWQ6IHN0cmluZyk6IHZvaWQge1xyXG4gICAgdGhpcy5vbkRlbGV0ZUNvbnRhY3QuZW1pdChjb250YWN0SWQpO1xyXG4gIH1cclxufVxyXG4iXX0=
