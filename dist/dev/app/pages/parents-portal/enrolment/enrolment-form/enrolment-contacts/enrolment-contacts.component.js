"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var angular2_toaster_1 = require('angular2-toaster');
var index_1 = require('../../index');
var index_2 = require('../../../../../shared/index');
var index_3 = require('../../../../../core/index');
var EnrolmentContactComponent = (function () {
    function EnrolmentContactComponent(_router, _route, _enrolmentService, _userService, _toasterService) {
        this._router = _router;
        this._route = _route;
        this._enrolmentService = _enrolmentService;
        this._userService = _userService;
        this._toasterService = _toasterService;
        this.contactEnrolments = new Array();
    }
    EnrolmentContactComponent.prototype.ngOnInit = function () {
        this.getContacts();
    };
    EnrolmentContactComponent.prototype.getContacts = function () {
        var _this = this;
        this._enrolmentService
            .getContacts$(this.enrolmentFormId)
            .subscribe(function (res) {
            _this.contactEnrolments = res;
        });
    };
    EnrolmentContactComponent.prototype.addContact = function () {
        var navigationExtras = {
            queryParams: { 'enrolmentFormId': this.enrolmentFormId },
            relativeTo: this._route
        };
        this._router.navigate(['../contact-edit-enrolment'], navigationExtras);
    };
    EnrolmentContactComponent.prototype.deleteContact = function (contactId) {
        var _this = this;
        var savingToast = this._toasterService.pop({
            type: 'info',
            title: 'Saving',
            timeout: 0,
            body: index_2.SpinnerComponent,
            bodyOutputType: angular2_toaster_1.BodyOutputType.Component
        });
        this._enrolmentService
            .deleteContact$(contactId)
            .map(function () {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this._toasterService.popAsync('success', 'Saved', '');
            _this.contactEnrolments = null;
        })
            .subscribe(function () { return _this.getContacts(); });
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], EnrolmentContactComponent.prototype, "enrolmentFormId", void 0);
    EnrolmentContactComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            encapsulation: core_1.ViewEncapsulation.None,
            selector: 'enrolment-contacts-cmp',
            templateUrl: 'enrolment-contacts.component.html',
            styleUrls: ['enrolment-contacts.component.css']
        }), 
        __metadata('design:paramtypes', [router_1.Router, router_1.ActivatedRoute, index_1.EnrolmentService, index_3.ParentUserService, angular2_toaster_1.ToasterService])
    ], EnrolmentContactComponent);
    return EnrolmentContactComponent;
}());
exports.EnrolmentContactComponent = EnrolmentContactComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvZW5yb2xtZW50LWZvcm0vZW5yb2xtZW50LWNvbnRhY3RzL2Vucm9sbWVudC1jb250YWN0cy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUE0RCxlQUFlLENBQUMsQ0FBQTtBQUM1RSx1QkFBeUQsaUJBQWlCLENBQUMsQ0FBQTtBQUMzRSxpQ0FBK0Msa0JBQWtCLENBQUMsQ0FBQTtBQUVsRSxzQkFBaUMsYUFBYSxDQUFDLENBQUE7QUFDL0Msc0JBQWlDLDZCQUE2QixDQUFDLENBQUE7QUFDL0Qsc0JBQWtDLDJCQUEyQixDQUFDLENBQUE7QUFXOUQ7SUFJRSxtQ0FBb0IsT0FBZSxFQUN6QixNQUFzQixFQUN0QixpQkFBbUMsRUFDbkMsWUFBK0IsRUFDL0IsZUFBK0I7UUFKckIsWUFBTyxHQUFQLE9BQU8sQ0FBUTtRQUN6QixXQUFNLEdBQU4sTUFBTSxDQUFnQjtRQUN0QixzQkFBaUIsR0FBakIsaUJBQWlCLENBQWtCO1FBQ25DLGlCQUFZLEdBQVosWUFBWSxDQUFtQjtRQUMvQixvQkFBZSxHQUFmLGVBQWUsQ0FBZ0I7UUFDdkMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksS0FBSyxFQUFFLENBQUM7SUFDdkMsQ0FBQztJQUVELDRDQUFRLEdBQVI7UUFDRSxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDckIsQ0FBQztJQUVELCtDQUFXLEdBQVg7UUFBQSxpQkFNQztRQUxDLElBQUksQ0FBQyxpQkFBaUI7YUFDbkIsWUFBWSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUM7YUFDbEMsU0FBUyxDQUFDLFVBQUEsR0FBRztZQUNaLEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxHQUFHLENBQUM7UUFDL0IsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsOENBQVUsR0FBVjtRQUNFLElBQUksZ0JBQWdCLEdBQXFCO1lBQ3ZDLFdBQVcsRUFBRSxFQUFFLGlCQUFpQixFQUFFLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDeEQsVUFBVSxFQUFFLElBQUksQ0FBQyxNQUFNO1NBQ3hCLENBQUM7UUFDRixJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLDJCQUEyQixDQUFDLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztJQUN6RSxDQUFDO0lBRUQsaURBQWEsR0FBYixVQUFjLFNBQWlCO1FBQS9CLGlCQWtCQztRQWhCQyxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQztZQUN6QyxJQUFJLEVBQUUsTUFBTTtZQUNaLEtBQUssRUFBRSxRQUFRO1lBQ2YsT0FBTyxFQUFFLENBQUM7WUFDVixJQUFJLEVBQUUsd0JBQWdCO1lBQ3RCLGNBQWMsRUFBRSxpQ0FBYyxDQUFDLFNBQVM7U0FDekMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLGlCQUFpQjthQUNuQixjQUFjLENBQUMsU0FBUyxDQUFDO2FBQ3pCLEdBQUcsQ0FBQztZQUNILEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDOUUsS0FBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLE9BQU8sRUFBRSxFQUFFLENBQUMsQ0FBQztZQUN0RCxLQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1FBQ2hDLENBQUMsQ0FBQzthQUNELFNBQVMsQ0FBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLFdBQVcsRUFBRSxFQUFsQixDQUFrQixDQUFDLENBQUM7SUFDekMsQ0FBQztJQWhERDtRQUFDLFlBQUssRUFBRTs7c0VBQUE7SUFWVjtRQUFDLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsYUFBYSxFQUFFLHdCQUFpQixDQUFDLElBQUk7WUFDckMsUUFBUSxFQUFFLHdCQUF3QjtZQUNsQyxXQUFXLEVBQUUsbUNBQW1DO1lBQ2hELFNBQVMsRUFBRSxDQUFDLGtDQUFrQyxDQUFDO1NBQ2hELENBQUM7O2lDQUFBO0lBcURGLGdDQUFDO0FBQUQsQ0FuREEsQUFtREMsSUFBQTtBQW5EWSxpQ0FBeUIsNEJBbURyQyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvZW5yb2xtZW50LWZvcm0vZW5yb2xtZW50LWNvbnRhY3RzL2Vucm9sbWVudC1jb250YWN0cy5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIFZpZXdFbmNhcHN1bGF0aW9uLCBPbkluaXQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJvdXRlciwgQWN0aXZhdGVkUm91dGUsIE5hdmlnYXRpb25FeHRyYXMgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBUb2FzdGVyU2VydmljZSwgQm9keU91dHB1dFR5cGUgfSBmcm9tICdhbmd1bGFyMi10b2FzdGVyJztcclxuXHJcbmltcG9ydCB7IEVucm9sbWVudFNlcnZpY2UgfSBmcm9tICcuLi8uLi9pbmRleCc7XHJcbmltcG9ydCB7IFNwaW5uZXJDb21wb25lbnQgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9zaGFyZWQvaW5kZXgnO1xyXG5pbXBvcnQgeyBQYXJlbnRVc2VyU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL2NvcmUvaW5kZXgnO1xyXG5pbXBvcnQgeyBDb250YWN0RHRvIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vYXBpL2luZGV4JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcclxuICBzZWxlY3RvcjogJ2Vucm9sbWVudC1jb250YWN0cy1jbXAnLFxyXG4gIHRlbXBsYXRlVXJsOiAnZW5yb2xtZW50LWNvbnRhY3RzLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnZW5yb2xtZW50LWNvbnRhY3RzLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEVucm9sbWVudENvbnRhY3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBASW5wdXQoKSBlbnJvbG1lbnRGb3JtSWQ6IHN0cmluZztcclxuICBwcml2YXRlIGNvbnRhY3RFbnJvbG1lbnRzOiBDb250YWN0RHRvW107XHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBfcm91dGVyOiBSb3V0ZXIsXHJcbiAgICBwcml2YXRlIF9yb3V0ZTogQWN0aXZhdGVkUm91dGUsXHJcbiAgICBwcml2YXRlIF9lbnJvbG1lbnRTZXJ2aWNlOiBFbnJvbG1lbnRTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBfdXNlclNlcnZpY2U6IFBhcmVudFVzZXJTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBfdG9hc3RlclNlcnZpY2U6IFRvYXN0ZXJTZXJ2aWNlKSB7XHJcbiAgICB0aGlzLmNvbnRhY3RFbnJvbG1lbnRzID0gbmV3IEFycmF5KCk7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIHRoaXMuZ2V0Q29udGFjdHMoKTtcclxuICB9XHJcblxyXG4gIGdldENvbnRhY3RzKCkge1xyXG4gICAgdGhpcy5fZW5yb2xtZW50U2VydmljZVxyXG4gICAgICAuZ2V0Q29udGFjdHMkKHRoaXMuZW5yb2xtZW50Rm9ybUlkKVxyXG4gICAgICAuc3Vic2NyaWJlKHJlcyA9PiB7XHJcbiAgICAgICAgdGhpcy5jb250YWN0RW5yb2xtZW50cyA9IHJlcztcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBhZGRDb250YWN0KCk6IHZvaWQge1xyXG4gICAgbGV0IG5hdmlnYXRpb25FeHRyYXM6IE5hdmlnYXRpb25FeHRyYXMgPSB7XHJcbiAgICAgIHF1ZXJ5UGFyYW1zOiB7ICdlbnJvbG1lbnRGb3JtSWQnOiB0aGlzLmVucm9sbWVudEZvcm1JZCB9LFxyXG4gICAgICByZWxhdGl2ZVRvOiB0aGlzLl9yb3V0ZVxyXG4gICAgfTtcclxuICAgIHRoaXMuX3JvdXRlci5uYXZpZ2F0ZShbJy4uL2NvbnRhY3QtZWRpdC1lbnJvbG1lbnQnXSwgbmF2aWdhdGlvbkV4dHJhcyk7XHJcbiAgfVxyXG5cclxuICBkZWxldGVDb250YWN0KGNvbnRhY3RJZDogc3RyaW5nKTogdm9pZCB7XHJcblxyXG4gICAgbGV0IHNhdmluZ1RvYXN0ID0gdGhpcy5fdG9hc3RlclNlcnZpY2UucG9wKHtcclxuICAgICAgdHlwZTogJ2luZm8nLFxyXG4gICAgICB0aXRsZTogJ1NhdmluZycsXHJcbiAgICAgIHRpbWVvdXQ6IDAsXHJcbiAgICAgIGJvZHk6IFNwaW5uZXJDb21wb25lbnQsXHJcbiAgICAgIGJvZHlPdXRwdXRUeXBlOiBCb2R5T3V0cHV0VHlwZS5Db21wb25lbnRcclxuICAgIH0pO1xyXG5cclxuICAgIHRoaXMuX2Vucm9sbWVudFNlcnZpY2VcclxuICAgICAgLmRlbGV0ZUNvbnRhY3QkKGNvbnRhY3RJZClcclxuICAgICAgLm1hcCgoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoc2F2aW5nVG9hc3QudG9hc3RJZCwgc2F2aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCk7XHJcbiAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UucG9wQXN5bmMoJ3N1Y2Nlc3MnLCAnU2F2ZWQnLCAnJyk7XHJcbiAgICAgICAgdGhpcy5jb250YWN0RW5yb2xtZW50cyA9IG51bGw7XHJcbiAgICAgIH0pXHJcbiAgICAgIC5zdWJzY3JpYmUoKCkgPT4gdGhpcy5nZXRDb250YWN0cygpKTtcclxuICB9XHJcbn1cclxuIl19
