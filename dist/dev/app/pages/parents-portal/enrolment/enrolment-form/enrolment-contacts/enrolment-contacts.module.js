"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../../../shared/index');
var index_2 = require('./index');
var EnrolmentContactModule = (function () {
    function EnrolmentContactModule() {
    }
    EnrolmentContactModule = __decorate([
        core_1.NgModule({
            imports: [index_1.SharedModule],
            declarations: [index_2.EnrolmentContactComponent, index_2.EnrolmentSingleContactComponent],
            providers: [],
            exports: [index_2.EnrolmentContactComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], EnrolmentContactModule);
    return EnrolmentContactModule;
}());
exports.EnrolmentContactModule = EnrolmentContactModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvZW5yb2xtZW50LWZvcm0vZW5yb2xtZW50LWNvbnRhY3RzL2Vucm9sbWVudC1jb250YWN0cy5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUF5QixlQUFlLENBQUMsQ0FBQTtBQUN6QyxzQkFBNkIsNkJBQTZCLENBQUMsQ0FBQTtBQUUzRCxzQkFBMkUsU0FBUyxDQUFDLENBQUE7QUFTckY7SUFBQTtJQUFzQyxDQUFDO0lBUHZDO1FBQUMsZUFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFLENBQUMsb0JBQVksQ0FBQztZQUN2QixZQUFZLEVBQUUsQ0FBQyxpQ0FBeUIsRUFBRSx1Q0FBK0IsQ0FBQztZQUMxRSxTQUFTLEVBQUUsRUFBRTtZQUNiLE9BQU8sRUFBRSxDQUFDLGlDQUF5QixDQUFDO1NBQ3ZDLENBQUM7OzhCQUFBO0lBRW9DLDZCQUFDO0FBQUQsQ0FBdEMsQUFBdUMsSUFBQTtBQUExQiw4QkFBc0IseUJBQUksQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvcGFyZW50cy1wb3J0YWwvZW5yb2xtZW50L2Vucm9sbWVudC1mb3JtL2Vucm9sbWVudC1jb250YWN0cy9lbnJvbG1lbnQtY29udGFjdHMubW9kdWxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU2hhcmVkTW9kdWxlIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vc2hhcmVkL2luZGV4JztcclxuXHJcbmltcG9ydCB7IEVucm9sbWVudENvbnRhY3RDb21wb25lbnQsIEVucm9sbWVudFNpbmdsZUNvbnRhY3RDb21wb25lbnQgfSBmcm9tICcuL2luZGV4JztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiBbU2hhcmVkTW9kdWxlXSxcclxuICAgIGRlY2xhcmF0aW9uczogW0Vucm9sbWVudENvbnRhY3RDb21wb25lbnQsIEVucm9sbWVudFNpbmdsZUNvbnRhY3RDb21wb25lbnRdLFxyXG4gICAgcHJvdmlkZXJzOiBbXSxcclxuICAgIGV4cG9ydHM6IFtFbnJvbG1lbnRDb250YWN0Q29tcG9uZW50XVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEVucm9sbWVudENvbnRhY3RNb2R1bGUgeyB9XHJcbiJdfQ==
