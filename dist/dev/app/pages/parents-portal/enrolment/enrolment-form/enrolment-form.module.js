"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../../shared/index');
var index_2 = require('./enrolment-parent/index');
var index_3 = require('./enrolment-guardians/index');
var index_4 = require('./enrolment-kids/index');
var index_5 = require('./enrolment-contacts/index');
var index_6 = require('./enrolment-consent/index');
var index_7 = require('./index');
var EnrolmentFormModule = (function () {
    function EnrolmentFormModule() {
    }
    EnrolmentFormModule = __decorate([
        core_1.NgModule({
            imports: [
                index_1.SharedModule,
                index_2.EnrolmentParentModule,
                index_3.EnrolmentGuardianModule,
                index_4.EnrolmentKidModule,
                index_5.EnrolmentContactModule,
                index_6.EnrolmentConsentModule
            ],
            declarations: [index_7.EnrolmentFormComponent],
            providers: [],
            exports: [index_7.EnrolmentFormComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], EnrolmentFormModule);
    return EnrolmentFormModule;
}());
exports.EnrolmentFormModule = EnrolmentFormModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvZW5yb2xtZW50LWZvcm0vZW5yb2xtZW50LWZvcm0ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBeUIsZUFBZSxDQUFDLENBQUE7QUFDekMsc0JBQTZCLDBCQUEwQixDQUFDLENBQUE7QUFFeEQsc0JBQXNDLDBCQUEwQixDQUFDLENBQUE7QUFDakUsc0JBQXdDLDZCQUE2QixDQUFDLENBQUE7QUFDdEUsc0JBQW1DLHdCQUF3QixDQUFDLENBQUE7QUFDNUQsc0JBQXVDLDRCQUE0QixDQUFDLENBQUE7QUFDcEUsc0JBQXVDLDJCQUEyQixDQUFDLENBQUE7QUFDbkUsc0JBQXVDLFNBQVMsQ0FBQyxDQUFBO0FBZ0JqRDtJQUFBO0lBQW1DLENBQUM7SUFkcEM7UUFBQyxlQUFRLENBQUM7WUFDTixPQUFPLEVBQUU7Z0JBQ0wsb0JBQVk7Z0JBQ1osNkJBQXFCO2dCQUNyQiwrQkFBdUI7Z0JBQ3ZCLDBCQUFrQjtnQkFDbEIsOEJBQXNCO2dCQUN0Qiw4QkFBc0I7YUFDekI7WUFDRCxZQUFZLEVBQUUsQ0FBQyw4QkFBc0IsQ0FBQztZQUN0QyxTQUFTLEVBQUUsRUFBRTtZQUNiLE9BQU8sRUFBRSxDQUFDLDhCQUFzQixDQUFDO1NBQ3BDLENBQUM7OzJCQUFBO0lBRWlDLDBCQUFDO0FBQUQsQ0FBbkMsQUFBb0MsSUFBQTtBQUF2QiwyQkFBbUIsc0JBQUksQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvcGFyZW50cy1wb3J0YWwvZW5yb2xtZW50L2Vucm9sbWVudC1mb3JtL2Vucm9sbWVudC1mb3JtLm1vZHVsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFNoYXJlZE1vZHVsZSB9IGZyb20gJy4uLy4uLy4uLy4uL3NoYXJlZC9pbmRleCc7XHJcblxyXG5pbXBvcnQgeyBFbnJvbG1lbnRQYXJlbnRNb2R1bGUgfSBmcm9tICcuL2Vucm9sbWVudC1wYXJlbnQvaW5kZXgnO1xyXG5pbXBvcnQgeyBFbnJvbG1lbnRHdWFyZGlhbk1vZHVsZSB9IGZyb20gJy4vZW5yb2xtZW50LWd1YXJkaWFucy9pbmRleCc7XHJcbmltcG9ydCB7IEVucm9sbWVudEtpZE1vZHVsZSB9IGZyb20gJy4vZW5yb2xtZW50LWtpZHMvaW5kZXgnO1xyXG5pbXBvcnQgeyBFbnJvbG1lbnRDb250YWN0TW9kdWxlIH0gZnJvbSAnLi9lbnJvbG1lbnQtY29udGFjdHMvaW5kZXgnO1xyXG5pbXBvcnQgeyBFbnJvbG1lbnRDb25zZW50TW9kdWxlIH0gZnJvbSAnLi9lbnJvbG1lbnQtY29uc2VudC9pbmRleCc7XHJcbmltcG9ydCB7IEVucm9sbWVudEZvcm1Db21wb25lbnQgfSBmcm9tICcuL2luZGV4JztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiBbXHJcbiAgICAgICAgU2hhcmVkTW9kdWxlLFxyXG4gICAgICAgIEVucm9sbWVudFBhcmVudE1vZHVsZSxcclxuICAgICAgICBFbnJvbG1lbnRHdWFyZGlhbk1vZHVsZSxcclxuICAgICAgICBFbnJvbG1lbnRLaWRNb2R1bGUsXHJcbiAgICAgICAgRW5yb2xtZW50Q29udGFjdE1vZHVsZSxcclxuICAgICAgICBFbnJvbG1lbnRDb25zZW50TW9kdWxlXHJcbiAgICBdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBbRW5yb2xtZW50Rm9ybUNvbXBvbmVudF0sXHJcbiAgICBwcm92aWRlcnM6IFtdLFxyXG4gICAgZXhwb3J0czogW0Vucm9sbWVudEZvcm1Db21wb25lbnRdXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgRW5yb2xtZW50Rm9ybU1vZHVsZSB7IH1cclxuIl19
