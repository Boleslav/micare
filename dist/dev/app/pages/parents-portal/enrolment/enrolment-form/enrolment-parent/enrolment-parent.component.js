"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var index_1 = require('../../index');
var index_2 = require('../../../index');
var EnrolmentParentComponent = (function () {
    function EnrolmentParentComponent(_router, _route, _enrolmentService, _centreService) {
        this._router = _router;
        this._route = _route;
        this._enrolmentService = _enrolmentService;
        this._centreService = _centreService;
        this.onEnrolmentFormId = new core_1.EventEmitter();
        this.parentDto = null;
    }
    EnrolmentParentComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._enrolmentService
            .getParent$()
            .subscribe(function (res) {
            _this.parentDto = res;
            _this.onEnrolmentFormId.emit(_this.parentDto.enrolmentFormId);
        });
    };
    EnrolmentParentComponent.prototype.editParent = function () {
        this._router.navigate(['../parent-edit-enrolment', this.parentDto.parentId], { relativeTo: this._route });
    };
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], EnrolmentParentComponent.prototype, "onEnrolmentFormId", void 0);
    EnrolmentParentComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            encapsulation: core_1.ViewEncapsulation.None,
            selector: 'enrolment-parent-cmp',
            templateUrl: 'enrolment-parent.component.html',
            styleUrls: ['enrolment-parent.component.css']
        }), 
        __metadata('design:paramtypes', [router_1.Router, router_1.ActivatedRoute, index_1.EnrolmentService, index_2.CentreService])
    ], EnrolmentParentComponent);
    return EnrolmentParentComponent;
}());
exports.EnrolmentParentComponent = EnrolmentParentComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvZW5yb2xtZW50LWZvcm0vZW5yb2xtZW50LXBhcmVudC9lbnJvbG1lbnQtcGFyZW50LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQTJFLGVBQWUsQ0FBQyxDQUFBO0FBQzNGLHVCQUF1QyxpQkFBaUIsQ0FBQyxDQUFBO0FBRXpELHNCQUFpQyxhQUFhLENBQUMsQ0FBQTtBQUUvQyxzQkFBOEIsZ0JBQWdCLENBQUMsQ0FBQTtBQVUvQztJQUtFLGtDQUFvQixPQUFlLEVBQ3pCLE1BQXNCLEVBQ3RCLGlCQUFtQyxFQUNuQyxjQUE2QjtRQUhuQixZQUFPLEdBQVAsT0FBTyxDQUFRO1FBQ3pCLFdBQU0sR0FBTixNQUFNLENBQWdCO1FBQ3RCLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBa0I7UUFDbkMsbUJBQWMsR0FBZCxjQUFjLENBQWU7UUFON0Isc0JBQWlCLEdBQUcsSUFBSSxtQkFBWSxFQUFVLENBQUM7UUFDakQsY0FBUyxHQUFjLElBQUksQ0FBQztJQU1wQyxDQUFDO0lBRUQsMkNBQVEsR0FBUjtRQUFBLGlCQU9DO1FBTkMsSUFBSSxDQUFDLGlCQUFpQjthQUNuQixVQUFVLEVBQUU7YUFDWixTQUFTLENBQUMsVUFBQyxHQUFjO1lBQ3hCLEtBQUksQ0FBQyxTQUFTLEdBQUcsR0FBRyxDQUFDO1lBQ3JCLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUM5RCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCw2Q0FBVSxHQUFWO1FBQ0UsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQywwQkFBMEIsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFLEVBQUUsVUFBVSxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO0lBQzVHLENBQUM7SUFwQkQ7UUFBQyxhQUFNLEVBQUU7O3VFQUFBO0lBVlg7UUFBQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLGFBQWEsRUFBRSx3QkFBaUIsQ0FBQyxJQUFJO1lBQ3JDLFFBQVEsRUFBRSxzQkFBc0I7WUFDaEMsV0FBVyxFQUFFLGlDQUFpQztZQUM5QyxTQUFTLEVBQUUsQ0FBQyxnQ0FBZ0MsQ0FBQztTQUM5QyxDQUFDOztnQ0FBQTtJQXlCRiwrQkFBQztBQUFELENBdkJBLEFBdUJDLElBQUE7QUF2QlksZ0NBQXdCLDJCQXVCcEMsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvcGFyZW50cy1wb3J0YWwvZW5yb2xtZW50L2Vucm9sbWVudC1mb3JtL2Vucm9sbWVudC1wYXJlbnQvZW5yb2xtZW50LXBhcmVudC5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIFZpZXdFbmNhcHN1bGF0aW9uLCBPbkluaXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJvdXRlciwgQWN0aXZhdGVkUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5cclxuaW1wb3J0IHsgRW5yb2xtZW50U2VydmljZSB9IGZyb20gJy4uLy4uL2luZGV4JztcclxuaW1wb3J0IHsgUGFyZW50RHRvIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vYXBpL2luZGV4JztcclxuaW1wb3J0IHsgQ2VudHJlU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL2luZGV4JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcclxuICBzZWxlY3RvcjogJ2Vucm9sbWVudC1wYXJlbnQtY21wJyxcclxuICB0ZW1wbGF0ZVVybDogJ2Vucm9sbWVudC1wYXJlbnQuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWydlbnJvbG1lbnQtcGFyZW50LmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEVucm9sbWVudFBhcmVudENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIEBPdXRwdXQoKSBvbkVucm9sbWVudEZvcm1JZCA9IG5ldyBFdmVudEVtaXR0ZXI8c3RyaW5nPigpO1xyXG4gIHByaXZhdGUgcGFyZW50RHRvOiBQYXJlbnREdG8gPSBudWxsO1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9yb3V0ZXI6IFJvdXRlcixcclxuICAgIHByaXZhdGUgX3JvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcclxuICAgIHByaXZhdGUgX2Vucm9sbWVudFNlcnZpY2U6IEVucm9sbWVudFNlcnZpY2UsXHJcbiAgICBwcml2YXRlIF9jZW50cmVTZXJ2aWNlOiBDZW50cmVTZXJ2aWNlKSB7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIHRoaXMuX2Vucm9sbWVudFNlcnZpY2VcclxuICAgICAgLmdldFBhcmVudCQoKVxyXG4gICAgICAuc3Vic2NyaWJlKChyZXM6IFBhcmVudER0bykgPT4ge1xyXG4gICAgICAgIHRoaXMucGFyZW50RHRvID0gcmVzO1xyXG4gICAgICAgIHRoaXMub25FbnJvbG1lbnRGb3JtSWQuZW1pdCh0aGlzLnBhcmVudER0by5lbnJvbG1lbnRGb3JtSWQpO1xyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIGVkaXRQYXJlbnQoKTogdm9pZCB7XHJcbiAgICB0aGlzLl9yb3V0ZXIubmF2aWdhdGUoWycuLi9wYXJlbnQtZWRpdC1lbnJvbG1lbnQnLCB0aGlzLnBhcmVudER0by5wYXJlbnRJZF0sIHsgcmVsYXRpdmVUbzogdGhpcy5fcm91dGUgfSk7XHJcbiAgfVxyXG59XHJcbiJdfQ==
