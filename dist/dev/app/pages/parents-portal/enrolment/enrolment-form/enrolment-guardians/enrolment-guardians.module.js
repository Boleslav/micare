"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../../../shared/index');
var index_2 = require('./index');
var EnrolmentGuardianModule = (function () {
    function EnrolmentGuardianModule() {
    }
    EnrolmentGuardianModule = __decorate([
        core_1.NgModule({
            imports: [index_1.SharedModule],
            declarations: [index_2.EnrolmentGuardianComponent, index_2.EnrolmentSingleGuardianComponent],
            providers: [],
            exports: [index_2.EnrolmentGuardianComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], EnrolmentGuardianModule);
    return EnrolmentGuardianModule;
}());
exports.EnrolmentGuardianModule = EnrolmentGuardianModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvZW5yb2xtZW50LWZvcm0vZW5yb2xtZW50LWd1YXJkaWFucy9lbnJvbG1lbnQtZ3VhcmRpYW5zLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQXlCLGVBQWUsQ0FBQyxDQUFBO0FBQ3pDLHNCQUE2Qiw2QkFBNkIsQ0FBQyxDQUFBO0FBRTNELHNCQUE2RSxTQUFTLENBQUMsQ0FBQTtBQVN2RjtJQUFBO0lBQXVDLENBQUM7SUFQeEM7UUFBQyxlQUFRLENBQUM7WUFDTixPQUFPLEVBQUUsQ0FBQyxvQkFBWSxDQUFDO1lBQ3ZCLFlBQVksRUFBRSxDQUFDLGtDQUEwQixFQUFFLHdDQUFnQyxDQUFDO1lBQzVFLFNBQVMsRUFBRSxFQUFFO1lBQ2IsT0FBTyxFQUFFLENBQUMsa0NBQTBCLENBQUM7U0FDeEMsQ0FBQzs7K0JBQUE7SUFFcUMsOEJBQUM7QUFBRCxDQUF2QyxBQUF3QyxJQUFBO0FBQTNCLCtCQUF1QiwwQkFBSSxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvZW5yb2xtZW50LWZvcm0vZW5yb2xtZW50LWd1YXJkaWFucy9lbnJvbG1lbnQtZ3VhcmRpYW5zLm1vZHVsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFNoYXJlZE1vZHVsZSB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL3NoYXJlZC9pbmRleCc7XHJcblxyXG5pbXBvcnQgeyBFbnJvbG1lbnRHdWFyZGlhbkNvbXBvbmVudCwgRW5yb2xtZW50U2luZ2xlR3VhcmRpYW5Db21wb25lbnQgfSBmcm9tICcuL2luZGV4JztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiBbU2hhcmVkTW9kdWxlXSxcclxuICAgIGRlY2xhcmF0aW9uczogW0Vucm9sbWVudEd1YXJkaWFuQ29tcG9uZW50LCBFbnJvbG1lbnRTaW5nbGVHdWFyZGlhbkNvbXBvbmVudF0sXHJcbiAgICBwcm92aWRlcnM6IFtdLFxyXG4gICAgZXhwb3J0czogW0Vucm9sbWVudEd1YXJkaWFuQ29tcG9uZW50XVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEVucm9sbWVudEd1YXJkaWFuTW9kdWxlIHsgfVxyXG4iXX0=
