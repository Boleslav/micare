"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var angular2_toaster_1 = require('angular2-toaster');
var index_1 = require('../../index');
var index_2 = require('../../../../../shared/index');
var index_3 = require('../../../../../core/index');
var EnrolmentGuardianComponent = (function () {
    function EnrolmentGuardianComponent(_router, _route, _enrolmentService, _userService, _toasterService) {
        this._router = _router;
        this._route = _route;
        this._enrolmentService = _enrolmentService;
        this._userService = _userService;
        this._toasterService = _toasterService;
        this.guardianEnrolments = null;
    }
    EnrolmentGuardianComponent.prototype.ngOnInit = function () {
        this.getGuardians();
    };
    EnrolmentGuardianComponent.prototype.getGuardians = function () {
        var _this = this;
        this._enrolmentService
            .getGuardians$(this.enrolmentFormId)
            .subscribe(function (res) {
            _this.guardianEnrolments = res;
        });
    };
    EnrolmentGuardianComponent.prototype.addGuardian = function () {
        var navigationExtras = {
            queryParams: { 'enrolmentFormId': this.enrolmentFormId },
            relativeTo: this._route
        };
        this._router.navigate(['../guardian-edit-enrolment'], navigationExtras);
    };
    EnrolmentGuardianComponent.prototype.deleteGuardian = function (guardianId) {
        var _this = this;
        var savingToast = this._toasterService.pop(new index_2.SpinningToast());
        this._enrolmentService
            .deleteGuardian$(guardianId)
            .map(function () {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this._toasterService.popAsync('success', 'Saved', '');
            _this.guardianEnrolments = null;
        })
            .subscribe(function () { return _this.getGuardians(); });
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], EnrolmentGuardianComponent.prototype, "enrolmentFormId", void 0);
    EnrolmentGuardianComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            encapsulation: core_1.ViewEncapsulation.None,
            selector: 'enrolment-guardians-cmp',
            templateUrl: 'enrolment-guardians.component.html',
            styleUrls: ['enrolment-guardians.component.css']
        }), 
        __metadata('design:paramtypes', [router_1.Router, router_1.ActivatedRoute, index_1.EnrolmentService, index_3.ParentUserService, angular2_toaster_1.ToasterService])
    ], EnrolmentGuardianComponent);
    return EnrolmentGuardianComponent;
}());
exports.EnrolmentGuardianComponent = EnrolmentGuardianComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvZW5yb2xtZW50LWZvcm0vZW5yb2xtZW50LWd1YXJkaWFucy9lbnJvbG1lbnQtZ3VhcmRpYW5zLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQTRELGVBQWUsQ0FBQyxDQUFBO0FBQzVFLHVCQUF5RCxpQkFBaUIsQ0FBQyxDQUFBO0FBQzNFLGlDQUErQixrQkFBa0IsQ0FBQyxDQUFBO0FBQ2xELHNCQUFpQyxhQUFhLENBQUMsQ0FBQTtBQUMvQyxzQkFBOEIsNkJBQTZCLENBQUMsQ0FBQTtBQUM1RCxzQkFBa0MsMkJBQTJCLENBQUMsQ0FBQTtBQVc5RDtJQUtFLG9DQUFvQixPQUFlLEVBQ3pCLE1BQXNCLEVBQ3RCLGlCQUFtQyxFQUNuQyxZQUErQixFQUMvQixlQUErQjtRQUpyQixZQUFPLEdBQVAsT0FBTyxDQUFRO1FBQ3pCLFdBQU0sR0FBTixNQUFNLENBQWdCO1FBQ3RCLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBa0I7UUFDbkMsaUJBQVksR0FBWixZQUFZLENBQW1CO1FBQy9CLG9CQUFlLEdBQWYsZUFBZSxDQUFnQjtRQU5qQyx1QkFBa0IsR0FBa0IsSUFBSSxDQUFDO0lBT2pELENBQUM7SUFFRCw2Q0FBUSxHQUFSO1FBQ0UsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ3RCLENBQUM7SUFFRCxpREFBWSxHQUFaO1FBQUEsaUJBTUM7UUFMQyxJQUFJLENBQUMsaUJBQWlCO2FBQ25CLGFBQWEsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDO2FBQ25DLFNBQVMsQ0FBQyxVQUFBLEdBQUc7WUFDWixLQUFJLENBQUMsa0JBQWtCLEdBQUcsR0FBRyxDQUFDO1FBQ2hDLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGdEQUFXLEdBQVg7UUFDRSxJQUFJLGdCQUFnQixHQUFxQjtZQUN2QyxXQUFXLEVBQUUsRUFBRSxpQkFBaUIsRUFBRSxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQ3hELFVBQVUsRUFBRSxJQUFJLENBQUMsTUFBTTtTQUN4QixDQUFDO1FBQ0YsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyw0QkFBNEIsQ0FBQyxFQUFFLGdCQUFnQixDQUFDLENBQUM7SUFDMUUsQ0FBQztJQUVELG1EQUFjLEdBQWQsVUFBZSxVQUFrQjtRQUFqQyxpQkFZQztRQVZDLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUkscUJBQWEsRUFBRSxDQUFDLENBQUM7UUFFaEUsSUFBSSxDQUFDLGlCQUFpQjthQUNuQixlQUFlLENBQUMsVUFBVSxDQUFDO2FBQzNCLEdBQUcsQ0FBQztZQUNILEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDOUUsS0FBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLE9BQU8sRUFBRSxFQUFFLENBQUMsQ0FBQztZQUN0RCxLQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO1FBQ2pDLENBQUMsQ0FBQzthQUNELFNBQVMsQ0FBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLFlBQVksRUFBRSxFQUFuQixDQUFtQixDQUFDLENBQUM7SUFDMUMsQ0FBQztJQTFDRDtRQUFDLFlBQUssRUFBRTs7dUVBQUE7SUFWVjtRQUFDLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsYUFBYSxFQUFFLHdCQUFpQixDQUFDLElBQUk7WUFDckMsUUFBUSxFQUFFLHlCQUF5QjtZQUNuQyxXQUFXLEVBQUUsb0NBQW9DO1lBQ2pELFNBQVMsRUFBRSxDQUFDLG1DQUFtQyxDQUFDO1NBQ2pELENBQUM7O2tDQUFBO0lBK0NGLGlDQUFDO0FBQUQsQ0E3Q0EsQUE2Q0MsSUFBQTtBQTdDWSxrQ0FBMEIsNkJBNkN0QyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvZW5yb2xtZW50LWZvcm0vZW5yb2xtZW50LWd1YXJkaWFucy9lbnJvbG1lbnQtZ3VhcmRpYW5zLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgVmlld0VuY2Fwc3VsYXRpb24sIE9uSW5pdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgUm91dGVyLCBBY3RpdmF0ZWRSb3V0ZSwgTmF2aWdhdGlvbkV4dHJhcyB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IFRvYXN0ZXJTZXJ2aWNlIH0gZnJvbSAnYW5ndWxhcjItdG9hc3Rlcic7XHJcbmltcG9ydCB7IEVucm9sbWVudFNlcnZpY2UgfSBmcm9tICcuLi8uLi9pbmRleCc7XHJcbmltcG9ydCB7IFNwaW5uaW5nVG9hc3QgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9zaGFyZWQvaW5kZXgnO1xyXG5pbXBvcnQgeyBQYXJlbnRVc2VyU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL2NvcmUvaW5kZXgnO1xyXG5pbXBvcnQgeyBHdWFyZGlhbkR0byB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL2FwaS9pbmRleCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmUsXHJcbiAgc2VsZWN0b3I6ICdlbnJvbG1lbnQtZ3VhcmRpYW5zLWNtcCcsXHJcbiAgdGVtcGxhdGVVcmw6ICdlbnJvbG1lbnQtZ3VhcmRpYW5zLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnZW5yb2xtZW50LWd1YXJkaWFucy5jb21wb25lbnQuY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBFbnJvbG1lbnRHdWFyZGlhbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIEBJbnB1dCgpIGVucm9sbWVudEZvcm1JZDogc3RyaW5nO1xyXG4gIHByaXZhdGUgZ3VhcmRpYW5FbnJvbG1lbnRzOiBHdWFyZGlhbkR0b1tdID0gbnVsbDtcclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBfcm91dGVyOiBSb3V0ZXIsXHJcbiAgICBwcml2YXRlIF9yb3V0ZTogQWN0aXZhdGVkUm91dGUsXHJcbiAgICBwcml2YXRlIF9lbnJvbG1lbnRTZXJ2aWNlOiBFbnJvbG1lbnRTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBfdXNlclNlcnZpY2U6IFBhcmVudFVzZXJTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBfdG9hc3RlclNlcnZpY2U6IFRvYXN0ZXJTZXJ2aWNlKSB7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIHRoaXMuZ2V0R3VhcmRpYW5zKCk7XHJcbiAgfVxyXG5cclxuICBnZXRHdWFyZGlhbnMoKSB7XHJcbiAgICB0aGlzLl9lbnJvbG1lbnRTZXJ2aWNlXHJcbiAgICAgIC5nZXRHdWFyZGlhbnMkKHRoaXMuZW5yb2xtZW50Rm9ybUlkKVxyXG4gICAgICAuc3Vic2NyaWJlKHJlcyA9PiB7XHJcbiAgICAgICAgdGhpcy5ndWFyZGlhbkVucm9sbWVudHMgPSByZXM7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgYWRkR3VhcmRpYW4oKTogdm9pZCB7XHJcbiAgICBsZXQgbmF2aWdhdGlvbkV4dHJhczogTmF2aWdhdGlvbkV4dHJhcyA9IHtcclxuICAgICAgcXVlcnlQYXJhbXM6IHsgJ2Vucm9sbWVudEZvcm1JZCc6IHRoaXMuZW5yb2xtZW50Rm9ybUlkIH0sXHJcbiAgICAgIHJlbGF0aXZlVG86IHRoaXMuX3JvdXRlXHJcbiAgICB9O1xyXG4gICAgdGhpcy5fcm91dGVyLm5hdmlnYXRlKFsnLi4vZ3VhcmRpYW4tZWRpdC1lbnJvbG1lbnQnXSwgbmF2aWdhdGlvbkV4dHJhcyk7XHJcbiAgfVxyXG5cclxuICBkZWxldGVHdWFyZGlhbihndWFyZGlhbklkOiBzdHJpbmcpOiB2b2lkIHtcclxuXHJcbiAgICBsZXQgc2F2aW5nVG9hc3QgPSB0aGlzLl90b2FzdGVyU2VydmljZS5wb3AobmV3IFNwaW5uaW5nVG9hc3QoKSk7XHJcblxyXG4gICAgdGhpcy5fZW5yb2xtZW50U2VydmljZVxyXG4gICAgICAuZGVsZXRlR3VhcmRpYW4kKGd1YXJkaWFuSWQpXHJcbiAgICAgIC5tYXAoKCkgPT4ge1xyXG4gICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLmNsZWFyKHNhdmluZ1RvYXN0LnRvYXN0SWQsIHNhdmluZ1RvYXN0LnRvYXN0Q29udGFpbmVySWQpO1xyXG4gICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLnBvcEFzeW5jKCdzdWNjZXNzJywgJ1NhdmVkJywgJycpO1xyXG4gICAgICAgIHRoaXMuZ3VhcmRpYW5FbnJvbG1lbnRzID0gbnVsbDtcclxuICAgICAgfSlcclxuICAgICAgLnN1YnNjcmliZSgoKSA9PiB0aGlzLmdldEd1YXJkaWFucygpKTtcclxuICB9XHJcbn1cclxuIl19
