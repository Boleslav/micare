"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var index_1 = require('../../../../../../api/index');
var EnrolmentSingleGuardianComponent = (function () {
    function EnrolmentSingleGuardianComponent(_router, _route) {
        this._router = _router;
        this._route = _route;
        this.onDeleteGuardian = new core_1.EventEmitter();
    }
    EnrolmentSingleGuardianComponent.prototype.editGuardian = function (guardianId) {
        var navigationExtras = {
            queryParams: { 'guardianId': guardianId },
            relativeTo: this._route
        };
        this._router.navigate(['../guardian-edit-enrolment'], navigationExtras);
    };
    EnrolmentSingleGuardianComponent.prototype.deleteGuardian = function (guardianId) {
        this.onDeleteGuardian.emit(guardianId);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', index_1.GuardianDto)
    ], EnrolmentSingleGuardianComponent.prototype, "guardian", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], EnrolmentSingleGuardianComponent.prototype, "onDeleteGuardian", void 0);
    EnrolmentSingleGuardianComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            encapsulation: core_1.ViewEncapsulation.None,
            selector: 'enrolment-single-guardian-cmp',
            templateUrl: 'enrolment-single-guardian.component.html',
            styleUrls: ['enrolment-single-guardian.component.css']
        }), 
        __metadata('design:paramtypes', [router_1.Router, router_1.ActivatedRoute])
    ], EnrolmentSingleGuardianComponent);
    return EnrolmentSingleGuardianComponent;
}());
exports.EnrolmentSingleGuardianComponent = EnrolmentSingleGuardianComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvZW5yb2xtZW50LWZvcm0vZW5yb2xtZW50LWd1YXJkaWFucy9lbnJvbG1lbnQtc2luZ2xlLWd1YXJkaWFuL2Vucm9sbWVudC1zaW5nbGUtZ3VhcmRpYW4uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBMEUsZUFBZSxDQUFDLENBQUE7QUFDMUYsdUJBQXlELGlCQUFpQixDQUFDLENBQUE7QUFDM0Usc0JBQTRCLDZCQUE2QixDQUFDLENBQUE7QUFVMUQ7SUFLRSwwQ0FBb0IsT0FBZSxFQUN6QixNQUFzQjtRQURaLFlBQU8sR0FBUCxPQUFPLENBQVE7UUFDekIsV0FBTSxHQUFOLE1BQU0sQ0FBZ0I7UUFIdEIscUJBQWdCLEdBQUcsSUFBSSxtQkFBWSxFQUFVLENBQUM7SUFJeEQsQ0FBQztJQUVELHVEQUFZLEdBQVosVUFBYSxVQUFrQjtRQUM3QixJQUFJLGdCQUFnQixHQUFxQjtZQUN2QyxXQUFXLEVBQUUsRUFBRSxZQUFZLEVBQUUsVUFBVSxFQUFFO1lBQ3pDLFVBQVUsRUFBRSxJQUFJLENBQUMsTUFBTTtTQUN4QixDQUFDO1FBQ0YsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyw0QkFBNEIsQ0FBQyxFQUFFLGdCQUFnQixDQUFDLENBQUM7SUFDMUUsQ0FBQztJQUVELHlEQUFjLEdBQWQsVUFBZSxVQUFrQjtRQUMvQixJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ3pDLENBQUM7SUFqQkQ7UUFBQyxZQUFLLEVBQUU7O3NFQUFBO0lBQ1I7UUFBQyxhQUFNLEVBQUU7OzhFQUFBO0lBWFg7UUFBQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLGFBQWEsRUFBRSx3QkFBaUIsQ0FBQyxJQUFJO1lBQ3JDLFFBQVEsRUFBRSwrQkFBK0I7WUFDekMsV0FBVyxFQUFFLDBDQUEwQztZQUN2RCxTQUFTLEVBQUUsQ0FBQyx5Q0FBeUMsQ0FBQztTQUN2RCxDQUFDOzt3Q0FBQTtJQXNCRix1Q0FBQztBQUFELENBcEJBLEFBb0JDLElBQUE7QUFwQlksd0NBQWdDLG1DQW9CNUMsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvcGFyZW50cy1wb3J0YWwvZW5yb2xtZW50L2Vucm9sbWVudC1mb3JtL2Vucm9sbWVudC1ndWFyZGlhbnMvZW5yb2xtZW50LXNpbmdsZS1ndWFyZGlhbi9lbnJvbG1lbnQtc2luZ2xlLWd1YXJkaWFuLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgVmlld0VuY2Fwc3VsYXRpb24sIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBSb3V0ZXIsIEFjdGl2YXRlZFJvdXRlLCBOYXZpZ2F0aW9uRXh0cmFzIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgR3VhcmRpYW5EdG8gfSBmcm9tICcuLi8uLi8uLi8uLi8uLi8uLi9hcGkvaW5kZXgnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lLFxyXG4gIHNlbGVjdG9yOiAnZW5yb2xtZW50LXNpbmdsZS1ndWFyZGlhbi1jbXAnLFxyXG4gIHRlbXBsYXRlVXJsOiAnZW5yb2xtZW50LXNpbmdsZS1ndWFyZGlhbi5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJ2Vucm9sbWVudC1zaW5nbGUtZ3VhcmRpYW4uY29tcG9uZW50LmNzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgRW5yb2xtZW50U2luZ2xlR3VhcmRpYW5Db21wb25lbnQge1xyXG5cclxuICBASW5wdXQoKSBndWFyZGlhbjogR3VhcmRpYW5EdG87XHJcbiAgQE91dHB1dCgpIG9uRGVsZXRlR3VhcmRpYW4gPSBuZXcgRXZlbnRFbWl0dGVyPHN0cmluZz4oKTtcclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBfcm91dGVyOiBSb3V0ZXIsXHJcbiAgICBwcml2YXRlIF9yb3V0ZTogQWN0aXZhdGVkUm91dGUpIHtcclxuICB9XHJcblxyXG4gIGVkaXRHdWFyZGlhbihndWFyZGlhbklkOiBzdHJpbmcpOiB2b2lkIHtcclxuICAgIGxldCBuYXZpZ2F0aW9uRXh0cmFzOiBOYXZpZ2F0aW9uRXh0cmFzID0ge1xyXG4gICAgICBxdWVyeVBhcmFtczogeyAnZ3VhcmRpYW5JZCc6IGd1YXJkaWFuSWQgfSxcclxuICAgICAgcmVsYXRpdmVUbzogdGhpcy5fcm91dGVcclxuICAgIH07XHJcbiAgICB0aGlzLl9yb3V0ZXIubmF2aWdhdGUoWycuLi9ndWFyZGlhbi1lZGl0LWVucm9sbWVudCddLCBuYXZpZ2F0aW9uRXh0cmFzKTtcclxuICB9XHJcblxyXG4gIGRlbGV0ZUd1YXJkaWFuKGd1YXJkaWFuSWQ6IHN0cmluZyk6IHZvaWQge1xyXG4gICAgdGhpcy5vbkRlbGV0ZUd1YXJkaWFuLmVtaXQoZ3VhcmRpYW5JZCk7XHJcbiAgfVxyXG59XHJcbiJdfQ==
