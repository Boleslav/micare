"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../../../shared/index');
var index_2 = require('./index');
var EnrolmentKidModule = (function () {
    function EnrolmentKidModule() {
    }
    EnrolmentKidModule = __decorate([
        core_1.NgModule({
            imports: [index_1.SharedModule],
            declarations: [index_2.EnrolmentKidComponent, index_2.EnrolmentSingleKidComponent],
            providers: [],
            exports: [index_2.EnrolmentKidComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], EnrolmentKidModule);
    return EnrolmentKidModule;
}());
exports.EnrolmentKidModule = EnrolmentKidModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvZW5yb2xtZW50LWZvcm0vZW5yb2xtZW50LWtpZHMvZW5yb2xtZW50LWtpZHMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBeUIsZUFBZSxDQUFDLENBQUE7QUFDekMsc0JBQTZCLDZCQUE2QixDQUFDLENBQUE7QUFFM0Qsc0JBQW1FLFNBQVMsQ0FBQyxDQUFBO0FBUzdFO0lBQUE7SUFBa0MsQ0FBQztJQVBuQztRQUFDLGVBQVEsQ0FBQztZQUNOLE9BQU8sRUFBRSxDQUFDLG9CQUFZLENBQUM7WUFDdkIsWUFBWSxFQUFFLENBQUMsNkJBQXFCLEVBQUUsbUNBQTJCLENBQUM7WUFDbEUsU0FBUyxFQUFFLEVBQUU7WUFDYixPQUFPLEVBQUUsQ0FBQyw2QkFBcUIsQ0FBQztTQUNuQyxDQUFDOzswQkFBQTtJQUVnQyx5QkFBQztBQUFELENBQWxDLEFBQW1DLElBQUE7QUFBdEIsMEJBQWtCLHFCQUFJLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL3BhcmVudHMtcG9ydGFsL2Vucm9sbWVudC9lbnJvbG1lbnQtZm9ybS9lbnJvbG1lbnQta2lkcy9lbnJvbG1lbnQta2lkcy5tb2R1bGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTaGFyZWRNb2R1bGUgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9zaGFyZWQvaW5kZXgnO1xyXG5cclxuaW1wb3J0IHsgRW5yb2xtZW50S2lkQ29tcG9uZW50LCBFbnJvbG1lbnRTaW5nbGVLaWRDb21wb25lbnQgfSBmcm9tICcuL2luZGV4JztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiBbU2hhcmVkTW9kdWxlXSxcclxuICAgIGRlY2xhcmF0aW9uczogW0Vucm9sbWVudEtpZENvbXBvbmVudCwgRW5yb2xtZW50U2luZ2xlS2lkQ29tcG9uZW50XSxcclxuICAgIHByb3ZpZGVyczogW10sXHJcbiAgICBleHBvcnRzOiBbRW5yb2xtZW50S2lkQ29tcG9uZW50XVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEVucm9sbWVudEtpZE1vZHVsZSB7IH1cclxuIl19
