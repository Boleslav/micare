"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var angular2_toaster_1 = require('angular2-toaster');
var index_1 = require('../../index');
var index_2 = require('../../../index');
var index_3 = require('../../../../../core/index');
var index_4 = require('../../../../../shared/index');
var EnrolmentKidComponent = (function () {
    function EnrolmentKidComponent(_router, _route, _enrolmentService, _userService, _centreService, _toasterService) {
        this._router = _router;
        this._route = _route;
        this._enrolmentService = _enrolmentService;
        this._userService = _userService;
        this._centreService = _centreService;
        this._toasterService = _toasterService;
    }
    EnrolmentKidComponent.prototype.ngOnInit = function () {
        this.getKids();
    };
    EnrolmentKidComponent.prototype.getKids = function () {
        var _this = this;
        this._enrolmentService
            .getChildApprovalStatusOptions$()
            .flatMap(function () { return _this._enrolmentService.getEnrolmentFormChildren$(_this._centreService.SelectedCentreId); })
            .subscribe(function (res) {
            _this.kids = res;
        });
    };
    EnrolmentKidComponent.prototype.addKid = function () {
        this._router.navigate(['../kid-edit-enrolment'], { relativeTo: this._route });
    };
    EnrolmentKidComponent.prototype.deleteKid = function (kidId) {
        var _this = this;
        var savingToast = this._toasterService.pop(new index_4.SpinningToast());
        this._enrolmentService
            .deleteKid$(kidId)
            .map(function () {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this._toasterService.popAsync('success', 'Saved', '');
            _this.kids = null;
        })
            .subscribe(function () { return _this.getKids(); }, function (error) {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
        });
    };
    EnrolmentKidComponent.prototype.requestEnrolmentApproval = function (kidId) {
        var _this = this;
        var savingToast = this._toasterService.pop(new index_4.SpinningToast("Requesting Enrolment at " + this._centreService.SelectedCentreQualifiedName));
        this._enrolmentService
            .requestEnrolChild(this._centreService.SelectedCentreId, kidId)
            .subscribe(function () {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this._toasterService.popAsync('success', 'Enrolment requested', '');
        }, function (error) {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
        });
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], EnrolmentKidComponent.prototype, "enrolmentFormId", void 0);
    EnrolmentKidComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            encapsulation: core_1.ViewEncapsulation.None,
            selector: 'enrolment-kids-cmp',
            templateUrl: 'enrolment-kids.component.html',
            styleUrls: ['enrolment-kids.component.css']
        }), 
        __metadata('design:paramtypes', [router_1.Router, router_1.ActivatedRoute, index_1.EnrolmentService, index_3.ParentUserService, index_2.CentreService, angular2_toaster_1.ToasterService])
    ], EnrolmentKidComponent);
    return EnrolmentKidComponent;
}());
exports.EnrolmentKidComponent = EnrolmentKidComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvZW5yb2xtZW50LWZvcm0vZW5yb2xtZW50LWtpZHMvZW5yb2xtZW50LWtpZHMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBNEQsZUFBZSxDQUFDLENBQUE7QUFDNUUsdUJBQXVDLGlCQUFpQixDQUFDLENBQUE7QUFDekQsaUNBQStCLGtCQUFrQixDQUFDLENBQUE7QUFFbEQsc0JBQWlDLGFBQWEsQ0FBQyxDQUFBO0FBQy9DLHNCQUE4QixnQkFBZ0IsQ0FBQyxDQUFBO0FBQy9DLHNCQUFrQywyQkFBMkIsQ0FBQyxDQUFBO0FBRTlELHNCQUE4Qiw2QkFBNkIsQ0FBQyxDQUFBO0FBVTVEO0lBSUUsK0JBQW9CLE9BQWUsRUFDekIsTUFBc0IsRUFDdEIsaUJBQW1DLEVBQ25DLFlBQStCLEVBQy9CLGNBQTZCLEVBQzdCLGVBQStCO1FBTHJCLFlBQU8sR0FBUCxPQUFPLENBQVE7UUFDekIsV0FBTSxHQUFOLE1BQU0sQ0FBZ0I7UUFDdEIsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFrQjtRQUNuQyxpQkFBWSxHQUFaLFlBQVksQ0FBbUI7UUFDL0IsbUJBQWMsR0FBZCxjQUFjLENBQWU7UUFDN0Isb0JBQWUsR0FBZixlQUFlLENBQWdCO0lBQ3pDLENBQUM7SUFFRCx3Q0FBUSxHQUFSO1FBQ0UsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ2pCLENBQUM7SUFFRCx1Q0FBTyxHQUFQO1FBQUEsaUJBUUM7UUFOQyxJQUFJLENBQUMsaUJBQWlCO2FBQ25CLDhCQUE4QixFQUFFO2FBQ2hDLE9BQU8sQ0FBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLGlCQUFpQixDQUFDLHlCQUF5QixDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsRUFBdEYsQ0FBc0YsQ0FBQzthQUNyRyxTQUFTLENBQUMsVUFBQyxHQUFrQztZQUM1QyxLQUFJLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQztRQUNsQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxzQ0FBTSxHQUFOO1FBQ0UsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFLEVBQUUsVUFBVSxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO0lBQ2hGLENBQUM7SUFFRCx5Q0FBUyxHQUFULFVBQVUsS0FBYTtRQUF2QixpQkFjQztRQWJDLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUkscUJBQWEsRUFBRSxDQUFDLENBQUM7UUFFaEUsSUFBSSxDQUFDLGlCQUFpQjthQUNuQixVQUFVLENBQUMsS0FBSyxDQUFDO2FBQ2pCLEdBQUcsQ0FBQztZQUNILEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDOUUsS0FBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLE9BQU8sRUFBRSxFQUFFLENBQUMsQ0FBQztZQUN0RCxLQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztRQUNuQixDQUFDLENBQUM7YUFDRCxTQUFTLENBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxPQUFPLEVBQUUsRUFBZCxDQUFjLEVBQy9CLFVBQUMsS0FBSztZQUNKLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDaEYsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsd0RBQXdCLEdBQXhCLFVBQXlCLEtBQWE7UUFBdEMsaUJBY0M7UUFaQyxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FDeEMsSUFBSSxxQkFBYSxDQUFDLDZCQUEyQixJQUFJLENBQUMsY0FBYyxDQUFDLDJCQUE2QixDQUFDLENBQUMsQ0FBQztRQUVuRyxJQUFJLENBQUMsaUJBQWlCO2FBQ25CLGlCQUFpQixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLEVBQUUsS0FBSyxDQUFDO2FBQzlELFNBQVMsQ0FBQztZQUNULEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDOUUsS0FBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLHFCQUFxQixFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQ3RFLENBQUMsRUFDRCxVQUFDLEtBQUs7WUFDSixLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ2hGLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQTFERDtRQUFDLFlBQUssRUFBRTs7a0VBQUE7SUFWVjtRQUFDLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsYUFBYSxFQUFFLHdCQUFpQixDQUFDLElBQUk7WUFDckMsUUFBUSxFQUFFLG9CQUFvQjtZQUM5QixXQUFXLEVBQUUsK0JBQStCO1lBQzVDLFNBQVMsRUFBRSxDQUFDLDhCQUE4QixDQUFDO1NBQzVDLENBQUM7OzZCQUFBO0lBK0RGLDRCQUFDO0FBQUQsQ0E3REEsQUE2REMsSUFBQTtBQTdEWSw2QkFBcUIsd0JBNkRqQyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvZW5yb2xtZW50LWZvcm0vZW5yb2xtZW50LWtpZHMvZW5yb2xtZW50LWtpZHMuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBWaWV3RW5jYXBzdWxhdGlvbiwgT25Jbml0LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBSb3V0ZXIsIEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgVG9hc3RlclNlcnZpY2UgfSBmcm9tICdhbmd1bGFyMi10b2FzdGVyJztcclxuXHJcbmltcG9ydCB7IEVucm9sbWVudFNlcnZpY2UgfSBmcm9tICcuLi8uLi9pbmRleCc7XHJcbmltcG9ydCB7IENlbnRyZVNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9pbmRleCc7XHJcbmltcG9ydCB7IFBhcmVudFVzZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vY29yZS9pbmRleCc7XHJcbmltcG9ydCB7IEVucm9sbWVudEZvcm1DaGlsZENlbnRyZUR0byB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL2FwaS9pbmRleCc7XHJcbmltcG9ydCB7IFNwaW5uaW5nVG9hc3QgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9zaGFyZWQvaW5kZXgnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lLFxyXG4gIHNlbGVjdG9yOiAnZW5yb2xtZW50LWtpZHMtY21wJyxcclxuICB0ZW1wbGF0ZVVybDogJ2Vucm9sbWVudC1raWRzLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnZW5yb2xtZW50LWtpZHMuY29tcG9uZW50LmNzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgRW5yb2xtZW50S2lkQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgQElucHV0KCkgZW5yb2xtZW50Rm9ybUlkOiBzdHJpbmc7XHJcbiAgcHJpdmF0ZSBraWRzOiBFbnJvbG1lbnRGb3JtQ2hpbGRDZW50cmVEdG9bXTtcclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9yb3V0ZXI6IFJvdXRlcixcclxuICAgIHByaXZhdGUgX3JvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcclxuICAgIHByaXZhdGUgX2Vucm9sbWVudFNlcnZpY2U6IEVucm9sbWVudFNlcnZpY2UsXHJcbiAgICBwcml2YXRlIF91c2VyU2VydmljZTogUGFyZW50VXNlclNlcnZpY2UsXHJcbiAgICBwcml2YXRlIF9jZW50cmVTZXJ2aWNlOiBDZW50cmVTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBfdG9hc3RlclNlcnZpY2U6IFRvYXN0ZXJTZXJ2aWNlKSB7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIHRoaXMuZ2V0S2lkcygpO1xyXG4gIH1cclxuXHJcbiAgZ2V0S2lkcygpOiB2b2lkIHtcclxuXHJcbiAgICB0aGlzLl9lbnJvbG1lbnRTZXJ2aWNlXHJcbiAgICAgIC5nZXRDaGlsZEFwcHJvdmFsU3RhdHVzT3B0aW9ucyQoKVxyXG4gICAgICAuZmxhdE1hcCgoKSA9PiB0aGlzLl9lbnJvbG1lbnRTZXJ2aWNlLmdldEVucm9sbWVudEZvcm1DaGlsZHJlbiQodGhpcy5fY2VudHJlU2VydmljZS5TZWxlY3RlZENlbnRyZUlkKSlcclxuICAgICAgLnN1YnNjcmliZSgocmVzOiBFbnJvbG1lbnRGb3JtQ2hpbGRDZW50cmVEdG9bXSkgPT4ge1xyXG4gICAgICAgIHRoaXMua2lkcyA9IHJlcztcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBhZGRLaWQoKTogdm9pZCB7XHJcbiAgICB0aGlzLl9yb3V0ZXIubmF2aWdhdGUoWycuLi9raWQtZWRpdC1lbnJvbG1lbnQnXSwgeyByZWxhdGl2ZVRvOiB0aGlzLl9yb3V0ZSB9KTtcclxuICB9XHJcblxyXG4gIGRlbGV0ZUtpZChraWRJZDogc3RyaW5nKTogdm9pZCB7XHJcbiAgICBsZXQgc2F2aW5nVG9hc3QgPSB0aGlzLl90b2FzdGVyU2VydmljZS5wb3AobmV3IFNwaW5uaW5nVG9hc3QoKSk7XHJcblxyXG4gICAgdGhpcy5fZW5yb2xtZW50U2VydmljZVxyXG4gICAgICAuZGVsZXRlS2lkJChraWRJZClcclxuICAgICAgLm1hcCgoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoc2F2aW5nVG9hc3QudG9hc3RJZCwgc2F2aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCk7XHJcbiAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UucG9wQXN5bmMoJ3N1Y2Nlc3MnLCAnU2F2ZWQnLCAnJyk7XHJcbiAgICAgICAgdGhpcy5raWRzID0gbnVsbDtcclxuICAgICAgfSlcclxuICAgICAgLnN1YnNjcmliZSgoKSA9PiB0aGlzLmdldEtpZHMoKSxcclxuICAgICAgKGVycm9yKSA9PiB7XHJcbiAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoc2F2aW5nVG9hc3QudG9hc3RJZCwgc2F2aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcmVxdWVzdEVucm9sbWVudEFwcHJvdmFsKGtpZElkOiBzdHJpbmcpOiB2b2lkIHtcclxuXHJcbiAgICBsZXQgc2F2aW5nVG9hc3QgPSB0aGlzLl90b2FzdGVyU2VydmljZS5wb3AoXHJcbiAgICAgIG5ldyBTcGlubmluZ1RvYXN0KGBSZXF1ZXN0aW5nIEVucm9sbWVudCBhdCAke3RoaXMuX2NlbnRyZVNlcnZpY2UuU2VsZWN0ZWRDZW50cmVRdWFsaWZpZWROYW1lfWApKTtcclxuXHJcbiAgICB0aGlzLl9lbnJvbG1lbnRTZXJ2aWNlXHJcbiAgICAgIC5yZXF1ZXN0RW5yb2xDaGlsZCh0aGlzLl9jZW50cmVTZXJ2aWNlLlNlbGVjdGVkQ2VudHJlSWQsIGtpZElkKVxyXG4gICAgICAuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcihzYXZpbmdUb2FzdC50b2FzdElkLCBzYXZpbmdUb2FzdC50b2FzdENvbnRhaW5lcklkKTtcclxuICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5wb3BBc3luYygnc3VjY2VzcycsICdFbnJvbG1lbnQgcmVxdWVzdGVkJywgJycpO1xyXG4gICAgICB9LFxyXG4gICAgICAoZXJyb3IpID0+IHtcclxuICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcihzYXZpbmdUb2FzdC50b2FzdElkLCBzYXZpbmdUb2FzdC50b2FzdENvbnRhaW5lcklkKTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG59XHJcbiJdfQ==
