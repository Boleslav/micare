"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var index_1 = require('../../../index');
var index_2 = require('../../../../../../api/index');
var EnrolmentSingleKidComponent = (function () {
    function EnrolmentSingleKidComponent(_enrolmentService, _router, _route) {
        this._enrolmentService = _enrolmentService;
        this._router = _router;
        this._route = _route;
        this.onDeleteKid = new core_1.EventEmitter();
        this.onRequestApproval = new core_1.EventEmitter();
        this.approvalStatuses = null;
    }
    EnrolmentSingleKidComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._enrolmentService
            .getChildApprovalStatusOptions$()
            .subscribe(function (res) { return _this.approvalStatuses = res; });
    };
    EnrolmentSingleKidComponent.prototype.editKid = function (kidId) {
        this._router.navigate(['../kid-edit-enrolment', kidId], { relativeTo: this._route });
    };
    EnrolmentSingleKidComponent.prototype.deleteKid = function (kidId) {
        this.onDeleteKid.emit(kidId);
    };
    EnrolmentSingleKidComponent.prototype.requestEnrolmentApproval = function (kidId) {
        this.kid.childApprovalStatus = 1;
        this.onRequestApproval.emit(kidId);
    };
    EnrolmentSingleKidComponent.prototype.getStatus = function (childApprovalStatus) {
        if (this.approvalStatuses !== null)
            return (childApprovalStatus === 0) ? 'Request Enrolment' : this.approvalStatuses[childApprovalStatus];
        return '';
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', index_2.EnrolmentFormChildCentreDto)
    ], EnrolmentSingleKidComponent.prototype, "kid", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], EnrolmentSingleKidComponent.prototype, "onDeleteKid", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], EnrolmentSingleKidComponent.prototype, "onRequestApproval", void 0);
    EnrolmentSingleKidComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            encapsulation: core_1.ViewEncapsulation.None,
            selector: 'enrolment-single-kid-cmp',
            templateUrl: 'enrolment-single-kid.component.html',
            styleUrls: ['enrolment-single-kid.component.css']
        }), 
        __metadata('design:paramtypes', [index_1.EnrolmentService, router_1.Router, router_1.ActivatedRoute])
    ], EnrolmentSingleKidComponent);
    return EnrolmentSingleKidComponent;
}());
exports.EnrolmentSingleKidComponent = EnrolmentSingleKidComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvZW5yb2xtZW50LWZvcm0vZW5yb2xtZW50LWtpZHMvZW5yb2xtZW50LXNpbmdsZS1raWQvZW5yb2xtZW50LXNpbmdsZS1raWQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFHTyxlQUFlLENBQUMsQ0FBQTtBQUN2Qix1QkFBdUMsaUJBQWlCLENBQUMsQ0FBQTtBQUV6RCxzQkFBaUMsZ0JBQWdCLENBQUMsQ0FBQTtBQUNsRCxzQkFBNEMsNkJBQTZCLENBQUMsQ0FBQTtBQVUxRTtJQVFFLHFDQUFvQixpQkFBbUMsRUFDN0MsT0FBZSxFQUNmLE1BQXNCO1FBRlosc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFrQjtRQUM3QyxZQUFPLEdBQVAsT0FBTyxDQUFRO1FBQ2YsV0FBTSxHQUFOLE1BQU0sQ0FBZ0I7UUFQdEIsZ0JBQVcsR0FBRyxJQUFJLG1CQUFZLEVBQVUsQ0FBQztRQUN6QyxzQkFBaUIsR0FBRyxJQUFJLG1CQUFZLEVBQVUsQ0FBQztRQUVqRCxxQkFBZ0IsR0FBOEIsSUFBSSxDQUFDO0lBSzNELENBQUM7SUFFRCw4Q0FBUSxHQUFSO1FBQUEsaUJBSUM7UUFIQyxJQUFJLENBQUMsaUJBQWlCO2FBQ25CLDhCQUE4QixFQUFFO2FBQ2hDLFNBQVMsQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxHQUFHLEVBQTNCLENBQTJCLENBQUMsQ0FBQztJQUNuRCxDQUFDO0lBRUQsNkNBQU8sR0FBUCxVQUFRLEtBQWE7UUFDbkIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyx1QkFBdUIsRUFBRSxLQUFLLENBQUMsRUFBRSxFQUFFLFVBQVUsRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQztJQUN2RixDQUFDO0lBRUQsK0NBQVMsR0FBVCxVQUFVLEtBQWE7UUFDckIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDL0IsQ0FBQztJQUVELDhEQUF3QixHQUF4QixVQUF5QixLQUFhO1FBRXBDLElBQUksQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEdBQUcsQ0FBQyxDQUFDO1FBQ2pDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDckMsQ0FBQztJQUVELCtDQUFTLEdBQVQsVUFBVSxtQkFBMkI7UUFFbkMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixLQUFLLElBQUksQ0FBQztZQUNqQyxNQUFNLENBQUMsQ0FBQyxtQkFBbUIsS0FBSyxDQUFDLENBQUMsR0FBRyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUN4RyxNQUFNLENBQUMsRUFBRSxDQUFDO0lBQ1osQ0FBQztJQXBDRDtRQUFDLFlBQUssRUFBRTs7NERBQUE7SUFDUjtRQUFDLGFBQU0sRUFBRTs7b0VBQUE7SUFDVDtRQUFDLGFBQU0sRUFBRTs7MEVBQUE7SUFaWDtRQUFDLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsYUFBYSxFQUFFLHdCQUFpQixDQUFDLElBQUk7WUFDckMsUUFBUSxFQUFFLDBCQUEwQjtZQUNwQyxXQUFXLEVBQUUscUNBQXFDO1lBQ2xELFNBQVMsRUFBRSxDQUFDLG9DQUFvQyxDQUFDO1NBQ2xELENBQUM7O21DQUFBO0lBeUNGLGtDQUFDO0FBQUQsQ0F2Q0EsQUF1Q0MsSUFBQTtBQXZDWSxtQ0FBMkIsOEJBdUN2QyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvZW5yb2xtZW50LWZvcm0vZW5yb2xtZW50LWtpZHMvZW5yb2xtZW50LXNpbmdsZS1raWQvZW5yb2xtZW50LXNpbmdsZS1raWQuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICBDb21wb25lbnQsIFZpZXdFbmNhcHN1bGF0aW9uLCBFdmVudEVtaXR0ZXIsXHJcbiAgSW5wdXQsIE91dHB1dCwgT25Jbml0XHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJvdXRlciwgQWN0aXZhdGVkUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5cclxuaW1wb3J0IHsgRW5yb2xtZW50U2VydmljZSB9IGZyb20gJy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IHsgRW5yb2xtZW50Rm9ybUNoaWxkQ2VudHJlRHRvIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vLi4vYXBpL2luZGV4JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcclxuICBzZWxlY3RvcjogJ2Vucm9sbWVudC1zaW5nbGUta2lkLWNtcCcsXHJcbiAgdGVtcGxhdGVVcmw6ICdlbnJvbG1lbnQtc2luZ2xlLWtpZC5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJ2Vucm9sbWVudC1zaW5nbGUta2lkLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEVucm9sbWVudFNpbmdsZUtpZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIEBJbnB1dCgpIGtpZDogRW5yb2xtZW50Rm9ybUNoaWxkQ2VudHJlRHRvO1xyXG4gIEBPdXRwdXQoKSBvbkRlbGV0ZUtpZCA9IG5ldyBFdmVudEVtaXR0ZXI8c3RyaW5nPigpO1xyXG4gIEBPdXRwdXQoKSBvblJlcXVlc3RBcHByb3ZhbCA9IG5ldyBFdmVudEVtaXR0ZXI8c3RyaW5nPigpO1xyXG5cclxuICBwcml2YXRlIGFwcHJvdmFsU3RhdHVzZXM6IHsgW2tleTogc3RyaW5nXTogc3RyaW5nIH0gPSBudWxsO1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9lbnJvbG1lbnRTZXJ2aWNlOiBFbnJvbG1lbnRTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBfcm91dGVyOiBSb3V0ZXIsXHJcbiAgICBwcml2YXRlIF9yb3V0ZTogQWN0aXZhdGVkUm91dGUpIHtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgdGhpcy5fZW5yb2xtZW50U2VydmljZVxyXG4gICAgICAuZ2V0Q2hpbGRBcHByb3ZhbFN0YXR1c09wdGlvbnMkKClcclxuICAgICAgLnN1YnNjcmliZShyZXMgPT4gdGhpcy5hcHByb3ZhbFN0YXR1c2VzID0gcmVzKTtcclxuICB9XHJcblxyXG4gIGVkaXRLaWQoa2lkSWQ6IHN0cmluZyk6IHZvaWQge1xyXG4gICAgdGhpcy5fcm91dGVyLm5hdmlnYXRlKFsnLi4va2lkLWVkaXQtZW5yb2xtZW50Jywga2lkSWRdLCB7IHJlbGF0aXZlVG86IHRoaXMuX3JvdXRlIH0pO1xyXG4gIH1cclxuXHJcbiAgZGVsZXRlS2lkKGtpZElkOiBzdHJpbmcpOiB2b2lkIHtcclxuICAgIHRoaXMub25EZWxldGVLaWQuZW1pdChraWRJZCk7XHJcbiAgfVxyXG5cclxuICByZXF1ZXN0RW5yb2xtZW50QXBwcm92YWwoa2lkSWQ6IHN0cmluZyk6IHZvaWQge1xyXG5cclxuICAgIHRoaXMua2lkLmNoaWxkQXBwcm92YWxTdGF0dXMgPSAxO1xyXG4gICAgdGhpcy5vblJlcXVlc3RBcHByb3ZhbC5lbWl0KGtpZElkKTtcclxuICB9XHJcblxyXG4gIGdldFN0YXR1cyhjaGlsZEFwcHJvdmFsU3RhdHVzOiBudW1iZXIpOiBzdHJpbmcge1xyXG5cclxuICAgIGlmICh0aGlzLmFwcHJvdmFsU3RhdHVzZXMgIT09IG51bGwpXHJcbiAgICAgIHJldHVybiAoY2hpbGRBcHByb3ZhbFN0YXR1cyA9PT0gMCkgPyAnUmVxdWVzdCBFbnJvbG1lbnQnIDogdGhpcy5hcHByb3ZhbFN0YXR1c2VzW2NoaWxkQXBwcm92YWxTdGF0dXNdO1xyXG4gICAgcmV0dXJuICcnO1xyXG4gIH1cclxufVxyXG4iXX0=
