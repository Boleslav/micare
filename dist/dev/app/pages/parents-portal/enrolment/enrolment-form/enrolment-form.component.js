"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var EnrolmentFormComponent = (function () {
    function EnrolmentFormComponent() {
        this.enrolmentFormId = null;
    }
    EnrolmentFormComponent.prototype.onEnrolmentFormId = function (value) {
        this.enrolmentFormId = value;
    };
    EnrolmentFormComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'enrolment-form-cmp',
            templateUrl: 'enrolment-form.component.html',
            styleUrls: ['enrolment-form.component.css']
        }), 
        __metadata('design:paramtypes', [])
    ], EnrolmentFormComponent);
    return EnrolmentFormComponent;
}());
exports.EnrolmentFormComponent = EnrolmentFormComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvZW5yb2xtZW50LWZvcm0vZW5yb2xtZW50LWZvcm0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBMEIsZUFBZSxDQUFDLENBQUE7QUFTMUM7SUFBQTtRQUNVLG9CQUFlLEdBQVcsSUFBSSxDQUFDO0lBS3pDLENBQUM7SUFIUSxrREFBaUIsR0FBeEIsVUFBeUIsS0FBVTtRQUNqQyxJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztJQUMvQixDQUFDO0lBWkg7UUFBQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxvQkFBb0I7WUFDOUIsV0FBVyxFQUFFLCtCQUErQjtZQUM1QyxTQUFTLEVBQUUsQ0FBQyw4QkFBOEIsQ0FBQztTQUM1QyxDQUFDOzs4QkFBQTtJQVFGLDZCQUFDO0FBQUQsQ0FOQSxBQU1DLElBQUE7QUFOWSw4QkFBc0IseUJBTWxDLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL3BhcmVudHMtcG9ydGFsL2Vucm9sbWVudC9lbnJvbG1lbnQtZm9ybS9lbnJvbG1lbnQtZm9ybS5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgc2VsZWN0b3I6ICdlbnJvbG1lbnQtZm9ybS1jbXAnLFxyXG4gIHRlbXBsYXRlVXJsOiAnZW5yb2xtZW50LWZvcm0uY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWydlbnJvbG1lbnQtZm9ybS5jb21wb25lbnQuY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBFbnJvbG1lbnRGb3JtQ29tcG9uZW50IHtcclxuICBwcml2YXRlIGVucm9sbWVudEZvcm1JZDogc3RyaW5nID0gbnVsbDtcclxuXHJcbiAgcHVibGljIG9uRW5yb2xtZW50Rm9ybUlkKHZhbHVlOiBhbnkpOiB2b2lkIHtcclxuICAgIHRoaXMuZW5yb2xtZW50Rm9ybUlkID0gdmFsdWU7XHJcbiAgfVxyXG59XHJcbiJdfQ==
