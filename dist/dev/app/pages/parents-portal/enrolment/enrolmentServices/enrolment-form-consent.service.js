"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../index');
var index_2 = require('../../index');
var EnrolmentFormConsentResolver = (function () {
    function EnrolmentFormConsentResolver(_enrolmentService, _centreService) {
        this._enrolmentService = _enrolmentService;
        this._centreService = _centreService;
    }
    EnrolmentFormConsentResolver.prototype.resolve = function (route, state) {
        return this._enrolmentService
            .enrolmentFormAccessGranted$(this._centreService.SelectedCentreId);
    };
    EnrolmentFormConsentResolver = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.EnrolmentService, index_2.CentreService])
    ], EnrolmentFormConsentResolver);
    return EnrolmentFormConsentResolver;
}());
exports.EnrolmentFormConsentResolver = EnrolmentFormConsentResolver;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvZW5yb2xtZW50U2VydmljZXMvZW5yb2xtZW50LWZvcm0tY29uc2VudC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBMkIsZUFBZSxDQUFDLENBQUE7QUFJM0Msc0JBQWlDLFVBQVUsQ0FBQyxDQUFBO0FBQzVDLHNCQUE4QixhQUFhLENBQUMsQ0FBQTtBQUc1QztJQUNJLHNDQUNZLGlCQUFtQyxFQUNuQyxjQUE2QjtRQUQ3QixzQkFBaUIsR0FBakIsaUJBQWlCLENBQWtCO1FBQ25DLG1CQUFjLEdBQWQsY0FBYyxDQUFlO0lBQUksQ0FBQztJQUM5Qyw4Q0FBTyxHQUFQLFVBQ0ksS0FBNkIsRUFDN0IsS0FBMEI7UUFFMUIsTUFBTSxDQUFDLElBQUksQ0FBQyxpQkFBaUI7YUFDeEIsMkJBQTJCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO0lBQzNFLENBQUM7SUFYTDtRQUFDLGlCQUFVLEVBQUU7O29DQUFBO0lBWWIsbUNBQUM7QUFBRCxDQVhBLEFBV0MsSUFBQTtBQVhZLG9DQUE0QiwrQkFXeEMsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvcGFyZW50cy1wb3J0YWwvZW5yb2xtZW50L2Vucm9sbWVudFNlcnZpY2VzL2Vucm9sbWVudC1mb3JtLWNvbnNlbnQuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgUmVzb2x2ZSwgQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgUm91dGVyU3RhdGVTbmFwc2hvdCB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuXHJcbmltcG9ydCB7IEVucm9sbWVudFNlcnZpY2UgfSBmcm9tICcuLi9pbmRleCc7XHJcbmltcG9ydCB7IENlbnRyZVNlcnZpY2UgfSBmcm9tICcuLi8uLi9pbmRleCc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBFbnJvbG1lbnRGb3JtQ29uc2VudFJlc29sdmVyIGltcGxlbWVudHMgUmVzb2x2ZTxib29sZWFuPiB7XHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwcml2YXRlIF9lbnJvbG1lbnRTZXJ2aWNlOiBFbnJvbG1lbnRTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgX2NlbnRyZVNlcnZpY2U6IENlbnRyZVNlcnZpY2UpIHsgfVxyXG4gICAgcmVzb2x2ZShcclxuICAgICAgICByb3V0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCxcclxuICAgICAgICBzdGF0ZTogUm91dGVyU3RhdGVTbmFwc2hvdFxyXG4gICAgKTogT2JzZXJ2YWJsZTxib29sZWFuPiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2Vucm9sbWVudFNlcnZpY2VcclxuICAgICAgICAgICAgLmVucm9sbWVudEZvcm1BY2Nlc3NHcmFudGVkJCh0aGlzLl9jZW50cmVTZXJ2aWNlLlNlbGVjdGVkQ2VudHJlSWQpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
