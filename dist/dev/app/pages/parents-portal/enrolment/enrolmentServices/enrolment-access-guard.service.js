"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rxjs_1 = require('rxjs');
var index_1 = require('../index');
var index_2 = require('../../index');
var EnrolmentAccessGuardService = (function () {
    function EnrolmentAccessGuardService(_enrolmentService, _centreService, router) {
        this._enrolmentService = _enrolmentService;
        this._centreService = _centreService;
        this.router = router;
    }
    EnrolmentAccessGuardService.prototype.canActivateChild = function (route, state) {
        var _this = this;
        if (this._centreService.SelectedCentreId === null) {
            return rxjs_1.Observable.of(false);
        }
        return this._enrolmentService
            .enrolmentFormAccessGranted$(this._centreService.SelectedCentreId)
            .flatMap(function (hasAccess) {
            if (!hasAccess) {
                _this.router.navigate(['parents-portal', _this._centreService.SelectedCentreUniqueName, 'enrolment']);
                return rxjs_1.Observable.of(false);
            }
            else {
                return rxjs_1.Observable.of(true);
            }
        });
    };
    EnrolmentAccessGuardService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.EnrolmentService, index_2.CentreService, router_1.Router])
    ], EnrolmentAccessGuardService);
    return EnrolmentAccessGuardService;
}());
exports.EnrolmentAccessGuardService = EnrolmentAccessGuardService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvZW5yb2xtZW50U2VydmljZXMvZW5yb2xtZW50LWFjY2Vzcy1ndWFyZC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBMkIsZUFBZSxDQUFDLENBQUE7QUFDM0MsdUJBSU8saUJBQWlCLENBQUMsQ0FBQTtBQUN6QixxQkFBMkIsTUFBTSxDQUFDLENBQUE7QUFDbEMsc0JBQWlDLFVBQVUsQ0FBQyxDQUFBO0FBQzVDLHNCQUE4QixhQUFhLENBQUMsQ0FBQTtBQUc1QztJQUVJLHFDQUNZLGlCQUFtQyxFQUNuQyxjQUE2QixFQUM3QixNQUFjO1FBRmQsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFrQjtRQUNuQyxtQkFBYyxHQUFkLGNBQWMsQ0FBZTtRQUM3QixXQUFNLEdBQU4sTUFBTSxDQUFRO0lBRTFCLENBQUM7SUFFRCxzREFBZ0IsR0FBaEIsVUFBaUIsS0FBNkIsRUFBRSxLQUEwQjtRQUExRSxpQkFpQkM7UUFiRyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDaEQsTUFBTSxDQUFDLGlCQUFVLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2hDLENBQUM7UUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDLGlCQUFpQjthQUN4QiwyQkFBMkIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDO2FBQ2pFLE9BQU8sQ0FBQyxVQUFDLFNBQWtCO1lBQ3hCLEVBQUUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFDYixLQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLGdCQUFnQixFQUFFLEtBQUksQ0FBQyxjQUFjLENBQUMsd0JBQXdCLEVBQUUsV0FBVyxDQUFDLENBQUMsQ0FBQztnQkFDcEcsTUFBTSxDQUFDLGlCQUFVLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2hDLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDSixNQUFNLENBQUMsaUJBQVUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDL0IsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQTNCTDtRQUFDLGlCQUFVLEVBQUU7O21DQUFBO0lBNEJiLGtDQUFDO0FBQUQsQ0EzQkEsQUEyQkMsSUFBQTtBQTNCWSxtQ0FBMkIsOEJBMkJ2QyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvZW5yb2xtZW50U2VydmljZXMvZW5yb2xtZW50LWFjY2Vzcy1ndWFyZC5zZXJ2aWNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQge1xyXG4gICAgQ2FuQWN0aXZhdGVDaGlsZCwgUm91dGVyLFxyXG4gICAgQWN0aXZhdGVkUm91dGVTbmFwc2hvdCxcclxuICAgIFJvdXRlclN0YXRlU25hcHNob3RcclxufSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEVucm9sbWVudFNlcnZpY2UgfSBmcm9tICcuLi9pbmRleCc7XHJcbmltcG9ydCB7IENlbnRyZVNlcnZpY2UgfSBmcm9tICcuLi8uLi9pbmRleCc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBFbnJvbG1lbnRBY2Nlc3NHdWFyZFNlcnZpY2UgaW1wbGVtZW50cyBDYW5BY3RpdmF0ZUNoaWxkIHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwcml2YXRlIF9lbnJvbG1lbnRTZXJ2aWNlOiBFbnJvbG1lbnRTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgX2NlbnRyZVNlcnZpY2U6IENlbnRyZVNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcikge1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBjYW5BY3RpdmF0ZUNoaWxkKHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBzdGF0ZTogUm91dGVyU3RhdGVTbmFwc2hvdCk6IE9ic2VydmFibGU8Ym9vbGVhbj4ge1xyXG5cclxuICAgICAgICAvLyBJZiBzZWxlY3RlZCBjZW50cmUgaGFzbid0IGluaXRpYWxpc2VkIHlldCwgYWxsb3cgYWN0aXZhdGlvbi5cclxuICAgICAgICAvLyBUaGlzIG1ldGhvZCB3aWxsIGJlIGNhbGxlZCBhZ2FpbiBvbmNlIFNlbGVjdGVkQ2VudHJlSWQgaW5pdGlhbGlzZWRcclxuICAgICAgICBpZiAodGhpcy5fY2VudHJlU2VydmljZS5TZWxlY3RlZENlbnRyZUlkID09PSBudWxsKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBPYnNlcnZhYmxlLm9mKGZhbHNlKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2Vucm9sbWVudFNlcnZpY2VcclxuICAgICAgICAgICAgLmVucm9sbWVudEZvcm1BY2Nlc3NHcmFudGVkJCh0aGlzLl9jZW50cmVTZXJ2aWNlLlNlbGVjdGVkQ2VudHJlSWQpXHJcbiAgICAgICAgICAgIC5mbGF0TWFwKChoYXNBY2Nlc3M6IGJvb2xlYW4pID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICghaGFzQWNjZXNzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWydwYXJlbnRzLXBvcnRhbCcsIHRoaXMuX2NlbnRyZVNlcnZpY2UuU2VsZWN0ZWRDZW50cmVVbmlxdWVOYW1lLCAnZW5yb2xtZW50J10pO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBPYnNlcnZhYmxlLm9mKGZhbHNlKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIE9ic2VydmFibGUub2YodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
