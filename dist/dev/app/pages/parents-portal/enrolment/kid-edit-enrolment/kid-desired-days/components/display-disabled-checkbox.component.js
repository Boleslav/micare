"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var DisplayDisabledCheckboxComponent = (function () {
    function DisplayDisabledCheckboxComponent() {
    }
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], DisplayDisabledCheckboxComponent.prototype, "value", void 0);
    DisplayDisabledCheckboxComponent = __decorate([
        core_1.Component({
            template: "\n    <input name=\"inp-day\" type=\"checkbox\" [checked]=\"value\" disabled=\"disabled\" >\n    "
        }), 
        __metadata('design:paramtypes', [])
    ], DisplayDisabledCheckboxComponent);
    return DisplayDisabledCheckboxComponent;
}());
exports.DisplayDisabledCheckboxComponent = DisplayDisabledCheckboxComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQva2lkLWVkaXQtZW5yb2xtZW50L2tpZC1kZXNpcmVkLWRheXMvY29tcG9uZW50cy9kaXNwbGF5LWRpc2FibGVkLWNoZWNrYm94LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQWlDLGVBQWUsQ0FBQyxDQUFBO0FBU2pEO0lBQUE7SUFHQSxDQUFDO0lBREc7UUFBQyxZQUFLLEVBQUU7O21FQUFBO0lBUFo7UUFBQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLG1HQUVUO1NBQ0osQ0FBQzs7d0NBQUE7SUFJRix1Q0FBQztBQUFELENBSEEsQUFHQyxJQUFBO0FBSFksd0NBQWdDLG1DQUc1QyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQva2lkLWVkaXQtZW5yb2xtZW50L2tpZC1kZXNpcmVkLWRheXMvY29tcG9uZW50cy9kaXNwbGF5LWRpc2FibGVkLWNoZWNrYm94LmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmltcG9ydCB7IFZpZXdDZWxsIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vLi4vc2hhcmVkL25nMi1zbWFydC10YWJsZS9pbmRleCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHRlbXBsYXRlOiBgXHJcbiAgICA8aW5wdXQgbmFtZT1cImlucC1kYXlcIiB0eXBlPVwiY2hlY2tib3hcIiBbY2hlY2tlZF09XCJ2YWx1ZVwiIGRpc2FibGVkPVwiZGlzYWJsZWRcIiA+XHJcbiAgICBgXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBEaXNwbGF5RGlzYWJsZWRDaGVja2JveENvbXBvbmVudCBpbXBsZW1lbnRzIFZpZXdDZWxsIHtcclxuXHJcbiAgICBASW5wdXQoKSB2YWx1ZTogc3RyaW5nIHwgbnVtYmVyO1xyXG59XHJcbiJdfQ==
