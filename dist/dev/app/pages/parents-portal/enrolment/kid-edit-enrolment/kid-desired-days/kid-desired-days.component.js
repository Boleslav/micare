"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var index_1 = require('./index');
var index_2 = require('../../index');
var index_3 = require('../../../index');
var index_4 = require('../../../../../api/index');
var angular2_toaster_1 = require('angular2-toaster');
var index_5 = require('../../../../../shared/index');
var local_data_source_1 = require('../../../../../shared/ng2-smart-table/ng2-smart-table/lib/data-source/local/local.data-source');
var KidDesiredDaysComponent = (function () {
    function KidDesiredDaysComponent(_router, _route, _enrolmentService, _centreService, _toasterService) {
        this._router = _router;
        this._route = _route;
        this._enrolmentService = _enrolmentService;
        this._centreService = _centreService;
        this._toasterService = _toasterService;
        this.source = new local_data_source_1.LocalDataSource();
        this.desiredDays = null;
    }
    KidDesiredDaysComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._enrolmentService
            .getCareTypesForCentre$(this._centreService.SelectedCentreId)
            .map(function (res) { return _this.careTypes = res; })
            .flatMap(function () { return _this._route.params; })
            .map(function (params) {
            _this.kidId = params['kidId'];
            _this.centreId = _this._centreService.SelectedCentreId;
        })
            .flatMap(function () { return _this._enrolmentService.getKidDesiredDays$(_this.centreId, _this.kidId); })
            .subscribe(function (res) {
            _this.initSettings();
            _this.desiredDays = res;
            _this.source.load(_this.desiredDays);
        });
    };
    KidDesiredDaysComponent.prototype.addDesiredDays = function (event) {
        this.addUpdateDesiredDays(event);
    };
    KidDesiredDaysComponent.prototype.editDesiredDays = function (event) {
        this.addUpdateDesiredDays(event);
    };
    KidDesiredDaysComponent.prototype.addUpdateDesiredDays = function (event) {
        var _this = this;
        if (event.newData.desiredCareType === '') {
            this._toasterService
                .popAsync('error', 'Care type cannot be empty', '');
            return;
        }
        this.source.getElements()
            .then(function (elements) {
            var desiredCareTypeExists = elements.filter(function (el) {
                return el.desiredCareType === parseInt(event.newData.desiredCareType) &&
                    (event.data === undefined ||
                        parseInt(event.newData.desiredCareType) !== event.data.desiredCareType);
            }).length > 0;
            if (desiredCareTypeExists) {
                _this._toasterService
                    .popAsync('error', 'Days for ' + _this.careTypes[event.newData.desiredCareType] + ' already exist', '');
                return event.confirm.reject();
            }
            else {
                var savingToast_1 = _this._toasterService.pop(new index_5.SpinningToast());
                var updateDesiredDays = new index_4.CreateUpdateEnrolmentFormDesiredDaysDto();
                updateDesiredDays.init({
                    CentreId: _this.centreId,
                    KidId: _this.kidId,
                    DesiredCareType: event.newData.desiredCareType,
                    DesiredStartDate: event.newData.desiredStartDate,
                    Mon: event.newData.mon,
                    Tue: event.newData.tue,
                    Wed: event.newData.wed,
                    Thu: event.newData.thu,
                    Fri: event.newData.fri,
                    Sat: event.newData.sat,
                    Sun: event.newData.sun
                });
                _this._enrolmentService
                    .addUpdateKidDesiredDays$(updateDesiredDays)
                    .toPromise()
                    .then(function (res) {
                    _this._toasterService.clear(savingToast_1.toastId, savingToast_1.toastContainerId);
                    _this._toasterService.popAsync('success', 'Saved', '');
                    return event.confirm.resolve(res);
                })
                    .catch(function (error) {
                    _this._toasterService.clear(savingToast_1.toastId, savingToast_1.toastContainerId);
                    return event.confirm.reject();
                });
            }
        });
    };
    KidDesiredDaysComponent.prototype.removeDesiredDays = function (event) {
        var _this = this;
        if (window.confirm('Are you sure you want to delete?')) {
            var savingToast_2 = this._toasterService.pop(new index_5.SpinningToast());
            var daysToDelete = new index_4.DeleteEnrolmentFormDesiredDaysDto();
            daysToDelete.init({
                CentreId: this.centreId,
                KidId: this.kidId,
                CareType: event.data.desiredCareType,
            });
            this._enrolmentService
                .deleteKidDesiredDays$(daysToDelete)
                .toPromise()
                .then(function (res) {
                _this._toasterService.clear(savingToast_2.toastId, savingToast_2.toastContainerId);
                _this._toasterService.popAsync('success', 'Deleted', '');
                return event.confirm.resolve();
            })
                .catch(function (error) {
                _this._toasterService.clear(savingToast_2.toastId, savingToast_2.toastContainerId);
                return event.confirm.reject();
            });
        }
        else {
            event.confirm.reject();
        }
    };
    KidDesiredDaysComponent.prototype.continue = function () {
        this._router.navigate(['../../kid-health-info', this.kidId], { relativeTo: this._route });
    };
    KidDesiredDaysComponent.prototype.cancelEdit = function () {
        this._router.navigate(['../../enrolment-form'], { relativeTo: this._route });
    };
    KidDesiredDaysComponent.prototype.getCareTypeViewModel = function () {
        var careTypesView = new Array();
        for (var key in this.careTypes) {
            if (this.careTypes.hasOwnProperty(key)) {
                careTypesView.push({
                    title: this.careTypes[key],
                    value: key
                });
            }
        }
        return careTypesView;
    };
    KidDesiredDaysComponent.prototype.initSettings = function () {
        this.settings = {
            mode: 'internal',
            hideHeader: false,
            hideSubHeader: false,
            sort: false,
            actions: {
                columnTitle: '',
                add: true,
                edit: true,
                delete: true,
                position: 'right'
            },
            add: {
                addButtonContent: 'Add',
                createButtonContent: 'Create',
                cancelButtonContent: 'Cancel',
                confirmCreate: true
            },
            edit: {
                editButtonContent: 'Edit',
                saveButtonContent: 'Save',
                cancelButtonContent: 'Cancel',
                confirmSave: true
            },
            delete: {
                deleteButtonContent: 'Remove',
                confirmDelete: true
            },
            columns: {
                desiredCareType: {
                    title: 'Care Type',
                    filter: false,
                    type: 'custom',
                    renderComponent: index_1.DisplayCareTypeComponent,
                    editor: {
                        type: 'list',
                        config: {
                            list: this.getCareTypeViewModel()
                        }
                    },
                },
                mon: {
                    title: 'Monday',
                    filter: false,
                    type: 'custom',
                    renderComponent: index_1.DisplayDisabledCheckboxComponent,
                    editor: {
                        type: 'checkbox'
                    }
                },
                tue: {
                    title: 'Tuesday',
                    filter: false,
                    type: 'custom',
                    renderComponent: index_1.DisplayDisabledCheckboxComponent,
                    editor: {
                        type: 'checkbox'
                    }
                },
                wed: {
                    title: 'Wednesday',
                    filter: false,
                    type: 'custom',
                    renderComponent: index_1.DisplayDisabledCheckboxComponent,
                    editor: {
                        type: 'checkbox'
                    }
                },
                thu: {
                    title: 'Thursday',
                    filter: false,
                    type: 'custom',
                    renderComponent: index_1.DisplayDisabledCheckboxComponent,
                    editor: {
                        type: 'checkbox'
                    }
                },
                fri: {
                    title: 'Friday',
                    filter: false,
                    type: 'custom',
                    renderComponent: index_1.DisplayDisabledCheckboxComponent,
                    editor: {
                        type: 'checkbox'
                    }
                },
                sat: {
                    title: 'Saturday',
                    filter: false,
                    type: 'custom',
                    renderComponent: index_1.DisplayDisabledCheckboxComponent,
                    editor: {
                        type: 'checkbox'
                    }
                },
                sun: {
                    title: 'Sunday',
                    filter: false,
                    type: 'custom',
                    renderComponent: index_1.DisplayDisabledCheckboxComponent,
                    editor: {
                        type: 'checkbox'
                    }
                },
                desiredStartDate: {
                    title: 'Desired Start Date',
                    filter: false,
                    valuePrepareFunction: function (value) { return value !== '' ? value.toDateString() : ''; },
                    editor: {
                        type: 'custom',
                        component: index_1.EditDateComponent
                    }
                }
            },
            noDataMessage: 'No Desired Days'
        };
    };
    KidDesiredDaysComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            encapsulation: core_1.ViewEncapsulation.None,
            selector: 'kid-desired-days-cmp',
            templateUrl: 'kid-desired-days.component.html',
            styleUrls: ['kid-desired-days.component.css']
        }), 
        __metadata('design:paramtypes', [router_1.Router, router_1.ActivatedRoute, index_2.EnrolmentService, index_3.CentreService, angular2_toaster_1.ToasterService])
    ], KidDesiredDaysComponent);
    return KidDesiredDaysComponent;
}());
exports.KidDesiredDaysComponent = KidDesiredDaysComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQva2lkLWVkaXQtZW5yb2xtZW50L2tpZC1kZXNpcmVkLWRheXMva2lkLWRlc2lyZWQtZGF5cy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUFzRCxlQUFlLENBQUMsQ0FBQTtBQUN0RSx1QkFBK0MsaUJBQWlCLENBQUMsQ0FBQTtBQUVqRSxzQkFBOEYsU0FBUyxDQUFDLENBQUE7QUFDeEcsc0JBQWlDLGFBQWEsQ0FBQyxDQUFBO0FBQy9DLHNCQUE4QixnQkFBZ0IsQ0FBQyxDQUFBO0FBQy9DLHNCQUlPLDBCQUEwQixDQUFDLENBQUE7QUFDbEMsaUNBQStCLGtCQUFrQixDQUFDLENBQUE7QUFDbEQsc0JBQThCLDZCQUE2QixDQUFDLENBQUE7QUFDNUQsa0NBQWdDLCtGQUErRixDQUFDLENBQUE7QUFVaEk7SUFTRSxpQ0FBb0IsT0FBZSxFQUN6QixNQUFzQixFQUN0QixpQkFBbUMsRUFDbkMsY0FBNkIsRUFDN0IsZUFBK0I7UUFKckIsWUFBTyxHQUFQLE9BQU8sQ0FBUTtRQUN6QixXQUFNLEdBQU4sTUFBTSxDQUFnQjtRQUN0QixzQkFBaUIsR0FBakIsaUJBQWlCLENBQWtCO1FBQ25DLG1CQUFjLEdBQWQsY0FBYyxDQUFlO1FBQzdCLG9CQUFlLEdBQWYsZUFBZSxDQUFnQjtRQVZqQyxXQUFNLEdBQW9CLElBQUksbUNBQWUsRUFBRSxDQUFDO1FBQ2hELGdCQUFXLEdBQWtDLElBQUksQ0FBQztJQVUxRCxDQUFDO0lBRUQsMENBQVEsR0FBUjtRQUFBLGlCQWdCQztRQWRDLElBQUksQ0FBQyxpQkFBaUI7YUFDbkIsc0JBQXNCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQzthQUM1RCxHQUFHLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxLQUFJLENBQUMsU0FBUyxHQUFHLEdBQUcsRUFBcEIsQ0FBb0IsQ0FBQzthQUNoQyxPQUFPLENBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFsQixDQUFrQixDQUFDO2FBQ2pDLEdBQUcsQ0FBQyxVQUFDLE1BQWM7WUFDbEIsS0FBSSxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDN0IsS0FBSSxDQUFDLFFBQVEsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDO1FBQ3ZELENBQUMsQ0FBQzthQUNELE9BQU8sQ0FBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLEtBQUksQ0FBQyxRQUFRLEVBQUUsS0FBSSxDQUFDLEtBQUssQ0FBQyxFQUFwRSxDQUFvRSxDQUFDO2FBQ25GLFNBQVMsQ0FBQyxVQUFBLEdBQUc7WUFDWixLQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7WUFDcEIsS0FBSSxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUM7WUFDdkIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3JDLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGdEQUFjLEdBQWQsVUFBZSxLQUFVO1FBQ3ZCLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRUQsaURBQWUsR0FBZixVQUFnQixLQUFVO1FBQ3hCLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRUQsc0RBQW9CLEdBQXBCLFVBQXFCLEtBQVU7UUFBL0IsaUJBcURDO1FBbkRDLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsZUFBZSxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDekMsSUFBSSxDQUFDLGVBQWU7aUJBQ2pCLFFBQVEsQ0FBQyxPQUFPLEVBQUUsMkJBQTJCLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDdEQsTUFBTSxDQUFDO1FBQ1QsQ0FBQztRQUVELElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFO2FBQ3RCLElBQUksQ0FBQyxVQUFDLFFBQXVDO1lBRTVDLElBQUkscUJBQXFCLEdBQ3ZCLFFBQVEsQ0FBQyxNQUFNLENBQUMsVUFBQyxFQUErQjtnQkFDOUMsT0FBQSxFQUFFLENBQUMsZUFBZSxLQUFLLFFBQVEsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQztvQkFDOUQsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLFNBQVM7d0JBQ3ZCLFFBQVEsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxLQUFLLEtBQUssQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDO1lBRnpFLENBRXlFLENBQzFFLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztZQUVmLEVBQUUsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQztnQkFDMUIsS0FBSSxDQUFDLGVBQWU7cUJBQ2pCLFFBQVEsQ0FBQyxPQUFPLEVBQUUsV0FBVyxHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsR0FBRyxnQkFBZ0IsRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDekcsTUFBTSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUM7WUFDaEMsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNOLElBQUksYUFBVyxHQUFHLEtBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUkscUJBQWEsRUFBRSxDQUFDLENBQUM7Z0JBQ2hFLElBQUksaUJBQWlCLEdBQTRDLElBQUksK0NBQXVDLEVBQUUsQ0FBQztnQkFDL0csaUJBQWlCLENBQUMsSUFBSSxDQUFDO29CQUNyQixRQUFRLEVBQUUsS0FBSSxDQUFDLFFBQVE7b0JBQ3ZCLEtBQUssRUFBRSxLQUFJLENBQUMsS0FBSztvQkFDakIsZUFBZSxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsZUFBZTtvQkFDOUMsZ0JBQWdCLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0I7b0JBQ2hELEdBQUcsRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUc7b0JBQ3RCLEdBQUcsRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUc7b0JBQ3RCLEdBQUcsRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUc7b0JBQ3RCLEdBQUcsRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUc7b0JBQ3RCLEdBQUcsRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUc7b0JBQ3RCLEdBQUcsRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUc7b0JBQ3RCLEdBQUcsRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUc7aUJBQ3ZCLENBQUMsQ0FBQztnQkFFSCxLQUFJLENBQUMsaUJBQWlCO3FCQUNuQix3QkFBd0IsQ0FBQyxpQkFBaUIsQ0FBQztxQkFDM0MsU0FBUyxFQUFFO3FCQUNYLElBQUksQ0FBQyxVQUFDLEdBQUc7b0JBQ1IsS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsYUFBVyxDQUFDLE9BQU8sRUFBRSxhQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztvQkFDOUUsS0FBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLE9BQU8sRUFBRSxFQUFFLENBQUMsQ0FBQztvQkFDdEQsTUFBTSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNwQyxDQUFDLENBQUM7cUJBQ0QsS0FBSyxDQUFDLFVBQUMsS0FBSztvQkFDWCxLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxhQUFXLENBQUMsT0FBTyxFQUFFLGFBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO29CQUM5RSxNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFDaEMsQ0FBQyxDQUFDLENBQUM7WUFDUCxDQUFDO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsbURBQWlCLEdBQWpCLFVBQWtCLEtBQVU7UUFBNUIsaUJBeUJDO1FBeEJDLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsa0NBQWtDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFdkQsSUFBSSxhQUFXLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsSUFBSSxxQkFBYSxFQUFFLENBQUMsQ0FBQztZQUNoRSxJQUFJLFlBQVksR0FBRyxJQUFJLHlDQUFpQyxFQUFFLENBQUM7WUFDM0QsWUFBWSxDQUFDLElBQUksQ0FBQztnQkFDaEIsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO2dCQUN2QixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7Z0JBQ2pCLFFBQVEsRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLGVBQWU7YUFDckMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLGlCQUFpQjtpQkFDbkIscUJBQXFCLENBQUMsWUFBWSxDQUFDO2lCQUNuQyxTQUFTLEVBQUU7aUJBQ1gsSUFBSSxDQUFDLFVBQUMsR0FBRztnQkFDUixLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxhQUFXLENBQUMsT0FBTyxFQUFFLGFBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2dCQUM5RSxLQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsU0FBUyxFQUFFLEVBQUUsQ0FBQyxDQUFDO2dCQUN4RCxNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUNqQyxDQUFDLENBQUM7aUJBQ0QsS0FBSyxDQUFDLFVBQUMsS0FBSztnQkFDWCxLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxhQUFXLENBQUMsT0FBTyxFQUFFLGFBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2dCQUM5RSxNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUNoQyxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLEtBQUssQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDekIsQ0FBQztJQUNILENBQUM7SUFFRCwwQ0FBUSxHQUFSO1FBQ0UsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyx1QkFBdUIsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxVQUFVLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7SUFDNUYsQ0FBQztJQUVELDRDQUFVLEdBQVY7UUFDRSxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLHNCQUFzQixDQUFDLEVBQUUsRUFBRSxVQUFVLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7SUFDL0UsQ0FBQztJQUVELHNEQUFvQixHQUFwQjtRQUNFLElBQUksYUFBYSxHQUFnQyxJQUFJLEtBQUssRUFBNkIsQ0FBQztRQUN4RixHQUFHLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUMvQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZDLGFBQWEsQ0FBQyxJQUFJLENBQUM7b0JBQ2pCLEtBQUssRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQztvQkFDMUIsS0FBSyxFQUFFLEdBQUc7aUJBQ1gsQ0FBQyxDQUFDO1lBQ0wsQ0FBQztRQUNILENBQUM7UUFDRCxNQUFNLENBQUMsYUFBYSxDQUFDO0lBQ3ZCLENBQUM7SUFFTyw4Q0FBWSxHQUFwQjtRQUNFLElBQUksQ0FBQyxRQUFRLEdBQUc7WUFDZCxJQUFJLEVBQUUsVUFBVTtZQUNoQixVQUFVLEVBQUUsS0FBSztZQUNqQixhQUFhLEVBQUUsS0FBSztZQUNwQixJQUFJLEVBQUUsS0FBSztZQUNYLE9BQU8sRUFBRTtnQkFDUCxXQUFXLEVBQUUsRUFBRTtnQkFDZixHQUFHLEVBQUUsSUFBSTtnQkFDVCxJQUFJLEVBQUUsSUFBSTtnQkFDVixNQUFNLEVBQUUsSUFBSTtnQkFDWixRQUFRLEVBQUUsT0FBTzthQUNsQjtZQUNELEdBQUcsRUFBRTtnQkFDSCxnQkFBZ0IsRUFBRSxLQUFLO2dCQUN2QixtQkFBbUIsRUFBRSxRQUFRO2dCQUM3QixtQkFBbUIsRUFBRSxRQUFRO2dCQUM3QixhQUFhLEVBQUUsSUFBSTthQUNwQjtZQUNELElBQUksRUFBRTtnQkFDSixpQkFBaUIsRUFBRSxNQUFNO2dCQUN6QixpQkFBaUIsRUFBRSxNQUFNO2dCQUN6QixtQkFBbUIsRUFBRSxRQUFRO2dCQUM3QixXQUFXLEVBQUUsSUFBSTthQUNsQjtZQUNELE1BQU0sRUFBRTtnQkFDTixtQkFBbUIsRUFBRSxRQUFRO2dCQUM3QixhQUFhLEVBQUUsSUFBSTthQUNwQjtZQUNELE9BQU8sRUFBRTtnQkFDUCxlQUFlLEVBQUU7b0JBQ2YsS0FBSyxFQUFFLFdBQVc7b0JBQ2xCLE1BQU0sRUFBRSxLQUFLO29CQUNiLElBQUksRUFBRSxRQUFRO29CQUNkLGVBQWUsRUFBRSxnQ0FBd0I7b0JBQ3pDLE1BQU0sRUFBRTt3QkFDTixJQUFJLEVBQUUsTUFBTTt3QkFDWixNQUFNLEVBQUU7NEJBQ04sSUFBSSxFQUFFLElBQUksQ0FBQyxvQkFBb0IsRUFBRTt5QkFDbEM7cUJBQ0Y7aUJBQ0Y7Z0JBQ0QsR0FBRyxFQUFFO29CQUNILEtBQUssRUFBRSxRQUFRO29CQUNmLE1BQU0sRUFBRSxLQUFLO29CQUNiLElBQUksRUFBRSxRQUFRO29CQUNkLGVBQWUsRUFBRSx3Q0FBZ0M7b0JBQ2pELE1BQU0sRUFBRTt3QkFDTixJQUFJLEVBQUUsVUFBVTtxQkFDakI7aUJBQ0Y7Z0JBQ0QsR0FBRyxFQUFFO29CQUNILEtBQUssRUFBRSxTQUFTO29CQUNoQixNQUFNLEVBQUUsS0FBSztvQkFDYixJQUFJLEVBQUUsUUFBUTtvQkFDZCxlQUFlLEVBQUUsd0NBQWdDO29CQUNqRCxNQUFNLEVBQUU7d0JBQ04sSUFBSSxFQUFFLFVBQVU7cUJBQ2pCO2lCQUNGO2dCQUNELEdBQUcsRUFBRTtvQkFDSCxLQUFLLEVBQUUsV0FBVztvQkFDbEIsTUFBTSxFQUFFLEtBQUs7b0JBQ2IsSUFBSSxFQUFFLFFBQVE7b0JBQ2QsZUFBZSxFQUFFLHdDQUFnQztvQkFDakQsTUFBTSxFQUFFO3dCQUNOLElBQUksRUFBRSxVQUFVO3FCQUNqQjtpQkFDRjtnQkFDRCxHQUFHLEVBQUU7b0JBQ0gsS0FBSyxFQUFFLFVBQVU7b0JBQ2pCLE1BQU0sRUFBRSxLQUFLO29CQUNiLElBQUksRUFBRSxRQUFRO29CQUNkLGVBQWUsRUFBRSx3Q0FBZ0M7b0JBQ2pELE1BQU0sRUFBRTt3QkFDTixJQUFJLEVBQUUsVUFBVTtxQkFDakI7aUJBQ0Y7Z0JBQ0QsR0FBRyxFQUFFO29CQUNILEtBQUssRUFBRSxRQUFRO29CQUNmLE1BQU0sRUFBRSxLQUFLO29CQUNiLElBQUksRUFBRSxRQUFRO29CQUNkLGVBQWUsRUFBRSx3Q0FBZ0M7b0JBQ2pELE1BQU0sRUFBRTt3QkFDTixJQUFJLEVBQUUsVUFBVTtxQkFDakI7aUJBQ0Y7Z0JBQ0QsR0FBRyxFQUFFO29CQUNILEtBQUssRUFBRSxVQUFVO29CQUNqQixNQUFNLEVBQUUsS0FBSztvQkFDYixJQUFJLEVBQUUsUUFBUTtvQkFDZCxlQUFlLEVBQUUsd0NBQWdDO29CQUNqRCxNQUFNLEVBQUU7d0JBQ04sSUFBSSxFQUFFLFVBQVU7cUJBQ2pCO2lCQUNGO2dCQUNELEdBQUcsRUFBRTtvQkFDSCxLQUFLLEVBQUUsUUFBUTtvQkFDZixNQUFNLEVBQUUsS0FBSztvQkFDYixJQUFJLEVBQUUsUUFBUTtvQkFDZCxlQUFlLEVBQUUsd0NBQWdDO29CQUNqRCxNQUFNLEVBQUU7d0JBQ04sSUFBSSxFQUFFLFVBQVU7cUJBQ2pCO2lCQUNGO2dCQUNELGdCQUFnQixFQUFFO29CQUNoQixLQUFLLEVBQUUsb0JBQW9CO29CQUMzQixNQUFNLEVBQUUsS0FBSztvQkFDYixvQkFBb0IsRUFBRSxVQUFDLEtBQVUsSUFBSyxPQUFBLEtBQUssS0FBSyxFQUFFLEdBQUcsS0FBSyxDQUFDLFlBQVksRUFBRSxHQUFHLEVBQUUsRUFBeEMsQ0FBd0M7b0JBQzlFLE1BQU0sRUFBRTt3QkFDTixJQUFJLEVBQUUsUUFBUTt3QkFDZCxTQUFTLEVBQUUseUJBQWlCO3FCQUM3QjtpQkFDRjthQUNGO1lBQ0QsYUFBYSxFQUFFLGlCQUFpQjtTQUNqQyxDQUFDO0lBQ0osQ0FBQztJQTlRSDtRQUFDLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsYUFBYSxFQUFFLHdCQUFpQixDQUFDLElBQUk7WUFDckMsUUFBUSxFQUFFLHNCQUFzQjtZQUNoQyxXQUFXLEVBQUUsaUNBQWlDO1lBQzlDLFNBQVMsRUFBRSxDQUFDLGdDQUFnQyxDQUFDO1NBQzlDLENBQUM7OytCQUFBO0lBeVFGLDhCQUFDO0FBQUQsQ0F2UUEsQUF1UUMsSUFBQTtBQXZRWSwrQkFBdUIsMEJBdVFuQyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQva2lkLWVkaXQtZW5yb2xtZW50L2tpZC1kZXNpcmVkLWRheXMva2lkLWRlc2lyZWQtZGF5cy5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0VuY2Fwc3VsYXRpb24sIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJvdXRlciwgQWN0aXZhdGVkUm91dGUsIFBhcmFtcyB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcblxyXG5pbXBvcnQgeyBEaXNwbGF5RGlzYWJsZWRDaGVja2JveENvbXBvbmVudCwgRGlzcGxheUNhcmVUeXBlQ29tcG9uZW50LCBFZGl0RGF0ZUNvbXBvbmVudCB9IGZyb20gJy4vaW5kZXgnO1xyXG5pbXBvcnQgeyBFbnJvbG1lbnRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vaW5kZXgnO1xyXG5pbXBvcnQgeyBDZW50cmVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vaW5kZXgnO1xyXG5pbXBvcnQge1xyXG4gIEVucm9sbWVudEZvcm1EZXNpcmVkRGF5c0R0byxcclxuICBDcmVhdGVVcGRhdGVFbnJvbG1lbnRGb3JtRGVzaXJlZERheXNEdG8sXHJcbiAgRGVsZXRlRW5yb2xtZW50Rm9ybURlc2lyZWREYXlzRHRvXHJcbn0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vYXBpL2luZGV4JztcclxuaW1wb3J0IHsgVG9hc3RlclNlcnZpY2UgfSBmcm9tICdhbmd1bGFyMi10b2FzdGVyJztcclxuaW1wb3J0IHsgU3Bpbm5pbmdUb2FzdCB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL3NoYXJlZC9pbmRleCc7XHJcbmltcG9ydCB7IExvY2FsRGF0YVNvdXJjZSB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL3NoYXJlZC9uZzItc21hcnQtdGFibGUvbmcyLXNtYXJ0LXRhYmxlL2xpYi9kYXRhLXNvdXJjZS9sb2NhbC9sb2NhbC5kYXRhLXNvdXJjZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmUsXHJcbiAgc2VsZWN0b3I6ICdraWQtZGVzaXJlZC1kYXlzLWNtcCcsXHJcbiAgdGVtcGxhdGVVcmw6ICdraWQtZGVzaXJlZC1kYXlzLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsna2lkLWRlc2lyZWQtZGF5cy5jb21wb25lbnQuY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBLaWREZXNpcmVkRGF5c0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIHByaXZhdGUgc2V0dGluZ3M6IGFueTtcclxuICBwcml2YXRlIHNvdXJjZTogTG9jYWxEYXRhU291cmNlID0gbmV3IExvY2FsRGF0YVNvdXJjZSgpO1xyXG4gIHByaXZhdGUgZGVzaXJlZERheXM6IEVucm9sbWVudEZvcm1EZXNpcmVkRGF5c0R0b1tdID0gbnVsbDtcclxuICBwcml2YXRlIGNhcmVUeXBlczogeyBba2V5OiBzdHJpbmddOiBzdHJpbmcgfTtcclxuICBwcml2YXRlIGNlbnRyZUlkOiBzdHJpbmc7XHJcbiAgcHJpdmF0ZSBraWRJZDogc3RyaW5nO1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9yb3V0ZXI6IFJvdXRlcixcclxuICAgIHByaXZhdGUgX3JvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcclxuICAgIHByaXZhdGUgX2Vucm9sbWVudFNlcnZpY2U6IEVucm9sbWVudFNlcnZpY2UsXHJcbiAgICBwcml2YXRlIF9jZW50cmVTZXJ2aWNlOiBDZW50cmVTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBfdG9hc3RlclNlcnZpY2U6IFRvYXN0ZXJTZXJ2aWNlKSB7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuXHJcbiAgICB0aGlzLl9lbnJvbG1lbnRTZXJ2aWNlXHJcbiAgICAgIC5nZXRDYXJlVHlwZXNGb3JDZW50cmUkKHRoaXMuX2NlbnRyZVNlcnZpY2UuU2VsZWN0ZWRDZW50cmVJZClcclxuICAgICAgLm1hcChyZXMgPT4gdGhpcy5jYXJlVHlwZXMgPSByZXMpXHJcbiAgICAgIC5mbGF0TWFwKCgpID0+IHRoaXMuX3JvdXRlLnBhcmFtcylcclxuICAgICAgLm1hcCgocGFyYW1zOiBQYXJhbXMpID0+IHtcclxuICAgICAgICB0aGlzLmtpZElkID0gcGFyYW1zWydraWRJZCddO1xyXG4gICAgICAgIHRoaXMuY2VudHJlSWQgPSB0aGlzLl9jZW50cmVTZXJ2aWNlLlNlbGVjdGVkQ2VudHJlSWQ7XHJcbiAgICAgIH0pXHJcbiAgICAgIC5mbGF0TWFwKCgpID0+IHRoaXMuX2Vucm9sbWVudFNlcnZpY2UuZ2V0S2lkRGVzaXJlZERheXMkKHRoaXMuY2VudHJlSWQsIHRoaXMua2lkSWQpKVxyXG4gICAgICAuc3Vic2NyaWJlKHJlcyA9PiB7XHJcbiAgICAgICAgdGhpcy5pbml0U2V0dGluZ3MoKTtcclxuICAgICAgICB0aGlzLmRlc2lyZWREYXlzID0gcmVzO1xyXG4gICAgICAgIHRoaXMuc291cmNlLmxvYWQodGhpcy5kZXNpcmVkRGF5cyk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgYWRkRGVzaXJlZERheXMoZXZlbnQ6IGFueSk6IHZvaWQge1xyXG4gICAgdGhpcy5hZGRVcGRhdGVEZXNpcmVkRGF5cyhldmVudCk7XHJcbiAgfVxyXG5cclxuICBlZGl0RGVzaXJlZERheXMoZXZlbnQ6IGFueSk6IHZvaWQge1xyXG4gICAgdGhpcy5hZGRVcGRhdGVEZXNpcmVkRGF5cyhldmVudCk7XHJcbiAgfVxyXG5cclxuICBhZGRVcGRhdGVEZXNpcmVkRGF5cyhldmVudDogYW55KTogdm9pZCB7XHJcblxyXG4gICAgaWYgKGV2ZW50Lm5ld0RhdGEuZGVzaXJlZENhcmVUeXBlID09PSAnJykge1xyXG4gICAgICB0aGlzLl90b2FzdGVyU2VydmljZVxyXG4gICAgICAgIC5wb3BBc3luYygnZXJyb3InLCAnQ2FyZSB0eXBlIGNhbm5vdCBiZSBlbXB0eScsICcnKTtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuc291cmNlLmdldEVsZW1lbnRzKClcclxuICAgICAgLnRoZW4oKGVsZW1lbnRzOiBFbnJvbG1lbnRGb3JtRGVzaXJlZERheXNEdG9bXSkgPT4ge1xyXG5cclxuICAgICAgICB2YXIgZGVzaXJlZENhcmVUeXBlRXhpc3RzID1cclxuICAgICAgICAgIGVsZW1lbnRzLmZpbHRlcigoZWw6IEVucm9sbWVudEZvcm1EZXNpcmVkRGF5c0R0bykgPT5cclxuICAgICAgICAgICAgZWwuZGVzaXJlZENhcmVUeXBlID09PSBwYXJzZUludChldmVudC5uZXdEYXRhLmRlc2lyZWRDYXJlVHlwZSkgJiZcclxuICAgICAgICAgICAgKGV2ZW50LmRhdGEgPT09IHVuZGVmaW5lZCB8fFxyXG4gICAgICAgICAgICAgIHBhcnNlSW50KGV2ZW50Lm5ld0RhdGEuZGVzaXJlZENhcmVUeXBlKSAhPT0gZXZlbnQuZGF0YS5kZXNpcmVkQ2FyZVR5cGUpXHJcbiAgICAgICAgICApLmxlbmd0aCA+IDA7XHJcblxyXG4gICAgICAgIGlmIChkZXNpcmVkQ2FyZVR5cGVFeGlzdHMpIHtcclxuICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5wb3BBc3luYygnZXJyb3InLCAnRGF5cyBmb3IgJyArIHRoaXMuY2FyZVR5cGVzW2V2ZW50Lm5ld0RhdGEuZGVzaXJlZENhcmVUeXBlXSArICcgYWxyZWFkeSBleGlzdCcsICcnKTtcclxuICAgICAgICAgIHJldHVybiBldmVudC5jb25maXJtLnJlamVjdCgpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBsZXQgc2F2aW5nVG9hc3QgPSB0aGlzLl90b2FzdGVyU2VydmljZS5wb3AobmV3IFNwaW5uaW5nVG9hc3QoKSk7XHJcbiAgICAgICAgICBsZXQgdXBkYXRlRGVzaXJlZERheXM6IENyZWF0ZVVwZGF0ZUVucm9sbWVudEZvcm1EZXNpcmVkRGF5c0R0byA9IG5ldyBDcmVhdGVVcGRhdGVFbnJvbG1lbnRGb3JtRGVzaXJlZERheXNEdG8oKTtcclxuICAgICAgICAgIHVwZGF0ZURlc2lyZWREYXlzLmluaXQoe1xyXG4gICAgICAgICAgICBDZW50cmVJZDogdGhpcy5jZW50cmVJZCxcclxuICAgICAgICAgICAgS2lkSWQ6IHRoaXMua2lkSWQsXHJcbiAgICAgICAgICAgIERlc2lyZWRDYXJlVHlwZTogZXZlbnQubmV3RGF0YS5kZXNpcmVkQ2FyZVR5cGUsXHJcbiAgICAgICAgICAgIERlc2lyZWRTdGFydERhdGU6IGV2ZW50Lm5ld0RhdGEuZGVzaXJlZFN0YXJ0RGF0ZSxcclxuICAgICAgICAgICAgTW9uOiBldmVudC5uZXdEYXRhLm1vbixcclxuICAgICAgICAgICAgVHVlOiBldmVudC5uZXdEYXRhLnR1ZSxcclxuICAgICAgICAgICAgV2VkOiBldmVudC5uZXdEYXRhLndlZCxcclxuICAgICAgICAgICAgVGh1OiBldmVudC5uZXdEYXRhLnRodSxcclxuICAgICAgICAgICAgRnJpOiBldmVudC5uZXdEYXRhLmZyaSxcclxuICAgICAgICAgICAgU2F0OiBldmVudC5uZXdEYXRhLnNhdCxcclxuICAgICAgICAgICAgU3VuOiBldmVudC5uZXdEYXRhLnN1blxyXG4gICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgdGhpcy5fZW5yb2xtZW50U2VydmljZVxyXG4gICAgICAgICAgICAuYWRkVXBkYXRlS2lkRGVzaXJlZERheXMkKHVwZGF0ZURlc2lyZWREYXlzKVxyXG4gICAgICAgICAgICAudG9Qcm9taXNlKClcclxuICAgICAgICAgICAgLnRoZW4oKHJlcykgPT4ge1xyXG4gICAgICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLmNsZWFyKHNhdmluZ1RvYXN0LnRvYXN0SWQsIHNhdmluZ1RvYXN0LnRvYXN0Q29udGFpbmVySWQpO1xyXG4gICAgICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLnBvcEFzeW5jKCdzdWNjZXNzJywgJ1NhdmVkJywgJycpO1xyXG4gICAgICAgICAgICAgIHJldHVybiBldmVudC5jb25maXJtLnJlc29sdmUocmVzKTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLmNhdGNoKChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLmNsZWFyKHNhdmluZ1RvYXN0LnRvYXN0SWQsIHNhdmluZ1RvYXN0LnRvYXN0Q29udGFpbmVySWQpO1xyXG4gICAgICAgICAgICAgIHJldHVybiBldmVudC5jb25maXJtLnJlamVjdCgpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcmVtb3ZlRGVzaXJlZERheXMoZXZlbnQ6IGFueSk6IHZvaWQge1xyXG4gICAgaWYgKHdpbmRvdy5jb25maXJtKCdBcmUgeW91IHN1cmUgeW91IHdhbnQgdG8gZGVsZXRlPycpKSB7XHJcblxyXG4gICAgICBsZXQgc2F2aW5nVG9hc3QgPSB0aGlzLl90b2FzdGVyU2VydmljZS5wb3AobmV3IFNwaW5uaW5nVG9hc3QoKSk7XHJcbiAgICAgIGxldCBkYXlzVG9EZWxldGUgPSBuZXcgRGVsZXRlRW5yb2xtZW50Rm9ybURlc2lyZWREYXlzRHRvKCk7XHJcbiAgICAgIGRheXNUb0RlbGV0ZS5pbml0KHtcclxuICAgICAgICBDZW50cmVJZDogdGhpcy5jZW50cmVJZCxcclxuICAgICAgICBLaWRJZDogdGhpcy5raWRJZCxcclxuICAgICAgICBDYXJlVHlwZTogZXZlbnQuZGF0YS5kZXNpcmVkQ2FyZVR5cGUsXHJcbiAgICAgIH0pO1xyXG4gICAgICB0aGlzLl9lbnJvbG1lbnRTZXJ2aWNlXHJcbiAgICAgICAgLmRlbGV0ZUtpZERlc2lyZWREYXlzJChkYXlzVG9EZWxldGUpXHJcbiAgICAgICAgLnRvUHJvbWlzZSgpXHJcbiAgICAgICAgLnRoZW4oKHJlcykgPT4ge1xyXG4gICAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoc2F2aW5nVG9hc3QudG9hc3RJZCwgc2F2aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCk7XHJcbiAgICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5wb3BBc3luYygnc3VjY2VzcycsICdEZWxldGVkJywgJycpO1xyXG4gICAgICAgICAgcmV0dXJuIGV2ZW50LmNvbmZpcm0ucmVzb2x2ZSgpO1xyXG4gICAgICAgIH0pXHJcbiAgICAgICAgLmNhdGNoKChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoc2F2aW5nVG9hc3QudG9hc3RJZCwgc2F2aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCk7XHJcbiAgICAgICAgICByZXR1cm4gZXZlbnQuY29uZmlybS5yZWplY3QoKTtcclxuICAgICAgICB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGV2ZW50LmNvbmZpcm0ucmVqZWN0KCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBjb250aW51ZSgpOiB2b2lkIHtcclxuICAgIHRoaXMuX3JvdXRlci5uYXZpZ2F0ZShbJy4uLy4uL2tpZC1oZWFsdGgtaW5mbycsIHRoaXMua2lkSWRdLCB7IHJlbGF0aXZlVG86IHRoaXMuX3JvdXRlIH0pO1xyXG4gIH1cclxuXHJcbiAgY2FuY2VsRWRpdCgpOiB2b2lkIHtcclxuICAgIHRoaXMuX3JvdXRlci5uYXZpZ2F0ZShbJy4uLy4uL2Vucm9sbWVudC1mb3JtJ10sIHsgcmVsYXRpdmVUbzogdGhpcy5fcm91dGUgfSk7XHJcbiAgfVxyXG5cclxuICBnZXRDYXJlVHlwZVZpZXdNb2RlbCgpOiB7IFtrZXk6IHN0cmluZ106IHN0cmluZyB9W10ge1xyXG4gICAgbGV0IGNhcmVUeXBlc1ZpZXc6IHsgW2tleTogc3RyaW5nXTogc3RyaW5nIH1bXSA9IG5ldyBBcnJheTx7IFtrZXk6IHN0cmluZ106IHN0cmluZyB9PigpO1xyXG4gICAgZm9yIChsZXQga2V5IGluIHRoaXMuY2FyZVR5cGVzKSB7XHJcbiAgICAgIGlmICh0aGlzLmNhcmVUeXBlcy5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XHJcbiAgICAgICAgY2FyZVR5cGVzVmlldy5wdXNoKHtcclxuICAgICAgICAgIHRpdGxlOiB0aGlzLmNhcmVUeXBlc1trZXldLFxyXG4gICAgICAgICAgdmFsdWU6IGtleVxyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gY2FyZVR5cGVzVmlldztcclxuICB9XHJcblxyXG4gIHByaXZhdGUgaW5pdFNldHRpbmdzKCk6IHZvaWQge1xyXG4gICAgdGhpcy5zZXR0aW5ncyA9IHtcclxuICAgICAgbW9kZTogJ2ludGVybmFsJyxcclxuICAgICAgaGlkZUhlYWRlcjogZmFsc2UsXHJcbiAgICAgIGhpZGVTdWJIZWFkZXI6IGZhbHNlLFxyXG4gICAgICBzb3J0OiBmYWxzZSxcclxuICAgICAgYWN0aW9uczoge1xyXG4gICAgICAgIGNvbHVtblRpdGxlOiAnJyxcclxuICAgICAgICBhZGQ6IHRydWUsXHJcbiAgICAgICAgZWRpdDogdHJ1ZSxcclxuICAgICAgICBkZWxldGU6IHRydWUsXHJcbiAgICAgICAgcG9zaXRpb246ICdyaWdodCdcclxuICAgICAgfSxcclxuICAgICAgYWRkOiB7XHJcbiAgICAgICAgYWRkQnV0dG9uQ29udGVudDogJ0FkZCcsXHJcbiAgICAgICAgY3JlYXRlQnV0dG9uQ29udGVudDogJ0NyZWF0ZScsXHJcbiAgICAgICAgY2FuY2VsQnV0dG9uQ29udGVudDogJ0NhbmNlbCcsXHJcbiAgICAgICAgY29uZmlybUNyZWF0ZTogdHJ1ZVxyXG4gICAgICB9LFxyXG4gICAgICBlZGl0OiB7XHJcbiAgICAgICAgZWRpdEJ1dHRvbkNvbnRlbnQ6ICdFZGl0JyxcclxuICAgICAgICBzYXZlQnV0dG9uQ29udGVudDogJ1NhdmUnLFxyXG4gICAgICAgIGNhbmNlbEJ1dHRvbkNvbnRlbnQ6ICdDYW5jZWwnLFxyXG4gICAgICAgIGNvbmZpcm1TYXZlOiB0cnVlXHJcbiAgICAgIH0sXHJcbiAgICAgIGRlbGV0ZToge1xyXG4gICAgICAgIGRlbGV0ZUJ1dHRvbkNvbnRlbnQ6ICdSZW1vdmUnLFxyXG4gICAgICAgIGNvbmZpcm1EZWxldGU6IHRydWVcclxuICAgICAgfSxcclxuICAgICAgY29sdW1uczoge1xyXG4gICAgICAgIGRlc2lyZWRDYXJlVHlwZToge1xyXG4gICAgICAgICAgdGl0bGU6ICdDYXJlIFR5cGUnLFxyXG4gICAgICAgICAgZmlsdGVyOiBmYWxzZSxcclxuICAgICAgICAgIHR5cGU6ICdjdXN0b20nLFxyXG4gICAgICAgICAgcmVuZGVyQ29tcG9uZW50OiBEaXNwbGF5Q2FyZVR5cGVDb21wb25lbnQsXHJcbiAgICAgICAgICBlZGl0b3I6IHtcclxuICAgICAgICAgICAgdHlwZTogJ2xpc3QnLFxyXG4gICAgICAgICAgICBjb25maWc6IHtcclxuICAgICAgICAgICAgICBsaXN0OiB0aGlzLmdldENhcmVUeXBlVmlld01vZGVsKClcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSxcclxuICAgICAgICB9LFxyXG4gICAgICAgIG1vbjoge1xyXG4gICAgICAgICAgdGl0bGU6ICdNb25kYXknLFxyXG4gICAgICAgICAgZmlsdGVyOiBmYWxzZSxcclxuICAgICAgICAgIHR5cGU6ICdjdXN0b20nLFxyXG4gICAgICAgICAgcmVuZGVyQ29tcG9uZW50OiBEaXNwbGF5RGlzYWJsZWRDaGVja2JveENvbXBvbmVudCxcclxuICAgICAgICAgIGVkaXRvcjoge1xyXG4gICAgICAgICAgICB0eXBlOiAnY2hlY2tib3gnXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICB0dWU6IHtcclxuICAgICAgICAgIHRpdGxlOiAnVHVlc2RheScsXHJcbiAgICAgICAgICBmaWx0ZXI6IGZhbHNlLFxyXG4gICAgICAgICAgdHlwZTogJ2N1c3RvbScsXHJcbiAgICAgICAgICByZW5kZXJDb21wb25lbnQ6IERpc3BsYXlEaXNhYmxlZENoZWNrYm94Q29tcG9uZW50LFxyXG4gICAgICAgICAgZWRpdG9yOiB7XHJcbiAgICAgICAgICAgIHR5cGU6ICdjaGVja2JveCdcclxuICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIHdlZDoge1xyXG4gICAgICAgICAgdGl0bGU6ICdXZWRuZXNkYXknLFxyXG4gICAgICAgICAgZmlsdGVyOiBmYWxzZSxcclxuICAgICAgICAgIHR5cGU6ICdjdXN0b20nLFxyXG4gICAgICAgICAgcmVuZGVyQ29tcG9uZW50OiBEaXNwbGF5RGlzYWJsZWRDaGVja2JveENvbXBvbmVudCxcclxuICAgICAgICAgIGVkaXRvcjoge1xyXG4gICAgICAgICAgICB0eXBlOiAnY2hlY2tib3gnXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICB0aHU6IHtcclxuICAgICAgICAgIHRpdGxlOiAnVGh1cnNkYXknLFxyXG4gICAgICAgICAgZmlsdGVyOiBmYWxzZSxcclxuICAgICAgICAgIHR5cGU6ICdjdXN0b20nLFxyXG4gICAgICAgICAgcmVuZGVyQ29tcG9uZW50OiBEaXNwbGF5RGlzYWJsZWRDaGVja2JveENvbXBvbmVudCxcclxuICAgICAgICAgIGVkaXRvcjoge1xyXG4gICAgICAgICAgICB0eXBlOiAnY2hlY2tib3gnXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICBmcmk6IHtcclxuICAgICAgICAgIHRpdGxlOiAnRnJpZGF5JyxcclxuICAgICAgICAgIGZpbHRlcjogZmFsc2UsXHJcbiAgICAgICAgICB0eXBlOiAnY3VzdG9tJyxcclxuICAgICAgICAgIHJlbmRlckNvbXBvbmVudDogRGlzcGxheURpc2FibGVkQ2hlY2tib3hDb21wb25lbnQsXHJcbiAgICAgICAgICBlZGl0b3I6IHtcclxuICAgICAgICAgICAgdHlwZTogJ2NoZWNrYm94J1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgc2F0OiB7XHJcbiAgICAgICAgICB0aXRsZTogJ1NhdHVyZGF5JyxcclxuICAgICAgICAgIGZpbHRlcjogZmFsc2UsXHJcbiAgICAgICAgICB0eXBlOiAnY3VzdG9tJyxcclxuICAgICAgICAgIHJlbmRlckNvbXBvbmVudDogRGlzcGxheURpc2FibGVkQ2hlY2tib3hDb21wb25lbnQsXHJcbiAgICAgICAgICBlZGl0b3I6IHtcclxuICAgICAgICAgICAgdHlwZTogJ2NoZWNrYm94J1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgc3VuOiB7XHJcbiAgICAgICAgICB0aXRsZTogJ1N1bmRheScsXHJcbiAgICAgICAgICBmaWx0ZXI6IGZhbHNlLFxyXG4gICAgICAgICAgdHlwZTogJ2N1c3RvbScsXHJcbiAgICAgICAgICByZW5kZXJDb21wb25lbnQ6IERpc3BsYXlEaXNhYmxlZENoZWNrYm94Q29tcG9uZW50LFxyXG4gICAgICAgICAgZWRpdG9yOiB7XHJcbiAgICAgICAgICAgIHR5cGU6ICdjaGVja2JveCdcclxuICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIGRlc2lyZWRTdGFydERhdGU6IHtcclxuICAgICAgICAgIHRpdGxlOiAnRGVzaXJlZCBTdGFydCBEYXRlJyxcclxuICAgICAgICAgIGZpbHRlcjogZmFsc2UsXHJcbiAgICAgICAgICB2YWx1ZVByZXBhcmVGdW5jdGlvbjogKHZhbHVlOiBhbnkpID0+IHZhbHVlICE9PSAnJyA/IHZhbHVlLnRvRGF0ZVN0cmluZygpIDogJycsXHJcbiAgICAgICAgICBlZGl0b3I6IHtcclxuICAgICAgICAgICAgdHlwZTogJ2N1c3RvbScsXHJcbiAgICAgICAgICAgIGNvbXBvbmVudDogRWRpdERhdGVDb21wb25lbnRcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH0sXHJcbiAgICAgIG5vRGF0YU1lc3NhZ2U6ICdObyBEZXNpcmVkIERheXMnXHJcbiAgICB9O1xyXG4gIH1cclxufVxyXG4iXX0=
