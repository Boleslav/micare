"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../../../../shared/ng2-smart-table/index');
var EditDateComponent = (function (_super) {
    __extends(EditDateComponent, _super);
    function EditDateComponent() {
        _super.apply(this, arguments);
    }
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], EditDateComponent.prototype, "value", void 0);
    EditDateComponent = __decorate([
        core_1.Component({
            template: "\n    <input id=\"inp-desiredStartDate\" type=\"date\" [(ngModel)]=\"this.cell.newValue\" class=\"form-control\" useValueAsDate>\n    "
        }), 
        __metadata('design:paramtypes', [])
    ], EditDateComponent);
    return EditDateComponent;
}(index_1.DefaultEditor));
exports.EditDateComponent = EditDateComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQva2lkLWVkaXQtZW5yb2xtZW50L2tpZC1kZXNpcmVkLWRheXMvY29tcG9uZW50cy9lZGl0LWRhdGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7OztBQUFBLHFCQUFpQyxlQUFlLENBQUMsQ0FBQTtBQUNqRCxzQkFBOEIsZ0RBQWdELENBQUMsQ0FBQTtBQU8vRTtJQUF1QyxxQ0FBYTtJQUFwRDtRQUF1Qyw4QkFBYTtJQUdwRCxDQUFDO0lBREc7UUFBQyxZQUFLLEVBQUU7O29EQUFBO0lBUFo7UUFBQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLHdJQUVUO1NBQ0osQ0FBQzs7eUJBQUE7SUFJRix3QkFBQztBQUFELENBSEEsQUFHQyxDQUhzQyxxQkFBYSxHQUduRDtBQUhZLHlCQUFpQixvQkFHN0IsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvcGFyZW50cy1wb3J0YWwvZW5yb2xtZW50L2tpZC1lZGl0LWVucm9sbWVudC9raWQtZGVzaXJlZC1kYXlzL2NvbXBvbmVudHMvZWRpdC1kYXRlLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRGVmYXVsdEVkaXRvciB9IGZyb20gJy4uLy4uLy4uLy4uLy4uLy4uL3NoYXJlZC9uZzItc21hcnQtdGFibGUvaW5kZXgnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICB0ZW1wbGF0ZTogYFxyXG4gICAgPGlucHV0IGlkPVwiaW5wLWRlc2lyZWRTdGFydERhdGVcIiB0eXBlPVwiZGF0ZVwiIFsobmdNb2RlbCldPVwidGhpcy5jZWxsLm5ld1ZhbHVlXCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiB1c2VWYWx1ZUFzRGF0ZT5cclxuICAgIGBcclxufSlcclxuZXhwb3J0IGNsYXNzIEVkaXREYXRlQ29tcG9uZW50IGV4dGVuZHMgRGVmYXVsdEVkaXRvciB7XHJcblxyXG4gICAgQElucHV0KCkgdmFsdWU6IHN0cmluZyB8IG51bWJlcjtcclxufVxyXG4iXX0=
