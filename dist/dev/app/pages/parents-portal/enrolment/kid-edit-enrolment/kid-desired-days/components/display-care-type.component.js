"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../index');
var DisplayCareTypeComponent = (function () {
    function DisplayCareTypeComponent(_enrolmentService) {
        this._enrolmentService = _enrolmentService;
        this.careTypeName = '';
    }
    DisplayCareTypeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._enrolmentService
            .getCareTypes$()
            .subscribe(function (res) {
            var name = res[_this.value];
            if (name !== undefined)
                _this.careTypeName = name;
        });
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], DisplayCareTypeComponent.prototype, "value", void 0);
    DisplayCareTypeComponent = __decorate([
        core_1.Component({
            template: "\n    <span>{{ careTypeName }}</span>\n    "
        }), 
        __metadata('design:paramtypes', [index_1.EnrolmentService])
    ], DisplayCareTypeComponent);
    return DisplayCareTypeComponent;
}());
exports.DisplayCareTypeComponent = DisplayCareTypeComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQva2lkLWVkaXQtZW5yb2xtZW50L2tpZC1kZXNpcmVkLWRheXMvY29tcG9uZW50cy9kaXNwbGF5LWNhcmUtdHlwZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUF5QyxlQUFlLENBQUMsQ0FBQTtBQUN6RCxzQkFBaUMsZ0JBQWdCLENBQUMsQ0FBQTtBQVFsRDtJQUlJLGtDQUFvQixpQkFBbUM7UUFBbkMsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFrQjtRQUNuRCxJQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztJQUMzQixDQUFDO0lBRUQsMkNBQVEsR0FBUjtRQUFBLGlCQVFDO1FBUEcsSUFBSSxDQUFDLGlCQUFpQjthQUNqQixhQUFhLEVBQUU7YUFDZixTQUFTLENBQUMsVUFBQSxHQUFHO1lBQ1YsSUFBSSxJQUFJLEdBQVcsR0FBRyxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNuQyxFQUFFLENBQUMsQ0FBQyxJQUFJLEtBQUssU0FBUyxDQUFDO2dCQUNuQixLQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztRQUNqQyxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFkRDtRQUFDLFlBQUssRUFBRTs7MkRBQUE7SUFQWjtRQUFDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsNkNBRVQ7U0FDSixDQUFDOztnQ0FBQTtJQWtCRiwrQkFBQztBQUFELENBakJBLEFBaUJDLElBQUE7QUFqQlksZ0NBQXdCLDJCQWlCcEMsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvcGFyZW50cy1wb3J0YWwvZW5yb2xtZW50L2tpZC1lZGl0LWVucm9sbWVudC9raWQtZGVzaXJlZC1kYXlzL2NvbXBvbmVudHMvZGlzcGxheS1jYXJlLXR5cGUuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEVucm9sbWVudFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9pbmRleCc7XHJcbmltcG9ydCB7IFZpZXdDZWxsIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vLi4vc2hhcmVkL25nMi1zbWFydC10YWJsZS9pbmRleCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHRlbXBsYXRlOiBgXHJcbiAgICA8c3Bhbj57eyBjYXJlVHlwZU5hbWUgfX08L3NwYW4+XHJcbiAgICBgXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBEaXNwbGF5Q2FyZVR5cGVDb21wb25lbnQgaW1wbGVtZW50cyBWaWV3Q2VsbCwgT25Jbml0IHtcclxuXHJcbiAgICBASW5wdXQoKSB2YWx1ZTogc3RyaW5nIHwgbnVtYmVyO1xyXG4gICAgcHJpdmF0ZSBjYXJlVHlwZU5hbWU6IHN0cmluZztcclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX2Vucm9sbWVudFNlcnZpY2U6IEVucm9sbWVudFNlcnZpY2UpIHtcclxuICAgICAgICB0aGlzLmNhcmVUeXBlTmFtZSA9ICcnO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIHRoaXMuX2Vucm9sbWVudFNlcnZpY2VcclxuICAgICAgICAgICAgLmdldENhcmVUeXBlcyQoKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKHJlcyA9PiB7XHJcbiAgICAgICAgICAgICAgICBsZXQgbmFtZTogc3RyaW5nID0gcmVzW3RoaXMudmFsdWVdO1xyXG4gICAgICAgICAgICAgICAgaWYgKG5hbWUgIT09IHVuZGVmaW5lZClcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNhcmVUeXBlTmFtZSA9IG5hbWU7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
