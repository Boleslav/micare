"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var router_1 = require('@angular/router');
var rxjs_1 = require('rxjs');
var index_1 = require('../../index');
var index_2 = require('../../../index');
var index_3 = require('../../../../../api/index');
var angular2_toaster_1 = require('angular2-toaster');
var index_4 = require('../../../../../shared/index');
var KidCustodyInfoComponent = (function () {
    function KidCustodyInfoComponent(_router, _route, _enrolmentService, _centreService, _toasterService, fb) {
        this._router = _router;
        this._route = _route;
        this._enrolmentService = _enrolmentService;
        this._centreService = _centreService;
        this._toasterService = _toasterService;
        this.centreName = null;
        this.kidDto = null;
        this.initForm(fb);
    }
    KidCustodyInfoComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.centreName = this._centreService.SelectedCentreQualifiedName;
        this._route
            .params
            .flatMap(function (params) {
            _this.kidId = params['kidId'];
            return rxjs_1.Observable.of(_this.kidId);
        })
            .flatMap(function (kidId) { return _this._enrolmentService.getChildCustodyInformation$(_this.kidId); })
            .subscribe(function (res) {
            _this.kidDto = res;
        });
    };
    KidCustodyInfoComponent.prototype.saveChanges = function () {
        var _this = this;
        var savingToast = this._toasterService.pop(new index_4.SpinningToast());
        var updateCustodyInfo = new index_3.CreateUpdateChildCustodyInformationDto();
        updateCustodyInfo.init(this.kidDto.toJSON());
        updateCustodyInfo.kidId = this.kidId;
        this._enrolmentService
            .addUpdateChildCustodyInformationDto$(updateCustodyInfo)
            .subscribe(function (res) {
            _this.kidDto = res;
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this._toasterService.popAsync('success', 'Saved', '');
            _this._router.navigate(['../../enrolment-form'], { relativeTo: _this._route });
        }, function (error) {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
        });
    };
    KidCustodyInfoComponent.prototype.cancelEdit = function () {
        this._router.navigate(['../../enrolment-form'], { relativeTo: this._route });
    };
    KidCustodyInfoComponent.prototype.initForm = function (fb) {
        this.form = fb.group({
            'jointCustody': [''],
            'courtOrderAccessibility': [''],
            'courtOrderResidence': [''],
        });
        this.jointCustody = this.form.controls['jointCustody'];
        this.courtOrderAccessibility = this.form.controls['courtOrderAccessibility'];
        this.courtOrderResidence = this.form.controls['courtOrderResidence'];
    };
    KidCustodyInfoComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            encapsulation: core_1.ViewEncapsulation.None,
            selector: 'kid-custody-info-cmp',
            templateUrl: 'kid-custody-info.component.html',
            styleUrls: ['kid-custody-info.component.css']
        }), 
        __metadata('design:paramtypes', [router_1.Router, router_1.ActivatedRoute, index_1.EnrolmentService, index_2.CentreService, angular2_toaster_1.ToasterService, forms_1.FormBuilder])
    ], KidCustodyInfoComponent);
    return KidCustodyInfoComponent;
}());
exports.KidCustodyInfoComponent = KidCustodyInfoComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQva2lkLWVkaXQtZW5yb2xtZW50L2tpZC1jdXN0b2R5LWluZm8va2lkLWN1c3RvZHktaW5mby5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUFzRCxlQUFlLENBQUMsQ0FBQTtBQUN0RSxzQkFBd0QsZ0JBQWdCLENBQUMsQ0FBQTtBQUN6RSx1QkFBK0MsaUJBQWlCLENBQUMsQ0FBQTtBQUNqRSxxQkFBMkIsTUFBTSxDQUFDLENBQUE7QUFFbEMsc0JBQWlDLGFBQWEsQ0FBQyxDQUFBO0FBQy9DLHNCQUE4QixnQkFBZ0IsQ0FBQyxDQUFBO0FBQy9DLHNCQUFtRiwwQkFBMEIsQ0FBQyxDQUFBO0FBQzlHLGlDQUErQixrQkFBa0IsQ0FBQyxDQUFBO0FBQ2xELHNCQUE4Qiw2QkFBNkIsQ0FBQyxDQUFBO0FBVTVEO0lBVUUsaUNBQW9CLE9BQWUsRUFDekIsTUFBc0IsRUFDdEIsaUJBQW1DLEVBQ25DLGNBQTZCLEVBQzdCLGVBQStCLEVBQ3ZDLEVBQWU7UUFMRyxZQUFPLEdBQVAsT0FBTyxDQUFRO1FBQ3pCLFdBQU0sR0FBTixNQUFNLENBQWdCO1FBQ3RCLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBa0I7UUFDbkMsbUJBQWMsR0FBZCxjQUFjLENBQWU7UUFDN0Isb0JBQWUsR0FBZixlQUFlLENBQWdCO1FBWmpDLGVBQVUsR0FBVyxJQUFJLENBQUM7UUFFMUIsV0FBTSxHQUErQixJQUFJLENBQUM7UUFZaEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUNwQixDQUFDO0lBRUQsMENBQVEsR0FBUjtRQUFBLGlCQWFDO1FBWEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLDJCQUEyQixDQUFDO1FBQ2xFLElBQUksQ0FBQyxNQUFNO2FBQ1IsTUFBTTthQUNOLE9BQU8sQ0FBQyxVQUFDLE1BQWM7WUFDdEIsS0FBSSxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDN0IsTUFBTSxDQUFDLGlCQUFVLENBQUMsRUFBRSxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNuQyxDQUFDLENBQUM7YUFDRCxPQUFPLENBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxLQUFJLENBQUMsaUJBQWlCLENBQUMsMkJBQTJCLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxFQUE5RCxDQUE4RCxDQUFDO2FBQ2hGLFNBQVMsQ0FBQyxVQUFBLEdBQUc7WUFDWixLQUFJLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQztRQUNwQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCw2Q0FBVyxHQUFYO1FBQUEsaUJBZ0JDO1FBZkMsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsSUFBSSxxQkFBYSxFQUFFLENBQUMsQ0FBQztRQUNoRSxJQUFJLGlCQUFpQixHQUFHLElBQUksOENBQXNDLEVBQUUsQ0FBQztRQUNyRSxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO1FBQzdDLGlCQUFpQixDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQ3JDLElBQUksQ0FBQyxpQkFBaUI7YUFDbkIsb0NBQW9DLENBQUMsaUJBQWlCLENBQUM7YUFDdkQsU0FBUyxDQUFDLFVBQUEsR0FBRztZQUNaLEtBQUksQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDO1lBQ2xCLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDOUUsS0FBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLE9BQU8sRUFBRSxFQUFFLENBQUMsQ0FBQztZQUN0RCxLQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLHNCQUFzQixDQUFDLEVBQUUsRUFBRSxVQUFVLEVBQUUsS0FBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7UUFDL0UsQ0FBQyxFQUNELFVBQUMsS0FBSztZQUNKLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDaEYsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsNENBQVUsR0FBVjtRQUNFLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsc0JBQXNCLENBQUMsRUFBRSxFQUFFLFVBQVUsRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQztJQUMvRSxDQUFDO0lBRU8sMENBQVEsR0FBaEIsVUFBaUIsRUFBZTtRQUM5QixJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQyxLQUFLLENBQUM7WUFDbkIsY0FBYyxFQUFFLENBQUMsRUFBRSxDQUFDO1lBQ3BCLHlCQUF5QixFQUFFLENBQUMsRUFBRSxDQUFDO1lBQy9CLHFCQUFxQixFQUFFLENBQUMsRUFBRSxDQUFDO1NBQzVCLENBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDdkQsSUFBSSxDQUFDLHVCQUF1QixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLHlCQUF5QixDQUFDLENBQUM7UUFDN0UsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLHFCQUFxQixDQUFDLENBQUM7SUFDdkUsQ0FBQztJQTFFSDtRQUFDLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsYUFBYSxFQUFFLHdCQUFpQixDQUFDLElBQUk7WUFDckMsUUFBUSxFQUFFLHNCQUFzQjtZQUNoQyxXQUFXLEVBQUUsaUNBQWlDO1lBQzlDLFNBQVMsRUFBRSxDQUFDLGdDQUFnQyxDQUFDO1NBQzlDLENBQUM7OytCQUFBO0lBcUVGLDhCQUFDO0FBQUQsQ0FuRUEsQUFtRUMsSUFBQTtBQW5FWSwrQkFBdUIsMEJBbUVuQyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQva2lkLWVkaXQtZW5yb2xtZW50L2tpZC1jdXN0b2R5LWluZm8va2lkLWN1c3RvZHktaW5mby5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0VuY2Fwc3VsYXRpb24sIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1Hcm91cCwgQWJzdHJhY3RDb250cm9sLCBGb3JtQnVpbGRlciB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgUm91dGVyLCBBY3RpdmF0ZWRSb3V0ZSwgUGFyYW1zIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgRW5yb2xtZW50U2VydmljZSB9IGZyb20gJy4uLy4uL2luZGV4JztcclxuaW1wb3J0IHsgQ2VudHJlU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IHsgQ2hpbGRDdXN0b2R5SW5mb3JtYXRpb25EdG8sIENyZWF0ZVVwZGF0ZUNoaWxkQ3VzdG9keUluZm9ybWF0aW9uRHRvIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vYXBpL2luZGV4JztcclxuaW1wb3J0IHsgVG9hc3RlclNlcnZpY2UgfSBmcm9tICdhbmd1bGFyMi10b2FzdGVyJztcclxuaW1wb3J0IHsgU3Bpbm5pbmdUb2FzdCB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL3NoYXJlZC9pbmRleCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmUsXHJcbiAgc2VsZWN0b3I6ICdraWQtY3VzdG9keS1pbmZvLWNtcCcsXHJcbiAgdGVtcGxhdGVVcmw6ICdraWQtY3VzdG9keS1pbmZvLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsna2lkLWN1c3RvZHktaW5mby5jb21wb25lbnQuY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBLaWRDdXN0b2R5SW5mb0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIHByaXZhdGUgY2VudHJlTmFtZTogc3RyaW5nID0gbnVsbDtcclxuICBwcml2YXRlIGZvcm06IEZvcm1Hcm91cDtcclxuICBwcml2YXRlIGtpZER0bzogQ2hpbGRDdXN0b2R5SW5mb3JtYXRpb25EdG8gPSBudWxsO1xyXG4gIHByaXZhdGUga2lkSWQ6IHN0cmluZztcclxuICBwcml2YXRlIGpvaW50Q3VzdG9keTogQWJzdHJhY3RDb250cm9sO1xyXG4gIHByaXZhdGUgY291cnRPcmRlckFjY2Vzc2liaWxpdHk6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIGNvdXJ0T3JkZXJSZXNpZGVuY2U6IEFic3RyYWN0Q29udHJvbDtcclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBfcm91dGVyOiBSb3V0ZXIsXHJcbiAgICBwcml2YXRlIF9yb3V0ZTogQWN0aXZhdGVkUm91dGUsXHJcbiAgICBwcml2YXRlIF9lbnJvbG1lbnRTZXJ2aWNlOiBFbnJvbG1lbnRTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBfY2VudHJlU2VydmljZTogQ2VudHJlU2VydmljZSxcclxuICAgIHByaXZhdGUgX3RvYXN0ZXJTZXJ2aWNlOiBUb2FzdGVyU2VydmljZSxcclxuICAgIGZiOiBGb3JtQnVpbGRlcikge1xyXG4gICAgdGhpcy5pbml0Rm9ybShmYik7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuXHJcbiAgICB0aGlzLmNlbnRyZU5hbWUgPSB0aGlzLl9jZW50cmVTZXJ2aWNlLlNlbGVjdGVkQ2VudHJlUXVhbGlmaWVkTmFtZTtcclxuICAgIHRoaXMuX3JvdXRlXHJcbiAgICAgIC5wYXJhbXNcclxuICAgICAgLmZsYXRNYXAoKHBhcmFtczogUGFyYW1zKSA9PiB7XHJcbiAgICAgICAgdGhpcy5raWRJZCA9IHBhcmFtc1sna2lkSWQnXTtcclxuICAgICAgICByZXR1cm4gT2JzZXJ2YWJsZS5vZih0aGlzLmtpZElkKTtcclxuICAgICAgfSlcclxuICAgICAgLmZsYXRNYXAoa2lkSWQgPT4gdGhpcy5fZW5yb2xtZW50U2VydmljZS5nZXRDaGlsZEN1c3RvZHlJbmZvcm1hdGlvbiQodGhpcy5raWRJZCkpXHJcbiAgICAgIC5zdWJzY3JpYmUocmVzID0+IHtcclxuICAgICAgICB0aGlzLmtpZER0byA9IHJlcztcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBzYXZlQ2hhbmdlcygpIHtcclxuICAgIGxldCBzYXZpbmdUb2FzdCA9IHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLnBvcChuZXcgU3Bpbm5pbmdUb2FzdCgpKTtcclxuICAgIGxldCB1cGRhdGVDdXN0b2R5SW5mbyA9IG5ldyBDcmVhdGVVcGRhdGVDaGlsZEN1c3RvZHlJbmZvcm1hdGlvbkR0bygpO1xyXG4gICAgdXBkYXRlQ3VzdG9keUluZm8uaW5pdCh0aGlzLmtpZER0by50b0pTT04oKSk7XHJcbiAgICB1cGRhdGVDdXN0b2R5SW5mby5raWRJZCA9IHRoaXMua2lkSWQ7XHJcbiAgICB0aGlzLl9lbnJvbG1lbnRTZXJ2aWNlXHJcbiAgICAgIC5hZGRVcGRhdGVDaGlsZEN1c3RvZHlJbmZvcm1hdGlvbkR0byQodXBkYXRlQ3VzdG9keUluZm8pXHJcbiAgICAgIC5zdWJzY3JpYmUocmVzID0+IHtcclxuICAgICAgICB0aGlzLmtpZER0byA9IHJlcztcclxuICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcihzYXZpbmdUb2FzdC50b2FzdElkLCBzYXZpbmdUb2FzdC50b2FzdENvbnRhaW5lcklkKTtcclxuICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5wb3BBc3luYygnc3VjY2VzcycsICdTYXZlZCcsICcnKTtcclxuICAgICAgICB0aGlzLl9yb3V0ZXIubmF2aWdhdGUoWycuLi8uLi9lbnJvbG1lbnQtZm9ybSddLCB7IHJlbGF0aXZlVG86IHRoaXMuX3JvdXRlIH0pO1xyXG4gICAgICB9LFxyXG4gICAgICAoZXJyb3IpID0+IHtcclxuICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcihzYXZpbmdUb2FzdC50b2FzdElkLCBzYXZpbmdUb2FzdC50b2FzdENvbnRhaW5lcklkKTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBjYW5jZWxFZGl0KCkge1xyXG4gICAgdGhpcy5fcm91dGVyLm5hdmlnYXRlKFsnLi4vLi4vZW5yb2xtZW50LWZvcm0nXSwgeyByZWxhdGl2ZVRvOiB0aGlzLl9yb3V0ZSB9KTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgaW5pdEZvcm0oZmI6IEZvcm1CdWlsZGVyKTogdm9pZCB7XHJcbiAgICB0aGlzLmZvcm0gPSBmYi5ncm91cCh7XHJcbiAgICAgICdqb2ludEN1c3RvZHknOiBbJyddLFxyXG4gICAgICAnY291cnRPcmRlckFjY2Vzc2liaWxpdHknOiBbJyddLFxyXG4gICAgICAnY291cnRPcmRlclJlc2lkZW5jZSc6IFsnJ10sXHJcbiAgICB9KTtcclxuXHJcbiAgICB0aGlzLmpvaW50Q3VzdG9keSA9IHRoaXMuZm9ybS5jb250cm9sc1snam9pbnRDdXN0b2R5J107XHJcbiAgICB0aGlzLmNvdXJ0T3JkZXJBY2Nlc3NpYmlsaXR5ID0gdGhpcy5mb3JtLmNvbnRyb2xzWydjb3VydE9yZGVyQWNjZXNzaWJpbGl0eSddO1xyXG4gICAgdGhpcy5jb3VydE9yZGVyUmVzaWRlbmNlID0gdGhpcy5mb3JtLmNvbnRyb2xzWydjb3VydE9yZGVyUmVzaWRlbmNlJ107XHJcbiAgfVxyXG59XHJcbiJdfQ==
