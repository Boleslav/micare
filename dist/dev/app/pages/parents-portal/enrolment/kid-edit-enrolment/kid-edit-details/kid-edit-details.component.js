"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var router_1 = require('@angular/router');
var rxjs_1 = require('rxjs');
var index_1 = require('./index');
var index_2 = require('../../index');
var index_3 = require('../../../index');
var index_4 = require('../../../../../api/index');
var angular2_toaster_1 = require('angular2-toaster');
var index_5 = require('../../../../../shared/index');
var EnrolmentKidEditComponent = (function () {
    function EnrolmentKidEditComponent(_router, _route, _enrolmentService, _centreService, _toasterService, fb) {
        this._router = _router;
        this._route = _route;
        this._enrolmentService = _enrolmentService;
        this._centreService = _centreService;
        this._toasterService = _toasterService;
        this.maxDateOfBirth = null;
        this.minDateOfBirth = null;
        this.kidDto = null;
        var today = new Date();
        this.maxDateOfBirth = new Date().toJSON().split('T')[0];
        this.minDateOfBirth = new Date(today.getFullYear() - 7, today.getMonth()).toJSON().split('T')[0];
        this.form = fb.group({
            'firstname': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.maxLength(100)])],
            'lastname': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.maxLength(100)])],
            'crn': ['', forms_1.Validators.compose([forms_1.Validators.pattern('[0-9]{9}[a-zA-Z]')])],
            'dateOfBirth': ['', forms_1.Validators.required],
            'gender': ['', forms_1.Validators.required],
            'countryOfBirth': ['', forms_1.Validators.maxLength(50)],
            'streetAddress': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.maxLength(200)])],
            'suburb': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.maxLength(50)])],
            'postcode': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.pattern('(\\d{4})')])],
            'state': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.maxLength(50)])],
            'primaryLanguage': ['', forms_1.Validators.maxLength(50)],
            'secondaryLanguage': ['', forms_1.Validators.maxLength(50)],
            'aboriginalOrTorres': fb.group({
                'aboriginal': [''],
                'torres': [''],
                'neitherAboriginalNorTorres': [''],
            }, { validator: index_1.AboriginalOrTorresValidator.validate('aboriginal', 'torres', 'neitherAboriginalNorTorres') })
        });
        this.firstname = this.form.controls['firstname'];
        this.lastname = this.form.controls['lastname'];
        this.crn = this.form.controls['crn'];
        this.dateOfBirth = this.form.controls['dateOfBirth'];
        this.gender = this.form.controls['gender'];
        this.countryOfBirth = this.form.controls['countryOfBirth'];
        this.streetAddress = this.form.controls['streetAddress'];
        this.suburb = this.form.controls['suburb'];
        this.postcode = this.form.controls['postcode'];
        this.state = this.form.controls['state'];
        this.aboriginalOrTorres = this.form.controls['aboriginalOrTorres'];
        this.aboriginal = this.aboriginalOrTorres.controls['aboriginal'];
        this.torres = this.aboriginalOrTorres.controls['torres'];
        this.neitherAboriginalNorTorres = this.aboriginalOrTorres.controls['neitherAboriginalNorTorres'];
        this.primaryLanguage = this.form.controls['primaryLanguage'];
        this.secondaryLanguage = this.form.controls['secondaryLanguage'];
    }
    EnrolmentKidEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route
            .params
            .flatMap(function (params) { return rxjs_1.Observable.of(params['kidId']); })
            .flatMap(function (kidId) {
            return (kidId === null || kidId === undefined) ?
                rxjs_1.Observable.of(new index_4.KidDto()) :
                _this._enrolmentService.getKid$(kidId);
        })
            .subscribe(function (res) {
            _this.kidDto = res;
            _this.neitherAboriginalNorTorres.setValue(_this.kidDto.aboriginal === false && _this.kidDto.torres === false);
        });
    };
    EnrolmentKidEditComponent.prototype.saveChanges = function () {
        var _this = this;
        var savingToast = this._toasterService.pop(new index_5.SpinningToast());
        var updateKid = new index_4.CreateUpdateKidDto();
        updateKid.init(this.kidDto.toJSON());
        this._enrolmentService
            .createUpdateKid$(updateKid)
            .subscribe(function (res) {
            _this.kidDto = res;
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this._toasterService.popAsync('success', 'Saved', '');
            var url = (updateKid.id === undefined) ? '../enrolment-form' : '../../enrolment-form';
            _this._router.navigate([url], { relativeTo: _this._route });
        }, function (error) {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
        });
    };
    EnrolmentKidEditComponent.prototype.saveChangesAndContinue = function () {
        var _this = this;
        var savingToast = this._toasterService.pop(new index_5.SpinningToast());
        var updateKid = new index_4.CreateUpdateKidDto();
        updateKid.init(this.kidDto.toJSON());
        this._enrolmentService
            .createUpdateKid$(updateKid)
            .subscribe(function (res) {
            _this.kidDto = res;
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this._toasterService.popAsync('success', 'Saved', '');
            var url = (updateKid.id === undefined) ? '../kid-desired-days' : '../../kid-desired-days';
            _this._router.navigate([url, _this.kidDto.id], { relativeTo: _this._route });
        }, function (error) {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
        });
    };
    EnrolmentKidEditComponent.prototype.cancelEdit = function () {
        var url = this.cancelLink();
        this._router.navigate([url], { relativeTo: this._route });
    };
    EnrolmentKidEditComponent.prototype.cancelLink = function () {
        if (this.kidDto === null)
            return '';
        return (this.kidDto.id === undefined) ? '../enrolment-form' : '../../enrolment-form';
    };
    EnrolmentKidEditComponent.prototype.selectAboriginalOrTorres = function (checked) {
        if (checked) {
            this.neitherAboriginalNorTorres.setValue(false);
        }
    };
    EnrolmentKidEditComponent.prototype.selectNeitherAboriginalOrTorres = function (checked) {
        if (checked) {
            this.aboriginal.setValue(false);
            this.torres.setValue(false);
        }
    };
    EnrolmentKidEditComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            encapsulation: core_1.ViewEncapsulation.None,
            selector: 'kid-edit-details-cmp',
            templateUrl: 'kid-edit-details.component.html',
            styleUrls: ['kid-edit-details.component.css']
        }), 
        __metadata('design:paramtypes', [router_1.Router, router_1.ActivatedRoute, index_2.EnrolmentService, index_3.CentreService, angular2_toaster_1.ToasterService, forms_1.FormBuilder])
    ], EnrolmentKidEditComponent);
    return EnrolmentKidEditComponent;
}());
exports.EnrolmentKidEditComponent = EnrolmentKidEditComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQva2lkLWVkaXQtZW5yb2xtZW50L2tpZC1lZGl0LWRldGFpbHMva2lkLWVkaXQtZGV0YWlscy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUFzRCxlQUFlLENBQUMsQ0FBQTtBQUN0RSxzQkFBb0UsZ0JBQWdCLENBQUMsQ0FBQTtBQUNyRix1QkFBK0MsaUJBQWlCLENBQUMsQ0FBQTtBQUNqRSxxQkFBMkIsTUFBTSxDQUFDLENBQUE7QUFFbEMsc0JBQTRDLFNBQVMsQ0FBQyxDQUFBO0FBQ3RELHNCQUFpQyxhQUFhLENBQUMsQ0FBQTtBQUMvQyxzQkFBOEIsZ0JBQWdCLENBQUMsQ0FBQTtBQUMvQyxzQkFBMkMsMEJBQTBCLENBQUMsQ0FBQTtBQUN0RSxpQ0FBK0Isa0JBQWtCLENBQUMsQ0FBQTtBQUNsRCxzQkFBOEIsNkJBQTZCLENBQUMsQ0FBQTtBQVU1RDtJQXdCRSxtQ0FBb0IsT0FBZSxFQUN6QixNQUFzQixFQUN0QixpQkFBbUMsRUFDbkMsY0FBNkIsRUFDN0IsZUFBK0IsRUFDdkMsRUFBZTtRQUxHLFlBQU8sR0FBUCxPQUFPLENBQVE7UUFDekIsV0FBTSxHQUFOLE1BQU0sQ0FBZ0I7UUFDdEIsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFrQjtRQUNuQyxtQkFBYyxHQUFkLGNBQWMsQ0FBZTtRQUM3QixvQkFBZSxHQUFmLGVBQWUsQ0FBZ0I7UUExQmxDLG1CQUFjLEdBQVcsSUFBSSxDQUFDO1FBQzlCLG1CQUFjLEdBQVcsSUFBSSxDQUFDO1FBQzlCLFdBQU0sR0FBVyxJQUFJLENBQUM7UUEyQjNCLElBQUksS0FBSyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7UUFDdkIsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN4RCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUUsR0FBRyxDQUFDLEVBQUUsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRWpHLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQztZQUNuQixXQUFXLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdkYsVUFBVSxFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsa0JBQVUsQ0FBQyxRQUFRLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3RGLEtBQUssRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLGtCQUFVLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3pFLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLFFBQVEsQ0FBQztZQUN4QyxRQUFRLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxRQUFRLENBQUM7WUFDbkMsZ0JBQWdCLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDaEQsZUFBZSxFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsa0JBQVUsQ0FBQyxRQUFRLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzNGLFFBQVEsRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLGtCQUFVLENBQUMsUUFBUSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNuRixVQUFVLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsRUFBRSxrQkFBVSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDM0YsT0FBTyxFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsa0JBQVUsQ0FBQyxRQUFRLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2xGLGlCQUFpQixFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ2pELG1CQUFtQixFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ25ELG9CQUFvQixFQUFFLEVBQUUsQ0FBQyxLQUFLLENBQUM7Z0JBQzdCLFlBQVksRUFBRSxDQUFDLEVBQUUsQ0FBQztnQkFDbEIsUUFBUSxFQUFFLENBQUMsRUFBRSxDQUFDO2dCQUNkLDRCQUE0QixFQUFFLENBQUMsRUFBRSxDQUFDO2FBQ25DLEVBQUUsRUFBRSxTQUFTLEVBQUUsbUNBQTJCLENBQUMsUUFBUSxDQUFDLFlBQVksRUFBRSxRQUFRLEVBQUUsNEJBQTRCLENBQUMsRUFBRSxDQUFDO1NBQzlHLENBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDakQsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUMvQyxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3JDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDckQsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMzQyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDM0QsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUN6RCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzNDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDL0MsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsa0JBQWtCLEdBQWMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUM5RSxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDakUsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3pELElBQUksQ0FBQywwQkFBMEIsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLDRCQUE0QixDQUFDLENBQUM7UUFDakcsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQzdELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO0lBQ25FLENBQUM7SUFFRCw0Q0FBUSxHQUFSO1FBQUEsaUJBYUM7UUFYQyxJQUFJLENBQUMsTUFBTTthQUNSLE1BQU07YUFDTixPQUFPLENBQUMsVUFBQyxNQUFjLElBQUssT0FBQSxpQkFBVSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBOUIsQ0FBOEIsQ0FBQzthQUMzRCxPQUFPLENBQUMsVUFBQSxLQUFLO1lBQ1osT0FBQSxDQUFDLEtBQUssS0FBSyxJQUFJLElBQUksS0FBSyxLQUFLLFNBQVMsQ0FBQztnQkFDckMsaUJBQVUsQ0FBQyxFQUFFLENBQUMsSUFBSSxjQUFNLEVBQUUsQ0FBQztnQkFDM0IsS0FBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUM7UUFGdkMsQ0FFdUMsQ0FBQzthQUN6QyxTQUFTLENBQUMsVUFBQSxHQUFHO1lBQ1osS0FBSSxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUM7WUFDbEIsS0FBSSxDQUFDLDBCQUEwQixDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsS0FBSyxLQUFLLElBQUksS0FBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEtBQUssS0FBSyxDQUFDLENBQUM7UUFDN0csQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsK0NBQVcsR0FBWDtRQUFBLGlCQWlCQztRQWhCQyxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxJQUFJLHFCQUFhLEVBQUUsQ0FBQyxDQUFDO1FBQ2hFLElBQUksU0FBUyxHQUF1QixJQUFJLDBCQUFrQixFQUFFLENBQUM7UUFDN0QsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7UUFDckMsSUFBSSxDQUFDLGlCQUFpQjthQUNuQixnQkFBZ0IsQ0FBQyxTQUFTLENBQUM7YUFDM0IsU0FBUyxDQUFDLFVBQUEsR0FBRztZQUNaLEtBQUksQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDO1lBQ2xCLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDOUUsS0FBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLE9BQU8sRUFBRSxFQUFFLENBQUMsQ0FBQztZQUV0RCxJQUFJLEdBQUcsR0FBVyxDQUFDLFNBQVMsQ0FBQyxFQUFFLEtBQUssU0FBUyxDQUFDLEdBQUcsbUJBQW1CLEdBQUcsc0JBQXNCLENBQUM7WUFDOUYsS0FBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxFQUFFLFVBQVUsRUFBRSxLQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQztRQUM1RCxDQUFDLEVBQ0QsVUFBQyxLQUFLO1lBQ0osS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxXQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUNoRixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCwwREFBc0IsR0FBdEI7UUFBQSxpQkFpQkM7UUFoQkMsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsSUFBSSxxQkFBYSxFQUFFLENBQUMsQ0FBQztRQUNoRSxJQUFJLFNBQVMsR0FBdUIsSUFBSSwwQkFBa0IsRUFBRSxDQUFDO1FBQzdELFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO1FBQ3JDLElBQUksQ0FBQyxpQkFBaUI7YUFDbkIsZ0JBQWdCLENBQUMsU0FBUyxDQUFDO2FBQzNCLFNBQVMsQ0FBQyxVQUFBLEdBQUc7WUFDWixLQUFJLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQztZQUNsQixLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1lBQzlFLEtBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLFNBQVMsRUFBRSxPQUFPLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFFdEQsSUFBSSxHQUFHLEdBQVcsQ0FBQyxTQUFTLENBQUMsRUFBRSxLQUFLLFNBQVMsQ0FBQyxHQUFHLHFCQUFxQixHQUFHLHdCQUF3QixDQUFDO1lBQ2xHLEtBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxFQUFFLEtBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxVQUFVLEVBQUUsS0FBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7UUFDNUUsQ0FBQyxFQUNELFVBQUMsS0FBSztZQUNKLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDaEYsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsOENBQVUsR0FBVjtRQUNFLElBQUksR0FBRyxHQUFXLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUNwQyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEVBQUUsVUFBVSxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO0lBQzVELENBQUM7SUFFRCw4Q0FBVSxHQUFWO1FBQ0UsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sS0FBSyxJQUFJLENBQUM7WUFDdkIsTUFBTSxDQUFDLEVBQUUsQ0FBQztRQUNaLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxLQUFLLFNBQVMsQ0FBQyxHQUFHLG1CQUFtQixHQUFHLHNCQUFzQixDQUFDO0lBQ3ZGLENBQUM7SUFFRCw0REFBd0IsR0FBeEIsVUFBeUIsT0FBZ0I7UUFDdkMsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUNaLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDbEQsQ0FBQztJQUNILENBQUM7SUFFRCxtRUFBK0IsR0FBL0IsVUFBZ0MsT0FBZ0I7UUFDOUMsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUNaLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2hDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzlCLENBQUM7SUFDSCxDQUFDO0lBNUpIO1FBQUMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixhQUFhLEVBQUUsd0JBQWlCLENBQUMsSUFBSTtZQUNyQyxRQUFRLEVBQUUsc0JBQXNCO1lBQ2hDLFdBQVcsRUFBRSxpQ0FBaUM7WUFDOUMsU0FBUyxFQUFFLENBQUMsZ0NBQWdDLENBQUM7U0FDOUMsQ0FBQzs7aUNBQUE7SUF1SkYsZ0NBQUM7QUFBRCxDQXJKQSxBQXFKQyxJQUFBO0FBckpZLGlDQUF5Qiw0QkFxSnJDLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL3BhcmVudHMtcG9ydGFsL2Vucm9sbWVudC9raWQtZWRpdC1lbnJvbG1lbnQva2lkLWVkaXQtZGV0YWlscy9raWQtZWRpdC1kZXRhaWxzLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3RW5jYXBzdWxhdGlvbiwgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybUdyb3VwLCBBYnN0cmFjdENvbnRyb2wsIEZvcm1CdWlsZGVyLCBWYWxpZGF0b3JzIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBSb3V0ZXIsIEFjdGl2YXRlZFJvdXRlLCBQYXJhbXMgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcblxyXG5pbXBvcnQgeyBBYm9yaWdpbmFsT3JUb3JyZXNWYWxpZGF0b3IgfSBmcm9tICcuL2luZGV4JztcclxuaW1wb3J0IHsgRW5yb2xtZW50U2VydmljZSB9IGZyb20gJy4uLy4uL2luZGV4JztcclxuaW1wb3J0IHsgQ2VudHJlU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL2luZGV4JztcclxuaW1wb3J0IHsgS2lkRHRvLCBDcmVhdGVVcGRhdGVLaWREdG8gfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9hcGkvaW5kZXgnO1xyXG5pbXBvcnQgeyBUb2FzdGVyU2VydmljZSB9IGZyb20gJ2FuZ3VsYXIyLXRvYXN0ZXInO1xyXG5pbXBvcnQgeyBTcGlubmluZ1RvYXN0IH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vc2hhcmVkL2luZGV4JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcclxuICBzZWxlY3RvcjogJ2tpZC1lZGl0LWRldGFpbHMtY21wJyxcclxuICB0ZW1wbGF0ZVVybDogJ2tpZC1lZGl0LWRldGFpbHMuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWydraWQtZWRpdC1kZXRhaWxzLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEVucm9sbWVudEtpZEVkaXRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBwdWJsaWMgbWF4RGF0ZU9mQmlydGg6IHN0cmluZyA9IG51bGw7XHJcbiAgcHVibGljIG1pbkRhdGVPZkJpcnRoOiBzdHJpbmcgPSBudWxsO1xyXG4gIHB1YmxpYyBraWREdG86IEtpZER0byA9IG51bGw7XHJcblxyXG4gIHByaXZhdGUgZm9ybTogRm9ybUdyb3VwO1xyXG4gIHByaXZhdGUgZmlyc3RuYW1lOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBsYXN0bmFtZTogQWJzdHJhY3RDb250cm9sO1xyXG4gIHByaXZhdGUgZ2VuZGVyOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBkYXRlT2ZCaXJ0aDogQWJzdHJhY3RDb250cm9sO1xyXG4gIHByaXZhdGUgY291bnRyeU9mQmlydGg6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIHN0cmVldEFkZHJlc3M6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIHN1YnVyYjogQWJzdHJhY3RDb250cm9sO1xyXG4gIHByaXZhdGUgcG9zdGNvZGU6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIHN0YXRlOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBjcm46IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIGFib3JpZ2luYWxPclRvcnJlczogRm9ybUdyb3VwO1xyXG4gIHByaXZhdGUgYWJvcmlnaW5hbDogQWJzdHJhY3RDb250cm9sO1xyXG4gIHByaXZhdGUgdG9ycmVzOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBuZWl0aGVyQWJvcmlnaW5hbE5vclRvcnJlczogQWJzdHJhY3RDb250cm9sO1xyXG4gIHByaXZhdGUgcHJpbWFyeUxhbmd1YWdlOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBzZWNvbmRhcnlMYW5ndWFnZTogQWJzdHJhY3RDb250cm9sO1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9yb3V0ZXI6IFJvdXRlcixcclxuICAgIHByaXZhdGUgX3JvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcclxuICAgIHByaXZhdGUgX2Vucm9sbWVudFNlcnZpY2U6IEVucm9sbWVudFNlcnZpY2UsXHJcbiAgICBwcml2YXRlIF9jZW50cmVTZXJ2aWNlOiBDZW50cmVTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBfdG9hc3RlclNlcnZpY2U6IFRvYXN0ZXJTZXJ2aWNlLFxyXG4gICAgZmI6IEZvcm1CdWlsZGVyKSB7XHJcblxyXG4gICAgbGV0IHRvZGF5ID0gbmV3IERhdGUoKTtcclxuICAgIHRoaXMubWF4RGF0ZU9mQmlydGggPSBuZXcgRGF0ZSgpLnRvSlNPTigpLnNwbGl0KCdUJylbMF07XHJcbiAgICB0aGlzLm1pbkRhdGVPZkJpcnRoID0gbmV3IERhdGUodG9kYXkuZ2V0RnVsbFllYXIoKSAtIDcsIHRvZGF5LmdldE1vbnRoKCkpLnRvSlNPTigpLnNwbGl0KCdUJylbMF07XHJcblxyXG4gICAgdGhpcy5mb3JtID0gZmIuZ3JvdXAoe1xyXG4gICAgICAnZmlyc3RuYW1lJzogWycnLCBWYWxpZGF0b3JzLmNvbXBvc2UoW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDEwMCldKV0sXHJcbiAgICAgICdsYXN0bmFtZSc6IFsnJywgVmFsaWRhdG9ycy5jb21wb3NlKFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1heExlbmd0aCgxMDApXSldLFxyXG4gICAgICAnY3JuJzogWycnLCBWYWxpZGF0b3JzLmNvbXBvc2UoW1ZhbGlkYXRvcnMucGF0dGVybignWzAtOV17OX1bYS16QS1aXScpXSldLFxyXG4gICAgICAnZGF0ZU9mQmlydGgnOiBbJycsIFZhbGlkYXRvcnMucmVxdWlyZWRdLFxyXG4gICAgICAnZ2VuZGVyJzogWycnLCBWYWxpZGF0b3JzLnJlcXVpcmVkXSxcclxuICAgICAgJ2NvdW50cnlPZkJpcnRoJzogWycnLCBWYWxpZGF0b3JzLm1heExlbmd0aCg1MCldLFxyXG4gICAgICAnc3RyZWV0QWRkcmVzcyc6IFsnJywgVmFsaWRhdG9ycy5jb21wb3NlKFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1heExlbmd0aCgyMDApXSldLFxyXG4gICAgICAnc3VidXJiJzogWycnLCBWYWxpZGF0b3JzLmNvbXBvc2UoW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDUwKV0pXSxcclxuICAgICAgJ3Bvc3Rjb2RlJzogWycnLCBWYWxpZGF0b3JzLmNvbXBvc2UoW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMucGF0dGVybignKFxcXFxkezR9KScpXSldLFxyXG4gICAgICAnc3RhdGUnOiBbJycsIFZhbGlkYXRvcnMuY29tcG9zZShbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoNTApXSldLFxyXG4gICAgICAncHJpbWFyeUxhbmd1YWdlJzogWycnLCBWYWxpZGF0b3JzLm1heExlbmd0aCg1MCldLFxyXG4gICAgICAnc2Vjb25kYXJ5TGFuZ3VhZ2UnOiBbJycsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDUwKV0sXHJcbiAgICAgICdhYm9yaWdpbmFsT3JUb3JyZXMnOiBmYi5ncm91cCh7XHJcbiAgICAgICAgJ2Fib3JpZ2luYWwnOiBbJyddLFxyXG4gICAgICAgICd0b3JyZXMnOiBbJyddLFxyXG4gICAgICAgICduZWl0aGVyQWJvcmlnaW5hbE5vclRvcnJlcyc6IFsnJ10sXHJcbiAgICAgIH0sIHsgdmFsaWRhdG9yOiBBYm9yaWdpbmFsT3JUb3JyZXNWYWxpZGF0b3IudmFsaWRhdGUoJ2Fib3JpZ2luYWwnLCAndG9ycmVzJywgJ25laXRoZXJBYm9yaWdpbmFsTm9yVG9ycmVzJykgfSlcclxuICAgIH0pO1xyXG5cclxuICAgIHRoaXMuZmlyc3RuYW1lID0gdGhpcy5mb3JtLmNvbnRyb2xzWydmaXJzdG5hbWUnXTtcclxuICAgIHRoaXMubGFzdG5hbWUgPSB0aGlzLmZvcm0uY29udHJvbHNbJ2xhc3RuYW1lJ107XHJcbiAgICB0aGlzLmNybiA9IHRoaXMuZm9ybS5jb250cm9sc1snY3JuJ107XHJcbiAgICB0aGlzLmRhdGVPZkJpcnRoID0gdGhpcy5mb3JtLmNvbnRyb2xzWydkYXRlT2ZCaXJ0aCddO1xyXG4gICAgdGhpcy5nZW5kZXIgPSB0aGlzLmZvcm0uY29udHJvbHNbJ2dlbmRlciddO1xyXG4gICAgdGhpcy5jb3VudHJ5T2ZCaXJ0aCA9IHRoaXMuZm9ybS5jb250cm9sc1snY291bnRyeU9mQmlydGgnXTtcclxuICAgIHRoaXMuc3RyZWV0QWRkcmVzcyA9IHRoaXMuZm9ybS5jb250cm9sc1snc3RyZWV0QWRkcmVzcyddO1xyXG4gICAgdGhpcy5zdWJ1cmIgPSB0aGlzLmZvcm0uY29udHJvbHNbJ3N1YnVyYiddO1xyXG4gICAgdGhpcy5wb3N0Y29kZSA9IHRoaXMuZm9ybS5jb250cm9sc1sncG9zdGNvZGUnXTtcclxuICAgIHRoaXMuc3RhdGUgPSB0aGlzLmZvcm0uY29udHJvbHNbJ3N0YXRlJ107XHJcbiAgICB0aGlzLmFib3JpZ2luYWxPclRvcnJlcyA9IDxGb3JtR3JvdXA+dGhpcy5mb3JtLmNvbnRyb2xzWydhYm9yaWdpbmFsT3JUb3JyZXMnXTtcclxuICAgIHRoaXMuYWJvcmlnaW5hbCA9IHRoaXMuYWJvcmlnaW5hbE9yVG9ycmVzLmNvbnRyb2xzWydhYm9yaWdpbmFsJ107XHJcbiAgICB0aGlzLnRvcnJlcyA9IHRoaXMuYWJvcmlnaW5hbE9yVG9ycmVzLmNvbnRyb2xzWyd0b3JyZXMnXTtcclxuICAgIHRoaXMubmVpdGhlckFib3JpZ2luYWxOb3JUb3JyZXMgPSB0aGlzLmFib3JpZ2luYWxPclRvcnJlcy5jb250cm9sc1snbmVpdGhlckFib3JpZ2luYWxOb3JUb3JyZXMnXTtcclxuICAgIHRoaXMucHJpbWFyeUxhbmd1YWdlID0gdGhpcy5mb3JtLmNvbnRyb2xzWydwcmltYXJ5TGFuZ3VhZ2UnXTtcclxuICAgIHRoaXMuc2Vjb25kYXJ5TGFuZ3VhZ2UgPSB0aGlzLmZvcm0uY29udHJvbHNbJ3NlY29uZGFyeUxhbmd1YWdlJ107XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuXHJcbiAgICB0aGlzLl9yb3V0ZVxyXG4gICAgICAucGFyYW1zXHJcbiAgICAgIC5mbGF0TWFwKChwYXJhbXM6IFBhcmFtcykgPT4gT2JzZXJ2YWJsZS5vZihwYXJhbXNbJ2tpZElkJ10pKVxyXG4gICAgICAuZmxhdE1hcChraWRJZCA9PlxyXG4gICAgICAgIChraWRJZCA9PT0gbnVsbCB8fCBraWRJZCA9PT0gdW5kZWZpbmVkKSA/XHJcbiAgICAgICAgICBPYnNlcnZhYmxlLm9mKG5ldyBLaWREdG8oKSkgOlxyXG4gICAgICAgICAgdGhpcy5fZW5yb2xtZW50U2VydmljZS5nZXRLaWQkKGtpZElkKSlcclxuICAgICAgLnN1YnNjcmliZShyZXMgPT4ge1xyXG4gICAgICAgIHRoaXMua2lkRHRvID0gcmVzO1xyXG4gICAgICAgIHRoaXMubmVpdGhlckFib3JpZ2luYWxOb3JUb3JyZXMuc2V0VmFsdWUodGhpcy5raWREdG8uYWJvcmlnaW5hbCA9PT0gZmFsc2UgJiYgdGhpcy5raWREdG8udG9ycmVzID09PSBmYWxzZSk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgc2F2ZUNoYW5nZXMoKTogdm9pZCB7XHJcbiAgICBsZXQgc2F2aW5nVG9hc3QgPSB0aGlzLl90b2FzdGVyU2VydmljZS5wb3AobmV3IFNwaW5uaW5nVG9hc3QoKSk7XHJcbiAgICBsZXQgdXBkYXRlS2lkOiBDcmVhdGVVcGRhdGVLaWREdG8gPSBuZXcgQ3JlYXRlVXBkYXRlS2lkRHRvKCk7XHJcbiAgICB1cGRhdGVLaWQuaW5pdCh0aGlzLmtpZER0by50b0pTT04oKSk7XHJcbiAgICB0aGlzLl9lbnJvbG1lbnRTZXJ2aWNlXHJcbiAgICAgIC5jcmVhdGVVcGRhdGVLaWQkKHVwZGF0ZUtpZClcclxuICAgICAgLnN1YnNjcmliZShyZXMgPT4ge1xyXG4gICAgICAgIHRoaXMua2lkRHRvID0gcmVzO1xyXG4gICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLmNsZWFyKHNhdmluZ1RvYXN0LnRvYXN0SWQsIHNhdmluZ1RvYXN0LnRvYXN0Q29udGFpbmVySWQpO1xyXG4gICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLnBvcEFzeW5jKCdzdWNjZXNzJywgJ1NhdmVkJywgJycpO1xyXG5cclxuICAgICAgICBsZXQgdXJsOiBzdHJpbmcgPSAodXBkYXRlS2lkLmlkID09PSB1bmRlZmluZWQpID8gJy4uL2Vucm9sbWVudC1mb3JtJyA6ICcuLi8uLi9lbnJvbG1lbnQtZm9ybSc7XHJcbiAgICAgICAgdGhpcy5fcm91dGVyLm5hdmlnYXRlKFt1cmxdLCB7IHJlbGF0aXZlVG86IHRoaXMuX3JvdXRlIH0pO1xyXG4gICAgICB9LFxyXG4gICAgICAoZXJyb3IpID0+IHtcclxuICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcihzYXZpbmdUb2FzdC50b2FzdElkLCBzYXZpbmdUb2FzdC50b2FzdENvbnRhaW5lcklkKTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBzYXZlQ2hhbmdlc0FuZENvbnRpbnVlKCk6IHZvaWQge1xyXG4gICAgbGV0IHNhdmluZ1RvYXN0ID0gdGhpcy5fdG9hc3RlclNlcnZpY2UucG9wKG5ldyBTcGlubmluZ1RvYXN0KCkpO1xyXG4gICAgbGV0IHVwZGF0ZUtpZDogQ3JlYXRlVXBkYXRlS2lkRHRvID0gbmV3IENyZWF0ZVVwZGF0ZUtpZER0bygpO1xyXG4gICAgdXBkYXRlS2lkLmluaXQodGhpcy5raWREdG8udG9KU09OKCkpO1xyXG4gICAgdGhpcy5fZW5yb2xtZW50U2VydmljZVxyXG4gICAgICAuY3JlYXRlVXBkYXRlS2lkJCh1cGRhdGVLaWQpXHJcbiAgICAgIC5zdWJzY3JpYmUocmVzID0+IHtcclxuICAgICAgICB0aGlzLmtpZER0byA9IHJlcztcclxuICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcihzYXZpbmdUb2FzdC50b2FzdElkLCBzYXZpbmdUb2FzdC50b2FzdENvbnRhaW5lcklkKTtcclxuICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5wb3BBc3luYygnc3VjY2VzcycsICdTYXZlZCcsICcnKTtcclxuXHJcbiAgICAgICAgbGV0IHVybDogc3RyaW5nID0gKHVwZGF0ZUtpZC5pZCA9PT0gdW5kZWZpbmVkKSA/ICcuLi9raWQtZGVzaXJlZC1kYXlzJyA6ICcuLi8uLi9raWQtZGVzaXJlZC1kYXlzJztcclxuICAgICAgICB0aGlzLl9yb3V0ZXIubmF2aWdhdGUoW3VybCwgdGhpcy5raWREdG8uaWRdLCB7IHJlbGF0aXZlVG86IHRoaXMuX3JvdXRlIH0pO1xyXG4gICAgICB9LFxyXG4gICAgICAoZXJyb3IpID0+IHtcclxuICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcihzYXZpbmdUb2FzdC50b2FzdElkLCBzYXZpbmdUb2FzdC50b2FzdENvbnRhaW5lcklkKTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBjYW5jZWxFZGl0KCkge1xyXG4gICAgbGV0IHVybDogc3RyaW5nID0gdGhpcy5jYW5jZWxMaW5rKCk7XHJcbiAgICB0aGlzLl9yb3V0ZXIubmF2aWdhdGUoW3VybF0sIHsgcmVsYXRpdmVUbzogdGhpcy5fcm91dGUgfSk7XHJcbiAgfVxyXG5cclxuICBjYW5jZWxMaW5rKCk6IHN0cmluZyB7XHJcbiAgICBpZiAodGhpcy5raWREdG8gPT09IG51bGwpXHJcbiAgICAgIHJldHVybiAnJztcclxuICAgIHJldHVybiAodGhpcy5raWREdG8uaWQgPT09IHVuZGVmaW5lZCkgPyAnLi4vZW5yb2xtZW50LWZvcm0nIDogJy4uLy4uL2Vucm9sbWVudC1mb3JtJztcclxuICB9XHJcblxyXG4gIHNlbGVjdEFib3JpZ2luYWxPclRvcnJlcyhjaGVja2VkOiBib29sZWFuKTogdm9pZCB7XHJcbiAgICBpZiAoY2hlY2tlZCkge1xyXG4gICAgICB0aGlzLm5laXRoZXJBYm9yaWdpbmFsTm9yVG9ycmVzLnNldFZhbHVlKGZhbHNlKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHNlbGVjdE5laXRoZXJBYm9yaWdpbmFsT3JUb3JyZXMoY2hlY2tlZDogYm9vbGVhbik6IHZvaWQge1xyXG4gICAgaWYgKGNoZWNrZWQpIHtcclxuICAgICAgdGhpcy5hYm9yaWdpbmFsLnNldFZhbHVlKGZhbHNlKTtcclxuICAgICAgdGhpcy50b3JyZXMuc2V0VmFsdWUoZmFsc2UpO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0=
