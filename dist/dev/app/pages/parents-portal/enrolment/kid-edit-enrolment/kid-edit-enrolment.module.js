"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../../shared/index');
var index_2 = require('./kid-edit-details/index');
var index_3 = require('./kid-desired-days/index');
var index_4 = require('./kid-health-info/index');
var index_5 = require('./kid-custody-info/index');
var EnrolmentKidEditModule = (function () {
    function EnrolmentKidEditModule() {
    }
    EnrolmentKidEditModule = __decorate([
        core_1.NgModule({
            imports: [index_1.SharedModule],
            declarations: [
                index_2.EnrolmentKidEditComponent,
                index_3.KidDesiredDaysComponent,
                index_3.DisplayDisabledCheckboxComponent,
                index_3.DisplayCareTypeComponent,
                index_3.EditDateComponent,
                index_4.KidHealthInfoComponent,
                index_5.KidCustodyInfoComponent],
            entryComponents: [index_3.DisplayDisabledCheckboxComponent, index_3.DisplayCareTypeComponent, index_3.EditDateComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], EnrolmentKidEditModule);
    return EnrolmentKidEditModule;
}());
exports.EnrolmentKidEditModule = EnrolmentKidEditModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQva2lkLWVkaXQtZW5yb2xtZW50L2tpZC1lZGl0LWVucm9sbWVudC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUF5QixlQUFlLENBQUMsQ0FBQTtBQUN6QyxzQkFBNkIsMEJBQTBCLENBQUMsQ0FBQTtBQUV4RCxzQkFBMEMsMEJBQTBCLENBQUMsQ0FBQTtBQUNyRSxzQkFHTywwQkFBMEIsQ0FBQyxDQUFBO0FBQ2xDLHNCQUF1Qyx5QkFBeUIsQ0FBQyxDQUFBO0FBQ2pFLHNCQUF3QywwQkFBMEIsQ0FBQyxDQUFBO0FBZW5FO0lBQUE7SUFBc0MsQ0FBQztJQWJ2QztRQUFDLGVBQVEsQ0FBQztZQUNSLE9BQU8sRUFBRSxDQUFDLG9CQUFZLENBQUM7WUFDdkIsWUFBWSxFQUFFO2dCQUNaLGlDQUF5QjtnQkFDekIsK0JBQXVCO2dCQUN2Qix3Q0FBZ0M7Z0JBQ2hDLGdDQUF3QjtnQkFDeEIseUJBQWlCO2dCQUNqQiw4QkFBc0I7Z0JBQ3RCLCtCQUF1QixDQUFDO1lBQzFCLGVBQWUsRUFBRSxDQUFDLHdDQUFnQyxFQUFFLGdDQUF3QixFQUFFLHlCQUFpQixDQUFDO1NBQ2pHLENBQUM7OzhCQUFBO0lBRW9DLDZCQUFDO0FBQUQsQ0FBdEMsQUFBdUMsSUFBQTtBQUExQiw4QkFBc0IseUJBQUksQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvcGFyZW50cy1wb3J0YWwvZW5yb2xtZW50L2tpZC1lZGl0LWVucm9sbWVudC9raWQtZWRpdC1lbnJvbG1lbnQubW9kdWxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU2hhcmVkTW9kdWxlIH0gZnJvbSAnLi4vLi4vLi4vLi4vc2hhcmVkL2luZGV4JztcclxuXHJcbmltcG9ydCB7IEVucm9sbWVudEtpZEVkaXRDb21wb25lbnQgfSBmcm9tICcuL2tpZC1lZGl0LWRldGFpbHMvaW5kZXgnO1xyXG5pbXBvcnQge1xyXG4gIEtpZERlc2lyZWREYXlzQ29tcG9uZW50LCBEaXNwbGF5RGlzYWJsZWRDaGVja2JveENvbXBvbmVudCxcclxuICBEaXNwbGF5Q2FyZVR5cGVDb21wb25lbnQsIEVkaXREYXRlQ29tcG9uZW50XHJcbn0gZnJvbSAnLi9raWQtZGVzaXJlZC1kYXlzL2luZGV4JztcclxuaW1wb3J0IHsgS2lkSGVhbHRoSW5mb0NvbXBvbmVudCB9IGZyb20gJy4va2lkLWhlYWx0aC1pbmZvL2luZGV4JztcclxuaW1wb3J0IHsgS2lkQ3VzdG9keUluZm9Db21wb25lbnQgfSBmcm9tICcuL2tpZC1jdXN0b2R5LWluZm8vaW5kZXgnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBpbXBvcnRzOiBbU2hhcmVkTW9kdWxlXSxcclxuICBkZWNsYXJhdGlvbnM6IFtcclxuICAgIEVucm9sbWVudEtpZEVkaXRDb21wb25lbnQsXHJcbiAgICBLaWREZXNpcmVkRGF5c0NvbXBvbmVudCxcclxuICAgIERpc3BsYXlEaXNhYmxlZENoZWNrYm94Q29tcG9uZW50LFxyXG4gICAgRGlzcGxheUNhcmVUeXBlQ29tcG9uZW50LFxyXG4gICAgRWRpdERhdGVDb21wb25lbnQsXHJcbiAgICBLaWRIZWFsdGhJbmZvQ29tcG9uZW50LFxyXG4gICAgS2lkQ3VzdG9keUluZm9Db21wb25lbnRdLFxyXG4gIGVudHJ5Q29tcG9uZW50czogW0Rpc3BsYXlEaXNhYmxlZENoZWNrYm94Q29tcG9uZW50LCBEaXNwbGF5Q2FyZVR5cGVDb21wb25lbnQsIEVkaXREYXRlQ29tcG9uZW50XVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEVucm9sbWVudEtpZEVkaXRNb2R1bGUgeyB9XHJcbiJdfQ==
