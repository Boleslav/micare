"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var router_1 = require('@angular/router');
var rxjs_1 = require('rxjs');
var ng2_bootstrap_1 = require('ng2-bootstrap');
var _ = require('lodash');
var index_1 = require('../../index');
var index_2 = require('../../../index');
var index_3 = require('../../../../../api/index');
var angular2_toaster_1 = require('angular2-toaster');
var index_4 = require('../../../../../shared/index');
var KidHealthInfoComponent = (function () {
    function KidHealthInfoComponent(_router, _route, _enrolmentService, _centreService, _toasterService, fb) {
        this._router = _router;
        this._route = _route;
        this._enrolmentService = _enrolmentService;
        this._centreService = _centreService;
        this._toasterService = _toasterService;
        this.centreName = null;
        this.pendingFunction = null;
        this.kidDto = null;
        this.kidDtoModified = null;
        this.initForm(fb);
    }
    KidHealthInfoComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.centreName = this._centreService.SelectedCentreQualifiedName;
        this._route
            .params
            .flatMap(function (params) {
            _this.kidId = params['kidId'];
            return rxjs_1.Observable.of(_this.kidId);
        })
            .flatMap(function (kidId) { return _this._enrolmentService.getKidHealthInformation$(_this.kidId); })
            .subscribe(function (res) {
            _this.kidDto = res;
            _this.kidDtoModified = _.cloneDeep(_this.kidDto);
        });
    };
    KidHealthInfoComponent.prototype.saveChangesAndGoBack = function () {
        this.createOrUpdateHealthInfo(['../../enrolment-form']);
    };
    KidHealthInfoComponent.prototype.saveChangesAndContinue = function () {
        this.createOrUpdateHealthInfo(['../../kid-custody-info', this.kidId]);
    };
    KidHealthInfoComponent.prototype.createOrUpdateHealthInfo = function (redirectUrl) {
        var _this = this;
        if (this.kidDto.kidId === '00000000-0000-0000-0000-000000000000') {
            this.saveAndRedirect(redirectUrl);
        }
        else if (this.wasNotModified(this.kidDto, this.kidDtoModified)) {
            this.saveAndRedirect(redirectUrl);
        }
        else {
            this.pendingFunction = function () { return _this.saveAndRedirect(redirectUrl); };
            this.staticModal.show();
        }
    };
    KidHealthInfoComponent.prototype.saveAndRedirect = function (redirectUrl) {
        var _this = this;
        var savingToast = this._toasterService.pop(new index_4.SpinningToast());
        var updateHealthInfo = new index_3.CreateUpdateKidHealthInformationDto();
        updateHealthInfo.init(this.kidDto.toJSON());
        updateHealthInfo.kidId = this.kidId;
        this._enrolmentService
            .addUpdateKidHealthInformationDto$(updateHealthInfo)
            .subscribe(function (res) {
            _this.kidDto = res;
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this._toasterService.popAsync('success', 'Saved', '');
            _this._router.navigate(redirectUrl, { relativeTo: _this._route });
        }, function (error) {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
        });
    };
    KidHealthInfoComponent.prototype.cancelEdit = function () {
        this._router.navigate(['../../enrolment-form'], { relativeTo: this._route });
    };
    KidHealthInfoComponent.prototype.onHide = function (event) {
        this.pendingFunction();
    };
    KidHealthInfoComponent.prototype.initForm = function (fb) {
        this.form = fb.group({
            'medicalServicesName': ['', forms_1.Validators.maxLength(100)],
            'medicalServicesContactNumber': ['', forms_1.Validators.maxLength(50)],
            'medicareNumber': ['', forms_1.Validators.maxLength(50)],
            'medicareExpiryDate': [''],
            'healthcareCardNumber': ['', forms_1.Validators.maxLength(50)],
            'healthcareCardExpiryDate': [''],
            'privateHealthInsurer': ['', forms_1.Validators.maxLength(100)],
            'privateHealthMembershipNumber': ['', forms_1.Validators.maxLength(100)],
            'hasAllergies': [''],
            'hasAnaphylaxis': [''],
            'hasInjectionDevice': [''],
            'hasAsthma': [''],
            'isImmunised': [''],
            'hasSpecialNeeds': [''],
            'hasSupportServices': [''],
            'dietaryNeeds': ['', forms_1.Validators.maxLength(2000)],
            'dentalNeeds': ['', forms_1.Validators.maxLength(2000)],
            'medicalCondition1': ['', forms_1.Validators.maxLength(100)],
            'medicalCondition2': ['', forms_1.Validators.maxLength(100)],
            'medicalCondition3': ['', forms_1.Validators.maxLength(100)],
            'medicalCondition4': ['', forms_1.Validators.maxLength(100)],
            'consentToAccuracy': ['', forms_1.Validators.required],
            'consentToCollection': ['', forms_1.Validators.required],
            'consentToTreatment': ['', forms_1.Validators.required],
            'contentToCost': ['', forms_1.Validators.required]
        });
        this.medicalServicesName = this.form.controls['medicalServicesName'];
        this.medicalServicesContactNumber = this.form.controls['medicalServicesContactNumber'];
        this.medicareNumber = this.form.controls['medicareNumber'];
        this.medicareExpiryDate = this.form.controls['medicareExpiryDate'];
        this.healthcareCardNumber = this.form.controls['healthcareCardNumber'];
        this.healthcareCardExpiryDate = this.form.controls['healthcareCardExpiryDate'];
        this.privateHealthInsurer = this.form.controls['privateHealthInsurer'];
        this.privateHealthMembershipNumber = this.form.controls['privateHealthMembershipNumber'];
        this.hasAllergies = this.form.controls['hasAllergies'];
        this.hasAnaphylaxis = this.form.controls['hasAnaphylaxis'];
        this.hasInjectionDevice = this.form.controls['hasInjectionDevice'];
        this.hasAsthma = this.form.controls['hasAsthma'];
        this.isImmunised = this.form.controls['isImmunised'];
        this.hasSpecialNeeds = this.form.controls['hasSpecialNeeds'];
        this.hasSupportServices = this.form.controls['hasSupportServices'];
        this.dietaryNeeds = this.form.controls['dietaryNeeds'];
        this.dentalNeeds = this.form.controls['dentalNeeds'];
        this.medicalCondition1 = this.form.controls['medicalCondition1'];
        this.medicalCondition2 = this.form.controls['medicalCondition2'];
        this.medicalCondition3 = this.form.controls['medicalCondition3'];
        this.medicalCondition4 = this.form.controls['medicalCondition4'];
        this.consentToAccuracy = this.form.controls['consentToAccuracy'];
        this.consentToCollection = this.form.controls['consentToCollection'];
        this.consentToTreatment = this.form.controls['consentToTreatment'];
        this.contentToCost = this.form.controls['contentToCost'];
    };
    KidHealthInfoComponent.prototype.wasNotModified = function (modifiedKid, originalKid) {
        return _.isEqual(modifiedKid, originalKid);
    };
    __decorate([
        core_1.ViewChild('staticModal'), 
        __metadata('design:type', ng2_bootstrap_1.ModalDirective)
    ], KidHealthInfoComponent.prototype, "staticModal", void 0);
    KidHealthInfoComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            encapsulation: core_1.ViewEncapsulation.None,
            selector: 'kid-health-info-cmp',
            templateUrl: 'kid-health-info.component.html',
            styleUrls: ['kid-health-info.component.css']
        }), 
        __metadata('design:paramtypes', [router_1.Router, router_1.ActivatedRoute, index_1.EnrolmentService, index_2.CentreService, angular2_toaster_1.ToasterService, forms_1.FormBuilder])
    ], KidHealthInfoComponent);
    return KidHealthInfoComponent;
}());
exports.KidHealthInfoComponent = KidHealthInfoComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQva2lkLWVkaXQtZW5yb2xtZW50L2tpZC1oZWFsdGgtaW5mby9raWQtaGVhbHRoLWluZm8uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBaUUsZUFBZSxDQUFDLENBQUE7QUFDakYsc0JBQW9FLGdCQUFnQixDQUFDLENBQUE7QUFDckYsdUJBQStDLGlCQUFpQixDQUFDLENBQUE7QUFDakUscUJBQTJCLE1BQU0sQ0FBQyxDQUFBO0FBQ2xDLDhCQUErQixlQUFlLENBQUMsQ0FBQTtBQUMvQyxJQUFZLENBQUMsV0FBTSxRQUFRLENBQUMsQ0FBQTtBQUU1QixzQkFBaUMsYUFBYSxDQUFDLENBQUE7QUFDL0Msc0JBQThCLGdCQUFnQixDQUFDLENBQUE7QUFDL0Msc0JBQTZFLDBCQUEwQixDQUFDLENBQUE7QUFDeEcsaUNBQStCLGtCQUFrQixDQUFDLENBQUE7QUFDbEQsc0JBQThCLDZCQUE2QixDQUFDLENBQUE7QUFVNUQ7SUFtQ0UsZ0NBQW9CLE9BQWUsRUFDekIsTUFBc0IsRUFDdEIsaUJBQW1DLEVBQ25DLGNBQTZCLEVBQzdCLGVBQStCLEVBQ3ZDLEVBQWU7UUFMRyxZQUFPLEdBQVAsT0FBTyxDQUFRO1FBQ3pCLFdBQU0sR0FBTixNQUFNLENBQWdCO1FBQ3RCLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBa0I7UUFDbkMsbUJBQWMsR0FBZCxjQUFjLENBQWU7UUFDN0Isb0JBQWUsR0FBZixlQUFlLENBQWdCO1FBcENqQyxlQUFVLEdBQVcsSUFBSSxDQUFDO1FBQzFCLG9CQUFlLEdBQVEsSUFBSSxDQUFDO1FBRTVCLFdBQU0sR0FBNEIsSUFBSSxDQUFDO1FBQ3ZDLG1CQUFjLEdBQTRCLElBQUksQ0FBQztRQWtDckQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUNwQixDQUFDO0lBRUQseUNBQVEsR0FBUjtRQUFBLGlCQWNDO1FBWkMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLDJCQUEyQixDQUFDO1FBQ2xFLElBQUksQ0FBQyxNQUFNO2FBQ1IsTUFBTTthQUNOLE9BQU8sQ0FBQyxVQUFDLE1BQWM7WUFDdEIsS0FBSSxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDN0IsTUFBTSxDQUFDLGlCQUFVLENBQUMsRUFBRSxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNuQyxDQUFDLENBQUM7YUFDRCxPQUFPLENBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxLQUFJLENBQUMsaUJBQWlCLENBQUMsd0JBQXdCLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxFQUEzRCxDQUEyRCxDQUFDO2FBQzdFLFNBQVMsQ0FBQyxVQUFBLEdBQUc7WUFDWixLQUFJLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQztZQUNsQixLQUFJLENBQUMsY0FBYyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ2pELENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHFEQUFvQixHQUFwQjtRQUNFLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQztJQUMxRCxDQUFDO0lBRUQsdURBQXNCLEdBQXRCO1FBQ0UsSUFBSSxDQUFDLHdCQUF3QixDQUFDLENBQUMsd0JBQXdCLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFDeEUsQ0FBQztJQUVELHlEQUF3QixHQUF4QixVQUF5QixXQUFxQjtRQUE5QyxpQkFVQztRQVJDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxLQUFLLHNDQUFzQyxDQUFDLENBQUMsQ0FBQztZQUNqRSxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3BDLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDakUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNwQyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFJLENBQUMsZUFBZSxHQUFHLGNBQU0sT0FBQSxLQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxFQUFqQyxDQUFpQyxDQUFDO1lBQy9ELElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDMUIsQ0FBQztJQUNILENBQUM7SUFFRCxnREFBZSxHQUFmLFVBQWdCLFdBQXFCO1FBQXJDLGlCQWdCQztRQWZDLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUkscUJBQWEsRUFBRSxDQUFDLENBQUM7UUFDaEUsSUFBSSxnQkFBZ0IsR0FBRyxJQUFJLDJDQUFtQyxFQUFFLENBQUM7UUFDakUsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQztRQUM1QyxnQkFBZ0IsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUNwQyxJQUFJLENBQUMsaUJBQWlCO2FBQ25CLGlDQUFpQyxDQUFDLGdCQUFnQixDQUFDO2FBQ25ELFNBQVMsQ0FBQyxVQUFBLEdBQUc7WUFDWixLQUFJLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQztZQUNsQixLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1lBQzlFLEtBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLFNBQVMsRUFBRSxPQUFPLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDdEQsS0FBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLEVBQUUsVUFBVSxFQUFFLEtBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO1FBQ2xFLENBQUMsRUFDRCxVQUFDLEtBQUs7WUFDSixLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ2hGLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDJDQUFVLEdBQVY7UUFDRSxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLHNCQUFzQixDQUFDLEVBQUUsRUFBRSxVQUFVLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7SUFDL0UsQ0FBQztJQUVELHVDQUFNLEdBQU4sVUFBTyxLQUFVO1FBQ2YsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO0lBQ3pCLENBQUM7SUFFTyx5Q0FBUSxHQUFoQixVQUFpQixFQUFlO1FBQzlCLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQztZQUNuQixxQkFBcUIsRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN0RCw4QkFBOEIsRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUM5RCxnQkFBZ0IsRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUNoRCxvQkFBb0IsRUFBRSxDQUFDLEVBQUUsQ0FBQztZQUMxQixzQkFBc0IsRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUN0RCwwQkFBMEIsRUFBRSxDQUFDLEVBQUUsQ0FBQztZQUNoQyxzQkFBc0IsRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN2RCwrQkFBK0IsRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNoRSxjQUFjLEVBQUUsQ0FBQyxFQUFFLENBQUM7WUFDcEIsZ0JBQWdCLEVBQUUsQ0FBQyxFQUFFLENBQUM7WUFDdEIsb0JBQW9CLEVBQUUsQ0FBQyxFQUFFLENBQUM7WUFDMUIsV0FBVyxFQUFFLENBQUMsRUFBRSxDQUFDO1lBQ2pCLGFBQWEsRUFBRSxDQUFDLEVBQUUsQ0FBQztZQUNuQixpQkFBaUIsRUFBRSxDQUFDLEVBQUUsQ0FBQztZQUN2QixvQkFBb0IsRUFBRSxDQUFDLEVBQUUsQ0FBQztZQUMxQixjQUFjLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDaEQsYUFBYSxFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQy9DLG1CQUFtQixFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3BELG1CQUFtQixFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3BELG1CQUFtQixFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3BELG1CQUFtQixFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3BELG1CQUFtQixFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsUUFBUSxDQUFDO1lBQzlDLHFCQUFxQixFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsUUFBUSxDQUFDO1lBQ2hELG9CQUFvQixFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsUUFBUSxDQUFDO1lBQy9DLGVBQWUsRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLFFBQVEsQ0FBQztTQUMzQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUNyRSxJQUFJLENBQUMsNEJBQTRCLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsOEJBQThCLENBQUMsQ0FBQztRQUN2RixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDM0QsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLG9CQUFvQixDQUFDLENBQUM7UUFDbkUsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLHNCQUFzQixDQUFDLENBQUM7UUFDdkUsSUFBSSxDQUFDLHdCQUF3QixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLDBCQUEwQixDQUFDLENBQUM7UUFDL0UsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLHNCQUFzQixDQUFDLENBQUM7UUFDdkUsSUFBSSxDQUFDLDZCQUE2QixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLCtCQUErQixDQUFDLENBQUM7UUFDekYsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUN2RCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDM0QsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLG9CQUFvQixDQUFDLENBQUM7UUFDbkUsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNqRCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ3JELElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUM3RCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUNuRSxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ3ZELElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDckQsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDakUsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDakUsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDakUsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDakUsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDakUsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLHFCQUFxQixDQUFDLENBQUM7UUFDckUsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLG9CQUFvQixDQUFDLENBQUM7UUFDbkUsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsQ0FBQztJQUMzRCxDQUFDO0lBRU8sK0NBQWMsR0FBdEIsVUFBdUIsV0FBb0MsRUFBRSxXQUFvQztRQUMvRixNQUFNLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsV0FBVyxDQUFDLENBQUM7SUFDN0MsQ0FBQztJQWxLRDtRQUFDLGdCQUFTLENBQUMsYUFBYSxDQUFDOzsrREFBQTtJQVYzQjtRQUFDLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsYUFBYSxFQUFFLHdCQUFpQixDQUFDLElBQUk7WUFDckMsUUFBUSxFQUFFLHFCQUFxQjtZQUMvQixXQUFXLEVBQUUsZ0NBQWdDO1lBQzdDLFNBQVMsRUFBRSxDQUFDLCtCQUErQixDQUFDO1NBQzdDLENBQUM7OzhCQUFBO0lBdUtGLDZCQUFDO0FBQUQsQ0FyS0EsQUFxS0MsSUFBQTtBQXJLWSw4QkFBc0IseUJBcUtsQyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQva2lkLWVkaXQtZW5yb2xtZW50L2tpZC1oZWFsdGgtaW5mby9raWQtaGVhbHRoLWluZm8uY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIFZpZXdDaGlsZCwgVmlld0VuY2Fwc3VsYXRpb24sIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1Hcm91cCwgQWJzdHJhY3RDb250cm9sLCBGb3JtQnVpbGRlciwgVmFsaWRhdG9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgUm91dGVyLCBBY3RpdmF0ZWRSb3V0ZSwgUGFyYW1zIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBNb2RhbERpcmVjdGl2ZSB9IGZyb20gJ25nMi1ib290c3RyYXAnO1xyXG5pbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCc7XHJcblxyXG5pbXBvcnQgeyBFbnJvbG1lbnRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vaW5kZXgnO1xyXG5pbXBvcnQgeyBDZW50cmVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vaW5kZXgnO1xyXG5pbXBvcnQgeyBLaWRIZWFsdGhJbmZvcm1hdGlvbkR0bywgQ3JlYXRlVXBkYXRlS2lkSGVhbHRoSW5mb3JtYXRpb25EdG8gfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9hcGkvaW5kZXgnO1xyXG5pbXBvcnQgeyBUb2FzdGVyU2VydmljZSB9IGZyb20gJ2FuZ3VsYXIyLXRvYXN0ZXInO1xyXG5pbXBvcnQgeyBTcGlubmluZ1RvYXN0IH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vc2hhcmVkL2luZGV4JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcclxuICBzZWxlY3RvcjogJ2tpZC1oZWFsdGgtaW5mby1jbXAnLFxyXG4gIHRlbXBsYXRlVXJsOiAna2lkLWhlYWx0aC1pbmZvLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsna2lkLWhlYWx0aC1pbmZvLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEtpZEhlYWx0aEluZm9Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBAVmlld0NoaWxkKCdzdGF0aWNNb2RhbCcpIHB1YmxpYyBzdGF0aWNNb2RhbDogTW9kYWxEaXJlY3RpdmU7XHJcbiAgcHJpdmF0ZSBjZW50cmVOYW1lOiBzdHJpbmcgPSBudWxsO1xyXG4gIHByaXZhdGUgcGVuZGluZ0Z1bmN0aW9uOiBhbnkgPSBudWxsO1xyXG4gIHByaXZhdGUgZm9ybTogRm9ybUdyb3VwO1xyXG4gIHByaXZhdGUga2lkRHRvOiBLaWRIZWFsdGhJbmZvcm1hdGlvbkR0byA9IG51bGw7XHJcbiAgcHJpdmF0ZSBraWREdG9Nb2RpZmllZDogS2lkSGVhbHRoSW5mb3JtYXRpb25EdG8gPSBudWxsO1xyXG4gIHByaXZhdGUga2lkSWQ6IHN0cmluZztcclxuICBwcml2YXRlIG1lZGljYWxTZXJ2aWNlc05hbWU6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIG1lZGljYWxTZXJ2aWNlc0NvbnRhY3ROdW1iZXI6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIG1lZGljYXJlTnVtYmVyOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBtZWRpY2FyZUV4cGlyeURhdGU6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIGhlYWx0aGNhcmVDYXJkTnVtYmVyOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBoZWFsdGhjYXJlQ2FyZEV4cGlyeURhdGU6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIHByaXZhdGVIZWFsdGhJbnN1cmVyOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBwcml2YXRlSGVhbHRoTWVtYmVyc2hpcE51bWJlcjogQWJzdHJhY3RDb250cm9sO1xyXG4gIHByaXZhdGUgaGFzQWxsZXJnaWVzOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBoYXNBbmFwaHlsYXhpczogQWJzdHJhY3RDb250cm9sO1xyXG4gIHByaXZhdGUgaGFzSW5qZWN0aW9uRGV2aWNlOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBoYXNBc3RobWE6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIGlzSW1tdW5pc2VkOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBoYXNTcGVjaWFsTmVlZHM6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIGhhc1N1cHBvcnRTZXJ2aWNlczogQWJzdHJhY3RDb250cm9sO1xyXG4gIHByaXZhdGUgZGlldGFyeU5lZWRzOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBkZW50YWxOZWVkczogQWJzdHJhY3RDb250cm9sO1xyXG4gIHByaXZhdGUgbWVkaWNhbENvbmRpdGlvbjE6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIG1lZGljYWxDb25kaXRpb24yOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBtZWRpY2FsQ29uZGl0aW9uMzogQWJzdHJhY3RDb250cm9sO1xyXG4gIHByaXZhdGUgbWVkaWNhbENvbmRpdGlvbjQ6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIGNvbnNlbnRUb0FjY3VyYWN5OiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBjb25zZW50VG9Db2xsZWN0aW9uOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBjb25zZW50VG9UcmVhdG1lbnQ6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIGNvbnRlbnRUb0Nvc3Q6IEFic3RyYWN0Q29udHJvbDtcclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBfcm91dGVyOiBSb3V0ZXIsXHJcbiAgICBwcml2YXRlIF9yb3V0ZTogQWN0aXZhdGVkUm91dGUsXHJcbiAgICBwcml2YXRlIF9lbnJvbG1lbnRTZXJ2aWNlOiBFbnJvbG1lbnRTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBfY2VudHJlU2VydmljZTogQ2VudHJlU2VydmljZSxcclxuICAgIHByaXZhdGUgX3RvYXN0ZXJTZXJ2aWNlOiBUb2FzdGVyU2VydmljZSxcclxuICAgIGZiOiBGb3JtQnVpbGRlcikge1xyXG4gICAgdGhpcy5pbml0Rm9ybShmYik7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuXHJcbiAgICB0aGlzLmNlbnRyZU5hbWUgPSB0aGlzLl9jZW50cmVTZXJ2aWNlLlNlbGVjdGVkQ2VudHJlUXVhbGlmaWVkTmFtZTtcclxuICAgIHRoaXMuX3JvdXRlXHJcbiAgICAgIC5wYXJhbXNcclxuICAgICAgLmZsYXRNYXAoKHBhcmFtczogUGFyYW1zKSA9PiB7XHJcbiAgICAgICAgdGhpcy5raWRJZCA9IHBhcmFtc1sna2lkSWQnXTtcclxuICAgICAgICByZXR1cm4gT2JzZXJ2YWJsZS5vZih0aGlzLmtpZElkKTtcclxuICAgICAgfSlcclxuICAgICAgLmZsYXRNYXAoa2lkSWQgPT4gdGhpcy5fZW5yb2xtZW50U2VydmljZS5nZXRLaWRIZWFsdGhJbmZvcm1hdGlvbiQodGhpcy5raWRJZCkpXHJcbiAgICAgIC5zdWJzY3JpYmUocmVzID0+IHtcclxuICAgICAgICB0aGlzLmtpZER0byA9IHJlcztcclxuICAgICAgICB0aGlzLmtpZER0b01vZGlmaWVkID0gXy5jbG9uZURlZXAodGhpcy5raWREdG8pO1xyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIHNhdmVDaGFuZ2VzQW5kR29CYWNrKCk6IHZvaWQge1xyXG4gICAgdGhpcy5jcmVhdGVPclVwZGF0ZUhlYWx0aEluZm8oWycuLi8uLi9lbnJvbG1lbnQtZm9ybSddKTtcclxuICB9XHJcblxyXG4gIHNhdmVDaGFuZ2VzQW5kQ29udGludWUoKTogdm9pZCB7XHJcbiAgICB0aGlzLmNyZWF0ZU9yVXBkYXRlSGVhbHRoSW5mbyhbJy4uLy4uL2tpZC1jdXN0b2R5LWluZm8nLCB0aGlzLmtpZElkXSk7XHJcbiAgfVxyXG5cclxuICBjcmVhdGVPclVwZGF0ZUhlYWx0aEluZm8ocmVkaXJlY3RVcmw6IHN0cmluZ1tdKTogdm9pZCB7XHJcblxyXG4gICAgaWYgKHRoaXMua2lkRHRvLmtpZElkID09PSAnMDAwMDAwMDAtMDAwMC0wMDAwLTAwMDAtMDAwMDAwMDAwMDAwJykge1xyXG4gICAgICB0aGlzLnNhdmVBbmRSZWRpcmVjdChyZWRpcmVjdFVybCk7XHJcbiAgICB9IGVsc2UgaWYgKHRoaXMud2FzTm90TW9kaWZpZWQodGhpcy5raWREdG8sIHRoaXMua2lkRHRvTW9kaWZpZWQpKSB7XHJcbiAgICAgIHRoaXMuc2F2ZUFuZFJlZGlyZWN0KHJlZGlyZWN0VXJsKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMucGVuZGluZ0Z1bmN0aW9uID0gKCkgPT4gdGhpcy5zYXZlQW5kUmVkaXJlY3QocmVkaXJlY3RVcmwpO1xyXG4gICAgICB0aGlzLnN0YXRpY01vZGFsLnNob3coKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHNhdmVBbmRSZWRpcmVjdChyZWRpcmVjdFVybDogc3RyaW5nW10pIHtcclxuICAgIGxldCBzYXZpbmdUb2FzdCA9IHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLnBvcChuZXcgU3Bpbm5pbmdUb2FzdCgpKTtcclxuICAgIGxldCB1cGRhdGVIZWFsdGhJbmZvID0gbmV3IENyZWF0ZVVwZGF0ZUtpZEhlYWx0aEluZm9ybWF0aW9uRHRvKCk7XHJcbiAgICB1cGRhdGVIZWFsdGhJbmZvLmluaXQodGhpcy5raWREdG8udG9KU09OKCkpO1xyXG4gICAgdXBkYXRlSGVhbHRoSW5mby5raWRJZCA9IHRoaXMua2lkSWQ7XHJcbiAgICB0aGlzLl9lbnJvbG1lbnRTZXJ2aWNlXHJcbiAgICAgIC5hZGRVcGRhdGVLaWRIZWFsdGhJbmZvcm1hdGlvbkR0byQodXBkYXRlSGVhbHRoSW5mbylcclxuICAgICAgLnN1YnNjcmliZShyZXMgPT4ge1xyXG4gICAgICAgIHRoaXMua2lkRHRvID0gcmVzO1xyXG4gICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLmNsZWFyKHNhdmluZ1RvYXN0LnRvYXN0SWQsIHNhdmluZ1RvYXN0LnRvYXN0Q29udGFpbmVySWQpO1xyXG4gICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLnBvcEFzeW5jKCdzdWNjZXNzJywgJ1NhdmVkJywgJycpO1xyXG4gICAgICAgIHRoaXMuX3JvdXRlci5uYXZpZ2F0ZShyZWRpcmVjdFVybCwgeyByZWxhdGl2ZVRvOiB0aGlzLl9yb3V0ZSB9KTtcclxuICAgICAgfSxcclxuICAgICAgKGVycm9yKSA9PiB7XHJcbiAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoc2F2aW5nVG9hc3QudG9hc3RJZCwgc2F2aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgY2FuY2VsRWRpdCgpIHtcclxuICAgIHRoaXMuX3JvdXRlci5uYXZpZ2F0ZShbJy4uLy4uL2Vucm9sbWVudC1mb3JtJ10sIHsgcmVsYXRpdmVUbzogdGhpcy5fcm91dGUgfSk7XHJcbiAgfVxyXG5cclxuICBvbkhpZGUoZXZlbnQ6IGFueSk6IHZvaWQge1xyXG4gICAgdGhpcy5wZW5kaW5nRnVuY3Rpb24oKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgaW5pdEZvcm0oZmI6IEZvcm1CdWlsZGVyKTogdm9pZCB7XHJcbiAgICB0aGlzLmZvcm0gPSBmYi5ncm91cCh7XHJcbiAgICAgICdtZWRpY2FsU2VydmljZXNOYW1lJzogWycnLCBWYWxpZGF0b3JzLm1heExlbmd0aCgxMDApXSxcclxuICAgICAgJ21lZGljYWxTZXJ2aWNlc0NvbnRhY3ROdW1iZXInOiBbJycsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDUwKV0sXHJcbiAgICAgICdtZWRpY2FyZU51bWJlcic6IFsnJywgVmFsaWRhdG9ycy5tYXhMZW5ndGgoNTApXSxcclxuICAgICAgJ21lZGljYXJlRXhwaXJ5RGF0ZSc6IFsnJ10sXHJcbiAgICAgICdoZWFsdGhjYXJlQ2FyZE51bWJlcic6IFsnJywgVmFsaWRhdG9ycy5tYXhMZW5ndGgoNTApXSxcclxuICAgICAgJ2hlYWx0aGNhcmVDYXJkRXhwaXJ5RGF0ZSc6IFsnJ10sXHJcbiAgICAgICdwcml2YXRlSGVhbHRoSW5zdXJlcic6IFsnJywgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTAwKV0sXHJcbiAgICAgICdwcml2YXRlSGVhbHRoTWVtYmVyc2hpcE51bWJlcic6IFsnJywgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTAwKV0sXHJcbiAgICAgICdoYXNBbGxlcmdpZXMnOiBbJyddLFxyXG4gICAgICAnaGFzQW5hcGh5bGF4aXMnOiBbJyddLFxyXG4gICAgICAnaGFzSW5qZWN0aW9uRGV2aWNlJzogWycnXSxcclxuICAgICAgJ2hhc0FzdGhtYSc6IFsnJ10sXHJcbiAgICAgICdpc0ltbXVuaXNlZCc6IFsnJ10sXHJcbiAgICAgICdoYXNTcGVjaWFsTmVlZHMnOiBbJyddLFxyXG4gICAgICAnaGFzU3VwcG9ydFNlcnZpY2VzJzogWycnXSxcclxuICAgICAgJ2RpZXRhcnlOZWVkcyc6IFsnJywgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMjAwMCldLFxyXG4gICAgICAnZGVudGFsTmVlZHMnOiBbJycsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDIwMDApXSxcclxuICAgICAgJ21lZGljYWxDb25kaXRpb24xJzogWycnLCBWYWxpZGF0b3JzLm1heExlbmd0aCgxMDApXSxcclxuICAgICAgJ21lZGljYWxDb25kaXRpb24yJzogWycnLCBWYWxpZGF0b3JzLm1heExlbmd0aCgxMDApXSxcclxuICAgICAgJ21lZGljYWxDb25kaXRpb24zJzogWycnLCBWYWxpZGF0b3JzLm1heExlbmd0aCgxMDApXSxcclxuICAgICAgJ21lZGljYWxDb25kaXRpb240JzogWycnLCBWYWxpZGF0b3JzLm1heExlbmd0aCgxMDApXSxcclxuICAgICAgJ2NvbnNlbnRUb0FjY3VyYWN5JzogWycnLCBWYWxpZGF0b3JzLnJlcXVpcmVkXSxcclxuICAgICAgJ2NvbnNlbnRUb0NvbGxlY3Rpb24nOiBbJycsIFZhbGlkYXRvcnMucmVxdWlyZWRdLFxyXG4gICAgICAnY29uc2VudFRvVHJlYXRtZW50JzogWycnLCBWYWxpZGF0b3JzLnJlcXVpcmVkXSxcclxuICAgICAgJ2NvbnRlbnRUb0Nvc3QnOiBbJycsIFZhbGlkYXRvcnMucmVxdWlyZWRdXHJcbiAgICB9KTtcclxuXHJcbiAgICB0aGlzLm1lZGljYWxTZXJ2aWNlc05hbWUgPSB0aGlzLmZvcm0uY29udHJvbHNbJ21lZGljYWxTZXJ2aWNlc05hbWUnXTtcclxuICAgIHRoaXMubWVkaWNhbFNlcnZpY2VzQ29udGFjdE51bWJlciA9IHRoaXMuZm9ybS5jb250cm9sc1snbWVkaWNhbFNlcnZpY2VzQ29udGFjdE51bWJlciddO1xyXG4gICAgdGhpcy5tZWRpY2FyZU51bWJlciA9IHRoaXMuZm9ybS5jb250cm9sc1snbWVkaWNhcmVOdW1iZXInXTtcclxuICAgIHRoaXMubWVkaWNhcmVFeHBpcnlEYXRlID0gdGhpcy5mb3JtLmNvbnRyb2xzWydtZWRpY2FyZUV4cGlyeURhdGUnXTtcclxuICAgIHRoaXMuaGVhbHRoY2FyZUNhcmROdW1iZXIgPSB0aGlzLmZvcm0uY29udHJvbHNbJ2hlYWx0aGNhcmVDYXJkTnVtYmVyJ107XHJcbiAgICB0aGlzLmhlYWx0aGNhcmVDYXJkRXhwaXJ5RGF0ZSA9IHRoaXMuZm9ybS5jb250cm9sc1snaGVhbHRoY2FyZUNhcmRFeHBpcnlEYXRlJ107XHJcbiAgICB0aGlzLnByaXZhdGVIZWFsdGhJbnN1cmVyID0gdGhpcy5mb3JtLmNvbnRyb2xzWydwcml2YXRlSGVhbHRoSW5zdXJlciddO1xyXG4gICAgdGhpcy5wcml2YXRlSGVhbHRoTWVtYmVyc2hpcE51bWJlciA9IHRoaXMuZm9ybS5jb250cm9sc1sncHJpdmF0ZUhlYWx0aE1lbWJlcnNoaXBOdW1iZXInXTtcclxuICAgIHRoaXMuaGFzQWxsZXJnaWVzID0gdGhpcy5mb3JtLmNvbnRyb2xzWydoYXNBbGxlcmdpZXMnXTtcclxuICAgIHRoaXMuaGFzQW5hcGh5bGF4aXMgPSB0aGlzLmZvcm0uY29udHJvbHNbJ2hhc0FuYXBoeWxheGlzJ107XHJcbiAgICB0aGlzLmhhc0luamVjdGlvbkRldmljZSA9IHRoaXMuZm9ybS5jb250cm9sc1snaGFzSW5qZWN0aW9uRGV2aWNlJ107XHJcbiAgICB0aGlzLmhhc0FzdGhtYSA9IHRoaXMuZm9ybS5jb250cm9sc1snaGFzQXN0aG1hJ107XHJcbiAgICB0aGlzLmlzSW1tdW5pc2VkID0gdGhpcy5mb3JtLmNvbnRyb2xzWydpc0ltbXVuaXNlZCddO1xyXG4gICAgdGhpcy5oYXNTcGVjaWFsTmVlZHMgPSB0aGlzLmZvcm0uY29udHJvbHNbJ2hhc1NwZWNpYWxOZWVkcyddO1xyXG4gICAgdGhpcy5oYXNTdXBwb3J0U2VydmljZXMgPSB0aGlzLmZvcm0uY29udHJvbHNbJ2hhc1N1cHBvcnRTZXJ2aWNlcyddO1xyXG4gICAgdGhpcy5kaWV0YXJ5TmVlZHMgPSB0aGlzLmZvcm0uY29udHJvbHNbJ2RpZXRhcnlOZWVkcyddO1xyXG4gICAgdGhpcy5kZW50YWxOZWVkcyA9IHRoaXMuZm9ybS5jb250cm9sc1snZGVudGFsTmVlZHMnXTtcclxuICAgIHRoaXMubWVkaWNhbENvbmRpdGlvbjEgPSB0aGlzLmZvcm0uY29udHJvbHNbJ21lZGljYWxDb25kaXRpb24xJ107XHJcbiAgICB0aGlzLm1lZGljYWxDb25kaXRpb24yID0gdGhpcy5mb3JtLmNvbnRyb2xzWydtZWRpY2FsQ29uZGl0aW9uMiddO1xyXG4gICAgdGhpcy5tZWRpY2FsQ29uZGl0aW9uMyA9IHRoaXMuZm9ybS5jb250cm9sc1snbWVkaWNhbENvbmRpdGlvbjMnXTtcclxuICAgIHRoaXMubWVkaWNhbENvbmRpdGlvbjQgPSB0aGlzLmZvcm0uY29udHJvbHNbJ21lZGljYWxDb25kaXRpb240J107XHJcbiAgICB0aGlzLmNvbnNlbnRUb0FjY3VyYWN5ID0gdGhpcy5mb3JtLmNvbnRyb2xzWydjb25zZW50VG9BY2N1cmFjeSddO1xyXG4gICAgdGhpcy5jb25zZW50VG9Db2xsZWN0aW9uID0gdGhpcy5mb3JtLmNvbnRyb2xzWydjb25zZW50VG9Db2xsZWN0aW9uJ107XHJcbiAgICB0aGlzLmNvbnNlbnRUb1RyZWF0bWVudCA9IHRoaXMuZm9ybS5jb250cm9sc1snY29uc2VudFRvVHJlYXRtZW50J107XHJcbiAgICB0aGlzLmNvbnRlbnRUb0Nvc3QgPSB0aGlzLmZvcm0uY29udHJvbHNbJ2NvbnRlbnRUb0Nvc3QnXTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgd2FzTm90TW9kaWZpZWQobW9kaWZpZWRLaWQ6IEtpZEhlYWx0aEluZm9ybWF0aW9uRHRvLCBvcmlnaW5hbEtpZDogS2lkSGVhbHRoSW5mb3JtYXRpb25EdG8pOiBib29sZWFuIHtcclxuICAgIHJldHVybiBfLmlzRXF1YWwobW9kaWZpZWRLaWQsIG9yaWdpbmFsS2lkKTtcclxuICB9XHJcbn1cclxuIl19
