"use strict";
var index_1 = require('./kid-edit-details/index');
var index_2 = require('./kid-desired-days/index');
var index_3 = require('./kid-health-info/index');
var index_4 = require('./kid-custody-info/index');
exports.EnrolmentKidEditRoutes = [
    {
        path: 'kid-edit-enrolment',
        redirectTo: 'kid-edit-details'
    },
    {
        path: 'kid-edit-details',
        component: index_1.EnrolmentKidEditComponent
    },
    {
        path: 'kid-edit-details/:kidId',
        component: index_1.EnrolmentKidEditComponent
    },
    {
        path: 'kid-desired-days/:kidId',
        component: index_2.KidDesiredDaysComponent
    },
    {
        path: 'kid-health-info/:kidId',
        component: index_3.KidHealthInfoComponent
    },
    {
        path: 'kid-custody-info/:kidId',
        component: index_4.KidCustodyInfoComponent
    }
];

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQva2lkLWVkaXQtZW5yb2xtZW50L2tpZC1lZGl0LWVucm9sbWVudC5yb3V0ZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBLHNCQUEwQywwQkFBMEIsQ0FBQyxDQUFBO0FBQ3JFLHNCQUF3QywwQkFBMEIsQ0FBQyxDQUFBO0FBQ25FLHNCQUF1Qyx5QkFBeUIsQ0FBQyxDQUFBO0FBQ2pFLHNCQUF3QywwQkFBMEIsQ0FBQyxDQUFBO0FBRXRELDhCQUFzQixHQUFZO0lBQzlDO1FBQ0MsSUFBSSxFQUFFLG9CQUFvQjtRQUMxQixVQUFVLEVBQUUsa0JBQWtCO0tBQzlCO0lBQ0Q7UUFDQyxJQUFJLEVBQUUsa0JBQWtCO1FBQ3hCLFNBQVMsRUFBRSxpQ0FBeUI7S0FDcEM7SUFDRDtRQUNDLElBQUksRUFBRSx5QkFBeUI7UUFDL0IsU0FBUyxFQUFFLGlDQUF5QjtLQUNwQztJQUNEO1FBQ0MsSUFBSSxFQUFFLHlCQUF5QjtRQUMvQixTQUFTLEVBQUUsK0JBQXVCO0tBQ2xDO0lBQ0Q7UUFDQyxJQUFJLEVBQUUsd0JBQXdCO1FBQzlCLFNBQVMsRUFBRSw4QkFBc0I7S0FDakM7SUFDRDtRQUNDLElBQUksRUFBRSx5QkFBeUI7UUFDL0IsU0FBUyxFQUFFLCtCQUF1QjtLQUNsQztDQUNELENBQUMiLCJmaWxlIjoiYXBwL3BhZ2VzL3BhcmVudHMtcG9ydGFsL2Vucm9sbWVudC9raWQtZWRpdC1lbnJvbG1lbnQva2lkLWVkaXQtZW5yb2xtZW50LnJvdXRlcy5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgRW5yb2xtZW50S2lkRWRpdENvbXBvbmVudCB9IGZyb20gJy4va2lkLWVkaXQtZGV0YWlscy9pbmRleCc7XHJcbmltcG9ydCB7IEtpZERlc2lyZWREYXlzQ29tcG9uZW50IH0gZnJvbSAnLi9raWQtZGVzaXJlZC1kYXlzL2luZGV4JztcclxuaW1wb3J0IHsgS2lkSGVhbHRoSW5mb0NvbXBvbmVudCB9IGZyb20gJy4va2lkLWhlYWx0aC1pbmZvL2luZGV4JztcclxuaW1wb3J0IHsgS2lkQ3VzdG9keUluZm9Db21wb25lbnQgfSBmcm9tICcuL2tpZC1jdXN0b2R5LWluZm8vaW5kZXgnO1xyXG5cclxuZXhwb3J0IGNvbnN0IEVucm9sbWVudEtpZEVkaXRSb3V0ZXM6IFJvdXRlW10gPSBbXHJcblx0e1xyXG5cdFx0cGF0aDogJ2tpZC1lZGl0LWVucm9sbWVudCcsXHJcblx0XHRyZWRpcmVjdFRvOiAna2lkLWVkaXQtZGV0YWlscydcclxuXHR9LFxyXG5cdHtcclxuXHRcdHBhdGg6ICdraWQtZWRpdC1kZXRhaWxzJyxcclxuXHRcdGNvbXBvbmVudDogRW5yb2xtZW50S2lkRWRpdENvbXBvbmVudFxyXG5cdH0sXHJcblx0e1xyXG5cdFx0cGF0aDogJ2tpZC1lZGl0LWRldGFpbHMvOmtpZElkJyxcclxuXHRcdGNvbXBvbmVudDogRW5yb2xtZW50S2lkRWRpdENvbXBvbmVudFxyXG5cdH0sXHJcblx0e1xyXG5cdFx0cGF0aDogJ2tpZC1kZXNpcmVkLWRheXMvOmtpZElkJyxcclxuXHRcdGNvbXBvbmVudDogS2lkRGVzaXJlZERheXNDb21wb25lbnRcclxuXHR9LFxyXG5cdHtcclxuXHRcdHBhdGg6ICdraWQtaGVhbHRoLWluZm8vOmtpZElkJyxcclxuXHRcdGNvbXBvbmVudDogS2lkSGVhbHRoSW5mb0NvbXBvbmVudFxyXG5cdH0sXHJcblx0e1xyXG5cdFx0cGF0aDogJ2tpZC1jdXN0b2R5LWluZm8vOmtpZElkJyxcclxuXHRcdGNvbXBvbmVudDogS2lkQ3VzdG9keUluZm9Db21wb25lbnRcclxuXHR9XHJcbl07XHJcbiJdfQ==
