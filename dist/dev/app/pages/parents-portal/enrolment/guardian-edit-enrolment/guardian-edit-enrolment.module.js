"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../../shared/index');
var index_2 = require('./index');
var EnrolmentGuardianEditModule = (function () {
    function EnrolmentGuardianEditModule() {
    }
    EnrolmentGuardianEditModule = __decorate([
        core_1.NgModule({
            imports: [index_1.SharedModule],
            declarations: [index_2.EnrolmentGuardianEditComponent],
            exports: [index_2.EnrolmentGuardianEditComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], EnrolmentGuardianEditModule);
    return EnrolmentGuardianEditModule;
}());
exports.EnrolmentGuardianEditModule = EnrolmentGuardianEditModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvZ3VhcmRpYW4tZWRpdC1lbnJvbG1lbnQvZ3VhcmRpYW4tZWRpdC1lbnJvbG1lbnQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBeUIsZUFBZSxDQUFDLENBQUE7QUFDekMsc0JBQTZCLDBCQUEwQixDQUFDLENBQUE7QUFFeEQsc0JBQStDLFNBQVMsQ0FBQyxDQUFBO0FBUXpEO0lBQUE7SUFBMkMsQ0FBQztJQU41QztRQUFDLGVBQVEsQ0FBQztZQUNSLE9BQU8sRUFBRSxDQUFDLG9CQUFZLENBQUM7WUFDckIsWUFBWSxFQUFFLENBQUMsc0NBQThCLENBQUM7WUFDOUMsT0FBTyxFQUFFLENBQUMsc0NBQThCLENBQUM7U0FDNUMsQ0FBQzs7bUNBQUE7SUFFeUMsa0NBQUM7QUFBRCxDQUEzQyxBQUE0QyxJQUFBO0FBQS9CLG1DQUEyQiw4QkFBSSxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvZ3VhcmRpYW4tZWRpdC1lbnJvbG1lbnQvZ3VhcmRpYW4tZWRpdC1lbnJvbG1lbnQubW9kdWxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU2hhcmVkTW9kdWxlIH0gZnJvbSAnLi4vLi4vLi4vLi4vc2hhcmVkL2luZGV4JztcclxuXHJcbmltcG9ydCB7IEVucm9sbWVudEd1YXJkaWFuRWRpdENvbXBvbmVudCB9IGZyb20gJy4vaW5kZXgnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBpbXBvcnRzOiBbU2hhcmVkTW9kdWxlXSxcclxuICAgIGRlY2xhcmF0aW9uczogW0Vucm9sbWVudEd1YXJkaWFuRWRpdENvbXBvbmVudF0sXHJcbiAgICBleHBvcnRzOiBbRW5yb2xtZW50R3VhcmRpYW5FZGl0Q29tcG9uZW50XVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEVucm9sbWVudEd1YXJkaWFuRWRpdE1vZHVsZSB7IH1cclxuIl19
