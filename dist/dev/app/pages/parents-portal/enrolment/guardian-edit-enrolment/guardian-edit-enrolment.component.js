"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var router_1 = require('@angular/router');
var rxjs_1 = require('rxjs');
var index_1 = require('../index');
var index_2 = require('../../index');
var index_3 = require('../../../../api/index');
var angular2_toaster_1 = require('angular2-toaster');
var index_4 = require('../../../../shared/index');
var index_5 = require('../../../../validators/index');
var EnrolmentGuardianEditComponent = (function () {
    function EnrolmentGuardianEditComponent(_router, _route, _enrolmentService, _centreService, _toasterService, fb) {
        this._router = _router;
        this._route = _route;
        this._enrolmentService = _enrolmentService;
        this._centreService = _centreService;
        this._toasterService = _toasterService;
        this.maxDateOfBirth = null;
        this.guardianDto = null;
        this.relationshipOptions = new Array();
        this.titles = new Array();
        this.genders = new Array();
        var today = new Date();
        this.maxDateOfBirth = new Date(today.getFullYear() - 16, today.getMonth()).toJSON().split('T')[0];
        this.form = fb.group({
            'title': [''],
            'firstname': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.maxLength(100)])],
            'lastname': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.maxLength(100)])],
            'mobile': ['', index_5.MobileValidator.validate],
            'email': ['', forms_1.Validators.compose([index_5.EmailValidator.validate, forms_1.Validators.maxLength(100)])],
            'homePhone': ['', forms_1.Validators.maxLength(100)],
            'workPhone': ['', forms_1.Validators.maxLength(100)],
            'gender': ['',],
            'dateOfBirth': [''],
            'nationality': ['', forms_1.Validators.maxLength(50)],
            'relationship': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.maxLength(50)])],
            'streetAddress': ['', forms_1.Validators.maxLength(200)],
            'suburb': ['', forms_1.Validators.maxLength(50)],
            'postcode': ['', forms_1.Validators.pattern('(\\d{4})')],
            'state': ['', forms_1.Validators.maxLength(50)],
            'crn': ['', forms_1.Validators.pattern('[0-9]{9}[a-zA-Z]')]
        });
        this.title = this.form.controls['title'];
        this.firstname = this.form.controls['firstname'];
        this.lastname = this.form.controls['lastname'];
        this.mobile = this.form.controls['mobile'];
        this.email = this.form.controls['email'];
        this.homePhone = this.form.controls['homePhone'];
        this.workPhone = this.form.controls['workPhone'];
        this.gender = this.form.controls['gender'];
        this.dateOfBirth = this.form.controls['dateOfBirth'];
        this.nationality = this.form.controls['nationality'];
        this.relationship = this.form.controls['relationship'];
        this.streetAddress = this.form.controls['streetAddress'];
        this.suburb = this.form.controls['suburb'];
        this.postcode = this.form.controls['postcode'];
        this.state = this.form.controls['state'];
        this.crn = this.form.controls['crn'];
    }
    EnrolmentGuardianEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route
            .queryParams
            .map(function (params) { return params['guardianId']; })
            .flatMap(function (guardianId) {
            return (guardianId === null || guardianId === undefined) ?
                rxjs_1.Observable.of(null) :
                _this._enrolmentService.getGuardian$(guardianId);
        })
            .subscribe(function (dto) {
            if ((dto !== null && dto !== undefined)) {
                _this.guardianDto = dto;
            }
            else {
                _this._route
                    .queryParams
                    .map(function (params) { return params['enrolmentFormId']; })
                    .subscribe(function (enrolmentFormId) {
                    if (enrolmentFormId === null || enrolmentFormId === undefined || enrolmentFormId === null)
                        throw new Error('Must provide enrolment form Id when creating new guardian');
                    _this.guardianDto = new index_3.GuardianDto();
                    _this.guardianDto.init({
                        EnrolmentFormId: enrolmentFormId
                    });
                });
            }
        });
        rxjs_1.Observable
            .forkJoin(this._enrolmentService.getRelationshipOptions$(), this._enrolmentService.getTitles$(), this._enrolmentService.getGenders$())
            .subscribe(function (res) {
            _this.relationshipOptions = _this._enrolmentService.convertEnumToArray(res[0]);
            _this.titles = _this._enrolmentService.convertEnumToArray(res[1]);
            _this.genders = _this._enrolmentService.convertEnumToArray(res[2]);
        });
    };
    EnrolmentGuardianEditComponent.prototype.saveChanges = function () {
        var _this = this;
        var savingToast = this._toasterService.pop(new index_4.SpinningToast());
        var updateGuardian = new index_3.CreateUpdateGuardianDto();
        updateGuardian.init(this.guardianDto.toJSON());
        this._enrolmentService
            .addOrUpdateGuardian$(updateGuardian)
            .subscribe(function (res) {
            _this.guardianDto = res;
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this._toasterService.popAsync('success', 'Saved', '');
            _this._router.navigate(['../enrolment-form'], { relativeTo: _this._route });
        }, function (error) {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
        });
    };
    EnrolmentGuardianEditComponent.prototype.cancelEdit = function () {
        this._router.navigate(['../enrolment-form'], { relativeTo: this._route });
    };
    EnrolmentGuardianEditComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            encapsulation: core_1.ViewEncapsulation.None,
            selector: 'guardian-edit-enrolment-cmp',
            templateUrl: 'guardian-edit-enrolment.component.html',
            styleUrls: ['guardian-edit-enrolment.component.css']
        }), 
        __metadata('design:paramtypes', [router_1.Router, router_1.ActivatedRoute, index_1.EnrolmentService, index_2.CentreService, angular2_toaster_1.ToasterService, forms_1.FormBuilder])
    ], EnrolmentGuardianEditComponent);
    return EnrolmentGuardianEditComponent;
}());
exports.EnrolmentGuardianEditComponent = EnrolmentGuardianEditComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9lbnJvbG1lbnQvZ3VhcmRpYW4tZWRpdC1lbnJvbG1lbnQvZ3VhcmRpYW4tZWRpdC1lbnJvbG1lbnQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBc0QsZUFBZSxDQUFDLENBQUE7QUFDdEUsc0JBQW9FLGdCQUFnQixDQUFDLENBQUE7QUFDckYsdUJBQStDLGlCQUFpQixDQUFDLENBQUE7QUFDakUscUJBQTJCLE1BQU0sQ0FBQyxDQUFBO0FBRWxDLHNCQUFpQyxVQUFVLENBQUMsQ0FBQTtBQUM1QyxzQkFBOEIsYUFBYSxDQUFDLENBQUE7QUFDNUMsc0JBQXFELHVCQUF1QixDQUFDLENBQUE7QUFDN0UsaUNBQStCLGtCQUFrQixDQUFDLENBQUE7QUFDbEQsc0JBQThCLDBCQUEwQixDQUFDLENBQUE7QUFDekQsc0JBQWdELDhCQUE4QixDQUFDLENBQUE7QUFVL0U7SUEyQkUsd0NBQW9CLE9BQWUsRUFDekIsTUFBc0IsRUFDdEIsaUJBQW1DLEVBQ25DLGNBQTZCLEVBQzdCLGVBQStCLEVBQ3ZDLEVBQWU7UUFMRyxZQUFPLEdBQVAsT0FBTyxDQUFRO1FBQ3pCLFdBQU0sR0FBTixNQUFNLENBQWdCO1FBQ3RCLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBa0I7UUFDbkMsbUJBQWMsR0FBZCxjQUFjLENBQWU7UUFDN0Isb0JBQWUsR0FBZixlQUFlLENBQWdCO1FBN0JsQyxtQkFBYyxHQUFXLElBQUksQ0FBQztRQUM5QixnQkFBVyxHQUFnQixJQUFJLENBQUM7UUFvQi9CLHdCQUFtQixHQUFnQyxJQUFJLEtBQUssRUFBNkIsQ0FBQztRQUMxRixXQUFNLEdBQWdDLElBQUksS0FBSyxFQUE2QixDQUFDO1FBQzdFLFlBQU8sR0FBZ0MsSUFBSSxLQUFLLEVBQTZCLENBQUM7UUFTcEYsSUFBSSxLQUFLLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUN2QixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUUsR0FBRyxFQUFFLEVBQUUsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRWxHLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQztZQUNuQixPQUFPLEVBQUUsQ0FBQyxFQUFFLENBQUM7WUFDYixXQUFXLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdkYsVUFBVSxFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsa0JBQVUsQ0FBQyxRQUFRLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3RGLFFBQVEsRUFBRSxDQUFDLEVBQUUsRUFBRSx1QkFBZSxDQUFDLFFBQVEsQ0FBQztZQUN4QyxPQUFPLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxzQkFBYyxDQUFDLFFBQVEsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdkYsV0FBVyxFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzVDLFdBQVcsRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUM1QyxRQUFRLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDZixhQUFhLEVBQUUsQ0FBQyxFQUFFLENBQUM7WUFDbkIsYUFBYSxFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQzdDLGNBQWMsRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLGtCQUFVLENBQUMsUUFBUSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN6RixlQUFlLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDaEQsUUFBUSxFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ3hDLFVBQVUsRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUNoRCxPQUFPLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDdkMsS0FBSyxFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLENBQUM7U0FDcEQsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ2pELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDL0MsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMzQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDakQsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNqRCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzNDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDckQsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNyRCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ3ZELElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDekQsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMzQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQy9DLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDekMsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN2QyxDQUFDO0lBRUQsaURBQVEsR0FBUjtRQUFBLGlCQXVDQztRQXJDQyxJQUFJLENBQUMsTUFBTTthQUNSLFdBQVc7YUFDWCxHQUFHLENBQUMsVUFBQyxNQUFjLElBQUssT0FBQSxNQUFNLENBQUMsWUFBWSxDQUFDLEVBQXBCLENBQW9CLENBQUM7YUFDN0MsT0FBTyxDQUFDLFVBQUEsVUFBVTtZQUNqQixPQUFBLENBQUMsVUFBVSxLQUFLLElBQUksSUFBSSxVQUFVLEtBQUssU0FBUyxDQUFDO2dCQUMvQyxpQkFBVSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7Z0JBQ25CLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDO1FBRmpELENBRWlELENBQUM7YUFDbkQsU0FBUyxDQUFDLFVBQUEsR0FBRztZQUVaLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxLQUFLLElBQUksSUFBSSxHQUFHLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN4QyxLQUFJLENBQUMsV0FBVyxHQUFHLEdBQUcsQ0FBQztZQUN6QixDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ04sS0FBSSxDQUFDLE1BQU07cUJBQ1IsV0FBVztxQkFDWCxHQUFHLENBQUMsVUFBQyxNQUFjLElBQUssT0FBQSxNQUFNLENBQUMsaUJBQWlCLENBQUMsRUFBekIsQ0FBeUIsQ0FBQztxQkFDbEQsU0FBUyxDQUFDLFVBQUEsZUFBZTtvQkFDeEIsRUFBRSxDQUFDLENBQUMsZUFBZSxLQUFLLElBQUksSUFBSSxlQUFlLEtBQUssU0FBUyxJQUFJLGVBQWUsS0FBSyxJQUFJLENBQUM7d0JBQ3hGLE1BQU0sSUFBSSxLQUFLLENBQUMsMkRBQTJELENBQUMsQ0FBQztvQkFDL0UsS0FBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLG1CQUFXLEVBQUUsQ0FBQztvQkFDckMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUM7d0JBQ3BCLGVBQWUsRUFBRSxlQUFlO3FCQUNqQyxDQUFDLENBQUM7Z0JBQ0wsQ0FBQyxDQUFDLENBQUM7WUFDUCxDQUFDO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFFTCxpQkFBVTthQUNQLFFBQVEsQ0FDVCxJQUFJLENBQUMsaUJBQWlCLENBQUMsdUJBQXVCLEVBQUUsRUFDaEQsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsRUFBRSxFQUNuQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsV0FBVyxFQUFFLENBQ25DO2FBQ0EsU0FBUyxDQUFDLFVBQUEsR0FBRztZQUNaLEtBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFJLENBQUMsaUJBQWlCLENBQUMsa0JBQWtCLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDN0UsS0FBSSxDQUFDLE1BQU0sR0FBRyxLQUFJLENBQUMsaUJBQWlCLENBQUMsa0JBQWtCLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDaEUsS0FBSSxDQUFDLE9BQU8sR0FBRyxLQUFJLENBQUMsaUJBQWlCLENBQUMsa0JBQWtCLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbkUsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsb0RBQVcsR0FBWDtRQUFBLGlCQWVDO1FBZEMsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsSUFBSSxxQkFBYSxFQUFFLENBQUMsQ0FBQztRQUNoRSxJQUFJLGNBQWMsR0FBRyxJQUFJLCtCQUF1QixFQUFFLENBQUM7UUFDbkQsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7UUFDL0MsSUFBSSxDQUFDLGlCQUFpQjthQUNuQixvQkFBb0IsQ0FBQyxjQUFjLENBQUM7YUFDcEMsU0FBUyxDQUFDLFVBQUEsR0FBRztZQUNaLEtBQUksQ0FBQyxXQUFXLEdBQUcsR0FBRyxDQUFDO1lBQ3ZCLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDOUUsS0FBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLE9BQU8sRUFBRSxFQUFFLENBQUMsQ0FBQztZQUN0RCxLQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLEVBQUUsRUFBRSxVQUFVLEVBQUUsS0FBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7UUFDNUUsQ0FBQyxFQUNELFVBQUMsS0FBSztZQUNKLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDaEYsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsbURBQVUsR0FBVjtRQUNFLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsbUJBQW1CLENBQUMsRUFBRSxFQUFFLFVBQVUsRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQztJQUM1RSxDQUFDO0lBN0lIO1FBQUMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixhQUFhLEVBQUUsd0JBQWlCLENBQUMsSUFBSTtZQUNyQyxRQUFRLEVBQUUsNkJBQTZCO1lBQ3ZDLFdBQVcsRUFBRSx3Q0FBd0M7WUFDckQsU0FBUyxFQUFFLENBQUMsdUNBQXVDLENBQUM7U0FDckQsQ0FBQzs7c0NBQUE7SUF3SUYscUNBQUM7QUFBRCxDQXRJQSxBQXNJQyxJQUFBO0FBdElZLHNDQUE4QixpQ0FzSTFDLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL3BhcmVudHMtcG9ydGFsL2Vucm9sbWVudC9ndWFyZGlhbi1lZGl0LWVucm9sbWVudC9ndWFyZGlhbi1lZGl0LWVucm9sbWVudC5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0VuY2Fwc3VsYXRpb24sIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1Hcm91cCwgQWJzdHJhY3RDb250cm9sLCBGb3JtQnVpbGRlciwgVmFsaWRhdG9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgUm91dGVyLCBBY3RpdmF0ZWRSb3V0ZSwgUGFyYW1zIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgRW5yb2xtZW50U2VydmljZSB9IGZyb20gJy4uL2luZGV4JztcclxuaW1wb3J0IHsgQ2VudHJlU2VydmljZSB9IGZyb20gJy4uLy4uL2luZGV4JztcclxuaW1wb3J0IHsgR3VhcmRpYW5EdG8sIENyZWF0ZVVwZGF0ZUd1YXJkaWFuRHRvIH0gZnJvbSAnLi4vLi4vLi4vLi4vYXBpL2luZGV4JztcclxuaW1wb3J0IHsgVG9hc3RlclNlcnZpY2UgfSBmcm9tICdhbmd1bGFyMi10b2FzdGVyJztcclxuaW1wb3J0IHsgU3Bpbm5pbmdUb2FzdCB9IGZyb20gJy4uLy4uLy4uLy4uL3NoYXJlZC9pbmRleCc7XHJcbmltcG9ydCB7IEVtYWlsVmFsaWRhdG9yLCBNb2JpbGVWYWxpZGF0b3IgfSBmcm9tICcuLi8uLi8uLi8uLi92YWxpZGF0b3JzL2luZGV4JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcclxuICBzZWxlY3RvcjogJ2d1YXJkaWFuLWVkaXQtZW5yb2xtZW50LWNtcCcsXHJcbiAgdGVtcGxhdGVVcmw6ICdndWFyZGlhbi1lZGl0LWVucm9sbWVudC5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJ2d1YXJkaWFuLWVkaXQtZW5yb2xtZW50LmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEVucm9sbWVudEd1YXJkaWFuRWRpdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIHB1YmxpYyBtYXhEYXRlT2ZCaXJ0aDogc3RyaW5nID0gbnVsbDtcclxuICBwdWJsaWMgZ3VhcmRpYW5EdG86IEd1YXJkaWFuRHRvID0gbnVsbDtcclxuXHJcbiAgcHJpdmF0ZSBmb3JtOiBGb3JtR3JvdXA7XHJcbiAgcHJpdmF0ZSB0aXRsZTogQWJzdHJhY3RDb250cm9sO1xyXG4gIHByaXZhdGUgZmlyc3RuYW1lOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBsYXN0bmFtZTogQWJzdHJhY3RDb250cm9sO1xyXG4gIHByaXZhdGUgbW9iaWxlOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBlbWFpbDogQWJzdHJhY3RDb250cm9sO1xyXG4gIHByaXZhdGUgaG9tZVBob25lOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSB3b3JrUGhvbmU6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIGdlbmRlcjogQWJzdHJhY3RDb250cm9sO1xyXG4gIHByaXZhdGUgZGF0ZU9mQmlydGg6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIG5hdGlvbmFsaXR5OiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSByZWxhdGlvbnNoaXA6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIHN0cmVldEFkZHJlc3M6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIHN1YnVyYjogQWJzdHJhY3RDb250cm9sO1xyXG4gIHByaXZhdGUgcG9zdGNvZGU6IEFic3RyYWN0Q29udHJvbDtcclxuICBwcml2YXRlIHN0YXRlOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgcHJpdmF0ZSBjcm46IEFic3RyYWN0Q29udHJvbDtcclxuXHJcbiAgcHJpdmF0ZSByZWxhdGlvbnNoaXBPcHRpb25zOiB7IFtrZXk6IHN0cmluZ106IHN0cmluZyB9W10gPSBuZXcgQXJyYXk8eyBba2V5OiBzdHJpbmddOiBzdHJpbmcgfT4oKTtcclxuICBwcml2YXRlIHRpdGxlczogeyBba2V5OiBzdHJpbmddOiBzdHJpbmcgfVtdID0gbmV3IEFycmF5PHsgW2tleTogc3RyaW5nXTogc3RyaW5nIH0+KCk7XHJcbiAgcHJpdmF0ZSBnZW5kZXJzOiB7IFtrZXk6IHN0cmluZ106IHN0cmluZyB9W10gPSBuZXcgQXJyYXk8eyBba2V5OiBzdHJpbmddOiBzdHJpbmcgfT4oKTtcclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBfcm91dGVyOiBSb3V0ZXIsXHJcbiAgICBwcml2YXRlIF9yb3V0ZTogQWN0aXZhdGVkUm91dGUsXHJcbiAgICBwcml2YXRlIF9lbnJvbG1lbnRTZXJ2aWNlOiBFbnJvbG1lbnRTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBfY2VudHJlU2VydmljZTogQ2VudHJlU2VydmljZSxcclxuICAgIHByaXZhdGUgX3RvYXN0ZXJTZXJ2aWNlOiBUb2FzdGVyU2VydmljZSxcclxuICAgIGZiOiBGb3JtQnVpbGRlcikge1xyXG5cclxuICAgIGxldCB0b2RheSA9IG5ldyBEYXRlKCk7XHJcbiAgICB0aGlzLm1heERhdGVPZkJpcnRoID0gbmV3IERhdGUodG9kYXkuZ2V0RnVsbFllYXIoKSAtIDE2LCB0b2RheS5nZXRNb250aCgpKS50b0pTT04oKS5zcGxpdCgnVCcpWzBdO1xyXG5cclxuICAgIHRoaXMuZm9ybSA9IGZiLmdyb3VwKHtcclxuICAgICAgJ3RpdGxlJzogWycnXSxcclxuICAgICAgJ2ZpcnN0bmFtZSc6IFsnJywgVmFsaWRhdG9ycy5jb21wb3NlKFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1heExlbmd0aCgxMDApXSldLFxyXG4gICAgICAnbGFzdG5hbWUnOiBbJycsIFZhbGlkYXRvcnMuY29tcG9zZShbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTAwKV0pXSxcclxuICAgICAgJ21vYmlsZSc6IFsnJywgTW9iaWxlVmFsaWRhdG9yLnZhbGlkYXRlXSxcclxuICAgICAgJ2VtYWlsJzogWycnLCBWYWxpZGF0b3JzLmNvbXBvc2UoW0VtYWlsVmFsaWRhdG9yLnZhbGlkYXRlLCBWYWxpZGF0b3JzLm1heExlbmd0aCgxMDApXSldLFxyXG4gICAgICAnaG9tZVBob25lJzogWycnLCBWYWxpZGF0b3JzLm1heExlbmd0aCgxMDApXSxcclxuICAgICAgJ3dvcmtQaG9uZSc6IFsnJywgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTAwKV0sXHJcbiAgICAgICdnZW5kZXInOiBbJycsXSxcclxuICAgICAgJ2RhdGVPZkJpcnRoJzogWycnXSxcclxuICAgICAgJ25hdGlvbmFsaXR5JzogWycnLCBWYWxpZGF0b3JzLm1heExlbmd0aCg1MCldLFxyXG4gICAgICAncmVsYXRpb25zaGlwJzogWycnLCBWYWxpZGF0b3JzLmNvbXBvc2UoW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDUwKV0pXSxcclxuICAgICAgJ3N0cmVldEFkZHJlc3MnOiBbJycsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDIwMCldLFxyXG4gICAgICAnc3VidXJiJzogWycnLCBWYWxpZGF0b3JzLm1heExlbmd0aCg1MCldLFxyXG4gICAgICAncG9zdGNvZGUnOiBbJycsIFZhbGlkYXRvcnMucGF0dGVybignKFxcXFxkezR9KScpXSxcclxuICAgICAgJ3N0YXRlJzogWycnLCBWYWxpZGF0b3JzLm1heExlbmd0aCg1MCldLFxyXG4gICAgICAnY3JuJzogWycnLCBWYWxpZGF0b3JzLnBhdHRlcm4oJ1swLTldezl9W2EtekEtWl0nKV1cclxuICAgIH0pO1xyXG4gICAgdGhpcy50aXRsZSA9IHRoaXMuZm9ybS5jb250cm9sc1sndGl0bGUnXTtcclxuICAgIHRoaXMuZmlyc3RuYW1lID0gdGhpcy5mb3JtLmNvbnRyb2xzWydmaXJzdG5hbWUnXTtcclxuICAgIHRoaXMubGFzdG5hbWUgPSB0aGlzLmZvcm0uY29udHJvbHNbJ2xhc3RuYW1lJ107XHJcbiAgICB0aGlzLm1vYmlsZSA9IHRoaXMuZm9ybS5jb250cm9sc1snbW9iaWxlJ107XHJcbiAgICB0aGlzLmVtYWlsID0gdGhpcy5mb3JtLmNvbnRyb2xzWydlbWFpbCddO1xyXG4gICAgdGhpcy5ob21lUGhvbmUgPSB0aGlzLmZvcm0uY29udHJvbHNbJ2hvbWVQaG9uZSddO1xyXG4gICAgdGhpcy53b3JrUGhvbmUgPSB0aGlzLmZvcm0uY29udHJvbHNbJ3dvcmtQaG9uZSddO1xyXG4gICAgdGhpcy5nZW5kZXIgPSB0aGlzLmZvcm0uY29udHJvbHNbJ2dlbmRlciddO1xyXG4gICAgdGhpcy5kYXRlT2ZCaXJ0aCA9IHRoaXMuZm9ybS5jb250cm9sc1snZGF0ZU9mQmlydGgnXTtcclxuICAgIHRoaXMubmF0aW9uYWxpdHkgPSB0aGlzLmZvcm0uY29udHJvbHNbJ25hdGlvbmFsaXR5J107XHJcbiAgICB0aGlzLnJlbGF0aW9uc2hpcCA9IHRoaXMuZm9ybS5jb250cm9sc1sncmVsYXRpb25zaGlwJ107XHJcbiAgICB0aGlzLnN0cmVldEFkZHJlc3MgPSB0aGlzLmZvcm0uY29udHJvbHNbJ3N0cmVldEFkZHJlc3MnXTtcclxuICAgIHRoaXMuc3VidXJiID0gdGhpcy5mb3JtLmNvbnRyb2xzWydzdWJ1cmInXTtcclxuICAgIHRoaXMucG9zdGNvZGUgPSB0aGlzLmZvcm0uY29udHJvbHNbJ3Bvc3Rjb2RlJ107XHJcbiAgICB0aGlzLnN0YXRlID0gdGhpcy5mb3JtLmNvbnRyb2xzWydzdGF0ZSddO1xyXG4gICAgdGhpcy5jcm4gPSB0aGlzLmZvcm0uY29udHJvbHNbJ2NybiddO1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcblxyXG4gICAgdGhpcy5fcm91dGVcclxuICAgICAgLnF1ZXJ5UGFyYW1zXHJcbiAgICAgIC5tYXAoKHBhcmFtczogUGFyYW1zKSA9PiBwYXJhbXNbJ2d1YXJkaWFuSWQnXSlcclxuICAgICAgLmZsYXRNYXAoZ3VhcmRpYW5JZCA9PlxyXG4gICAgICAgIChndWFyZGlhbklkID09PSBudWxsIHx8IGd1YXJkaWFuSWQgPT09IHVuZGVmaW5lZCkgP1xyXG4gICAgICAgICAgT2JzZXJ2YWJsZS5vZihudWxsKSA6XHJcbiAgICAgICAgICB0aGlzLl9lbnJvbG1lbnRTZXJ2aWNlLmdldEd1YXJkaWFuJChndWFyZGlhbklkKSlcclxuICAgICAgLnN1YnNjcmliZShkdG8gPT4ge1xyXG5cclxuICAgICAgICBpZiAoKGR0byAhPT0gbnVsbCAmJiBkdG8gIT09IHVuZGVmaW5lZCkpIHtcclxuICAgICAgICAgIHRoaXMuZ3VhcmRpYW5EdG8gPSBkdG87XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRoaXMuX3JvdXRlXHJcbiAgICAgICAgICAgIC5xdWVyeVBhcmFtc1xyXG4gICAgICAgICAgICAubWFwKChwYXJhbXM6IFBhcmFtcykgPT4gcGFyYW1zWydlbnJvbG1lbnRGb3JtSWQnXSlcclxuICAgICAgICAgICAgLnN1YnNjcmliZShlbnJvbG1lbnRGb3JtSWQgPT4ge1xyXG4gICAgICAgICAgICAgIGlmIChlbnJvbG1lbnRGb3JtSWQgPT09IG51bGwgfHwgZW5yb2xtZW50Rm9ybUlkID09PSB1bmRlZmluZWQgfHwgZW5yb2xtZW50Rm9ybUlkID09PSBudWxsKVxyXG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdNdXN0IHByb3ZpZGUgZW5yb2xtZW50IGZvcm0gSWQgd2hlbiBjcmVhdGluZyBuZXcgZ3VhcmRpYW4nKTtcclxuICAgICAgICAgICAgICB0aGlzLmd1YXJkaWFuRHRvID0gbmV3IEd1YXJkaWFuRHRvKCk7XHJcbiAgICAgICAgICAgICAgdGhpcy5ndWFyZGlhbkR0by5pbml0KHtcclxuICAgICAgICAgICAgICAgIEVucm9sbWVudEZvcm1JZDogZW5yb2xtZW50Rm9ybUlkXHJcbiAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcblxyXG4gICAgT2JzZXJ2YWJsZVxyXG4gICAgICAuZm9ya0pvaW4oXHJcbiAgICAgIHRoaXMuX2Vucm9sbWVudFNlcnZpY2UuZ2V0UmVsYXRpb25zaGlwT3B0aW9ucyQoKSxcclxuICAgICAgdGhpcy5fZW5yb2xtZW50U2VydmljZS5nZXRUaXRsZXMkKCksXHJcbiAgICAgIHRoaXMuX2Vucm9sbWVudFNlcnZpY2UuZ2V0R2VuZGVycyQoKVxyXG4gICAgICApXHJcbiAgICAgIC5zdWJzY3JpYmUocmVzID0+IHtcclxuICAgICAgICB0aGlzLnJlbGF0aW9uc2hpcE9wdGlvbnMgPSB0aGlzLl9lbnJvbG1lbnRTZXJ2aWNlLmNvbnZlcnRFbnVtVG9BcnJheShyZXNbMF0pO1xyXG4gICAgICAgIHRoaXMudGl0bGVzID0gdGhpcy5fZW5yb2xtZW50U2VydmljZS5jb252ZXJ0RW51bVRvQXJyYXkocmVzWzFdKTtcclxuICAgICAgICB0aGlzLmdlbmRlcnMgPSB0aGlzLl9lbnJvbG1lbnRTZXJ2aWNlLmNvbnZlcnRFbnVtVG9BcnJheShyZXNbMl0pO1xyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIHNhdmVDaGFuZ2VzKCkge1xyXG4gICAgbGV0IHNhdmluZ1RvYXN0ID0gdGhpcy5fdG9hc3RlclNlcnZpY2UucG9wKG5ldyBTcGlubmluZ1RvYXN0KCkpO1xyXG4gICAgbGV0IHVwZGF0ZUd1YXJkaWFuID0gbmV3IENyZWF0ZVVwZGF0ZUd1YXJkaWFuRHRvKCk7XHJcbiAgICB1cGRhdGVHdWFyZGlhbi5pbml0KHRoaXMuZ3VhcmRpYW5EdG8udG9KU09OKCkpO1xyXG4gICAgdGhpcy5fZW5yb2xtZW50U2VydmljZVxyXG4gICAgICAuYWRkT3JVcGRhdGVHdWFyZGlhbiQodXBkYXRlR3VhcmRpYW4pXHJcbiAgICAgIC5zdWJzY3JpYmUocmVzID0+IHtcclxuICAgICAgICB0aGlzLmd1YXJkaWFuRHRvID0gcmVzO1xyXG4gICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLmNsZWFyKHNhdmluZ1RvYXN0LnRvYXN0SWQsIHNhdmluZ1RvYXN0LnRvYXN0Q29udGFpbmVySWQpO1xyXG4gICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLnBvcEFzeW5jKCdzdWNjZXNzJywgJ1NhdmVkJywgJycpO1xyXG4gICAgICAgIHRoaXMuX3JvdXRlci5uYXZpZ2F0ZShbJy4uL2Vucm9sbWVudC1mb3JtJ10sIHsgcmVsYXRpdmVUbzogdGhpcy5fcm91dGUgfSk7XHJcbiAgICAgIH0sXHJcbiAgICAgIChlcnJvcikgPT4ge1xyXG4gICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLmNsZWFyKHNhdmluZ1RvYXN0LnRvYXN0SWQsIHNhdmluZ1RvYXN0LnRvYXN0Q29udGFpbmVySWQpO1xyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIGNhbmNlbEVkaXQoKSB7XHJcbiAgICB0aGlzLl9yb3V0ZXIubmF2aWdhdGUoWycuLi9lbnJvbG1lbnQtZm9ybSddLCB7IHJlbGF0aXZlVG86IHRoaXMuX3JvdXRlIH0pO1xyXG4gIH1cclxufVxyXG4iXX0=
