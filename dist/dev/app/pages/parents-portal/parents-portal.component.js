"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var colorString = require('color-string');
var _ = require('lodash');
var angular2_toaster_1 = require('angular2-toaster');
var index_1 = require('../../shared/index');
var ParentsPortalComponent = (function () {
    function ParentsPortalComponent(_toasterService, _route, _router) {
        this._toasterService = _toasterService;
        this._route = _route;
        this._router = _router;
        if (index_1.Config.ENABLE_ENROLMENT_FEATURE !== 'true') {
            this._router.navigate(['/']);
        }
        this._toasterService.clear();
        if (index_1.Config.ENABLE_ENROLMENT_FEATURE !== 'true') {
            this._router.navigate(['/']);
        }
    }
    ParentsPortalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route
            .data
            .subscribe(function (data) {
            _this.primaryColour = data.centreBrandingDto.baseColour;
            var rgbVals = colorString.get.rgb(data.centreBrandingDto.baseColour);
            var rgbValsLight = _.clone(rgbVals);
            if (rgbValsLight.length === 4) {
                rgbValsLight[3] = 0.6;
            }
            _this.primaryColourLight = colorString.to.rgb(rgbValsLight);
            var rgbValsExtraLight = _.clone(rgbVals);
            if (rgbValsExtraLight.length === 4) {
                rgbValsExtraLight[3] = 0.3;
            }
            _this.primaryColourExtraLight = colorString.to.rgb(rgbValsExtraLight);
            _this.logo = data.centreLogo.imageBase64;
        });
    };
    ParentsPortalComponent.prototype.ngAfterViewChecked = function () {
        this.setPageHeaderStyle();
        this.setButtonPrimaryStyle();
        this.setBreadcrumbStyle();
        this.setLinkStyle();
        this.setProfileIconStyle();
    };
    ParentsPortalComponent.prototype.setPageHeaderStyle = function () {
        var className = 'page-header';
        this.setCssClassStyle(className, 'color', this.primaryColour);
    };
    ParentsPortalComponent.prototype.setButtonPrimaryStyle = function () {
        var className = 'btn-primary';
        this.setCssClassStyle(className, 'color', '#FFF');
        this.setCssClassStyle(className, 'background-color', this.primaryColour);
        this.setCssClassStyle(className, 'border-color', this.primaryColour);
    };
    ParentsPortalComponent.prototype.setBreadcrumbStyle = function () {
        var className = 'breadcrumb';
        this.setCssClassStyle(className, 'background-color', this.primaryColourLight);
    };
    ParentsPortalComponent.prototype.setLinkStyle = function () {
        var className = 'breadcrumb';
        var tagName = 'a';
        this.setTagStyle(className, tagName, 'color', this.primaryColour);
    };
    ParentsPortalComponent.prototype.setProfileIconStyle = function () {
        var className = 'nav-link';
        this.setCssClassStyle(className, 'color', this.primaryColourExtraLight);
    };
    ParentsPortalComponent.prototype.setCssClassStyle = function (className, property, color) {
        var elements = document.getElementsByClassName(className);
        for (var i = 0; i < elements.length; i++) {
            var elem = elements[i];
            var cssStyle = elem.style;
            cssStyle[property] = color;
        }
    };
    ParentsPortalComponent.prototype.setTagStyle = function (parentClass, tagName, property, color) {
        var elements = document.getElementsByClassName(parentClass);
        for (var i = 0; i < elements.length; ++i) {
            var childElements = elements[i].getElementsByTagName(tagName);
            for (var j = 0; j < childElements.length; ++j) {
                var childElement = childElements[j];
                var cssStyle = childElement.style;
                cssStyle[property] = color;
            }
        }
    };
    ParentsPortalComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            encapsulation: core_1.ViewEncapsulation.None,
            selector: 'parents-portal-cmp',
            templateUrl: 'parents-portal.component.html',
            styleUrls: ['parents-portal.component.css']
        }), 
        __metadata('design:paramtypes', [angular2_toaster_1.ToasterService, router_1.ActivatedRoute, router_1.Router])
    ], ParentsPortalComponent);
    return ParentsPortalComponent;
}());
exports.ParentsPortalComponent = ParentsPortalComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9wYXJlbnRzLXBvcnRhbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUF1RSxlQUFlLENBQUMsQ0FBQTtBQUN2Rix1QkFBdUMsaUJBQWlCLENBQUMsQ0FBQTtBQUN6RCxJQUFJLFdBQVcsR0FBUSxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUM7QUFDL0MsSUFBWSxDQUFDLFdBQU0sUUFBUSxDQUFDLENBQUE7QUFFNUIsaUNBQStCLGtCQUFrQixDQUFDLENBQUE7QUFFbEQsc0JBQXVCLG9CQUFvQixDQUFDLENBQUE7QUFTNUM7SUFPSSxnQ0FDWSxlQUErQixFQUMvQixNQUFzQixFQUN0QixPQUFlO1FBRmYsb0JBQWUsR0FBZixlQUFlLENBQWdCO1FBQy9CLFdBQU0sR0FBTixNQUFNLENBQWdCO1FBQ3RCLFlBQU8sR0FBUCxPQUFPLENBQVE7UUFFdkIsRUFBRSxDQUFDLENBQUMsY0FBTSxDQUFDLHdCQUF3QixLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDN0MsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQ2pDLENBQUM7UUFFRCxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBRTdCLEVBQUUsQ0FBQyxDQUFDLGNBQU0sQ0FBQyx3QkFBd0IsS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQzdDLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUNqQyxDQUFDO0lBQ0wsQ0FBQztJQUVELHlDQUFRLEdBQVI7UUFBQSxpQkFtQkM7UUFqQkcsSUFBSSxDQUFDLE1BQU07YUFDTixJQUFJO2FBQ0osU0FBUyxDQUFDLFVBQUMsSUFBeUU7WUFDakYsS0FBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDO1lBQ3ZELElBQUksT0FBTyxHQUFHLFdBQVcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUNyRSxJQUFJLFlBQVksR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3BDLEVBQUUsQ0FBQyxDQUFDLFlBQVksQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDNUIsWUFBWSxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQztZQUMxQixDQUFDO1lBQ0QsS0FBSSxDQUFDLGtCQUFrQixHQUFHLFdBQVcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQzNELElBQUksaUJBQWlCLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUN6QyxFQUFFLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDakMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO1lBQy9CLENBQUM7WUFDRCxLQUFJLENBQUMsdUJBQXVCLEdBQUcsV0FBVyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsaUJBQWlCLENBQUMsQ0FBQztZQUNyRSxLQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDO1FBQzVDLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVELG1EQUFrQixHQUFsQjtRQUNJLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBQzFCLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO1FBQzdCLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBQzFCLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUNwQixJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBRUQsbURBQWtCLEdBQWxCO1FBQ0ksSUFBTSxTQUFTLEdBQVcsYUFBYSxDQUFDO1FBQ3hDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUNsRSxDQUFDO0lBRUQsc0RBQXFCLEdBQXJCO1FBQ0ksSUFBTSxTQUFTLEdBQVcsYUFBYSxDQUFDO1FBQ3hDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLEVBQUUsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ2xELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLEVBQUUsa0JBQWtCLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ3pFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLEVBQUUsY0FBYyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUN6RSxDQUFDO0lBRUQsbURBQWtCLEdBQWxCO1FBQ0ksSUFBTSxTQUFTLEdBQVcsWUFBWSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLEVBQUUsa0JBQWtCLEVBQUUsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUM7SUFDbEYsQ0FBQztJQUVELDZDQUFZLEdBQVo7UUFDSSxJQUFNLFNBQVMsR0FBVyxZQUFZLENBQUM7UUFDdkMsSUFBTSxPQUFPLEdBQVcsR0FBRyxDQUFDO1FBQzVCLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO0lBQ3RFLENBQUM7SUFFRCxvREFBbUIsR0FBbkI7UUFDSSxJQUFNLFNBQVMsR0FBVyxVQUFVLENBQUM7UUFDckMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLHVCQUF1QixDQUFDLENBQUM7SUFDNUUsQ0FBQztJQUVELGlEQUFnQixHQUFoQixVQUFpQixTQUFpQixFQUFFLFFBQWdCLEVBQUUsS0FBYTtRQUMvRCxJQUFJLFFBQVEsR0FBRyxRQUFRLENBQUMsc0JBQXNCLENBQUMsU0FBUyxDQUFDLENBQUM7UUFFMUQsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQVcsQ0FBQyxFQUFFLENBQUMsR0FBRyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDL0MsSUFBSSxJQUFJLEdBQWdCLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNwQyxJQUFJLFFBQVEsR0FBUSxJQUFJLENBQUMsS0FBSyxDQUFDO1lBQy9CLFFBQVEsQ0FBQyxRQUFRLENBQUMsR0FBRyxLQUFLLENBQUM7UUFDL0IsQ0FBQztJQUNMLENBQUM7SUFFRCw0Q0FBVyxHQUFYLFVBQVksV0FBbUIsRUFBRSxPQUFlLEVBQUUsUUFBZ0IsRUFBRSxLQUFhO1FBQzdFLElBQUksUUFBUSxHQUFHLFFBQVEsQ0FBQyxzQkFBc0IsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUU1RCxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBVyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQztZQUMvQyxJQUFJLGFBQWEsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsb0JBQW9CLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDOUQsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQVcsQ0FBQyxFQUFFLENBQUMsR0FBRyxhQUFhLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUM7Z0JBQ3BELElBQUksWUFBWSxHQUFnQixhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pELElBQUksUUFBUSxHQUFRLFlBQVksQ0FBQyxLQUFLLENBQUM7Z0JBQ3ZDLFFBQVEsQ0FBQyxRQUFRLENBQUMsR0FBRyxLQUFLLENBQUM7WUFDL0IsQ0FBQztRQUNMLENBQUM7SUFDTCxDQUFDO0lBNUdMO1FBQUMsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixhQUFhLEVBQUUsd0JBQWlCLENBQUMsSUFBSTtZQUNyQyxRQUFRLEVBQUUsb0JBQW9CO1lBQzlCLFdBQVcsRUFBRSwrQkFBK0I7WUFDNUMsU0FBUyxFQUFFLENBQUMsOEJBQThCLENBQUM7U0FDOUMsQ0FBQzs7OEJBQUE7SUF1R0YsNkJBQUM7QUFBRCxDQXRHQSxBQXNHQyxJQUFBO0FBdEdZLDhCQUFzQix5QkFzR2xDLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL3BhcmVudHMtcG9ydGFsL3BhcmVudHMtcG9ydGFsLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgVmlld0VuY2Fwc3VsYXRpb24sIE9uSW5pdCwgQWZ0ZXJWaWV3Q2hlY2tlZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZSwgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxubGV0IGNvbG9yU3RyaW5nOiBhbnkgPSByZXF1aXJlKCdjb2xvci1zdHJpbmcnKTtcclxuaW1wb3J0ICogYXMgXyBmcm9tICdsb2Rhc2gnO1xyXG5cclxuaW1wb3J0IHsgVG9hc3RlclNlcnZpY2UgfSBmcm9tICdhbmd1bGFyMi10b2FzdGVyJztcclxuaW1wb3J0IHsgQ2VudHJlQnJhbmRpbmdEdG8sIENlbnRyZUxvZ29EdG8gfSBmcm9tICcuLi8uLi9hcGkvaW5kZXgnO1xyXG5pbXBvcnQgeyBDb25maWcgfSBmcm9tICcuLi8uLi9zaGFyZWQvaW5kZXgnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcclxuICAgIHNlbGVjdG9yOiAncGFyZW50cy1wb3J0YWwtY21wJyxcclxuICAgIHRlbXBsYXRlVXJsOiAncGFyZW50cy1wb3J0YWwuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJ3BhcmVudHMtcG9ydGFsLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgUGFyZW50c1BvcnRhbENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgQWZ0ZXJWaWV3Q2hlY2tlZCB7XHJcblxyXG4gICAgcHJpdmF0ZSBwcmltYXJ5Q29sb3VyOiBzdHJpbmc7XHJcbiAgICBwcml2YXRlIHByaW1hcnlDb2xvdXJMaWdodDogc3RyaW5nO1xyXG4gICAgcHJpdmF0ZSBwcmltYXJ5Q29sb3VyRXh0cmFMaWdodDogc3RyaW5nO1xyXG4gICAgcHJpdmF0ZSBsb2dvOiBzdHJpbmc7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSBfdG9hc3RlclNlcnZpY2U6IFRvYXN0ZXJTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgX3JvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcclxuICAgICAgICBwcml2YXRlIF9yb3V0ZXI6IFJvdXRlcikge1xyXG5cclxuICAgICAgICBpZiAoQ29uZmlnLkVOQUJMRV9FTlJPTE1FTlRfRkVBVFVSRSAhPT0gJ3RydWUnKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3JvdXRlci5uYXZpZ2F0ZShbJy8nXSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcigpO1xyXG5cclxuICAgICAgICBpZiAoQ29uZmlnLkVOQUJMRV9FTlJPTE1FTlRfRkVBVFVSRSAhPT0gJ3RydWUnKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3JvdXRlci5uYXZpZ2F0ZShbJy8nXSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xyXG5cclxuICAgICAgICB0aGlzLl9yb3V0ZVxyXG4gICAgICAgICAgICAuZGF0YVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKChkYXRhOiB7IGNlbnRyZUJyYW5kaW5nRHRvOiBDZW50cmVCcmFuZGluZ0R0bywgY2VudHJlTG9nbzogQ2VudHJlTG9nb0R0byB9KSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnByaW1hcnlDb2xvdXIgPSBkYXRhLmNlbnRyZUJyYW5kaW5nRHRvLmJhc2VDb2xvdXI7XHJcbiAgICAgICAgICAgICAgICBsZXQgcmdiVmFscyA9IGNvbG9yU3RyaW5nLmdldC5yZ2IoZGF0YS5jZW50cmVCcmFuZGluZ0R0by5iYXNlQ29sb3VyKTtcclxuICAgICAgICAgICAgICAgIGxldCByZ2JWYWxzTGlnaHQgPSBfLmNsb25lKHJnYlZhbHMpO1xyXG4gICAgICAgICAgICAgICAgaWYgKHJnYlZhbHNMaWdodC5sZW5ndGggPT09IDQpIHtcclxuICAgICAgICAgICAgICAgICAgICByZ2JWYWxzTGlnaHRbM10gPSAwLjY7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB0aGlzLnByaW1hcnlDb2xvdXJMaWdodCA9IGNvbG9yU3RyaW5nLnRvLnJnYihyZ2JWYWxzTGlnaHQpO1xyXG4gICAgICAgICAgICAgICAgbGV0IHJnYlZhbHNFeHRyYUxpZ2h0ID0gXy5jbG9uZShyZ2JWYWxzKTtcclxuICAgICAgICAgICAgICAgIGlmIChyZ2JWYWxzRXh0cmFMaWdodC5sZW5ndGggPT09IDQpIHtcclxuICAgICAgICAgICAgICAgICAgICByZ2JWYWxzRXh0cmFMaWdodFszXSA9IDAuMztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRoaXMucHJpbWFyeUNvbG91ckV4dHJhTGlnaHQgPSBjb2xvclN0cmluZy50by5yZ2IocmdiVmFsc0V4dHJhTGlnaHQpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sb2dvID0gZGF0YS5jZW50cmVMb2dvLmltYWdlQmFzZTY0O1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBuZ0FmdGVyVmlld0NoZWNrZWQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5zZXRQYWdlSGVhZGVyU3R5bGUoKTtcclxuICAgICAgICB0aGlzLnNldEJ1dHRvblByaW1hcnlTdHlsZSgpO1xyXG4gICAgICAgIHRoaXMuc2V0QnJlYWRjcnVtYlN0eWxlKCk7XHJcbiAgICAgICAgdGhpcy5zZXRMaW5rU3R5bGUoKTtcclxuICAgICAgICB0aGlzLnNldFByb2ZpbGVJY29uU3R5bGUoKTtcclxuICAgIH1cclxuXHJcbiAgICBzZXRQYWdlSGVhZGVyU3R5bGUoKTogdm9pZCB7XHJcbiAgICAgICAgY29uc3QgY2xhc3NOYW1lOiBzdHJpbmcgPSAncGFnZS1oZWFkZXInO1xyXG4gICAgICAgIHRoaXMuc2V0Q3NzQ2xhc3NTdHlsZShjbGFzc05hbWUsICdjb2xvcicsIHRoaXMucHJpbWFyeUNvbG91cik7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0QnV0dG9uUHJpbWFyeVN0eWxlKCk6IHZvaWQge1xyXG4gICAgICAgIGNvbnN0IGNsYXNzTmFtZTogc3RyaW5nID0gJ2J0bi1wcmltYXJ5JztcclxuICAgICAgICB0aGlzLnNldENzc0NsYXNzU3R5bGUoY2xhc3NOYW1lLCAnY29sb3InLCAnI0ZGRicpO1xyXG4gICAgICAgIHRoaXMuc2V0Q3NzQ2xhc3NTdHlsZShjbGFzc05hbWUsICdiYWNrZ3JvdW5kLWNvbG9yJywgdGhpcy5wcmltYXJ5Q29sb3VyKTtcclxuICAgICAgICB0aGlzLnNldENzc0NsYXNzU3R5bGUoY2xhc3NOYW1lLCAnYm9yZGVyLWNvbG9yJywgdGhpcy5wcmltYXJ5Q29sb3VyKTtcclxuICAgIH1cclxuXHJcbiAgICBzZXRCcmVhZGNydW1iU3R5bGUoKTogdm9pZCB7XHJcbiAgICAgICAgY29uc3QgY2xhc3NOYW1lOiBzdHJpbmcgPSAnYnJlYWRjcnVtYic7XHJcbiAgICAgICAgdGhpcy5zZXRDc3NDbGFzc1N0eWxlKGNsYXNzTmFtZSwgJ2JhY2tncm91bmQtY29sb3InLCB0aGlzLnByaW1hcnlDb2xvdXJMaWdodCk7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0TGlua1N0eWxlKCk6IHZvaWQge1xyXG4gICAgICAgIGNvbnN0IGNsYXNzTmFtZTogc3RyaW5nID0gJ2JyZWFkY3J1bWInO1xyXG4gICAgICAgIGNvbnN0IHRhZ05hbWU6IHN0cmluZyA9ICdhJztcclxuICAgICAgICB0aGlzLnNldFRhZ1N0eWxlKGNsYXNzTmFtZSwgdGFnTmFtZSwgJ2NvbG9yJywgdGhpcy5wcmltYXJ5Q29sb3VyKTtcclxuICAgIH1cclxuXHJcbiAgICBzZXRQcm9maWxlSWNvblN0eWxlKCk6IHZvaWQge1xyXG4gICAgICAgIGNvbnN0IGNsYXNzTmFtZTogc3RyaW5nID0gJ25hdi1saW5rJztcclxuICAgICAgICB0aGlzLnNldENzc0NsYXNzU3R5bGUoY2xhc3NOYW1lLCAnY29sb3InLCB0aGlzLnByaW1hcnlDb2xvdXJFeHRyYUxpZ2h0KTtcclxuICAgIH1cclxuXHJcbiAgICBzZXRDc3NDbGFzc1N0eWxlKGNsYXNzTmFtZTogc3RyaW5nLCBwcm9wZXJ0eTogc3RyaW5nLCBjb2xvcjogc3RyaW5nKSB7XHJcbiAgICAgICAgdmFyIGVsZW1lbnRzID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZShjbGFzc05hbWUpO1xyXG5cclxuICAgICAgICBmb3IgKGxldCBpOiBudW1iZXIgPSAwOyBpIDwgZWxlbWVudHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgbGV0IGVsZW0gPSA8SFRNTEVsZW1lbnQ+ZWxlbWVudHNbaV07XHJcbiAgICAgICAgICAgIGxldCBjc3NTdHlsZTogYW55ID0gZWxlbS5zdHlsZTtcclxuICAgICAgICAgICAgY3NzU3R5bGVbcHJvcGVydHldID0gY29sb3I7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHNldFRhZ1N0eWxlKHBhcmVudENsYXNzOiBzdHJpbmcsIHRhZ05hbWU6IHN0cmluZywgcHJvcGVydHk6IHN0cmluZywgY29sb3I6IHN0cmluZykge1xyXG4gICAgICAgIHZhciBlbGVtZW50cyA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUocGFyZW50Q2xhc3MpO1xyXG5cclxuICAgICAgICBmb3IgKGxldCBpOiBudW1iZXIgPSAwOyBpIDwgZWxlbWVudHMubGVuZ3RoOyArK2kpIHtcclxuICAgICAgICAgICAgbGV0IGNoaWxkRWxlbWVudHMgPSBlbGVtZW50c1tpXS5nZXRFbGVtZW50c0J5VGFnTmFtZSh0YWdOYW1lKTtcclxuICAgICAgICAgICAgZm9yIChsZXQgajogbnVtYmVyID0gMDsgaiA8IGNoaWxkRWxlbWVudHMubGVuZ3RoOyArK2opIHtcclxuICAgICAgICAgICAgICAgIGxldCBjaGlsZEVsZW1lbnQgPSA8SFRNTEVsZW1lbnQ+Y2hpbGRFbGVtZW50c1tqXTtcclxuICAgICAgICAgICAgICAgIGxldCBjc3NTdHlsZTogYW55ID0gY2hpbGRFbGVtZW50LnN0eWxlO1xyXG4gICAgICAgICAgICAgICAgY3NzU3R5bGVbcHJvcGVydHldID0gY29sb3I7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19
