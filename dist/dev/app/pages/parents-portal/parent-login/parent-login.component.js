"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var index_1 = require('../../../shared/index');
var ParentLoginComponent = (function () {
    function ParentLoginComponent(_router, _route) {
        this._router = _router;
        this._route = _route;
        this.logo = null;
        if (index_1.Config.ENABLE_ENROLMENT_FEATURE !== 'true') {
            this._router.navigate(['/']);
        }
    }
    ParentLoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route
            .data
            .subscribe(function (data) {
            if (!data.centreExists) {
                var centreName = _this._route.snapshot.params['centreName'];
                _this._router.navigate(['centre-not-found', centreName]);
            }
            if (data.centreLogo.imageBase64) {
                _this.logo = data.centreLogo.imageBase64;
            }
        });
    };
    ParentLoginComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'parent-login-cmp',
            templateUrl: 'parent-login.component.html',
            styleUrls: ['parent-login.component.css']
        }), 
        __metadata('design:paramtypes', [router_1.Router, router_1.ActivatedRoute])
    ], ParentLoginComponent);
    return ParentLoginComponent;
}());
exports.ParentLoginComponent = ParentLoginComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9wYXJlbnQtbG9naW4vcGFyZW50LWxvZ2luLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQWtDLGVBQWUsQ0FBQyxDQUFBO0FBQ2xELHVCQUF1QyxpQkFBaUIsQ0FBQyxDQUFBO0FBR3pELHNCQUF1Qix1QkFBdUIsQ0FBQyxDQUFBO0FBUy9DO0lBSUMsOEJBQ1MsT0FBZSxFQUNmLE1BQXNCO1FBRHRCLFlBQU8sR0FBUCxPQUFPLENBQVE7UUFDZixXQUFNLEdBQU4sTUFBTSxDQUFnQjtRQUp2QixTQUFJLEdBQVcsSUFBSSxDQUFDO1FBTTNCLEVBQUUsQ0FBQyxDQUFDLGNBQU0sQ0FBQyx3QkFBd0IsS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ2hELElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUM5QixDQUFDO0lBQ0YsQ0FBQztJQUVELHVDQUFRLEdBQVI7UUFBQSxpQkFlQztRQWJBLElBQUksQ0FBQyxNQUFNO2FBQ1QsSUFBSTthQUNKLFNBQVMsQ0FBQyxVQUFDLElBQTBEO1lBRXJFLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7Z0JBQ3hCLElBQUksVUFBVSxHQUFHLEtBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDM0QsS0FBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxrQkFBa0IsRUFBRSxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQ3pELENBQUM7WUFFRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pDLEtBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUM7WUFDekMsQ0FBQztRQUNGLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQW5DRjtRQUFDLGdCQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLGtCQUFrQjtZQUM1QixXQUFXLEVBQUUsNkJBQTZCO1lBQzFDLFNBQVMsRUFBRSxDQUFDLDRCQUE0QixDQUFDO1NBQ3pDLENBQUM7OzRCQUFBO0lBK0JGLDJCQUFDO0FBQUQsQ0E3QkEsQUE2QkMsSUFBQTtBQTdCWSw0QkFBb0IsdUJBNkJoQyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9wYXJlbnQtbG9naW4vcGFyZW50LWxvZ2luLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlLCBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5cclxuaW1wb3J0IHsgQ2VudHJlTG9nb0R0byB9IGZyb20gJy4uLy4uLy4uL2FwaS9pbmRleCc7XHJcbmltcG9ydCB7IENvbmZpZyB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9pbmRleCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuXHRtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG5cdHNlbGVjdG9yOiAncGFyZW50LWxvZ2luLWNtcCcsXHJcblx0dGVtcGxhdGVVcmw6ICdwYXJlbnQtbG9naW4uY29tcG9uZW50Lmh0bWwnLFxyXG5cdHN0eWxlVXJsczogWydwYXJlbnQtbG9naW4uY29tcG9uZW50LmNzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgUGFyZW50TG9naW5Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuXHRwcml2YXRlIGxvZ286IHN0cmluZyA9IG51bGw7XHJcblxyXG5cdGNvbnN0cnVjdG9yKFxyXG5cdFx0cHJpdmF0ZSBfcm91dGVyOiBSb3V0ZXIsXHJcblx0XHRwcml2YXRlIF9yb3V0ZTogQWN0aXZhdGVkUm91dGUpIHtcclxuXHJcblx0XHRpZiAoQ29uZmlnLkVOQUJMRV9FTlJPTE1FTlRfRkVBVFVSRSAhPT0gJ3RydWUnKSB7XHJcblx0XHRcdHRoaXMuX3JvdXRlci5uYXZpZ2F0ZShbJy8nXSk7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRuZ09uSW5pdCgpOiB2b2lkIHtcclxuXHJcblx0XHR0aGlzLl9yb3V0ZVxyXG5cdFx0XHQuZGF0YVxyXG5cdFx0XHQuc3Vic2NyaWJlKChkYXRhOiB7IGNlbnRyZUV4aXN0czogYm9vbGVhbiwgY2VudHJlTG9nbzogQ2VudHJlTG9nb0R0byB9KSA9PiB7XHJcblxyXG5cdFx0XHRcdGlmICghZGF0YS5jZW50cmVFeGlzdHMpIHtcclxuXHRcdFx0XHRcdGxldCBjZW50cmVOYW1lID0gdGhpcy5fcm91dGUuc25hcHNob3QucGFyYW1zWydjZW50cmVOYW1lJ107XHJcblx0XHRcdFx0XHR0aGlzLl9yb3V0ZXIubmF2aWdhdGUoWydjZW50cmUtbm90LWZvdW5kJywgY2VudHJlTmFtZV0pO1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0aWYgKGRhdGEuY2VudHJlTG9nby5pbWFnZUJhc2U2NCkge1xyXG5cdFx0XHRcdFx0dGhpcy5sb2dvID0gZGF0YS5jZW50cmVMb2dvLmltYWdlQmFzZTY0O1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSk7XHJcblx0fVxyXG59XHJcbiJdfQ==
