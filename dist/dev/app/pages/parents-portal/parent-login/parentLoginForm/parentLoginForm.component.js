"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var router_1 = require('@angular/router');
var index_1 = require('../index');
var angular2_toaster_1 = require('angular2-toaster');
var index_2 = require('../../../../shared/index');
var ParentLoginFormComponent = (function () {
    function ParentLoginFormComponent(fb, _toasterService, _loginService, _route, _router) {
        this._toasterService = _toasterService;
        this._loginService = _loginService;
        this._route = _route;
        this._router = _router;
        this.submitted = false;
        this.centreName = null;
        this.form = fb.group({
            'email': ['', forms_1.Validators.required],
            'password': ['', forms_1.Validators.required]
        });
        this.email = this.form.controls['email'];
        this.password = this.form.controls['password'];
    }
    ParentLoginFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route.params
            .subscribe(function (params) {
            _this.centreName = params['centreName'];
        });
    };
    ParentLoginFormComponent.prototype.onSubmit = function (values) {
        var _this = this;
        this.submitted = true;
        var authenticatingToast = this._toasterService.pop(new index_2.SpinningToast('Authenticating'));
        this._loginService
            .login$(this.email.value, this.password.value)
            .subscribe(function (res) {
            _this._router.navigate(['parents-portal', _this.centreName]);
        }, function (err) {
            _this._toasterService.clear(authenticatingToast.toastId, authenticatingToast.toastContainerId);
            _this.submitted = false;
        }, function () {
            _this.submitted = false;
        });
    };
    ;
    ParentLoginFormComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'parent-login-form-cmp',
            styleUrls: ['parentLoginForm.component.css'],
            templateUrl: 'parentLoginForm.component.html'
        }),
        core_1.Injectable(), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, angular2_toaster_1.ToasterService, index_1.ParentLoginService, router_1.ActivatedRoute, router_1.Router])
    ], ParentLoginFormComponent);
    return ParentLoginFormComponent;
}());
exports.ParentLoginFormComponent = ParentLoginFormComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9wYXJlbnQtbG9naW4vcGFyZW50TG9naW5Gb3JtL3BhcmVudExvZ2luRm9ybS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUE4QyxlQUFlLENBQUMsQ0FBQTtBQUM5RCxzQkFBb0UsZ0JBQWdCLENBQUMsQ0FBQTtBQUNyRix1QkFBK0MsaUJBQWlCLENBQUMsQ0FBQTtBQUVqRSxzQkFBbUMsVUFBVSxDQUFDLENBQUE7QUFDOUMsaUNBQStCLGtCQUFrQixDQUFDLENBQUE7QUFDbEQsc0JBQThCLDBCQUEwQixDQUFDLENBQUE7QUFVekQ7SUFRSSxrQ0FBWSxFQUFlLEVBQ2YsZUFBK0IsRUFDL0IsYUFBaUMsRUFDakMsTUFBc0IsRUFDdEIsT0FBZTtRQUhmLG9CQUFlLEdBQWYsZUFBZSxDQUFnQjtRQUMvQixrQkFBYSxHQUFiLGFBQWEsQ0FBb0I7UUFDakMsV0FBTSxHQUFOLE1BQU0sQ0FBZ0I7UUFDdEIsWUFBTyxHQUFQLE9BQU8sQ0FBUTtRQVBwQixjQUFTLEdBQVksS0FBSyxDQUFDO1FBQzFCLGVBQVUsR0FBVyxJQUFJLENBQUM7UUFROUIsSUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDO1lBQ2pCLE9BQU8sRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLFFBQVEsQ0FBQztZQUNsQyxVQUFVLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxRQUFRLENBQUM7U0FDeEMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ25ELENBQUM7SUFFRCwyQ0FBUSxHQUFSO1FBQUEsaUJBS0M7UUFKRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU07YUFDYixTQUFTLENBQUMsVUFBQyxNQUFjO1lBQ3RCLEtBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQzNDLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVELDJDQUFRLEdBQVIsVUFBUyxNQUFXO1FBQXBCLGlCQWdCQztRQWZHLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLElBQUksbUJBQW1CLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsSUFBSSxxQkFBYSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQztRQUV4RixJQUFJLENBQUMsYUFBYTthQUNiLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQzthQUM3QyxTQUFTLENBQUMsVUFBQSxHQUFHO1lBQ1YsS0FBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxnQkFBZ0IsRUFBRSxLQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztRQUMvRCxDQUFDLEVBQ0QsVUFBQSxHQUFHO1lBQ0MsS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsbUJBQW1CLENBQUMsT0FBTyxFQUFFLG1CQUFtQixDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDOUYsS0FBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7UUFDM0IsQ0FBQyxFQUNEO1lBQ0ksS0FBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7UUFDM0IsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDOztJQXRETDtRQUFDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLHVCQUF1QjtZQUNqQyxTQUFTLEVBQUUsQ0FBQywrQkFBK0IsQ0FBQztZQUM1QyxXQUFXLEVBQUUsZ0NBQWdDO1NBQ2hELENBQUM7UUFFRCxpQkFBVSxFQUFFOztnQ0FBQTtJQWdEYiwrQkFBQztBQUFELENBL0NBLEFBK0NDLElBQUE7QUEvQ1ksZ0NBQXdCLDJCQStDcEMsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvcGFyZW50cy1wb3J0YWwvcGFyZW50LWxvZ2luL3BhcmVudExvZ2luRm9ybS9wYXJlbnRMb2dpbkZvcm0uY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSwgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybUdyb3VwLCBBYnN0cmFjdENvbnRyb2wsIEZvcm1CdWlsZGVyLCBWYWxpZGF0b3JzIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBSb3V0ZXIsIEFjdGl2YXRlZFJvdXRlLCBQYXJhbXMgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5cclxuaW1wb3J0IHsgUGFyZW50TG9naW5TZXJ2aWNlIH0gZnJvbSAnLi4vaW5kZXgnO1xyXG5pbXBvcnQgeyBUb2FzdGVyU2VydmljZSB9IGZyb20gJ2FuZ3VsYXIyLXRvYXN0ZXInO1xyXG5pbXBvcnQgeyBTcGlubmluZ1RvYXN0IH0gZnJvbSAnLi4vLi4vLi4vLi4vc2hhcmVkL2luZGV4JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHNlbGVjdG9yOiAncGFyZW50LWxvZ2luLWZvcm0tY21wJyxcclxuICAgIHN0eWxlVXJsczogWydwYXJlbnRMb2dpbkZvcm0uY29tcG9uZW50LmNzcyddLFxyXG4gICAgdGVtcGxhdGVVcmw6ICdwYXJlbnRMb2dpbkZvcm0uY29tcG9uZW50Lmh0bWwnXHJcbn0pXHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBQYXJlbnRMb2dpbkZvcm1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gICAgcHVibGljIGZvcm06IEZvcm1Hcm91cDtcclxuICAgIHB1YmxpYyBlbWFpbDogQWJzdHJhY3RDb250cm9sO1xyXG4gICAgcHVibGljIHBhc3N3b3JkOiBBYnN0cmFjdENvbnRyb2w7XHJcblxyXG4gICAgcHVibGljIHN1Ym1pdHRlZDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgcHJpdmF0ZSBjZW50cmVOYW1lOiBzdHJpbmcgPSBudWxsO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGZiOiBGb3JtQnVpbGRlcixcclxuICAgICAgICBwcml2YXRlIF90b2FzdGVyU2VydmljZTogVG9hc3RlclNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBfbG9naW5TZXJ2aWNlOiBQYXJlbnRMb2dpblNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBfcm91dGU6IEFjdGl2YXRlZFJvdXRlLFxyXG4gICAgICAgIHByaXZhdGUgX3JvdXRlcjogUm91dGVyKSB7XHJcblxyXG4gICAgICAgIHRoaXMuZm9ybSA9IGZiLmdyb3VwKHtcclxuICAgICAgICAgICAgJ2VtYWlsJzogWycnLCBWYWxpZGF0b3JzLnJlcXVpcmVkXSxcclxuICAgICAgICAgICAgJ3Bhc3N3b3JkJzogWycnLCBWYWxpZGF0b3JzLnJlcXVpcmVkXVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICB0aGlzLmVtYWlsID0gdGhpcy5mb3JtLmNvbnRyb2xzWydlbWFpbCddO1xyXG4gICAgICAgIHRoaXMucGFzc3dvcmQgPSB0aGlzLmZvcm0uY29udHJvbHNbJ3Bhc3N3b3JkJ107XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5fcm91dGUucGFyYW1zXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoKHBhcmFtczogUGFyYW1zKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNlbnRyZU5hbWUgPSBwYXJhbXNbJ2NlbnRyZU5hbWUnXTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgb25TdWJtaXQodmFsdWVzOiBhbnkpIHtcclxuICAgICAgICB0aGlzLnN1Ym1pdHRlZCA9IHRydWU7XHJcbiAgICAgICAgdmFyIGF1dGhlbnRpY2F0aW5nVG9hc3QgPSB0aGlzLl90b2FzdGVyU2VydmljZS5wb3AobmV3IFNwaW5uaW5nVG9hc3QoJ0F1dGhlbnRpY2F0aW5nJykpO1xyXG5cclxuICAgICAgICB0aGlzLl9sb2dpblNlcnZpY2VcclxuICAgICAgICAgICAgLmxvZ2luJCh0aGlzLmVtYWlsLnZhbHVlLCB0aGlzLnBhc3N3b3JkLnZhbHVlKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKHJlcyA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9yb3V0ZXIubmF2aWdhdGUoWydwYXJlbnRzLXBvcnRhbCcsIHRoaXMuY2VudHJlTmFtZV0pO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBlcnIgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoYXV0aGVudGljYXRpbmdUb2FzdC50b2FzdElkLCBhdXRoZW50aWNhdGluZ1RvYXN0LnRvYXN0Q29udGFpbmVySWQpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zdWJtaXR0ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zdWJtaXR0ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9O1xyXG59XHJcbiJdfQ==
