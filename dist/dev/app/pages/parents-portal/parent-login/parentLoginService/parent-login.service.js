"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../../core/index');
var index_2 = require('../../../../shared/index');
var index_3 = require('../../../../api/index');
var ParentLoginService = (function () {
    function ParentLoginService(_authenticationService, _apiService) {
        this._authenticationService = _authenticationService;
        this._apiService = _apiService;
    }
    ParentLoginService.prototype.login$ = function (username, password) {
        var _this = this;
        return this._apiService
            .token('password', username, password, index_2.Config.CLIENT_ID)
            .map(function (res) {
            _this._authenticationService.saveAccessToken(res.value, true);
        });
    };
    ParentLoginService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.AccessTokenService, index_3.ApiClientService])
    ], ParentLoginService);
    return ParentLoginService;
}());
exports.ParentLoginService = ParentLoginService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9wYXJlbnQtbG9naW4vcGFyZW50TG9naW5TZXJ2aWNlL3BhcmVudC1sb2dpbi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBMkIsZUFBZSxDQUFDLENBQUE7QUFHM0Msc0JBQW1DLHdCQUF3QixDQUFDLENBQUE7QUFDNUQsc0JBQXVCLDBCQUEwQixDQUFDLENBQUE7QUFDbEQsc0JBQWlDLHVCQUF1QixDQUFDLENBQUE7QUFHekQ7SUFFSSw0QkFDWSxzQkFBMEMsRUFDMUMsV0FBNkI7UUFEN0IsMkJBQXNCLEdBQXRCLHNCQUFzQixDQUFvQjtRQUMxQyxnQkFBVyxHQUFYLFdBQVcsQ0FBa0I7SUFDekMsQ0FBQztJQUVNLG1DQUFNLEdBQWIsVUFBYyxRQUFnQixFQUFFLFFBQWdCO1FBQWhELGlCQU1DO1FBTEcsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXO2FBQ2xCLEtBQUssQ0FBQyxVQUFVLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxjQUFNLENBQUMsU0FBUyxDQUFDO2FBQ3ZELEdBQUcsQ0FBQyxVQUFBLEdBQUc7WUFDSixLQUFJLENBQUMsc0JBQXNCLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDakUsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBZEw7UUFBQyxpQkFBVSxFQUFFOzswQkFBQTtJQWViLHlCQUFDO0FBQUQsQ0FkQSxBQWNDLElBQUE7QUFkWSwwQkFBa0IscUJBYzlCLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL3BhcmVudHMtcG9ydGFsL3BhcmVudC1sb2dpbi9wYXJlbnRMb2dpblNlcnZpY2UvcGFyZW50LWxvZ2luLnNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuXHJcbmltcG9ydCB7IEFjY2Vzc1Rva2VuU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uL2NvcmUvaW5kZXgnO1xyXG5pbXBvcnQgeyBDb25maWcgfSBmcm9tICcuLi8uLi8uLi8uLi9zaGFyZWQvaW5kZXgnO1xyXG5pbXBvcnQgeyBBcGlDbGllbnRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vYXBpL2luZGV4JztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIFBhcmVudExvZ2luU2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSBfYXV0aGVudGljYXRpb25TZXJ2aWNlOiBBY2Nlc3NUb2tlblNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBfYXBpU2VydmljZTogQXBpQ2xpZW50U2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBsb2dpbiQodXNlcm5hbWU6IHN0cmluZywgcGFzc3dvcmQ6IHN0cmluZyk6IE9ic2VydmFibGU8dm9pZD4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9hcGlTZXJ2aWNlXHJcbiAgICAgICAgICAgIC50b2tlbigncGFzc3dvcmQnLCB1c2VybmFtZSwgcGFzc3dvcmQsIENvbmZpZy5DTElFTlRfSUQpXHJcbiAgICAgICAgICAgIC5tYXAocmVzID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2F1dGhlbnRpY2F0aW9uU2VydmljZS5zYXZlQWNjZXNzVG9rZW4ocmVzLnZhbHVlLCB0cnVlKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcbn1cclxuIl19
