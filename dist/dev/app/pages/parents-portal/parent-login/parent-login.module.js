"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../shared/index');
var index_2 = require('./index');
var ParentLoginModule = (function () {
    function ParentLoginModule() {
    }
    ParentLoginModule = __decorate([
        core_1.NgModule({
            imports: [index_1.SharedModule],
            declarations: [index_2.ParentLoginComponent, index_2.ParentLoginFormComponent],
            providers: [index_2.ParentLoginService]
        }), 
        __metadata('design:paramtypes', [])
    ], ParentLoginModule);
    return ParentLoginModule;
}());
exports.ParentLoginModule = ParentLoginModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9wYXJlbnQtbG9naW4vcGFyZW50LWxvZ2luLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQXlCLGVBQWUsQ0FBQyxDQUFBO0FBQ3pDLHNCQUE2Qix1QkFBdUIsQ0FBQyxDQUFBO0FBRXJELHNCQUdPLFNBQVMsQ0FBQyxDQUFBO0FBUWpCO0lBQUE7SUFBaUMsQ0FBQztJQU5sQztRQUFDLGVBQVEsQ0FBQztZQUNOLE9BQU8sRUFBRSxDQUFDLG9CQUFZLENBQUM7WUFDdkIsWUFBWSxFQUFFLENBQUMsNEJBQW9CLEVBQUUsZ0NBQXdCLENBQUM7WUFDOUQsU0FBUyxFQUFFLENBQUMsMEJBQWtCLENBQUM7U0FDbEMsQ0FBQzs7eUJBQUE7SUFFK0Isd0JBQUM7QUFBRCxDQUFqQyxBQUFrQyxJQUFBO0FBQXJCLHlCQUFpQixvQkFBSSxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9wYXJlbnQtbG9naW4vcGFyZW50LWxvZ2luLm1vZHVsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFNoYXJlZE1vZHVsZSB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9pbmRleCc7XHJcblxyXG5pbXBvcnQge1xyXG4gICAgUGFyZW50TG9naW5Db21wb25lbnQsIFBhcmVudExvZ2luRm9ybUNvbXBvbmVudCxcclxuICAgIFBhcmVudExvZ2luU2VydmljZVxyXG59IGZyb20gJy4vaW5kZXgnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6IFtTaGFyZWRNb2R1bGVdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBbUGFyZW50TG9naW5Db21wb25lbnQsIFBhcmVudExvZ2luRm9ybUNvbXBvbmVudF0sXHJcbiAgICBwcm92aWRlcnM6IFtQYXJlbnRMb2dpblNlcnZpY2VdXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgUGFyZW50TG9naW5Nb2R1bGUgeyB9XHJcbiJdfQ==
