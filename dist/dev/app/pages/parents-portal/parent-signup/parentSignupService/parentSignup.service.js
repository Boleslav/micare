"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var rxjs_1 = require('rxjs');
var index_1 = require('../../../../api/index');
var ParentSignupService = (function () {
    function ParentSignupService(_apiClient) {
        this._apiClient = _apiClient;
    }
    ParentSignupService.prototype.RegisterParent = function (newParent) {
        return this._apiClient
            .account_RegisterParent(newParent).catch(function (error) {
            if (error.status === 403 || error.status === 417) {
                return rxjs_1.Observable.throw(error);
            }
            else {
                return rxjs_1.Observable.throw('Something went wrong, but we are not quite sure what happened..');
            }
        });
    };
    ParentSignupService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.ApiClientService])
    ], ParentSignupService);
    return ParentSignupService;
}());
exports.ParentSignupService = ParentSignupService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9wYXJlbnQtc2lnbnVwL3BhcmVudFNpZ251cFNlcnZpY2UvcGFyZW50U2lnbnVwLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUEyQixlQUFlLENBQUMsQ0FBQTtBQUMzQyxxQkFBMkIsTUFBTSxDQUFDLENBQUE7QUFFbEMsc0JBQXNELHVCQUF1QixDQUFDLENBQUE7QUFHOUU7SUFFSSw2QkFBb0IsVUFBNEI7UUFBNUIsZUFBVSxHQUFWLFVBQVUsQ0FBa0I7SUFDaEQsQ0FBQztJQUVNLDRDQUFjLEdBQXJCLFVBQXNCLFNBQThCO1FBQ2hELE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVTthQUNqQixzQkFBc0IsQ0FBQyxTQUFTLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQyxLQUFVO1lBRWhELEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLEtBQUssR0FBRyxJQUFJLEtBQUssQ0FBQyxNQUFNLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDL0MsTUFBTSxDQUFDLGlCQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ25DLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDSixNQUFNLENBQUMsaUJBQVUsQ0FBQyxLQUFLLENBQUMsaUVBQWlFLENBQUMsQ0FBQztZQUMvRixDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBaEJMO1FBQUMsaUJBQVUsRUFBRTs7MkJBQUE7SUFpQmIsMEJBQUM7QUFBRCxDQWhCQSxBQWdCQyxJQUFBO0FBaEJZLDJCQUFtQixzQkFnQi9CLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL3BhcmVudHMtcG9ydGFsL3BhcmVudC1zaWdudXAvcGFyZW50U2lnbnVwU2VydmljZS9wYXJlbnRTaWdudXAuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgQXBpQ2xpZW50U2VydmljZSwgUmVnaXN0ZXJQYXJlbnRNb2RlbCB9IGZyb20gJy4uLy4uLy4uLy4uL2FwaS9pbmRleCc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBQYXJlbnRTaWdudXBTZXJ2aWNlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9hcGlDbGllbnQ6IEFwaUNsaWVudFNlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgUmVnaXN0ZXJQYXJlbnQobmV3UGFyZW50OiBSZWdpc3RlclBhcmVudE1vZGVsKTogT2JzZXJ2YWJsZTx2b2lkPiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2FwaUNsaWVudFxyXG4gICAgICAgICAgICAuYWNjb3VudF9SZWdpc3RlclBhcmVudChuZXdQYXJlbnQpLmNhdGNoKChlcnJvcjogYW55KSA9PiB7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKGVycm9yLnN0YXR1cyA9PT0gNDAzIHx8IGVycm9yLnN0YXR1cyA9PT0gNDE3KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIE9ic2VydmFibGUudGhyb3coZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gT2JzZXJ2YWJsZS50aHJvdygnU29tZXRoaW5nIHdlbnQgd3JvbmcsIGJ1dCB3ZSBhcmUgbm90IHF1aXRlIHN1cmUgd2hhdCBoYXBwZW5lZC4uJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
