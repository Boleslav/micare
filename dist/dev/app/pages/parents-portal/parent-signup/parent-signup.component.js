"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var ParentSignupComponent = (function () {
    function ParentSignupComponent(_router, _route) {
        this._router = _router;
        this._route = _route;
        this.logo = null;
    }
    ParentSignupComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route
            .params
            .subscribe(function (params) {
            var centreName = params['centreName'];
            return _this._route.data
                .subscribe(function (data) {
                if (data.userExists) {
                    _this._router.navigate(['/parent-login', centreName]);
                }
                if (data.centreLogo.imageBase64) {
                    _this.logo = data.centreLogo.imageBase64;
                }
            });
        });
    };
    ParentSignupComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'parent-signup-cmp',
            templateUrl: 'parent-signup.component.html'
        }), 
        __metadata('design:paramtypes', [router_1.Router, router_1.ActivatedRoute])
    ], ParentSignupComponent);
    return ParentSignupComponent;
}());
exports.ParentSignupComponent = ParentSignupComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9wYXJlbnQtc2lnbnVwL3BhcmVudC1zaWdudXAuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBa0MsZUFBZSxDQUFDLENBQUE7QUFDbEQsdUJBQStDLGlCQUFpQixDQUFDLENBQUE7QUFTakU7SUFHRSwrQkFBb0IsT0FBZSxFQUFVLE1BQXNCO1FBQS9DLFlBQU8sR0FBUCxPQUFPLENBQVE7UUFBVSxXQUFNLEdBQU4sTUFBTSxDQUFnQjtRQUQzRCxTQUFJLEdBQVcsSUFBSSxDQUFDO0lBRTVCLENBQUM7SUFFRCx3Q0FBUSxHQUFSO1FBQUEsaUJBZ0JDO1FBZkMsSUFBSSxDQUFDLE1BQU07YUFDUixNQUFNO2FBQ04sU0FBUyxDQUFDLFVBQUMsTUFBYztZQUN4QixJQUFJLFVBQVUsR0FBRyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDdEMsTUFBTSxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSTtpQkFDcEIsU0FBUyxDQUFDLFVBQUMsSUFBd0Q7Z0JBQ2xFLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO29CQUNwQixLQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLGVBQWUsRUFBRSxVQUFVLENBQUMsQ0FBQyxDQUFDO2dCQUN2RCxDQUFDO2dCQUVELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztvQkFDaEMsS0FBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQztnQkFDMUMsQ0FBQztZQUNILENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBNUJIO1FBQUMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsbUJBQW1CO1lBQzdCLFdBQVcsRUFBRSw4QkFBOEI7U0FDNUMsQ0FBQzs7NkJBQUE7SUF5QkYsNEJBQUM7QUFBRCxDQXZCQSxBQXVCQyxJQUFBO0FBdkJZLDZCQUFxQix3QkF1QmpDLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL3BhcmVudHMtcG9ydGFsL3BhcmVudC1zaWdudXAvcGFyZW50LXNpZ251cC5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBSb3V0ZXIsIEFjdGl2YXRlZFJvdXRlLCBQYXJhbXMgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBDZW50cmVMb2dvRHRvIH0gZnJvbSAnLi4vLi4vLi4vYXBpL2luZGV4JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgc2VsZWN0b3I6ICdwYXJlbnQtc2lnbnVwLWNtcCcsXHJcbiAgdGVtcGxhdGVVcmw6ICdwYXJlbnQtc2lnbnVwLmNvbXBvbmVudC5odG1sJ1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFBhcmVudFNpZ251cENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIHByaXZhdGUgbG9nbzogc3RyaW5nID0gbnVsbDtcclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9yb3V0ZXI6IFJvdXRlciwgcHJpdmF0ZSBfcm91dGU6IEFjdGl2YXRlZFJvdXRlKSB7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIHRoaXMuX3JvdXRlXHJcbiAgICAgIC5wYXJhbXNcclxuICAgICAgLnN1YnNjcmliZSgocGFyYW1zOiBQYXJhbXMpID0+IHtcclxuICAgICAgICBsZXQgY2VudHJlTmFtZSA9IHBhcmFtc1snY2VudHJlTmFtZSddO1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9yb3V0ZS5kYXRhXHJcbiAgICAgICAgICAuc3Vic2NyaWJlKChkYXRhOiB7IHVzZXJFeGlzdHM6IGJvb2xlYW4sIGNlbnRyZUxvZ286IENlbnRyZUxvZ29EdG8gfSkgPT4ge1xyXG4gICAgICAgICAgICBpZiAoZGF0YS51c2VyRXhpc3RzKSB7XHJcbiAgICAgICAgICAgICAgdGhpcy5fcm91dGVyLm5hdmlnYXRlKFsnL3BhcmVudC1sb2dpbicsIGNlbnRyZU5hbWVdKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKGRhdGEuY2VudHJlTG9nby5pbWFnZUJhc2U2NCkge1xyXG4gICAgICAgICAgICAgIHRoaXMubG9nbyA9IGRhdGEuY2VudHJlTG9nby5pbWFnZUJhc2U2NDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxufVxyXG4iXX0=
