"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var router_1 = require('@angular/router');
var index_1 = require('../../../../validators/index');
var ng2_bootstrap_1 = require('ng2-bootstrap');
var _ = require('lodash');
var parentSignup_service_1 = require('../parentSignupService/parentSignup.service');
var angular2_toaster_1 = require('angular2-toaster');
var index_2 = require('../../../../shared/index');
var index_3 = require('../../../../api/index');
var ParentSignupFormComponent = (function () {
    function ParentSignupFormComponent(fb, _signupService, _toasterService, _router, _route) {
        this._signupService = _signupService;
        this._toasterService = _toasterService;
        this._router = _router;
        this._route = _route;
        this.submitted = false;
        this.userCreated = false;
        this.captcha = false;
        this.captchaToken = null;
        this.captcha_key = index_2.Config.RECAPTCHA_UI_KEY;
        this.captcha_enabled = this.convertStringToBool(index_2.Config.RecaptchaEnabled);
        this.centreName = null;
        this.form = fb.group({
            'firstName': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(1)])],
            'lastName': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(1)])],
            'email': ['', forms_1.Validators.compose([forms_1.Validators.required, index_1.EmailValidator.validate])],
            'mobilePhone': ['', forms_1.Validators.compose([forms_1.Validators.required, index_1.MobileValidator.validate])],
            'passwords': fb.group({
                'password': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(8), index_1.ComplexPasswordValidator.validate])],
                'confirmPassword': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(8)])]
            }, { validator: index_1.EqualPasswordsValidator.validate('password', 'confirmPassword') })
        });
        this.firstName = this.form.controls['firstName'];
        this.lastName = this.form.controls['lastName'];
        this.email = this.form.controls['email'];
        this.mobilePhone = this.form.controls['mobilePhone'];
        this.passwords = this.form.controls['passwords'];
        this.password = this.passwords.controls['password'];
        this.confirmPassword = this.passwords.controls['confirmPassword'];
        if (this.captcha_enabled === false) {
            this.captcha = true;
        }
    }
    ParentSignupFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route
            .params
            .subscribe(function (params) {
            _this.centreName = params['centreName'];
        });
    };
    ParentSignupFormComponent.prototype.onSubmit = function (values) {
        var _this = this;
        this.submitted = true;
        var savingToast = this._toasterService.pop(new index_2.SpinningToast());
        var newParent = new index_3.RegisterParentModel();
        newParent.init({
            FirstName: values.firstName,
            LastName: values.lastName,
            Email: values.email,
            PrimaryPhone: values.mobilePhone,
            Password: values.passwords.password,
            ConfirmPassword: values.passwords.confirmPassword,
            CaptchaToken: this.captchaToken,
            CentreUniqueName: this.centreName
        });
        this._signupService
            .RegisterParent(newParent)
            .subscribe(function () {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this._toasterService.pop('success', 'User Created', '');
            _this.userCreated = true;
        }, function (err) {
            if (_.includes(err.response, 'in use')) {
                _this._toasterService.clear();
                _this.staticModal.show();
            }
            else {
                _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                _this.submitted = false;
                if (_this.captcha_enabled)
                    _this.recaptcha.reset();
            }
        });
    };
    ;
    ParentSignupFormComponent.prototype.handleCorrectCaptcha = function (event) {
        this.captchaToken = event;
        this.captcha = true;
    };
    ;
    ParentSignupFormComponent.prototype.handleCaptchaExpired = function () {
        this.captchaToken = null;
    };
    ParentSignupFormComponent.prototype.convertStringToBool = function (input) {
        var result = false;
        if (input === 'true') {
            result = true;
        }
        return result;
    };
    ParentSignupFormComponent.prototype.confirmRedirect = function () {
        this._router.navigate(['/parent-login', this.centreName]);
    };
    __decorate([
        core_1.ViewChild('recaptcha'), 
        __metadata('design:type', Object)
    ], ParentSignupFormComponent.prototype, "recaptcha", void 0);
    __decorate([
        core_1.ViewChild('staticModal'), 
        __metadata('design:type', ng2_bootstrap_1.ModalDirective)
    ], ParentSignupFormComponent.prototype, "staticModal", void 0);
    ParentSignupFormComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'parent-signup-form-cmp',
            templateUrl: 'parentSignupForm.component.html',
            styleUrls: ['./parentSignupForm.component.css']
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, parentSignup_service_1.ParentSignupService, angular2_toaster_1.ToasterService, router_1.Router, router_1.ActivatedRoute])
    ], ParentSignupFormComponent);
    return ParentSignupFormComponent;
}());
exports.ParentSignupFormComponent = ParentSignupFormComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9wYXJlbnQtc2lnbnVwL3BhcmVudFNpZ251cEZvcm0vcGFyZW50U2lnbnVwRm9ybS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUE2QyxlQUFlLENBQUMsQ0FBQTtBQUM3RCxzQkFBb0UsZ0JBQWdCLENBQUMsQ0FBQTtBQUNyRix1QkFBK0MsaUJBQWlCLENBQUMsQ0FBQTtBQUNqRSxzQkFBbUcsOEJBQThCLENBQUMsQ0FBQTtBQUNsSSw4QkFBK0IsZUFBZSxDQUFDLENBQUE7QUFDL0MsSUFBWSxDQUFDLFdBQU0sUUFBUSxDQUFDLENBQUE7QUFFNUIscUNBQW9DLDZDQUE2QyxDQUFDLENBQUE7QUFDbEYsaUNBQStCLGtCQUFrQixDQUFDLENBQUE7QUFDbEQsc0JBQXNDLDBCQUEwQixDQUFDLENBQUE7QUFDakUsc0JBQW9DLHVCQUF1QixDQUFDLENBQUE7QUFTNUQ7SUFzQkksbUNBQVksRUFBZSxFQUNmLGNBQW1DLEVBQ25DLGVBQStCLEVBQy9CLE9BQWUsRUFDZixNQUFzQjtRQUh0QixtQkFBYyxHQUFkLGNBQWMsQ0FBcUI7UUFDbkMsb0JBQWUsR0FBZixlQUFlLENBQWdCO1FBQy9CLFlBQU8sR0FBUCxPQUFPLENBQVE7UUFDZixXQUFNLEdBQU4sTUFBTSxDQUFnQjtRQVozQixjQUFTLEdBQVksS0FBSyxDQUFDO1FBQzNCLGdCQUFXLEdBQVksS0FBSyxDQUFDO1FBQzdCLFlBQU8sR0FBWSxLQUFLLENBQUM7UUFDekIsaUJBQVksR0FBVyxJQUFJLENBQUM7UUFDNUIsZ0JBQVcsR0FBVyxjQUFNLENBQUMsZ0JBQWdCLENBQUM7UUFDOUMsb0JBQWUsR0FBWSxJQUFJLENBQUMsbUJBQW1CLENBQUMsY0FBTSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFFNUUsZUFBVSxHQUFXLElBQUksQ0FBQztRQU85QixJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQyxLQUFLLENBQUM7WUFDakIsV0FBVyxFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsa0JBQVUsQ0FBQyxRQUFRLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3JGLFVBQVUsRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLGtCQUFVLENBQUMsUUFBUSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNwRixPQUFPLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsRUFBRSxzQkFBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDakYsYUFBYSxFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsa0JBQVUsQ0FBQyxRQUFRLEVBQUUsdUJBQWUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ3hGLFdBQVcsRUFBRSxFQUFFLENBQUMsS0FBSyxDQUFDO2dCQUNsQixVQUFVLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxnQ0FBd0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUN2SCxpQkFBaUIsRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLGtCQUFVLENBQUMsUUFBUSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUM5RixFQUFFLEVBQUUsU0FBUyxFQUFFLCtCQUF1QixDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsaUJBQWlCLENBQUMsRUFBRSxDQUFDO1NBQ3JGLENBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDakQsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUMvQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDckQsSUFBSSxDQUFDLFNBQVMsR0FBYyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUM1RCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3BELElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUVsRSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxLQUFLLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFFakMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDeEIsQ0FBQztJQUNMLENBQUM7SUFFRCw0Q0FBUSxHQUFSO1FBQUEsaUJBTUM7UUFMRyxJQUFJLENBQUMsTUFBTTthQUNOLE1BQU07YUFDTixTQUFTLENBQUMsVUFBQyxNQUFjO1lBQ3RCLEtBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQzNDLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVELDRDQUFRLEdBQVIsVUFBUyxNQUFXO1FBQXBCLGlCQW9DQztRQWxDRyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUV0QixJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxJQUFJLHFCQUFhLEVBQUUsQ0FBQyxDQUFDO1FBRWhFLElBQUksU0FBUyxHQUFHLElBQUksMkJBQW1CLEVBQUUsQ0FBQztRQUMxQyxTQUFTLENBQUMsSUFBSSxDQUFDO1lBQ1gsU0FBUyxFQUFFLE1BQU0sQ0FBQyxTQUFTO1lBQzNCLFFBQVEsRUFBRSxNQUFNLENBQUMsUUFBUTtZQUN6QixLQUFLLEVBQUUsTUFBTSxDQUFDLEtBQUs7WUFDbkIsWUFBWSxFQUFFLE1BQU0sQ0FBQyxXQUFXO1lBQ2hDLFFBQVEsRUFBRSxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVE7WUFDbkMsZUFBZSxFQUFFLE1BQU0sQ0FBQyxTQUFTLENBQUMsZUFBZTtZQUNqRCxZQUFZLEVBQUUsSUFBSSxDQUFDLFlBQVk7WUFDL0IsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLFVBQVU7U0FDcEMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLGNBQWM7YUFDZCxjQUFjLENBQUMsU0FBUyxDQUFDO2FBQ3pCLFNBQVMsQ0FBQztZQUNQLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDOUUsS0FBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLGNBQWMsRUFBRSxFQUFFLENBQUMsQ0FBQztZQUN4RCxLQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztRQUM1QixDQUFDLEVBQ0QsVUFBQSxHQUFHO1lBQ0MsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDckMsS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDN0IsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUM1QixDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxXQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztnQkFDOUUsS0FBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7Z0JBQ3ZCLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxlQUFlLENBQUM7b0JBQ3JCLEtBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDL0IsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQzs7SUFFRCx3REFBb0IsR0FBcEIsVUFBcUIsS0FBYTtRQUM5QixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztJQUN4QixDQUFDOztJQUVELHdEQUFvQixHQUFwQjtRQUNJLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO0lBQzdCLENBQUM7SUFFRCx1REFBbUIsR0FBbkIsVUFBb0IsS0FBYTtRQUM3QixJQUFJLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDbkIsRUFBRSxDQUFDLENBQUMsS0FBSyxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDbkIsTUFBTSxHQUFHLElBQUksQ0FBQztRQUNsQixDQUFDO1FBRUQsTUFBTSxDQUFDLE1BQU0sQ0FBQztJQUNsQixDQUFDO0lBRUQsbURBQWUsR0FBZjtRQUNJLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFySEQ7UUFBQyxnQkFBUyxDQUFDLFdBQVcsQ0FBQzs7Z0VBQUE7SUFDdkI7UUFBQyxnQkFBUyxDQUFDLGFBQWEsQ0FBQzs7a0VBQUE7SUFWN0I7UUFBQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSx3QkFBd0I7WUFDbEMsV0FBVyxFQUFFLGlDQUFpQztZQUM5QyxTQUFTLEVBQUUsQ0FBQyxrQ0FBa0MsQ0FBQztTQUNsRCxDQUFDOztpQ0FBQTtJQTBIRixnQ0FBQztBQUFELENBeEhBLEFBd0hDLElBQUE7QUF4SFksaUNBQXlCLDRCQXdIckMsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvcGFyZW50cy1wb3J0YWwvcGFyZW50LXNpZ251cC9wYXJlbnRTaWdudXBGb3JtL3BhcmVudFNpZ251cEZvcm0uY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBWaWV3Q2hpbGQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGb3JtR3JvdXAsIEFic3RyYWN0Q29udHJvbCwgRm9ybUJ1aWxkZXIsIFZhbGlkYXRvcnMgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IFJvdXRlciwgQWN0aXZhdGVkUm91dGUsIFBhcmFtcyB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IEVtYWlsVmFsaWRhdG9yLCBNb2JpbGVWYWxpZGF0b3IsIENvbXBsZXhQYXNzd29yZFZhbGlkYXRvciwgRXF1YWxQYXNzd29yZHNWYWxpZGF0b3IgfSBmcm9tICcuLi8uLi8uLi8uLi92YWxpZGF0b3JzL2luZGV4JztcclxuaW1wb3J0IHsgTW9kYWxEaXJlY3RpdmUgfSBmcm9tICduZzItYm9vdHN0cmFwJztcclxuaW1wb3J0ICogYXMgXyBmcm9tICdsb2Rhc2gnO1xyXG5cclxuaW1wb3J0IHsgUGFyZW50U2lnbnVwU2VydmljZSB9IGZyb20gJy4uL3BhcmVudFNpZ251cFNlcnZpY2UvcGFyZW50U2lnbnVwLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBUb2FzdGVyU2VydmljZSB9IGZyb20gJ2FuZ3VsYXIyLXRvYXN0ZXInO1xyXG5pbXBvcnQgeyBDb25maWcsIFNwaW5uaW5nVG9hc3QgfSBmcm9tICcuLi8uLi8uLi8uLi9zaGFyZWQvaW5kZXgnO1xyXG5pbXBvcnQgeyBSZWdpc3RlclBhcmVudE1vZGVsIH0gZnJvbSAnLi4vLi4vLi4vLi4vYXBpL2luZGV4JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHNlbGVjdG9yOiAncGFyZW50LXNpZ251cC1mb3JtLWNtcCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJ3BhcmVudFNpZ251cEZvcm0uY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vcGFyZW50U2lnbnVwRm9ybS5jb21wb25lbnQuY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBQYXJlbnRTaWdudXBGb3JtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBAVmlld0NoaWxkKCdyZWNhcHRjaGEnKSByZWNhcHRjaGE6IGFueTtcclxuICAgIEBWaWV3Q2hpbGQoJ3N0YXRpY01vZGFsJykgcHVibGljIHN0YXRpY01vZGFsOiBNb2RhbERpcmVjdGl2ZTtcclxuXHJcbiAgICBwdWJsaWMgZm9ybTogRm9ybUdyb3VwO1xyXG4gICAgcHVibGljIGZpcnN0TmFtZTogQWJzdHJhY3RDb250cm9sO1xyXG4gICAgcHVibGljIGxhc3ROYW1lOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgICBwdWJsaWMgZW1haWw6IEFic3RyYWN0Q29udHJvbDtcclxuICAgIHB1YmxpYyBtb2JpbGVQaG9uZTogQWJzdHJhY3RDb250cm9sO1xyXG4gICAgcHVibGljIHBhc3N3b3JkOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgICBwdWJsaWMgY29uZmlybVBhc3N3b3JkOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgICBwdWJsaWMgcGFzc3dvcmRzOiBGb3JtR3JvdXA7XHJcblxyXG4gICAgcHVibGljIHN1Ym1pdHRlZDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgcHVibGljIHVzZXJDcmVhdGVkOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBwdWJsaWMgY2FwdGNoYTogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgcHVibGljIGNhcHRjaGFUb2tlbjogc3RyaW5nID0gbnVsbDtcclxuICAgIHB1YmxpYyBjYXB0Y2hhX2tleTogc3RyaW5nID0gQ29uZmlnLlJFQ0FQVENIQV9VSV9LRVk7XHJcbiAgICBwdWJsaWMgY2FwdGNoYV9lbmFibGVkOiBib29sZWFuID0gdGhpcy5jb252ZXJ0U3RyaW5nVG9Cb29sKENvbmZpZy5SZWNhcHRjaGFFbmFibGVkKTtcclxuXHJcbiAgICBwcml2YXRlIGNlbnRyZU5hbWU6IHN0cmluZyA9IG51bGw7XHJcbiAgICBjb25zdHJ1Y3RvcihmYjogRm9ybUJ1aWxkZXIsXHJcbiAgICAgICAgcHJpdmF0ZSBfc2lnbnVwU2VydmljZTogUGFyZW50U2lnbnVwU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIF90b2FzdGVyU2VydmljZTogVG9hc3RlclNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBfcm91dGVyOiBSb3V0ZXIsXHJcbiAgICAgICAgcHJpdmF0ZSBfcm91dGU6IEFjdGl2YXRlZFJvdXRlKSB7XHJcblxyXG4gICAgICAgIHRoaXMuZm9ybSA9IGZiLmdyb3VwKHtcclxuICAgICAgICAgICAgJ2ZpcnN0TmFtZSc6IFsnJywgVmFsaWRhdG9ycy5jb21wb3NlKFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1pbkxlbmd0aCgxKV0pXSxcclxuICAgICAgICAgICAgJ2xhc3ROYW1lJzogWycnLCBWYWxpZGF0b3JzLmNvbXBvc2UoW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWluTGVuZ3RoKDEpXSldLFxyXG4gICAgICAgICAgICAnZW1haWwnOiBbJycsIFZhbGlkYXRvcnMuY29tcG9zZShbVmFsaWRhdG9ycy5yZXF1aXJlZCwgRW1haWxWYWxpZGF0b3IudmFsaWRhdGVdKV0sXHJcbiAgICAgICAgICAgICdtb2JpbGVQaG9uZSc6IFsnJywgVmFsaWRhdG9ycy5jb21wb3NlKFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBNb2JpbGVWYWxpZGF0b3IudmFsaWRhdGVdKV0sXHJcbiAgICAgICAgICAgICdwYXNzd29yZHMnOiBmYi5ncm91cCh7XHJcbiAgICAgICAgICAgICAgICAncGFzc3dvcmQnOiBbJycsIFZhbGlkYXRvcnMuY29tcG9zZShbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5taW5MZW5ndGgoOCksIENvbXBsZXhQYXNzd29yZFZhbGlkYXRvci52YWxpZGF0ZV0pXSxcclxuICAgICAgICAgICAgICAgICdjb25maXJtUGFzc3dvcmQnOiBbJycsIFZhbGlkYXRvcnMuY29tcG9zZShbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5taW5MZW5ndGgoOCldKV1cclxuICAgICAgICAgICAgfSwgeyB2YWxpZGF0b3I6IEVxdWFsUGFzc3dvcmRzVmFsaWRhdG9yLnZhbGlkYXRlKCdwYXNzd29yZCcsICdjb25maXJtUGFzc3dvcmQnKSB9KVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICB0aGlzLmZpcnN0TmFtZSA9IHRoaXMuZm9ybS5jb250cm9sc1snZmlyc3ROYW1lJ107XHJcbiAgICAgICAgdGhpcy5sYXN0TmFtZSA9IHRoaXMuZm9ybS5jb250cm9sc1snbGFzdE5hbWUnXTtcclxuICAgICAgICB0aGlzLmVtYWlsID0gdGhpcy5mb3JtLmNvbnRyb2xzWydlbWFpbCddO1xyXG4gICAgICAgIHRoaXMubW9iaWxlUGhvbmUgPSB0aGlzLmZvcm0uY29udHJvbHNbJ21vYmlsZVBob25lJ107XHJcbiAgICAgICAgdGhpcy5wYXNzd29yZHMgPSA8Rm9ybUdyb3VwPnRoaXMuZm9ybS5jb250cm9sc1sncGFzc3dvcmRzJ107XHJcbiAgICAgICAgdGhpcy5wYXNzd29yZCA9IHRoaXMucGFzc3dvcmRzLmNvbnRyb2xzWydwYXNzd29yZCddO1xyXG4gICAgICAgIHRoaXMuY29uZmlybVBhc3N3b3JkID0gdGhpcy5wYXNzd29yZHMuY29udHJvbHNbJ2NvbmZpcm1QYXNzd29yZCddO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5jYXB0Y2hhX2VuYWJsZWQgPT09IGZhbHNlKSB7XHJcbiAgICAgICAgICAgIC8vaWYgaXRzIGRpc2FibGVkIGxldCB1c2VyIGNsaWNrIGJ1dHRvblxyXG4gICAgICAgICAgICB0aGlzLmNhcHRjaGEgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLl9yb3V0ZVxyXG4gICAgICAgICAgICAucGFyYW1zXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoKHBhcmFtczogUGFyYW1zKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNlbnRyZU5hbWUgPSBwYXJhbXNbJ2NlbnRyZU5hbWUnXTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgb25TdWJtaXQodmFsdWVzOiBhbnkpIHtcclxuXHJcbiAgICAgICAgdGhpcy5zdWJtaXR0ZWQgPSB0cnVlO1xyXG5cclxuICAgICAgICBsZXQgc2F2aW5nVG9hc3QgPSB0aGlzLl90b2FzdGVyU2VydmljZS5wb3AobmV3IFNwaW5uaW5nVG9hc3QoKSk7XHJcblxyXG4gICAgICAgIHZhciBuZXdQYXJlbnQgPSBuZXcgUmVnaXN0ZXJQYXJlbnRNb2RlbCgpO1xyXG4gICAgICAgIG5ld1BhcmVudC5pbml0KHtcclxuICAgICAgICAgICAgRmlyc3ROYW1lOiB2YWx1ZXMuZmlyc3ROYW1lLFxyXG4gICAgICAgICAgICBMYXN0TmFtZTogdmFsdWVzLmxhc3ROYW1lLFxyXG4gICAgICAgICAgICBFbWFpbDogdmFsdWVzLmVtYWlsLFxyXG4gICAgICAgICAgICBQcmltYXJ5UGhvbmU6IHZhbHVlcy5tb2JpbGVQaG9uZSxcclxuICAgICAgICAgICAgUGFzc3dvcmQ6IHZhbHVlcy5wYXNzd29yZHMucGFzc3dvcmQsXHJcbiAgICAgICAgICAgIENvbmZpcm1QYXNzd29yZDogdmFsdWVzLnBhc3N3b3Jkcy5jb25maXJtUGFzc3dvcmQsXHJcbiAgICAgICAgICAgIENhcHRjaGFUb2tlbjogdGhpcy5jYXB0Y2hhVG9rZW4sXHJcbiAgICAgICAgICAgIENlbnRyZVVuaXF1ZU5hbWU6IHRoaXMuY2VudHJlTmFtZVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICB0aGlzLl9zaWdudXBTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5SZWdpc3RlclBhcmVudChuZXdQYXJlbnQpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoc2F2aW5nVG9hc3QudG9hc3RJZCwgc2F2aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5wb3AoJ3N1Y2Nlc3MnLCAnVXNlciBDcmVhdGVkJywgJycpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy51c2VyQ3JlYXRlZCA9IHRydWU7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGVyciA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoXy5pbmNsdWRlcyhlcnIucmVzcG9uc2UsICdpbiB1c2UnKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLmNsZWFyKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0aWNNb2RhbC5zaG93KCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLmNsZWFyKHNhdmluZ1RvYXN0LnRvYXN0SWQsIHNhdmluZ1RvYXN0LnRvYXN0Q29udGFpbmVySWQpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc3VibWl0dGVkID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuY2FwdGNoYV9lbmFibGVkKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJlY2FwdGNoYS5yZXNldCgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgaGFuZGxlQ29ycmVjdENhcHRjaGEoZXZlbnQ6IHN0cmluZyk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuY2FwdGNoYVRva2VuID0gZXZlbnQ7XHJcbiAgICAgICAgdGhpcy5jYXB0Y2hhID0gdHJ1ZTtcclxuICAgIH07XHJcblxyXG4gICAgaGFuZGxlQ2FwdGNoYUV4cGlyZWQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5jYXB0Y2hhVG9rZW4gPSBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnZlcnRTdHJpbmdUb0Jvb2woaW5wdXQ6IHN0cmluZyk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHZhciByZXN1bHQgPSBmYWxzZTtcclxuICAgICAgICBpZiAoaW5wdXQgPT09ICd0cnVlJykge1xyXG4gICAgICAgICAgICByZXN1bHQgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgIH1cclxuXHJcbiAgICBjb25maXJtUmVkaXJlY3QoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5fcm91dGVyLm5hdmlnYXRlKFsnL3BhcmVudC1sb2dpbicsIHRoaXMuY2VudHJlTmFtZV0pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
