"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../shared/index');
var index_2 = require('./index');
var angular2_recaptcha_1 = require('angular2-recaptcha');
var ParentSignupModule = (function () {
    function ParentSignupModule() {
    }
    ParentSignupModule = __decorate([
        core_1.NgModule({
            imports: [index_1.SharedModule, angular2_recaptcha_1.ReCaptchaModule],
            declarations: [index_2.ParentSignupComponent, index_2.ParentSignupFormComponent],
            providers: [index_2.ParentSignupService]
        }), 
        __metadata('design:paramtypes', [])
    ], ParentSignupModule);
    return ParentSignupModule;
}());
exports.ParentSignupModule = ParentSignupModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9wYXJlbnQtc2lnbnVwL3BhcmVudC1zaWdudXAubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBeUIsZUFBZSxDQUFDLENBQUE7QUFDekMsc0JBQTZCLHVCQUF1QixDQUFDLENBQUE7QUFFckQsc0JBR08sU0FBUyxDQUFDLENBQUE7QUFFakIsbUNBQWdDLG9CQUFvQixDQUFDLENBQUE7QUFTckQ7SUFBQTtJQUFrQyxDQUFDO0lBTm5DO1FBQUMsZUFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFLENBQUMsb0JBQVksRUFBRSxvQ0FBZSxDQUFDO1lBQ3hDLFlBQVksRUFBRSxDQUFDLDZCQUFxQixFQUFFLGlDQUF5QixDQUFDO1lBQ2hFLFNBQVMsRUFBRSxDQUFDLDJCQUFtQixDQUFDO1NBQ25DLENBQUM7OzBCQUFBO0lBRWdDLHlCQUFDO0FBQUQsQ0FBbEMsQUFBbUMsSUFBQTtBQUF0QiwwQkFBa0IscUJBQUksQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvcGFyZW50cy1wb3J0YWwvcGFyZW50LXNpZ251cC9wYXJlbnQtc2lnbnVwLm1vZHVsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFNoYXJlZE1vZHVsZSB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9pbmRleCc7XHJcblxyXG5pbXBvcnQge1xyXG4gICAgUGFyZW50U2lnbnVwQ29tcG9uZW50LCBQYXJlbnRTaWdudXBGb3JtQ29tcG9uZW50LFxyXG4gICAgUGFyZW50U2lnbnVwU2VydmljZVxyXG59IGZyb20gJy4vaW5kZXgnO1xyXG5cclxuaW1wb3J0IHsgUmVDYXB0Y2hhTW9kdWxlIH0gZnJvbSAnYW5ndWxhcjItcmVjYXB0Y2hhJztcclxuXHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgaW1wb3J0czogW1NoYXJlZE1vZHVsZSwgUmVDYXB0Y2hhTW9kdWxlXSxcclxuICAgIGRlY2xhcmF0aW9uczogW1BhcmVudFNpZ251cENvbXBvbmVudCwgUGFyZW50U2lnbnVwRm9ybUNvbXBvbmVudF0sXHJcbiAgICBwcm92aWRlcnM6IFtQYXJlbnRTaWdudXBTZXJ2aWNlXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFBhcmVudFNpZ251cE1vZHVsZSB7IH1cclxuIl19
