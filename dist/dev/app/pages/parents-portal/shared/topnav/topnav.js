"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var index_1 = require('../../index');
var TopNavComponent = (function () {
    function TopNavComponent(_centreService, _router, _route) {
        this._centreService = _centreService;
        this._router = _router;
        this._route = _route;
        this.centreName = null;
    }
    TopNavComponent.prototype.ngOnInit = function () {
        this.centreName = this._centreService.SelectedCentreUniqueName;
    };
    TopNavComponent.prototype.changeTheme = function (color) {
        var link = $('<link>');
        link
            .appendTo('head')
            .attr({ type: 'text/css', rel: 'stylesheet' })
            .attr('href', 'themes/app-' + color + '.css');
    };
    TopNavComponent.prototype.rtl = function () {
        var body = $('body');
        body.toggleClass('rtl');
    };
    TopNavComponent.prototype.sidebarToggler = function () {
        var sidebar = $('#sidebar');
        var mainContainer = $('.main-container');
        sidebar.toggleClass('sidebar-left-zero');
        mainContainer.toggleClass('main-container-ml-zero');
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], TopNavComponent.prototype, "logo", void 0);
    TopNavComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'top-nav',
            templateUrl: 'topnav.html',
        }), 
        __metadata('design:paramtypes', [index_1.CentreService, router_1.Router, router_1.ActivatedRoute])
    ], TopNavComponent);
    return TopNavComponent;
}());
exports.TopNavComponent = TopNavComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9zaGFyZWQvdG9wbmF2L3RvcG5hdi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQXlDLGVBQWUsQ0FBQyxDQUFBO0FBQ3pELHVCQUF1QyxpQkFBaUIsQ0FBQyxDQUFBO0FBRXpELHNCQUE4QixhQUFhLENBQUMsQ0FBQTtBQVE1QztJQUtDLHlCQUFvQixjQUE2QixFQUN4QyxPQUFlLEVBQ2YsTUFBc0I7UUFGWCxtQkFBYyxHQUFkLGNBQWMsQ0FBZTtRQUN4QyxZQUFPLEdBQVAsT0FBTyxDQUFRO1FBQ2YsV0FBTSxHQUFOLE1BQU0sQ0FBZ0I7UUFKdkIsZUFBVSxHQUFXLElBQUksQ0FBQztJQUtsQyxDQUFDO0lBRUQsa0NBQVEsR0FBUjtRQUNDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyx3QkFBd0IsQ0FBQztJQUNoRSxDQUFDO0lBRUQscUNBQVcsR0FBWCxVQUFZLEtBQWE7UUFDeEIsSUFBSSxJQUFJLEdBQVEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzVCLElBQUk7YUFDRixRQUFRLENBQUMsTUFBTSxDQUFDO2FBQ2hCLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsR0FBRyxFQUFFLFlBQVksRUFBRSxDQUFDO2FBQzdDLElBQUksQ0FBQyxNQUFNLEVBQUUsYUFBYSxHQUFHLEtBQUssR0FBRyxNQUFNLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRUQsNkJBQUcsR0FBSDtRQUNDLElBQUksSUFBSSxHQUFRLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMxQixJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3pCLENBQUM7SUFFRCx3Q0FBYyxHQUFkO1FBQ0MsSUFBSSxPQUFPLEdBQVEsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ2pDLElBQUksYUFBYSxHQUFRLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQzlDLE9BQU8sQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUN6QyxhQUFhLENBQUMsV0FBVyxDQUFDLHdCQUF3QixDQUFDLENBQUM7SUFDckQsQ0FBQztJQTlCRDtRQUFDLFlBQUssRUFBRTs7aURBQUE7SUFSVDtRQUFDLGdCQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLFNBQVM7WUFDbkIsV0FBVyxFQUFFLGFBQWE7U0FDMUIsQ0FBQzs7dUJBQUE7SUFtQ0Ysc0JBQUM7QUFBRCxDQWpDQSxBQWlDQyxJQUFBO0FBakNZLHVCQUFlLGtCQWlDM0IsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvcGFyZW50cy1wb3J0YWwvc2hhcmVkL3RvcG5hdi90b3BuYXYuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgUm91dGVyLCBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcblxyXG5pbXBvcnQgeyBDZW50cmVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vaW5kZXgnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcblx0bW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuXHRzZWxlY3RvcjogJ3RvcC1uYXYnLFxyXG5cdHRlbXBsYXRlVXJsOiAndG9wbmF2Lmh0bWwnLFxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFRvcE5hdkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG5cdEBJbnB1dCgpIGxvZ286IHN0cmluZztcclxuXHRwcml2YXRlIGNlbnRyZU5hbWU6IHN0cmluZyA9IG51bGw7XHJcblxyXG5cdGNvbnN0cnVjdG9yKHByaXZhdGUgX2NlbnRyZVNlcnZpY2U6IENlbnRyZVNlcnZpY2UsXHJcblx0XHRwcml2YXRlIF9yb3V0ZXI6IFJvdXRlcixcclxuXHRcdHByaXZhdGUgX3JvdXRlOiBBY3RpdmF0ZWRSb3V0ZSkge1xyXG5cdH1cclxuXHJcblx0bmdPbkluaXQoKTogdm9pZCB7XHJcblx0XHR0aGlzLmNlbnRyZU5hbWUgPSB0aGlzLl9jZW50cmVTZXJ2aWNlLlNlbGVjdGVkQ2VudHJlVW5pcXVlTmFtZTtcclxuXHR9XHJcblxyXG5cdGNoYW5nZVRoZW1lKGNvbG9yOiBzdHJpbmcpOiB2b2lkIHtcclxuXHRcdHZhciBsaW5rOiBhbnkgPSAkKCc8bGluaz4nKTtcclxuXHRcdGxpbmtcclxuXHRcdFx0LmFwcGVuZFRvKCdoZWFkJylcclxuXHRcdFx0LmF0dHIoeyB0eXBlOiAndGV4dC9jc3MnLCByZWw6ICdzdHlsZXNoZWV0JyB9KVxyXG5cdFx0XHQuYXR0cignaHJlZicsICd0aGVtZXMvYXBwLScgKyBjb2xvciArICcuY3NzJyk7XHJcblx0fVxyXG5cclxuXHRydGwoKTogdm9pZCB7XHJcblx0XHR2YXIgYm9keTogYW55ID0gJCgnYm9keScpO1xyXG5cdFx0Ym9keS50b2dnbGVDbGFzcygncnRsJyk7XHJcblx0fVxyXG5cclxuXHRzaWRlYmFyVG9nZ2xlcigpOiB2b2lkIHtcclxuXHRcdHZhciBzaWRlYmFyOiBhbnkgPSAkKCcjc2lkZWJhcicpO1xyXG5cdFx0dmFyIG1haW5Db250YWluZXI6IGFueSA9ICQoJy5tYWluLWNvbnRhaW5lcicpO1xyXG5cdFx0c2lkZWJhci50b2dnbGVDbGFzcygnc2lkZWJhci1sZWZ0LXplcm8nKTtcclxuXHRcdG1haW5Db250YWluZXIudG9nZ2xlQ2xhc3MoJ21haW4tY29udGFpbmVyLW1sLXplcm8nKTtcclxuXHR9XHJcbn1cclxuIl19
