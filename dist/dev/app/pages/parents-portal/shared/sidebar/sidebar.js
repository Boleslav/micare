"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../../core/index');
var SidebarComponent = (function () {
    function SidebarComponent(_userService) {
        this._userService = _userService;
        this.isActive = false;
        this.showMenu = '';
        this.isSysAdmin = false;
        this.isCentreManager = false;
    }
    SidebarComponent.prototype.ngOnInit = function () { ; };
    SidebarComponent.prototype.eventCalled = function () {
        this.isActive = !this.isActive;
    };
    SidebarComponent.prototype.addExpandClass = function (element) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        }
        else {
            this.showMenu = element;
        }
    };
    SidebarComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'sidebar-cmp',
            templateUrl: 'sidebar.html'
        }), 
        __metadata('design:paramtypes', [index_1.ParentUserService])
    ], SidebarComponent);
    return SidebarComponent;
}());
exports.SidebarComponent = SidebarComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9zaGFyZWQvc2lkZWJhci9zaWRlYmFyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBa0MsZUFBZSxDQUFDLENBQUE7QUFFbEQsc0JBQWtDLHdCQUF3QixDQUFDLENBQUE7QUFRM0Q7SUFPQywwQkFBb0IsWUFBK0I7UUFBL0IsaUJBQVksR0FBWixZQUFZLENBQW1CO1FBTG5ELGFBQVEsR0FBRyxLQUFLLENBQUM7UUFDakIsYUFBUSxHQUFXLEVBQUUsQ0FBQztRQUN0QixlQUFVLEdBQVksS0FBSyxDQUFDO1FBQzVCLG9CQUFlLEdBQVksS0FBSyxDQUFDO0lBR2pDLENBQUM7SUFFRCxtQ0FBUSxHQUFSLGNBQW1CLENBQUMsQ0FBQyxDQUFDO0lBRXRCLHNDQUFXLEdBQVg7UUFDQyxJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUNoQyxDQUFDO0lBRUQseUNBQWMsR0FBZCxVQUFlLE9BQVk7UUFDMUIsRUFBRSxDQUFDLENBQUMsT0FBTyxLQUFLLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQy9CLElBQUksQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDO1FBQ3JCLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNQLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDO1FBQ3pCLENBQUM7SUFDRixDQUFDO0lBNUJGO1FBQUMsZ0JBQVMsQ0FBQztZQUNWLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsYUFBYTtZQUN2QixXQUFXLEVBQUUsY0FBYztTQUMzQixDQUFDOzt3QkFBQTtJQXlCRix1QkFBQztBQUFELENBdkJBLEFBdUJDLElBQUE7QUF2Qlksd0JBQWdCLG1CQXVCNUIsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvcGFyZW50cy1wb3J0YWwvc2hhcmVkL3NpZGViYXIvc2lkZWJhci5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBQYXJlbnRVc2VyU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uL2NvcmUvaW5kZXgnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcblx0bW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuXHRzZWxlY3RvcjogJ3NpZGViYXItY21wJyxcclxuXHR0ZW1wbGF0ZVVybDogJ3NpZGViYXIuaHRtbCdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBTaWRlYmFyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcblx0aXNBY3RpdmUgPSBmYWxzZTtcclxuXHRzaG93TWVudTogc3RyaW5nID0gJyc7XHJcblx0aXNTeXNBZG1pbjogYm9vbGVhbiA9IGZhbHNlO1xyXG5cdGlzQ2VudHJlTWFuYWdlcjogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuXHRjb25zdHJ1Y3Rvcihwcml2YXRlIF91c2VyU2VydmljZTogUGFyZW50VXNlclNlcnZpY2UpIHtcclxuXHR9XHJcblxyXG5cdG5nT25Jbml0KCk6IHZvaWQgeyA7IH1cclxuXHJcblx0ZXZlbnRDYWxsZWQoKSB7XHJcblx0XHR0aGlzLmlzQWN0aXZlID0gIXRoaXMuaXNBY3RpdmU7XHJcblx0fVxyXG5cclxuXHRhZGRFeHBhbmRDbGFzcyhlbGVtZW50OiBhbnkpIHtcclxuXHRcdGlmIChlbGVtZW50ID09PSB0aGlzLnNob3dNZW51KSB7XHJcblx0XHRcdHRoaXMuc2hvd01lbnUgPSAnMCc7XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHR0aGlzLnNob3dNZW51ID0gZWxlbWVudDtcclxuXHRcdH1cclxuXHR9XHJcbn1cclxuIl19
