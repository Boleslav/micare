"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var rxjs_1 = require('rxjs');
var index_1 = require('../../../../core/index');
var index_2 = require('../../../../api/index');
var CentreService = (function () {
    function CentreService(_apiClient, _parentService) {
        this._apiClient = _apiClient;
        this._parentService = _parentService;
        this.ImageEncoding = 'data:image/png;base64,';
        this.logoDto = null;
        this.centreBrandingDto = null;
        this._selectedCentreQualifiedName = null;
        this._selectedCentreUniqueName = null;
        this._selectedCentreId = null;
    }
    Object.defineProperty(CentreService.prototype, "SelectedCentreQualifiedName", {
        get: function () {
            return this._selectedCentreQualifiedName;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CentreService.prototype, "SelectedCentreUniqueName", {
        get: function () {
            return this._selectedCentreUniqueName;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CentreService.prototype, "SelectedCentreId", {
        get: function () {
            return this._selectedCentreId;
        },
        enumerable: true,
        configurable: true
    });
    CentreService.prototype.getCentreBranding$ = function (uniquePath) {
        var _this = this;
        if (this.centreBrandingDto !== null) {
            return rxjs_1.Observable.of(this.centreBrandingDto);
        }
        return this.getCentreByUniquePath$(uniquePath)
            .flatMap(function (centreId) { return _this._apiClient.centre_GetCentreBranding(centreId); })
            .map(function (res) { return _this.centreBrandingDto = res; });
    };
    CentreService.prototype.getCentreByUniquePath$ = function (uniquePath) {
        var _this = this;
        if (this._selectedCentreId !== null) {
            return rxjs_1.Observable.of(this._selectedCentreId);
        }
        return this._apiClient
            .centre_GetCentreIdByUniqueName(uniquePath)
            .map(function (centre) {
            var centreId = Object.keys(centre)[0];
            var centreName = centre[centreId];
            _this._selectedCentreId = centreId;
            _this._selectedCentreQualifiedName = centreName;
            _this._selectedCentreUniqueName = uniquePath;
            _this._parentService.SetLastSelectedCentreName(_this._selectedCentreUniqueName);
        })
            .map(function () { return _this.SelectedCentreId; });
    };
    CentreService.prototype.getCentreLogo$ = function (uniqueName) {
        var _this = this;
        if (this.logoDto !== null) {
            return rxjs_1.Observable.of(this.logoDto);
        }
        return this._apiClient
            .centre_GetCentreLogoByUniqueName(uniqueName)
            .map(function (res) {
            _this.logoDto = res;
            _this.logoDto.imageBase64 = _this.ImageEncoding + _this.logoDto.imageBase64;
        })
            .map(function () { return _this.logoDto; });
    };
    CentreService.prototype.doesCentreExist$ = function (uniqueName) {
        return this
            ._apiClient
            .centre_CentreExistsByUniqueName(uniqueName);
    };
    CentreService.prototype.getParentEnrolmentForm$ = function (parentId) {
        return this._apiClient.enrolmentForm_IsAccessEnrolmentFormGranted(parentId, this.SelectedCentreId);
    };
    CentreService.prototype.logout = function () {
        this.logoDto = null;
        this.centreBrandingDto = null;
        this._selectedCentreId = null;
        this._selectedCentreUniqueName = null;
        this._selectedCentreQualifiedName = null;
    };
    CentreService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_2.ApiClientService, index_1.ParentUserService])
    ], CentreService);
    return CentreService;
}());
exports.CentreService = CentreService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9jb21wb25lbnRzL3NlcnZpY2VzL2NlbnRyZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBMkIsZUFBZSxDQUFDLENBQUE7QUFDM0MscUJBQTJCLE1BQU0sQ0FBQyxDQUFBO0FBRWxDLHNCQUFrQyx3QkFBd0IsQ0FBQyxDQUFBO0FBQzNELHNCQUFtRSx1QkFBdUIsQ0FBQyxDQUFBO0FBRzNGO0lBc0JJLHVCQUFvQixVQUE0QixFQUFVLGNBQWlDO1FBQXZFLGVBQVUsR0FBVixVQUFVLENBQWtCO1FBQVUsbUJBQWMsR0FBZCxjQUFjLENBQW1CO1FBcEIxRSxrQkFBYSxHQUFXLHdCQUF3QixDQUFDO1FBRTFELFlBQU8sR0FBa0IsSUFBSSxDQUFDO1FBQzlCLHNCQUFpQixHQUFzQixJQUFJLENBQUM7UUFFNUMsaUNBQTRCLEdBQVcsSUFBSSxDQUFDO1FBSzVDLDhCQUF5QixHQUFXLElBQUksQ0FBQztRQUt6QyxzQkFBaUIsR0FBVyxJQUFJLENBQUM7SUFLc0QsQ0FBQztJQWRoRyxzQkFBSSxzREFBMkI7YUFBL0I7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLDRCQUE0QixDQUFDO1FBQzdDLENBQUM7OztPQUFBO0lBR0Qsc0JBQUksbURBQXdCO2FBQTVCO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyx5QkFBeUIsQ0FBQztRQUMxQyxDQUFDOzs7T0FBQTtJQUdELHNCQUFJLDJDQUFnQjthQUFwQjtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUM7UUFDbEMsQ0FBQzs7O09BQUE7SUFJTSwwQ0FBa0IsR0FBekIsVUFBMEIsVUFBa0I7UUFBNUMsaUJBUUM7UUFORyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNsQyxNQUFNLENBQUMsaUJBQVUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7UUFDakQsQ0FBQztRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsVUFBVSxDQUFDO2FBQ3pDLE9BQU8sQ0FBQyxVQUFBLFFBQVEsSUFBSSxPQUFBLEtBQUksQ0FBQyxVQUFVLENBQUMsd0JBQXdCLENBQUMsUUFBUSxDQUFDLEVBQWxELENBQWtELENBQUM7YUFDdkUsR0FBRyxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsS0FBSSxDQUFDLGlCQUFpQixHQUFHLEdBQUcsRUFBNUIsQ0FBNEIsQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFFTSw4Q0FBc0IsR0FBN0IsVUFBOEIsVUFBa0I7UUFBaEQsaUJBZ0JDO1FBZEcsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDbEMsTUFBTSxDQUFDLGlCQUFVLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQ2pELENBQUM7UUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVU7YUFDakIsOEJBQThCLENBQUMsVUFBVSxDQUFDO2FBQzFDLEdBQUcsQ0FBQyxVQUFDLE1BQWlDO1lBQ25DLElBQUksUUFBUSxHQUFXLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDOUMsSUFBSSxVQUFVLEdBQVcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzFDLEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxRQUFRLENBQUM7WUFDbEMsS0FBSSxDQUFDLDRCQUE0QixHQUFHLFVBQVUsQ0FBQztZQUMvQyxLQUFJLENBQUMseUJBQXlCLEdBQUcsVUFBVSxDQUFDO1lBQzVDLEtBQUksQ0FBQyxjQUFjLENBQUMseUJBQXlCLENBQUMsS0FBSSxDQUFDLHlCQUF5QixDQUFDLENBQUM7UUFDbEYsQ0FBQyxDQUFDO2FBQ0QsR0FBRyxDQUFDLGNBQU0sT0FBQSxLQUFJLENBQUMsZ0JBQWdCLEVBQXJCLENBQXFCLENBQUMsQ0FBQztJQUMxQyxDQUFDO0lBRU0sc0NBQWMsR0FBckIsVUFBc0IsVUFBa0I7UUFBeEMsaUJBWUM7UUFWRyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDeEIsTUFBTSxDQUFDLGlCQUFVLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN2QyxDQUFDO1FBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVO2FBQ2pCLGdDQUFnQyxDQUFDLFVBQVUsQ0FBQzthQUM1QyxHQUFHLENBQUMsVUFBQSxHQUFHO1lBQ0osS0FBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUM7WUFDbkIsS0FBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEdBQUcsS0FBSSxDQUFDLGFBQWEsR0FBRyxLQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQztRQUM3RSxDQUFDLENBQUM7YUFDRCxHQUFHLENBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxPQUFPLEVBQVosQ0FBWSxDQUFDLENBQUM7SUFDakMsQ0FBQztJQUVNLHdDQUFnQixHQUF2QixVQUF3QixVQUFrQjtRQUN0QyxNQUFNLENBQUMsSUFBSTthQUNOLFVBQVU7YUFDViwrQkFBK0IsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBRU0sK0NBQXVCLEdBQTlCLFVBQStCLFFBQWdCO1FBQzNDLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLDBDQUEwQyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztJQUN2RyxDQUFDO0lBRU0sOEJBQU0sR0FBYjtRQUNJLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7UUFDOUIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztRQUM5QixJQUFJLENBQUMseUJBQXlCLEdBQUcsSUFBSSxDQUFDO1FBQ3RDLElBQUksQ0FBQyw0QkFBNEIsR0FBRyxJQUFJLENBQUM7SUFDN0MsQ0FBQztJQW5GTDtRQUFDLGlCQUFVLEVBQUU7O3FCQUFBO0lBb0ZiLG9CQUFDO0FBQUQsQ0FuRkEsQUFtRkMsSUFBQTtBQW5GWSxxQkFBYSxnQkFtRnpCLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL3BhcmVudHMtcG9ydGFsL2NvbXBvbmVudHMvc2VydmljZXMvY2VudHJlLnNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuXHJcbmltcG9ydCB7IFBhcmVudFVzZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vY29yZS9pbmRleCc7XHJcbmltcG9ydCB7IEFwaUNsaWVudFNlcnZpY2UsIENlbnRyZUxvZ29EdG8sIENlbnRyZUJyYW5kaW5nRHRvIH0gZnJvbSAnLi4vLi4vLi4vLi4vYXBpL2luZGV4JztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIENlbnRyZVNlcnZpY2Uge1xyXG5cclxuICAgIHByaXZhdGUgcmVhZG9ubHkgSW1hZ2VFbmNvZGluZzogc3RyaW5nID0gJ2RhdGE6aW1hZ2UvcG5nO2Jhc2U2NCwnO1xyXG5cclxuICAgIHByaXZhdGUgbG9nb0R0bzogQ2VudHJlTG9nb0R0byA9IG51bGw7XHJcbiAgICBwcml2YXRlIGNlbnRyZUJyYW5kaW5nRHRvOiBDZW50cmVCcmFuZGluZ0R0byA9IG51bGw7XHJcblxyXG4gICAgcHJpdmF0ZSBfc2VsZWN0ZWRDZW50cmVRdWFsaWZpZWROYW1lOiBzdHJpbmcgPSBudWxsO1xyXG4gICAgZ2V0IFNlbGVjdGVkQ2VudHJlUXVhbGlmaWVkTmFtZSgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9zZWxlY3RlZENlbnRyZVF1YWxpZmllZE5hbWU7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBfc2VsZWN0ZWRDZW50cmVVbmlxdWVOYW1lOiBzdHJpbmcgPSBudWxsO1xyXG4gICAgZ2V0IFNlbGVjdGVkQ2VudHJlVW5pcXVlTmFtZSgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9zZWxlY3RlZENlbnRyZVVuaXF1ZU5hbWU7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBfc2VsZWN0ZWRDZW50cmVJZDogc3RyaW5nID0gbnVsbDtcclxuICAgIGdldCBTZWxlY3RlZENlbnRyZUlkKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3NlbGVjdGVkQ2VudHJlSWQ7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfYXBpQ2xpZW50OiBBcGlDbGllbnRTZXJ2aWNlLCBwcml2YXRlIF9wYXJlbnRTZXJ2aWNlOiBQYXJlbnRVc2VyU2VydmljZSkgeyB9XHJcblxyXG4gICAgcHVibGljIGdldENlbnRyZUJyYW5kaW5nJCh1bmlxdWVQYXRoOiBzdHJpbmcpOiBPYnNlcnZhYmxlPENlbnRyZUJyYW5kaW5nRHRvPiB7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLmNlbnRyZUJyYW5kaW5nRHRvICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBPYnNlcnZhYmxlLm9mKHRoaXMuY2VudHJlQnJhbmRpbmdEdG8pO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpcy5nZXRDZW50cmVCeVVuaXF1ZVBhdGgkKHVuaXF1ZVBhdGgpXHJcbiAgICAgICAgICAgIC5mbGF0TWFwKGNlbnRyZUlkID0+IHRoaXMuX2FwaUNsaWVudC5jZW50cmVfR2V0Q2VudHJlQnJhbmRpbmcoY2VudHJlSWQpKVxyXG4gICAgICAgICAgICAubWFwKHJlcyA9PiB0aGlzLmNlbnRyZUJyYW5kaW5nRHRvID0gcmVzKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0Q2VudHJlQnlVbmlxdWVQYXRoJCh1bmlxdWVQYXRoOiBzdHJpbmcpOiBPYnNlcnZhYmxlPHN0cmluZz4ge1xyXG5cclxuICAgICAgICBpZiAodGhpcy5fc2VsZWN0ZWRDZW50cmVJZCAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICByZXR1cm4gT2JzZXJ2YWJsZS5vZih0aGlzLl9zZWxlY3RlZENlbnRyZUlkKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2FwaUNsaWVudFxyXG4gICAgICAgICAgICAuY2VudHJlX0dldENlbnRyZUlkQnlVbmlxdWVOYW1lKHVuaXF1ZVBhdGgpXHJcbiAgICAgICAgICAgIC5tYXAoKGNlbnRyZTogeyBba2V5OiBzdHJpbmddOiBzdHJpbmcgfSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgbGV0IGNlbnRyZUlkOiBzdHJpbmcgPSBPYmplY3Qua2V5cyhjZW50cmUpWzBdO1xyXG4gICAgICAgICAgICAgICAgbGV0IGNlbnRyZU5hbWU6IHN0cmluZyA9IGNlbnRyZVtjZW50cmVJZF07XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9zZWxlY3RlZENlbnRyZUlkID0gY2VudHJlSWQ7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9zZWxlY3RlZENlbnRyZVF1YWxpZmllZE5hbWUgPSBjZW50cmVOYW1lO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fc2VsZWN0ZWRDZW50cmVVbmlxdWVOYW1lID0gdW5pcXVlUGF0aDtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3BhcmVudFNlcnZpY2UuU2V0TGFzdFNlbGVjdGVkQ2VudHJlTmFtZSh0aGlzLl9zZWxlY3RlZENlbnRyZVVuaXF1ZU5hbWUpO1xyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAubWFwKCgpID0+IHRoaXMuU2VsZWN0ZWRDZW50cmVJZCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldENlbnRyZUxvZ28kKHVuaXF1ZU5hbWU6IHN0cmluZyk6IE9ic2VydmFibGU8Q2VudHJlTG9nb0R0bz4ge1xyXG5cclxuICAgICAgICBpZiAodGhpcy5sb2dvRHRvICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBPYnNlcnZhYmxlLm9mKHRoaXMubG9nb0R0byk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzLl9hcGlDbGllbnRcclxuICAgICAgICAgICAgLmNlbnRyZV9HZXRDZW50cmVMb2dvQnlVbmlxdWVOYW1lKHVuaXF1ZU5hbWUpXHJcbiAgICAgICAgICAgIC5tYXAocmVzID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMubG9nb0R0byA9IHJlcztcclxuICAgICAgICAgICAgICAgIHRoaXMubG9nb0R0by5pbWFnZUJhc2U2NCA9IHRoaXMuSW1hZ2VFbmNvZGluZyArIHRoaXMubG9nb0R0by5pbWFnZUJhc2U2NDtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLm1hcCgoKSA9PiB0aGlzLmxvZ29EdG8pO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBkb2VzQ2VudHJlRXhpc3QkKHVuaXF1ZU5hbWU6IHN0cmluZyk6IE9ic2VydmFibGU8Ym9vbGVhbj4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzXHJcbiAgICAgICAgICAgIC5fYXBpQ2xpZW50XHJcbiAgICAgICAgICAgIC5jZW50cmVfQ2VudHJlRXhpc3RzQnlVbmlxdWVOYW1lKHVuaXF1ZU5hbWUpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRQYXJlbnRFbnJvbG1lbnRGb3JtJChwYXJlbnRJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxib29sZWFuPiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2FwaUNsaWVudC5lbnJvbG1lbnRGb3JtX0lzQWNjZXNzRW5yb2xtZW50Rm9ybUdyYW50ZWQocGFyZW50SWQsIHRoaXMuU2VsZWN0ZWRDZW50cmVJZCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGxvZ291dCgpIHtcclxuICAgICAgICB0aGlzLmxvZ29EdG8gPSBudWxsO1xyXG4gICAgICAgIHRoaXMuY2VudHJlQnJhbmRpbmdEdG8gPSBudWxsO1xyXG4gICAgICAgIHRoaXMuX3NlbGVjdGVkQ2VudHJlSWQgPSBudWxsO1xyXG4gICAgICAgIHRoaXMuX3NlbGVjdGVkQ2VudHJlVW5pcXVlTmFtZSA9IG51bGw7XHJcbiAgICAgICAgdGhpcy5fc2VsZWN0ZWRDZW50cmVRdWFsaWZpZWROYW1lID0gbnVsbDtcclxuICAgIH1cclxufVxyXG4iXX0=
