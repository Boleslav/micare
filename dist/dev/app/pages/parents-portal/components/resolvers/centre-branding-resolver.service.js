"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var rxjs_1 = require('rxjs');
var index_1 = require('../../index');
var CentreBrandingResolver = (function () {
    function CentreBrandingResolver(_centreService) {
        this._centreService = _centreService;
    }
    CentreBrandingResolver.prototype.resolve = function (route, state) {
        var centreName = route.params['centreName'];
        if (centreName !== undefined) {
            return this._centreService.getCentreBranding$(centreName);
        }
        else {
            return rxjs_1.Observable.of(null);
        }
    };
    CentreBrandingResolver = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.CentreService])
    ], CentreBrandingResolver);
    return CentreBrandingResolver;
}());
exports.CentreBrandingResolver = CentreBrandingResolver;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9jb21wb25lbnRzL3Jlc29sdmVycy9jZW50cmUtYnJhbmRpbmctcmVzb2x2ZXIuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQTJCLGVBQWUsQ0FBQyxDQUFBO0FBRTNDLHFCQUEyQixNQUFNLENBQUMsQ0FBQTtBQUVsQyxzQkFBOEIsYUFBYSxDQUFDLENBQUE7QUFJNUM7SUFDSSxnQ0FBb0IsY0FBNkI7UUFBN0IsbUJBQWMsR0FBZCxjQUFjLENBQWU7SUFBSSxDQUFDO0lBRXRELHdDQUFPLEdBQVAsVUFDSSxLQUE2QixFQUM3QixLQUEwQjtRQUUxQixJQUFJLFVBQVUsR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQzVDLEVBQUUsQ0FBQyxDQUFDLFVBQVUsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQzNCLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLGtCQUFrQixDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzlELENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLE1BQU0sQ0FBQyxpQkFBVSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMvQixDQUFDO0lBQ0wsQ0FBQztJQWRMO1FBQUMsaUJBQVUsRUFBRTs7OEJBQUE7SUFlYiw2QkFBQztBQUFELENBZEEsQUFjQyxJQUFBO0FBZFksOEJBQXNCLHlCQWNsQyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9jb21wb25lbnRzL3Jlc29sdmVycy9jZW50cmUtYnJhbmRpbmctcmVzb2x2ZXIuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgUmVzb2x2ZSwgQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgUm91dGVyU3RhdGVTbmFwc2hvdCB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuXHJcbmltcG9ydCB7IENlbnRyZVNlcnZpY2UgfSBmcm9tICcuLi8uLi9pbmRleCc7XHJcbmltcG9ydCB7IENlbnRyZUJyYW5kaW5nRHRvIH0gZnJvbSAnLi4vLi4vLi4vLi4vYXBpL2luZGV4JztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIENlbnRyZUJyYW5kaW5nUmVzb2x2ZXIgaW1wbGVtZW50cyBSZXNvbHZlPENlbnRyZUJyYW5kaW5nRHRvPiB7XHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9jZW50cmVTZXJ2aWNlOiBDZW50cmVTZXJ2aWNlKSB7IH1cclxuXHJcbiAgICByZXNvbHZlKFxyXG4gICAgICAgIHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LFxyXG4gICAgICAgIHN0YXRlOiBSb3V0ZXJTdGF0ZVNuYXBzaG90XHJcbiAgICApOiBPYnNlcnZhYmxlPENlbnRyZUJyYW5kaW5nRHRvPiB7XHJcbiAgICAgICAgbGV0IGNlbnRyZU5hbWUgPSByb3V0ZS5wYXJhbXNbJ2NlbnRyZU5hbWUnXTtcclxuICAgICAgICBpZiAoY2VudHJlTmFtZSAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLl9jZW50cmVTZXJ2aWNlLmdldENlbnRyZUJyYW5kaW5nJChjZW50cmVOYW1lKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gT2JzZXJ2YWJsZS5vZihudWxsKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19
