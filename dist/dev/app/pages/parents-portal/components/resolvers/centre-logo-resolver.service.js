"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var rxjs_1 = require('rxjs');
var index_1 = require('../../index');
var CentreLogoResolver = (function () {
    function CentreLogoResolver(_centreService) {
        this._centreService = _centreService;
    }
    CentreLogoResolver.prototype.resolve = function (route, state) {
        var centreName = route.params['centreName'];
        if (centreName !== undefined) {
            return this._centreService.getCentreLogo$(centreName);
        }
        else {
            return rxjs_1.Observable.of(null);
        }
    };
    CentreLogoResolver = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.CentreService])
    ], CentreLogoResolver);
    return CentreLogoResolver;
}());
exports.CentreLogoResolver = CentreLogoResolver;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9jb21wb25lbnRzL3Jlc29sdmVycy9jZW50cmUtbG9nby1yZXNvbHZlci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBMkIsZUFBZSxDQUFDLENBQUE7QUFFM0MscUJBQTJCLE1BQU0sQ0FBQyxDQUFBO0FBRWxDLHNCQUE4QixhQUFhLENBQUMsQ0FBQTtBQUk1QztJQUNJLDRCQUFvQixjQUE2QjtRQUE3QixtQkFBYyxHQUFkLGNBQWMsQ0FBZTtJQUFJLENBQUM7SUFDdEQsb0NBQU8sR0FBUCxVQUNJLEtBQTZCLEVBQzdCLEtBQTBCO1FBRTFCLElBQUksVUFBVSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDNUMsRUFBRSxDQUFDLENBQUMsVUFBVSxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDM0IsTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzFELENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLE1BQU0sQ0FBQyxpQkFBVSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMvQixDQUFDO0lBQ0wsQ0FBQztJQWJMO1FBQUMsaUJBQVUsRUFBRTs7MEJBQUE7SUFjYix5QkFBQztBQUFELENBYkEsQUFhQyxJQUFBO0FBYlksMEJBQWtCLHFCQWE5QixDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9jb21wb25lbnRzL3Jlc29sdmVycy9jZW50cmUtbG9nby1yZXNvbHZlci5zZXJ2aWNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBSZXNvbHZlLCBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBSb3V0ZXJTdGF0ZVNuYXBzaG90IH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgQ2VudHJlU2VydmljZSB9IGZyb20gJy4uLy4uL2luZGV4JztcclxuaW1wb3J0IHsgQ2VudHJlTG9nb0R0byB9IGZyb20gJy4uLy4uLy4uLy4uL2FwaS9pbmRleCc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBDZW50cmVMb2dvUmVzb2x2ZXIgaW1wbGVtZW50cyBSZXNvbHZlPENlbnRyZUxvZ29EdG8+IHtcclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX2NlbnRyZVNlcnZpY2U6IENlbnRyZVNlcnZpY2UpIHsgfVxyXG4gICAgcmVzb2x2ZShcclxuICAgICAgICByb3V0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCxcclxuICAgICAgICBzdGF0ZTogUm91dGVyU3RhdGVTbmFwc2hvdFxyXG4gICAgKTogT2JzZXJ2YWJsZTxDZW50cmVMb2dvRHRvPiB7XHJcbiAgICAgICAgbGV0IGNlbnRyZU5hbWUgPSByb3V0ZS5wYXJhbXNbJ2NlbnRyZU5hbWUnXTtcclxuICAgICAgICBpZiAoY2VudHJlTmFtZSAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLl9jZW50cmVTZXJ2aWNlLmdldENlbnRyZUxvZ28kKGNlbnRyZU5hbWUpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiBPYnNlcnZhYmxlLm9mKG51bGwpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=
