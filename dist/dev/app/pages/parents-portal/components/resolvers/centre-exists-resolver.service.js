"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var rxjs_1 = require('rxjs');
var index_1 = require('../../index');
var CentreExistsResolver = (function () {
    function CentreExistsResolver(_centreService) {
        this._centreService = _centreService;
    }
    CentreExistsResolver.prototype.resolve = function (route, state) {
        var centreName = route.params['centreName'];
        if (centreName !== undefined) {
            return this._centreService.doesCentreExist$(centreName);
        }
        else {
            return rxjs_1.Observable.of(false);
        }
    };
    CentreExistsResolver = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.CentreService])
    ], CentreExistsResolver);
    return CentreExistsResolver;
}());
exports.CentreExistsResolver = CentreExistsResolver;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9jb21wb25lbnRzL3Jlc29sdmVycy9jZW50cmUtZXhpc3RzLXJlc29sdmVyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUEyQixlQUFlLENBQUMsQ0FBQTtBQUUzQyxxQkFBMkIsTUFBTSxDQUFDLENBQUE7QUFFbEMsc0JBQThCLGFBQWEsQ0FBQyxDQUFBO0FBRzVDO0lBQ0ksOEJBQW9CLGNBQTZCO1FBQTdCLG1CQUFjLEdBQWQsY0FBYyxDQUFlO0lBQUksQ0FBQztJQUN0RCxzQ0FBTyxHQUFQLFVBQ0ksS0FBNkIsRUFDN0IsS0FBMEI7UUFFMUIsSUFBSSxVQUFVLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUM1QyxFQUFFLENBQUMsQ0FBQyxVQUFVLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQztZQUMzQixNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUM1RCxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixNQUFNLENBQUMsaUJBQVUsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDaEMsQ0FBQztJQUNMLENBQUM7SUFiTDtRQUFDLGlCQUFVLEVBQUU7OzRCQUFBO0lBY2IsMkJBQUM7QUFBRCxDQWJBLEFBYUMsSUFBQTtBQWJZLDRCQUFvQix1QkFhaEMsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvcGFyZW50cy1wb3J0YWwvY29tcG9uZW50cy9yZXNvbHZlcnMvY2VudHJlLWV4aXN0cy1yZXNvbHZlci5zZXJ2aWNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBSZXNvbHZlLCBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBSb3V0ZXJTdGF0ZVNuYXBzaG90IH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgQ2VudHJlU2VydmljZSB9IGZyb20gJy4uLy4uL2luZGV4JztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIENlbnRyZUV4aXN0c1Jlc29sdmVyIGltcGxlbWVudHMgUmVzb2x2ZTxib29sZWFuPiB7XHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9jZW50cmVTZXJ2aWNlOiBDZW50cmVTZXJ2aWNlKSB7IH1cclxuICAgIHJlc29sdmUoXHJcbiAgICAgICAgcm91dGU6IEFjdGl2YXRlZFJvdXRlU25hcHNob3QsXHJcbiAgICAgICAgc3RhdGU6IFJvdXRlclN0YXRlU25hcHNob3RcclxuICAgICk6IE9ic2VydmFibGU8Ym9vbGVhbj4ge1xyXG4gICAgICAgIGxldCBjZW50cmVOYW1lID0gcm91dGUucGFyYW1zWydjZW50cmVOYW1lJ107XHJcbiAgICAgICAgaWYgKGNlbnRyZU5hbWUgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5fY2VudHJlU2VydmljZS5kb2VzQ2VudHJlRXhpc3QkKGNlbnRyZU5hbWUpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiBPYnNlcnZhYmxlLm9mKGZhbHNlKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19
