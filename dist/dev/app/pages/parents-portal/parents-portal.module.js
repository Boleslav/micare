"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../shared/index');
var index_2 = require('./index');
var index_3 = require('./shared/index');
var parents_portal_component_1 = require('./parents-portal.component');
var index_4 = require('./parent-signup/index');
var index_5 = require('./parent-login/index');
var index_6 = require('./parent-logout/index');
var index_7 = require('./enrolment/index');
var index_8 = require('./centre-not-found/index');
var ParentsPortalModule = (function () {
    function ParentsPortalModule() {
    }
    ParentsPortalModule = __decorate([
        core_1.NgModule({
            imports: [
                index_1.SharedModule,
                index_4.ParentSignupModule,
                index_5.ParentLoginModule,
                index_6.ParentLogoutModule,
                index_7.EnrolmentModule,
                index_8.CentreNotFoundModule
            ],
            providers: [
                index_2.CentreService,
                index_2.CentreExistsResolver,
                index_2.CentreLogoResolver,
                index_2.CentreBrandingResolver
            ],
            declarations: [
                index_3.TopNavComponent,
                index_3.SidebarComponent,
                parents_portal_component_1.ParentsPortalComponent
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], ParentsPortalModule);
    return ParentsPortalModule;
}());
exports.ParentsPortalModule = ParentsPortalModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9wYXJlbnRzLXBvcnRhbC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUF5QixlQUFlLENBQUMsQ0FBQTtBQUN6QyxzQkFBNkIsb0JBQW9CLENBQUMsQ0FBQTtBQUVsRCxzQkFHTyxTQUFTLENBQUMsQ0FBQTtBQUNqQixzQkFBa0QsZ0JBQWdCLENBQUMsQ0FBQTtBQUNuRSx5Q0FBdUMsNEJBQTRCLENBQUMsQ0FBQTtBQUNwRSxzQkFBbUMsdUJBQXVCLENBQUMsQ0FBQTtBQUMzRCxzQkFBa0Msc0JBQXNCLENBQUMsQ0FBQTtBQUN6RCxzQkFBbUMsdUJBQXVCLENBQUMsQ0FBQTtBQUMzRCxzQkFBZ0MsbUJBQW1CLENBQUMsQ0FBQTtBQUNwRCxzQkFBcUMsMEJBQTBCLENBQUMsQ0FBQTtBQXVCaEU7SUFBQTtJQUFtQyxDQUFDO0lBckJwQztRQUFDLGVBQVEsQ0FBQztZQUNOLE9BQU8sRUFBRTtnQkFDTCxvQkFBWTtnQkFDWiwwQkFBa0I7Z0JBQ2xCLHlCQUFpQjtnQkFDakIsMEJBQWtCO2dCQUNsQix1QkFBZTtnQkFDZiw0QkFBb0I7YUFDdkI7WUFDRCxTQUFTLEVBQUU7Z0JBQ1AscUJBQWE7Z0JBQ2IsNEJBQW9CO2dCQUNwQiwwQkFBa0I7Z0JBQ2xCLDhCQUFzQjthQUN6QjtZQUNELFlBQVksRUFBRTtnQkFDVix1QkFBZTtnQkFDZix3QkFBZ0I7Z0JBQ2hCLGlEQUFzQjthQUN6QjtTQUNKLENBQUM7OzJCQUFBO0lBQ2lDLDBCQUFDO0FBQUQsQ0FBbkMsQUFBb0MsSUFBQTtBQUF2QiwyQkFBbUIsc0JBQUksQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvcGFyZW50cy1wb3J0YWwvcGFyZW50cy1wb3J0YWwubW9kdWxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU2hhcmVkTW9kdWxlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL2luZGV4JztcclxuXHJcbmltcG9ydCB7XHJcbiAgICBDZW50cmVTZXJ2aWNlLCBDZW50cmVMb2dvUmVzb2x2ZXIsXHJcbiAgICBDZW50cmVCcmFuZGluZ1Jlc29sdmVyLCBDZW50cmVFeGlzdHNSZXNvbHZlclxyXG59IGZyb20gJy4vaW5kZXgnO1xyXG5pbXBvcnQgeyBUb3BOYXZDb21wb25lbnQsIFNpZGViYXJDb21wb25lbnQgfSBmcm9tICcuL3NoYXJlZC9pbmRleCc7XHJcbmltcG9ydCB7IFBhcmVudHNQb3J0YWxDb21wb25lbnQgfSBmcm9tICcuL3BhcmVudHMtcG9ydGFsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFBhcmVudFNpZ251cE1vZHVsZSB9IGZyb20gJy4vcGFyZW50LXNpZ251cC9pbmRleCc7XHJcbmltcG9ydCB7IFBhcmVudExvZ2luTW9kdWxlIH0gZnJvbSAnLi9wYXJlbnQtbG9naW4vaW5kZXgnO1xyXG5pbXBvcnQgeyBQYXJlbnRMb2dvdXRNb2R1bGUgfSBmcm9tICcuL3BhcmVudC1sb2dvdXQvaW5kZXgnO1xyXG5pbXBvcnQgeyBFbnJvbG1lbnRNb2R1bGUgfSBmcm9tICcuL2Vucm9sbWVudC9pbmRleCc7XHJcbmltcG9ydCB7IENlbnRyZU5vdEZvdW5kTW9kdWxlIH0gZnJvbSAnLi9jZW50cmUtbm90LWZvdW5kL2luZGV4JztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiBbXHJcbiAgICAgICAgU2hhcmVkTW9kdWxlLFxyXG4gICAgICAgIFBhcmVudFNpZ251cE1vZHVsZSxcclxuICAgICAgICBQYXJlbnRMb2dpbk1vZHVsZSxcclxuICAgICAgICBQYXJlbnRMb2dvdXRNb2R1bGUsXHJcbiAgICAgICAgRW5yb2xtZW50TW9kdWxlLFxyXG4gICAgICAgIENlbnRyZU5vdEZvdW5kTW9kdWxlXHJcbiAgICBdLFxyXG4gICAgcHJvdmlkZXJzOiBbXHJcbiAgICAgICAgQ2VudHJlU2VydmljZSxcclxuICAgICAgICBDZW50cmVFeGlzdHNSZXNvbHZlcixcclxuICAgICAgICBDZW50cmVMb2dvUmVzb2x2ZXIsXHJcbiAgICAgICAgQ2VudHJlQnJhbmRpbmdSZXNvbHZlclxyXG4gICAgXSxcclxuICAgIGRlY2xhcmF0aW9uczogW1xyXG4gICAgICAgIFRvcE5hdkNvbXBvbmVudCxcclxuICAgICAgICBTaWRlYmFyQ29tcG9uZW50LFxyXG4gICAgICAgIFBhcmVudHNQb3J0YWxDb21wb25lbnRcclxuICAgIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIFBhcmVudHNQb3J0YWxNb2R1bGUgeyB9XHJcbiJdfQ==
