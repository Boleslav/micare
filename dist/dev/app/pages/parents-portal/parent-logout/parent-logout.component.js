"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var index_1 = require('../index');
var index_2 = require('../../../core/index');
var ParentLogoutComponent = (function () {
    function ParentLogoutComponent(_authenticationService, _userService, _centreService, _route) {
        this._authenticationService = _authenticationService;
        this._userService = _userService;
        this._centreService = _centreService;
        this._route = _route;
        this.centreName = null;
        this.logo = null;
    }
    ParentLogoutComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route
            .data
            .subscribe(function (data) {
            if (data.centreLogo.imageBase64) {
                _this.logo = data.centreLogo.imageBase64;
            }
        });
        this.centreName = this._centreService.SelectedCentreUniqueName;
        this._userService.Clear();
        this._centreService.logout();
        this._authenticationService.logout();
    };
    ParentLogoutComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'parent-logout-cmp',
            templateUrl: 'parent-logout.component.html'
        }), 
        __metadata('design:paramtypes', [index_2.AccessTokenService, index_2.ParentUserService, index_1.CentreService, router_1.ActivatedRoute])
    ], ParentLogoutComponent);
    return ParentLogoutComponent;
}());
exports.ParentLogoutComponent = ParentLogoutComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9wYXJlbnQtbG9nb3V0L3BhcmVudC1sb2dvdXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBa0MsZUFBZSxDQUFDLENBQUE7QUFDbEQsdUJBQStCLGlCQUFpQixDQUFDLENBQUE7QUFFakQsc0JBQThCLFVBQVUsQ0FBQyxDQUFBO0FBQ3pDLHNCQUFzRCxxQkFBcUIsQ0FBQyxDQUFBO0FBUzVFO0lBS0ksK0JBQ1ksc0JBQTBDLEVBQzFDLFlBQStCLEVBQy9CLGNBQTZCLEVBQzdCLE1BQXNCO1FBSHRCLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBb0I7UUFDMUMsaUJBQVksR0FBWixZQUFZLENBQW1CO1FBQy9CLG1CQUFjLEdBQWQsY0FBYyxDQUFlO1FBQzdCLFdBQU0sR0FBTixNQUFNLENBQWdCO1FBUDFCLGVBQVUsR0FBVyxJQUFJLENBQUM7UUFDMUIsU0FBSSxHQUFXLElBQUksQ0FBQztJQU81QixDQUFDO0lBRUQsd0NBQVEsR0FBUjtRQUFBLGlCQWVDO1FBYkcsSUFBSSxDQUFDLE1BQU07YUFDTixJQUFJO2FBQ0osU0FBUyxDQUFDLFVBQUMsSUFBbUM7WUFDM0MsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO2dCQUM5QixLQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDO1lBQzVDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUVQLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyx3QkFBd0IsQ0FBQztRQUUvRCxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQzFCLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDN0IsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ3pDLENBQUM7SUFqQ0w7UUFBQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxtQkFBbUI7WUFDN0IsV0FBVyxFQUFFLDhCQUE4QjtTQUM5QyxDQUFDOzs2QkFBQTtJQThCRiw0QkFBQztBQUFELENBNUJBLEFBNEJDLElBQUE7QUE1QlksNkJBQXFCLHdCQTRCakMsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvcGFyZW50cy1wb3J0YWwvcGFyZW50LWxvZ291dC9wYXJlbnQtbG9nb3V0LmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuXHJcbmltcG9ydCB7IENlbnRyZVNlcnZpY2UgfSBmcm9tICcuLi9pbmRleCc7XHJcbmltcG9ydCB7IFBhcmVudFVzZXJTZXJ2aWNlLCBBY2Nlc3NUb2tlblNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9jb3JlL2luZGV4JztcclxuaW1wb3J0IHsgQ2VudHJlTG9nb0R0byB9IGZyb20gJy4uLy4uLy4uL2FwaS9pbmRleCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICBzZWxlY3RvcjogJ3BhcmVudC1sb2dvdXQtY21wJyxcclxuICAgIHRlbXBsYXRlVXJsOiAncGFyZW50LWxvZ291dC5jb21wb25lbnQuaHRtbCdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBQYXJlbnRMb2dvdXRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIHByaXZhdGUgY2VudHJlTmFtZTogc3RyaW5nID0gbnVsbDtcclxuICAgIHByaXZhdGUgbG9nbzogc3RyaW5nID0gbnVsbDtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwcml2YXRlIF9hdXRoZW50aWNhdGlvblNlcnZpY2U6IEFjY2Vzc1Rva2VuU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIF91c2VyU2VydmljZTogUGFyZW50VXNlclNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBfY2VudHJlU2VydmljZTogQ2VudHJlU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIF9yb3V0ZTogQWN0aXZhdGVkUm91dGUpIHtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuXHJcbiAgICAgICAgdGhpcy5fcm91dGVcclxuICAgICAgICAgICAgLmRhdGFcclxuICAgICAgICAgICAgLnN1YnNjcmliZSgoZGF0YTogeyBjZW50cmVMb2dvOiBDZW50cmVMb2dvRHRvIH0pID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChkYXRhLmNlbnRyZUxvZ28uaW1hZ2VCYXNlNjQpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvZ28gPSBkYXRhLmNlbnRyZUxvZ28uaW1hZ2VCYXNlNjQ7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICB0aGlzLmNlbnRyZU5hbWUgPSB0aGlzLl9jZW50cmVTZXJ2aWNlLlNlbGVjdGVkQ2VudHJlVW5pcXVlTmFtZTtcclxuXHJcbiAgICAgICAgdGhpcy5fdXNlclNlcnZpY2UuQ2xlYXIoKTtcclxuICAgICAgICB0aGlzLl9jZW50cmVTZXJ2aWNlLmxvZ291dCgpO1xyXG4gICAgICAgIHRoaXMuX2F1dGhlbnRpY2F0aW9uU2VydmljZS5sb2dvdXQoKTtcclxuICAgIH1cclxufVxyXG4iXX0=
