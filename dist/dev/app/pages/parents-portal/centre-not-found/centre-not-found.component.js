"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var CentreNotFoundComponent = (function () {
    function CentreNotFoundComponent(_route) {
        this._route = _route;
        this.centreNameNotFound = null;
    }
    CentreNotFoundComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route
            .params
            .subscribe(function (params) {
            var centreName = params['centreName'];
            if (centreName !== undefined && centreName !== null && centreName !== '')
                _this.centreNameNotFound = centreName;
        });
    };
    CentreNotFoundComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'centre-not-found-cmp',
            templateUrl: 'centre-not-found.component.html',
            styleUrls: ['centre-not-found.component.css']
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute])
    ], CentreNotFoundComponent);
    return CentreNotFoundComponent;
}());
exports.CentreNotFoundComponent = CentreNotFoundComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9jZW50cmUtbm90LWZvdW5kL2NlbnRyZS1ub3QtZm91bmQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBa0MsZUFBZSxDQUFDLENBQUE7QUFDbEQsdUJBQXVDLGlCQUFpQixDQUFDLENBQUE7QUFTekQ7SUFHRSxpQ0FBb0IsTUFBc0I7UUFBdEIsV0FBTSxHQUFOLE1BQU0sQ0FBZ0I7UUFEbEMsdUJBQWtCLEdBQVcsSUFBSSxDQUFDO0lBRTFDLENBQUM7SUFFRCwwQ0FBUSxHQUFSO1FBQUEsaUJBUUM7UUFQQyxJQUFJLENBQUMsTUFBTTthQUNSLE1BQU07YUFDTixTQUFTLENBQUMsVUFBQyxNQUFjO1lBQ3hCLElBQUksVUFBVSxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUN0QyxFQUFFLENBQUMsQ0FBQyxVQUFVLEtBQUssU0FBUyxJQUFJLFVBQVUsS0FBSyxJQUFJLElBQUksVUFBVSxLQUFLLEVBQUUsQ0FBQztnQkFDdkUsS0FBSSxDQUFDLGtCQUFrQixHQUFHLFVBQVUsQ0FBQztRQUN6QyxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFyQkg7UUFBQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxzQkFBc0I7WUFDaEMsV0FBVyxFQUFFLGlDQUFpQztZQUM5QyxTQUFTLEVBQUUsQ0FBQyxnQ0FBZ0MsQ0FBQztTQUM5QyxDQUFDOzsrQkFBQTtJQWlCRiw4QkFBQztBQUFELENBZkEsQUFlQyxJQUFBO0FBZlksK0JBQXVCLDBCQWVuQyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9wYXJlbnRzLXBvcnRhbC9jZW50cmUtbm90LWZvdW5kL2NlbnRyZS1ub3QtZm91bmQuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgUGFyYW1zLCBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gIHNlbGVjdG9yOiAnY2VudHJlLW5vdC1mb3VuZC1jbXAnLFxyXG4gIHRlbXBsYXRlVXJsOiAnY2VudHJlLW5vdC1mb3VuZC5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJ2NlbnRyZS1ub3QtZm91bmQuY29tcG9uZW50LmNzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgQ2VudHJlTm90Rm91bmRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBwcml2YXRlIGNlbnRyZU5hbWVOb3RGb3VuZDogc3RyaW5nID0gbnVsbDtcclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9yb3V0ZTogQWN0aXZhdGVkUm91dGUpIHtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgdGhpcy5fcm91dGVcclxuICAgICAgLnBhcmFtc1xyXG4gICAgICAuc3Vic2NyaWJlKChwYXJhbXM6IFBhcmFtcykgPT4ge1xyXG4gICAgICAgIGxldCBjZW50cmVOYW1lID0gcGFyYW1zWydjZW50cmVOYW1lJ107XHJcbiAgICAgICAgaWYgKGNlbnRyZU5hbWUgIT09IHVuZGVmaW5lZCAmJiBjZW50cmVOYW1lICE9PSBudWxsICYmIGNlbnRyZU5hbWUgIT09ICcnKVxyXG4gICAgICAgICAgdGhpcy5jZW50cmVOYW1lTm90Rm91bmQgPSBjZW50cmVOYW1lO1xyXG4gICAgICB9KTtcclxuICB9XHJcbn1cclxuIl19
