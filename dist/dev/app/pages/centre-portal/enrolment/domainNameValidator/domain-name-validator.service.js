"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var rxjs_1 = require('rxjs');
var index_1 = require('../../../../api/index');
var DomainNameValidatorService = (function () {
    function DomainNameValidatorService(_apiClient) {
        this._apiClient = _apiClient;
        this.searchTermStream = new rxjs_1.Subject();
    }
    DomainNameValidatorService.prototype.ValidateAsync = function (name) {
        this.searchTermStream.next(name);
    };
    DomainNameValidatorService.prototype.isDomainNameValid$ = function (centreId) {
        var _this = this;
        return this.searchTermStream
            .debounceTime(300)
            .filter(function (val) { return val !== ''; })
            .switchMap(function (term) {
            return _this._apiClient
                .centre_GetDomainPathAvailability(centreId, term);
        });
    };
    DomainNameValidatorService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.ApiClientService])
    ], DomainNameValidatorService);
    return DomainNameValidatorService;
}());
exports.DomainNameValidatorService = DomainNameValidatorService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2Vucm9sbWVudC9kb21haW5OYW1lVmFsaWRhdG9yL2RvbWFpbi1uYW1lLXZhbGlkYXRvci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBMkIsZUFBZSxDQUFDLENBQUE7QUFDM0MscUJBQW9DLE1BQU0sQ0FBQyxDQUFBO0FBQzNDLHNCQUFpQyx1QkFBdUIsQ0FBQyxDQUFBO0FBR3pEO0lBSUksb0NBQW9CLFVBQTRCO1FBQTVCLGVBQVUsR0FBVixVQUFVLENBQWtCO1FBQzVDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLGNBQU8sRUFBVSxDQUFDO0lBQ2xELENBQUM7SUFFTSxrREFBYSxHQUFwQixVQUFxQixJQUFZO1FBQzdCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDckMsQ0FBQztJQUVNLHVEQUFrQixHQUF6QixVQUEwQixRQUFnQjtRQUExQyxpQkFRQztRQVBHLE1BQU0sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCO2FBQ3ZCLFlBQVksQ0FBQyxHQUFHLENBQUM7YUFDakIsTUFBTSxDQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsR0FBRyxLQUFLLEVBQUUsRUFBVixDQUFVLENBQUM7YUFDM0IsU0FBUyxDQUFDLFVBQUMsSUFBWTtZQUNwQixNQUFNLENBQUMsS0FBSSxDQUFDLFVBQVU7aUJBQ2pCLGdDQUFnQyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUMxRCxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFyQkw7UUFBQyxpQkFBVSxFQUFFOztrQ0FBQTtJQXVCYixpQ0FBQztBQUFELENBdEJBLEFBc0JDLElBQUE7QUF0Qlksa0NBQTBCLDZCQXNCdEMsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9lbnJvbG1lbnQvZG9tYWluTmFtZVZhbGlkYXRvci9kb21haW4tbmFtZS12YWxpZGF0b3Iuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBBcGlDbGllbnRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vYXBpL2luZGV4JztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIERvbWFpbk5hbWVWYWxpZGF0b3JTZXJ2aWNlIHtcclxuXHJcbiAgICBwcml2YXRlIHNlYXJjaFRlcm1TdHJlYW06IFN1YmplY3Q8c3RyaW5nPjtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9hcGlDbGllbnQ6IEFwaUNsaWVudFNlcnZpY2UpIHtcclxuICAgICAgICB0aGlzLnNlYXJjaFRlcm1TdHJlYW0gPSBuZXcgU3ViamVjdDxzdHJpbmc+KCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIFZhbGlkYXRlQXN5bmMobmFtZTogc3RyaW5nKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5zZWFyY2hUZXJtU3RyZWFtLm5leHQobmFtZSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGlzRG9tYWluTmFtZVZhbGlkJChjZW50cmVJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxib29sZWFuPiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VhcmNoVGVybVN0cmVhbVxyXG4gICAgICAgICAgICAuZGVib3VuY2VUaW1lKDMwMClcclxuICAgICAgICAgICAgLmZpbHRlcigodmFsKSA9PiB2YWwgIT09ICcnKVxyXG4gICAgICAgICAgICAuc3dpdGNoTWFwKCh0ZXJtOiBzdHJpbmcpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLl9hcGlDbGllbnRcclxuICAgICAgICAgICAgICAgICAgICAuY2VudHJlX0dldERvbWFpblBhdGhBdmFpbGFiaWxpdHkoY2VudHJlSWQsIHRlcm0pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19
