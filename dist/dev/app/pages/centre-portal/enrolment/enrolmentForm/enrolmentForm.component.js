"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var angular2_toaster_1 = require('angular2-toaster');
var ngx_uploader_1 = require('ngx-uploader');
var angular2_color_picker_1 = require('angular2-color-picker');
var index_1 = require('../index');
var index_2 = require('../../../../api/index');
var index_3 = require('../../../../core/index');
var index_4 = require('../../../../core/index');
var index_5 = require('../../../../shared/index');
var EnrolmentFormComponent = (function () {
    function EnrolmentFormComponent(fb, _enrolmentService, _domainValidator, _authService, _userService, _toasterService, _colorPickerService, _element) {
        this._enrolmentService = _enrolmentService;
        this._domainValidator = _domainValidator;
        this._authService = _authService;
        this._userService = _userService;
        this._toasterService = _toasterService;
        this._colorPickerService = _colorPickerService;
        this._element = _element;
        this.originUrl = null;
        this.enrolment = null;
        this.centreLogo = null;
        this.imageUploading = false;
        this.logoName = null;
        this.logoBase64 = null;
        this.ImageEncoding = 'data:image/png;base64,';
        this.DefaultColor = '#944ea6';
        this.submitted = false;
        this.originUrl = index_5.Config.API + '/';
        this.approvalStatus = -1;
        this.form = fb.group({
            'domainPath': ['',
                forms_1.Validators.compose([
                    forms_1.Validators.required,
                    forms_1.Validators.maxLength(100),
                    forms_1.Validators.pattern('[A-Za-z0-9]*')])],
            'additionalEnrolmentFormComments': ['', forms_1.Validators.maxLength(4000)]
        });
        this.domainPath = this.form.controls['domainPath'];
        this.additionalEnrolmentFormComments = this.form.controls['additionalEnrolmentFormComments'];
        this.uploaderOptions = new ngx_uploader_1.NgUploaderOptions({
            url: this.originUrl + 'api/Centre/UploadCentreLogo?centreId=' + this._userService.SelectedCentre.id,
            authToken: this._authService.getAccessToken(),
            calculateSpeed: true,
            filterExtensions: true,
            allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
            autoUpload: true
        });
    }
    EnrolmentFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.validatorSubscription = this.subscribeDomainNameValidator();
        this._enrolmentService
            .GetCentreBranding$(this._userService.SelectedCentre.id)
            .subscribe(function (res) {
            _this.enrolment = index_2.CreateUpdateCentreBrandingDto.fromJS(res.toJSON());
            if (_this.enrolment.baseColour === undefined || _this.enrolment.baseColour === null) {
                _this.enrolment.baseColour = _this.DefaultColor;
            }
        });
        this.getCentreLogo();
    };
    EnrolmentFormComponent.prototype.getCentreLogo = function () {
        var _this = this;
        this.centreLogo = null;
        this._enrolmentService
            .GetCentreLogo$(this._userService.SelectedCentre.id)
            .subscribe(function (res) {
            _this.centreLogo = res;
            if (_this.centreLogo.imageBase64 !== undefined &&
                _this.centreLogo.imageBase64 !== null &&
                _this.centreLogo.imageBase64 !== '') {
                _this.logoName = 'Click Here to Upload Logo';
                _this.logoBase64 = _this.ImageEncoding + _this.centreLogo.imageBase64;
            }
        }, function (err) {
            _this.centreLogo = new index_2.CentreLogoDto();
        });
    };
    EnrolmentFormComponent.prototype.validateDomainPath = function (val) {
        if (this.domainPath.errors === null) {
            if (this.validatorSubscription.closed)
                this.validatorSubscription = this.subscribeDomainNameValidator();
            this.domainPath.markAsPending();
            this._domainValidator.ValidateAsync(val);
        }
        else {
            this.validatorSubscription.unsubscribe();
        }
    };
    EnrolmentFormComponent.prototype.subscribeDomainNameValidator = function () {
        var _this = this;
        return this._domainValidator
            .isDomainNameValid$(this._userService.SelectedCentre.id)
            .subscribe(function (res) {
            var isValid = res;
            var validity = isValid ? null : { 'domainPathTaken': true };
            if (_this.domainPath.value)
                _this.domainPath.setErrors(validity);
            _this.domainPath.markAsDirty();
        });
    };
    EnrolmentFormComponent.prototype.hasSuccess = function (control) {
        return (control.touched || control.dirty || control.value) && control.valid;
    };
    EnrolmentFormComponent.prototype.hasError = function (control) {
        return (control.touched || control.dirty) && control.invalid;
    };
    EnrolmentFormComponent.prototype.hasWarning = function (control) {
        return control.pending;
    };
    EnrolmentFormComponent.prototype.onLogoUpload = function (event) {
        if (!this.imageUploading) {
            this.logoName = 'Click Here to Upload Logo';
            this._toasterService.pop(new index_5.SpinningToast());
            this.imageUploading = true;
        }
    };
    EnrolmentFormComponent.prototype.onLogoUploadComplete = function (event) {
        this._toasterService.clear();
        if (event.status > 400) {
            this._toasterService.pop('error', event.statusText);
            this.getCentreLogo();
        }
        else {
            this._toasterService.pop('success', 'Logo Uploaded');
        }
        this.imageUploading = false;
    };
    EnrolmentFormComponent.prototype.onSubmit = function () {
        var _this = this;
        this.submitted = true;
        var savingToast = this._toasterService.pop(new index_5.SpinningToast());
        var centreBrandingDto = new index_2.CreateUpdateCentreBrandingDto();
        centreBrandingDto.init({
            'CentreId': this._userService.SelectedCentre.id,
            'DomainPath': this.domainPath.value,
            'BaseColour': this.enrolment.baseColour,
            'AdditionalEnrolmentFormComments': this.additionalEnrolmentFormComments.value
        });
        this._enrolmentService
            .CreateUpdateCentreBranding$(centreBrandingDto)
            .subscribe(function (res) {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this._toasterService.pop('success', 'Saved');
            _this.enrolment = res;
            _this.submitted = false;
        }, function (err) { return _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId); });
    };
    EnrolmentFormComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'enrolment-form-cmp',
            templateUrl: './enrolmentForm.component.html',
            styleUrls: ['enrolmentForm.component.css']
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, index_1.EnrolmentService, index_1.DomainNameValidatorService, index_4.AccessTokenService, index_3.CentreUserService, angular2_toaster_1.ToasterService, angular2_color_picker_1.ColorPickerService, core_1.ElementRef])
    ], EnrolmentFormComponent);
    return EnrolmentFormComponent;
}());
exports.EnrolmentFormComponent = EnrolmentFormComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2Vucm9sbWVudC9lbnJvbG1lbnRGb3JtL2Vucm9sbWVudEZvcm0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBOEMsZUFBZSxDQUFDLENBQUE7QUFDOUQsc0JBQW9FLGdCQUFnQixDQUFDLENBQUE7QUFFckYsaUNBQStCLGtCQUFrQixDQUFDLENBQUE7QUFDbEQsNkJBQWtDLGNBQWMsQ0FBQyxDQUFBO0FBQ2pELHNDQUFtQyx1QkFBdUIsQ0FBQyxDQUFBO0FBRTNELHNCQUE2RCxVQUFVLENBQUMsQ0FBQTtBQUN4RSxzQkFBNkQsdUJBQXVCLENBQUMsQ0FBQTtBQUNyRixzQkFBa0Msd0JBQXdCLENBQUMsQ0FBQTtBQUMzRCxzQkFBbUMsd0JBQXdCLENBQUMsQ0FBQTtBQUM1RCxzQkFBc0MsMEJBQTBCLENBQUMsQ0FBQTtBQVFqRTtJQXNCSSxnQ0FBWSxFQUFlLEVBQ2YsaUJBQW1DLEVBQ25DLGdCQUE0QyxFQUM1QyxZQUFnQyxFQUNoQyxZQUErQixFQUMvQixlQUErQixFQUMvQixtQkFBdUMsRUFDdkMsUUFBb0I7UUFOcEIsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFrQjtRQUNuQyxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQTRCO1FBQzVDLGlCQUFZLEdBQVosWUFBWSxDQUFvQjtRQUNoQyxpQkFBWSxHQUFaLFlBQVksQ0FBbUI7UUFDL0Isb0JBQWUsR0FBZixlQUFlLENBQWdCO1FBQy9CLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBb0I7UUFDdkMsYUFBUSxHQUFSLFFBQVEsQ0FBWTtRQTFCeEIsY0FBUyxHQUFXLElBQUksQ0FBQztRQUt6QixjQUFTLEdBQWtDLElBQUksQ0FBQztRQUNoRCxlQUFVLEdBQWtCLElBQUksQ0FBQztRQUNqQyxtQkFBYyxHQUFZLEtBQUssQ0FBQztRQUVoQyxhQUFRLEdBQVcsSUFBSSxDQUFDO1FBQ3hCLGVBQVUsR0FBVyxJQUFJLENBQUM7UUFDakIsa0JBQWEsR0FBVyx3QkFBd0IsQ0FBQztRQUNqRCxpQkFBWSxHQUFHLFNBQVMsQ0FBQztRQUdsQyxjQUFTLEdBQVksS0FBSyxDQUFDO1FBYS9CLElBQUksQ0FBQyxTQUFTLEdBQUcsY0FBTSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7UUFDbEMsSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUV6QixJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQyxLQUFLLENBQUM7WUFDakIsWUFBWSxFQUFFLENBQUMsRUFBRTtnQkFDYixrQkFBVSxDQUFDLE9BQU8sQ0FBQztvQkFDZixrQkFBVSxDQUFDLFFBQVE7b0JBQ25CLGtCQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQztvQkFDekIsa0JBQVUsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzdDLGlDQUFpQyxFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3RFLENBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDbkQsSUFBSSxDQUFDLCtCQUErQixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGlDQUFpQyxDQUFDLENBQUM7UUFFN0YsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLGdDQUFpQixDQUFDO1lBQ3pDLEdBQUcsRUFBRSxJQUFJLENBQUMsU0FBUyxHQUFHLHVDQUF1QyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLEVBQUU7WUFDbkcsU0FBUyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxFQUFFO1lBQzdDLGNBQWMsRUFBRSxJQUFJO1lBQ3BCLGdCQUFnQixFQUFFLElBQUk7WUFDdEIsaUJBQWlCLEVBQUUsQ0FBQyxLQUFLLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUM7WUFDaEQsVUFBVSxFQUFFLElBQUk7U0FDbkIsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHlDQUFRLEdBQVI7UUFBQSxpQkFhQztRQVhHLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsNEJBQTRCLEVBQUUsQ0FBQztRQUNqRSxJQUFJLENBQUMsaUJBQWlCO2FBQ2pCLGtCQUFrQixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FBQzthQUN2RCxTQUFTLENBQUMsVUFBQSxHQUFHO1lBQ1YsS0FBSSxDQUFDLFNBQVMsR0FBRyxxQ0FBNkIsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7WUFDcEUsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEtBQUssU0FBUyxJQUFJLEtBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQ2hGLEtBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxHQUFHLEtBQUksQ0FBQyxZQUFZLENBQUM7WUFDbEQsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO1FBRVAsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3pCLENBQUM7SUFFRCw4Q0FBYSxHQUFiO1FBQUEsaUJBa0JDO1FBakJHLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxpQkFBaUI7YUFDakIsY0FBYyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FBQzthQUNuRCxTQUFTLENBQUMsVUFBQSxHQUFHO1lBQ1YsS0FBSSxDQUFDLFVBQVUsR0FBRyxHQUFHLENBQUM7WUFFdEIsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEtBQUssU0FBUztnQkFDekMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEtBQUssSUFBSTtnQkFDcEMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFFckMsS0FBSSxDQUFDLFFBQVEsR0FBRywyQkFBMkIsQ0FBQztnQkFDNUMsS0FBSSxDQUFDLFVBQVUsR0FBRyxLQUFJLENBQUMsYUFBYSxHQUFHLEtBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDO1lBQ3ZFLENBQUM7UUFDTCxDQUFDLEVBQ0QsVUFBQSxHQUFHO1lBQ0MsS0FBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLHFCQUFhLEVBQUUsQ0FBQztRQUMxQyxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFRCxtREFBa0IsR0FBbEIsVUFBbUIsR0FBVztRQUMxQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ2xDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLENBQUM7Z0JBQ2xDLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsNEJBQTRCLEVBQUUsQ0FBQztZQUVyRSxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRSxDQUFDO1lBQ2hDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDN0MsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osSUFBSSxDQUFDLHFCQUFxQixDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQzdDLENBQUM7SUFDTCxDQUFDO0lBRUQsNkRBQTRCLEdBQTVCO1FBQUEsaUJBV0M7UUFWRyxNQUFNLENBQUMsSUFBSSxDQUFDLGdCQUFnQjthQUN2QixrQkFBa0IsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxFQUFFLENBQUM7YUFDdkQsU0FBUyxDQUFDLFVBQUEsR0FBRztZQUNWLElBQUksT0FBTyxHQUFZLEdBQUcsQ0FBQztZQUMzQixJQUFJLFFBQVEsR0FBRyxPQUFPLEdBQUcsSUFBSSxHQUFHLEVBQUUsaUJBQWlCLEVBQUUsSUFBSSxFQUFFLENBQUM7WUFFNUQsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUM7Z0JBQ3RCLEtBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3hDLEtBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDbEMsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRUQsMkNBQVUsR0FBVixVQUFXLE9BQXdCO1FBQy9CLE1BQU0sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxPQUFPLElBQUksT0FBTyxDQUFDLEtBQUssSUFBSSxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksT0FBTyxDQUFDLEtBQUssQ0FBQztJQUNoRixDQUFDO0lBRUQseUNBQVEsR0FBUixVQUFTLE9BQXdCO1FBQzdCLE1BQU0sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxPQUFPLElBQUksT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLE9BQU8sQ0FBQyxPQUFPLENBQUM7SUFDakUsQ0FBQztJQUVELDJDQUFVLEdBQVYsVUFBVyxPQUF3QjtRQUMvQixNQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQztJQUMzQixDQUFDO0lBRUQsNkNBQVksR0FBWixVQUFhLEtBQVU7UUFFbkIsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztZQUN2QixJQUFJLENBQUMsUUFBUSxHQUFHLDJCQUEyQixDQUFDO1lBQzVDLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUkscUJBQWEsRUFBRSxDQUFDLENBQUM7WUFDOUMsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7UUFDL0IsQ0FBQztJQUNMLENBQUM7SUFFRCxxREFBb0IsR0FBcEIsVUFBcUIsS0FBVTtRQUUzQixJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQzdCLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNyQixJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3BELElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUN6QixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsZUFBZSxDQUFDLENBQUM7UUFDekQsQ0FBQztRQUNELElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO0lBQ2hDLENBQUM7SUFFRCx5Q0FBUSxHQUFSO1FBQUEsaUJBdUJDO1FBckJHLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUkscUJBQWEsRUFBRSxDQUFDLENBQUM7UUFFaEUsSUFBSSxpQkFBaUIsR0FDakIsSUFBSSxxQ0FBNkIsRUFBRSxDQUFDO1FBQ3hDLGlCQUFpQixDQUFDLElBQUksQ0FBQztZQUNuQixVQUFVLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsRUFBRTtZQUMvQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLO1lBQ25DLFlBQVksRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVU7WUFDdkMsaUNBQWlDLEVBQUUsSUFBSSxDQUFDLCtCQUErQixDQUFDLEtBQUs7U0FDaEYsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLGlCQUFpQjthQUNqQiwyQkFBMkIsQ0FBQyxpQkFBaUIsQ0FBQzthQUM5QyxTQUFTLENBQUMsVUFBQSxHQUFHO1lBRVYsS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxXQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUM5RSxLQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDN0MsS0FBSSxDQUFDLFNBQVMsR0FBRyxHQUFHLENBQUM7WUFDckIsS0FBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7UUFDM0IsQ0FBQyxFQUNELFVBQUMsR0FBRyxJQUFLLE9BQUEsS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxXQUFXLENBQUMsZ0JBQWdCLENBQUMsRUFBN0UsQ0FBNkUsQ0FBQyxDQUFDO0lBQ2hHLENBQUM7SUFsTEw7UUFBQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxvQkFBb0I7WUFDOUIsV0FBVyxFQUFFLGdDQUFnQztZQUM3QyxTQUFTLEVBQUUsQ0FBQyw2QkFBNkIsQ0FBQztTQUM3QyxDQUFDOzs4QkFBQTtJQThLRiw2QkFBQztBQUFELENBN0tBLEFBNktDLElBQUE7QUE3S1ksOEJBQXNCLHlCQTZLbEMsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9lbnJvbG1lbnQvZW5yb2xtZW50Rm9ybS9lbnJvbG1lbnRGb3JtLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBFbGVtZW50UmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1Hcm91cCwgQWJzdHJhY3RDb250cm9sLCBGb3JtQnVpbGRlciwgVmFsaWRhdG9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IFRvYXN0ZXJTZXJ2aWNlIH0gZnJvbSAnYW5ndWxhcjItdG9hc3Rlcic7XHJcbmltcG9ydCB7IE5nVXBsb2FkZXJPcHRpb25zIH0gZnJvbSAnbmd4LXVwbG9hZGVyJztcclxuaW1wb3J0IHsgQ29sb3JQaWNrZXJTZXJ2aWNlIH0gZnJvbSAnYW5ndWxhcjItY29sb3ItcGlja2VyJztcclxuXHJcbmltcG9ydCB7IEVucm9sbWVudFNlcnZpY2UsIERvbWFpbk5hbWVWYWxpZGF0b3JTZXJ2aWNlIH0gZnJvbSAnLi4vaW5kZXgnO1xyXG5pbXBvcnQgeyBDcmVhdGVVcGRhdGVDZW50cmVCcmFuZGluZ0R0bywgQ2VudHJlTG9nb0R0byB9IGZyb20gJy4uLy4uLy4uLy4uL2FwaS9pbmRleCc7XHJcbmltcG9ydCB7IENlbnRyZVVzZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vY29yZS9pbmRleCc7XHJcbmltcG9ydCB7IEFjY2Vzc1Rva2VuU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uL2NvcmUvaW5kZXgnO1xyXG5pbXBvcnQgeyBTcGlubmluZ1RvYXN0LCBDb25maWcgfSBmcm9tICcuLi8uLi8uLi8uLi9zaGFyZWQvaW5kZXgnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgc2VsZWN0b3I6ICdlbnJvbG1lbnQtZm9ybS1jbXAnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2Vucm9sbWVudEZvcm0uY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJ2Vucm9sbWVudEZvcm0uY29tcG9uZW50LmNzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBFbnJvbG1lbnRGb3JtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBwcml2YXRlIHZhbGlkYXRvclN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xyXG4gICAgcHJpdmF0ZSBvcmlnaW5Vcmw6IHN0cmluZyA9IG51bGw7XHJcbiAgICBwcml2YXRlIGZvcm06IEZvcm1Hcm91cDtcclxuICAgIHByaXZhdGUgZG9tYWluUGF0aDogQWJzdHJhY3RDb250cm9sO1xyXG4gICAgcHJpdmF0ZSBhZGRpdGlvbmFsRW5yb2xtZW50Rm9ybUNvbW1lbnRzOiBBYnN0cmFjdENvbnRyb2w7XHJcblxyXG4gICAgcHJpdmF0ZSBlbnJvbG1lbnQ6IENyZWF0ZVVwZGF0ZUNlbnRyZUJyYW5kaW5nRHRvID0gbnVsbDtcclxuICAgIHByaXZhdGUgY2VudHJlTG9nbzogQ2VudHJlTG9nb0R0byA9IG51bGw7XHJcbiAgICBwcml2YXRlIGltYWdlVXBsb2FkaW5nOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgcHJpdmF0ZSBsb2dvTmFtZTogc3RyaW5nID0gbnVsbDtcclxuICAgIHByaXZhdGUgbG9nb0Jhc2U2NDogc3RyaW5nID0gbnVsbDtcclxuICAgIHByaXZhdGUgcmVhZG9ubHkgSW1hZ2VFbmNvZGluZzogc3RyaW5nID0gJ2RhdGE6aW1hZ2UvcG5nO2Jhc2U2NCwnO1xyXG4gICAgcHJpdmF0ZSByZWFkb25seSBEZWZhdWx0Q29sb3IgPSAnIzk0NGVhNic7XHJcblxyXG4gICAgcHJpdmF0ZSBhcHByb3ZhbFN0YXR1czogbnVtYmVyO1xyXG4gICAgcHJpdmF0ZSBzdWJtaXR0ZWQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICBwcml2YXRlIHVwbG9hZGVyT3B0aW9uczogTmdVcGxvYWRlck9wdGlvbnM7XHJcblxyXG4gICAgY29uc3RydWN0b3IoZmI6IEZvcm1CdWlsZGVyLFxyXG4gICAgICAgIHByaXZhdGUgX2Vucm9sbWVudFNlcnZpY2U6IEVucm9sbWVudFNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBfZG9tYWluVmFsaWRhdG9yOiBEb21haW5OYW1lVmFsaWRhdG9yU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIF9hdXRoU2VydmljZTogQWNjZXNzVG9rZW5TZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgX3VzZXJTZXJ2aWNlOiBDZW50cmVVc2VyU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIF90b2FzdGVyU2VydmljZTogVG9hc3RlclNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBfY29sb3JQaWNrZXJTZXJ2aWNlOiBDb2xvclBpY2tlclNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBfZWxlbWVudDogRWxlbWVudFJlZikge1xyXG5cclxuICAgICAgICB0aGlzLm9yaWdpblVybCA9IENvbmZpZy5BUEkgKyAnLyc7XHJcbiAgICAgICAgdGhpcy5hcHByb3ZhbFN0YXR1cyA9IC0xO1xyXG5cclxuICAgICAgICB0aGlzLmZvcm0gPSBmYi5ncm91cCh7XHJcbiAgICAgICAgICAgICdkb21haW5QYXRoJzogWycnLFxyXG4gICAgICAgICAgICAgICAgVmFsaWRhdG9ycy5jb21wb3NlKFtcclxuICAgICAgICAgICAgICAgICAgICBWYWxpZGF0b3JzLnJlcXVpcmVkLFxyXG4gICAgICAgICAgICAgICAgICAgIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDEwMCksXHJcbiAgICAgICAgICAgICAgICAgICAgVmFsaWRhdG9ycy5wYXR0ZXJuKCdbQS1aYS16MC05XSonKV0pXSxcclxuICAgICAgICAgICAgJ2FkZGl0aW9uYWxFbnJvbG1lbnRGb3JtQ29tbWVudHMnOiBbJycsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDQwMDApXVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICB0aGlzLmRvbWFpblBhdGggPSB0aGlzLmZvcm0uY29udHJvbHNbJ2RvbWFpblBhdGgnXTtcclxuICAgICAgICB0aGlzLmFkZGl0aW9uYWxFbnJvbG1lbnRGb3JtQ29tbWVudHMgPSB0aGlzLmZvcm0uY29udHJvbHNbJ2FkZGl0aW9uYWxFbnJvbG1lbnRGb3JtQ29tbWVudHMnXTtcclxuXHJcbiAgICAgICAgdGhpcy51cGxvYWRlck9wdGlvbnMgPSBuZXcgTmdVcGxvYWRlck9wdGlvbnMoe1xyXG4gICAgICAgICAgICB1cmw6IHRoaXMub3JpZ2luVXJsICsgJ2FwaS9DZW50cmUvVXBsb2FkQ2VudHJlTG9nbz9jZW50cmVJZD0nICsgdGhpcy5fdXNlclNlcnZpY2UuU2VsZWN0ZWRDZW50cmUuaWQsXHJcbiAgICAgICAgICAgIGF1dGhUb2tlbjogdGhpcy5fYXV0aFNlcnZpY2UuZ2V0QWNjZXNzVG9rZW4oKSxcclxuICAgICAgICAgICAgY2FsY3VsYXRlU3BlZWQ6IHRydWUsXHJcbiAgICAgICAgICAgIGZpbHRlckV4dGVuc2lvbnM6IHRydWUsXHJcbiAgICAgICAgICAgIGFsbG93ZWRFeHRlbnNpb25zOiBbJ2pwZycsICdqcGVnJywgJ3BuZycsICdnaWYnXSxcclxuICAgICAgICAgICAgYXV0b1VwbG9hZDogdHJ1ZVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xyXG5cclxuICAgICAgICB0aGlzLnZhbGlkYXRvclN1YnNjcmlwdGlvbiA9IHRoaXMuc3Vic2NyaWJlRG9tYWluTmFtZVZhbGlkYXRvcigpO1xyXG4gICAgICAgIHRoaXMuX2Vucm9sbWVudFNlcnZpY2VcclxuICAgICAgICAgICAgLkdldENlbnRyZUJyYW5kaW5nJCh0aGlzLl91c2VyU2VydmljZS5TZWxlY3RlZENlbnRyZS5pZClcclxuICAgICAgICAgICAgLnN1YnNjcmliZShyZXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lbnJvbG1lbnQgPSBDcmVhdGVVcGRhdGVDZW50cmVCcmFuZGluZ0R0by5mcm9tSlMocmVzLnRvSlNPTigpKTtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLmVucm9sbWVudC5iYXNlQ29sb3VyID09PSB1bmRlZmluZWQgfHwgdGhpcy5lbnJvbG1lbnQuYmFzZUNvbG91ciA9PT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZW5yb2xtZW50LmJhc2VDb2xvdXIgPSB0aGlzLkRlZmF1bHRDb2xvcjtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRoaXMuZ2V0Q2VudHJlTG9nbygpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldENlbnRyZUxvZ28oKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5jZW50cmVMb2dvID0gbnVsbDtcclxuICAgICAgICB0aGlzLl9lbnJvbG1lbnRTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5HZXRDZW50cmVMb2dvJCh0aGlzLl91c2VyU2VydmljZS5TZWxlY3RlZENlbnRyZS5pZClcclxuICAgICAgICAgICAgLnN1YnNjcmliZShyZXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jZW50cmVMb2dvID0gcmVzO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLmNlbnRyZUxvZ28uaW1hZ2VCYXNlNjQgIT09IHVuZGVmaW5lZCAmJlxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2VudHJlTG9nby5pbWFnZUJhc2U2NCAhPT0gbnVsbCAmJlxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2VudHJlTG9nby5pbWFnZUJhc2U2NCAhPT0gJycpIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2dvTmFtZSA9ICdDbGljayBIZXJlIHRvIFVwbG9hZCBMb2dvJztcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvZ29CYXNlNjQgPSB0aGlzLkltYWdlRW5jb2RpbmcgKyB0aGlzLmNlbnRyZUxvZ28uaW1hZ2VCYXNlNjQ7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGVyciA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNlbnRyZUxvZ28gPSBuZXcgQ2VudHJlTG9nb0R0bygpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICB2YWxpZGF0ZURvbWFpblBhdGgodmFsOiBzdHJpbmcpIHtcclxuICAgICAgICBpZiAodGhpcy5kb21haW5QYXRoLmVycm9ycyA9PT0gbnVsbCkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy52YWxpZGF0b3JTdWJzY3JpcHRpb24uY2xvc2VkKVxyXG4gICAgICAgICAgICAgICAgdGhpcy52YWxpZGF0b3JTdWJzY3JpcHRpb24gPSB0aGlzLnN1YnNjcmliZURvbWFpbk5hbWVWYWxpZGF0b3IoKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuZG9tYWluUGF0aC5tYXJrQXNQZW5kaW5nKCk7XHJcbiAgICAgICAgICAgIHRoaXMuX2RvbWFpblZhbGlkYXRvci5WYWxpZGF0ZUFzeW5jKHZhbCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy52YWxpZGF0b3JTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgc3Vic2NyaWJlRG9tYWluTmFtZVZhbGlkYXRvcigpOiBTdWJzY3JpcHRpb24ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9kb21haW5WYWxpZGF0b3JcclxuICAgICAgICAgICAgLmlzRG9tYWluTmFtZVZhbGlkJCh0aGlzLl91c2VyU2VydmljZS5TZWxlY3RlZENlbnRyZS5pZClcclxuICAgICAgICAgICAgLnN1YnNjcmliZShyZXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgbGV0IGlzVmFsaWQ6IGJvb2xlYW4gPSByZXM7XHJcbiAgICAgICAgICAgICAgICBsZXQgdmFsaWRpdHkgPSBpc1ZhbGlkID8gbnVsbCA6IHsgJ2RvbWFpblBhdGhUYWtlbic6IHRydWUgfTtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5kb21haW5QYXRoLnZhbHVlKVxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZG9tYWluUGF0aC5zZXRFcnJvcnModmFsaWRpdHkpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kb21haW5QYXRoLm1hcmtBc0RpcnR5KCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGhhc1N1Y2Nlc3MoY29udHJvbDogQWJzdHJhY3RDb250cm9sKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIChjb250cm9sLnRvdWNoZWQgfHwgY29udHJvbC5kaXJ0eSB8fCBjb250cm9sLnZhbHVlKSAmJiBjb250cm9sLnZhbGlkO1xyXG4gICAgfVxyXG5cclxuICAgIGhhc0Vycm9yKGNvbnRyb2w6IEFic3RyYWN0Q29udHJvbCkge1xyXG4gICAgICAgIHJldHVybiAoY29udHJvbC50b3VjaGVkIHx8IGNvbnRyb2wuZGlydHkpICYmIGNvbnRyb2wuaW52YWxpZDtcclxuICAgIH1cclxuXHJcbiAgICBoYXNXYXJuaW5nKGNvbnRyb2w6IEFic3RyYWN0Q29udHJvbCkge1xyXG4gICAgICAgIHJldHVybiBjb250cm9sLnBlbmRpbmc7XHJcbiAgICB9XHJcblxyXG4gICAgb25Mb2dvVXBsb2FkKGV2ZW50OiBhbnkpOiB2b2lkIHtcclxuXHJcbiAgICAgICAgaWYgKCF0aGlzLmltYWdlVXBsb2FkaW5nKSB7XHJcbiAgICAgICAgICAgIHRoaXMubG9nb05hbWUgPSAnQ2xpY2sgSGVyZSB0byBVcGxvYWQgTG9nbyc7XHJcbiAgICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLnBvcChuZXcgU3Bpbm5pbmdUb2FzdCgpKTtcclxuICAgICAgICAgICAgdGhpcy5pbWFnZVVwbG9hZGluZyA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uTG9nb1VwbG9hZENvbXBsZXRlKGV2ZW50OiBhbnkpOiB2b2lkIHtcclxuXHJcbiAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoKTtcclxuICAgICAgICBpZiAoZXZlbnQuc3RhdHVzID4gNDAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLnBvcCgnZXJyb3InLCBldmVudC5zdGF0dXNUZXh0KTtcclxuICAgICAgICAgICAgdGhpcy5nZXRDZW50cmVMb2dvKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UucG9wKCdzdWNjZXNzJywgJ0xvZ28gVXBsb2FkZWQnKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5pbWFnZVVwbG9hZGluZyA9IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIG9uU3VibWl0KCk6IHZvaWQge1xyXG5cclxuICAgICAgICB0aGlzLnN1Ym1pdHRlZCA9IHRydWU7XHJcbiAgICAgICAgbGV0IHNhdmluZ1RvYXN0ID0gdGhpcy5fdG9hc3RlclNlcnZpY2UucG9wKG5ldyBTcGlubmluZ1RvYXN0KCkpO1xyXG5cclxuICAgICAgICB2YXIgY2VudHJlQnJhbmRpbmdEdG8gPVxyXG4gICAgICAgICAgICBuZXcgQ3JlYXRlVXBkYXRlQ2VudHJlQnJhbmRpbmdEdG8oKTtcclxuICAgICAgICBjZW50cmVCcmFuZGluZ0R0by5pbml0KHtcclxuICAgICAgICAgICAgJ0NlbnRyZUlkJzogdGhpcy5fdXNlclNlcnZpY2UuU2VsZWN0ZWRDZW50cmUuaWQsXHJcbiAgICAgICAgICAgICdEb21haW5QYXRoJzogdGhpcy5kb21haW5QYXRoLnZhbHVlLFxyXG4gICAgICAgICAgICAnQmFzZUNvbG91cic6IHRoaXMuZW5yb2xtZW50LmJhc2VDb2xvdXIsXHJcbiAgICAgICAgICAgICdBZGRpdGlvbmFsRW5yb2xtZW50Rm9ybUNvbW1lbnRzJzogdGhpcy5hZGRpdGlvbmFsRW5yb2xtZW50Rm9ybUNvbW1lbnRzLnZhbHVlXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5fZW5yb2xtZW50U2VydmljZVxyXG4gICAgICAgICAgICAuQ3JlYXRlVXBkYXRlQ2VudHJlQnJhbmRpbmckKGNlbnRyZUJyYW5kaW5nRHRvKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKHJlcyA9PiB7XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoc2F2aW5nVG9hc3QudG9hc3RJZCwgc2F2aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5wb3AoJ3N1Y2Nlc3MnLCAnU2F2ZWQnKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZW5yb2xtZW50ID0gcmVzO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zdWJtaXR0ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgKGVycikgPT4gdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoc2F2aW5nVG9hc3QudG9hc3RJZCwgc2F2aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCkpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
