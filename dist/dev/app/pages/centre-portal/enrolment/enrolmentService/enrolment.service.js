"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../../api/index');
var EnrolmentService = (function () {
    function EnrolmentService(_apiService) {
        this._apiService = _apiService;
    }
    EnrolmentService.prototype.CreateUpdateCentreBranding$ = function (centreBrandingDto) {
        return this._apiService
            .centre_CreateUpdateCentreBranding(centreBrandingDto);
    };
    EnrolmentService.prototype.GetCentreBranding$ = function (centreId) {
        return this._apiService
            .centre_GetCentreBranding(centreId);
    };
    EnrolmentService.prototype.GetCentreLogo$ = function (centreId) {
        return this._apiService.centre_GetCentreLogo(centreId);
    };
    EnrolmentService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.ApiClientService])
    ], EnrolmentService);
    return EnrolmentService;
}());
exports.EnrolmentService = EnrolmentService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2Vucm9sbWVudC9lbnJvbG1lbnRTZXJ2aWNlL2Vucm9sbWVudC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBMkIsZUFBZSxDQUFDLENBQUE7QUFHM0Msc0JBR08sdUJBQXVCLENBQUMsQ0FBQTtBQUcvQjtJQUVJLDBCQUFvQixXQUE2QjtRQUE3QixnQkFBVyxHQUFYLFdBQVcsQ0FBa0I7SUFFakQsQ0FBQztJQUVNLHNEQUEyQixHQUFsQyxVQUFtQyxpQkFBZ0Q7UUFDL0UsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXO2FBQ2xCLGlDQUFpQyxDQUFDLGlCQUFpQixDQUFDLENBQUM7SUFDOUQsQ0FBQztJQUVNLDZDQUFrQixHQUF6QixVQUEwQixRQUFnQjtRQUN0QyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVc7YUFDbEIsd0JBQXdCLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDNUMsQ0FBQztJQUVNLHlDQUFjLEdBQXJCLFVBQXNCLFFBQWdCO1FBQ2xDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQzNELENBQUM7SUFuQkw7UUFBQyxpQkFBVSxFQUFFOzt3QkFBQTtJQW9CYix1QkFBQztBQUFELENBbkJBLEFBbUJDLElBQUE7QUFuQlksd0JBQWdCLG1CQW1CNUIsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9lbnJvbG1lbnQvZW5yb2xtZW50U2VydmljZS9lbnJvbG1lbnQuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHtcclxuICAgIEFwaUNsaWVudFNlcnZpY2UsIENlbnRyZUJyYW5kaW5nRHRvLFxyXG4gICAgQ3JlYXRlVXBkYXRlQ2VudHJlQnJhbmRpbmdEdG8sIENlbnRyZUxvZ29EdG9cclxufSBmcm9tICcuLi8uLi8uLi8uLi9hcGkvaW5kZXgnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgRW5yb2xtZW50U2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfYXBpU2VydmljZTogQXBpQ2xpZW50U2VydmljZSkge1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgQ3JlYXRlVXBkYXRlQ2VudHJlQnJhbmRpbmckKGNlbnRyZUJyYW5kaW5nRHRvOiBDcmVhdGVVcGRhdGVDZW50cmVCcmFuZGluZ0R0bykge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9hcGlTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5jZW50cmVfQ3JlYXRlVXBkYXRlQ2VudHJlQnJhbmRpbmcoY2VudHJlQnJhbmRpbmdEdG8pO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBHZXRDZW50cmVCcmFuZGluZyQoY2VudHJlSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8Q2VudHJlQnJhbmRpbmdEdG8+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fYXBpU2VydmljZVxyXG4gICAgICAgICAgICAuY2VudHJlX0dldENlbnRyZUJyYW5kaW5nKGNlbnRyZUlkKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgR2V0Q2VudHJlTG9nbyQoY2VudHJlSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8Q2VudHJlTG9nb0R0bz4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9hcGlTZXJ2aWNlLmNlbnRyZV9HZXRDZW50cmVMb2dvKGNlbnRyZUlkKTtcclxuICAgIH1cclxufVxyXG4iXX0=
