"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var index_1 = require('../../../shared/index');
var EnrolmentComponent = (function () {
    function EnrolmentComponent(_router) {
        this._router = _router;
        if (index_1.Config.ENABLE_ENROLMENT_FEATURE !== 'true') {
            this._router.navigate(['/']);
        }
    }
    EnrolmentComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'enrolment-cmp',
            templateUrl: './enrolment.component.html'
        }), 
        __metadata('design:paramtypes', [router_1.Router])
    ], EnrolmentComponent);
    return EnrolmentComponent;
}());
exports.EnrolmentComponent = EnrolmentComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2Vucm9sbWVudC9lbnJvbG1lbnQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBMEIsZUFBZSxDQUFDLENBQUE7QUFDMUMsdUJBQXVCLGlCQUFpQixDQUFDLENBQUE7QUFFekMsc0JBQXVCLHVCQUF1QixDQUFDLENBQUE7QUFRL0M7SUFFSSw0QkFBb0IsT0FBZTtRQUFmLFlBQU8sR0FBUCxPQUFPLENBQVE7UUFDL0IsRUFBRSxDQUFDLENBQUMsY0FBTSxDQUFDLHdCQUF3QixLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDN0MsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQ2pDLENBQUM7SUFDTCxDQUFDO0lBWkw7UUFBQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxlQUFlO1lBQ3pCLFdBQVcsRUFBRSw0QkFBNEI7U0FDNUMsQ0FBQzs7MEJBQUE7SUFTRix5QkFBQztBQUFELENBUEEsQUFPQyxJQUFBO0FBUFksMEJBQWtCLHFCQU85QixDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2Vucm9sbWVudC9lbnJvbG1lbnQuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcblxyXG5pbXBvcnQgeyBDb25maWcgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvaW5kZXgnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgc2VsZWN0b3I6ICdlbnJvbG1lbnQtY21wJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9lbnJvbG1lbnQuY29tcG9uZW50Lmh0bWwnXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgRW5yb2xtZW50Q29tcG9uZW50IHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9yb3V0ZXI6IFJvdXRlcikge1xyXG4gICAgICAgIGlmIChDb25maWcuRU5BQkxFX0VOUk9MTUVOVF9GRUFUVVJFICE9PSAndHJ1ZScpIHtcclxuICAgICAgICAgICAgdGhpcy5fcm91dGVyLm5hdmlnYXRlKFsnLyddKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19
