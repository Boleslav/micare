"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var angular2_color_picker_1 = require('angular2-color-picker');
var index_1 = require('../../../shared/index');
var index_2 = require('./index');
var EnrolmentModule = (function () {
    function EnrolmentModule() {
    }
    EnrolmentModule = __decorate([
        core_1.NgModule({
            imports: [
                index_1.SharedModule,
                angular2_color_picker_1.ColorPickerModule
            ],
            declarations: [
                index_2.EnrolmentComponent,
                index_2.EnrolmentFormComponent
            ],
            providers: [index_2.EnrolmentService, index_2.DomainNameValidatorService]
        }), 
        __metadata('design:paramtypes', [])
    ], EnrolmentModule);
    return EnrolmentModule;
}());
exports.EnrolmentModule = EnrolmentModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2Vucm9sbWVudC9lbnJvbG1lbnQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBeUIsZUFBZSxDQUFDLENBQUE7QUFDekMsc0NBQWtDLHVCQUF1QixDQUFDLENBQUE7QUFFMUQsc0JBQTZCLHVCQUF1QixDQUFDLENBQUE7QUFDckQsc0JBR08sU0FBUyxDQUFDLENBQUE7QUFjakI7SUFBQTtJQUErQixDQUFDO0lBWmhDO1FBQUMsZUFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFO2dCQUNMLG9CQUFZO2dCQUNaLHlDQUFpQjthQUNwQjtZQUNELFlBQVksRUFBRTtnQkFDViwwQkFBa0I7Z0JBQ2xCLDhCQUFzQjthQUN6QjtZQUNELFNBQVMsRUFBRSxDQUFDLHdCQUFnQixFQUFFLGtDQUEwQixDQUFDO1NBQzVELENBQUM7O3VCQUFBO0lBRTZCLHNCQUFDO0FBQUQsQ0FBL0IsQUFBZ0MsSUFBQTtBQUFuQix1QkFBZSxrQkFBSSxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2Vucm9sbWVudC9lbnJvbG1lbnQubW9kdWxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29sb3JQaWNrZXJNb2R1bGUgfSBmcm9tICdhbmd1bGFyMi1jb2xvci1waWNrZXInO1xyXG5cclxuaW1wb3J0IHsgU2hhcmVkTW9kdWxlIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL2luZGV4JztcclxuaW1wb3J0IHtcclxuICAgIEVucm9sbWVudENvbXBvbmVudCwgRW5yb2xtZW50Rm9ybUNvbXBvbmVudCxcclxuICAgIEVucm9sbWVudFNlcnZpY2UsIERvbWFpbk5hbWVWYWxpZGF0b3JTZXJ2aWNlXHJcbn0gZnJvbSAnLi9pbmRleCc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgaW1wb3J0czogW1xyXG4gICAgICAgIFNoYXJlZE1vZHVsZSxcclxuICAgICAgICBDb2xvclBpY2tlck1vZHVsZVxyXG4gICAgXSxcclxuICAgIGRlY2xhcmF0aW9uczogW1xyXG4gICAgICAgIEVucm9sbWVudENvbXBvbmVudCxcclxuICAgICAgICBFbnJvbG1lbnRGb3JtQ29tcG9uZW50XHJcbiAgICBdLFxyXG4gICAgcHJvdmlkZXJzOiBbRW5yb2xtZW50U2VydmljZSwgRG9tYWluTmFtZVZhbGlkYXRvclNlcnZpY2VdXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgRW5yb2xtZW50TW9kdWxlIHsgfVxyXG4iXX0=
