"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../core/index');
var index_2 = require('../../../core/index');
var index_3 = require('../../../shared/index');
var LogoutComponent = (function () {
    function LogoutComponent(_authenticationService, _intercomService, _userService) {
        this._authenticationService = _authenticationService;
        this._intercomService = _intercomService;
        this._userService = _userService;
    }
    LogoutComponent.prototype.ngOnInit = function () {
        this._authenticationService.logout();
        this._userService.SetLastSelectedCentreId(this._userService.SelectedCentre.id);
        this._userService.Clear();
        this._intercomService.userLoggedOut(index_3.Config.INTERCOM_APP_ID);
    };
    LogoutComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'logout-cmp',
            templateUrl: 'logout.component.html'
        }), 
        __metadata('design:paramtypes', [index_2.AccessTokenService, index_3.IntercomService, index_1.CentreUserService])
    ], LogoutComponent);
    return LogoutComponent;
}());
exports.LogoutComponent = LogoutComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2xvZ291dC9sb2dvdXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBa0MsZUFBZSxDQUFDLENBQUE7QUFFbEQsc0JBQWtDLHFCQUFxQixDQUFDLENBQUE7QUFDeEQsc0JBQW1DLHFCQUFxQixDQUFDLENBQUE7QUFDekQsc0JBQXdDLHVCQUF1QixDQUFDLENBQUE7QUFRaEU7SUFFSSx5QkFDWSxzQkFBMEMsRUFDMUMsZ0JBQWlDLEVBQ2pDLFlBQStCO1FBRi9CLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBb0I7UUFDMUMscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFpQjtRQUNqQyxpQkFBWSxHQUFaLFlBQVksQ0FBbUI7SUFDM0MsQ0FBQztJQUVELGtDQUFRLEdBQVI7UUFDSSxJQUFJLENBQUMsc0JBQXNCLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDckMsSUFBSSxDQUFDLFlBQVksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUMvRSxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQzFCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsY0FBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDO0lBQ2hFLENBQUM7SUFuQkw7UUFBQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxZQUFZO1lBQ3RCLFdBQVcsRUFBRSx1QkFBdUI7U0FDdkMsQ0FBQzs7dUJBQUE7SUFnQkYsc0JBQUM7QUFBRCxDQWRBLEFBY0MsSUFBQTtBQWRZLHVCQUFlLGtCQWMzQixDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2xvZ291dC9sb2dvdXQuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmltcG9ydCB7IENlbnRyZVVzZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vY29yZS9pbmRleCc7XHJcbmltcG9ydCB7IEFjY2Vzc1Rva2VuU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL2NvcmUvaW5kZXgnO1xyXG5pbXBvcnQgeyBDb25maWcsIEludGVyY29tU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9pbmRleCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICBzZWxlY3RvcjogJ2xvZ291dC1jbXAnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICdsb2dvdXQuY29tcG9uZW50Lmh0bWwnXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgTG9nb3V0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwcml2YXRlIF9hdXRoZW50aWNhdGlvblNlcnZpY2U6IEFjY2Vzc1Rva2VuU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIF9pbnRlcmNvbVNlcnZpY2U6IEludGVyY29tU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIF91c2VyU2VydmljZTogQ2VudHJlVXNlclNlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLl9hdXRoZW50aWNhdGlvblNlcnZpY2UubG9nb3V0KCk7XHJcbiAgICAgICAgdGhpcy5fdXNlclNlcnZpY2UuU2V0TGFzdFNlbGVjdGVkQ2VudHJlSWQodGhpcy5fdXNlclNlcnZpY2UuU2VsZWN0ZWRDZW50cmUuaWQpO1xyXG4gICAgICAgIHRoaXMuX3VzZXJTZXJ2aWNlLkNsZWFyKCk7XHJcbiAgICAgICAgdGhpcy5faW50ZXJjb21TZXJ2aWNlLnVzZXJMb2dnZWRPdXQoQ29uZmlnLklOVEVSQ09NX0FQUF9JRCk7XHJcbiAgICB9XHJcbn1cclxuIl19
