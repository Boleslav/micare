"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../../api/index');
var StaffService = (function () {
    function StaffService(_apiService) {
        this._apiService = _apiService;
    }
    StaffService.prototype.GetCurrentStaffMembersForCentre$ = function (centreId) {
        return this._apiService.staff_GetCurrentStaffMembersForCentreAsync(centreId);
    };
    StaffService.prototype.RemoveCentreStaffMember$ = function (centreId, staffMemberId) {
        return this._apiService.staff_RemoveCentreStaffMemberAsync(centreId, staffMemberId);
    };
    StaffService.prototype.InviteStaffMember$ = function (centreId, email) {
        return this._apiService.account_InviteStaffMember(centreId, email);
    };
    StaffService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.ApiClientService])
    ], StaffService);
    return StaffService;
}());
exports.StaffService = StaffService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3N0YWZmL3N0YWZmU2VydmljZS9zdGFmZi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBMkIsZUFBZSxDQUFDLENBQUE7QUFHM0Msc0JBQXVELHVCQUF1QixDQUFDLENBQUE7QUFHL0U7SUFFSSxzQkFBb0IsV0FBNkI7UUFBN0IsZ0JBQVcsR0FBWCxXQUFXLENBQWtCO0lBRWpELENBQUM7SUFFTSx1REFBZ0MsR0FBdkMsVUFBd0MsUUFBZ0I7UUFDcEQsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsMENBQTBDLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDakYsQ0FBQztJQUVNLCtDQUF3QixHQUEvQixVQUFnQyxRQUFnQixFQUFFLGFBQXFCO1FBQ25FLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGtDQUFrQyxDQUFDLFFBQVEsRUFBRSxhQUFhLENBQUMsQ0FBQztJQUN4RixDQUFDO0lBRU0seUNBQWtCLEdBQXpCLFVBQTBCLFFBQWdCLEVBQUUsS0FBYTtRQUNyRCxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyx5QkFBeUIsQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDdkUsQ0FBQztJQWpCTDtRQUFDLGlCQUFVLEVBQUU7O29CQUFBO0lBa0JiLG1CQUFDO0FBQUQsQ0FqQkEsQUFpQkMsSUFBQTtBQWpCWSxvQkFBWSxlQWlCeEIsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9zdGFmZi9zdGFmZlNlcnZpY2Uvc3RhZmYuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgQXBpQ2xpZW50U2VydmljZSwgQ2VudHJlU3RhZmZNZW1iZXJEdG8gfSBmcm9tICcuLi8uLi8uLi8uLi9hcGkvaW5kZXgnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgU3RhZmZTZXJ2aWNlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9hcGlTZXJ2aWNlOiBBcGlDbGllbnRTZXJ2aWNlKSB7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBHZXRDdXJyZW50U3RhZmZNZW1iZXJzRm9yQ2VudHJlJChjZW50cmVJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxDZW50cmVTdGFmZk1lbWJlckR0b1tdPiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2FwaVNlcnZpY2Uuc3RhZmZfR2V0Q3VycmVudFN0YWZmTWVtYmVyc0ZvckNlbnRyZUFzeW5jKGNlbnRyZUlkKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgUmVtb3ZlQ2VudHJlU3RhZmZNZW1iZXIkKGNlbnRyZUlkOiBzdHJpbmcsIHN0YWZmTWVtYmVySWQ6IHN0cmluZyk6IE9ic2VydmFibGU8dm9pZD4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9hcGlTZXJ2aWNlLnN0YWZmX1JlbW92ZUNlbnRyZVN0YWZmTWVtYmVyQXN5bmMoY2VudHJlSWQsIHN0YWZmTWVtYmVySWQpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBJbnZpdGVTdGFmZk1lbWJlciQoY2VudHJlSWQ6IHN0cmluZywgZW1haWw6IHN0cmluZyk6IE9ic2VydmFibGU8dm9pZD4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9hcGlTZXJ2aWNlLmFjY291bnRfSW52aXRlU3RhZmZNZW1iZXIoY2VudHJlSWQsIGVtYWlsKTtcclxuICAgIH1cclxufVxyXG4iXX0=
