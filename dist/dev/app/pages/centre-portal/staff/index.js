"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
__export(require('./staff.component'));
__export(require('./staffTable/staffTable.component'));
__export(require('./staffService/staff.service'));
__export(require('./staff.routes'));
__export(require('./staff.module'));

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3N0YWZmL2luZGV4LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxpQkFBYyxtQkFBbUIsQ0FBQyxFQUFBO0FBQ2xDLGlCQUFjLG1DQUFtQyxDQUFDLEVBQUE7QUFDbEQsaUJBQWMsOEJBQThCLENBQUMsRUFBQTtBQUM3QyxpQkFBYyxnQkFBZ0IsQ0FBQyxFQUFBO0FBQy9CLGlCQUFjLGdCQUFnQixDQUFDLEVBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL2NlbnRyZS1wb3J0YWwvc3RhZmYvaW5kZXguanMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgKiBmcm9tICcuL3N0YWZmLmNvbXBvbmVudCc7XHJcbmV4cG9ydCAqIGZyb20gJy4vc3RhZmZUYWJsZS9zdGFmZlRhYmxlLmNvbXBvbmVudCc7XHJcbmV4cG9ydCAqIGZyb20gJy4vc3RhZmZTZXJ2aWNlL3N0YWZmLnNlcnZpY2UnO1xyXG5leHBvcnQgKiBmcm9tICcuL3N0YWZmLnJvdXRlcyc7XHJcbmV4cG9ydCAqIGZyb20gJy4vc3RhZmYubW9kdWxlJztcclxuIl19
