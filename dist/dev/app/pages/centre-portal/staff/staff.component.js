"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var StaffComponent = (function () {
    function StaffComponent() {
    }
    StaffComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'staff-cmp',
            templateUrl: './staff.component.html'
        }), 
        __metadata('design:paramtypes', [])
    ], StaffComponent);
    return StaffComponent;
}());
exports.StaffComponent = StaffComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3N0YWZmL3N0YWZmLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQTBCLGVBQWUsQ0FBQyxDQUFBO0FBUTFDO0lBQUE7SUFDQSxDQUFDO0lBUEQ7UUFBQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxXQUFXO1lBQ3JCLFdBQVcsRUFBRSx3QkFBd0I7U0FDeEMsQ0FBQzs7c0JBQUE7SUFHRixxQkFBQztBQUFELENBREEsQUFDQyxJQUFBO0FBRFksc0JBQWMsaUJBQzFCLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL2NlbnRyZS1wb3J0YWwvc3RhZmYvc3RhZmYuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICBzZWxlY3RvcjogJ3N0YWZmLWNtcCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vc3RhZmYuY29tcG9uZW50Lmh0bWwnXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgU3RhZmZDb21wb25lbnQge1xyXG59XHJcbiJdfQ==
