"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var angular2_toaster_1 = require('angular2-toaster');
var staff_service_1 = require('../staffService/staff.service');
var index_1 = require('../../../../core/index');
var index_2 = require('../../../../shared/index');
var StaffTableComponent = (function () {
    function StaffTableComponent(_staffService, _userService, _toasterService) {
        this._staffService = _staffService;
        this._userService = _userService;
        this._toasterService = _toasterService;
        this.disableButtons = false;
        this.addMode = false;
    }
    StaffTableComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._staffService
            .GetCurrentStaffMembersForCentre$(this._userService.SelectedCentre.id)
            .subscribe(function (staffRes) {
            _this.staffMembers = staffRes;
        });
    };
    StaffTableComponent.prototype.setAddMode = function (flag) {
        this.addMode = flag;
    };
    StaffTableComponent.prototype.inviteStaffMember = function (email) {
        var _this = this;
        console.log(email);
        var savingToast = this._toasterService.pop(new index_2.SpinningToast());
        this.disableButtons = true;
        this._staffService
            .InviteStaffMember$(this._userService.SelectedCentre.id, email)
            .subscribe(function (res) {
            _this._staffService
                .GetCurrentStaffMembersForCentre$(_this._userService.SelectedCentre.id)
                .subscribe(function (staffRes) {
                _this.staffMembers = staffRes;
            });
            _this.addMode = false;
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this._toasterService.popAsync('success', 'Invite Sent', '');
        }, function (err) { return _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId); });
        this.disableButtons = false;
        this.email = '';
    };
    StaffTableComponent.prototype.removeStaffMember = function (centreId, staffMemberId) {
        var _this = this;
        if (window.confirm('Are you sure you want to remove the staff member from the centre. Press Ok to continue.')) {
            var savingToast_1 = this._toasterService.pop(new index_2.SpinningToast());
            this.disableButtons = true;
            this._staffService
                .RemoveCentreStaffMember$(centreId, staffMemberId)
                .subscribe(function (res) {
                _this._staffService
                    .GetCurrentStaffMembersForCentre$(_this._userService.SelectedCentre.id)
                    .subscribe(function (staffRes) {
                    _this.staffMembers = staffRes;
                });
                _this._toasterService.clear(savingToast_1.toastId, savingToast_1.toastContainerId);
                _this._toasterService.popAsync('success', 'Removed', '');
            }, function (err) { return _this._toasterService.clear(savingToast_1.toastId, savingToast_1.toastContainerId); });
            this.disableButtons = false;
        }
    };
    StaffTableComponent.prototype.staffStatusString = function (status) {
        switch (status) {
            case 0:
                return 'Invited';
            case 1:
                return 'Active';
            case 2:
                return 'Inactive';
            case 3:
                return 'Locked Out';
            case 4:
                return 'Password Expired';
            default:
                return 'Unknown';
        }
    };
    StaffTableComponent.prototype.staffStatusStyle = function (status) {
        switch (status) {
            case 0:
                return 'btn btn-sm btn-info btn-block';
            case 1:
                return 'btn btn-sm btn-success btn-block';
            case 2:
                return 'btn btn-sm btn-warning btn-block';
            case 3:
                return 'btn btn-sm btn-danger btn-block';
            case 4:
                return 'btn btn-sm btn-danger btn-block';
            default:
                return 'btn btn-sm btn-seconday btn-block';
        }
    };
    StaffTableComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'staff-table-cmp',
            templateUrl: './staffTable.component.html'
        }), 
        __metadata('design:paramtypes', [staff_service_1.StaffService, index_1.CentreUserService, angular2_toaster_1.ToasterService])
    ], StaffTableComponent);
    return StaffTableComponent;
}());
exports.StaffTableComponent = StaffTableComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3N0YWZmL3N0YWZmVGFibGUvc3RhZmZUYWJsZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUFrQyxlQUFlLENBQUMsQ0FBQTtBQUNsRCxpQ0FBK0Isa0JBQWtCLENBQUMsQ0FBQTtBQUNsRCw4QkFBNkIsK0JBQStCLENBQUMsQ0FBQTtBQUM3RCxzQkFBa0Msd0JBQXdCLENBQUMsQ0FBQTtBQUMzRCxzQkFBOEIsMEJBQTBCLENBQUMsQ0FBQTtBQVN6RDtJQVdJLDZCQUFvQixhQUEyQixFQUNuQyxZQUErQixFQUMvQixlQUErQjtRQUZ2QixrQkFBYSxHQUFiLGFBQWEsQ0FBYztRQUNuQyxpQkFBWSxHQUFaLFlBQVksQ0FBbUI7UUFDL0Isb0JBQWUsR0FBZixlQUFlLENBQWdCO1FBUG5DLG1CQUFjLEdBQUcsS0FBSyxDQUFDO1FBR3ZCLFlBQU8sR0FBRyxLQUFLLENBQUM7SUFLeEIsQ0FBQztJQUVELHNDQUFRLEdBQVI7UUFBQSxpQkFNQztRQUxHLElBQUksQ0FBQyxhQUFhO2FBQ2IsZ0NBQWdDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsRUFBRSxDQUFDO2FBQ3JFLFNBQVMsQ0FBQyxVQUFBLFFBQVE7WUFDZixLQUFJLENBQUMsWUFBWSxHQUFHLFFBQVEsQ0FBQztRQUNqQyxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFRCx3Q0FBVSxHQUFWLFVBQVcsSUFBYTtRQUNwQixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztJQUN4QixDQUFDO0lBRUQsK0NBQWlCLEdBQWpCLFVBQWtCLEtBQWE7UUFBL0IsaUJBcUJDO1FBcEJHLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDbkIsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsSUFBSSxxQkFBYSxFQUFFLENBQUMsQ0FBQztRQUNoRSxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztRQUMzQixJQUFJLENBQUMsYUFBYTthQUNiLGtCQUFrQixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLEVBQUUsRUFBRSxLQUFLLENBQUM7YUFDOUQsU0FBUyxDQUNWLFVBQUEsR0FBRztZQUVDLEtBQUksQ0FBQyxhQUFhO2lCQUNiLGdDQUFnQyxDQUFDLEtBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FBQztpQkFDckUsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDZixLQUFJLENBQUMsWUFBWSxHQUFHLFFBQVEsQ0FBQztZQUNqQyxDQUFDLENBQUMsQ0FBQztZQUNQLEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1lBQ3JCLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDOUUsS0FBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLGFBQWEsRUFBRSxFQUFFLENBQUMsQ0FBQztRQUNoRSxDQUFDLEVBQ0QsVUFBQyxHQUFHLElBQUssT0FBQSxLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUE3RSxDQUE2RSxDQUFDLENBQUM7UUFDNUYsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7UUFDNUIsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7SUFDcEIsQ0FBQztJQUVELCtDQUFpQixHQUFqQixVQUFrQixRQUFnQixFQUFFLGFBQXFCO1FBQXpELGlCQW9CQztRQW5CRyxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLHlGQUF5RixDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzVHLElBQUksYUFBVyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUkscUJBQWEsRUFBRSxDQUFDLENBQUM7WUFDaEUsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7WUFDM0IsSUFBSSxDQUFDLGFBQWE7aUJBQ2Isd0JBQXdCLENBQUMsUUFBUSxFQUFFLGFBQWEsQ0FBQztpQkFDakQsU0FBUyxDQUNWLFVBQUEsR0FBRztnQkFFQyxLQUFJLENBQUMsYUFBYTtxQkFDYixnQ0FBZ0MsQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxFQUFFLENBQUM7cUJBQ3JFLFNBQVMsQ0FBQyxVQUFBLFFBQVE7b0JBQ2YsS0FBSSxDQUFDLFlBQVksR0FBRyxRQUFRLENBQUM7Z0JBQ2pDLENBQUMsQ0FBQyxDQUFDO2dCQUNQLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLGFBQVcsQ0FBQyxPQUFPLEVBQUUsYUFBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0JBQzlFLEtBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLFNBQVMsRUFBRSxTQUFTLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDNUQsQ0FBQyxFQUNELFVBQUMsR0FBRyxJQUFLLE9BQUEsS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsYUFBVyxDQUFDLE9BQU8sRUFBRSxhQUFXLENBQUMsZ0JBQWdCLENBQUMsRUFBN0UsQ0FBNkUsQ0FBQyxDQUFDO1lBQzVGLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1FBQ2hDLENBQUM7SUFDTCxDQUFDO0lBRUQsK0NBQWlCLEdBQWpCLFVBQWtCLE1BQWM7UUFDNUIsTUFBTSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUNiLEtBQUssQ0FBQztnQkFDRixNQUFNLENBQUMsU0FBUyxDQUFDO1lBQ3JCLEtBQUssQ0FBQztnQkFDRixNQUFNLENBQUMsUUFBUSxDQUFDO1lBQ3BCLEtBQUssQ0FBQztnQkFDRixNQUFNLENBQUMsVUFBVSxDQUFDO1lBQ3RCLEtBQUssQ0FBQztnQkFDRixNQUFNLENBQUMsWUFBWSxDQUFDO1lBQ3hCLEtBQUssQ0FBQztnQkFDRixNQUFNLENBQUMsa0JBQWtCLENBQUM7WUFDOUI7Z0JBQ0ksTUFBTSxDQUFDLFNBQVMsQ0FBQztRQUN6QixDQUFDO0lBQ0wsQ0FBQztJQUVELDhDQUFnQixHQUFoQixVQUFpQixNQUFjO1FBQzNCLE1BQU0sQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDYixLQUFLLENBQUM7Z0JBQ0YsTUFBTSxDQUFDLCtCQUErQixDQUFDO1lBQzNDLEtBQUssQ0FBQztnQkFDRixNQUFNLENBQUMsa0NBQWtDLENBQUM7WUFDOUMsS0FBSyxDQUFDO2dCQUNGLE1BQU0sQ0FBQyxrQ0FBa0MsQ0FBQztZQUM5QyxLQUFLLENBQUM7Z0JBQ0YsTUFBTSxDQUFDLGlDQUFpQyxDQUFDO1lBQzdDLEtBQUssQ0FBQztnQkFDRixNQUFNLENBQUMsaUNBQWlDLENBQUM7WUFDN0M7Z0JBQ0ksTUFBTSxDQUFDLG1DQUFtQyxDQUFDO1FBQ25ELENBQUM7SUFDTCxDQUFDO0lBL0dMO1FBQUMsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsaUJBQWlCO1lBQzNCLFdBQVcsRUFBRSw2QkFBNkI7U0FDN0MsQ0FBQzs7MkJBQUE7SUE0R0YsMEJBQUM7QUFBRCxDQTFHQSxBQTBHQyxJQUFBO0FBMUdZLDJCQUFtQixzQkEwRy9CLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL2NlbnRyZS1wb3J0YWwvc3RhZmYvc3RhZmZUYWJsZS9zdGFmZlRhYmxlLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFRvYXN0ZXJTZXJ2aWNlIH0gZnJvbSAnYW5ndWxhcjItdG9hc3Rlcic7XHJcbmltcG9ydCB7IFN0YWZmU2VydmljZSB9IGZyb20gJy4uL3N0YWZmU2VydmljZS9zdGFmZi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQ2VudHJlVXNlclNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi9jb3JlL2luZGV4JztcclxuaW1wb3J0IHsgU3Bpbm5pbmdUb2FzdCB9IGZyb20gJy4uLy4uLy4uLy4uL3NoYXJlZC9pbmRleCc7XHJcbmltcG9ydCB7IENlbnRyZVN0YWZmTWVtYmVyRHRvIH0gZnJvbSAnLi4vLi4vLi4vLi4vYXBpL2luZGV4JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHNlbGVjdG9yOiAnc3RhZmYtdGFibGUtY21wJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9zdGFmZlRhYmxlLmNvbXBvbmVudC5odG1sJ1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFN0YWZmVGFibGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIHByaXZhdGUgc3RhZmZNZW1iZXJzOiBDZW50cmVTdGFmZk1lbWJlckR0b1tdO1xyXG4gICAgcHJpdmF0ZSBlbWFpbDogc3RyaW5nO1xyXG5cclxuICAgIC8vIEZsYWcgdG8gcHJldmVudCBkb3VibGUgc3VibWl0XHJcbiAgICBwcml2YXRlIGRpc2FibGVCdXR0b25zID0gZmFsc2U7XHJcblxyXG4gICAgLy8gRmxhZyB0byBlbnRlciBBZGQgU3RhZmYgTW9kZVxyXG4gICAgcHJpdmF0ZSBhZGRNb2RlID0gZmFsc2U7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfc3RhZmZTZXJ2aWNlOiBTdGFmZlNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBfdXNlclNlcnZpY2U6IENlbnRyZVVzZXJTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgX3RvYXN0ZXJTZXJ2aWNlOiBUb2FzdGVyU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuX3N0YWZmU2VydmljZVxyXG4gICAgICAgICAgICAuR2V0Q3VycmVudFN0YWZmTWVtYmVyc0ZvckNlbnRyZSQodGhpcy5fdXNlclNlcnZpY2UuU2VsZWN0ZWRDZW50cmUuaWQpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoc3RhZmZSZXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zdGFmZk1lbWJlcnMgPSBzdGFmZlJlcztcclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0QWRkTW9kZShmbGFnOiBib29sZWFuKSB7XHJcbiAgICAgICAgdGhpcy5hZGRNb2RlID0gZmxhZztcclxuICAgIH1cclxuXHJcbiAgICBpbnZpdGVTdGFmZk1lbWJlcihlbWFpbDogc3RyaW5nKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coZW1haWwpO1xyXG4gICAgICAgIGxldCBzYXZpbmdUb2FzdCA9IHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLnBvcChuZXcgU3Bpbm5pbmdUb2FzdCgpKTtcclxuICAgICAgICB0aGlzLmRpc2FibGVCdXR0b25zID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLl9zdGFmZlNlcnZpY2VcclxuICAgICAgICAgICAgLkludml0ZVN0YWZmTWVtYmVyJCh0aGlzLl91c2VyU2VydmljZS5TZWxlY3RlZENlbnRyZS5pZCwgZW1haWwpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgIHJlcyA9PiB7XHJcbiAgICAgICAgICAgICAgICAvLyBHZXQgbGF0ZXN0IHN0YWZmIG1lbWJlcnNcclxuICAgICAgICAgICAgICAgIHRoaXMuX3N0YWZmU2VydmljZVxyXG4gICAgICAgICAgICAgICAgICAgIC5HZXRDdXJyZW50U3RhZmZNZW1iZXJzRm9yQ2VudHJlJCh0aGlzLl91c2VyU2VydmljZS5TZWxlY3RlZENlbnRyZS5pZClcclxuICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHN0YWZmUmVzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGFmZk1lbWJlcnMgPSBzdGFmZlJlcztcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIHRoaXMuYWRkTW9kZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoc2F2aW5nVG9hc3QudG9hc3RJZCwgc2F2aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5wb3BBc3luYygnc3VjY2VzcycsICdJbnZpdGUgU2VudCcsICcnKTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgKGVycikgPT4gdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoc2F2aW5nVG9hc3QudG9hc3RJZCwgc2F2aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCkpO1xyXG4gICAgICAgIHRoaXMuZGlzYWJsZUJ1dHRvbnMgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLmVtYWlsID0gJyc7XHJcbiAgICB9XHJcblxyXG4gICAgcmVtb3ZlU3RhZmZNZW1iZXIoY2VudHJlSWQ6IHN0cmluZywgc3RhZmZNZW1iZXJJZDogc3RyaW5nKSB7XHJcbiAgICAgICAgaWYgKHdpbmRvdy5jb25maXJtKCdBcmUgeW91IHN1cmUgeW91IHdhbnQgdG8gcmVtb3ZlIHRoZSBzdGFmZiBtZW1iZXIgZnJvbSB0aGUgY2VudHJlLiBQcmVzcyBPayB0byBjb250aW51ZS4nKSkge1xyXG4gICAgICAgICAgICBsZXQgc2F2aW5nVG9hc3QgPSB0aGlzLl90b2FzdGVyU2VydmljZS5wb3AobmV3IFNwaW5uaW5nVG9hc3QoKSk7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzYWJsZUJ1dHRvbnMgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLl9zdGFmZlNlcnZpY2VcclxuICAgICAgICAgICAgICAgIC5SZW1vdmVDZW50cmVTdGFmZk1lbWJlciQoY2VudHJlSWQsIHN0YWZmTWVtYmVySWQpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgICAgcmVzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBHZXQgbGF0ZXN0IHN0YWZmIG1lbWJlcnNcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl9zdGFmZlNlcnZpY2VcclxuICAgICAgICAgICAgICAgICAgICAgICAgLkdldEN1cnJlbnRTdGFmZk1lbWJlcnNGb3JDZW50cmUkKHRoaXMuX3VzZXJTZXJ2aWNlLlNlbGVjdGVkQ2VudHJlLmlkKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHN0YWZmUmVzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc3RhZmZNZW1iZXJzID0gc3RhZmZSZXM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLmNsZWFyKHNhdmluZ1RvYXN0LnRvYXN0SWQsIHNhdmluZ1RvYXN0LnRvYXN0Q29udGFpbmVySWQpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLnBvcEFzeW5jKCdzdWNjZXNzJywgJ1JlbW92ZWQnLCAnJyk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgKGVycikgPT4gdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoc2F2aW5nVG9hc3QudG9hc3RJZCwgc2F2aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCkpO1xyXG4gICAgICAgICAgICB0aGlzLmRpc2FibGVCdXR0b25zID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHN0YWZmU3RhdHVzU3RyaW5nKHN0YXR1czogbnVtYmVyKSB7XHJcbiAgICAgICAgc3dpdGNoIChzdGF0dXMpIHtcclxuICAgICAgICAgICAgY2FzZSAwOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuICdJbnZpdGVkJztcclxuICAgICAgICAgICAgY2FzZSAxOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuICdBY3RpdmUnO1xyXG4gICAgICAgICAgICBjYXNlIDI6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gJ0luYWN0aXZlJztcclxuICAgICAgICAgICAgY2FzZSAzOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuICdMb2NrZWQgT3V0JztcclxuICAgICAgICAgICAgY2FzZSA0OlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuICdQYXNzd29yZCBFeHBpcmVkJztcclxuICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgICAgIHJldHVybiAnVW5rbm93bic7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHN0YWZmU3RhdHVzU3R5bGUoc3RhdHVzOiBudW1iZXIpIHtcclxuICAgICAgICBzd2l0Y2ggKHN0YXR1cykge1xyXG4gICAgICAgICAgICBjYXNlIDA6IC8vIEludml0ZWRcclxuICAgICAgICAgICAgICAgIHJldHVybiAnYnRuIGJ0bi1zbSBidG4taW5mbyBidG4tYmxvY2snO1xyXG4gICAgICAgICAgICBjYXNlIDE6IC8vIEFjdGl2ZVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuICdidG4gYnRuLXNtIGJ0bi1zdWNjZXNzIGJ0bi1ibG9jayc7XHJcbiAgICAgICAgICAgIGNhc2UgMjogLy8gSW5hY3RpdmVcclxuICAgICAgICAgICAgICAgIHJldHVybiAnYnRuIGJ0bi1zbSBidG4td2FybmluZyBidG4tYmxvY2snO1xyXG4gICAgICAgICAgICBjYXNlIDM6IC8vIExvY2tlZE91dFxyXG4gICAgICAgICAgICAgICAgcmV0dXJuICdidG4gYnRuLXNtIGJ0bi1kYW5nZXIgYnRuLWJsb2NrJztcclxuICAgICAgICAgICAgY2FzZSA0OiAvLyBQYXNzd29yZCBFeHBpcmVkXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gJ2J0biBidG4tc20gYnRuLWRhbmdlciBidG4tYmxvY2snO1xyXG4gICAgICAgICAgICBkZWZhdWx0OiAgICAvLyBVbmtvd25cclxuICAgICAgICAgICAgICAgIHJldHVybiAnYnRuIGJ0bi1zbSBidG4tc2Vjb25kYXkgYnRuLWJsb2NrJztcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19
