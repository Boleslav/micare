"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var bookings_service_1 = require('../bookingsService/bookings.service');
var rooms_service_1 = require('../../rooms/roomsService/rooms.service');
var index_1 = require('../../../../core/index');
var angular2_toaster_1 = require('angular2-toaster');
var index_2 = require('../../../../shared/index');
var index_3 = require('../../../../api/index');
var age_converter_1 = require('../../rooms/components/age-converter');
var BookingsTableComponent = (function () {
    function BookingsTableComponent(_bookingsService, _roomsService, _userService, _toasterService) {
        this._bookingsService = _bookingsService;
        this._roomsService = _roomsService;
        this._userService = _userService;
        this._toasterService = _toasterService;
        this.isDisableAddEdit = false;
        this.disableSave = false;
    }
    BookingsTableComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.isAddMode = false;
        this._bookingsService
            .GetCentrePermanentBookings$(this._userService.SelectedCentre.id)
            .subscribe(function (res) {
            _this.centreChildren = res;
        });
        this._roomsService
            .GetRooms$(this._userService.SelectedCentre.id)
            .subscribe(function (res) {
            _this.rooms = res;
        });
    };
    BookingsTableComponent.prototype.addBooking = function (child, childIdx, bookingIdx) {
        this.isAddMode = true;
        this.editChildRef = childIdx;
        this.isDisableAddEdit = true;
        var newBooking = new index_3.PermanentBookingDto();
        newBooking.centreId = child.centreId;
        newBooking.kidId = child.kidId;
        child.permanentBookingList.push(newBooking);
        this.editBookingRef = child.permanentBookingList.length - 1;
    };
    BookingsTableComponent.prototype.updateBooking = function (booking, child) {
        var _this = this;
        var savingToast = this._toasterService.pop(new index_2.SpinningToast());
        this.disableSave = true;
        this._bookingsService
            .UpdatePermanentBooking$(booking)
            .subscribe(function (res) {
            _this.resetEditingProperties();
            _this._bookingsService.GetPermanentBookings$(child.centreId, child.kidId)
                .subscribe(function (res) {
                child.permanentBookingList = res;
            });
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this._toasterService.popAsync('success', 'Booking updated', '');
        }, function (err) { return _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId); });
        this.disableSave = false;
    };
    BookingsTableComponent.prototype.deleteBooking = function (booking, child) {
        var _this = this;
        if (window.confirm('Are you sure you want to remove the booking?')) {
            var savingToast_1 = this._toasterService.pop(new index_2.SpinningToast());
            this._bookingsService
                .DeletePermanentBooking$(booking.centreId, booking.id)
                .subscribe(function (res) {
                var index = child.permanentBookingList.indexOf(booking);
                child.permanentBookingList.splice(index, 1);
                _this._toasterService.clear(savingToast_1.toastId, savingToast_1.toastContainerId);
                _this._toasterService.popAsync('success', 'Booking removed', '');
            }, function (err) { return _this._toasterService.clear(savingToast_1.toastId, savingToast_1.toastContainerId); });
        }
    };
    BookingsTableComponent.prototype.editBooking = function (childIdx, bookingIdx, permanentBooking) {
        this.editChildRef = childIdx;
        this.editBookingRef = bookingIdx;
        this.isDisableAddEdit = true;
        this.preEditedRoomId = permanentBooking.roomId;
        this.preEditedMon = permanentBooking.mon;
        this.preEditedTue = permanentBooking.tue;
        this.preEditedWed = permanentBooking.wed;
        this.preEditedThu = permanentBooking.thu;
        this.preEditedFri = permanentBooking.fri;
        this.preEditedSat = permanentBooking.sat;
        this.preEditedSun = permanentBooking.sun;
    };
    BookingsTableComponent.prototype.cancelEdit = function (permanentBooking, child) {
        if (this.isAddMode) {
            var index = child.permanentBookingList.indexOf(permanentBooking);
            child.permanentBookingList.splice(index, 1);
        }
        else {
            permanentBooking.roomId = this.preEditedRoomId;
            permanentBooking.mon = this.preEditedMon;
            permanentBooking.tue = this.preEditedTue;
            permanentBooking.wed = this.preEditedWed;
            permanentBooking.thu = this.preEditedThu;
            permanentBooking.fri = this.preEditedFri;
            permanentBooking.sat = this.preEditedSat;
            permanentBooking.sun = this.preEditedSun;
        }
        this.resetEditingProperties();
    };
    BookingsTableComponent.prototype.editMode = function (childIdx, bookingIdx) {
        if (childIdx === this.editChildRef && bookingIdx === this.editBookingRef) {
            return true;
        }
        return false;
    };
    BookingsTableComponent.prototype.addMode = function () {
        return this.isAddMode;
    };
    BookingsTableComponent.prototype.disableAddEdit = function () {
        return this.isDisableAddEdit;
    };
    BookingsTableComponent.prototype.isInvalidRoom = function (booking) {
        return (booking.roomId === '' || booking.roomId === null || booking.roomId === undefined);
    };
    BookingsTableComponent.prototype.getRoomName = function (roomId) {
        if (this.rooms === undefined || this.rooms === null)
            return '';
        var filteredRooms = this.rooms.filter(function (x) { return x.id === roomId; });
        return filteredRooms.length > 0 ? filteredRooms[0].name : '';
    };
    BookingsTableComponent.prototype.resetEditingProperties = function () {
        this.editChildRef = null;
        this.editBookingRef = null;
        this.preEditedRoomId = null;
        this.preEditedMon = null;
        this.preEditedTue = null;
        this.preEditedWed = null;
        this.preEditedThu = null;
        this.preEditedFri = null;
        this.preEditedSat = null;
        this.preEditedSun = null;
        this.isAddMode = false;
        this.isDisableAddEdit = false;
    };
    BookingsTableComponent.prototype.getChildAgeYears = function (age) {
        return age_converter_1.AgeConverter.getYearFraction(parseFloat(age)).toString();
    };
    BookingsTableComponent.prototype.getChildAgeMonths = function (age) {
        return age_converter_1.AgeConverter.getMonthFraction(parseFloat(age)).toString();
    };
    BookingsTableComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'bookings-table-cmp',
            templateUrl: './bookingsTable.component.html',
            styleUrls: ['./bookingsTable.component.css']
        }), 
        __metadata('design:paramtypes', [bookings_service_1.BookingsService, rooms_service_1.RoomsService, index_1.CentreUserService, angular2_toaster_1.ToasterService])
    ], BookingsTableComponent);
    return BookingsTableComponent;
}());
exports.BookingsTableComponent = BookingsTableComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2Jvb2tpbmdzL2Jvb2tpbmdzVGFibGUvYm9va2luZ3NUYWJsZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUFrQyxlQUFlLENBQUMsQ0FBQTtBQUVsRCxpQ0FBZ0MscUNBQXFDLENBQUMsQ0FBQTtBQUN0RSw4QkFBNkIsd0NBQXdDLENBQUMsQ0FBQTtBQUN0RSxzQkFBa0Msd0JBQXdCLENBQUMsQ0FBQTtBQUMzRCxpQ0FBK0Isa0JBQWtCLENBQUMsQ0FBQTtBQUNsRCxzQkFBOEIsMEJBQTBCLENBQUMsQ0FBQTtBQUN6RCxzQkFBdUUsdUJBQXVCLENBQUMsQ0FBQTtBQUMvRiw4QkFBNkIsc0NBQXNDLENBQUMsQ0FBQTtBQVFwRTtJQXFCSSxnQ0FBb0IsZ0JBQWlDLEVBQ3pDLGFBQTJCLEVBQzNCLFlBQStCLEVBQy9CLGVBQStCO1FBSHZCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBaUI7UUFDekMsa0JBQWEsR0FBYixhQUFhLENBQWM7UUFDM0IsaUJBQVksR0FBWixZQUFZLENBQW1CO1FBQy9CLG9CQUFlLEdBQWYsZUFBZSxDQUFnQjtRQVBuQyxxQkFBZ0IsR0FBWSxLQUFLLENBQUM7UUFFbEMsZ0JBQVcsR0FBWSxLQUFLLENBQUM7SUFLVSxDQUFDO0lBRWhELHlDQUFRLEdBQVI7UUFBQSxpQkFlQztRQWRHLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxnQkFBZ0I7YUFDaEIsMkJBQTJCLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsRUFBRSxDQUFDO2FBQ2hFLFNBQVMsQ0FDVixVQUFBLEdBQUc7WUFDQyxLQUFJLENBQUMsY0FBYyxHQUFHLEdBQUcsQ0FBQztRQUM5QixDQUFDLENBQUMsQ0FBQztRQUVQLElBQUksQ0FBQyxhQUFhO2FBQ2IsU0FBUyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FBQzthQUM5QyxTQUFTLENBQ1YsVUFBQSxHQUFHO1lBQ0MsS0FBSSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7UUFDckIsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRUQsMkNBQVUsR0FBVixVQUFXLEtBQStCLEVBQUUsUUFBZ0IsRUFBRSxVQUFrQjtRQUU1RSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUN0QixJQUFJLENBQUMsWUFBWSxHQUFHLFFBQVEsQ0FBQztRQUM3QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO1FBRzdCLElBQUksVUFBVSxHQUFHLElBQUksMkJBQW1CLEVBQUUsQ0FBQztRQUMzQyxVQUFVLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUM7UUFDckMsVUFBVSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO1FBRy9CLEtBQUssQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFHNUMsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUMsb0JBQW9CLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztJQUNoRSxDQUFDO0lBRUQsOENBQWEsR0FBYixVQUFjLE9BQTRCLEVBQUUsS0FBK0I7UUFBM0UsaUJBb0JDO1FBbkJHLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUkscUJBQWEsRUFBRSxDQUFDLENBQUM7UUFDaEUsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7UUFDeEIsSUFBSSxDQUFDLGdCQUFnQjthQUNoQix1QkFBdUIsQ0FBQyxPQUFPLENBQUM7YUFDaEMsU0FBUyxDQUNWLFVBQUEsR0FBRztZQUVDLEtBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1lBRTlCLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxxQkFBcUIsQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxLQUFLLENBQUM7aUJBQ25FLFNBQVMsQ0FDVixVQUFBLEdBQUc7Z0JBQ0MsS0FBSyxDQUFDLG9CQUFvQixHQUFHLEdBQUcsQ0FBQztZQUNyQyxDQUFDLENBQUMsQ0FBQztZQUNQLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDOUUsS0FBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLGlCQUFpQixFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQ3BFLENBQUMsRUFDRCxVQUFDLEdBQUcsSUFBSyxPQUFBLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLGdCQUFnQixDQUFDLEVBQTdFLENBQTZFLENBQUMsQ0FBQztRQUM1RixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztJQUM3QixDQUFDO0lBRUQsOENBQWEsR0FBYixVQUFjLE9BQTRCLEVBQUUsS0FBK0I7UUFBM0UsaUJBYUM7UUFaRyxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLDhDQUE4QyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2pFLElBQUksYUFBVyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUkscUJBQWEsRUFBRSxDQUFDLENBQUM7WUFDaEUsSUFBSSxDQUFDLGdCQUFnQjtpQkFDaEIsdUJBQXVCLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxPQUFPLENBQUMsRUFBRSxDQUFDO2lCQUNyRCxTQUFTLENBQUMsVUFBQSxHQUFHO2dCQUNWLElBQUksS0FBSyxHQUFHLEtBQUssQ0FBQyxvQkFBb0IsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ3hELEtBQUssQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUM1QyxLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxhQUFXLENBQUMsT0FBTyxFQUFFLGFBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2dCQUM5RSxLQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsaUJBQWlCLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDcEUsQ0FBQyxFQUNELFVBQUMsR0FBRyxJQUFLLE9BQUEsS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsYUFBVyxDQUFDLE9BQU8sRUFBRSxhQUFXLENBQUMsZ0JBQWdCLENBQUMsRUFBN0UsQ0FBNkUsQ0FBQyxDQUFDO1FBQ2hHLENBQUM7SUFDTCxDQUFDO0lBRUQsNENBQVcsR0FBWCxVQUFZLFFBQWdCLEVBQUUsVUFBa0IsRUFBRSxnQkFBcUM7UUFDbkYsSUFBSSxDQUFDLFlBQVksR0FBRyxRQUFRLENBQUM7UUFDN0IsSUFBSSxDQUFDLGNBQWMsR0FBRyxVQUFVLENBQUM7UUFDakMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztRQUc3QixJQUFJLENBQUMsZUFBZSxHQUFHLGdCQUFnQixDQUFDLE1BQU0sQ0FBQztRQUMvQyxJQUFJLENBQUMsWUFBWSxHQUFHLGdCQUFnQixDQUFDLEdBQUcsQ0FBQztRQUN6QyxJQUFJLENBQUMsWUFBWSxHQUFHLGdCQUFnQixDQUFDLEdBQUcsQ0FBQztRQUN6QyxJQUFJLENBQUMsWUFBWSxHQUFHLGdCQUFnQixDQUFDLEdBQUcsQ0FBQztRQUN6QyxJQUFJLENBQUMsWUFBWSxHQUFHLGdCQUFnQixDQUFDLEdBQUcsQ0FBQztRQUN6QyxJQUFJLENBQUMsWUFBWSxHQUFHLGdCQUFnQixDQUFDLEdBQUcsQ0FBQztRQUN6QyxJQUFJLENBQUMsWUFBWSxHQUFHLGdCQUFnQixDQUFDLEdBQUcsQ0FBQztRQUN6QyxJQUFJLENBQUMsWUFBWSxHQUFHLGdCQUFnQixDQUFDLEdBQUcsQ0FBQztJQUM3QyxDQUFDO0lBRUQsMkNBQVUsR0FBVixVQUFXLGdCQUFxQyxFQUFFLEtBQStCO1FBRTdFLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ2pCLElBQUksS0FBSyxHQUFHLEtBQUssQ0FBQyxvQkFBb0IsQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUNqRSxLQUFLLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNoRCxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFFSixnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQztZQUMvQyxnQkFBZ0IsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztZQUN6QyxnQkFBZ0IsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztZQUN6QyxnQkFBZ0IsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztZQUN6QyxnQkFBZ0IsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztZQUN6QyxnQkFBZ0IsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztZQUN6QyxnQkFBZ0IsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztZQUN6QyxnQkFBZ0IsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztRQUM3QyxDQUFDO1FBQ0QsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7SUFDbEMsQ0FBQztJQUVELHlDQUFRLEdBQVIsVUFBUyxRQUFnQixFQUFFLFVBQWtCO1FBQ3pDLEVBQUUsQ0FBQyxDQUFDLFFBQVEsS0FBSyxJQUFJLENBQUMsWUFBWSxJQUFJLFVBQVUsS0FBSyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztZQUN2RSxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFDRCxNQUFNLENBQUMsS0FBSyxDQUFDO0lBQ2pCLENBQUM7SUFFRCx3Q0FBTyxHQUFQO1FBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDMUIsQ0FBQztJQUVELCtDQUFjLEdBQWQ7UUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDO0lBQ2pDLENBQUM7SUFFRCw4Q0FBYSxHQUFiLFVBQWMsT0FBNEI7UUFDdEMsTUFBTSxDQUFDLENBQUMsT0FBTyxDQUFDLE1BQU0sS0FBSyxFQUFFLElBQUksT0FBTyxDQUFDLE1BQU0sS0FBSyxJQUFJLElBQUksT0FBTyxDQUFDLE1BQU0sS0FBSyxTQUFTLENBQUMsQ0FBQztJQUM5RixDQUFDO0lBRUQsNENBQVcsR0FBWCxVQUFZLE1BQWM7UUFFdEIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssS0FBSyxTQUFTLElBQUksSUFBSSxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUM7WUFDaEQsTUFBTSxDQUFDLEVBQUUsQ0FBQztRQUNkLElBQUksYUFBYSxHQUFjLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLEVBQUUsS0FBSyxNQUFNLEVBQWYsQ0FBZSxDQUFDLENBQUM7UUFDdkUsTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxHQUFHLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDO0lBQ2pFLENBQUM7SUFFRCx1REFBc0IsR0FBdEI7UUFFSSxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztRQUN6QixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztRQUMzQixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztRQUM1QixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztRQUN6QixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztRQUN6QixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztRQUN6QixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztRQUN6QixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztRQUN6QixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztRQUN6QixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztRQUN6QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztRQUN2QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO0lBQ2xDLENBQUM7SUFFRCxpREFBZ0IsR0FBaEIsVUFBaUIsR0FBVztRQUN4QixNQUFNLENBQUMsNEJBQVksQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDcEUsQ0FBQztJQUVELGtEQUFpQixHQUFqQixVQUFrQixHQUFXO1FBQ3pCLE1BQU0sQ0FBQyw0QkFBWSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ3JFLENBQUM7SUE1TEw7UUFBQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxvQkFBb0I7WUFDOUIsV0FBVyxFQUFFLGdDQUFnQztZQUM3QyxTQUFTLEVBQUUsQ0FBQywrQkFBK0IsQ0FBQztTQUMvQyxDQUFDOzs4QkFBQTtJQXdMRiw2QkFBQztBQUFELENBdkxBLEFBdUxDLElBQUE7QUF2TFksOEJBQXNCLHlCQXVMbEMsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9ib29raW5ncy9ib29raW5nc1RhYmxlL2Jvb2tpbmdzVGFibGUuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmltcG9ydCB7IEJvb2tpbmdzU2VydmljZSB9IGZyb20gJy4uL2Jvb2tpbmdzU2VydmljZS9ib29raW5ncy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUm9vbXNTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vcm9vbXMvcm9vbXNTZXJ2aWNlL3Jvb21zLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDZW50cmVVc2VyU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uL2NvcmUvaW5kZXgnO1xyXG5pbXBvcnQgeyBUb2FzdGVyU2VydmljZSB9IGZyb20gJ2FuZ3VsYXIyLXRvYXN0ZXInO1xyXG5pbXBvcnQgeyBTcGlubmluZ1RvYXN0IH0gZnJvbSAnLi4vLi4vLi4vLi4vc2hhcmVkL2luZGV4JztcclxuaW1wb3J0IHsgQ2hpbGRQZXJtYW5lbnRCb29raW5nRHRvLCBQZXJtYW5lbnRCb29raW5nRHRvLCBSb29tRHRvIH0gZnJvbSAnLi4vLi4vLi4vLi4vYXBpL2luZGV4JztcclxuaW1wb3J0IHsgQWdlQ29udmVydGVyIH0gZnJvbSAnLi4vLi4vcm9vbXMvY29tcG9uZW50cy9hZ2UtY29udmVydGVyJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHNlbGVjdG9yOiAnYm9va2luZ3MtdGFibGUtY21wJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9ib29raW5nc1RhYmxlLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2Jvb2tpbmdzVGFibGUuY29tcG9uZW50LmNzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBCb29raW5nc1RhYmxlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBwcml2YXRlIGNlbnRyZUNoaWxkcmVuOiBDaGlsZFBlcm1hbmVudEJvb2tpbmdEdG9bXTtcclxuICAgIHByaXZhdGUgZWRpdENoaWxkUmVmPzogbnVtYmVyO1xyXG4gICAgcHJpdmF0ZSBlZGl0Qm9va2luZ1JlZj86IG51bWJlcjtcclxuICAgIHByaXZhdGUgcm9vbXM6IFJvb21EdG9bXTtcclxuICAgIC8vIFN0b3JlcyB2YWx1ZXMgYmVmb3JlIGVkaXQgc28gd2UgY2FuIHJvbGxiYWNrIG9uIGVycm9yXHJcbiAgICBwcml2YXRlIHByZUVkaXRlZFJvb21JZDogc3RyaW5nO1xyXG4gICAgcHJpdmF0ZSBwcmVFZGl0ZWRNb246IGJvb2xlYW47XHJcbiAgICBwcml2YXRlIHByZUVkaXRlZFR1ZTogYm9vbGVhbjtcclxuICAgIHByaXZhdGUgcHJlRWRpdGVkV2VkOiBib29sZWFuO1xyXG4gICAgcHJpdmF0ZSBwcmVFZGl0ZWRUaHU6IGJvb2xlYW47XHJcbiAgICBwcml2YXRlIHByZUVkaXRlZEZyaTogYm9vbGVhbjtcclxuICAgIHByaXZhdGUgcHJlRWRpdGVkU2F0OiBib29sZWFuO1xyXG4gICAgcHJpdmF0ZSBwcmVFZGl0ZWRTdW46IGJvb2xlYW47XHJcblxyXG4gICAgcHJpdmF0ZSBpc0FkZE1vZGU6IGJvb2xlYW47ICAgLy8gdXNlZCB0byBkZXRlcm1pbmUgaWYgd2UncmUgYWRkZGluZyBhIHJvd1xyXG4gICAgcHJpdmF0ZSBpc0Rpc2FibGVBZGRFZGl0OiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgcHJpdmF0ZSBkaXNhYmxlU2F2ZTogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX2Jvb2tpbmdzU2VydmljZTogQm9va2luZ3NTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgX3Jvb21zU2VydmljZTogUm9vbXNTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgX3VzZXJTZXJ2aWNlOiBDZW50cmVVc2VyU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIF90b2FzdGVyU2VydmljZTogVG9hc3RlclNlcnZpY2UpIHsgfVxyXG5cclxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuaXNBZGRNb2RlID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5fYm9va2luZ3NTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5HZXRDZW50cmVQZXJtYW5lbnRCb29raW5ncyQodGhpcy5fdXNlclNlcnZpY2UuU2VsZWN0ZWRDZW50cmUuaWQpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgIHJlcyA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNlbnRyZUNoaWxkcmVuID0gcmVzO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgdGhpcy5fcm9vbXNTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5HZXRSb29tcyQodGhpcy5fdXNlclNlcnZpY2UuU2VsZWN0ZWRDZW50cmUuaWQpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgIHJlcyA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJvb21zID0gcmVzO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBhZGRCb29raW5nKGNoaWxkOiBDaGlsZFBlcm1hbmVudEJvb2tpbmdEdG8sIGNoaWxkSWR4OiBudW1iZXIsIGJvb2tpbmdJZHg6IG51bWJlcikge1xyXG4gICAgICAgIC8vIFNldCBhZGQgbW9kZSBhbmQgcm93XHJcbiAgICAgICAgdGhpcy5pc0FkZE1vZGUgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuZWRpdENoaWxkUmVmID0gY2hpbGRJZHg7XHJcbiAgICAgICAgdGhpcy5pc0Rpc2FibGVBZGRFZGl0ID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgLy8gTmV3IHVwIGJvb2tpbmdcclxuICAgICAgICB2YXIgbmV3Qm9va2luZyA9IG5ldyBQZXJtYW5lbnRCb29raW5nRHRvKCk7XHJcbiAgICAgICAgbmV3Qm9va2luZy5jZW50cmVJZCA9IGNoaWxkLmNlbnRyZUlkO1xyXG4gICAgICAgIG5ld0Jvb2tpbmcua2lkSWQgPSBjaGlsZC5raWRJZDtcclxuXHJcbiAgICAgICAgLy8gQWRkIHRvIGNoaWxkcyBib29raW5ncyBsaXN0XHJcbiAgICAgICAgY2hpbGQucGVybWFuZW50Qm9va2luZ0xpc3QucHVzaChuZXdCb29raW5nKTtcclxuXHJcbiAgICAgICAgLy8gc2V0IHRoZSBib29raW5nIGluZGV4IHRvIHRoZSBsYXN0IGJvb2tpbmcgaW4gdGhlIGJvb2tpbmcgYXJyYXlcclxuICAgICAgICB0aGlzLmVkaXRCb29raW5nUmVmID0gY2hpbGQucGVybWFuZW50Qm9va2luZ0xpc3QubGVuZ3RoIC0gMTtcclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGVCb29raW5nKGJvb2tpbmc6IFBlcm1hbmVudEJvb2tpbmdEdG8sIGNoaWxkOiBDaGlsZFBlcm1hbmVudEJvb2tpbmdEdG8pIHtcclxuICAgICAgICBsZXQgc2F2aW5nVG9hc3QgPSB0aGlzLl90b2FzdGVyU2VydmljZS5wb3AobmV3IFNwaW5uaW5nVG9hc3QoKSk7XHJcbiAgICAgICAgdGhpcy5kaXNhYmxlU2F2ZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5fYm9va2luZ3NTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5VcGRhdGVQZXJtYW5lbnRCb29raW5nJChib29raW5nKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICByZXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgLy8gcmVzZXQgZWRpdCBpbmRleHMgYW5kIHByZUVkaXRlZEJvb2tpbmdcclxuICAgICAgICAgICAgICAgIHRoaXMucmVzZXRFZGl0aW5nUHJvcGVydGllcygpO1xyXG4gICAgICAgICAgICAgICAgLy8gdXBkYXRlIHBlcm1hbmVudCBib29raW5nIGFycmF5IGluIGNhc2UgYXBpIHNlcnZpY2UgaGFzIG1lcmdlZCBib29raW5ncy5cclxuICAgICAgICAgICAgICAgIHRoaXMuX2Jvb2tpbmdzU2VydmljZS5HZXRQZXJtYW5lbnRCb29raW5ncyQoY2hpbGQuY2VudHJlSWQsIGNoaWxkLmtpZElkKVxyXG4gICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAgICAgcmVzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2hpbGQucGVybWFuZW50Qm9va2luZ0xpc3QgPSByZXM7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcihzYXZpbmdUb2FzdC50b2FzdElkLCBzYXZpbmdUb2FzdC50b2FzdENvbnRhaW5lcklkKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLnBvcEFzeW5jKCdzdWNjZXNzJywgJ0Jvb2tpbmcgdXBkYXRlZCcsICcnKTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgKGVycikgPT4gdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoc2F2aW5nVG9hc3QudG9hc3RJZCwgc2F2aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCkpO1xyXG4gICAgICAgIHRoaXMuZGlzYWJsZVNhdmUgPSBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBkZWxldGVCb29raW5nKGJvb2tpbmc6IFBlcm1hbmVudEJvb2tpbmdEdG8sIGNoaWxkOiBDaGlsZFBlcm1hbmVudEJvb2tpbmdEdG8pIHtcclxuICAgICAgICBpZiAod2luZG93LmNvbmZpcm0oJ0FyZSB5b3Ugc3VyZSB5b3Ugd2FudCB0byByZW1vdmUgdGhlIGJvb2tpbmc/JykpIHtcclxuICAgICAgICAgICAgbGV0IHNhdmluZ1RvYXN0ID0gdGhpcy5fdG9hc3RlclNlcnZpY2UucG9wKG5ldyBTcGlubmluZ1RvYXN0KCkpO1xyXG4gICAgICAgICAgICB0aGlzLl9ib29raW5nc1NlcnZpY2VcclxuICAgICAgICAgICAgICAgIC5EZWxldGVQZXJtYW5lbnRCb29raW5nJChib29raW5nLmNlbnRyZUlkLCBib29raW5nLmlkKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciBpbmRleCA9IGNoaWxkLnBlcm1hbmVudEJvb2tpbmdMaXN0LmluZGV4T2YoYm9va2luZyk7XHJcbiAgICAgICAgICAgICAgICAgICAgY2hpbGQucGVybWFuZW50Qm9va2luZ0xpc3Quc3BsaWNlKGluZGV4LCAxKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcihzYXZpbmdUb2FzdC50b2FzdElkLCBzYXZpbmdUb2FzdC50b2FzdENvbnRhaW5lcklkKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5wb3BBc3luYygnc3VjY2VzcycsICdCb29raW5nIHJlbW92ZWQnLCAnJyk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgKGVycikgPT4gdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoc2F2aW5nVG9hc3QudG9hc3RJZCwgc2F2aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCkpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBlZGl0Qm9va2luZyhjaGlsZElkeDogbnVtYmVyLCBib29raW5nSWR4OiBudW1iZXIsIHBlcm1hbmVudEJvb2tpbmc6IFBlcm1hbmVudEJvb2tpbmdEdG8pIHtcclxuICAgICAgICB0aGlzLmVkaXRDaGlsZFJlZiA9IGNoaWxkSWR4O1xyXG4gICAgICAgIHRoaXMuZWRpdEJvb2tpbmdSZWYgPSBib29raW5nSWR4O1xyXG4gICAgICAgIHRoaXMuaXNEaXNhYmxlQWRkRWRpdCA9IHRydWU7XHJcblxyXG4gICAgICAgIC8vIHN0b3JlIHByZUVkaXRlZEJvb2tpbmdcclxuICAgICAgICB0aGlzLnByZUVkaXRlZFJvb21JZCA9IHBlcm1hbmVudEJvb2tpbmcucm9vbUlkO1xyXG4gICAgICAgIHRoaXMucHJlRWRpdGVkTW9uID0gcGVybWFuZW50Qm9va2luZy5tb247XHJcbiAgICAgICAgdGhpcy5wcmVFZGl0ZWRUdWUgPSBwZXJtYW5lbnRCb29raW5nLnR1ZTtcclxuICAgICAgICB0aGlzLnByZUVkaXRlZFdlZCA9IHBlcm1hbmVudEJvb2tpbmcud2VkO1xyXG4gICAgICAgIHRoaXMucHJlRWRpdGVkVGh1ID0gcGVybWFuZW50Qm9va2luZy50aHU7XHJcbiAgICAgICAgdGhpcy5wcmVFZGl0ZWRGcmkgPSBwZXJtYW5lbnRCb29raW5nLmZyaTtcclxuICAgICAgICB0aGlzLnByZUVkaXRlZFNhdCA9IHBlcm1hbmVudEJvb2tpbmcuc2F0O1xyXG4gICAgICAgIHRoaXMucHJlRWRpdGVkU3VuID0gcGVybWFuZW50Qm9va2luZy5zdW47XHJcbiAgICB9XHJcblxyXG4gICAgY2FuY2VsRWRpdChwZXJtYW5lbnRCb29raW5nOiBQZXJtYW5lbnRCb29raW5nRHRvLCBjaGlsZDogQ2hpbGRQZXJtYW5lbnRCb29raW5nRHRvKSB7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLmlzQWRkTW9kZSkge1xyXG4gICAgICAgICAgICB2YXIgaW5kZXggPSBjaGlsZC5wZXJtYW5lbnRCb29raW5nTGlzdC5pbmRleE9mKHBlcm1hbmVudEJvb2tpbmcpO1xyXG4gICAgICAgICAgICBjaGlsZC5wZXJtYW5lbnRCb29raW5nTGlzdC5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIC8vIEVkaXRpbmcgZXhpc3RpbmcgYm9va2luZywgcm9sbGJhY2sgcGVybWFuZW50Qm9va2luZ1xyXG4gICAgICAgICAgICBwZXJtYW5lbnRCb29raW5nLnJvb21JZCA9IHRoaXMucHJlRWRpdGVkUm9vbUlkO1xyXG4gICAgICAgICAgICBwZXJtYW5lbnRCb29raW5nLm1vbiA9IHRoaXMucHJlRWRpdGVkTW9uO1xyXG4gICAgICAgICAgICBwZXJtYW5lbnRCb29raW5nLnR1ZSA9IHRoaXMucHJlRWRpdGVkVHVlO1xyXG4gICAgICAgICAgICBwZXJtYW5lbnRCb29raW5nLndlZCA9IHRoaXMucHJlRWRpdGVkV2VkO1xyXG4gICAgICAgICAgICBwZXJtYW5lbnRCb29raW5nLnRodSA9IHRoaXMucHJlRWRpdGVkVGh1O1xyXG4gICAgICAgICAgICBwZXJtYW5lbnRCb29raW5nLmZyaSA9IHRoaXMucHJlRWRpdGVkRnJpO1xyXG4gICAgICAgICAgICBwZXJtYW5lbnRCb29raW5nLnNhdCA9IHRoaXMucHJlRWRpdGVkU2F0O1xyXG4gICAgICAgICAgICBwZXJtYW5lbnRCb29raW5nLnN1biA9IHRoaXMucHJlRWRpdGVkU3VuO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnJlc2V0RWRpdGluZ1Byb3BlcnRpZXMoKTtcclxuICAgIH1cclxuXHJcbiAgICBlZGl0TW9kZShjaGlsZElkeDogbnVtYmVyLCBib29raW5nSWR4OiBudW1iZXIpOiBib29sZWFuIHtcclxuICAgICAgICBpZiAoY2hpbGRJZHggPT09IHRoaXMuZWRpdENoaWxkUmVmICYmIGJvb2tpbmdJZHggPT09IHRoaXMuZWRpdEJvb2tpbmdSZWYpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBhZGRNb2RlKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmlzQWRkTW9kZTtcclxuICAgIH1cclxuXHJcbiAgICBkaXNhYmxlQWRkRWRpdCgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pc0Rpc2FibGVBZGRFZGl0O1xyXG4gICAgfVxyXG5cclxuICAgIGlzSW52YWxpZFJvb20oYm9va2luZzogUGVybWFuZW50Qm9va2luZ0R0byk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiAoYm9va2luZy5yb29tSWQgPT09ICcnIHx8IGJvb2tpbmcucm9vbUlkID09PSBudWxsIHx8IGJvb2tpbmcucm9vbUlkID09PSB1bmRlZmluZWQpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFJvb21OYW1lKHJvb21JZDogc3RyaW5nKTogc3RyaW5nIHtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMucm9vbXMgPT09IHVuZGVmaW5lZCB8fCB0aGlzLnJvb21zID09PSBudWxsKVxyXG4gICAgICAgICAgICByZXR1cm4gJyc7XHJcbiAgICAgICAgbGV0IGZpbHRlcmVkUm9vbXM6IFJvb21EdG9bXSA9IHRoaXMucm9vbXMuZmlsdGVyKHggPT4geC5pZCA9PT0gcm9vbUlkKTtcclxuICAgICAgICByZXR1cm4gZmlsdGVyZWRSb29tcy5sZW5ndGggPiAwID8gZmlsdGVyZWRSb29tc1swXS5uYW1lIDogJyc7XHJcbiAgICB9XHJcblxyXG4gICAgcmVzZXRFZGl0aW5nUHJvcGVydGllcygpIHtcclxuICAgICAgICAvLyBSZXNldCBlZGl0aW5nIHByb3BlcnRpZXNcclxuICAgICAgICB0aGlzLmVkaXRDaGlsZFJlZiA9IG51bGw7XHJcbiAgICAgICAgdGhpcy5lZGl0Qm9va2luZ1JlZiA9IG51bGw7XHJcbiAgICAgICAgdGhpcy5wcmVFZGl0ZWRSb29tSWQgPSBudWxsO1xyXG4gICAgICAgIHRoaXMucHJlRWRpdGVkTW9uID0gbnVsbDtcclxuICAgICAgICB0aGlzLnByZUVkaXRlZFR1ZSA9IG51bGw7XHJcbiAgICAgICAgdGhpcy5wcmVFZGl0ZWRXZWQgPSBudWxsO1xyXG4gICAgICAgIHRoaXMucHJlRWRpdGVkVGh1ID0gbnVsbDtcclxuICAgICAgICB0aGlzLnByZUVkaXRlZEZyaSA9IG51bGw7XHJcbiAgICAgICAgdGhpcy5wcmVFZGl0ZWRTYXQgPSBudWxsO1xyXG4gICAgICAgIHRoaXMucHJlRWRpdGVkU3VuID0gbnVsbDtcclxuICAgICAgICB0aGlzLmlzQWRkTW9kZSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuaXNEaXNhYmxlQWRkRWRpdCA9IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGdldENoaWxkQWdlWWVhcnMoYWdlOiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBBZ2VDb252ZXJ0ZXIuZ2V0WWVhckZyYWN0aW9uKHBhcnNlRmxvYXQoYWdlKSkudG9TdHJpbmcoKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDaGlsZEFnZU1vbnRocyhhZ2U6IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIEFnZUNvbnZlcnRlci5nZXRNb250aEZyYWN0aW9uKHBhcnNlRmxvYXQoYWdlKSkudG9TdHJpbmcoKTtcclxuICAgIH1cclxufVxyXG4iXX0=
