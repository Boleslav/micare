"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../../api/index');
var BookingsService = (function () {
    function BookingsService(_apiService) {
        this._apiService = _apiService;
    }
    BookingsService.prototype.GetCentrePermanentBookings$ = function (centreId) {
        return this._apiService.centre_CentreChildrenBookings(centreId);
    };
    BookingsService.prototype.UpdatePermanentBooking$ = function (booking) {
        return this._apiService.child_AddOrUpdatePermantBooking(booking);
    };
    BookingsService.prototype.DeletePermanentBooking$ = function (centreId, bookingId) {
        return this._apiService.child_DeletePermanentBooking(centreId, bookingId);
    };
    BookingsService.prototype.GetPermanentBookings$ = function (centreId, kidId) {
        return this._apiService.child_GetPermanentBookings(centreId, kidId);
    };
    BookingsService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.ApiClientService])
    ], BookingsService);
    return BookingsService;
}());
exports.BookingsService = BookingsService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2Jvb2tpbmdzL2Jvb2tpbmdzU2VydmljZS9ib29raW5ncy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBMkIsZUFBZSxDQUFDLENBQUE7QUFHM0Msc0JBQWdGLHVCQUF1QixDQUFDLENBQUE7QUFHeEc7SUFFSSx5QkFBb0IsV0FBNkI7UUFBN0IsZ0JBQVcsR0FBWCxXQUFXLENBQWtCO0lBQ2pELENBQUM7SUFFTSxxREFBMkIsR0FBbEMsVUFBbUMsUUFBZ0I7UUFDL0MsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsNkJBQTZCLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDcEUsQ0FBQztJQUVNLGlEQUF1QixHQUE5QixVQUErQixPQUE0QjtRQUN2RCxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQywrQkFBK0IsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUNyRSxDQUFDO0lBRU0saURBQXVCLEdBQTlCLFVBQStCLFFBQWdCLEVBQUUsU0FBaUI7UUFDOUQsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsNEJBQTRCLENBQUMsUUFBUSxFQUFFLFNBQVMsQ0FBQyxDQUFDO0lBQzlFLENBQUM7SUFFTSwrQ0FBcUIsR0FBNUIsVUFBNkIsUUFBZ0IsRUFBRSxLQUFhO1FBQ3hELE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLDBCQUEwQixDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUN4RSxDQUFDO0lBcEJMO1FBQUMsaUJBQVUsRUFBRTs7dUJBQUE7SUFxQmIsc0JBQUM7QUFBRCxDQXBCQSxBQW9CQyxJQUFBO0FBcEJZLHVCQUFlLGtCQW9CM0IsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9ib29raW5ncy9ib29raW5nc1NlcnZpY2UvYm9va2luZ3Muc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgQXBpQ2xpZW50U2VydmljZSwgQ2hpbGRQZXJtYW5lbnRCb29raW5nRHRvLCBQZXJtYW5lbnRCb29raW5nRHRvIH0gZnJvbSAnLi4vLi4vLi4vLi4vYXBpL2luZGV4JztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIEJvb2tpbmdzU2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfYXBpU2VydmljZTogQXBpQ2xpZW50U2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBHZXRDZW50cmVQZXJtYW5lbnRCb29raW5ncyQoY2VudHJlSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8Q2hpbGRQZXJtYW5lbnRCb29raW5nRHRvW10+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fYXBpU2VydmljZS5jZW50cmVfQ2VudHJlQ2hpbGRyZW5Cb29raW5ncyhjZW50cmVJZCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIFVwZGF0ZVBlcm1hbmVudEJvb2tpbmckKGJvb2tpbmc6IFBlcm1hbmVudEJvb2tpbmdEdG8pOiBPYnNlcnZhYmxlPFBlcm1hbmVudEJvb2tpbmdEdG8+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fYXBpU2VydmljZS5jaGlsZF9BZGRPclVwZGF0ZVBlcm1hbnRCb29raW5nKGJvb2tpbmcpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBEZWxldGVQZXJtYW5lbnRCb29raW5nJChjZW50cmVJZDogc3RyaW5nLCBib29raW5nSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8dm9pZD4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9hcGlTZXJ2aWNlLmNoaWxkX0RlbGV0ZVBlcm1hbmVudEJvb2tpbmcoY2VudHJlSWQsIGJvb2tpbmdJZCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIEdldFBlcm1hbmVudEJvb2tpbmdzJChjZW50cmVJZDogc3RyaW5nLCBraWRJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxQZXJtYW5lbnRCb29raW5nRHRvW10+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fYXBpU2VydmljZS5jaGlsZF9HZXRQZXJtYW5lbnRCb29raW5ncyhjZW50cmVJZCwga2lkSWQpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
