"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../shared/index');
var index_2 = require('./index');
var BookingsModule = (function () {
    function BookingsModule() {
    }
    BookingsModule = __decorate([
        core_1.NgModule({
            imports: [
                index_1.SharedModule
            ],
            declarations: [index_2.BookingsComponent, index_2.BookingsTableComponent],
            providers: [index_2.BookingsService],
            exports: [index_2.BookingsComponent, index_2.BookingsTableComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], BookingsModule);
    return BookingsModule;
}());
exports.BookingsModule = BookingsModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2Jvb2tpbmdzL2Jvb2tpbmdzLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQXlCLGVBQWUsQ0FBQyxDQUFBO0FBRXpDLHNCQUE2Qix1QkFBdUIsQ0FBQyxDQUFBO0FBQ3JELHNCQUEyRSxTQUFTLENBQUMsQ0FBQTtBQVdyRjtJQUFBO0lBQThCLENBQUM7SUFUL0I7UUFBQyxlQUFRLENBQUM7WUFDTixPQUFPLEVBQUU7Z0JBQ0wsb0JBQVk7YUFDZjtZQUNELFlBQVksRUFBRSxDQUFDLHlCQUFpQixFQUFFLDhCQUFzQixDQUFDO1lBQ3pELFNBQVMsRUFBRSxDQUFDLHVCQUFlLENBQUM7WUFDNUIsT0FBTyxFQUFFLENBQUMseUJBQWlCLEVBQUUsOEJBQXNCLENBQUM7U0FDdkQsQ0FBQzs7c0JBQUE7SUFFNEIscUJBQUM7QUFBRCxDQUE5QixBQUErQixJQUFBO0FBQWxCLHNCQUFjLGlCQUFJLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL2NlbnRyZS1wb3J0YWwvYm9va2luZ3MvYm9va2luZ3MubW9kdWxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmltcG9ydCB7IFNoYXJlZE1vZHVsZSB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9pbmRleCc7XHJcbmltcG9ydCB7IEJvb2tpbmdzQ29tcG9uZW50LCBCb29raW5nc1RhYmxlQ29tcG9uZW50LCBCb29raW5nc1NlcnZpY2UgfSBmcm9tICcuL2luZGV4JztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiBbXHJcbiAgICAgICAgU2hhcmVkTW9kdWxlXHJcbiAgICBdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBbQm9va2luZ3NDb21wb25lbnQsIEJvb2tpbmdzVGFibGVDb21wb25lbnRdLFxyXG4gICAgcHJvdmlkZXJzOiBbQm9va2luZ3NTZXJ2aWNlXSxcclxuICAgIGV4cG9ydHM6IFtCb29raW5nc0NvbXBvbmVudCwgQm9va2luZ3NUYWJsZUNvbXBvbmVudF1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBCb29raW5nc01vZHVsZSB7IH1cclxuIl19
