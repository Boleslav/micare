"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var SwapsComponent = (function () {
    function SwapsComponent() {
    }
    SwapsComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'swaps-cmp',
            templateUrl: './swaps.component.html'
        }), 
        __metadata('design:paramtypes', [])
    ], SwapsComponent);
    return SwapsComponent;
}());
exports.SwapsComponent = SwapsComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3N3YXBzL3N3YXBzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQXdCLGVBQWUsQ0FBQyxDQUFBO0FBUXhDO0lBQUE7SUFBNkIsQ0FBQztJQU45QjtRQUFDLGdCQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDaEIsUUFBUSxFQUFFLFdBQVc7WUFDckIsV0FBVyxFQUFFLHdCQUF3QjtTQUN4QyxDQUFDOztzQkFBQTtJQUUyQixxQkFBQztBQUFELENBQTdCLEFBQThCLElBQUE7QUFBakIsc0JBQWMsaUJBQUcsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9zd2Fwcy9zd2Fwcy5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuXHRtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgc2VsZWN0b3I6ICdzd2Fwcy1jbXAnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL3N3YXBzLmNvbXBvbmVudC5odG1sJ1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFN3YXBzQ29tcG9uZW50IHt9XHJcbiJdfQ==
