"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../../api/index');
var SwapsService = (function () {
    function SwapsService(_apiService) {
        this._apiService = _apiService;
    }
    SwapsService.prototype.GetSwapDays$ = function (centreId, dateFrom) {
        return this._apiService.swap_GetSwapDays(centreId, dateFrom);
    };
    SwapsService.prototype.RemoveSwapDay$ = function (miSwapId, centreId) {
        return this._apiService.swap_RemoveSwapDay(miSwapId, centreId);
    };
    SwapsService.prototype.ReverseSwapDay$ = function (miSwapId, centreId) {
        return this._apiService.swap_ReverseSwapDay(miSwapId, centreId);
    };
    SwapsService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.ApiClientService])
    ], SwapsService);
    return SwapsService;
}());
exports.SwapsService = SwapsService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3N3YXBzL3N3YXBzU2VydmljZS9zd2Fwcy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBMkIsZUFBZSxDQUFDLENBQUE7QUFHM0Msc0JBQTZDLHVCQUF1QixDQUFDLENBQUE7QUFHckU7SUFFSSxzQkFBb0IsV0FBNkI7UUFBN0IsZ0JBQVcsR0FBWCxXQUFXLENBQWtCO0lBRWpELENBQUM7SUFFTSxtQ0FBWSxHQUFuQixVQUFvQixRQUFnQixFQUFFLFFBQWM7UUFFaEQsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQ2pFLENBQUM7SUFFTSxxQ0FBYyxHQUFyQixVQUFzQixRQUFnQixFQUFFLFFBQWdCO1FBQ3BELE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFDLFFBQVEsRUFBRSxRQUFRLENBQUMsQ0FBQztJQUNuRSxDQUFDO0lBRU0sc0NBQWUsR0FBdEIsVUFBdUIsUUFBZ0IsRUFBRSxRQUFnQjtRQUNyRCxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDcEUsQ0FBQztJQWxCTDtRQUFDLGlCQUFVLEVBQUU7O29CQUFBO0lBbUJiLG1CQUFDO0FBQUQsQ0FsQkEsQUFrQkMsSUFBQTtBQWxCWSxvQkFBWSxlQWtCeEIsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9zd2Fwcy9zd2Fwc1NlcnZpY2Uvc3dhcHMuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgQXBpQ2xpZW50U2VydmljZSwgU3dhcERheUR0byB9IGZyb20gJy4uLy4uLy4uLy4uL2FwaS9pbmRleCc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBTd2Fwc1NlcnZpY2Uge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX2FwaVNlcnZpY2U6IEFwaUNsaWVudFNlcnZpY2UpIHtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIEdldFN3YXBEYXlzJChjZW50cmVJZDogc3RyaW5nLCBkYXRlRnJvbTogRGF0ZSk6IE9ic2VydmFibGU8U3dhcERheUR0b1tdPiB7XHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLl9hcGlTZXJ2aWNlLnN3YXBfR2V0U3dhcERheXMoY2VudHJlSWQsIGRhdGVGcm9tKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgUmVtb3ZlU3dhcERheSQobWlTd2FwSWQ6IHN0cmluZywgY2VudHJlSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8U3dhcERheUR0bz4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9hcGlTZXJ2aWNlLnN3YXBfUmVtb3ZlU3dhcERheShtaVN3YXBJZCwgY2VudHJlSWQpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBSZXZlcnNlU3dhcERheSQobWlTd2FwSWQ6IHN0cmluZywgY2VudHJlSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8U3dhcERheUR0bz4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9hcGlTZXJ2aWNlLnN3YXBfUmV2ZXJzZVN3YXBEYXkobWlTd2FwSWQsIGNlbnRyZUlkKTtcclxuICAgIH1cclxufVxyXG4iXX0=
