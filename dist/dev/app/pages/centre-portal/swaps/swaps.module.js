"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../shared/index');
var index_2 = require('./index');
var SwapsModule = (function () {
    function SwapsModule() {
    }
    SwapsModule = __decorate([
        core_1.NgModule({
            imports: [
                index_1.SharedModule
            ],
            declarations: [index_2.SwapsComponent, index_2.SwapsTableComponent],
            providers: [index_2.SwapsService],
            exports: [index_2.SwapsComponent, index_2.SwapsTableComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], SwapsModule);
    return SwapsModule;
}());
exports.SwapsModule = SwapsModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3N3YXBzL3N3YXBzLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQXlCLGVBQWUsQ0FBQyxDQUFBO0FBQ3pDLHNCQUE2Qix1QkFBdUIsQ0FBQyxDQUFBO0FBRXJELHNCQUFrRSxTQUFTLENBQUMsQ0FBQTtBQVc1RTtJQUFBO0lBQTJCLENBQUM7SUFUNUI7UUFBQyxlQUFRLENBQUM7WUFDTixPQUFPLEVBQUU7Z0JBQ0wsb0JBQVk7YUFDZjtZQUNELFlBQVksRUFBRSxDQUFDLHNCQUFjLEVBQUUsMkJBQW1CLENBQUM7WUFDbkQsU0FBUyxFQUFFLENBQUMsb0JBQVksQ0FBQztZQUN6QixPQUFPLEVBQUUsQ0FBQyxzQkFBYyxFQUFFLDJCQUFtQixDQUFDO1NBQ2pELENBQUM7O21CQUFBO0lBRXlCLGtCQUFDO0FBQUQsQ0FBM0IsQUFBNEIsSUFBQTtBQUFmLG1CQUFXLGNBQUksQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9zd2Fwcy9zd2Fwcy5tb2R1bGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTaGFyZWRNb2R1bGUgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvaW5kZXgnO1xyXG5cclxuaW1wb3J0IHsgU3dhcHNDb21wb25lbnQsIFN3YXBzVGFibGVDb21wb25lbnQsIFN3YXBzU2VydmljZSB9IGZyb20gJy4vaW5kZXgnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBTaGFyZWRNb2R1bGVcclxuICAgIF0sXHJcbiAgICBkZWNsYXJhdGlvbnM6IFtTd2Fwc0NvbXBvbmVudCwgU3dhcHNUYWJsZUNvbXBvbmVudF0sXHJcbiAgICBwcm92aWRlcnM6IFtTd2Fwc1NlcnZpY2VdLFxyXG4gICAgZXhwb3J0czogW1N3YXBzQ29tcG9uZW50LCBTd2Fwc1RhYmxlQ29tcG9uZW50XVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFN3YXBzTW9kdWxlIHsgfVxyXG4iXX0=
