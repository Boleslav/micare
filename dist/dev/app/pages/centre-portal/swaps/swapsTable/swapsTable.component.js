"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var angular2_toaster_1 = require('angular2-toaster');
var swaps_service_1 = require('../swapsService/swaps.service');
var index_1 = require('../../../../core/index');
var index_2 = require('../../../../shared/index');
var SwapsTableComponent = (function () {
    function SwapsTableComponent(_swapsService, _userService, _toasterService) {
        this._swapsService = _swapsService;
        this._userService = _userService;
        this._toasterService = _toasterService;
        this.fromDate = new Date();
        this.toDate = new Date();
        this.disableButtons = false;
    }
    SwapsTableComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.toDate.setDate(this.fromDate.getDate() + 90);
        this._swapsService
            .GetSwapDays$(this._userService.SelectedCentre.id, this.fromDate)
            .subscribe(function (swapsRes) {
            _this.swaps = swapsRes;
        });
    };
    SwapsTableComponent.prototype.removeSwapDay = function (miSwapId) {
        var _this = this;
        if (window.confirm('The parent giving the day will receive an email advising them of the removal. ' +
            'Do you want to remove this swap? Press Ok to continue.')) {
            var savingToast_1 = this._toasterService.pop(new index_2.SpinningToast());
            this.disableButtons = true;
            this._swapsService
                .RemoveSwapDay$(miSwapId, this._userService.SelectedCentre.id)
                .subscribe(function (res) {
                _this._swapsService
                    .GetSwapDays$(_this._userService.SelectedCentre.id, _this.fromDate)
                    .subscribe(function (swapsRes) {
                    _this.swaps = swapsRes;
                });
                _this._toasterService.clear(savingToast_1.toastId, savingToast_1.toastContainerId);
                _this._toasterService.popAsync('success', 'Saved', '');
            }, function (err) { return _this._toasterService.clear(savingToast_1.toastId, savingToast_1.toastContainerId); });
            this.disableButtons = false;
        }
    };
    SwapsTableComponent.prototype.reverseSwapDay = function (miSwapId) {
        var _this = this;
        if (window.confirm('Reversing a day that was already given will result in a ' +
            'processing fee being deducted from the giving MiSwap account. Press Ok to continue.')) {
            var savingToast_2 = this._toasterService.pop(new index_2.SpinningToast());
            this.disableButtons = true;
            this._swapsService
                .ReverseSwapDay$(miSwapId, this._userService.SelectedCentre.id)
                .subscribe(function (res) {
                _this._swapsService
                    .GetSwapDays$(_this._userService.SelectedCentre.id, _this.fromDate)
                    .subscribe(function (swapsRes) {
                    _this.swaps = swapsRes;
                });
                _this._toasterService.clear(savingToast_2.toastId, savingToast_2.toastContainerId);
                _this._toasterService.popAsync('success', 'Saved', '');
            }, function (err) { return _this._toasterService.clear(savingToast_2.toastId, savingToast_2.toastContainerId); });
            this.disableButtons = false;
        }
    };
    SwapsTableComponent.prototype.previousDay = function () {
        this.disableButtons = true;
        var fromDate = new Date(this.fromDate);
        fromDate.setDate(fromDate.getDate() - 1);
        var toDate = new Date(this.toDate);
        toDate.setDate(toDate.getDate() - 1);
        this.refreshSwaps(fromDate, toDate);
        this.disableButtons = false;
    };
    SwapsTableComponent.prototype.nextDay = function () {
        this.disableButtons = true;
        var fromDate = new Date(this.fromDate);
        fromDate.setDate(fromDate.getDate() + 1);
        var toDate = new Date(this.toDate);
        toDate.setDate(toDate.getDate() + 1);
        this.refreshSwaps(fromDate, toDate);
        this.disableButtons = false;
    };
    SwapsTableComponent.prototype.refreshSwaps = function (fromDate, toDate) {
        var _this = this;
        this._swapsService
            .GetSwapDays$(this._userService.SelectedCentre.id, fromDate)
            .subscribe(function (swapsRes) {
            _this.swaps = swapsRes;
            _this.fromDate = fromDate;
            _this.toDate = toDate;
        });
    };
    SwapsTableComponent.prototype.swapStateString = function (swapState) {
        switch (swapState) {
            case 0:
                return 'To Give';
            case 1:
                return 'Given';
            case 2:
                return 'Processing';
            case 3:
                return 'Cancelled';
            case 4:
                return 'Expired';
            default:
                return 'Unknown';
        }
    };
    SwapsTableComponent.prototype.swapStateStyle = function (swapState) {
        switch (swapState) {
            case 0:
                return 'btn btn-sm btn-success btn-block';
            case 1:
                return 'btn btn-sm btn-primary btn-block';
            case 2:
                return 'btn btn-sm btn-warning btn-block';
            case 3:
                return 'btn btn-sm btn-danger btn-block';
            case 4:
                return 'btn btn-sm btn-danger btn-block';
            default:
                return 'btn btn-sm btn-seconday btn-block';
        }
    };
    SwapsTableComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'swaps-table-cmp',
            templateUrl: './swapsTable.component.html'
        }), 
        __metadata('design:paramtypes', [swaps_service_1.SwapsService, index_1.CentreUserService, angular2_toaster_1.ToasterService])
    ], SwapsTableComponent);
    return SwapsTableComponent;
}());
exports.SwapsTableComponent = SwapsTableComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3N3YXBzL3N3YXBzVGFibGUvc3dhcHNUYWJsZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUFrQyxlQUFlLENBQUMsQ0FBQTtBQUNsRCxpQ0FBK0Isa0JBQWtCLENBQUMsQ0FBQTtBQUNsRCw4QkFBNkIsK0JBQStCLENBQUMsQ0FBQTtBQUM3RCxzQkFBa0Msd0JBQXdCLENBQUMsQ0FBQTtBQUMzRCxzQkFBOEIsMEJBQTBCLENBQUMsQ0FBQTtBQVF6RDtJQVFJLDZCQUFvQixhQUEyQixFQUNuQyxZQUErQixFQUMvQixlQUErQjtRQUZ2QixrQkFBYSxHQUFiLGFBQWEsQ0FBYztRQUNuQyxpQkFBWSxHQUFaLFlBQVksQ0FBbUI7UUFDL0Isb0JBQWUsR0FBZixlQUFlLENBQWdCO1FBUm5DLGFBQVEsR0FBUyxJQUFJLElBQUksRUFBRSxDQUFDO1FBQzVCLFdBQU0sR0FBUyxJQUFJLElBQUksRUFBRSxDQUFDO1FBRzFCLG1CQUFjLEdBQVksS0FBSyxDQUFDO0lBS3hDLENBQUM7SUFFRCxzQ0FBUSxHQUFSO1FBQUEsaUJBT0M7UUFORyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO1FBQ2xELElBQUksQ0FBQyxhQUFhO2FBQ2IsWUFBWSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDO2FBQ2hFLFNBQVMsQ0FBQyxVQUFBLFFBQVE7WUFDZixLQUFJLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQztRQUMxQixDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFRCwyQ0FBYSxHQUFiLFVBQWMsUUFBZ0I7UUFBOUIsaUJBcUJDO1FBcEJHLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsZ0ZBQWdGO1lBQy9GLHdEQUF3RCxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzVELElBQUksYUFBVyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUkscUJBQWEsRUFBRSxDQUFDLENBQUM7WUFDaEUsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7WUFDM0IsSUFBSSxDQUFDLGFBQWE7aUJBQ2IsY0FBYyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxFQUFFLENBQUM7aUJBQzdELFNBQVMsQ0FDVixVQUFBLEdBQUc7Z0JBRUMsS0FBSSxDQUFDLGFBQWE7cUJBQ2IsWUFBWSxDQUFDLEtBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLEVBQUUsRUFBRSxLQUFJLENBQUMsUUFBUSxDQUFDO3FCQUNoRSxTQUFTLENBQUMsVUFBQSxRQUFRO29CQUNmLEtBQUksQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDO2dCQUMxQixDQUFDLENBQUMsQ0FBQztnQkFDUCxLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxhQUFXLENBQUMsT0FBTyxFQUFFLGFBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2dCQUM5RSxLQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsT0FBTyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQzFELENBQUMsRUFDRCxVQUFDLEdBQUcsSUFBSyxPQUFBLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLGFBQVcsQ0FBQyxPQUFPLEVBQUUsYUFBVyxDQUFDLGdCQUFnQixDQUFDLEVBQTdFLENBQTZFLENBQUMsQ0FBQztZQUM1RixJQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztRQUNoQyxDQUFDO0lBQ0wsQ0FBQztJQUVELDRDQUFjLEdBQWQsVUFBZSxRQUFnQjtRQUEvQixpQkFxQkM7UUFwQkcsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQywwREFBMEQ7WUFDekUscUZBQXFGLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDekYsSUFBSSxhQUFXLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsSUFBSSxxQkFBYSxFQUFFLENBQUMsQ0FBQztZQUNoRSxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztZQUMzQixJQUFJLENBQUMsYUFBYTtpQkFDYixlQUFlLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FBQztpQkFDOUQsU0FBUyxDQUNWLFVBQUEsR0FBRztnQkFFQyxLQUFJLENBQUMsYUFBYTtxQkFDYixZQUFZLENBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsRUFBRSxFQUFFLEtBQUksQ0FBQyxRQUFRLENBQUM7cUJBQ2hFLFNBQVMsQ0FBQyxVQUFBLFFBQVE7b0JBQ2YsS0FBSSxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUM7Z0JBQzFCLENBQUMsQ0FBQyxDQUFDO2dCQUNQLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLGFBQVcsQ0FBQyxPQUFPLEVBQUUsYUFBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0JBQzlFLEtBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLFNBQVMsRUFBRSxPQUFPLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDMUQsQ0FBQyxFQUNELFVBQUMsR0FBRyxJQUFLLE9BQUEsS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsYUFBVyxDQUFDLE9BQU8sRUFBRSxhQUFXLENBQUMsZ0JBQWdCLENBQUMsRUFBN0UsQ0FBNkUsQ0FBQyxDQUFDO1lBQzVGLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1FBQ2hDLENBQUM7SUFDTCxDQUFDO0lBRUQseUNBQVcsR0FBWDtRQUNJLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1FBQzNCLElBQUksUUFBUSxHQUFTLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUM3QyxRQUFRLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUV6QyxJQUFJLE1BQU0sR0FBUyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDekMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFFckMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDcEMsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7SUFDaEMsQ0FBQztJQUVELHFDQUFPLEdBQVA7UUFDSSxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztRQUMzQixJQUFJLFFBQVEsR0FBUyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDN0MsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFFekMsSUFBSSxNQUFNLEdBQVMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3pDLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBRXJDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO0lBQ2hDLENBQUM7SUFFRCwwQ0FBWSxHQUFaLFVBQWEsUUFBYyxFQUFFLE1BQVk7UUFBekMsaUJBUUM7UUFQRyxJQUFJLENBQUMsYUFBYTthQUNiLFlBQVksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxFQUFFLEVBQUUsUUFBUSxDQUFDO2FBQzNELFNBQVMsQ0FBQyxVQUFBLFFBQVE7WUFDZixLQUFJLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQztZQUN0QixLQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztZQUN6QixLQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUN6QixDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFRCw2Q0FBZSxHQUFmLFVBQWdCLFNBQWlCO1FBQzdCLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDaEIsS0FBSyxDQUFDO2dCQUNGLE1BQU0sQ0FBQyxTQUFTLENBQUM7WUFDckIsS0FBSyxDQUFDO2dCQUNGLE1BQU0sQ0FBQyxPQUFPLENBQUM7WUFDbkIsS0FBSyxDQUFDO2dCQUNGLE1BQU0sQ0FBQyxZQUFZLENBQUM7WUFDeEIsS0FBSyxDQUFDO2dCQUNGLE1BQU0sQ0FBQyxXQUFXLENBQUM7WUFDdkIsS0FBSyxDQUFDO2dCQUNGLE1BQU0sQ0FBQyxTQUFTLENBQUM7WUFDckI7Z0JBQ0ksTUFBTSxDQUFDLFNBQVMsQ0FBQztRQUN6QixDQUFDO0lBQ0wsQ0FBQztJQUVELDRDQUFjLEdBQWQsVUFBZSxTQUFpQjtRQUM1QixNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ2hCLEtBQUssQ0FBQztnQkFDRixNQUFNLENBQUMsa0NBQWtDLENBQUM7WUFDOUMsS0FBSyxDQUFDO2dCQUNGLE1BQU0sQ0FBQyxrQ0FBa0MsQ0FBQztZQUM5QyxLQUFLLENBQUM7Z0JBQ0YsTUFBTSxDQUFDLGtDQUFrQyxDQUFDO1lBQzlDLEtBQUssQ0FBQztnQkFDRixNQUFNLENBQUMsaUNBQWlDLENBQUM7WUFDN0MsS0FBSyxDQUFDO2dCQUNGLE1BQU0sQ0FBQyxpQ0FBaUMsQ0FBQztZQUM3QztnQkFDSSxNQUFNLENBQUMsbUNBQW1DLENBQUM7UUFDbkQsQ0FBQztJQUNMLENBQUM7SUEzSUw7UUFBQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxpQkFBaUI7WUFDM0IsV0FBVyxFQUFFLDZCQUE2QjtTQUM3QyxDQUFDOzsyQkFBQTtJQXdJRiwwQkFBQztBQUFELENBdklBLEFBdUlDLElBQUE7QUF2SVksMkJBQW1CLHNCQXVJL0IsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9zd2Fwcy9zd2Fwc1RhYmxlL3N3YXBzVGFibGUuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgVG9hc3RlclNlcnZpY2UgfSBmcm9tICdhbmd1bGFyMi10b2FzdGVyJztcclxuaW1wb3J0IHsgU3dhcHNTZXJ2aWNlIH0gZnJvbSAnLi4vc3dhcHNTZXJ2aWNlL3N3YXBzLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDZW50cmVVc2VyU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uL2NvcmUvaW5kZXgnO1xyXG5pbXBvcnQgeyBTcGlubmluZ1RvYXN0IH0gZnJvbSAnLi4vLi4vLi4vLi4vc2hhcmVkL2luZGV4JztcclxuaW1wb3J0IHsgU3dhcERheUR0byB9IGZyb20gJy4uLy4uLy4uLy4uL2FwaS9pbmRleCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICBzZWxlY3RvcjogJ3N3YXBzLXRhYmxlLWNtcCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vc3dhcHNUYWJsZS5jb21wb25lbnQuaHRtbCdcclxufSlcclxuZXhwb3J0IGNsYXNzIFN3YXBzVGFibGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIHByaXZhdGUgZnJvbURhdGU6IERhdGUgPSBuZXcgRGF0ZSgpO1xyXG4gICAgcHJpdmF0ZSB0b0RhdGU6IERhdGUgPSBuZXcgRGF0ZSgpO1xyXG5cclxuICAgIHByaXZhdGUgc3dhcHM6IFN3YXBEYXlEdG9bXTtcclxuICAgIHByaXZhdGUgZGlzYWJsZUJ1dHRvbnM6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9zd2Fwc1NlcnZpY2U6IFN3YXBzU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIF91c2VyU2VydmljZTogQ2VudHJlVXNlclNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBfdG9hc3RlclNlcnZpY2U6IFRvYXN0ZXJTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy50b0RhdGUuc2V0RGF0ZSh0aGlzLmZyb21EYXRlLmdldERhdGUoKSArIDkwKTtcclxuICAgICAgICB0aGlzLl9zd2Fwc1NlcnZpY2VcclxuICAgICAgICAgICAgLkdldFN3YXBEYXlzJCh0aGlzLl91c2VyU2VydmljZS5TZWxlY3RlZENlbnRyZS5pZCwgdGhpcy5mcm9tRGF0ZSlcclxuICAgICAgICAgICAgLnN1YnNjcmliZShzd2Fwc1JlcyA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnN3YXBzID0gc3dhcHNSZXM7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHJlbW92ZVN3YXBEYXkobWlTd2FwSWQ6IHN0cmluZykge1xyXG4gICAgICAgIGlmICh3aW5kb3cuY29uZmlybSgnVGhlIHBhcmVudCBnaXZpbmcgdGhlIGRheSB3aWxsIHJlY2VpdmUgYW4gZW1haWwgYWR2aXNpbmcgdGhlbSBvZiB0aGUgcmVtb3ZhbC4gJyArXHJcbiAgICAgICAgICAgICdEbyB5b3Ugd2FudCB0byByZW1vdmUgdGhpcyBzd2FwPyBQcmVzcyBPayB0byBjb250aW51ZS4nKSkge1xyXG4gICAgICAgICAgICBsZXQgc2F2aW5nVG9hc3QgPSB0aGlzLl90b2FzdGVyU2VydmljZS5wb3AobmV3IFNwaW5uaW5nVG9hc3QoKSk7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzYWJsZUJ1dHRvbnMgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLl9zd2Fwc1NlcnZpY2VcclxuICAgICAgICAgICAgICAgIC5SZW1vdmVTd2FwRGF5JChtaVN3YXBJZCwgdGhpcy5fdXNlclNlcnZpY2UuU2VsZWN0ZWRDZW50cmUuaWQpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgICAgcmVzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBnZXQgbGF0ZXN0IHN3YXAgZGF5c1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3N3YXBzU2VydmljZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuR2V0U3dhcERheXMkKHRoaXMuX3VzZXJTZXJ2aWNlLlNlbGVjdGVkQ2VudHJlLmlkLCB0aGlzLmZyb21EYXRlKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHN3YXBzUmVzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc3dhcHMgPSBzd2Fwc1JlcztcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoc2F2aW5nVG9hc3QudG9hc3RJZCwgc2F2aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UucG9wQXN5bmMoJ3N1Y2Nlc3MnLCAnU2F2ZWQnLCAnJyk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgKGVycikgPT4gdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoc2F2aW5nVG9hc3QudG9hc3RJZCwgc2F2aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCkpO1xyXG4gICAgICAgICAgICB0aGlzLmRpc2FibGVCdXR0b25zID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHJldmVyc2VTd2FwRGF5KG1pU3dhcElkOiBzdHJpbmcpIHtcclxuICAgICAgICBpZiAod2luZG93LmNvbmZpcm0oJ1JldmVyc2luZyBhIGRheSB0aGF0IHdhcyBhbHJlYWR5IGdpdmVuIHdpbGwgcmVzdWx0IGluIGEgJyArXHJcbiAgICAgICAgICAgICdwcm9jZXNzaW5nIGZlZSBiZWluZyBkZWR1Y3RlZCBmcm9tIHRoZSBnaXZpbmcgTWlTd2FwIGFjY291bnQuIFByZXNzIE9rIHRvIGNvbnRpbnVlLicpKSB7XHJcbiAgICAgICAgICAgIGxldCBzYXZpbmdUb2FzdCA9IHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLnBvcChuZXcgU3Bpbm5pbmdUb2FzdCgpKTtcclxuICAgICAgICAgICAgdGhpcy5kaXNhYmxlQnV0dG9ucyA9IHRydWU7XHJcbiAgICAgICAgICAgIHRoaXMuX3N3YXBzU2VydmljZVxyXG4gICAgICAgICAgICAgICAgLlJldmVyc2VTd2FwRGF5JChtaVN3YXBJZCwgdGhpcy5fdXNlclNlcnZpY2UuU2VsZWN0ZWRDZW50cmUuaWQpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgICAgcmVzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBnZXQgbGF0ZXN0IHN3YXAgZGF5c1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3N3YXBzU2VydmljZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuR2V0U3dhcERheXMkKHRoaXMuX3VzZXJTZXJ2aWNlLlNlbGVjdGVkQ2VudHJlLmlkLCB0aGlzLmZyb21EYXRlKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHN3YXBzUmVzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc3dhcHMgPSBzd2Fwc1JlcztcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoc2F2aW5nVG9hc3QudG9hc3RJZCwgc2F2aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UucG9wQXN5bmMoJ3N1Y2Nlc3MnLCAnU2F2ZWQnLCAnJyk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgKGVycikgPT4gdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoc2F2aW5nVG9hc3QudG9hc3RJZCwgc2F2aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCkpO1xyXG4gICAgICAgICAgICB0aGlzLmRpc2FibGVCdXR0b25zID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByZXZpb3VzRGF5KCkge1xyXG4gICAgICAgIHRoaXMuZGlzYWJsZUJ1dHRvbnMgPSB0cnVlO1xyXG4gICAgICAgIHZhciBmcm9tRGF0ZTogRGF0ZSA9IG5ldyBEYXRlKHRoaXMuZnJvbURhdGUpO1xyXG4gICAgICAgIGZyb21EYXRlLnNldERhdGUoZnJvbURhdGUuZ2V0RGF0ZSgpIC0gMSk7XHJcblxyXG4gICAgICAgIHZhciB0b0RhdGU6IERhdGUgPSBuZXcgRGF0ZSh0aGlzLnRvRGF0ZSk7XHJcbiAgICAgICAgdG9EYXRlLnNldERhdGUodG9EYXRlLmdldERhdGUoKSAtIDEpO1xyXG5cclxuICAgICAgICB0aGlzLnJlZnJlc2hTd2Fwcyhmcm9tRGF0ZSwgdG9EYXRlKTtcclxuICAgICAgICB0aGlzLmRpc2FibGVCdXR0b25zID0gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgbmV4dERheSgpIHtcclxuICAgICAgICB0aGlzLmRpc2FibGVCdXR0b25zID0gdHJ1ZTtcclxuICAgICAgICB2YXIgZnJvbURhdGU6IERhdGUgPSBuZXcgRGF0ZSh0aGlzLmZyb21EYXRlKTtcclxuICAgICAgICBmcm9tRGF0ZS5zZXREYXRlKGZyb21EYXRlLmdldERhdGUoKSArIDEpO1xyXG5cclxuICAgICAgICB2YXIgdG9EYXRlOiBEYXRlID0gbmV3IERhdGUodGhpcy50b0RhdGUpO1xyXG4gICAgICAgIHRvRGF0ZS5zZXREYXRlKHRvRGF0ZS5nZXREYXRlKCkgKyAxKTtcclxuXHJcbiAgICAgICAgdGhpcy5yZWZyZXNoU3dhcHMoZnJvbURhdGUsIHRvRGF0ZSk7XHJcbiAgICAgICAgdGhpcy5kaXNhYmxlQnV0dG9ucyA9IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIHJlZnJlc2hTd2Fwcyhmcm9tRGF0ZTogRGF0ZSwgdG9EYXRlOiBEYXRlKSB7XHJcbiAgICAgICAgdGhpcy5fc3dhcHNTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5HZXRTd2FwRGF5cyQodGhpcy5fdXNlclNlcnZpY2UuU2VsZWN0ZWRDZW50cmUuaWQsIGZyb21EYXRlKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKHN3YXBzUmVzID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc3dhcHMgPSBzd2Fwc1JlcztcclxuICAgICAgICAgICAgICAgIHRoaXMuZnJvbURhdGUgPSBmcm9tRGF0ZTtcclxuICAgICAgICAgICAgICAgIHRoaXMudG9EYXRlID0gdG9EYXRlO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBzd2FwU3RhdGVTdHJpbmcoc3dhcFN0YXRlOiBudW1iZXIpIHtcclxuICAgICAgICBzd2l0Y2ggKHN3YXBTdGF0ZSkge1xyXG4gICAgICAgICAgICBjYXNlIDA6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gJ1RvIEdpdmUnO1xyXG4gICAgICAgICAgICBjYXNlIDE6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gJ0dpdmVuJztcclxuICAgICAgICAgICAgY2FzZSAyOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuICdQcm9jZXNzaW5nJztcclxuICAgICAgICAgICAgY2FzZSAzOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuICdDYW5jZWxsZWQnO1xyXG4gICAgICAgICAgICBjYXNlIDQ6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gJ0V4cGlyZWQnO1xyXG4gICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuICdVbmtub3duJztcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgc3dhcFN0YXRlU3R5bGUoc3dhcFN0YXRlOiBudW1iZXIpIHtcclxuICAgICAgICBzd2l0Y2ggKHN3YXBTdGF0ZSkge1xyXG4gICAgICAgICAgICBjYXNlIDA6IC8vIFRvIEdpdmVcclxuICAgICAgICAgICAgICAgIHJldHVybiAnYnRuIGJ0bi1zbSBidG4tc3VjY2VzcyBidG4tYmxvY2snO1xyXG4gICAgICAgICAgICBjYXNlIDE6IC8vIEdpdmVuXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gJ2J0biBidG4tc20gYnRuLXByaW1hcnkgYnRuLWJsb2NrJztcclxuICAgICAgICAgICAgY2FzZSAyOiAvLyBQcm9jZXNzaW5nXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gJ2J0biBidG4tc20gYnRuLXdhcm5pbmcgYnRuLWJsb2NrJztcclxuICAgICAgICAgICAgY2FzZSAzOiAvLyBDYW5jZWxsZWRcclxuICAgICAgICAgICAgICAgIHJldHVybiAnYnRuIGJ0bi1zbSBidG4tZGFuZ2VyIGJ0bi1ibG9jayc7XHJcbiAgICAgICAgICAgIGNhc2UgNDogLy8gRXhwaXJlZFxyXG4gICAgICAgICAgICAgICAgcmV0dXJuICdidG4gYnRuLXNtIGJ0bi1kYW5nZXIgYnRuLWJsb2NrJztcclxuICAgICAgICAgICAgZGVmYXVsdDogICAgLy8gVW5rb3duXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gJ2J0biBidG4tc20gYnRuLXNlY29uZGF5IGJ0bi1ibG9jayc7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==
