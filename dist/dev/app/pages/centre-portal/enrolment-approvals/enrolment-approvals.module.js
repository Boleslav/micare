"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../shared/index');
var index_2 = require('./index');
var EnrolmentApprovalsModule = (function () {
    function EnrolmentApprovalsModule() {
    }
    EnrolmentApprovalsModule = __decorate([
        core_1.NgModule({
            imports: [
                index_1.SharedModule
            ],
            declarations: [index_2.EnrolmentApprovalsComponent, index_2.EnrolmentApprovalsTableComponent],
            providers: [index_2.EnrolmentApprovalsService],
            exports: [index_2.EnrolmentApprovalsComponent, index_2.EnrolmentApprovalsTableComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], EnrolmentApprovalsModule);
    return EnrolmentApprovalsModule;
}());
exports.EnrolmentApprovalsModule = EnrolmentApprovalsModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2Vucm9sbWVudC1hcHByb3ZhbHMvZW5yb2xtZW50LWFwcHJvdmFscy5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUF5QixlQUFlLENBQUMsQ0FBQTtBQUN6QyxzQkFBNkIsdUJBQXVCLENBQUMsQ0FBQTtBQUNyRCxzQkFBeUcsU0FBUyxDQUFDLENBQUE7QUFXbkg7SUFBQTtJQUF3QyxDQUFDO0lBVHpDO1FBQUMsZUFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFO2dCQUNMLG9CQUFZO2FBQ2Y7WUFDRCxZQUFZLEVBQUUsQ0FBQyxtQ0FBMkIsRUFBRSx3Q0FBZ0MsQ0FBQztZQUM3RSxTQUFTLEVBQUUsQ0FBQyxpQ0FBeUIsQ0FBQztZQUN0QyxPQUFPLEVBQUUsQ0FBQyxtQ0FBMkIsRUFBRSx3Q0FBZ0MsQ0FBQztTQUMzRSxDQUFDOztnQ0FBQTtJQUVzQywrQkFBQztBQUFELENBQXhDLEFBQXlDLElBQUE7QUFBNUIsZ0NBQXdCLDJCQUFJLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL2NlbnRyZS1wb3J0YWwvZW5yb2xtZW50LWFwcHJvdmFscy9lbnJvbG1lbnQtYXBwcm92YWxzLm1vZHVsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFNoYXJlZE1vZHVsZSB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9pbmRleCc7XHJcbmltcG9ydCB7IEVucm9sbWVudEFwcHJvdmFsc0NvbXBvbmVudCwgRW5yb2xtZW50QXBwcm92YWxzVGFibGVDb21wb25lbnQsIEVucm9sbWVudEFwcHJvdmFsc1NlcnZpY2UgfSBmcm9tICcuL2luZGV4JztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiBbXHJcbiAgICAgICAgU2hhcmVkTW9kdWxlXHJcbiAgICBdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBbRW5yb2xtZW50QXBwcm92YWxzQ29tcG9uZW50LCBFbnJvbG1lbnRBcHByb3ZhbHNUYWJsZUNvbXBvbmVudF0sXHJcbiAgICBwcm92aWRlcnM6IFtFbnJvbG1lbnRBcHByb3ZhbHNTZXJ2aWNlXSxcclxuICAgIGV4cG9ydHM6IFtFbnJvbG1lbnRBcHByb3ZhbHNDb21wb25lbnQsIEVucm9sbWVudEFwcHJvdmFsc1RhYmxlQ29tcG9uZW50XVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEVucm9sbWVudEFwcHJvdmFsc01vZHVsZSB7IH1cclxuIl19
