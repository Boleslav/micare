"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var enrolmentApprovalsFilter_model_1 = require('./enrolmentApprovalsFilter.model');
var enrolmentApprovals_service_1 = require('../enrolmentApprovalsService/enrolmentApprovals.service');
var index_1 = require('../../../../core/index');
var angular2_toaster_1 = require('angular2-toaster');
var index_2 = require('../../../../shared/index');
var EnrolmentApprovalsTableComponent = (function () {
    function EnrolmentApprovalsTableComponent(_enrolmentApprovalsService, _userService, _toasterService) {
        this._enrolmentApprovalsService = _enrolmentApprovalsService;
        this._userService = _userService;
        this._toasterService = _toasterService;
        this.DefaultApprovalStatus = 'Show All';
        this.centreChildren = null;
        this.approvalStatuses = enrolmentApprovalsFilter_model_1.EnrolmentApprovalsFilter;
        this.approvalStatusFilter = {
            filterText: '',
            filterId: 0
        };
        this.setApprovalStatus(this.DefaultApprovalStatus);
    }
    EnrolmentApprovalsTableComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._enrolmentApprovalsService
            .GetCentreChildren$(this._userService.SelectedCentre.id)
            .subscribe(function (res) {
            _this.centreChildren = res;
        });
    };
    EnrolmentApprovalsTableComponent.prototype.keys = function () {
        return Object.keys(this.approvalStatuses);
    };
    EnrolmentApprovalsTableComponent.prototype.setApprovalStatus = function (status) {
        this.approvalStatusFilter.filterText = status;
        this.approvalStatusFilter.filterId = this.approvalStatuses[status];
    };
    EnrolmentApprovalsTableComponent.prototype.getDefaultApprovalStatusId = function () {
        return this.approvalStatuses[this.DefaultApprovalStatus];
    };
    EnrolmentApprovalsTableComponent.prototype.approveChild = function (child) {
        var _this = this;
        var savingToast = this._toasterService.pop(new index_2.SpinningToast());
        child.childApprovalStatus = this.approvalStatuses['Approved'];
        this._enrolmentApprovalsService
            .ApproveChildForCentre$(child.centreId, child.kidId)
            .subscribe(function () {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this._toasterService.popAsync('success', 'Saved', '');
        }, function (err) { return _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId); });
    };
    EnrolmentApprovalsTableComponent.prototype.denyChild = function (child) {
        var _this = this;
        var savingToast = this._toasterService.pop(new index_2.SpinningToast());
        child.childApprovalStatus = this.approvalStatuses['Denied'];
        this._enrolmentApprovalsService
            .DenyChildForCentre$(child.centreId, child.kidId)
            .subscribe(function () {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this._toasterService.popAsync('success', 'Saved', '');
        }, function (err) { return _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId); });
    };
    EnrolmentApprovalsTableComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'enrolment-approvals-table-cmp',
            templateUrl: './enrolmentApprovalsTable.component.html',
            styleUrls: ['./enrolmentApprovalsTable.component.css']
        }), 
        __metadata('design:paramtypes', [enrolmentApprovals_service_1.EnrolmentApprovalsService, index_1.CentreUserService, angular2_toaster_1.ToasterService])
    ], EnrolmentApprovalsTableComponent);
    return EnrolmentApprovalsTableComponent;
}());
exports.EnrolmentApprovalsTableComponent = EnrolmentApprovalsTableComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2Vucm9sbWVudC1hcHByb3ZhbHMvZW5yb2xtZW50QXBwcm92YWxzVGFibGUvZW5yb2xtZW50QXBwcm92YWxzVGFibGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBa0MsZUFBZSxDQUFDLENBQUE7QUFFbEQsK0NBQXlDLGtDQUFrQyxDQUFDLENBQUE7QUFDNUUsMkNBQTBDLHlEQUF5RCxDQUFDLENBQUE7QUFFcEcsc0JBQWtDLHdCQUF3QixDQUFDLENBQUE7QUFFM0QsaUNBQStCLGtCQUFrQixDQUFDLENBQUE7QUFDbEQsc0JBQThCLDBCQUEwQixDQUFDLENBQUE7QUFTekQ7SUFXSSwwQ0FDWSwwQkFBcUQsRUFDckQsWUFBK0IsRUFDL0IsZUFBK0I7UUFGL0IsK0JBQTBCLEdBQTFCLDBCQUEwQixDQUEyQjtRQUNyRCxpQkFBWSxHQUFaLFlBQVksQ0FBbUI7UUFDL0Isb0JBQWUsR0FBZixlQUFlLENBQWdCO1FBWmxDLDBCQUFxQixHQUFXLFVBQVUsQ0FBQztRQUU1QyxtQkFBYyxHQUFxQixJQUFJLENBQUM7UUFDeEMscUJBQWdCLEdBQVEseURBQXdCLENBQUM7UUFDakQseUJBQW9CLEdBQVE7WUFDaEMsVUFBVSxFQUFFLEVBQUU7WUFDZCxRQUFRLEVBQUUsQ0FBQztTQUNkLENBQUM7UUFNRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUM7SUFDdkQsQ0FBQztJQUVELG1EQUFRLEdBQVI7UUFBQSxpQkFPQztRQU5HLElBQUksQ0FBQywwQkFBMEI7YUFDMUIsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsRUFBRSxDQUFDO2FBQ3ZELFNBQVMsQ0FDVixVQUFBLEdBQUc7WUFDQyxLQUFJLENBQUMsY0FBYyxHQUFHLEdBQUcsQ0FBQztRQUM5QixDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFRCwrQ0FBSSxHQUFKO1FBQ0ksTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7SUFDOUMsQ0FBQztJQUVELDREQUFpQixHQUFqQixVQUFrQixNQUFjO1FBQzVCLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDO1FBQzlDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3ZFLENBQUM7SUFFRCxxRUFBMEIsR0FBMUI7UUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO0lBQzdELENBQUM7SUFFRCx1REFBWSxHQUFaLFVBQWEsS0FBcUI7UUFBbEMsaUJBU0M7UUFSRyxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxJQUFJLHFCQUFhLEVBQUUsQ0FBQyxDQUFDO1FBQ2hFLEtBQUssQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDOUQsSUFBSSxDQUFDLDBCQUEwQjthQUMxQixzQkFBc0IsQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxLQUFLLENBQUM7YUFDbkQsU0FBUyxDQUFDO1lBQ1AsS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxXQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUM5RSxLQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsT0FBTyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQzFELENBQUMsRUFBRSxVQUFDLEdBQUcsSUFBSyxPQUFBLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLGdCQUFnQixDQUFDLEVBQTdFLENBQTZFLENBQUMsQ0FBQztJQUNuRyxDQUFDO0lBRUQsb0RBQVMsR0FBVCxVQUFVLEtBQXFCO1FBQS9CLGlCQVNDO1FBUkcsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsSUFBSSxxQkFBYSxFQUFFLENBQUMsQ0FBQztRQUNoRSxLQUFLLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzVELElBQUksQ0FBQywwQkFBMEI7YUFDMUIsbUJBQW1CLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsS0FBSyxDQUFDO2FBQ2hELFNBQVMsQ0FBQztZQUNQLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDOUUsS0FBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLE9BQU8sRUFBRSxFQUFFLENBQUMsQ0FBQztRQUMxRCxDQUFDLEVBQUUsVUFBQyxHQUFHLElBQUssT0FBQSxLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUE3RSxDQUE2RSxDQUFDLENBQUM7SUFDbkcsQ0FBQztJQW5FTDtRQUFDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLCtCQUErQjtZQUN6QyxXQUFXLEVBQUUsMENBQTBDO1lBQ3ZELFNBQVMsRUFBRSxDQUFDLHlDQUF5QyxDQUFDO1NBQ3pELENBQUM7O3dDQUFBO0lBK0RGLHVDQUFDO0FBQUQsQ0E3REEsQUE2REMsSUFBQTtBQTdEWSx3Q0FBZ0MsbUNBNkQ1QyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2Vucm9sbWVudC1hcHByb3ZhbHMvZW5yb2xtZW50QXBwcm92YWxzVGFibGUvZW5yb2xtZW50QXBwcm92YWxzVGFibGUuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmltcG9ydCB7IEVucm9sbWVudEFwcHJvdmFsc0ZpbHRlciB9IGZyb20gJy4vZW5yb2xtZW50QXBwcm92YWxzRmlsdGVyLm1vZGVsJztcclxuaW1wb3J0IHsgRW5yb2xtZW50QXBwcm92YWxzU2VydmljZSB9IGZyb20gJy4uL2Vucm9sbWVudEFwcHJvdmFsc1NlcnZpY2UvZW5yb2xtZW50QXBwcm92YWxzLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDZW50cmVDaGlsZER0byB9IGZyb20gJy4uLy4uLy4uLy4uL2FwaS9pbmRleCc7XHJcbmltcG9ydCB7IENlbnRyZVVzZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vY29yZS9pbmRleCc7XHJcblxyXG5pbXBvcnQgeyBUb2FzdGVyU2VydmljZSB9IGZyb20gJ2FuZ3VsYXIyLXRvYXN0ZXInO1xyXG5pbXBvcnQgeyBTcGlubmluZ1RvYXN0IH0gZnJvbSAnLi4vLi4vLi4vLi4vc2hhcmVkL2luZGV4JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHNlbGVjdG9yOiAnZW5yb2xtZW50LWFwcHJvdmFscy10YWJsZS1jbXAnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2Vucm9sbWVudEFwcHJvdmFsc1RhYmxlLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2Vucm9sbWVudEFwcHJvdmFsc1RhYmxlLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEVucm9sbWVudEFwcHJvdmFsc1RhYmxlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICByZWFkb25seSBEZWZhdWx0QXBwcm92YWxTdGF0dXM6IHN0cmluZyA9ICdTaG93IEFsbCc7XHJcblxyXG4gICAgcHJpdmF0ZSBjZW50cmVDaGlsZHJlbjogQ2VudHJlQ2hpbGREdG9bXSA9IG51bGw7XHJcbiAgICBwcml2YXRlIGFwcHJvdmFsU3RhdHVzZXM6IGFueSA9IEVucm9sbWVudEFwcHJvdmFsc0ZpbHRlcjtcclxuICAgIHByaXZhdGUgYXBwcm92YWxTdGF0dXNGaWx0ZXI6IGFueSA9IHtcclxuICAgICAgICBmaWx0ZXJUZXh0OiAnJyxcclxuICAgICAgICBmaWx0ZXJJZDogMFxyXG4gICAgfTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwcml2YXRlIF9lbnJvbG1lbnRBcHByb3ZhbHNTZXJ2aWNlOiBFbnJvbG1lbnRBcHByb3ZhbHNTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgX3VzZXJTZXJ2aWNlOiBDZW50cmVVc2VyU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIF90b2FzdGVyU2VydmljZTogVG9hc3RlclNlcnZpY2UpIHtcclxuICAgICAgICB0aGlzLnNldEFwcHJvdmFsU3RhdHVzKHRoaXMuRGVmYXVsdEFwcHJvdmFsU3RhdHVzKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLl9lbnJvbG1lbnRBcHByb3ZhbHNTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5HZXRDZW50cmVDaGlsZHJlbiQodGhpcy5fdXNlclNlcnZpY2UuU2VsZWN0ZWRDZW50cmUuaWQpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgIHJlcyA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNlbnRyZUNoaWxkcmVuID0gcmVzO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBrZXlzKCk6IEFycmF5PHN0cmluZz4ge1xyXG4gICAgICAgIHJldHVybiBPYmplY3Qua2V5cyh0aGlzLmFwcHJvdmFsU3RhdHVzZXMpO1xyXG4gICAgfVxyXG5cclxuICAgIHNldEFwcHJvdmFsU3RhdHVzKHN0YXR1czogc3RyaW5nKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5hcHByb3ZhbFN0YXR1c0ZpbHRlci5maWx0ZXJUZXh0ID0gc3RhdHVzO1xyXG4gICAgICAgIHRoaXMuYXBwcm92YWxTdGF0dXNGaWx0ZXIuZmlsdGVySWQgPSB0aGlzLmFwcHJvdmFsU3RhdHVzZXNbc3RhdHVzXTtcclxuICAgIH1cclxuXHJcbiAgICBnZXREZWZhdWx0QXBwcm92YWxTdGF0dXNJZCgpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFwcHJvdmFsU3RhdHVzZXNbdGhpcy5EZWZhdWx0QXBwcm92YWxTdGF0dXNdO1xyXG4gICAgfVxyXG5cclxuICAgIGFwcHJvdmVDaGlsZChjaGlsZDogQ2VudHJlQ2hpbGREdG8pOiB2b2lkIHtcclxuICAgICAgICBsZXQgc2F2aW5nVG9hc3QgPSB0aGlzLl90b2FzdGVyU2VydmljZS5wb3AobmV3IFNwaW5uaW5nVG9hc3QoKSk7XHJcbiAgICAgICAgY2hpbGQuY2hpbGRBcHByb3ZhbFN0YXR1cyA9IHRoaXMuYXBwcm92YWxTdGF0dXNlc1snQXBwcm92ZWQnXTtcclxuICAgICAgICB0aGlzLl9lbnJvbG1lbnRBcHByb3ZhbHNTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5BcHByb3ZlQ2hpbGRGb3JDZW50cmUkKGNoaWxkLmNlbnRyZUlkLCBjaGlsZC5raWRJZClcclxuICAgICAgICAgICAgLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcihzYXZpbmdUb2FzdC50b2FzdElkLCBzYXZpbmdUb2FzdC50b2FzdENvbnRhaW5lcklkKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLnBvcEFzeW5jKCdzdWNjZXNzJywgJ1NhdmVkJywgJycpO1xyXG4gICAgICAgICAgICB9LCAoZXJyKSA9PiB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcihzYXZpbmdUb2FzdC50b2FzdElkLCBzYXZpbmdUb2FzdC50b2FzdENvbnRhaW5lcklkKSk7XHJcbiAgICB9XHJcblxyXG4gICAgZGVueUNoaWxkKGNoaWxkOiBDZW50cmVDaGlsZER0byk6IHZvaWQge1xyXG4gICAgICAgIGxldCBzYXZpbmdUb2FzdCA9IHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLnBvcChuZXcgU3Bpbm5pbmdUb2FzdCgpKTtcclxuICAgICAgICBjaGlsZC5jaGlsZEFwcHJvdmFsU3RhdHVzID0gdGhpcy5hcHByb3ZhbFN0YXR1c2VzWydEZW5pZWQnXTtcclxuICAgICAgICB0aGlzLl9lbnJvbG1lbnRBcHByb3ZhbHNTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5EZW55Q2hpbGRGb3JDZW50cmUkKGNoaWxkLmNlbnRyZUlkLCBjaGlsZC5raWRJZClcclxuICAgICAgICAgICAgLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcihzYXZpbmdUb2FzdC50b2FzdElkLCBzYXZpbmdUb2FzdC50b2FzdENvbnRhaW5lcklkKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLnBvcEFzeW5jKCdzdWNjZXNzJywgJ1NhdmVkJywgJycpO1xyXG4gICAgICAgICAgICB9LCAoZXJyKSA9PiB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcihzYXZpbmdUb2FzdC50b2FzdElkLCBzYXZpbmdUb2FzdC50b2FzdENvbnRhaW5lcklkKSk7XHJcbiAgICB9XHJcbn1cclxuIl19
