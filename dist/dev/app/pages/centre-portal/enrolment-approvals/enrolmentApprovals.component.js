"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var EnrolmentApprovalsComponent = (function () {
    function EnrolmentApprovalsComponent() {
    }
    EnrolmentApprovalsComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'enrolment-approvals-cmp',
            templateUrl: './enrolmentApprovals.component.html'
        }), 
        __metadata('design:paramtypes', [])
    ], EnrolmentApprovalsComponent);
    return EnrolmentApprovalsComponent;
}());
exports.EnrolmentApprovalsComponent = EnrolmentApprovalsComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2Vucm9sbWVudC1hcHByb3ZhbHMvZW5yb2xtZW50QXBwcm92YWxzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQXdCLGVBQWUsQ0FBQyxDQUFBO0FBUXhDO0lBQUE7SUFBMEMsQ0FBQztJQU4zQztRQUFDLGdCQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDaEIsUUFBUSxFQUFFLHlCQUF5QjtZQUNuQyxXQUFXLEVBQUUscUNBQXFDO1NBQ3JELENBQUM7O21DQUFBO0lBRXdDLGtDQUFDO0FBQUQsQ0FBMUMsQUFBMkMsSUFBQTtBQUE5QixtQ0FBMkIsOEJBQUcsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9lbnJvbG1lbnQtYXBwcm92YWxzL2Vucm9sbWVudEFwcHJvdmFscy5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuXHRtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgc2VsZWN0b3I6ICdlbnJvbG1lbnQtYXBwcm92YWxzLWNtcCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vZW5yb2xtZW50QXBwcm92YWxzLmNvbXBvbmVudC5odG1sJ1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEVucm9sbWVudEFwcHJvdmFsc0NvbXBvbmVudCB7fVxyXG4iXX0=
