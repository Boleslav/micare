"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../../api/index');
var EnrolmentApprovalsService = (function () {
    function EnrolmentApprovalsService(_apiService) {
        this._apiService = _apiService;
    }
    EnrolmentApprovalsService.prototype.ApproveChildForCentre$ = function (centreId, kidId) {
        return this._apiService.enrolmentForm_ApproveChildEnrolment(centreId, kidId);
    };
    EnrolmentApprovalsService.prototype.DenyChildForCentre$ = function (centreId, kidId) {
        return this._apiService.enrolmentForm_DenyChildEnrolment(centreId, kidId);
    };
    EnrolmentApprovalsService.prototype.GetCentreChildren$ = function (centreId) {
        return this._apiService.centre_GetCentreChildren(centreId);
    };
    EnrolmentApprovalsService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.ApiClientService])
    ], EnrolmentApprovalsService);
    return EnrolmentApprovalsService;
}());
exports.EnrolmentApprovalsService = EnrolmentApprovalsService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2Vucm9sbWVudC1hcHByb3ZhbHMvZW5yb2xtZW50QXBwcm92YWxzU2VydmljZS9lbnJvbG1lbnRBcHByb3ZhbHMuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQTJCLGVBQWUsQ0FBQyxDQUFBO0FBRzNDLHNCQUFpRCx1QkFBdUIsQ0FBQyxDQUFBO0FBR3pFO0lBRUksbUNBQW9CLFdBQTZCO1FBQTdCLGdCQUFXLEdBQVgsV0FBVyxDQUFrQjtJQUNqRCxDQUFDO0lBRU0sMERBQXNCLEdBQTdCLFVBQThCLFFBQWdCLEVBQUUsS0FBYTtRQUN6RCxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQ0FBbUMsQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDakYsQ0FBQztJQUVNLHVEQUFtQixHQUExQixVQUEyQixRQUFnQixFQUFFLEtBQWE7UUFDdEQsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsZ0NBQWdDLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzlFLENBQUM7SUFFTSxzREFBa0IsR0FBekIsVUFBMEIsUUFBZ0I7UUFDdEMsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsd0JBQXdCLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDL0QsQ0FBQztJQWhCTDtRQUFDLGlCQUFVLEVBQUU7O2lDQUFBO0lBaUJiLGdDQUFDO0FBQUQsQ0FoQkEsQUFnQkMsSUFBQTtBQWhCWSxpQ0FBeUIsNEJBZ0JyQyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2Vucm9sbWVudC1hcHByb3ZhbHMvZW5yb2xtZW50QXBwcm92YWxzU2VydmljZS9lbnJvbG1lbnRBcHByb3ZhbHMuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgQXBpQ2xpZW50U2VydmljZSwgQ2VudHJlQ2hpbGREdG8gfSBmcm9tICcuLi8uLi8uLi8uLi9hcGkvaW5kZXgnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgRW5yb2xtZW50QXBwcm92YWxzU2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfYXBpU2VydmljZTogQXBpQ2xpZW50U2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBBcHByb3ZlQ2hpbGRGb3JDZW50cmUkKGNlbnRyZUlkOiBzdHJpbmcsIGtpZElkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPHZvaWQ+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fYXBpU2VydmljZS5lbnJvbG1lbnRGb3JtX0FwcHJvdmVDaGlsZEVucm9sbWVudChjZW50cmVJZCwga2lkSWQpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBEZW55Q2hpbGRGb3JDZW50cmUkKGNlbnRyZUlkOiBzdHJpbmcsIGtpZElkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPHZvaWQ+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fYXBpU2VydmljZS5lbnJvbG1lbnRGb3JtX0RlbnlDaGlsZEVucm9sbWVudChjZW50cmVJZCwga2lkSWQpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBHZXRDZW50cmVDaGlsZHJlbiQoY2VudHJlSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8Q2VudHJlQ2hpbGREdG9bXT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9hcGlTZXJ2aWNlLmNlbnRyZV9HZXRDZW50cmVDaGlsZHJlbihjZW50cmVJZCk7XHJcbiAgICB9XHJcbn1cclxuIl19
