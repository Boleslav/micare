"use strict";
var index_1 = require('../../../auth/index');
var index_2 = require('./index');
exports.AdminApprovalsRoutes = [
    {
        path: 'admin-approvals',
        component: index_2.AdminApprovalsComponent,
        canActivate: [index_1.SysAdminGuardService]
    },
];

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2FkbWluLW1hc3MtaW52aXRlcy9hZG1pbi1hcHByb3ZhbHMucm91dGVzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFDQSxzQkFBcUMscUJBQXFCLENBQUMsQ0FBQTtBQUMzRCxzQkFBd0MsU0FBUyxDQUFDLENBQUE7QUFFckMsNEJBQW9CLEdBQVk7SUFDNUM7UUFDQyxJQUFJLEVBQUUsaUJBQWlCO1FBQ3ZCLFNBQVMsRUFBRSwrQkFBdUI7UUFDbEMsV0FBVyxFQUFFLENBQUMsNEJBQW9CLENBQUM7S0FDbkM7Q0FDRCxDQUFDIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2FkbWluLW1hc3MtaW52aXRlcy9hZG1pbi1hcHByb3ZhbHMucm91dGVzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBTeXNBZG1pbkd1YXJkU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL2F1dGgvaW5kZXgnO1xyXG5pbXBvcnQgeyBBZG1pbkFwcHJvdmFsc0NvbXBvbmVudCB9IGZyb20gJy4vaW5kZXgnO1xyXG5cclxuZXhwb3J0IGNvbnN0IEFkbWluQXBwcm92YWxzUm91dGVzOiBSb3V0ZVtdID0gW1xyXG5cdHtcclxuXHRcdHBhdGg6ICdhZG1pbi1hcHByb3ZhbHMnLFxyXG5cdFx0Y29tcG9uZW50OiBBZG1pbkFwcHJvdmFsc0NvbXBvbmVudCxcclxuXHRcdGNhbkFjdGl2YXRlOiBbU3lzQWRtaW5HdWFyZFNlcnZpY2VdXHJcblx0fSxcclxuXTtcclxuIl19
