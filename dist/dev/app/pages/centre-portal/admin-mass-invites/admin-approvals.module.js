"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../shared/index');
var index_2 = require('./index');
var AdminApprovalsModule = (function () {
    function AdminApprovalsModule() {
    }
    AdminApprovalsModule = __decorate([
        core_1.NgModule({
            imports: [
                index_1.SharedModule
            ],
            declarations: [index_2.AdminApprovalsComponent, index_2.AdminApprovalsTableComponent],
            providers: [index_2.AdminApprovalsService],
            exports: [index_2.AdminApprovalsComponent, index_2.AdminApprovalsTableComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AdminApprovalsModule);
    return AdminApprovalsModule;
}());
exports.AdminApprovalsModule = AdminApprovalsModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2FkbWluLW1hc3MtaW52aXRlcy9hZG1pbi1hcHByb3ZhbHMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBeUIsZUFBZSxDQUFDLENBQUE7QUFDekMsc0JBQTZCLHVCQUF1QixDQUFDLENBQUE7QUFDckQsc0JBQTZGLFNBQVMsQ0FBQyxDQUFBO0FBV3ZHO0lBQUE7SUFBb0MsQ0FBQztJQVRyQztRQUFDLGVBQVEsQ0FBQztZQUNOLE9BQU8sRUFBRTtnQkFDTCxvQkFBWTthQUNmO1lBQ0QsWUFBWSxFQUFFLENBQUMsK0JBQXVCLEVBQUUsb0NBQTRCLENBQUM7WUFDckUsU0FBUyxFQUFFLENBQUMsNkJBQXFCLENBQUM7WUFDbEMsT0FBTyxFQUFFLENBQUMsK0JBQXVCLEVBQUUsb0NBQTRCLENBQUM7U0FDbkUsQ0FBQzs7NEJBQUE7SUFFa0MsMkJBQUM7QUFBRCxDQUFwQyxBQUFxQyxJQUFBO0FBQXhCLDRCQUFvQix1QkFBSSxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2FkbWluLW1hc3MtaW52aXRlcy9hZG1pbi1hcHByb3ZhbHMubW9kdWxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU2hhcmVkTW9kdWxlIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL2luZGV4JztcclxuaW1wb3J0IHsgQWRtaW5BcHByb3ZhbHNDb21wb25lbnQsIEFkbWluQXBwcm92YWxzVGFibGVDb21wb25lbnQsIEFkbWluQXBwcm92YWxzU2VydmljZSB9IGZyb20gJy4vaW5kZXgnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBTaGFyZWRNb2R1bGVcclxuICAgIF0sXHJcbiAgICBkZWNsYXJhdGlvbnM6IFtBZG1pbkFwcHJvdmFsc0NvbXBvbmVudCwgQWRtaW5BcHByb3ZhbHNUYWJsZUNvbXBvbmVudF0sXHJcbiAgICBwcm92aWRlcnM6IFtBZG1pbkFwcHJvdmFsc1NlcnZpY2VdLFxyXG4gICAgZXhwb3J0czogW0FkbWluQXBwcm92YWxzQ29tcG9uZW50LCBBZG1pbkFwcHJvdmFsc1RhYmxlQ29tcG9uZW50XVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEFkbWluQXBwcm92YWxzTW9kdWxlIHsgfVxyXG4iXX0=
