"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var AdminApprovalsComponent = (function () {
    function AdminApprovalsComponent() {
    }
    AdminApprovalsComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'admin-approvals-cmp',
            templateUrl: './adminApprovals.component.html'
        }), 
        __metadata('design:paramtypes', [])
    ], AdminApprovalsComponent);
    return AdminApprovalsComponent;
}());
exports.AdminApprovalsComponent = AdminApprovalsComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2FkbWluLW1hc3MtaW52aXRlcy9hZG1pbk1hc3NJbnZpdGVzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQXdCLGVBQWUsQ0FBQyxDQUFBO0FBUXhDO0lBQUE7SUFBc0MsQ0FBQztJQU52QztRQUFDLGdCQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDaEIsUUFBUSxFQUFFLHFCQUFxQjtZQUMvQixXQUFXLEVBQUUsaUNBQWlDO1NBQ2pELENBQUM7OytCQUFBO0lBRW9DLDhCQUFDO0FBQUQsQ0FBdEMsQUFBdUMsSUFBQTtBQUExQiwrQkFBdUIsMEJBQUcsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9hZG1pbi1tYXNzLWludml0ZXMvYWRtaW5NYXNzSW52aXRlcy5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuXHRtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgc2VsZWN0b3I6ICdhZG1pbi1hcHByb3ZhbHMtY21wJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9hZG1pbkFwcHJvdmFscy5jb21wb25lbnQuaHRtbCdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBBZG1pbkFwcHJvdmFsc0NvbXBvbmVudCB7fVxyXG4iXX0=
