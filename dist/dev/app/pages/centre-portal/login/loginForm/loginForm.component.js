"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var router_1 = require('@angular/router');
var login_service_1 = require('../loginService/login.service');
var angular2_toaster_1 = require('angular2-toaster');
var index_1 = require('../../../../shared/index');
var LoginFormComponent = (function () {
    function LoginFormComponent(fb, _toasterService, _loginService, _router) {
        this._toasterService = _toasterService;
        this._loginService = _loginService;
        this._router = _router;
        this.submitted = false;
        this.form = fb.group({
            'email': ['', forms_1.Validators.required],
            'password': ['', forms_1.Validators.required]
        });
        this.email = this.form.controls['email'];
        this.password = this.form.controls['password'];
    }
    LoginFormComponent.prototype.onSubmit = function (values) {
        var _this = this;
        this.submitted = true;
        var authenticatingToast = this._toasterService.pop(new index_1.SpinningToast('Authenticating'));
        this._loginService
            .login(this.email.value, this.password.value)
            .subscribe(function (res) {
            _this._router.navigate(['centre-portal']);
        }, function (err) {
            _this._toasterService.clear(authenticatingToast.toastId, authenticatingToast.toastContainerId);
            _this.submitted = false;
        }, function () {
            _this.submitted = false;
        });
    };
    ;
    LoginFormComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'login-form-cmp',
            styleUrls: ['loginForm.component.css'],
            templateUrl: 'loginForm.component.html'
        }),
        core_1.Injectable(), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, angular2_toaster_1.ToasterService, login_service_1.LoginService, router_1.Router])
    ], LoginFormComponent);
    return LoginFormComponent;
}());
exports.LoginFormComponent = LoginFormComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2xvZ2luL2xvZ2luRm9ybS9sb2dpbkZvcm0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBc0MsZUFBZSxDQUFDLENBQUE7QUFDdEQsc0JBQW9FLGdCQUFnQixDQUFDLENBQUE7QUFDckYsdUJBQXVCLGlCQUFpQixDQUFDLENBQUE7QUFFekMsOEJBQTZCLCtCQUErQixDQUFDLENBQUE7QUFDN0QsaUNBQStCLGtCQUFrQixDQUFDLENBQUE7QUFDbEQsc0JBQThCLDBCQUEwQixDQUFDLENBQUE7QUFVekQ7SUFPSSw0QkFBWSxFQUFlLEVBQ2YsZUFBK0IsRUFDL0IsYUFBMkIsRUFDM0IsT0FBZTtRQUZmLG9CQUFlLEdBQWYsZUFBZSxDQUFnQjtRQUMvQixrQkFBYSxHQUFiLGFBQWEsQ0FBYztRQUMzQixZQUFPLEdBQVAsT0FBTyxDQUFRO1FBTHBCLGNBQVMsR0FBWSxLQUFLLENBQUM7UUFPOUIsSUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDO1lBQ2pCLE9BQU8sRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLFFBQVEsQ0FBQztZQUNsQyxVQUFVLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxRQUFRLENBQUM7U0FDeEMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ25ELENBQUM7SUFFRCxxQ0FBUSxHQUFSLFVBQVMsTUFBVztRQUFwQixpQkFnQkM7UUFmRyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUN0QixJQUFJLG1CQUFtQixHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUkscUJBQWEsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7UUFFeEYsSUFBSSxDQUFDLGFBQWE7YUFDYixLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7YUFDNUMsU0FBUyxDQUFDLFVBQUEsR0FBRztZQUNWLEtBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztRQUM3QyxDQUFDLEVBQ0QsVUFBQSxHQUFHO1lBQ0MsS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsbUJBQW1CLENBQUMsT0FBTyxFQUFFLG1CQUFtQixDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDOUYsS0FBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7UUFDM0IsQ0FBQyxFQUNEO1lBQ0ksS0FBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7UUFDM0IsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDOztJQTdDTDtRQUFDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLGdCQUFnQjtZQUMxQixTQUFTLEVBQUUsQ0FBQyx5QkFBeUIsQ0FBQztZQUN0QyxXQUFXLEVBQUUsMEJBQTBCO1NBQzFDLENBQUM7UUFFRCxpQkFBVSxFQUFFOzswQkFBQTtJQXVDYix5QkFBQztBQUFELENBdENBLEFBc0NDLElBQUE7QUF0Q1ksMEJBQWtCLHFCQXNDOUIsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9sb2dpbi9sb2dpbkZvcm0vbG9naW5Gb3JtLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIENvbXBvbmVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGb3JtR3JvdXAsIEFic3RyYWN0Q29udHJvbCwgRm9ybUJ1aWxkZXIsIFZhbGlkYXRvcnMgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcblxyXG5pbXBvcnQgeyBMb2dpblNlcnZpY2UgfSBmcm9tICcuLi9sb2dpblNlcnZpY2UvbG9naW4uc2VydmljZSc7XHJcbmltcG9ydCB7IFRvYXN0ZXJTZXJ2aWNlIH0gZnJvbSAnYW5ndWxhcjItdG9hc3Rlcic7XHJcbmltcG9ydCB7IFNwaW5uaW5nVG9hc3QgfSBmcm9tICcuLi8uLi8uLi8uLi9zaGFyZWQvaW5kZXgnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgc2VsZWN0b3I6ICdsb2dpbi1mb3JtLWNtcCcsXHJcbiAgICBzdHlsZVVybHM6IFsnbG9naW5Gb3JtLmNvbXBvbmVudC5jc3MnXSxcclxuICAgIHRlbXBsYXRlVXJsOiAnbG9naW5Gb3JtLmNvbXBvbmVudC5odG1sJ1xyXG59KVxyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgTG9naW5Gb3JtQ29tcG9uZW50IHtcclxuICAgIHB1YmxpYyBmb3JtOiBGb3JtR3JvdXA7XHJcbiAgICBwdWJsaWMgZW1haWw6IEFic3RyYWN0Q29udHJvbDtcclxuICAgIHB1YmxpYyBwYXNzd29yZDogQWJzdHJhY3RDb250cm9sO1xyXG5cclxuICAgIHB1YmxpYyBzdWJtaXR0ZWQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihmYjogRm9ybUJ1aWxkZXIsXHJcbiAgICAgICAgcHJpdmF0ZSBfdG9hc3RlclNlcnZpY2U6IFRvYXN0ZXJTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgX2xvZ2luU2VydmljZTogTG9naW5TZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgX3JvdXRlcjogUm91dGVyKSB7XHJcblxyXG4gICAgICAgIHRoaXMuZm9ybSA9IGZiLmdyb3VwKHtcclxuICAgICAgICAgICAgJ2VtYWlsJzogWycnLCBWYWxpZGF0b3JzLnJlcXVpcmVkXSxcclxuICAgICAgICAgICAgJ3Bhc3N3b3JkJzogWycnLCBWYWxpZGF0b3JzLnJlcXVpcmVkXVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICB0aGlzLmVtYWlsID0gdGhpcy5mb3JtLmNvbnRyb2xzWydlbWFpbCddO1xyXG4gICAgICAgIHRoaXMucGFzc3dvcmQgPSB0aGlzLmZvcm0uY29udHJvbHNbJ3Bhc3N3b3JkJ107XHJcbiAgICB9XHJcblxyXG4gICAgb25TdWJtaXQodmFsdWVzOiBhbnkpIHtcclxuICAgICAgICB0aGlzLnN1Ym1pdHRlZCA9IHRydWU7XHJcbiAgICAgICAgdmFyIGF1dGhlbnRpY2F0aW5nVG9hc3QgPSB0aGlzLl90b2FzdGVyU2VydmljZS5wb3AobmV3IFNwaW5uaW5nVG9hc3QoJ0F1dGhlbnRpY2F0aW5nJykpO1xyXG5cclxuICAgICAgICB0aGlzLl9sb2dpblNlcnZpY2VcclxuICAgICAgICAgICAgLmxvZ2luKHRoaXMuZW1haWwudmFsdWUsIHRoaXMucGFzc3dvcmQudmFsdWUpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3JvdXRlci5uYXZpZ2F0ZShbJ2NlbnRyZS1wb3J0YWwnXSk7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGVyciA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcihhdXRoZW50aWNhdGluZ1RvYXN0LnRvYXN0SWQsIGF1dGhlbnRpY2F0aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnN1Ym1pdHRlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnN1Ym1pdHRlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH07XHJcbn1cclxuIl19
