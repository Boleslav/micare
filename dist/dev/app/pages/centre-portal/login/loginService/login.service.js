"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../../core/index');
var index_2 = require('../../../../shared/index');
var index_3 = require('../../../../api/index');
var LoginService = (function () {
    function LoginService(_authenticationService, _apiService) {
        this._authenticationService = _authenticationService;
        this._apiService = _apiService;
    }
    LoginService.prototype.login = function (username, password) {
        var _this = this;
        return this._apiService
            .token('password', username, password, index_2.Config.CLIENT_ID)
            .map(function (res) {
            _this._authenticationService.saveAccessToken(res.value, false);
        });
    };
    LoginService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.AccessTokenService, index_3.ApiClientService])
    ], LoginService);
    return LoginService;
}());
exports.LoginService = LoginService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2xvZ2luL2xvZ2luU2VydmljZS9sb2dpbi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBMkIsZUFBZSxDQUFDLENBQUE7QUFHM0Msc0JBQW1DLHdCQUF3QixDQUFDLENBQUE7QUFDNUQsc0JBQXVCLDBCQUEwQixDQUFDLENBQUE7QUFDbEQsc0JBQWlDLHVCQUF1QixDQUFDLENBQUE7QUFHekQ7SUFFSSxzQkFDWSxzQkFBMEMsRUFDMUMsV0FBNkI7UUFEN0IsMkJBQXNCLEdBQXRCLHNCQUFzQixDQUFvQjtRQUMxQyxnQkFBVyxHQUFYLFdBQVcsQ0FBa0I7SUFDekMsQ0FBQztJQUVNLDRCQUFLLEdBQVosVUFBYSxRQUFnQixFQUFFLFFBQWdCO1FBQS9DLGlCQU9DO1FBTkcsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXO2FBQ2xCLEtBQUssQ0FBQyxVQUFVLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxjQUFNLENBQUMsU0FBUyxDQUFDO2FBQ3ZELEdBQUcsQ0FBQyxVQUFBLEdBQUc7WUFDSixLQUFJLENBQUMsc0JBQXNCLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDbEUsQ0FBQyxDQUFDLENBQUM7SUFFWCxDQUFDO0lBZkw7UUFBQyxpQkFBVSxFQUFFOztvQkFBQTtJQWdCYixtQkFBQztBQUFELENBZkEsQUFlQyxJQUFBO0FBZlksb0JBQVksZUFleEIsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9sb2dpbi9sb2dpblNlcnZpY2UvbG9naW4uc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgQWNjZXNzVG9rZW5TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vY29yZS9pbmRleCc7XHJcbmltcG9ydCB7IENvbmZpZyB9IGZyb20gJy4uLy4uLy4uLy4uL3NoYXJlZC9pbmRleCc7XHJcbmltcG9ydCB7IEFwaUNsaWVudFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi9hcGkvaW5kZXgnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgTG9naW5TZXJ2aWNlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwcml2YXRlIF9hdXRoZW50aWNhdGlvblNlcnZpY2U6IEFjY2Vzc1Rva2VuU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIF9hcGlTZXJ2aWNlOiBBcGlDbGllbnRTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGxvZ2luKHVzZXJuYW1lOiBzdHJpbmcsIHBhc3N3b3JkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPHZvaWQ+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fYXBpU2VydmljZVxyXG4gICAgICAgICAgICAudG9rZW4oJ3Bhc3N3b3JkJywgdXNlcm5hbWUsIHBhc3N3b3JkLCBDb25maWcuQ0xJRU5UX0lEKVxyXG4gICAgICAgICAgICAubWFwKHJlcyA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9hdXRoZW50aWNhdGlvblNlcnZpY2Uuc2F2ZUFjY2Vzc1Rva2VuKHJlcy52YWx1ZSwgZmFsc2UpO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICB9XHJcbn1cclxuIl19
