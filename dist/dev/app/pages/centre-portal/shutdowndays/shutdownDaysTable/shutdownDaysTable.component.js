"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var angular2_toaster_1 = require('angular2-toaster');
var shutdownDays_service_1 = require('../shutdownDaysService/shutdownDays.service');
var index_1 = require('../../../../core/index');
var index_2 = require('../../../../shared/index');
var index_3 = require('../../../../api/index');
var moment = require('moment/moment');
var ShutdownDaysTableComponent = (function () {
    function ShutdownDaysTableComponent(_ShutdownDaysService, _userService, _toasterService) {
        this._ShutdownDaysService = _ShutdownDaysService;
        this._userService = _userService;
        this._toasterService = _toasterService;
        this.minDate = new Date();
        this.addMode = false;
        this.disableSave = false;
        this.disableAddEdit = false;
        this.editRef = null;
    }
    ShutdownDaysTableComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._ShutdownDaysService
            .GetCentreShutdownDays$(this._userService.SelectedCentre.id)
            .subscribe(function (shutdownDaysRes) {
            _this.shutdownDays = shutdownDaysRes;
        });
    };
    ShutdownDaysTableComponent.prototype.addShutdownDay = function () {
        this.addMode = true;
        this.disableAddEdit = true;
        var newShutdownDay = new index_3.ShutdownDayDto();
        newShutdownDay.centreId = this._userService.SelectedCentre.id;
        this.shutdownDays.push(newShutdownDay);
        this.editRef = this.shutdownDays.length - 1;
    };
    ShutdownDaysTableComponent.prototype.formatIsoDate = function (date) {
        return moment(date).utc().format('DD/MM/YYYY');
    };
    ShutdownDaysTableComponent.prototype.updateShutdownDay = function (shutdownDay) {
        var _this = this;
        if (shutdownDay.date instanceof Date) {
        }
        var savingToast = this._toasterService.pop(new index_2.SpinningToast());
        this.disableSave = true;
        this._ShutdownDaysService
            .CreateUpdateShutdownDay$(shutdownDay)
            .subscribe(function (res) {
            _this.resetEditingProperties();
            shutdownDay.id = res.id;
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this._toasterService.popAsync('success', 'Saved', '');
        }, function (err) { return _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId); });
        this.disableSave = false;
    };
    ShutdownDaysTableComponent.prototype.deleteShutdownDay = function (shutdownDay) {
        var _this = this;
        if (window.confirm('Are you sure you want to remove the shutdown day?')) {
            var savingToast_1 = this._toasterService.pop(new index_2.SpinningToast());
            this._ShutdownDaysService
                .DeleteShutdownDay$(shutdownDay.centreId, shutdownDay.id)
                .subscribe(function (res) {
                var index = _this.shutdownDays.indexOf(shutdownDay);
                _this.shutdownDays.splice(index, 1);
                _this._toasterService.clear(savingToast_1.toastId, savingToast_1.toastContainerId);
                _this._toasterService.popAsync('success', 'Deleted', '');
            }, function (err) { return _this._toasterService.clear(savingToast_1.toastId, savingToast_1.toastContainerId); });
        }
    };
    ShutdownDaysTableComponent.prototype.editShutdownDay = function (shutdownDayIdx, shutdownDay) {
        this.editRef = shutdownDayIdx;
        this.disableAddEdit = true;
        this.preEditedDescription = shutdownDay.description;
        this.preEditedDate = shutdownDay.date;
    };
    ShutdownDaysTableComponent.prototype.cancelEdit = function (shutdownDay) {
        if (this.addMode) {
            var index = this.shutdownDays.indexOf(shutdownDay);
            this.shutdownDays.splice(index, 1);
        }
        else {
            shutdownDay.description = this.preEditedDescription;
            shutdownDay.date = this.preEditedDate;
        }
        this.resetEditingProperties();
    };
    ShutdownDaysTableComponent.prototype.editingCurrentRow = function (shutdownDayIdx) {
        if (shutdownDayIdx === this.editRef) {
            return true;
        }
        return false;
    };
    ShutdownDaysTableComponent.prototype.resetEditingProperties = function () {
        this.editRef = null;
        this.preEditedDescription = null;
        this.preEditedDate = null;
        this.addMode = false;
        this.disableAddEdit = false;
        this.disableSave = false;
    };
    ShutdownDaysTableComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'shutdowndays-table-cmp',
            templateUrl: './shutdownDaysTable.component.html'
        }), 
        __metadata('design:paramtypes', [shutdownDays_service_1.ShutdownDaysService, index_1.CentreUserService, angular2_toaster_1.ToasterService])
    ], ShutdownDaysTableComponent);
    return ShutdownDaysTableComponent;
}());
exports.ShutdownDaysTableComponent = ShutdownDaysTableComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3NodXRkb3duZGF5cy9zaHV0ZG93bkRheXNUYWJsZS9zaHV0ZG93bkRheXNUYWJsZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUFrQyxlQUFlLENBQUMsQ0FBQTtBQUVsRCxpQ0FBK0Isa0JBQWtCLENBQUMsQ0FBQTtBQUNsRCxxQ0FBb0MsNkNBQTZDLENBQUMsQ0FBQTtBQUNsRixzQkFBa0Msd0JBQXdCLENBQUMsQ0FBQTtBQUMzRCxzQkFBOEIsMEJBQTBCLENBQUMsQ0FBQTtBQUN6RCxzQkFBMkQsdUJBQXVCLENBQUMsQ0FBQTtBQUNuRixJQUFZLE1BQU0sV0FBTSxlQUFlLENBQUMsQ0FBQTtBQU94QztJQWNJLG9DQUFvQixvQkFBeUMsRUFDakQsWUFBK0IsRUFDL0IsZUFBK0I7UUFGdkIseUJBQW9CLEdBQXBCLG9CQUFvQixDQUFxQjtRQUNqRCxpQkFBWSxHQUFaLFlBQVksQ0FBbUI7UUFDL0Isb0JBQWUsR0FBZixlQUFlLENBQWdCO1FBZHBDLFlBQU8sR0FBUyxJQUFJLElBQUksRUFBRSxDQUFDO1FBRzFCLFlBQU8sR0FBWSxLQUFLLENBQUM7UUFDekIsZ0JBQVcsR0FBWSxLQUFLLENBQUM7UUFDN0IsbUJBQWMsR0FBWSxLQUFLLENBQUM7UUFDaEMsWUFBTyxHQUFZLElBQUksQ0FBQztJQVNoQyxDQUFDO0lBRUQsNkNBQVEsR0FBUjtRQUFBLGlCQU1DO1FBTEcsSUFBSSxDQUFDLG9CQUFvQjthQUNwQixzQkFBc0IsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxFQUFFLENBQUM7YUFDM0QsU0FBUyxDQUFDLFVBQUEsZUFBZTtZQUN0QixLQUFJLENBQUMsWUFBWSxHQUFHLGVBQWUsQ0FBQztRQUN4QyxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFRCxtREFBYyxHQUFkO1FBRUksSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDcEIsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7UUFHM0IsSUFBSSxjQUFjLEdBQUcsSUFBSSxzQkFBYyxFQUFFLENBQUM7UUFDMUMsY0FBYyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxFQUFFLENBQUM7UUFDOUQsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7UUFHdkMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUVELGtEQUFhLEdBQWIsVUFBYyxJQUFVO1FBQ3BCLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ25ELENBQUM7SUFFRCxzREFBaUIsR0FBakIsVUFBa0IsV0FBdUM7UUFBekQsaUJBdUJDO1FBdEJHLEVBQUUsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxJQUFJLFlBQVksSUFBSSxDQUFDLENBQUMsQ0FBQztRQU92QyxDQUFDO1FBQ0QsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsSUFBSSxxQkFBYSxFQUFFLENBQUMsQ0FBQztRQUNoRSxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztRQUN4QixJQUFJLENBQUMsb0JBQW9CO2FBQ3BCLHdCQUF3QixDQUFDLFdBQVcsQ0FBQzthQUNyQyxTQUFTLENBQ1YsVUFBQSxHQUFHO1lBRUMsS0FBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7WUFDOUIsV0FBVyxDQUFDLEVBQUUsR0FBRyxHQUFHLENBQUMsRUFBRSxDQUFDO1lBQ3hCLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDOUUsS0FBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLE9BQU8sRUFBRSxFQUFFLENBQUMsQ0FBQztRQUMxRCxDQUFDLEVBQ0QsVUFBQyxHQUFHLElBQUssT0FBQSxLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUE3RSxDQUE2RSxDQUFDLENBQUM7UUFDNUYsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7SUFDN0IsQ0FBQztJQUVELHNEQUFpQixHQUFqQixVQUFrQixXQUEyQjtRQUE3QyxpQkFjQztRQWJHLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsbURBQW1ELENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFdEUsSUFBSSxhQUFXLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsSUFBSSxxQkFBYSxFQUFFLENBQUMsQ0FBQztZQUNoRSxJQUFJLENBQUMsb0JBQW9CO2lCQUNwQixrQkFBa0IsQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFLFdBQVcsQ0FBQyxFQUFFLENBQUM7aUJBQ3hELFNBQVMsQ0FBQyxVQUFBLEdBQUc7Z0JBQ1YsSUFBSSxLQUFLLEdBQUcsS0FBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ25ELEtBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDbkMsS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsYUFBVyxDQUFDLE9BQU8sRUFBRSxhQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztnQkFDOUUsS0FBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLFNBQVMsRUFBRSxFQUFFLENBQUMsQ0FBQztZQUM1RCxDQUFDLEVBQ0QsVUFBQyxHQUFHLElBQUssT0FBQSxLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxhQUFXLENBQUMsT0FBTyxFQUFFLGFBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUE3RSxDQUE2RSxDQUFDLENBQUM7UUFDaEcsQ0FBQztJQUNMLENBQUM7SUFFRCxvREFBZSxHQUFmLFVBQWdCLGNBQXNCLEVBQUUsV0FBMkI7UUFDL0QsSUFBSSxDQUFDLE9BQU8sR0FBRyxjQUFjLENBQUM7UUFDOUIsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7UUFHM0IsSUFBSSxDQUFDLG9CQUFvQixHQUFHLFdBQVcsQ0FBQyxXQUFXLENBQUM7UUFDcEQsSUFBSSxDQUFDLGFBQWEsR0FBRyxXQUFXLENBQUMsSUFBSSxDQUFDO0lBQzFDLENBQUM7SUFFRCwrQ0FBVSxHQUFWLFVBQVcsV0FBMkI7UUFDbEMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDZixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUNuRCxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDdkMsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBRUosV0FBVyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUM7WUFDcEQsV0FBVyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDO1FBQzFDLENBQUM7UUFDRCxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztJQUNsQyxDQUFDO0lBRUQsc0RBQWlCLEdBQWpCLFVBQWtCLGNBQXNCO1FBQ3BDLEVBQUUsQ0FBQyxDQUFDLGNBQWMsS0FBSyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUNsQyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFDRCxNQUFNLENBQUMsS0FBSyxDQUFDO0lBQ2pCLENBQUM7SUFFRCwyREFBc0IsR0FBdEI7UUFFSSxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUNwQixJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDO1FBQ2pDLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1FBQzFCLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ3JCLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1FBQzVCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO0lBQzdCLENBQUM7SUEvSEw7UUFBQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSx3QkFBd0I7WUFDbEMsV0FBVyxFQUFFLG9DQUFvQztTQUNwRCxDQUFDOztrQ0FBQTtJQTRIRixpQ0FBQztBQUFELENBM0hBLEFBMkhDLElBQUE7QUEzSFksa0NBQTBCLDZCQTJIdEMsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9zaHV0ZG93bmRheXMvc2h1dGRvd25EYXlzVGFibGUvc2h1dGRvd25EYXlzVGFibGUuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmltcG9ydCB7IFRvYXN0ZXJTZXJ2aWNlIH0gZnJvbSAnYW5ndWxhcjItdG9hc3Rlcic7XHJcbmltcG9ydCB7IFNodXRkb3duRGF5c1NlcnZpY2UgfSBmcm9tICcuLi9zaHV0ZG93bkRheXNTZXJ2aWNlL3NodXRkb3duRGF5cy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQ2VudHJlVXNlclNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi9jb3JlL2luZGV4JztcclxuaW1wb3J0IHsgU3Bpbm5pbmdUb2FzdCB9IGZyb20gJy4uLy4uLy4uLy4uL3NoYXJlZC9pbmRleCc7XHJcbmltcG9ydCB7IFNodXRkb3duRGF5RHRvLCBDcmVhdGVVcGRhdGVTaHV0ZG93bkRheUR0byB9IGZyb20gJy4uLy4uLy4uLy4uL2FwaS9pbmRleCc7XHJcbmltcG9ydCAqIGFzIG1vbWVudCBmcm9tICdtb21lbnQvbW9tZW50JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHNlbGVjdG9yOiAnc2h1dGRvd25kYXlzLXRhYmxlLWNtcCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vc2h1dGRvd25EYXlzVGFibGUuY29tcG9uZW50Lmh0bWwnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTaHV0ZG93bkRheXNUYWJsZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgcHVibGljIG1pbkRhdGU6IERhdGUgPSBuZXcgRGF0ZSgpO1xyXG5cclxuICAgIHByaXZhdGUgc2h1dGRvd25EYXlzOiBTaHV0ZG93bkRheUR0b1tdO1xyXG4gICAgcHJpdmF0ZSBhZGRNb2RlOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBwcml2YXRlIGRpc2FibGVTYXZlOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBwcml2YXRlIGRpc2FibGVBZGRFZGl0OiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBwcml2YXRlIGVkaXRSZWY/OiBudW1iZXIgPSBudWxsO1xyXG5cclxuICAgIC8vIFN0b3JlcyB2YWx1ZXMgYmVmb3JlIGVkaXQgc28gd2UgY2FuIHJvbGxiYWNrIG9uIGVycm9yXHJcbiAgICBwcml2YXRlIHByZUVkaXRlZERlc2NyaXB0aW9uOiBzdHJpbmc7XHJcbiAgICBwcml2YXRlIHByZUVkaXRlZERhdGU6IERhdGU7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfU2h1dGRvd25EYXlzU2VydmljZTogU2h1dGRvd25EYXlzU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIF91c2VyU2VydmljZTogQ2VudHJlVXNlclNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBfdG9hc3RlclNlcnZpY2U6IFRvYXN0ZXJTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5fU2h1dGRvd25EYXlzU2VydmljZVxyXG4gICAgICAgICAgICAuR2V0Q2VudHJlU2h1dGRvd25EYXlzJCh0aGlzLl91c2VyU2VydmljZS5TZWxlY3RlZENlbnRyZS5pZClcclxuICAgICAgICAgICAgLnN1YnNjcmliZShzaHV0ZG93bkRheXNSZXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zaHV0ZG93bkRheXMgPSBzaHV0ZG93bkRheXNSZXM7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGFkZFNodXRkb3duRGF5KCkge1xyXG4gICAgICAgIC8vIFNldCBhZGQgbW9kZSBhbmQgcm93XHJcbiAgICAgICAgdGhpcy5hZGRNb2RlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmRpc2FibGVBZGRFZGl0ID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgLy8gTmV3IHVwIHNodXRkb3duIGRheVxyXG4gICAgICAgIHZhciBuZXdTaHV0ZG93bkRheSA9IG5ldyBTaHV0ZG93bkRheUR0bygpO1xyXG4gICAgICAgIG5ld1NodXRkb3duRGF5LmNlbnRyZUlkID0gdGhpcy5fdXNlclNlcnZpY2UuU2VsZWN0ZWRDZW50cmUuaWQ7XHJcbiAgICAgICAgdGhpcy5zaHV0ZG93bkRheXMucHVzaChuZXdTaHV0ZG93bkRheSk7XHJcblxyXG4gICAgICAgIC8vIHNldCB0aGUgZWRpdGluZyBpbmRleCB0byB0aGUgbGFzdCByb3cgaW4gdGhlIHRhYmxlXHJcbiAgICAgICAgdGhpcy5lZGl0UmVmID0gdGhpcy5zaHV0ZG93bkRheXMubGVuZ3RoIC0gMTtcclxuICAgIH1cclxuXHJcbiAgICBmb3JtYXRJc29EYXRlKGRhdGU6IERhdGUpIDogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gbW9tZW50KGRhdGUpLnV0YygpLmZvcm1hdCgnREQvTU0vWVlZWScpO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZVNodXRkb3duRGF5KHNodXRkb3duRGF5OiBDcmVhdGVVcGRhdGVTaHV0ZG93bkRheUR0bykge1xyXG4gICAgICAgIGlmIChzaHV0ZG93bkRheS5kYXRlIGluc3RhbmNlb2YgRGF0ZSkge1xyXG4gICAgICAgICAgICAvLyBjb252ZXJ0IHRvIHV0YyB0byBmaXggaXNzdWUgd2hlcmUgZGF0ZSBpcyBhbHdheXMgeWVzdGVyZGF5LlxyXG4gICAgICAgICAgICAvL3ZhciB1dGNEYXRlID0gbmV3IERhdGUoRGF0ZS5VVEMoc2h1dGRvd25EYXkuZGF0ZS5nZXRGdWxsWWVhcigpLFxyXG4gICAgICAgICAgICAgICAgLy9zaHV0ZG93bkRheS5kYXRlLmdldE1vbnRoKCksIHNodXRkb3duRGF5LmRhdGUuZ2V0RGF0ZSgpLCAwLCAwLCAwLCAwKSk7XHJcbiAgICAgICAgICAgIC8vc2h1dGRvd25EYXkuZGF0ZSA9IHV0Y0RhdGU7XHJcbiAgICAgICAgICAgIC8vc2h1dGRvd25EYXkuZGF0ZSA9XHJcbiAgICAgICAgICAgIC8vc2h1dGRvd25EYXkuZGF0ZSA9IG1vbWVudChzaHV0ZG93bkRheS5kYXRlKS51dGMoKS50b1N0cmluZygpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBsZXQgc2F2aW5nVG9hc3QgPSB0aGlzLl90b2FzdGVyU2VydmljZS5wb3AobmV3IFNwaW5uaW5nVG9hc3QoKSk7XHJcbiAgICAgICAgdGhpcy5kaXNhYmxlU2F2ZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5fU2h1dGRvd25EYXlzU2VydmljZVxyXG4gICAgICAgICAgICAuQ3JlYXRlVXBkYXRlU2h1dGRvd25EYXkkKHNodXRkb3duRGF5KVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICByZXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgLy8gcmVzZXQgZWRpdCBpbmRleHMgYW5kIHByZUVkaXRlZFNodXRkb3duRGF5XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJlc2V0RWRpdGluZ1Byb3BlcnRpZXMoKTtcclxuICAgICAgICAgICAgICAgIHNodXRkb3duRGF5LmlkID0gcmVzLmlkO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoc2F2aW5nVG9hc3QudG9hc3RJZCwgc2F2aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5wb3BBc3luYygnc3VjY2VzcycsICdTYXZlZCcsICcnKTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgKGVycikgPT4gdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoc2F2aW5nVG9hc3QudG9hc3RJZCwgc2F2aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCkpO1xyXG4gICAgICAgIHRoaXMuZGlzYWJsZVNhdmUgPSBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBkZWxldGVTaHV0ZG93bkRheShzaHV0ZG93bkRheTogU2h1dGRvd25EYXlEdG8pIHtcclxuICAgICAgICBpZiAod2luZG93LmNvbmZpcm0oJ0FyZSB5b3Ugc3VyZSB5b3Ugd2FudCB0byByZW1vdmUgdGhlIHNodXRkb3duIGRheT8nKSkge1xyXG5cclxuICAgICAgICAgICAgbGV0IHNhdmluZ1RvYXN0ID0gdGhpcy5fdG9hc3RlclNlcnZpY2UucG9wKG5ldyBTcGlubmluZ1RvYXN0KCkpO1xyXG4gICAgICAgICAgICB0aGlzLl9TaHV0ZG93bkRheXNTZXJ2aWNlXHJcbiAgICAgICAgICAgICAgICAuRGVsZXRlU2h1dGRvd25EYXkkKHNodXRkb3duRGF5LmNlbnRyZUlkLCBzaHV0ZG93bkRheS5pZClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB2YXIgaW5kZXggPSB0aGlzLnNodXRkb3duRGF5cy5pbmRleE9mKHNodXRkb3duRGF5KTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNodXRkb3duRGF5cy5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLmNsZWFyKHNhdmluZ1RvYXN0LnRvYXN0SWQsIHNhdmluZ1RvYXN0LnRvYXN0Q29udGFpbmVySWQpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLnBvcEFzeW5jKCdzdWNjZXNzJywgJ0RlbGV0ZWQnLCAnJyk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgKGVycikgPT4gdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoc2F2aW5nVG9hc3QudG9hc3RJZCwgc2F2aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCkpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBlZGl0U2h1dGRvd25EYXkoc2h1dGRvd25EYXlJZHg6IG51bWJlciwgc2h1dGRvd25EYXk6IFNodXRkb3duRGF5RHRvKSB7XHJcbiAgICAgICAgdGhpcy5lZGl0UmVmID0gc2h1dGRvd25EYXlJZHg7XHJcbiAgICAgICAgdGhpcy5kaXNhYmxlQWRkRWRpdCA9IHRydWU7XHJcblxyXG4gICAgICAgIC8vIHN0b3JlIHByZS1lZGl0ZWQgc2h1dGRvd24gZGF5XHJcbiAgICAgICAgdGhpcy5wcmVFZGl0ZWREZXNjcmlwdGlvbiA9IHNodXRkb3duRGF5LmRlc2NyaXB0aW9uO1xyXG4gICAgICAgIHRoaXMucHJlRWRpdGVkRGF0ZSA9IHNodXRkb3duRGF5LmRhdGU7XHJcbiAgICB9XHJcblxyXG4gICAgY2FuY2VsRWRpdChzaHV0ZG93bkRheTogU2h1dGRvd25EYXlEdG8pIHtcclxuICAgICAgICBpZiAodGhpcy5hZGRNb2RlKSB7XHJcbiAgICAgICAgICAgIHZhciBpbmRleCA9IHRoaXMuc2h1dGRvd25EYXlzLmluZGV4T2Yoc2h1dGRvd25EYXkpO1xyXG4gICAgICAgICAgICB0aGlzLnNodXRkb3duRGF5cy5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIC8vIEVkaXRpbmcgZXhpc3Rpbmcgc2h1dGRvd24gZGF5LCByb2xsYmFjayBzaHV0ZG93biBkYXkgZWRpdFxyXG4gICAgICAgICAgICBzaHV0ZG93bkRheS5kZXNjcmlwdGlvbiA9IHRoaXMucHJlRWRpdGVkRGVzY3JpcHRpb247XHJcbiAgICAgICAgICAgIHNodXRkb3duRGF5LmRhdGUgPSB0aGlzLnByZUVkaXRlZERhdGU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMucmVzZXRFZGl0aW5nUHJvcGVydGllcygpO1xyXG4gICAgfVxyXG5cclxuICAgIGVkaXRpbmdDdXJyZW50Um93KHNodXRkb3duRGF5SWR4OiBudW1iZXIpOiBib29sZWFuIHtcclxuICAgICAgICBpZiAoc2h1dGRvd25EYXlJZHggPT09IHRoaXMuZWRpdFJlZikge1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIHJlc2V0RWRpdGluZ1Byb3BlcnRpZXMoKSB7XHJcbiAgICAgICAgLy8gUmVzZXQgZWRpdGluZyBwcm9wZXJ0aWVzXHJcbiAgICAgICAgdGhpcy5lZGl0UmVmID0gbnVsbDtcclxuICAgICAgICB0aGlzLnByZUVkaXRlZERlc2NyaXB0aW9uID0gbnVsbDtcclxuICAgICAgICB0aGlzLnByZUVkaXRlZERhdGUgPSBudWxsO1xyXG4gICAgICAgIHRoaXMuYWRkTW9kZSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuZGlzYWJsZUFkZEVkaXQgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLmRpc2FibGVTYXZlID0gZmFsc2U7XHJcbiAgICB9XHJcbn1cclxuIl19
