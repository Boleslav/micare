"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ShutdownDaysComponent = (function () {
    function ShutdownDaysComponent() {
    }
    ShutdownDaysComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'shutdowndays-cmp',
            templateUrl: './shutdowndays.component.html'
        }), 
        __metadata('design:paramtypes', [])
    ], ShutdownDaysComponent);
    return ShutdownDaysComponent;
}());
exports.ShutdownDaysComponent = ShutdownDaysComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3NodXRkb3duZGF5cy9zaHV0ZG93bmRheXMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBd0IsZUFBZSxDQUFDLENBQUE7QUFReEM7SUFBQTtJQUFvQyxDQUFDO0lBTnJDO1FBQUMsZ0JBQVMsQ0FBQztZQUNWLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNoQixRQUFRLEVBQUUsa0JBQWtCO1lBQzVCLFdBQVcsRUFBRSwrQkFBK0I7U0FDL0MsQ0FBQzs7NkJBQUE7SUFFa0MsNEJBQUM7QUFBRCxDQUFwQyxBQUFxQyxJQUFBO0FBQXhCLDZCQUFxQix3QkFBRyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3NodXRkb3duZGF5cy9zaHV0ZG93bmRheXMuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcblx0bW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHNlbGVjdG9yOiAnc2h1dGRvd25kYXlzLWNtcCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vc2h1dGRvd25kYXlzLmNvbXBvbmVudC5odG1sJ1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFNodXRkb3duRGF5c0NvbXBvbmVudCB7fVxyXG4iXX0=
