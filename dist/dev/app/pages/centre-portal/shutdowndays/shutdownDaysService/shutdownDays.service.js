"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../../api/index');
var ShutdownDaysService = (function () {
    function ShutdownDaysService(_apiService) {
        this._apiService = _apiService;
    }
    ShutdownDaysService.prototype.CreateUpdateShutdownDay$ = function (shutdownDayDto) {
        return this._apiService.shutdownDay_CreateUpdateShutdownDay(shutdownDayDto);
    };
    ShutdownDaysService.prototype.GetCentreShutdownDays$ = function (centreId) {
        return this._apiService.shutdownDay_GetShutdownDays(centreId);
    };
    ShutdownDaysService.prototype.DeleteShutdownDay$ = function (centreId, shutdownDayId) {
        return this._apiService.shutdownDay_DeleteShutdownDay(centreId, shutdownDayId);
    };
    ShutdownDaysService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.ApiClientService])
    ], ShutdownDaysService);
    return ShutdownDaysService;
}());
exports.ShutdownDaysService = ShutdownDaysService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3NodXRkb3duZGF5cy9zaHV0ZG93bkRheXNTZXJ2aWNlL3NodXRkb3duRGF5cy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBMkIsZUFBZSxDQUFDLENBQUE7QUFHM0Msc0JBQTZFLHVCQUF1QixDQUFDLENBQUE7QUFHckc7SUFFSSw2QkFBb0IsV0FBNkI7UUFBN0IsZ0JBQVcsR0FBWCxXQUFXLENBQWtCO0lBRWpELENBQUM7SUFFTSxzREFBd0IsR0FBL0IsVUFBZ0MsY0FBMEM7UUFFdEUsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsbUNBQW1DLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDaEYsQ0FBQztJQUVNLG9EQUFzQixHQUE3QixVQUE4QixRQUFnQjtRQUMxQyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQywyQkFBMkIsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUNsRSxDQUFDO0lBRU0sZ0RBQWtCLEdBQXpCLFVBQTBCLFFBQWdCLEVBQUUsYUFBcUI7UUFDN0QsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsNkJBQTZCLENBQUMsUUFBUSxFQUFFLGFBQWEsQ0FBQyxDQUFDO0lBQ25GLENBQUM7SUFsQkw7UUFBQyxpQkFBVSxFQUFFOzsyQkFBQTtJQW1CYiwwQkFBQztBQUFELENBbEJBLEFBa0JDLElBQUE7QUFsQlksMkJBQW1CLHNCQWtCL0IsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9zaHV0ZG93bmRheXMvc2h1dGRvd25EYXlzU2VydmljZS9zaHV0ZG93bkRheXMuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgQXBpQ2xpZW50U2VydmljZSwgQ3JlYXRlVXBkYXRlU2h1dGRvd25EYXlEdG8sIFNodXRkb3duRGF5RHRvIH0gZnJvbSAnLi4vLi4vLi4vLi4vYXBpL2luZGV4JztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIFNodXRkb3duRGF5c1NlcnZpY2Uge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX2FwaVNlcnZpY2U6IEFwaUNsaWVudFNlcnZpY2UpIHtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIENyZWF0ZVVwZGF0ZVNodXRkb3duRGF5JChzaHV0ZG93bkRheUR0bzogQ3JlYXRlVXBkYXRlU2h1dGRvd25EYXlEdG8pOiBPYnNlcnZhYmxlPFNodXRkb3duRGF5RHRvPiB7XHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLl9hcGlTZXJ2aWNlLnNodXRkb3duRGF5X0NyZWF0ZVVwZGF0ZVNodXRkb3duRGF5KHNodXRkb3duRGF5RHRvKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgR2V0Q2VudHJlU2h1dGRvd25EYXlzJChjZW50cmVJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxTaHV0ZG93bkRheUR0b1tdPiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2FwaVNlcnZpY2Uuc2h1dGRvd25EYXlfR2V0U2h1dGRvd25EYXlzKGNlbnRyZUlkKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgRGVsZXRlU2h1dGRvd25EYXkkKGNlbnRyZUlkOiBzdHJpbmcsIHNodXRkb3duRGF5SWQ6IHN0cmluZyk6IE9ic2VydmFibGU8dm9pZD4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9hcGlTZXJ2aWNlLnNodXRkb3duRGF5X0RlbGV0ZVNodXRkb3duRGF5KGNlbnRyZUlkLCBzaHV0ZG93bkRheUlkKTtcclxuICAgIH1cclxufVxyXG4iXX0=
