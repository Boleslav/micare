"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../shared/index');
var index_2 = require('./index');
var ShutdownDaysModule = (function () {
    function ShutdownDaysModule() {
    }
    ShutdownDaysModule = __decorate([
        core_1.NgModule({
            imports: [
                index_1.SharedModule
            ],
            declarations: [index_2.ShutdownDaysComponent, index_2.ShutdownDaysTableComponent],
            providers: [index_2.ShutdownDaysService],
            exports: [index_2.ShutdownDaysComponent, index_2.ShutdownDaysTableComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], ShutdownDaysModule);
    return ShutdownDaysModule;
}());
exports.ShutdownDaysModule = ShutdownDaysModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3NodXRkb3duZGF5cy9zaHV0ZG93bmRheXMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBeUIsZUFBZSxDQUFDLENBQUE7QUFDekMsc0JBQTZCLHVCQUF1QixDQUFDLENBQUE7QUFDckQsc0JBQXVGLFNBQVMsQ0FBQyxDQUFBO0FBV2pHO0lBQUE7SUFBa0MsQ0FBQztJQVRuQztRQUFDLGVBQVEsQ0FBQztZQUNOLE9BQU8sRUFBRTtnQkFDTCxvQkFBWTthQUNmO1lBQ0QsWUFBWSxFQUFFLENBQUMsNkJBQXFCLEVBQUUsa0NBQTBCLENBQUM7WUFDakUsU0FBUyxFQUFFLENBQUMsMkJBQW1CLENBQUM7WUFDaEMsT0FBTyxFQUFFLENBQUMsNkJBQXFCLEVBQUUsa0NBQTBCLENBQUM7U0FDL0QsQ0FBQzs7MEJBQUE7SUFFZ0MseUJBQUM7QUFBRCxDQUFsQyxBQUFtQyxJQUFBO0FBQXRCLDBCQUFrQixxQkFBSSxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3NodXRkb3duZGF5cy9zaHV0ZG93bmRheXMubW9kdWxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU2hhcmVkTW9kdWxlIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL2luZGV4JztcclxuaW1wb3J0IHsgU2h1dGRvd25EYXlzQ29tcG9uZW50LCBTaHV0ZG93bkRheXNUYWJsZUNvbXBvbmVudCwgU2h1dGRvd25EYXlzU2VydmljZSB9IGZyb20gJy4vaW5kZXgnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBTaGFyZWRNb2R1bGVcclxuICAgIF0sXHJcbiAgICBkZWNsYXJhdGlvbnM6IFtTaHV0ZG93bkRheXNDb21wb25lbnQsIFNodXRkb3duRGF5c1RhYmxlQ29tcG9uZW50XSxcclxuICAgIHByb3ZpZGVyczogW1NodXRkb3duRGF5c1NlcnZpY2VdLFxyXG4gICAgZXhwb3J0czogW1NodXRkb3duRGF5c0NvbXBvbmVudCwgU2h1dGRvd25EYXlzVGFibGVDb21wb25lbnRdXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgU2h1dGRvd25EYXlzTW9kdWxlIHsgfVxyXG4iXX0=
