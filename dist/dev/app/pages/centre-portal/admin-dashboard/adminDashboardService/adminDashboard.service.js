"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../../api/index');
var AdminDashboardService = (function () {
    function AdminDashboardService(_apiClient) {
        this._apiClient = _apiClient;
    }
    AdminDashboardService.prototype.getTotalCentresCount$ = function () {
        return this._apiClient.sysAdminDashboard_GetTotalCentreCount();
    };
    AdminDashboardService.prototype.getTotalCentresAwaitingApprovalCount$ = function () {
        return this._apiClient.sysAdminDashboard_GetTotalCentresAwaitingApproval();
    };
    AdminDashboardService.prototype.getTotalIncome$ = function () {
        return this._apiClient.sysAdminDashboard_GetTotalIncome();
    };
    AdminDashboardService.prototype.getTotalCompletedSwaps$ = function () {
        return this._apiClient.sysAdminDashboard_GetTotalSwaps();
    };
    AdminDashboardService.prototype.getAdminDashboardData$ = function () {
        return this._apiClient.sysAdminDashboard_GetGraphDashboardData();
    };
    AdminDashboardService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.ApiClientService])
    ], AdminDashboardService);
    return AdminDashboardService;
}());
exports.AdminDashboardService = AdminDashboardService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2FkbWluLWRhc2hib2FyZC9hZG1pbkRhc2hib2FyZFNlcnZpY2UvYWRtaW5EYXNoYm9hcmQuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQTJCLGVBQWUsQ0FBQyxDQUFBO0FBRzNDLHNCQUE0RCx1QkFBdUIsQ0FBQyxDQUFBO0FBR3BGO0lBRUksK0JBQW9CLFVBQTRCO1FBQTVCLGVBQVUsR0FBVixVQUFVLENBQWtCO0lBQ2hELENBQUM7SUFFTSxxREFBcUIsR0FBNUI7UUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxxQ0FBcUMsRUFBRSxDQUFDO0lBRW5FLENBQUM7SUFFTSxxRUFBcUMsR0FBNUM7UUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpREFBaUQsRUFBRSxDQUFDO0lBQy9FLENBQUM7SUFFTSwrQ0FBZSxHQUF0QjtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGdDQUFnQyxFQUFFLENBQUM7SUFDOUQsQ0FBQztJQUVNLHVEQUF1QixHQUE5QjtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLCtCQUErQixFQUFFLENBQUM7SUFDN0QsQ0FBQztJQUVNLHNEQUFzQixHQUE3QjtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLHVDQUF1QyxFQUFFLENBQUM7SUFDckUsQ0FBQztJQXpCTDtRQUFDLGlCQUFVLEVBQUU7OzZCQUFBO0lBMEJiLDRCQUFDO0FBQUQsQ0F6QkEsQUF5QkMsSUFBQTtBQXpCWSw2QkFBcUIsd0JBeUJqQyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2FkbWluLWRhc2hib2FyZC9hZG1pbkRhc2hib2FyZFNlcnZpY2UvYWRtaW5EYXNoYm9hcmQuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgQXBpQ2xpZW50U2VydmljZSwgU3lzQWRtaW5EYXNoYm9hcmRHcmFwaER0byB9IGZyb20gJy4uLy4uLy4uLy4uL2FwaS9pbmRleCc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBBZG1pbkRhc2hib2FyZFNlcnZpY2Uge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX2FwaUNsaWVudDogQXBpQ2xpZW50U2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRUb3RhbENlbnRyZXNDb3VudCQoKTogT2JzZXJ2YWJsZTxudW1iZXI+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fYXBpQ2xpZW50LnN5c0FkbWluRGFzaGJvYXJkX0dldFRvdGFsQ2VudHJlQ291bnQoKTtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldFRvdGFsQ2VudHJlc0F3YWl0aW5nQXBwcm92YWxDb3VudCQoKTogT2JzZXJ2YWJsZTxudW1iZXI+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fYXBpQ2xpZW50LnN5c0FkbWluRGFzaGJvYXJkX0dldFRvdGFsQ2VudHJlc0F3YWl0aW5nQXBwcm92YWwoKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0VG90YWxJbmNvbWUkKCk6IE9ic2VydmFibGU8bnVtYmVyPiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2FwaUNsaWVudC5zeXNBZG1pbkRhc2hib2FyZF9HZXRUb3RhbEluY29tZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRUb3RhbENvbXBsZXRlZFN3YXBzJCgpOiBPYnNlcnZhYmxlPG51bWJlcj4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9hcGlDbGllbnQuc3lzQWRtaW5EYXNoYm9hcmRfR2V0VG90YWxTd2FwcygpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRBZG1pbkRhc2hib2FyZERhdGEkKCk6IE9ic2VydmFibGU8U3lzQWRtaW5EYXNoYm9hcmRHcmFwaER0bz4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9hcGlDbGllbnQuc3lzQWRtaW5EYXNoYm9hcmRfR2V0R3JhcGhEYXNoYm9hcmREYXRhKCk7XHJcbiAgICB9XHJcbn1cclxuIl19
