"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var adminDashboard_service_1 = require('./adminDashboardService/adminDashboard.service');
var index_1 = require('../../../core/index');
var AdminDashboardComponent = (function () {
    function AdminDashboardComponent(_userService, _adminDashboardService) {
        this._userService = _userService;
        this._adminDashboardService = _adminDashboardService;
        this.currentYear = new Date().getFullYear();
        this.totalCentres = null;
        this.totalCentreAwaitingApproval = null;
        this.totalIncome = null;
        this.totalMiSwaps = null;
        this.daysForSale = null;
        this.daysSold = null;
        this.totalCentresOverTime = null;
        this.totalStaffOverTime = null;
        this.totalParentsOverTime = null;
    }
    AdminDashboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._adminDashboardService
            .getTotalCentresCount$()
            .subscribe(function (res) { return _this.totalCentres = res; });
        this._adminDashboardService
            .getTotalCentresAwaitingApprovalCount$()
            .subscribe(function (res) { return _this.totalCentreAwaitingApproval = res; });
        this._adminDashboardService
            .getTotalIncome$()
            .subscribe(function (res) { return _this.totalIncome = res; });
        this._adminDashboardService
            .getTotalCompletedSwaps$()
            .subscribe(function (res) { return _this.totalMiSwaps = res; });
        this._adminDashboardService
            .getAdminDashboardData$()
            .subscribe(function (res) {
            _this.daysForSale = _this.mapGraphData(res.daysForSale);
            _this.daysSold = _this.mapGraphData(res.daysSold);
            _this.totalCentresOverTime = _this.mapGraphData(res.totalCentresOverTime);
            _this.totalStaffOverTime = _this.mapGraphData(res.totalStaffOverTime);
            _this.totalParentsOverTime = _this.mapGraphData(res.totalParentsOverTime);
            _this.initDaySalesChart();
            _this.initCentreSummaryChart();
        });
    };
    AdminDashboardComponent.prototype.mapGraphData = function (days) {
        var arr = new Array();
        for (var key in days) {
            if (days.hasOwnProperty(key)) {
                var day = [new Date(key).getTime(), days[key]];
                arr.push(day);
            }
        }
        return arr;
    };
    AdminDashboardComponent.prototype.initDaySalesChart = function () {
        var miSwaps = $('#miSwaps-table');
        miSwaps.highcharts({
            chart: {
                type: 'spline'
            },
            title: {
                text: 'MiCare ' + this.currentYear
            },
            subtitle: {
                text: 'Number of MiCare days'
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: {
                    month: '%e. %b',
                    year: '%b'
                },
                title: {
                    text: 'Date'
                }
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Total'
                },
                min: 0
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x:%e. %b}: {point.y}'
            },
            plotOptions: {
                spline: {
                    marker: {
                        enabled: true
                    }
                }
            },
            series: [{
                    name: 'To Give',
                    data: this.daysForSale
                }, {
                    name: 'Given',
                    data: this.daysSold
                }
            ]
        });
    };
    AdminDashboardComponent.prototype.initCentreSummaryChart = function () {
        var miSwaps = $('#miSwaps-summary-table');
        miSwaps.highcharts({
            chart: {
                type: 'spline'
            },
            title: {
                text: 'MiCare Summary ' + this.currentYear
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: {
                    month: '%e. %b',
                    year: '%b'
                },
                title: {
                    text: 'Date'
                }
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Total'
                },
                min: 0
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x:%e. %b}: {point.y}'
            },
            plotOptions: {
                spline: {
                    marker: {
                        enabled: true
                    }
                }
            },
            series: [{
                    name: 'MiCare Centres',
                    data: this.totalCentresOverTime
                },
                {
                    name: 'MiCare Staff',
                    data: this.totalStaffOverTime
                },
                {
                    name: 'MiCare Parents',
                    data: this.totalParentsOverTime
                }
            ]
        });
    };
    AdminDashboardComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'admin-dashboard-cmp',
            templateUrl: 'admin-dashboard.component.html'
        }), 
        __metadata('design:paramtypes', [index_1.CentreUserService, adminDashboard_service_1.AdminDashboardService])
    ], AdminDashboardComponent);
    return AdminDashboardComponent;
}());
exports.AdminDashboardComponent = AdminDashboardComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2FkbWluLWRhc2hib2FyZC9hZG1pbi1kYXNoYm9hcmQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBa0MsZUFBZSxDQUFDLENBQUE7QUFFbEQsdUNBQXNDLGdEQUFnRCxDQUFDLENBQUE7QUFDdkYsc0JBQWtDLHFCQUFxQixDQUFDLENBQUE7QUFReEQ7SUFnQkksaUNBQW9CLFlBQStCLEVBQ3ZDLHNCQUE2QztRQURyQyxpQkFBWSxHQUFaLFlBQVksQ0FBbUI7UUFDdkMsMkJBQXNCLEdBQXRCLHNCQUFzQixDQUF1QjtRQWZqRCxnQkFBVyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFLENBQUM7UUFFdkMsaUJBQVksR0FBVyxJQUFJLENBQUM7UUFDNUIsZ0NBQTJCLEdBQVcsSUFBSSxDQUFDO1FBQzNDLGdCQUFXLEdBQVcsSUFBSSxDQUFDO1FBQzNCLGlCQUFZLEdBQVcsSUFBSSxDQUFDO1FBRTVCLGdCQUFXLEdBQW9CLElBQUksQ0FBQztRQUNwQyxhQUFRLEdBQW9CLElBQUksQ0FBQztRQUVqQyx5QkFBb0IsR0FBb0IsSUFBSSxDQUFDO1FBQzdDLHVCQUFrQixHQUFvQixJQUFJLENBQUM7UUFDM0MseUJBQW9CLEdBQW9CLElBQUksQ0FBQztJQUlyRCxDQUFDO0lBRUQsMENBQVEsR0FBUjtRQUFBLGlCQTZCQztRQTVCRyxJQUFJLENBQUMsc0JBQXNCO2FBQ3RCLHFCQUFxQixFQUFFO2FBQ3ZCLFNBQVMsQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEtBQUksQ0FBQyxZQUFZLEdBQUcsR0FBRyxFQUF2QixDQUF1QixDQUFDLENBQUM7UUFFL0MsSUFBSSxDQUFDLHNCQUFzQjthQUN0QixxQ0FBcUMsRUFBRTthQUN2QyxTQUFTLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxLQUFJLENBQUMsMkJBQTJCLEdBQUcsR0FBRyxFQUF0QyxDQUFzQyxDQUFDLENBQUM7UUFFOUQsSUFBSSxDQUFDLHNCQUFzQjthQUN0QixlQUFlLEVBQUU7YUFDakIsU0FBUyxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsS0FBSSxDQUFDLFdBQVcsR0FBRyxHQUFHLEVBQXRCLENBQXNCLENBQUMsQ0FBQztRQUU5QyxJQUFJLENBQUMsc0JBQXNCO2FBQ3RCLHVCQUF1QixFQUFFO2FBQ3pCLFNBQVMsQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEtBQUksQ0FBQyxZQUFZLEdBQUcsR0FBRyxFQUF2QixDQUF1QixDQUFDLENBQUM7UUFHL0MsSUFBSSxDQUFDLHNCQUFzQjthQUN0QixzQkFBc0IsRUFBRTthQUN4QixTQUFTLENBQUMsVUFBQSxHQUFHO1lBQ1YsS0FBSSxDQUFDLFdBQVcsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUN0RCxLQUFJLENBQUMsUUFBUSxHQUFHLEtBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ2hELEtBQUksQ0FBQyxvQkFBb0IsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1lBQ3hFLEtBQUksQ0FBQyxrQkFBa0IsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1lBQ3BFLEtBQUksQ0FBQyxvQkFBb0IsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1lBQ3hFLEtBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1lBQ3pCLEtBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1FBQ2xDLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVPLDhDQUFZLEdBQXBCLFVBQXFCLElBQWdDO1FBQ2pELElBQUksR0FBRyxHQUFHLElBQUksS0FBSyxFQUFZLENBQUM7UUFDaEMsR0FBRyxDQUFDLENBQUMsSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNuQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDM0IsSUFBSSxHQUFHLEdBQWEsQ0FBQyxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLEVBQUUsRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDekQsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNsQixDQUFDO1FBQ0wsQ0FBQztRQUNELE1BQU0sQ0FBQyxHQUFHLENBQUM7SUFDZixDQUFDO0lBRU8sbURBQWlCLEdBQXpCO1FBQ0ksSUFBSSxPQUFPLEdBQVEsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDdkMsT0FBTyxDQUFDLFVBQVUsQ0FBQztZQUNmLEtBQUssRUFBRTtnQkFDSCxJQUFJLEVBQUUsUUFBUTthQUNqQjtZQUNELEtBQUssRUFBRTtnQkFDSCxJQUFJLEVBQUUsU0FBUyxHQUFHLElBQUksQ0FBQyxXQUFXO2FBQ3JDO1lBQ0QsUUFBUSxFQUFFO2dCQUNOLElBQUksRUFBRSx1QkFBdUI7YUFDaEM7WUFDRCxLQUFLLEVBQUU7Z0JBQ0gsSUFBSSxFQUFFLFVBQVU7Z0JBQ2hCLG9CQUFvQixFQUFFO29CQUNsQixLQUFLLEVBQUUsUUFBUTtvQkFDZixJQUFJLEVBQUUsSUFBSTtpQkFDYjtnQkFDRCxLQUFLLEVBQUU7b0JBQ0gsSUFBSSxFQUFFLE1BQU07aUJBQ2Y7YUFDSjtZQUNELEtBQUssRUFBRTtnQkFDSCxhQUFhLEVBQUUsS0FBSztnQkFDcEIsS0FBSyxFQUFFO29CQUNILElBQUksRUFBRSxPQUFPO2lCQUNoQjtnQkFDRCxHQUFHLEVBQUUsQ0FBQzthQUNUO1lBQ0QsT0FBTyxFQUFFO2dCQUNMLFlBQVksRUFBRSwwQkFBMEI7Z0JBQ3hDLFdBQVcsRUFBRSw2QkFBNkI7YUFDN0M7WUFFRCxXQUFXLEVBQUU7Z0JBQ1QsTUFBTSxFQUFFO29CQUNKLE1BQU0sRUFBRTt3QkFDSixPQUFPLEVBQUUsSUFBSTtxQkFDaEI7aUJBQ0o7YUFDSjtZQUVELE1BQU0sRUFBRSxDQUFDO29CQUNMLElBQUksRUFBRSxTQUFTO29CQUNmLElBQUksRUFBRSxJQUFJLENBQUMsV0FBVztpQkFDekIsRUFBRTtvQkFDQyxJQUFJLEVBQUUsT0FBTztvQkFDYixJQUFJLEVBQUUsSUFBSSxDQUFDLFFBQVE7aUJBQ3RCO2FBQ0E7U0FDSixDQUFDLENBQUM7SUFDUCxDQUFDO0lBRU8sd0RBQXNCLEdBQTlCO1FBQ0ksSUFBSSxPQUFPLEdBQVEsQ0FBQyxDQUFDLHdCQUF3QixDQUFDLENBQUM7UUFDL0MsT0FBTyxDQUFDLFVBQVUsQ0FBQztZQUNmLEtBQUssRUFBRTtnQkFDSCxJQUFJLEVBQUUsUUFBUTthQUNqQjtZQUNELEtBQUssRUFBRTtnQkFDSCxJQUFJLEVBQUUsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLFdBQVc7YUFDN0M7WUFDRCxLQUFLLEVBQUU7Z0JBQ0gsSUFBSSxFQUFFLFVBQVU7Z0JBQ2hCLG9CQUFvQixFQUFFO29CQUNsQixLQUFLLEVBQUUsUUFBUTtvQkFDZixJQUFJLEVBQUUsSUFBSTtpQkFDYjtnQkFDRCxLQUFLLEVBQUU7b0JBQ0gsSUFBSSxFQUFFLE1BQU07aUJBQ2Y7YUFDSjtZQUNELEtBQUssRUFBRTtnQkFDSCxhQUFhLEVBQUUsS0FBSztnQkFDcEIsS0FBSyxFQUFFO29CQUNILElBQUksRUFBRSxPQUFPO2lCQUNoQjtnQkFDRCxHQUFHLEVBQUUsQ0FBQzthQUNUO1lBQ0QsT0FBTyxFQUFFO2dCQUNMLFlBQVksRUFBRSwwQkFBMEI7Z0JBQ3hDLFdBQVcsRUFBRSw2QkFBNkI7YUFDN0M7WUFFRCxXQUFXLEVBQUU7Z0JBQ1QsTUFBTSxFQUFFO29CQUNKLE1BQU0sRUFBRTt3QkFDSixPQUFPLEVBQUUsSUFBSTtxQkFDaEI7aUJBQ0o7YUFDSjtZQUVELE1BQU0sRUFBRSxDQUFDO29CQUNMLElBQUksRUFBRSxnQkFBZ0I7b0JBQ3RCLElBQUksRUFBRSxJQUFJLENBQUMsb0JBQW9CO2lCQUNsQztnQkFDRDtvQkFDSSxJQUFJLEVBQUUsY0FBYztvQkFDcEIsSUFBSSxFQUFFLElBQUksQ0FBQyxrQkFBa0I7aUJBQ2hDO2dCQUNEO29CQUNJLElBQUksRUFBRSxnQkFBZ0I7b0JBQ3RCLElBQUksRUFBRSxJQUFJLENBQUMsb0JBQW9CO2lCQUNsQzthQUNBO1NBQ0osQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQTlLTDtRQUFDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLHFCQUFxQjtZQUMvQixXQUFXLEVBQUUsZ0NBQWdDO1NBQ2hELENBQUM7OytCQUFBO0lBMktGLDhCQUFDO0FBQUQsQ0F6S0EsQUF5S0MsSUFBQTtBQXpLWSwrQkFBdUIsMEJBeUtuQyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2FkbWluLWRhc2hib2FyZC9hZG1pbi1kYXNoYm9hcmQuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmltcG9ydCB7IEFkbWluRGFzaGJvYXJkU2VydmljZSB9IGZyb20gJy4vYWRtaW5EYXNoYm9hcmRTZXJ2aWNlL2FkbWluRGFzaGJvYXJkLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDZW50cmVVc2VyU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL2NvcmUvaW5kZXgnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgc2VsZWN0b3I6ICdhZG1pbi1kYXNoYm9hcmQtY21wJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnYWRtaW4tZGFzaGJvYXJkLmNvbXBvbmVudC5odG1sJ1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEFkbWluRGFzaGJvYXJkQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBwcml2YXRlIGN1cnJlbnRZZWFyID0gbmV3IERhdGUoKS5nZXRGdWxsWWVhcigpO1xyXG5cclxuICAgIHByaXZhdGUgdG90YWxDZW50cmVzOiBudW1iZXIgPSBudWxsO1xyXG4gICAgcHJpdmF0ZSB0b3RhbENlbnRyZUF3YWl0aW5nQXBwcm92YWw6IG51bWJlciA9IG51bGw7XHJcbiAgICBwcml2YXRlIHRvdGFsSW5jb21lOiBudW1iZXIgPSBudWxsO1xyXG4gICAgcHJpdmF0ZSB0b3RhbE1pU3dhcHM6IG51bWJlciA9IG51bGw7XHJcblxyXG4gICAgcHJpdmF0ZSBkYXlzRm9yU2FsZTogQXJyYXk8bnVtYmVyW10+ID0gbnVsbDtcclxuICAgIHByaXZhdGUgZGF5c1NvbGQ6IEFycmF5PG51bWJlcltdPiA9IG51bGw7XHJcblxyXG4gICAgcHJpdmF0ZSB0b3RhbENlbnRyZXNPdmVyVGltZTogQXJyYXk8bnVtYmVyW10+ID0gbnVsbDtcclxuICAgIHByaXZhdGUgdG90YWxTdGFmZk92ZXJUaW1lOiBBcnJheTxudW1iZXJbXT4gPSBudWxsO1xyXG4gICAgcHJpdmF0ZSB0b3RhbFBhcmVudHNPdmVyVGltZTogQXJyYXk8bnVtYmVyW10+ID0gbnVsbDtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF91c2VyU2VydmljZTogQ2VudHJlVXNlclNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBfYWRtaW5EYXNoYm9hcmRTZXJ2aWNlOiBBZG1pbkRhc2hib2FyZFNlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLl9hZG1pbkRhc2hib2FyZFNlcnZpY2VcclxuICAgICAgICAgICAgLmdldFRvdGFsQ2VudHJlc0NvdW50JCgpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzID0+IHRoaXMudG90YWxDZW50cmVzID0gcmVzKTtcclxuXHJcbiAgICAgICAgdGhpcy5fYWRtaW5EYXNoYm9hcmRTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5nZXRUb3RhbENlbnRyZXNBd2FpdGluZ0FwcHJvdmFsQ291bnQkKClcclxuICAgICAgICAgICAgLnN1YnNjcmliZShyZXMgPT4gdGhpcy50b3RhbENlbnRyZUF3YWl0aW5nQXBwcm92YWwgPSByZXMpO1xyXG5cclxuICAgICAgICB0aGlzLl9hZG1pbkRhc2hib2FyZFNlcnZpY2VcclxuICAgICAgICAgICAgLmdldFRvdGFsSW5jb21lJCgpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzID0+IHRoaXMudG90YWxJbmNvbWUgPSByZXMpO1xyXG5cclxuICAgICAgICB0aGlzLl9hZG1pbkRhc2hib2FyZFNlcnZpY2VcclxuICAgICAgICAgICAgLmdldFRvdGFsQ29tcGxldGVkU3dhcHMkKClcclxuICAgICAgICAgICAgLnN1YnNjcmliZShyZXMgPT4gdGhpcy50b3RhbE1pU3dhcHMgPSByZXMpO1xyXG5cclxuXHJcbiAgICAgICAgdGhpcy5fYWRtaW5EYXNoYm9hcmRTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5nZXRBZG1pbkRhc2hib2FyZERhdGEkKClcclxuICAgICAgICAgICAgLnN1YnNjcmliZShyZXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kYXlzRm9yU2FsZSA9IHRoaXMubWFwR3JhcGhEYXRhKHJlcy5kYXlzRm9yU2FsZSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRheXNTb2xkID0gdGhpcy5tYXBHcmFwaERhdGEocmVzLmRheXNTb2xkKTtcclxuICAgICAgICAgICAgICAgIHRoaXMudG90YWxDZW50cmVzT3ZlclRpbWUgPSB0aGlzLm1hcEdyYXBoRGF0YShyZXMudG90YWxDZW50cmVzT3ZlclRpbWUpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50b3RhbFN0YWZmT3ZlclRpbWUgPSB0aGlzLm1hcEdyYXBoRGF0YShyZXMudG90YWxTdGFmZk92ZXJUaW1lKTtcclxuICAgICAgICAgICAgICAgIHRoaXMudG90YWxQYXJlbnRzT3ZlclRpbWUgPSB0aGlzLm1hcEdyYXBoRGF0YShyZXMudG90YWxQYXJlbnRzT3ZlclRpbWUpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pbml0RGF5U2FsZXNDaGFydCgpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pbml0Q2VudHJlU3VtbWFyeUNoYXJ0KCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgbWFwR3JhcGhEYXRhKGRheXM6IHsgW2tleTogc3RyaW5nXTogbnVtYmVyOyB9KSB7XHJcbiAgICAgICAgbGV0IGFyciA9IG5ldyBBcnJheTxudW1iZXJbXT4oKTtcclxuICAgICAgICBmb3IgKHZhciBrZXkgaW4gZGF5cykge1xyXG4gICAgICAgICAgICBpZiAoZGF5cy5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgZGF5OiBudW1iZXJbXSA9IFtuZXcgRGF0ZShrZXkpLmdldFRpbWUoKSwgZGF5c1trZXldXTtcclxuICAgICAgICAgICAgICAgIGFyci5wdXNoKGRheSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGFycjtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGluaXREYXlTYWxlc0NoYXJ0KCk6IHZvaWQge1xyXG4gICAgICAgIHZhciBtaVN3YXBzOiBhbnkgPSAkKCcjbWlTd2Fwcy10YWJsZScpO1xyXG4gICAgICAgIG1pU3dhcHMuaGlnaGNoYXJ0cyh7XHJcbiAgICAgICAgICAgIGNoYXJ0OiB7XHJcbiAgICAgICAgICAgICAgICB0eXBlOiAnc3BsaW5lJ1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB0aXRsZToge1xyXG4gICAgICAgICAgICAgICAgdGV4dDogJ01pQ2FyZSAnICsgdGhpcy5jdXJyZW50WWVhclxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBzdWJ0aXRsZToge1xyXG4gICAgICAgICAgICAgICAgdGV4dDogJ051bWJlciBvZiBNaUNhcmUgZGF5cydcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgeEF4aXM6IHtcclxuICAgICAgICAgICAgICAgIHR5cGU6ICdkYXRldGltZScsXHJcbiAgICAgICAgICAgICAgICBkYXRlVGltZUxhYmVsRm9ybWF0czogeyAvLyBkb24ndCBkaXNwbGF5IHRoZSBkdW1teSB5ZWFyXHJcbiAgICAgICAgICAgICAgICAgICAgbW9udGg6ICclZS4gJWInLFxyXG4gICAgICAgICAgICAgICAgICAgIHllYXI6ICclYidcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB0aXRsZToge1xyXG4gICAgICAgICAgICAgICAgICAgIHRleHQ6ICdEYXRlJ1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB5QXhpczoge1xyXG4gICAgICAgICAgICAgICAgYWxsb3dEZWNpbWFsczogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICB0aXRsZToge1xyXG4gICAgICAgICAgICAgICAgICAgIHRleHQ6ICdUb3RhbCdcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBtaW46IDBcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgdG9vbHRpcDoge1xyXG4gICAgICAgICAgICAgICAgaGVhZGVyRm9ybWF0OiAnPGI+e3Nlcmllcy5uYW1lfTwvYj48YnI+JyxcclxuICAgICAgICAgICAgICAgIHBvaW50Rm9ybWF0OiAne3BvaW50Lng6JWUuICVifToge3BvaW50Lnl9J1xyXG4gICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgcGxvdE9wdGlvbnM6IHtcclxuICAgICAgICAgICAgICAgIHNwbGluZToge1xyXG4gICAgICAgICAgICAgICAgICAgIG1hcmtlcjoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBlbmFibGVkOiB0cnVlXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgc2VyaWVzOiBbe1xyXG4gICAgICAgICAgICAgICAgbmFtZTogJ1RvIEdpdmUnLFxyXG4gICAgICAgICAgICAgICAgZGF0YTogdGhpcy5kYXlzRm9yU2FsZVxyXG4gICAgICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgICAgICBuYW1lOiAnR2l2ZW4nLFxyXG4gICAgICAgICAgICAgICAgZGF0YTogdGhpcy5kYXlzU29sZFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIF1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGluaXRDZW50cmVTdW1tYXJ5Q2hhcnQoKTogdm9pZCB7XHJcbiAgICAgICAgdmFyIG1pU3dhcHM6IGFueSA9ICQoJyNtaVN3YXBzLXN1bW1hcnktdGFibGUnKTtcclxuICAgICAgICBtaVN3YXBzLmhpZ2hjaGFydHMoe1xyXG4gICAgICAgICAgICBjaGFydDoge1xyXG4gICAgICAgICAgICAgICAgdHlwZTogJ3NwbGluZSdcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgdGl0bGU6IHtcclxuICAgICAgICAgICAgICAgIHRleHQ6ICdNaUNhcmUgU3VtbWFyeSAnICsgdGhpcy5jdXJyZW50WWVhclxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB4QXhpczoge1xyXG4gICAgICAgICAgICAgICAgdHlwZTogJ2RhdGV0aW1lJyxcclxuICAgICAgICAgICAgICAgIGRhdGVUaW1lTGFiZWxGb3JtYXRzOiB7IC8vIGRvbid0IGRpc3BsYXkgdGhlIGR1bW15IHllYXJcclxuICAgICAgICAgICAgICAgICAgICBtb250aDogJyVlLiAlYicsXHJcbiAgICAgICAgICAgICAgICAgICAgeWVhcjogJyViJ1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHRpdGxlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGV4dDogJ0RhdGUnXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHlBeGlzOiB7XHJcbiAgICAgICAgICAgICAgICBhbGxvd0RlY2ltYWxzOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIHRpdGxlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGV4dDogJ1RvdGFsJ1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIG1pbjogMFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB0b29sdGlwOiB7XHJcbiAgICAgICAgICAgICAgICBoZWFkZXJGb3JtYXQ6ICc8Yj57c2VyaWVzLm5hbWV9PC9iPjxicj4nLFxyXG4gICAgICAgICAgICAgICAgcG9pbnRGb3JtYXQ6ICd7cG9pbnQueDolZS4gJWJ9OiB7cG9pbnQueX0nXHJcbiAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICAgICBwbG90T3B0aW9uczoge1xyXG4gICAgICAgICAgICAgICAgc3BsaW5lOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgbWFya2VyOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVuYWJsZWQ6IHRydWVcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICAgICBzZXJpZXM6IFt7XHJcbiAgICAgICAgICAgICAgICBuYW1lOiAnTWlDYXJlIENlbnRyZXMnLFxyXG4gICAgICAgICAgICAgICAgZGF0YTogdGhpcy50b3RhbENlbnRyZXNPdmVyVGltZVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBuYW1lOiAnTWlDYXJlIFN0YWZmJyxcclxuICAgICAgICAgICAgICAgIGRhdGE6IHRoaXMudG90YWxTdGFmZk92ZXJUaW1lXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIG5hbWU6ICdNaUNhcmUgUGFyZW50cycsXHJcbiAgICAgICAgICAgICAgICBkYXRhOiB0aGlzLnRvdGFsUGFyZW50c092ZXJUaW1lXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
