"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var VacanciesComponent = (function () {
    function VacanciesComponent() {
    }
    VacanciesComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'vacancies-cmp',
            templateUrl: './vacancies.component.html'
        }), 
        __metadata('design:paramtypes', [])
    ], VacanciesComponent);
    return VacanciesComponent;
}());
exports.VacanciesComponent = VacanciesComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3ZhY2FuY2llcy92YWNhbmNpZXMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBd0IsZUFBZSxDQUFDLENBQUE7QUFReEM7SUFBQTtJQUFpQyxDQUFDO0lBTmxDO1FBQUMsZ0JBQVMsQ0FBQztZQUNWLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNoQixRQUFRLEVBQUUsZUFBZTtZQUN6QixXQUFXLEVBQUUsNEJBQTRCO1NBQzVDLENBQUM7OzBCQUFBO0lBRStCLHlCQUFDO0FBQUQsQ0FBakMsQUFBa0MsSUFBQTtBQUFyQiwwQkFBa0IscUJBQUcsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC92YWNhbmNpZXMvdmFjYW5jaWVzLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50fSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG5cdG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICBzZWxlY3RvcjogJ3ZhY2FuY2llcy1jbXAnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL3ZhY2FuY2llcy5jb21wb25lbnQuaHRtbCdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBWYWNhbmNpZXNDb21wb25lbnQge31cclxuIl19
