"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var rxjs_1 = require('rxjs');
var angular2_toaster_1 = require('angular2-toaster');
var vacancies_service_1 = require('../vacanciesService/vacancies.service');
var index_1 = require('../../../../core/index');
var index_2 = require('../../../../shared/index');
var index_3 = require('../../../../api/index');
var moment = require('moment/moment');
var _ = require('lodash');
var VacanciesTableComponent = (function () {
    function VacanciesTableComponent(_vacancyService, _userService, _toasterService) {
        this._vacancyService = _vacancyService;
        this._userService = _userService;
        this._toasterService = _toasterService;
        this.dateKeyFormat = 'YYYY-MM-DD[T]HH:mm:ss';
        this.roomVacancies = null;
        this.originalRoomVacancies = null;
        this.startOfWeek = moment().startOf('isoWeek').utc(true);
        this.today = moment().startOf('day');
    }
    VacanciesTableComponent.prototype.ngOnInit = function () {
        this.getVacancies(this.startOfWeek);
    };
    VacanciesTableComponent.prototype.getVacancies = function (day) {
        var _this = this;
        this.daysOfWeek = this.getDaysOfWeek();
        this.displayWeek = this.showWeek(day);
        this.roomVacancies = null;
        this.originalRoomVacancies = null;
        this._vacancyService
            .getVacancies$(this._userService.SelectedCentre.id, day.toDate())
            .subscribe(function (res) {
            _this.roomVacancies = res;
            _this.originalRoomVacancies = _.cloneDeep(_this.roomVacancies);
        });
    };
    VacanciesTableComponent.prototype.getDaysOfWeek = function () {
        var weekDays = new Array();
        weekDays.push(this.startOfWeek);
        var startDate = this.startOfWeek.clone();
        for (var i = 0; i < 6; ++i) {
            startDate.add(1, 'days');
            weekDays.push(startDate.clone());
        }
        return weekDays;
    };
    VacanciesTableComponent.prototype.getVacanciesForDay = function (room, forDay) {
        var dateKey = this.getDateKey(forDay);
        var numOfVacancies = room.vacancies[dateKey];
        return numOfVacancies === undefined ? 0 : numOfVacancies;
    };
    VacanciesTableComponent.prototype.getListedByParentsForDay = function (room, forDay) {
        var dateKey = this.getDateKey(forDay);
        var numOfVacancies = room.listedByParents[dateKey];
        return numOfVacancies === undefined ? 0 : numOfVacancies;
    };
    VacanciesTableComponent.prototype.getSoldVacanciesForDay = function (room, forDay) {
        var dateKey = this.getDateKey(forDay);
        var numOfVacancies = room.sold[dateKey];
        return numOfVacancies === undefined ? 0 : numOfVacancies;
    };
    VacanciesTableComponent.prototype.getTotalVacanciesForDay = function (room, forDay) {
        return this.getVacanciesForDay(room, forDay) +
            this.getListedByParentsForDay(room, forDay) -
            this.getSoldVacanciesForDay(room, forDay);
    };
    VacanciesTableComponent.prototype.setNumberOfVacncies = function (room, forDay, input) {
        var dateKey = this.getDateKey(forDay);
        var numberOfVacancies = parseFloat(input);
        if (!isNaN(numberOfVacancies)) {
            room.vacancies[dateKey] = numberOfVacancies;
        }
    };
    VacanciesTableComponent.prototype.createUpdateRoomVacancy = function (room) {
        var _this = this;
        var savingToast = this._toasterService.pop(new index_2.SpinningToast());
        this.createUpdateRoomVacancy$(room)
            .subscribe(function () {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this._toasterService.popAsync('success', 'Saved', '');
        }, function (err) { return _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId); });
    };
    VacanciesTableComponent.prototype.createUpdateRoomVacancy$ = function (room) {
        var _this = this;
        var createUpdateVacancyDto = index_3.CreateUpdateVacancyDto.fromJS(room.toJSON());
        return this._vacancyService
            .CreateUpdateVacancies$(createUpdateVacancyDto)
            .flatMap(function (res) {
            var currentRoomVacancies = _this.roomVacancies.find(function (savedRoom) { return savedRoom.roomId === room.roomId; });
            currentRoomVacancies.vacancies = _.cloneDeep(res.vacancies);
            var origRoomVacancies = _this.originalRoomVacancies.find(function (savedRoom) { return savedRoom.roomId === room.roomId; });
            origRoomVacancies.vacancies = _.cloneDeep(res.vacancies);
            return rxjs_1.Observable.of(null);
        });
    };
    VacanciesTableComponent.prototype.getDateKey = function (forDay) {
        return forDay.format(this.dateKeyFormat) + 'Z';
    };
    VacanciesTableComponent.prototype.cancelEdit = function (room) {
        var originalVacancies = this.originalRoomVacancies
            .find(function (savedRoom) { return savedRoom.roomId === room.roomId; })
            .vacancies;
        room.vacancies = _.cloneDeep(originalVacancies);
    };
    VacanciesTableComponent.prototype.clearVacancies = function (room) {
        var _this = this;
        if (window.confirm('Are you sure you want to clear all vacancies for this room this week?')) {
            var savingToast_1 = this._toasterService.pop(new index_2.SpinningToast());
            for (var date in room.vacancies)
                if (room.vacancies.hasOwnProperty(date))
                    room.vacancies[date] = 0;
            this._vacancyService
                .CreateUpdateVacancies$(room)
                .subscribe(function (res) {
                var currentRoomVacancies = _this.roomVacancies.find(function (savedRoom) { return savedRoom.roomId === room.roomId; });
                currentRoomVacancies.vacancies = _.cloneDeep(res.vacancies);
                var origRoomVacancies = _this.originalRoomVacancies.find(function (savedRoom) { return savedRoom.roomId === room.roomId; });
                origRoomVacancies.vacancies = _.cloneDeep(res.vacancies);
                _this._toasterService.clear(savingToast_1.toastId, savingToast_1.toastContainerId);
                _this._toasterService.popAsync('success', 'Saved', '');
            });
        }
    };
    VacanciesTableComponent.prototype.previousWeek = function () {
        this.getVacancies(this.startOfWeek.add(-7, 'days'));
    };
    VacanciesTableComponent.prototype.nextWeek = function () {
        this.getVacancies(this.startOfWeek.add(7, 'days'));
    };
    VacanciesTableComponent.prototype.copyVacancies = function () {
        var _this = this;
        if (window.confirm('Are you sure you want to copy all vacancies from the previous week?\n' +
            'This will overwrite any existing vacancies for this week')) {
            var savingToast_2 = this._toasterService.pop(new index_2.SpinningToast());
            var lastWeek = this.startOfWeek.clone().add(-7, 'days');
            this._vacancyService
                .getVacancies$(this._userService.SelectedCentre.id, lastWeek.toDate())
                .flatMap(function (lastWeekVacancies) {
                var observables = new Array();
                for (var i = 0; i < lastWeekVacancies.length; ++i) {
                    var lastWeek_1 = lastWeekVacancies[i];
                    var thisWeekVacancies = new index_3.VacancyDto();
                    thisWeekVacancies.init({
                        'CentreId': lastWeek_1.centreId,
                        'RoomId': lastWeek_1.roomId,
                        'Vacancies': {}
                    });
                    for (var date in lastWeek_1.vacancies)
                        if (lastWeek_1.vacancies.hasOwnProperty(date)) {
                            thisWeekVacancies.vacancies[_this.getDateKey(moment(date).add(7, 'days'))]
                                = lastWeek_1.vacancies[date];
                        }
                    var obs = _this.createUpdateRoomVacancy$(thisWeekVacancies);
                    observables.push(obs);
                }
                return rxjs_1.Observable.forkJoin(observables);
            })
                .subscribe(function () {
                _this._toasterService.clear(savingToast_2.toastId, savingToast_2.toastContainerId);
                _this._toasterService.popAsync('success', 'Saved', '');
            }, function (err) { return _this._toasterService.clear(savingToast_2.toastId, savingToast_2.toastContainerId); });
        }
    };
    VacanciesTableComponent.prototype.showWeek = function (day) {
        var displayWeek = '';
        if (moment().isSame(day, 'week')) {
            displayWeek = 'This Week';
        }
        else {
            displayWeek = this.daysOfWeek[0].format('Do MMM') + " to \n                            " + this.daysOfWeek[this.daysOfWeek.length - 1].format('Do MMM');
        }
        return displayWeek;
    };
    VacanciesTableComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'vacancies-table-cmp',
            templateUrl: './vacanciesTable.component.html'
        }), 
        __metadata('design:paramtypes', [vacancies_service_1.VacanciesService, index_1.CentreUserService, angular2_toaster_1.ToasterService])
    ], VacanciesTableComponent);
    return VacanciesTableComponent;
}());
exports.VacanciesTableComponent = VacanciesTableComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3ZhY2FuY2llcy92YWNhbmNpZXNUYWJsZS92YWNhbmNpZXNUYWJsZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUFrQyxlQUFlLENBQUMsQ0FBQTtBQUNsRCxxQkFBMkIsTUFBTSxDQUFDLENBQUE7QUFDbEMsaUNBQStCLGtCQUFrQixDQUFDLENBQUE7QUFFbEQsa0NBQWlDLHVDQUF1QyxDQUFDLENBQUE7QUFDekUsc0JBQWtDLHdCQUF3QixDQUFDLENBQUE7QUFDM0Qsc0JBQThCLDBCQUEwQixDQUFDLENBQUE7QUFDekQsc0JBQW1ELHVCQUF1QixDQUFDLENBQUE7QUFDM0UsSUFBWSxNQUFNLFdBQU0sZUFBZSxDQUFDLENBQUE7QUFDeEMsSUFBWSxDQUFDLFdBQU0sUUFBUSxDQUFDLENBQUE7QUFPNUI7SUFTSSxpQ0FBb0IsZUFBaUMsRUFDekMsWUFBK0IsRUFDL0IsZUFBK0I7UUFGdkIsb0JBQWUsR0FBZixlQUFlLENBQWtCO1FBQ3pDLGlCQUFZLEdBQVosWUFBWSxDQUFtQjtRQUMvQixvQkFBZSxHQUFmLGVBQWUsQ0FBZ0I7UUFUMUIsa0JBQWEsR0FBRyx1QkFBdUIsQ0FBQztRQUNqRCxrQkFBYSxHQUFpQixJQUFJLENBQUM7UUFDbkMsMEJBQXFCLEdBQWlCLElBQUksQ0FBQztRQUUzQyxnQkFBVyxHQUFrQixNQUFNLEVBQUUsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBTXZFLElBQUksQ0FBQyxLQUFLLEdBQUcsTUFBTSxFQUFFLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3pDLENBQUM7SUFFRCwwQ0FBUSxHQUFSO1FBQ0ksSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDeEMsQ0FBQztJQUVNLDhDQUFZLEdBQW5CLFVBQW9CLEdBQWtCO1FBQXRDLGlCQVlDO1FBWEcsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDdkMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1FBQzFCLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUM7UUFFbEMsSUFBSSxDQUFDLGVBQWU7YUFDZixhQUFhLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsRUFBRSxFQUFFLEdBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQzthQUNoRSxTQUFTLENBQUMsVUFBQSxHQUFHO1lBQ1YsS0FBSSxDQUFDLGFBQWEsR0FBRyxHQUFHLENBQUM7WUFDekIsS0FBSSxDQUFDLHFCQUFxQixHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ2pFLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVNLCtDQUFhLEdBQXBCO1FBQ0ksSUFBSSxRQUFRLEdBQUcsSUFBSSxLQUFLLEVBQU8sQ0FBQztRQUNoQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNoQyxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3pDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFXLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUM7WUFDakMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDekIsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQztRQUNyQyxDQUFDO1FBQ0QsTUFBTSxDQUFDLFFBQVEsQ0FBQztJQUNwQixDQUFDO0lBRU0sb0RBQWtCLEdBQXpCLFVBQTBCLElBQWdCLEVBQUUsTUFBcUI7UUFDN0QsSUFBSSxPQUFPLEdBQVcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM5QyxJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzdDLE1BQU0sQ0FBQyxjQUFjLEtBQUssU0FBUyxHQUFHLENBQUMsR0FBRyxjQUFjLENBQUM7SUFDN0QsQ0FBQztJQUVNLDBEQUF3QixHQUEvQixVQUFnQyxJQUFnQixFQUFFLE1BQXFCO1FBQ25FLElBQUksT0FBTyxHQUFXLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDOUMsSUFBSSxjQUFjLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNuRCxNQUFNLENBQUMsY0FBYyxLQUFLLFNBQVMsR0FBRyxDQUFDLEdBQUcsY0FBYyxDQUFDO0lBQzdELENBQUM7SUFFTSx3REFBc0IsR0FBN0IsVUFBOEIsSUFBZ0IsRUFBRSxNQUFxQjtRQUNqRSxJQUFJLE9BQU8sR0FBVyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzlDLElBQUksY0FBYyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDeEMsTUFBTSxDQUFDLGNBQWMsS0FBSyxTQUFTLEdBQUcsQ0FBQyxHQUFHLGNBQWMsQ0FBQztJQUM3RCxDQUFDO0lBRU0seURBQXVCLEdBQTlCLFVBQStCLElBQWdCLEVBQUUsTUFBcUI7UUFDbEUsTUFBTSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDO1lBQ3hDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDO1lBQzNDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDbEQsQ0FBQztJQUVNLHFEQUFtQixHQUExQixVQUEyQixJQUFnQixFQUFFLE1BQXFCLEVBQUUsS0FBYTtRQUM3RSxJQUFJLE9BQU8sR0FBVyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzlDLElBQUksaUJBQWlCLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzVCLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEdBQUcsaUJBQWlCLENBQUM7UUFDaEQsQ0FBQztJQUNMLENBQUM7SUFFTSx5REFBdUIsR0FBOUIsVUFBK0IsSUFBZ0I7UUFBL0MsaUJBUUM7UUFQRyxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxJQUFJLHFCQUFhLEVBQUUsQ0FBQyxDQUFDO1FBQ2hFLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLENBQUM7YUFDOUIsU0FBUyxDQUFDO1lBQ1AsS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxXQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUM5RSxLQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsT0FBTyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQzFELENBQUMsRUFDRCxVQUFBLEdBQUcsSUFBSSxPQUFBLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLGdCQUFnQixDQUFDLEVBQTdFLENBQTZFLENBQUMsQ0FBQztJQUM5RixDQUFDO0lBRU0sMERBQXdCLEdBQS9CLFVBQWdDLElBQWdCO1FBQWhELGlCQWFDO1FBWEcsSUFBSSxzQkFBc0IsR0FBMkIsOEJBQXNCLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO1FBQ2xHLE1BQU0sQ0FBQyxJQUFJLENBQUMsZUFBZTthQUN0QixzQkFBc0IsQ0FBQyxzQkFBc0IsQ0FBQzthQUM5QyxPQUFPLENBQUMsVUFBQSxHQUFHO1lBRVIsSUFBSSxvQkFBb0IsR0FBRyxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxVQUFDLFNBQVMsSUFBSyxPQUFBLFNBQVMsQ0FBQyxNQUFNLEtBQUssSUFBSSxDQUFDLE1BQU0sRUFBaEMsQ0FBZ0MsQ0FBQyxDQUFDO1lBQ3BHLG9CQUFvQixDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUM1RCxJQUFJLGlCQUFpQixHQUFHLEtBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsVUFBQyxTQUFTLElBQUssT0FBQSxTQUFTLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxNQUFNLEVBQWhDLENBQWdDLENBQUMsQ0FBQztZQUN6RyxpQkFBaUIsQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDekQsTUFBTSxDQUFDLGlCQUFVLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQy9CLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVNLDRDQUFVLEdBQWpCLFVBQWtCLE1BQXFCO1FBQ25DLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxHQUFHLENBQUM7SUFDbkQsQ0FBQztJQUVNLDRDQUFVLEdBQWpCLFVBQWtCLElBQWdCO1FBQzlCLElBQUksaUJBQWlCLEdBQ2pCLElBQUksQ0FBQyxxQkFBcUI7YUFDckIsSUFBSSxDQUFDLFVBQUMsU0FBUyxJQUFLLE9BQUEsU0FBUyxDQUFDLE1BQU0sS0FBSyxJQUFJLENBQUMsTUFBTSxFQUFoQyxDQUFnQyxDQUFDO2FBQ3JELFNBQVMsQ0FBQztRQUNuQixJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsaUJBQWlCLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBRU0sZ0RBQWMsR0FBckIsVUFBc0IsSUFBZ0I7UUFBdEMsaUJBb0JDO1FBbkJHLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsdUVBQXVFLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFMUYsSUFBSSxhQUFXLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsSUFBSSxxQkFBYSxFQUFFLENBQUMsQ0FBQztZQUVoRSxHQUFHLENBQUMsQ0FBQyxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDO2dCQUM1QixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7WUFFakMsSUFBSSxDQUFDLGVBQWU7aUJBQ2Ysc0JBQXNCLENBQUMsSUFBSSxDQUFDO2lCQUM1QixTQUFTLENBQUMsVUFBQSxHQUFHO2dCQUNWLElBQUksb0JBQW9CLEdBQUcsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsVUFBQyxTQUFTLElBQUssT0FBQSxTQUFTLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxNQUFNLEVBQWhDLENBQWdDLENBQUMsQ0FBQztnQkFDcEcsb0JBQW9CLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUM1RCxJQUFJLGlCQUFpQixHQUFHLEtBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsVUFBQyxTQUFTLElBQUssT0FBQSxTQUFTLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxNQUFNLEVBQWhDLENBQWdDLENBQUMsQ0FBQztnQkFDekcsaUJBQWlCLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUN6RCxLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxhQUFXLENBQUMsT0FBTyxFQUFFLGFBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2dCQUM5RSxLQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsT0FBTyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQzFELENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQztJQUNMLENBQUM7SUFFTSw4Q0FBWSxHQUFuQjtRQUNJLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQztJQUN4RCxDQUFDO0lBRU0sMENBQVEsR0FBZjtRQUNJLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUM7SUFDdkQsQ0FBQztJQUVNLCtDQUFhLEdBQXBCO1FBQUEsaUJBdUNDO1FBdENHLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsdUVBQXVFO1lBQ3RGLDBEQUEwRCxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRTlELElBQUksYUFBVyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUkscUJBQWEsRUFBRSxDQUFDLENBQUM7WUFFaEUsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDeEQsSUFBSSxDQUFDLGVBQWU7aUJBQ2YsYUFBYSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLEVBQUUsRUFBRSxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUM7aUJBQ3JFLE9BQU8sQ0FBQyxVQUFBLGlCQUFpQjtnQkFDdEIsSUFBSSxXQUFXLEdBQUcsSUFBSSxLQUFLLEVBQW1CLENBQUM7Z0JBQy9DLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFXLENBQUMsRUFBRSxDQUFDLEdBQUcsaUJBQWlCLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUM7b0JBRXhELElBQUksVUFBUSxHQUFlLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUVoRCxJQUFJLGlCQUFpQixHQUFlLElBQUksa0JBQVUsRUFBRSxDQUFDO29CQUNyRCxpQkFBaUIsQ0FBQyxJQUFJLENBQUM7d0JBQ25CLFVBQVUsRUFBRSxVQUFRLENBQUMsUUFBUTt3QkFDN0IsUUFBUSxFQUFFLFVBQVEsQ0FBQyxNQUFNO3dCQUN6QixXQUFXLEVBQUUsRUFBRTtxQkFDbEIsQ0FBQyxDQUFDO29CQUNILEdBQUcsQ0FBQyxDQUFDLElBQUksSUFBSSxJQUFJLFVBQVEsQ0FBQyxTQUFTLENBQUM7d0JBQ2hDLEVBQUUsQ0FBQyxDQUFDLFVBQVEsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzs0QkFDMUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQztrQ0FDbkUsVUFBUSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDbkMsQ0FBQztvQkFFTCxJQUFJLEdBQUcsR0FBRyxLQUFJLENBQUMsd0JBQXdCLENBQUMsaUJBQWlCLENBQUMsQ0FBQztvQkFDM0QsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDMUIsQ0FBQztnQkFFRCxNQUFNLENBQUMsaUJBQVUsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDNUMsQ0FBQyxDQUFDO2lCQUNELFNBQVMsQ0FBQztnQkFDUCxLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxhQUFXLENBQUMsT0FBTyxFQUFFLGFBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2dCQUM5RSxLQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsT0FBTyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQzFELENBQUMsRUFDRCxVQUFBLEdBQUcsSUFBSSxPQUFBLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLGFBQVcsQ0FBQyxPQUFPLEVBQUUsYUFBVyxDQUFDLGdCQUFnQixDQUFDLEVBQTdFLENBQTZFLENBQUMsQ0FBQztRQUM5RixDQUFDO0lBQ0wsQ0FBQztJQUVNLDBDQUFRLEdBQWYsVUFBZ0IsR0FBa0I7UUFFOUIsSUFBSSxXQUFXLEdBQVcsRUFBRSxDQUFDO1FBQzdCLEVBQUUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQy9CLFdBQVcsR0FBRyxXQUFXLENBQUM7UUFDOUIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osV0FBVyxHQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQywwQ0FDbEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFHLENBQUM7UUFDckYsQ0FBQztRQUNELE1BQU0sQ0FBQyxXQUFXLENBQUM7SUFDdkIsQ0FBQztJQXZNTDtRQUFDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLHFCQUFxQjtZQUMvQixXQUFXLEVBQUUsaUNBQWlDO1NBQ2pELENBQUM7OytCQUFBO0lBb01GLDhCQUFDO0FBQUQsQ0FuTUEsQUFtTUMsSUFBQTtBQW5NWSwrQkFBdUIsMEJBbU1uQyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3ZhY2FuY2llcy92YWNhbmNpZXNUYWJsZS92YWNhbmNpZXNUYWJsZS5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IFRvYXN0ZXJTZXJ2aWNlIH0gZnJvbSAnYW5ndWxhcjItdG9hc3Rlcic7XHJcblxyXG5pbXBvcnQgeyBWYWNhbmNpZXNTZXJ2aWNlIH0gZnJvbSAnLi4vdmFjYW5jaWVzU2VydmljZS92YWNhbmNpZXMuc2VydmljZSc7XHJcbmltcG9ydCB7IENlbnRyZVVzZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vY29yZS9pbmRleCc7XHJcbmltcG9ydCB7IFNwaW5uaW5nVG9hc3QgfSBmcm9tICcuLi8uLi8uLi8uLi9zaGFyZWQvaW5kZXgnO1xyXG5pbXBvcnQgeyBWYWNhbmN5RHRvLCBDcmVhdGVVcGRhdGVWYWNhbmN5RHRvIH0gZnJvbSAnLi4vLi4vLi4vLi4vYXBpL2luZGV4JztcclxuaW1wb3J0ICogYXMgbW9tZW50IGZyb20gJ21vbWVudC9tb21lbnQnO1xyXG5pbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICBzZWxlY3RvcjogJ3ZhY2FuY2llcy10YWJsZS1jbXAnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL3ZhY2FuY2llc1RhYmxlLmNvbXBvbmVudC5odG1sJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgVmFjYW5jaWVzVGFibGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIHByaXZhdGUgcmVhZG9ubHkgZGF0ZUtleUZvcm1hdCA9ICdZWVlZLU1NLUREW1RdSEg6bW06c3MnO1xyXG4gICAgcHJpdmF0ZSByb29tVmFjYW5jaWVzOiBWYWNhbmN5RHRvW10gPSBudWxsO1xyXG4gICAgcHJpdmF0ZSBvcmlnaW5hbFJvb21WYWNhbmNpZXM6IFZhY2FuY3lEdG9bXSA9IG51bGw7XHJcbiAgICBwcml2YXRlIHRvZGF5OiBtb21lbnQuTW9tZW50O1xyXG4gICAgcHJpdmF0ZSBzdGFydE9mV2VlazogbW9tZW50Lk1vbWVudCA9IG1vbWVudCgpLnN0YXJ0T2YoJ2lzb1dlZWsnKS51dGModHJ1ZSk7XHJcbiAgICBwcml2YXRlIGRheXNPZldlZWs6IG1vbWVudC5Nb21lbnRbXTtcclxuICAgIHByaXZhdGUgZGlzcGxheVdlZWs6IHN0cmluZztcclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX3ZhY2FuY3lTZXJ2aWNlOiBWYWNhbmNpZXNTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgX3VzZXJTZXJ2aWNlOiBDZW50cmVVc2VyU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIF90b2FzdGVyU2VydmljZTogVG9hc3RlclNlcnZpY2UpIHtcclxuICAgICAgICB0aGlzLnRvZGF5ID0gbW9tZW50KCkuc3RhcnRPZignZGF5Jyk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5nZXRWYWNhbmNpZXModGhpcy5zdGFydE9mV2Vlayk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldFZhY2FuY2llcyhkYXk6IG1vbWVudC5Nb21lbnQpIHtcclxuICAgICAgICB0aGlzLmRheXNPZldlZWsgPSB0aGlzLmdldERheXNPZldlZWsoKTtcclxuICAgICAgICB0aGlzLmRpc3BsYXlXZWVrID0gdGhpcy5zaG93V2VlayhkYXkpO1xyXG4gICAgICAgIHRoaXMucm9vbVZhY2FuY2llcyA9IG51bGw7XHJcbiAgICAgICAgdGhpcy5vcmlnaW5hbFJvb21WYWNhbmNpZXMgPSBudWxsO1xyXG5cclxuICAgICAgICB0aGlzLl92YWNhbmN5U2VydmljZVxyXG4gICAgICAgICAgICAuZ2V0VmFjYW5jaWVzJCh0aGlzLl91c2VyU2VydmljZS5TZWxlY3RlZENlbnRyZS5pZCwgZGF5LnRvRGF0ZSgpKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKHJlcyA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJvb21WYWNhbmNpZXMgPSByZXM7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9yaWdpbmFsUm9vbVZhY2FuY2llcyA9IF8uY2xvbmVEZWVwKHRoaXMucm9vbVZhY2FuY2llcyk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXREYXlzT2ZXZWVrKCk6IG1vbWVudC5Nb21lbnRbXSB7XHJcbiAgICAgICAgbGV0IHdlZWtEYXlzID0gbmV3IEFycmF5PGFueT4oKTtcclxuICAgICAgICB3ZWVrRGF5cy5wdXNoKHRoaXMuc3RhcnRPZldlZWspO1xyXG4gICAgICAgIGxldCBzdGFydERhdGUgPSB0aGlzLnN0YXJ0T2ZXZWVrLmNsb25lKCk7XHJcbiAgICAgICAgZm9yIChsZXQgaTogbnVtYmVyID0gMDsgaSA8IDY7ICsraSkge1xyXG4gICAgICAgICAgICBzdGFydERhdGUuYWRkKDEsICdkYXlzJyk7XHJcbiAgICAgICAgICAgIHdlZWtEYXlzLnB1c2goc3RhcnREYXRlLmNsb25lKCkpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gd2Vla0RheXM7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldFZhY2FuY2llc0ZvckRheShyb29tOiBWYWNhbmN5RHRvLCBmb3JEYXk6IG1vbWVudC5Nb21lbnQpOiBudW1iZXIge1xyXG4gICAgICAgIGxldCBkYXRlS2V5OiBzdHJpbmcgPSB0aGlzLmdldERhdGVLZXkoZm9yRGF5KTtcclxuICAgICAgICBsZXQgbnVtT2ZWYWNhbmNpZXMgPSByb29tLnZhY2FuY2llc1tkYXRlS2V5XTtcclxuICAgICAgICByZXR1cm4gbnVtT2ZWYWNhbmNpZXMgPT09IHVuZGVmaW5lZCA/IDAgOiBudW1PZlZhY2FuY2llcztcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0TGlzdGVkQnlQYXJlbnRzRm9yRGF5KHJvb206IFZhY2FuY3lEdG8sIGZvckRheTogbW9tZW50Lk1vbWVudCk6IG51bWJlciB7XHJcbiAgICAgICAgbGV0IGRhdGVLZXk6IHN0cmluZyA9IHRoaXMuZ2V0RGF0ZUtleShmb3JEYXkpO1xyXG4gICAgICAgIGxldCBudW1PZlZhY2FuY2llcyA9IHJvb20ubGlzdGVkQnlQYXJlbnRzW2RhdGVLZXldO1xyXG4gICAgICAgIHJldHVybiBudW1PZlZhY2FuY2llcyA9PT0gdW5kZWZpbmVkID8gMCA6IG51bU9mVmFjYW5jaWVzO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRTb2xkVmFjYW5jaWVzRm9yRGF5KHJvb206IFZhY2FuY3lEdG8sIGZvckRheTogbW9tZW50Lk1vbWVudCk6IG51bWJlciB7XHJcbiAgICAgICAgbGV0IGRhdGVLZXk6IHN0cmluZyA9IHRoaXMuZ2V0RGF0ZUtleShmb3JEYXkpO1xyXG4gICAgICAgIGxldCBudW1PZlZhY2FuY2llcyA9IHJvb20uc29sZFtkYXRlS2V5XTtcclxuICAgICAgICByZXR1cm4gbnVtT2ZWYWNhbmNpZXMgPT09IHVuZGVmaW5lZCA/IDAgOiBudW1PZlZhY2FuY2llcztcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0VG90YWxWYWNhbmNpZXNGb3JEYXkocm9vbTogVmFjYW5jeUR0bywgZm9yRGF5OiBtb21lbnQuTW9tZW50KTogbnVtYmVyIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5nZXRWYWNhbmNpZXNGb3JEYXkocm9vbSwgZm9yRGF5KSArXHJcbiAgICAgICAgICAgIHRoaXMuZ2V0TGlzdGVkQnlQYXJlbnRzRm9yRGF5KHJvb20sIGZvckRheSkgLVxyXG4gICAgICAgICAgICB0aGlzLmdldFNvbGRWYWNhbmNpZXNGb3JEYXkocm9vbSwgZm9yRGF5KTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc2V0TnVtYmVyT2ZWYWNuY2llcyhyb29tOiBWYWNhbmN5RHRvLCBmb3JEYXk6IG1vbWVudC5Nb21lbnQsIGlucHV0OiBzdHJpbmcpOiB2b2lkIHtcclxuICAgICAgICBsZXQgZGF0ZUtleTogc3RyaW5nID0gdGhpcy5nZXREYXRlS2V5KGZvckRheSk7XHJcbiAgICAgICAgbGV0IG51bWJlck9mVmFjYW5jaWVzID0gcGFyc2VGbG9hdChpbnB1dCk7XHJcbiAgICAgICAgaWYgKCFpc05hTihudW1iZXJPZlZhY2FuY2llcykpIHtcclxuICAgICAgICAgICAgcm9vbS52YWNhbmNpZXNbZGF0ZUtleV0gPSBudW1iZXJPZlZhY2FuY2llcztcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGNyZWF0ZVVwZGF0ZVJvb21WYWNhbmN5KHJvb206IFZhY2FuY3lEdG8pOiB2b2lkIHtcclxuICAgICAgICBsZXQgc2F2aW5nVG9hc3QgPSB0aGlzLl90b2FzdGVyU2VydmljZS5wb3AobmV3IFNwaW5uaW5nVG9hc3QoKSk7XHJcbiAgICAgICAgdGhpcy5jcmVhdGVVcGRhdGVSb29tVmFjYW5jeSQocm9vbSlcclxuICAgICAgICAgICAgLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcihzYXZpbmdUb2FzdC50b2FzdElkLCBzYXZpbmdUb2FzdC50b2FzdENvbnRhaW5lcklkKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLnBvcEFzeW5jKCdzdWNjZXNzJywgJ1NhdmVkJywgJycpO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBlcnIgPT4gdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoc2F2aW5nVG9hc3QudG9hc3RJZCwgc2F2aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCkpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBjcmVhdGVVcGRhdGVSb29tVmFjYW5jeSQocm9vbTogVmFjYW5jeUR0byk6IE9ic2VydmFibGU8dm9pZD4ge1xyXG5cclxuICAgICAgICBsZXQgY3JlYXRlVXBkYXRlVmFjYW5jeUR0bzogQ3JlYXRlVXBkYXRlVmFjYW5jeUR0byA9IENyZWF0ZVVwZGF0ZVZhY2FuY3lEdG8uZnJvbUpTKHJvb20udG9KU09OKCkpO1xyXG4gICAgICAgIHJldHVybiB0aGlzLl92YWNhbmN5U2VydmljZVxyXG4gICAgICAgICAgICAuQ3JlYXRlVXBkYXRlVmFjYW5jaWVzJChjcmVhdGVVcGRhdGVWYWNhbmN5RHRvKVxyXG4gICAgICAgICAgICAuZmxhdE1hcChyZXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgLy8gcmVwbGFjZSByb29tSWRcclxuICAgICAgICAgICAgICAgIGxldCBjdXJyZW50Um9vbVZhY2FuY2llcyA9IHRoaXMucm9vbVZhY2FuY2llcy5maW5kKChzYXZlZFJvb20pID0+IHNhdmVkUm9vbS5yb29tSWQgPT09IHJvb20ucm9vbUlkKTtcclxuICAgICAgICAgICAgICAgIGN1cnJlbnRSb29tVmFjYW5jaWVzLnZhY2FuY2llcyA9IF8uY2xvbmVEZWVwKHJlcy52YWNhbmNpZXMpO1xyXG4gICAgICAgICAgICAgICAgbGV0IG9yaWdSb29tVmFjYW5jaWVzID0gdGhpcy5vcmlnaW5hbFJvb21WYWNhbmNpZXMuZmluZCgoc2F2ZWRSb29tKSA9PiBzYXZlZFJvb20ucm9vbUlkID09PSByb29tLnJvb21JZCk7XHJcbiAgICAgICAgICAgICAgICBvcmlnUm9vbVZhY2FuY2llcy52YWNhbmNpZXMgPSBfLmNsb25lRGVlcChyZXMudmFjYW5jaWVzKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBPYnNlcnZhYmxlLm9mKG51bGwpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0RGF0ZUtleShmb3JEYXk6IG1vbWVudC5Nb21lbnQpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBmb3JEYXkuZm9ybWF0KHRoaXMuZGF0ZUtleUZvcm1hdCkgKyAnWic7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGNhbmNlbEVkaXQocm9vbTogVmFjYW5jeUR0bykge1xyXG4gICAgICAgIGxldCBvcmlnaW5hbFZhY2FuY2llczogeyBba2V5OiBzdHJpbmddOiBudW1iZXIgfSA9XHJcbiAgICAgICAgICAgIHRoaXMub3JpZ2luYWxSb29tVmFjYW5jaWVzXHJcbiAgICAgICAgICAgICAgICAuZmluZCgoc2F2ZWRSb29tKSA9PiBzYXZlZFJvb20ucm9vbUlkID09PSByb29tLnJvb21JZClcclxuICAgICAgICAgICAgICAgIC52YWNhbmNpZXM7XHJcbiAgICAgICAgcm9vbS52YWNhbmNpZXMgPSBfLmNsb25lRGVlcChvcmlnaW5hbFZhY2FuY2llcyk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGNsZWFyVmFjYW5jaWVzKHJvb206IFZhY2FuY3lEdG8pIHtcclxuICAgICAgICBpZiAod2luZG93LmNvbmZpcm0oJ0FyZSB5b3Ugc3VyZSB5b3Ugd2FudCB0byBjbGVhciBhbGwgdmFjYW5jaWVzIGZvciB0aGlzIHJvb20gdGhpcyB3ZWVrPycpKSB7XHJcblxyXG4gICAgICAgICAgICBsZXQgc2F2aW5nVG9hc3QgPSB0aGlzLl90b2FzdGVyU2VydmljZS5wb3AobmV3IFNwaW5uaW5nVG9hc3QoKSk7XHJcblxyXG4gICAgICAgICAgICBmb3IgKGxldCBkYXRlIGluIHJvb20udmFjYW5jaWVzKVxyXG4gICAgICAgICAgICAgICAgaWYgKHJvb20udmFjYW5jaWVzLmhhc093blByb3BlcnR5KGRhdGUpKVxyXG4gICAgICAgICAgICAgICAgICAgIHJvb20udmFjYW5jaWVzW2RhdGVdID0gMDtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuX3ZhY2FuY3lTZXJ2aWNlXHJcbiAgICAgICAgICAgICAgICAuQ3JlYXRlVXBkYXRlVmFjYW5jaWVzJChyb29tKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBjdXJyZW50Um9vbVZhY2FuY2llcyA9IHRoaXMucm9vbVZhY2FuY2llcy5maW5kKChzYXZlZFJvb20pID0+IHNhdmVkUm9vbS5yb29tSWQgPT09IHJvb20ucm9vbUlkKTtcclxuICAgICAgICAgICAgICAgICAgICBjdXJyZW50Um9vbVZhY2FuY2llcy52YWNhbmNpZXMgPSBfLmNsb25lRGVlcChyZXMudmFjYW5jaWVzKTtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgb3JpZ1Jvb21WYWNhbmNpZXMgPSB0aGlzLm9yaWdpbmFsUm9vbVZhY2FuY2llcy5maW5kKChzYXZlZFJvb20pID0+IHNhdmVkUm9vbS5yb29tSWQgPT09IHJvb20ucm9vbUlkKTtcclxuICAgICAgICAgICAgICAgICAgICBvcmlnUm9vbVZhY2FuY2llcy52YWNhbmNpZXMgPSBfLmNsb25lRGVlcChyZXMudmFjYW5jaWVzKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcihzYXZpbmdUb2FzdC50b2FzdElkLCBzYXZpbmdUb2FzdC50b2FzdENvbnRhaW5lcklkKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5wb3BBc3luYygnc3VjY2VzcycsICdTYXZlZCcsICcnKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgcHJldmlvdXNXZWVrKCkge1xyXG4gICAgICAgIHRoaXMuZ2V0VmFjYW5jaWVzKHRoaXMuc3RhcnRPZldlZWsuYWRkKC03LCAnZGF5cycpKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgbmV4dFdlZWsoKSB7XHJcbiAgICAgICAgdGhpcy5nZXRWYWNhbmNpZXModGhpcy5zdGFydE9mV2Vlay5hZGQoNywgJ2RheXMnKSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGNvcHlWYWNhbmNpZXMoKTogdm9pZCB7XHJcbiAgICAgICAgaWYgKHdpbmRvdy5jb25maXJtKCdBcmUgeW91IHN1cmUgeW91IHdhbnQgdG8gY29weSBhbGwgdmFjYW5jaWVzIGZyb20gdGhlIHByZXZpb3VzIHdlZWs/XFxuJyArXHJcbiAgICAgICAgICAgICdUaGlzIHdpbGwgb3ZlcndyaXRlIGFueSBleGlzdGluZyB2YWNhbmNpZXMgZm9yIHRoaXMgd2VlaycpKSB7XHJcblxyXG4gICAgICAgICAgICBsZXQgc2F2aW5nVG9hc3QgPSB0aGlzLl90b2FzdGVyU2VydmljZS5wb3AobmV3IFNwaW5uaW5nVG9hc3QoKSk7XHJcblxyXG4gICAgICAgICAgICBsZXQgbGFzdFdlZWsgPSB0aGlzLnN0YXJ0T2ZXZWVrLmNsb25lKCkuYWRkKC03LCAnZGF5cycpO1xyXG4gICAgICAgICAgICB0aGlzLl92YWNhbmN5U2VydmljZVxyXG4gICAgICAgICAgICAgICAgLmdldFZhY2FuY2llcyQodGhpcy5fdXNlclNlcnZpY2UuU2VsZWN0ZWRDZW50cmUuaWQsIGxhc3RXZWVrLnRvRGF0ZSgpKVxyXG4gICAgICAgICAgICAgICAgLmZsYXRNYXAobGFzdFdlZWtWYWNhbmNpZXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBvYnNlcnZhYmxlcyA9IG5ldyBBcnJheTxPYnNlcnZhYmxlPGFueT4+KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgZm9yIChsZXQgaTogbnVtYmVyID0gMDsgaSA8IGxhc3RXZWVrVmFjYW5jaWVzLmxlbmd0aDsgKytpKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgbGFzdFdlZWs6IFZhY2FuY3lEdG8gPSBsYXN0V2Vla1ZhY2FuY2llc1tpXTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCB0aGlzV2Vla1ZhY2FuY2llczogVmFjYW5jeUR0byA9IG5ldyBWYWNhbmN5RHRvKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXNXZWVrVmFjYW5jaWVzLmluaXQoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ0NlbnRyZUlkJzogbGFzdFdlZWsuY2VudHJlSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnUm9vbUlkJzogbGFzdFdlZWsucm9vbUlkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ1ZhY2FuY2llcyc6IHt9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmb3IgKGxldCBkYXRlIGluIGxhc3RXZWVrLnZhY2FuY2llcylcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChsYXN0V2Vlay52YWNhbmNpZXMuaGFzT3duUHJvcGVydHkoZGF0ZSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzV2Vla1ZhY2FuY2llcy52YWNhbmNpZXNbdGhpcy5nZXREYXRlS2V5KG1vbWVudChkYXRlKS5hZGQoNywgJ2RheXMnKSldXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID0gbGFzdFdlZWsudmFjYW5jaWVzW2RhdGVdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IG9icyA9IHRoaXMuY3JlYXRlVXBkYXRlUm9vbVZhY2FuY3kkKHRoaXNXZWVrVmFjYW5jaWVzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2YWJsZXMucHVzaChvYnMpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIE9ic2VydmFibGUuZm9ya0pvaW4ob2JzZXJ2YWJsZXMpO1xyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLmNsZWFyKHNhdmluZ1RvYXN0LnRvYXN0SWQsIHNhdmluZ1RvYXN0LnRvYXN0Q29udGFpbmVySWQpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLnBvcEFzeW5jKCdzdWNjZXNzJywgJ1NhdmVkJywgJycpO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGVyciA9PiB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcihzYXZpbmdUb2FzdC50b2FzdElkLCBzYXZpbmdUb2FzdC50b2FzdENvbnRhaW5lcklkKSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzaG93V2VlayhkYXk6IG1vbWVudC5Nb21lbnQpOiBzdHJpbmcge1xyXG5cclxuICAgICAgICBsZXQgZGlzcGxheVdlZWs6IHN0cmluZyA9ICcnO1xyXG4gICAgICAgIGlmIChtb21lbnQoKS5pc1NhbWUoZGF5LCAnd2VlaycpKSB7XHJcbiAgICAgICAgICAgIGRpc3BsYXlXZWVrID0gJ1RoaXMgV2Vlayc7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgZGlzcGxheVdlZWsgPSBgJHt0aGlzLmRheXNPZldlZWtbMF0uZm9ybWF0KCdEbyBNTU0nKX0gdG8gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAke3RoaXMuZGF5c09mV2Vla1t0aGlzLmRheXNPZldlZWsubGVuZ3RoIC0gMV0uZm9ybWF0KCdEbyBNTU0nKX1gO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gZGlzcGxheVdlZWs7XHJcbiAgICB9XHJcbn1cclxuIl19
