"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../../api/index');
var VacanciesService = (function () {
    function VacanciesService(_apiService) {
        this._apiService = _apiService;
    }
    VacanciesService.prototype.getVacancies$ = function (centreId, startDate) {
        return this._apiService.room_GetAllVacanciesAsync(centreId, startDate);
    };
    VacanciesService.prototype.CreateUpdateVacancies$ = function (createUpdateVacancyDto) {
        return this._apiService.room_CreateUpdateVacanciesAsync(createUpdateVacancyDto);
    };
    VacanciesService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.ApiClientService])
    ], VacanciesService);
    return VacanciesService;
}());
exports.VacanciesService = VacanciesService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3ZhY2FuY2llcy92YWNhbmNpZXNTZXJ2aWNlL3ZhY2FuY2llcy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBMkIsZUFBZSxDQUFDLENBQUE7QUFFM0Msc0JBQXlELHVCQUF1QixDQUFDLENBQUE7QUFHakY7SUFFSSwwQkFBb0IsV0FBNkI7UUFBN0IsZ0JBQVcsR0FBWCxXQUFXLENBQWtCO0lBQ2pELENBQUM7SUFFTSx3Q0FBYSxHQUFwQixVQUFxQixRQUFnQixFQUFFLFNBQWU7UUFDbEQsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMseUJBQXlCLENBQUMsUUFBUSxFQUFFLFNBQVMsQ0FBQyxDQUFDO0lBQzNFLENBQUM7SUFFTSxpREFBc0IsR0FBN0IsVUFBOEIsc0JBQThDO1FBQ3hFLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLCtCQUErQixDQUFDLHNCQUFzQixDQUFDLENBQUM7SUFDcEYsQ0FBQztJQVpMO1FBQUMsaUJBQVUsRUFBRTs7d0JBQUE7SUFhYix1QkFBQztBQUFELENBWkEsQUFZQyxJQUFBO0FBWlksd0JBQWdCLG1CQVk1QixDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3ZhY2FuY2llcy92YWNhbmNpZXNTZXJ2aWNlL3ZhY2FuY2llcy5zZXJ2aWNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgQXBpQ2xpZW50U2VydmljZSwgQ3JlYXRlVXBkYXRlVmFjYW5jeUR0byB9IGZyb20gJy4uLy4uLy4uLy4uL2FwaS9pbmRleCc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBWYWNhbmNpZXNTZXJ2aWNlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9hcGlTZXJ2aWNlOiBBcGlDbGllbnRTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldFZhY2FuY2llcyQoY2VudHJlSWQ6IHN0cmluZywgc3RhcnREYXRlOiBEYXRlKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2FwaVNlcnZpY2Uucm9vbV9HZXRBbGxWYWNhbmNpZXNBc3luYyhjZW50cmVJZCwgc3RhcnREYXRlKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgQ3JlYXRlVXBkYXRlVmFjYW5jaWVzJChjcmVhdGVVcGRhdGVWYWNhbmN5RHRvOiBDcmVhdGVVcGRhdGVWYWNhbmN5RHRvKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2FwaVNlcnZpY2Uucm9vbV9DcmVhdGVVcGRhdGVWYWNhbmNpZXNBc3luYyhjcmVhdGVVcGRhdGVWYWNhbmN5RHRvKTtcclxuICAgIH1cclxufVxyXG4iXX0=
