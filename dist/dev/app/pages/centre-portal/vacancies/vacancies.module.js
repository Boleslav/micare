"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../shared/index');
var index_2 = require('./index');
var VacanciesModule = (function () {
    function VacanciesModule() {
    }
    VacanciesModule = __decorate([
        core_1.NgModule({
            imports: [
                index_1.SharedModule
            ],
            declarations: [index_2.VacanciesComponent, index_2.VacanciesTableComponent],
            providers: [index_2.VacanciesService],
            exports: [index_2.VacanciesComponent, index_2.VacanciesTableComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], VacanciesModule);
    return VacanciesModule;
}());
exports.VacanciesModule = VacanciesModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3ZhY2FuY2llcy92YWNhbmNpZXMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBeUIsZUFBZSxDQUFDLENBQUE7QUFDekMsc0JBQTZCLHVCQUF1QixDQUFDLENBQUE7QUFFckQsc0JBQThFLFNBQVMsQ0FBQyxDQUFBO0FBV3hGO0lBQUE7SUFBK0IsQ0FBQztJQVRoQztRQUFDLGVBQVEsQ0FBQztZQUNOLE9BQU8sRUFBRTtnQkFDTCxvQkFBWTthQUNmO1lBQ0QsWUFBWSxFQUFFLENBQUMsMEJBQWtCLEVBQUUsK0JBQXVCLENBQUM7WUFDM0QsU0FBUyxFQUFFLENBQUMsd0JBQWdCLENBQUM7WUFDN0IsT0FBTyxFQUFFLENBQUMsMEJBQWtCLEVBQUUsK0JBQXVCLENBQUM7U0FDekQsQ0FBQzs7dUJBQUE7SUFFNkIsc0JBQUM7QUFBRCxDQUEvQixBQUFnQyxJQUFBO0FBQW5CLHVCQUFlLGtCQUFJLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL2NlbnRyZS1wb3J0YWwvdmFjYW5jaWVzL3ZhY2FuY2llcy5tb2R1bGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTaGFyZWRNb2R1bGUgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvaW5kZXgnO1xyXG5cclxuaW1wb3J0IHsgVmFjYW5jaWVzQ29tcG9uZW50LCBWYWNhbmNpZXNUYWJsZUNvbXBvbmVudCwgVmFjYW5jaWVzU2VydmljZSB9IGZyb20gJy4vaW5kZXgnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBTaGFyZWRNb2R1bGVcclxuICAgIF0sXHJcbiAgICBkZWNsYXJhdGlvbnM6IFtWYWNhbmNpZXNDb21wb25lbnQsIFZhY2FuY2llc1RhYmxlQ29tcG9uZW50XSxcclxuICAgIHByb3ZpZGVyczogW1ZhY2FuY2llc1NlcnZpY2VdLFxyXG4gICAgZXhwb3J0czogW1ZhY2FuY2llc0NvbXBvbmVudCwgVmFjYW5jaWVzVGFibGVDb21wb25lbnRdXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgVmFjYW5jaWVzTW9kdWxlIHsgfVxyXG4iXX0=
