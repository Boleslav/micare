"use strict";
var index_1 = require('../../../shared/index');
var index_2 = require('./index');
exports.VacanciesRoutes = [
    {
        path: 'vacancies',
        component: index_2.VacanciesComponent,
        canActivate: [index_1.NavigationGuardService]
    }
];

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3ZhY2FuY2llcy92YWNhbmNpZXMucm91dGVzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFDQSxzQkFBdUMsdUJBQXVCLENBQUMsQ0FBQTtBQUMvRCxzQkFBbUMsU0FBUyxDQUFDLENBQUE7QUFFaEMsdUJBQWUsR0FBWTtJQUN2QztRQUNDLElBQUksRUFBRSxXQUFXO1FBQ2pCLFNBQVMsRUFBRSwwQkFBa0I7UUFDN0IsV0FBVyxFQUFFLENBQUMsOEJBQXNCLENBQUM7S0FDckM7Q0FDRCxDQUFDIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3ZhY2FuY2llcy92YWNhbmNpZXMucm91dGVzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBOYXZpZ2F0aW9uR3VhcmRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL2luZGV4JztcclxuaW1wb3J0IHsgVmFjYW5jaWVzQ29tcG9uZW50IH0gZnJvbSAnLi9pbmRleCc7XHJcblxyXG5leHBvcnQgY29uc3QgVmFjYW5jaWVzUm91dGVzOiBSb3V0ZVtdID0gW1xyXG5cdHtcclxuXHRcdHBhdGg6ICd2YWNhbmNpZXMnLFxyXG5cdFx0Y29tcG9uZW50OiBWYWNhbmNpZXNDb21wb25lbnQsXHJcblx0XHRjYW5BY3RpdmF0ZTogW05hdmlnYXRpb25HdWFyZFNlcnZpY2VdXHJcblx0fVxyXG5dO1xyXG4iXX0=
