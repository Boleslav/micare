"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var adminApprovalsFilter_model_1 = require('./adminApprovalsFilter.model');
var adminApprovals_service_1 = require('../adminApprovalsService/adminApprovals.service');
var index_1 = require('../../../../api/index');
var angular2_toaster_1 = require('angular2-toaster');
var index_2 = require('../../../../shared/index');
var AdminApprovalsTableComponent = (function () {
    function AdminApprovalsTableComponent(_adminAdpprovalsService, _toasterService) {
        this._adminAdpprovalsService = _adminAdpprovalsService;
        this._toasterService = _toasterService;
        this.DefaultApprovalStatus = 'Show All';
        this.centres = null;
        this.approvalStatuses = adminApprovalsFilter_model_1.AdminApprovalsFilter;
        this.approvalStatusFilter = {
            filterText: '',
            filterId: 0
        };
        this.searchTerm = '';
        this.setApprovalStatus(this.DefaultApprovalStatus);
    }
    AdminApprovalsTableComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._adminAdpprovalsService
            .getCentres$()
            .subscribe(function (res) {
            _this.centres = res;
            _this._toasterService.clear();
        });
        this._adminAdpprovalsService.searchCentre(this.searchTerm, {});
    };
    AdminApprovalsTableComponent.prototype.keys = function () {
        return Object.keys(this.approvalStatuses);
    };
    AdminApprovalsTableComponent.prototype.setApprovalStatus = function (status) {
        this.approvalStatusFilter.filterText = status;
        this.approvalStatusFilter.filterId = this.approvalStatuses[status];
    };
    AdminApprovalsTableComponent.prototype.filterByApprovalStatus = function (status) {
        this.setApprovalStatus(status);
        this._toasterService.clear();
        this._toasterService.pop(new index_2.SpinningToast('Loading'));
        this._adminAdpprovalsService.searchCentre(this.searchTerm, this.approvalStatusFilter.filterId === 4 ? {} : this.approvalStatusFilter.filterId);
    };
    AdminApprovalsTableComponent.prototype.getDefaultApprovalStatusId = function () {
        return this.approvalStatuses[this.DefaultApprovalStatus];
    };
    AdminApprovalsTableComponent.prototype.setCentreApprovalStatus = function (centre, approvalStatus) {
        var _this = this;
        var savingToast = this._toasterService.pop(new index_2.SpinningToast());
        var updateApprovalStatus = new index_1.UpdateCentreApprovalStatusDto();
        updateApprovalStatus.init({
            CentreId: centre.id
        });
        var approvalStatusId = this.approvalStatuses[approvalStatus];
        centre.approvalStatus = approvalStatusId;
        switch (approvalStatusId) {
            case 0:
                updateApprovalStatus.approvalStatusType = index_1.UpdateCentreApprovalStatusDtoApprovalStatusType.AwaitingApproval;
                break;
            case 1:
                updateApprovalStatus.approvalStatusType = index_1.UpdateCentreApprovalStatusDtoApprovalStatusType.DeniedApproval;
                break;
            case 2:
                updateApprovalStatus.approvalStatusType = index_1.UpdateCentreApprovalStatusDtoApprovalStatusType.Approved;
                break;
            case 3:
                updateApprovalStatus.approvalStatusType = index_1.UpdateCentreApprovalStatusDtoApprovalStatusType.UnClaimed;
                break;
            default:
                updateApprovalStatus.approvalStatusType = index_1.UpdateCentreApprovalStatusDtoApprovalStatusType.AwaitingApproval;
                break;
        }
        this._adminAdpprovalsService
            .setCentreApprovalStatus$(updateApprovalStatus)
            .subscribe(function (res) {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this._toasterService.popAsync('success', 'Saved', '');
        }, function (err) { return _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId); });
    };
    AdminApprovalsTableComponent.prototype.searchCentre = function (event) {
        this._toasterService.clear();
        this._toasterService.pop(new index_2.SpinningToast('Loading'));
        this._adminAdpprovalsService.searchCentre(event.target.value, this.approvalStatusFilter.filterId === 4 ? {} : this.approvalStatusFilter.filterId);
    };
    AdminApprovalsTableComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'admin-approvals-table-cmp',
            templateUrl: './adminApprovalsTable.component.html',
            styleUrls: ['./adminApprovalsTable.component.css']
        }), 
        __metadata('design:paramtypes', [adminApprovals_service_1.AdminApprovalsService, angular2_toaster_1.ToasterService])
    ], AdminApprovalsTableComponent);
    return AdminApprovalsTableComponent;
}());
exports.AdminApprovalsTableComponent = AdminApprovalsTableComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2FkbWluLWFwcHJvdmFscy9hZG1pbkFwcHJvdmFsc1RhYmxlL2FkbWluQXBwcm92YWxzVGFibGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBa0MsZUFBZSxDQUFDLENBQUE7QUFFbEQsMkNBQXFDLDhCQUE4QixDQUFDLENBQUE7QUFDcEUsdUNBQXNDLGlEQUFpRCxDQUFDLENBQUE7QUFDeEYsc0JBR08sdUJBQXVCLENBQUMsQ0FBQTtBQUUvQixpQ0FBK0Isa0JBQWtCLENBQUMsQ0FBQTtBQUNsRCxzQkFBOEIsMEJBQTBCLENBQUMsQ0FBQTtBQVN6RDtJQWFJLHNDQUFvQix1QkFBOEMsRUFDdEQsZUFBK0I7UUFEdkIsNEJBQXVCLEdBQXZCLHVCQUF1QixDQUF1QjtRQUN0RCxvQkFBZSxHQUFmLGVBQWUsQ0FBZ0I7UUFabEMsMEJBQXFCLEdBQVcsVUFBVSxDQUFDO1FBRXBELFlBQU8sR0FBZ0IsSUFBSSxDQUFDO1FBQzVCLHFCQUFnQixHQUFRLGlEQUFvQixDQUFDO1FBQzdDLHlCQUFvQixHQUFRO1lBQ3hCLFVBQVUsRUFBRSxFQUFFO1lBQ2QsUUFBUSxFQUFFLENBQUM7U0FDZCxDQUFDO1FBRU0sZUFBVSxHQUFXLEVBQUUsQ0FBQztRQUk1QixJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUM7SUFDdkQsQ0FBQztJQUVELCtDQUFRLEdBQVI7UUFBQSxpQkFTQztRQVBHLElBQUksQ0FBQyx1QkFBdUI7YUFDdkIsV0FBVyxFQUFFO2FBQ2IsU0FBUyxDQUFDLFVBQUEsR0FBRztZQUNWLEtBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1lBQ25CLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDakMsQ0FBQyxDQUFDLENBQUM7UUFDUCxJQUFJLENBQUMsdUJBQXVCLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQVUsRUFBRSxDQUFDLENBQUM7SUFDM0UsQ0FBQztJQUVELDJDQUFJLEdBQUo7UUFDSSxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBRUQsd0RBQWlCLEdBQWpCLFVBQWtCLE1BQWM7UUFDNUIsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUM7UUFDOUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDdkUsQ0FBQztJQUVELDZEQUFzQixHQUF0QixVQUF1QixNQUFjO1FBQ2pDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMvQixJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQzdCLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUkscUJBQWEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1FBSXZELElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxZQUFZLENBQ3JDLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsS0FBSyxDQUFDLEdBQVcsRUFBRSxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUNySCxDQUFDO0lBRUQsaUVBQTBCLEdBQTFCO1FBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQztJQUM3RCxDQUFDO0lBRUQsOERBQXVCLEdBQXZCLFVBQXdCLE1BQWlCLEVBQUUsY0FBc0I7UUFBakUsaUJBMkNDO1FBekNHLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUkscUJBQWEsRUFBRSxDQUFDLENBQUM7UUFFaEUsSUFBSSxvQkFBb0IsR0FBRyxJQUFJLHFDQUE2QixFQUFFLENBQUM7UUFDL0Qsb0JBQW9CLENBQUMsSUFBSSxDQUFDO1lBQ3RCLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtTQUN0QixDQUFDLENBQUM7UUFFSCxJQUFJLGdCQUFnQixHQUFXLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUVyRSxNQUFNLENBQUMsY0FBYyxHQUFHLGdCQUFnQixDQUFDO1FBRXpDLE1BQU0sQ0FBQyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQztZQUN2QixLQUFLLENBQUM7Z0JBQ0Ysb0JBQW9CLENBQUMsa0JBQWtCLEdBQUcsdURBQStDLENBQUMsZ0JBQWdCLENBQUM7Z0JBQzNHLEtBQUssQ0FBQztZQUVWLEtBQUssQ0FBQztnQkFDRixvQkFBb0IsQ0FBQyxrQkFBa0IsR0FBRyx1REFBK0MsQ0FBQyxjQUFjLENBQUM7Z0JBQ3pHLEtBQUssQ0FBQztZQUVWLEtBQUssQ0FBQztnQkFDRixvQkFBb0IsQ0FBQyxrQkFBa0IsR0FBRyx1REFBK0MsQ0FBQyxRQUFRLENBQUM7Z0JBQ25HLEtBQUssQ0FBQztZQUVWLEtBQUssQ0FBQztnQkFDRixvQkFBb0IsQ0FBQyxrQkFBa0IsR0FBRyx1REFBK0MsQ0FBQyxTQUFTLENBQUM7Z0JBQ3BHLEtBQUssQ0FBQztZQUVWO2dCQUNJLG9CQUFvQixDQUFDLGtCQUFrQixHQUFHLHVEQUErQyxDQUFDLGdCQUFnQixDQUFDO2dCQUMzRyxLQUFLLENBQUM7UUFDZCxDQUFDO1FBRUQsSUFBSSxDQUFDLHVCQUF1QjthQUN2Qix3QkFBd0IsQ0FBQyxvQkFBb0IsQ0FBQzthQUM5QyxTQUFTLENBQ1YsVUFBQSxHQUFHO1lBQ0MsS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxXQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUM5RSxLQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsT0FBTyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQzFELENBQUMsRUFDRCxVQUFDLEdBQUcsSUFBSyxPQUFBLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLGdCQUFnQixDQUFDLEVBQTdFLENBQTZFLENBQUMsQ0FBQztJQUNoRyxDQUFDO0lBRUQsbURBQVksR0FBWixVQUFhLEtBQVU7UUFDbkIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUM3QixJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxJQUFJLHFCQUFhLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztRQUl2RCxJQUFJLENBQUMsdUJBQXVCLENBQUMsWUFBWSxDQUNyQyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsUUFBUSxLQUFLLENBQUMsR0FBVyxFQUFFLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3hILENBQUM7SUFqSEw7UUFBQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSwyQkFBMkI7WUFDckMsV0FBVyxFQUFFLHNDQUFzQztZQUNuRCxTQUFTLEVBQUUsQ0FBQyxxQ0FBcUMsQ0FBQztTQUNyRCxDQUFDOztvQ0FBQTtJQTZHRixtQ0FBQztBQUFELENBM0dBLEFBMkdDLElBQUE7QUEzR1ksb0NBQTRCLCtCQTJHeEMsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9hZG1pbi1hcHByb3ZhbHMvYWRtaW5BcHByb3ZhbHNUYWJsZS9hZG1pbkFwcHJvdmFsc1RhYmxlLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBBZG1pbkFwcHJvdmFsc0ZpbHRlciB9IGZyb20gJy4vYWRtaW5BcHByb3ZhbHNGaWx0ZXIubW9kZWwnO1xyXG5pbXBvcnQgeyBBZG1pbkFwcHJvdmFsc1NlcnZpY2UgfSBmcm9tICcuLi9hZG1pbkFwcHJvdmFsc1NlcnZpY2UvYWRtaW5BcHByb3ZhbHMuc2VydmljZSc7XHJcbmltcG9ydCB7XHJcbiAgICBDZW50cmVEdG8sIFVwZGF0ZUNlbnRyZUFwcHJvdmFsU3RhdHVzRHRvLFxyXG4gICAgVXBkYXRlQ2VudHJlQXBwcm92YWxTdGF0dXNEdG9BcHByb3ZhbFN0YXR1c1R5cGUsIFN0YXR1c1xyXG59IGZyb20gJy4uLy4uLy4uLy4uL2FwaS9pbmRleCc7XHJcblxyXG5pbXBvcnQgeyBUb2FzdGVyU2VydmljZSB9IGZyb20gJ2FuZ3VsYXIyLXRvYXN0ZXInO1xyXG5pbXBvcnQgeyBTcGlubmluZ1RvYXN0IH0gZnJvbSAnLi4vLi4vLi4vLi4vc2hhcmVkL2luZGV4JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHNlbGVjdG9yOiAnYWRtaW4tYXBwcm92YWxzLXRhYmxlLWNtcCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vYWRtaW5BcHByb3ZhbHNUYWJsZS5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9hZG1pbkFwcHJvdmFsc1RhYmxlLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEFkbWluQXBwcm92YWxzVGFibGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIHJlYWRvbmx5IERlZmF1bHRBcHByb3ZhbFN0YXR1czogc3RyaW5nID0gJ1Nob3cgQWxsJztcclxuXHJcbiAgICBjZW50cmVzOiBDZW50cmVEdG9bXSA9IG51bGw7XHJcbiAgICBhcHByb3ZhbFN0YXR1c2VzOiBhbnkgPSBBZG1pbkFwcHJvdmFsc0ZpbHRlcjtcclxuICAgIGFwcHJvdmFsU3RhdHVzRmlsdGVyOiBhbnkgPSB7XHJcbiAgICAgICAgZmlsdGVyVGV4dDogJycsXHJcbiAgICAgICAgZmlsdGVySWQ6IDBcclxuICAgIH07XHJcblxyXG4gICAgcHJpdmF0ZSBzZWFyY2hUZXJtOiBzdHJpbmcgPSAnJztcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9hZG1pbkFkcHByb3ZhbHNTZXJ2aWNlOiBBZG1pbkFwcHJvdmFsc1NlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBfdG9hc3RlclNlcnZpY2U6IFRvYXN0ZXJTZXJ2aWNlKSB7XHJcbiAgICAgICAgdGhpcy5zZXRBcHByb3ZhbFN0YXR1cyh0aGlzLkRlZmF1bHRBcHByb3ZhbFN0YXR1cyk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKTogdm9pZCB7XHJcblxyXG4gICAgICAgIHRoaXMuX2FkbWluQWRwcHJvdmFsc1NlcnZpY2VcclxuICAgICAgICAgICAgLmdldENlbnRyZXMkKClcclxuICAgICAgICAgICAgLnN1YnNjcmliZShyZXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jZW50cmVzID0gcmVzO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5fYWRtaW5BZHBwcm92YWxzU2VydmljZS5zZWFyY2hDZW50cmUodGhpcy5zZWFyY2hUZXJtLCA8U3RhdHVzPnt9KTtcclxuICAgIH1cclxuXHJcbiAgICBrZXlzKCk6IEFycmF5PHN0cmluZz4ge1xyXG4gICAgICAgIHJldHVybiBPYmplY3Qua2V5cyh0aGlzLmFwcHJvdmFsU3RhdHVzZXMpO1xyXG4gICAgfVxyXG5cclxuICAgIHNldEFwcHJvdmFsU3RhdHVzKHN0YXR1czogc3RyaW5nKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5hcHByb3ZhbFN0YXR1c0ZpbHRlci5maWx0ZXJUZXh0ID0gc3RhdHVzO1xyXG4gICAgICAgIHRoaXMuYXBwcm92YWxTdGF0dXNGaWx0ZXIuZmlsdGVySWQgPSB0aGlzLmFwcHJvdmFsU3RhdHVzZXNbc3RhdHVzXTtcclxuICAgIH1cclxuXHJcbiAgICBmaWx0ZXJCeUFwcHJvdmFsU3RhdHVzKHN0YXR1czogc3RyaW5nKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5zZXRBcHByb3ZhbFN0YXR1cyhzdGF0dXMpO1xyXG4gICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLmNsZWFyKCk7XHJcbiAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UucG9wKG5ldyBTcGlubmluZ1RvYXN0KCdMb2FkaW5nJykpO1xyXG4gICAgICAgIC8vIFRoZXJlIGlzIGN1cnJlbnRseSBubyB3YXkgaW4gbnN3YWcgdG8gcGFzcyBpbiBvcHRpb25hbCBwYXJhbWV0ZXJzLlxyXG4gICAgICAgIC8vIFBsYW5uZWQgc29sdXRpb24gd2lsbCBiZSBkZWxpdmVyZWQgaW4gYW4gdW5rbm93biB0aW1lOlxyXG4gICAgICAgIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS9OU3dhZy9OU3dhZy9pc3N1ZXMvNjA4XHJcbiAgICAgICAgdGhpcy5fYWRtaW5BZHBwcm92YWxzU2VydmljZS5zZWFyY2hDZW50cmUoXHJcbiAgICAgICAgICAgIHRoaXMuc2VhcmNoVGVybSwgdGhpcy5hcHByb3ZhbFN0YXR1c0ZpbHRlci5maWx0ZXJJZCA9PT0gNCA/IDxTdGF0dXM+e30gOiB0aGlzLmFwcHJvdmFsU3RhdHVzRmlsdGVyLmZpbHRlcklkKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXREZWZhdWx0QXBwcm92YWxTdGF0dXNJZCgpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFwcHJvdmFsU3RhdHVzZXNbdGhpcy5EZWZhdWx0QXBwcm92YWxTdGF0dXNdO1xyXG4gICAgfVxyXG5cclxuICAgIHNldENlbnRyZUFwcHJvdmFsU3RhdHVzKGNlbnRyZTogQ2VudHJlRHRvLCBhcHByb3ZhbFN0YXR1czogc3RyaW5nKTogdm9pZCB7XHJcblxyXG4gICAgICAgIGxldCBzYXZpbmdUb2FzdCA9IHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLnBvcChuZXcgU3Bpbm5pbmdUb2FzdCgpKTtcclxuXHJcbiAgICAgICAgbGV0IHVwZGF0ZUFwcHJvdmFsU3RhdHVzID0gbmV3IFVwZGF0ZUNlbnRyZUFwcHJvdmFsU3RhdHVzRHRvKCk7XHJcbiAgICAgICAgdXBkYXRlQXBwcm92YWxTdGF0dXMuaW5pdCh7XHJcbiAgICAgICAgICAgIENlbnRyZUlkOiBjZW50cmUuaWRcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgbGV0IGFwcHJvdmFsU3RhdHVzSWQ6IG51bWJlciA9IHRoaXMuYXBwcm92YWxTdGF0dXNlc1thcHByb3ZhbFN0YXR1c107XHJcblxyXG4gICAgICAgIGNlbnRyZS5hcHByb3ZhbFN0YXR1cyA9IGFwcHJvdmFsU3RhdHVzSWQ7XHJcblxyXG4gICAgICAgIHN3aXRjaCAoYXBwcm92YWxTdGF0dXNJZCkge1xyXG4gICAgICAgICAgICBjYXNlIDA6XHJcbiAgICAgICAgICAgICAgICB1cGRhdGVBcHByb3ZhbFN0YXR1cy5hcHByb3ZhbFN0YXR1c1R5cGUgPSBVcGRhdGVDZW50cmVBcHByb3ZhbFN0YXR1c0R0b0FwcHJvdmFsU3RhdHVzVHlwZS5Bd2FpdGluZ0FwcHJvdmFsO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcblxyXG4gICAgICAgICAgICBjYXNlIDE6XHJcbiAgICAgICAgICAgICAgICB1cGRhdGVBcHByb3ZhbFN0YXR1cy5hcHByb3ZhbFN0YXR1c1R5cGUgPSBVcGRhdGVDZW50cmVBcHByb3ZhbFN0YXR1c0R0b0FwcHJvdmFsU3RhdHVzVHlwZS5EZW5pZWRBcHByb3ZhbDtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgICAgICAgY2FzZSAyOlxyXG4gICAgICAgICAgICAgICAgdXBkYXRlQXBwcm92YWxTdGF0dXMuYXBwcm92YWxTdGF0dXNUeXBlID0gVXBkYXRlQ2VudHJlQXBwcm92YWxTdGF0dXNEdG9BcHByb3ZhbFN0YXR1c1R5cGUuQXBwcm92ZWQ7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuXHJcbiAgICAgICAgICAgIGNhc2UgMzpcclxuICAgICAgICAgICAgICAgIHVwZGF0ZUFwcHJvdmFsU3RhdHVzLmFwcHJvdmFsU3RhdHVzVHlwZSA9IFVwZGF0ZUNlbnRyZUFwcHJvdmFsU3RhdHVzRHRvQXBwcm92YWxTdGF0dXNUeXBlLlVuQ2xhaW1lZDtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgICAgIHVwZGF0ZUFwcHJvdmFsU3RhdHVzLmFwcHJvdmFsU3RhdHVzVHlwZSA9IFVwZGF0ZUNlbnRyZUFwcHJvdmFsU3RhdHVzRHRvQXBwcm92YWxTdGF0dXNUeXBlLkF3YWl0aW5nQXBwcm92YWw7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuX2FkbWluQWRwcHJvdmFsc1NlcnZpY2VcclxuICAgICAgICAgICAgLnNldENlbnRyZUFwcHJvdmFsU3RhdHVzJCh1cGRhdGVBcHByb3ZhbFN0YXR1cylcclxuICAgICAgICAgICAgLnN1YnNjcmliZShcclxuICAgICAgICAgICAgcmVzID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLmNsZWFyKHNhdmluZ1RvYXN0LnRvYXN0SWQsIHNhdmluZ1RvYXN0LnRvYXN0Q29udGFpbmVySWQpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UucG9wQXN5bmMoJ3N1Y2Nlc3MnLCAnU2F2ZWQnLCAnJyk7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIChlcnIpID0+IHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLmNsZWFyKHNhdmluZ1RvYXN0LnRvYXN0SWQsIHNhdmluZ1RvYXN0LnRvYXN0Q29udGFpbmVySWQpKTtcclxuICAgIH1cclxuXHJcbiAgICBzZWFyY2hDZW50cmUoZXZlbnQ6IGFueSk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLmNsZWFyKCk7XHJcbiAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UucG9wKG5ldyBTcGlubmluZ1RvYXN0KCdMb2FkaW5nJykpO1xyXG4gICAgICAgIC8vIFRoZXJlIGlzIGN1cnJlbnRseSBubyB3YXkgaW4gbnN3YWcgdG8gcGFzcyBpbiBvcHRpb25hbCBwYXJhbWV0ZXJzLlxyXG4gICAgICAgIC8vIFBsYW5uZWQgc29sdXRpb24gd2lsbCBiZSBkZWxpdmVyZWQgaW4gYW4gdW5rbm93biB0aW1lOlxyXG4gICAgICAgIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS9OU3dhZy9OU3dhZy9pc3N1ZXMvNjA4XHJcbiAgICAgICAgdGhpcy5fYWRtaW5BZHBwcm92YWxzU2VydmljZS5zZWFyY2hDZW50cmUoXHJcbiAgICAgICAgICAgIGV2ZW50LnRhcmdldC52YWx1ZSwgdGhpcy5hcHByb3ZhbFN0YXR1c0ZpbHRlci5maWx0ZXJJZCA9PT0gNCA/IDxTdGF0dXM+e30gOiB0aGlzLmFwcHJvdmFsU3RhdHVzRmlsdGVyLmZpbHRlcklkKTtcclxuICAgIH1cclxufVxyXG4iXX0=
