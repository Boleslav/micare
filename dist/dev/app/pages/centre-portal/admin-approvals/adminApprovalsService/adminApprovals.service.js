"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var rxjs_1 = require('rxjs');
var index_1 = require('../../../../api/index');
var AdminApprovalsService = (function () {
    function AdminApprovalsService(_apiService) {
        this._apiService = _apiService;
        this.searchTermStream = new rxjs_1.Subject();
    }
    AdminApprovalsService.prototype.searchCentre = function (term, status) {
        this.searchTermStream.next({ term: term, status: status });
    };
    AdminApprovalsService.prototype.getCentres$ = function () {
        var _this = this;
        return this.searchTermStream
            .debounceTime(300)
            .distinctUntilChanged()
            .switchMap(function (search) { return _this._apiService.sysAdmin_GetCentres(search.term, search.status); });
    };
    AdminApprovalsService.prototype.setCentreApprovalStatus$ = function (updateApprovalStatusDto) {
        return this._apiService.sysAdmin_SetCentreApprovalStatus(updateApprovalStatusDto);
    };
    AdminApprovalsService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.ApiClientService])
    ], AdminApprovalsService);
    return AdminApprovalsService;
}());
exports.AdminApprovalsService = AdminApprovalsService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2FkbWluLWFwcHJvdmFscy9hZG1pbkFwcHJvdmFsc1NlcnZpY2UvYWRtaW5BcHByb3ZhbHMuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQTJCLGVBQWUsQ0FBQyxDQUFBO0FBQzNDLHFCQUFvQyxNQUFNLENBQUMsQ0FBQTtBQUUzQyxzQkFBMkUsdUJBQXVCLENBQUMsQ0FBQTtBQUduRztJQUlJLCtCQUFvQixXQUE2QjtRQUE3QixnQkFBVyxHQUFYLFdBQVcsQ0FBa0I7UUFGekMscUJBQWdCLEdBQUcsSUFBSSxjQUFPLEVBQW9DLENBQUM7SUFHM0UsQ0FBQztJQUVNLDRDQUFZLEdBQW5CLFVBQW9CLElBQVksRUFBRSxNQUFjO1FBQzVDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDO0lBQy9ELENBQUM7SUFFTSwyQ0FBVyxHQUFsQjtRQUFBLGlCQUtDO1FBSkcsTUFBTSxDQUFDLElBQUksQ0FBQyxnQkFBZ0I7YUFDdkIsWUFBWSxDQUFDLEdBQUcsQ0FBQzthQUNqQixvQkFBb0IsRUFBRTthQUN0QixTQUFTLENBQUMsVUFBQyxNQUF3QyxJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBaEUsQ0FBZ0UsQ0FBQyxDQUFDO0lBQ25JLENBQUM7SUFFTSx3REFBd0IsR0FBL0IsVUFBZ0MsdUJBQXNEO1FBQ2xGLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGdDQUFnQyxDQUFDLHVCQUF1QixDQUFDLENBQUM7SUFDdEYsQ0FBQztJQXJCTDtRQUFDLGlCQUFVLEVBQUU7OzZCQUFBO0lBc0JiLDRCQUFDO0FBQUQsQ0FyQkEsQUFxQkMsSUFBQTtBQXJCWSw2QkFBcUIsd0JBcUJqQyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2FkbWluLWFwcHJvdmFscy9hZG1pbkFwcHJvdmFsc1NlcnZpY2UvYWRtaW5BcHByb3ZhbHMuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgQXBpQ2xpZW50U2VydmljZSwgQ2VudHJlRHRvLCBVcGRhdGVDZW50cmVBcHByb3ZhbFN0YXR1c0R0byB9IGZyb20gJy4uLy4uLy4uLy4uL2FwaS9pbmRleCc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBBZG1pbkFwcHJvdmFsc1NlcnZpY2Uge1xyXG5cclxuICAgIHByaXZhdGUgc2VhcmNoVGVybVN0cmVhbSA9IG5ldyBTdWJqZWN0PHsgdGVybTogc3RyaW5nLCBzdGF0dXM6IG51bWJlciB9PigpO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX2FwaVNlcnZpY2U6IEFwaUNsaWVudFNlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc2VhcmNoQ2VudHJlKHRlcm06IHN0cmluZywgc3RhdHVzOiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLnNlYXJjaFRlcm1TdHJlYW0ubmV4dCh7IHRlcm06IHRlcm0sIHN0YXR1czogc3RhdHVzIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRDZW50cmVzJCgpOiBPYnNlcnZhYmxlPENlbnRyZUR0b1tdPiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VhcmNoVGVybVN0cmVhbVxyXG4gICAgICAgICAgICAuZGVib3VuY2VUaW1lKDMwMClcclxuICAgICAgICAgICAgLmRpc3RpbmN0VW50aWxDaGFuZ2VkKClcclxuICAgICAgICAgICAgLnN3aXRjaE1hcCgoc2VhcmNoOiB7IHRlcm06IHN0cmluZywgc3RhdHVzOiBudW1iZXIgfSkgPT4gdGhpcy5fYXBpU2VydmljZS5zeXNBZG1pbl9HZXRDZW50cmVzKHNlYXJjaC50ZXJtLCBzZWFyY2guc3RhdHVzKSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHNldENlbnRyZUFwcHJvdmFsU3RhdHVzJCh1cGRhdGVBcHByb3ZhbFN0YXR1c0R0bzogVXBkYXRlQ2VudHJlQXBwcm92YWxTdGF0dXNEdG8pOiBPYnNlcnZhYmxlPHZvaWQ+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fYXBpU2VydmljZS5zeXNBZG1pbl9TZXRDZW50cmVBcHByb3ZhbFN0YXR1cyh1cGRhdGVBcHByb3ZhbFN0YXR1c0R0byk7XHJcbiAgICB9XHJcbn1cclxuIl19
