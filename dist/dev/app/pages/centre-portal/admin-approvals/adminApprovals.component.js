"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var AdminApprovalsComponent = (function () {
    function AdminApprovalsComponent() {
    }
    AdminApprovalsComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'admin-approvals-cmp',
            templateUrl: './adminApprovals.component.html'
        }), 
        __metadata('design:paramtypes', [])
    ], AdminApprovalsComponent);
    return AdminApprovalsComponent;
}());
exports.AdminApprovalsComponent = AdminApprovalsComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2FkbWluLWFwcHJvdmFscy9hZG1pbkFwcHJvdmFscy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUF3QixlQUFlLENBQUMsQ0FBQTtBQVF4QztJQUFBO0lBQXNDLENBQUM7SUFOdkM7UUFBQyxnQkFBUyxDQUFDO1lBQ1YsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ2hCLFFBQVEsRUFBRSxxQkFBcUI7WUFDL0IsV0FBVyxFQUFFLGlDQUFpQztTQUNqRCxDQUFDOzsrQkFBQTtJQUVvQyw4QkFBQztBQUFELENBQXRDLEFBQXVDLElBQUE7QUFBMUIsK0JBQXVCLDBCQUFHLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL2NlbnRyZS1wb3J0YWwvYWRtaW4tYXBwcm92YWxzL2FkbWluQXBwcm92YWxzLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50fSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG5cdG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICBzZWxlY3RvcjogJ2FkbWluLWFwcHJvdmFscy1jbXAnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2FkbWluQXBwcm92YWxzLmNvbXBvbmVudC5odG1sJ1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEFkbWluQXBwcm92YWxzQ29tcG9uZW50IHt9XHJcbiJdfQ==
