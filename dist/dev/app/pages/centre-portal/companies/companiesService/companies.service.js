"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../../api/index');
var CompaniesService = (function () {
    function CompaniesService(_apiService) {
        this._apiService = _apiService;
    }
    CompaniesService.prototype.CreateUpdateCompany$ = function (companyDto) {
        return this._apiService.company_CreateUpdateCompanyAsync(companyDto);
    };
    CompaniesService.prototype.GetCompanies$ = function () {
        return this._apiService.company_GetAll();
    };
    CompaniesService.prototype.GetCompany$ = function (companyId) {
        return this._apiService.company_Get(companyId);
    };
    CompaniesService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.ApiClientService])
    ], CompaniesService);
    return CompaniesService;
}());
exports.CompaniesService = CompaniesService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NvbXBhbmllcy9jb21wYW5pZXNTZXJ2aWNlL2NvbXBhbmllcy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBMkIsZUFBZSxDQUFDLENBQUE7QUFHM0Msc0JBQTZDLHVCQUF1QixDQUFDLENBQUE7QUFHckU7SUFFSSwwQkFBb0IsV0FBNkI7UUFBN0IsZ0JBQVcsR0FBWCxXQUFXLENBQWtCO0lBRWpELENBQUM7SUFFTSwrQ0FBb0IsR0FBM0IsVUFBNEIsVUFBc0I7UUFDOUMsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsZ0NBQWdDLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDekUsQ0FBQztJQUVNLHdDQUFhLEdBQXBCO1FBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxFQUFFLENBQUM7SUFDN0MsQ0FBQztJQUVNLHNDQUFXLEdBQWxCLFVBQW1CLFNBQWlCO1FBQ2hDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUNuRCxDQUFDO0lBakJMO1FBQUMsaUJBQVUsRUFBRTs7d0JBQUE7SUFxQmIsdUJBQUM7QUFBRCxDQXBCQSxBQW9CQyxJQUFBO0FBcEJZLHdCQUFnQixtQkFvQjVCLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL2NlbnRyZS1wb3J0YWwvY29tcGFuaWVzL2NvbXBhbmllc1NlcnZpY2UvY29tcGFuaWVzLnNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5cbmltcG9ydCB7IEFwaUNsaWVudFNlcnZpY2UsIENvbXBhbnlEdG8gfSBmcm9tICcuLi8uLi8uLi8uLi9hcGkvaW5kZXgnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgQ29tcGFuaWVzU2VydmljZSB7XG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9hcGlTZXJ2aWNlOiBBcGlDbGllbnRTZXJ2aWNlKSB7XG5cbiAgICB9XG5cbiAgICBwdWJsaWMgQ3JlYXRlVXBkYXRlQ29tcGFueSQoY29tcGFueUR0bzogQ29tcGFueUR0byk6IE9ic2VydmFibGU8Q29tcGFueUR0bz4ge1xuICAgICAgICByZXR1cm4gdGhpcy5fYXBpU2VydmljZS5jb21wYW55X0NyZWF0ZVVwZGF0ZUNvbXBhbnlBc3luYyhjb21wYW55RHRvKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgR2V0Q29tcGFuaWVzJCgpOiBPYnNlcnZhYmxlPENvbXBhbnlEdG9bXT4ge1xuICAgICAgICByZXR1cm4gdGhpcy5fYXBpU2VydmljZS5jb21wYW55X0dldEFsbCgpO1xuICAgIH1cblxuICAgIHB1YmxpYyBHZXRDb21wYW55JChjb21wYW55SWQgOnN0cmluZyk6IE9ic2VydmFibGU8Q29tcGFueUR0bz4ge1xuICAgICAgICByZXR1cm4gdGhpcy5fYXBpU2VydmljZS5jb21wYW55X0dldChjb21wYW55SWQpO1xuICAgIH1cblxuXG5cbn1cbiJdfQ==
