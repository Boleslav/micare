"use strict";
var index_1 = require('../../../shared/index');
var index_2 = require('./index');
exports.CompaniesRoutes = [
    {
        path: 'companies',
        component: index_2.CompaniesComponent,
        canActivate: [index_1.NavigationGuardService],
        children: []
    },
    {
        path: 'companies/:companyId/centres',
        component: index_2.CompanyCentresComponent,
        canActivate: [index_1.NavigationGuardService],
    },
    {
        path: 'companies/:companyId/staff',
        component: index_2.CompanyStaffComponent,
        canActivate: [index_1.NavigationGuardService],
    }
];

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NvbXBhbmllcy9jb21wYW5pZXMucm91dGVzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFFQSxzQkFBdUMsdUJBQXVCLENBQUMsQ0FBQTtBQUMvRCxzQkFBbUYsU0FBUyxDQUFDLENBQUE7QUFFaEYsdUJBQWUsR0FBWTtJQUN2QztRQUNDLElBQUksRUFBRSxXQUFXO1FBQ2pCLFNBQVMsRUFBRSwwQkFBa0I7UUFDN0IsV0FBVyxFQUFFLENBQUMsOEJBQXNCLENBQUM7UUFDbkMsUUFBUSxFQUFFLEVBRVQ7S0FDSDtJQUNBO1FBQ0UsSUFBSSxFQUFFLDhCQUE4QjtRQUNwQyxTQUFTLEVBQUUsK0JBQXVCO1FBQ2xDLFdBQVcsRUFBRSxDQUFDLDhCQUFzQixDQUFDO0tBQ3RDO0lBQ0Q7UUFDRSxJQUFJLEVBQUUsNEJBQTRCO1FBQ2xDLFNBQVMsRUFBRSw2QkFBcUI7UUFDaEMsV0FBVyxFQUFFLENBQUMsOEJBQXNCLENBQUM7S0FDdEM7Q0FDRixDQUFDIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NvbXBhbmllcy9jb21wYW5pZXMucm91dGVzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuXG5pbXBvcnQgeyBOYXZpZ2F0aW9uR3VhcmRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL2luZGV4JztcbmltcG9ydCB7IENvbXBhbmllc0NvbXBvbmVudCwgQ29tcGFueUNlbnRyZXNDb21wb25lbnQsIENvbXBhbnlTdGFmZkNvbXBvbmVudCB9IGZyb20gJy4vaW5kZXgnO1xuXG5leHBvcnQgY29uc3QgQ29tcGFuaWVzUm91dGVzOiBSb3V0ZVtdID0gW1xuXHR7XG5cdFx0cGF0aDogJ2NvbXBhbmllcycsXG5cdFx0Y29tcG9uZW50OiBDb21wYW5pZXNDb21wb25lbnQsXG5cdFx0Y2FuQWN0aXZhdGU6IFtOYXZpZ2F0aW9uR3VhcmRTZXJ2aWNlXSxcbiAgICBjaGlsZHJlbjogW1xuXG4gICAgXVxuXHR9LFxuICB7XG4gICAgcGF0aDogJ2NvbXBhbmllcy86Y29tcGFueUlkL2NlbnRyZXMnLFxuICAgIGNvbXBvbmVudDogQ29tcGFueUNlbnRyZXNDb21wb25lbnQsXG4gICAgY2FuQWN0aXZhdGU6IFtOYXZpZ2F0aW9uR3VhcmRTZXJ2aWNlXSxcbiAgfSxcbiAge1xuICAgIHBhdGg6ICdjb21wYW5pZXMvOmNvbXBhbnlJZC9zdGFmZicsXG4gICAgY29tcG9uZW50OiBDb21wYW55U3RhZmZDb21wb25lbnQsXG4gICAgY2FuQWN0aXZhdGU6IFtOYXZpZ2F0aW9uR3VhcmRTZXJ2aWNlXSxcbiAgfVxuXTtcbiJdfQ==
