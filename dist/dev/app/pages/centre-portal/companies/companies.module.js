"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../shared/index');
var index_2 = require('./index');
var CompaniesModule = (function () {
    function CompaniesModule() {
    }
    CompaniesModule = __decorate([
        core_1.NgModule({
            imports: [
                index_1.SharedModule
            ],
            declarations: [
                index_2.CompaniesComponent,
                index_2.CompaniesTableComponent,
                index_2.CompanyExclussiveStatusCellRenderer,
                index_2.CompanyButtonsCellRenderer,
                index_2.CompanyCentresComponent,
                index_2.CompanyStaffComponent,
            ],
            entryComponents: [
                index_2.CompanyExclussiveStatusCellRenderer,
                index_2.CompanyButtonsCellRenderer
            ],
            providers: [index_2.CompaniesService],
            exports: [
                index_2.CompaniesComponent,
                index_2.CompaniesTableComponent
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], CompaniesModule);
    return CompaniesModule;
}());
exports.CompaniesModule = CompaniesModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NvbXBhbmllcy9jb21wYW5pZXMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBeUIsZUFBZSxDQUFDLENBQUE7QUFDekMsc0JBQTZCLHVCQUF1QixDQUFDLENBQUE7QUFFckQsc0JBUU8sU0FBUyxDQUFDLENBQUE7QUF5QmpCO0lBQUE7SUFBK0IsQ0FBQztJQXZCaEM7UUFBQyxlQUFRLENBQUM7WUFDTixPQUFPLEVBQUU7Z0JBQ0wsb0JBQVk7YUFDZjtZQUNELFlBQVksRUFBRTtnQkFDViwwQkFBa0I7Z0JBQ2xCLCtCQUF1QjtnQkFDdkIsMkNBQW1DO2dCQUNuQyxrQ0FBMEI7Z0JBQzFCLCtCQUF1QjtnQkFDdkIsNkJBQXFCO2FBQ3hCO1lBQ0QsZUFBZSxFQUFFO2dCQUNiLDJDQUFtQztnQkFDbkMsa0NBQTBCO2FBQzdCO1lBQ0QsU0FBUyxFQUFFLENBQUMsd0JBQWdCLENBQUM7WUFDN0IsT0FBTyxFQUFFO2dCQUNMLDBCQUFrQjtnQkFDbEIsK0JBQXVCO2FBQzFCO1NBQ0osQ0FBQzs7dUJBQUE7SUFFNkIsc0JBQUM7QUFBRCxDQUEvQixBQUFnQyxJQUFBO0FBQW5CLHVCQUFlLGtCQUFJLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL2NlbnRyZS1wb3J0YWwvY29tcGFuaWVzL2NvbXBhbmllcy5tb2R1bGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgU2hhcmVkTW9kdWxlIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL2luZGV4JztcblxuaW1wb3J0IHtcbiAgICBDb21wYW5pZXNDb21wb25lbnQsXG4gICAgQ29tcGFuaWVzVGFibGVDb21wb25lbnQsXG4gICAgQ29tcGFuaWVzU2VydmljZSxcbiAgICBDb21wYW55RXhjbHVzc2l2ZVN0YXR1c0NlbGxSZW5kZXJlcixcbiAgICBDb21wYW55QnV0dG9uc0NlbGxSZW5kZXJlcixcbiAgICBDb21wYW55Q2VudHJlc0NvbXBvbmVudCxcbiAgICBDb21wYW55U3RhZmZDb21wb25lbnQsXG59IGZyb20gJy4vaW5kZXgnO1xuXG5ATmdNb2R1bGUoe1xuICAgIGltcG9ydHM6IFtcbiAgICAgICAgU2hhcmVkTW9kdWxlXG4gICAgXSxcbiAgICBkZWNsYXJhdGlvbnM6IFtcbiAgICAgICAgQ29tcGFuaWVzQ29tcG9uZW50LFxuICAgICAgICBDb21wYW5pZXNUYWJsZUNvbXBvbmVudCxcbiAgICAgICAgQ29tcGFueUV4Y2x1c3NpdmVTdGF0dXNDZWxsUmVuZGVyZXIsXG4gICAgICAgIENvbXBhbnlCdXR0b25zQ2VsbFJlbmRlcmVyLFxuICAgICAgICBDb21wYW55Q2VudHJlc0NvbXBvbmVudCxcbiAgICAgICAgQ29tcGFueVN0YWZmQ29tcG9uZW50LFxuICAgIF0sXG4gICAgZW50cnlDb21wb25lbnRzOiBbXG4gICAgICAgIENvbXBhbnlFeGNsdXNzaXZlU3RhdHVzQ2VsbFJlbmRlcmVyLFxuICAgICAgICBDb21wYW55QnV0dG9uc0NlbGxSZW5kZXJlclxuICAgIF0sXG4gICAgcHJvdmlkZXJzOiBbQ29tcGFuaWVzU2VydmljZV0sXG4gICAgZXhwb3J0czogW1xuICAgICAgICBDb21wYW5pZXNDb21wb25lbnQsXG4gICAgICAgIENvbXBhbmllc1RhYmxlQ29tcG9uZW50XG4gICAgXVxufSlcblxuZXhwb3J0IGNsYXNzIENvbXBhbmllc01vZHVsZSB7IH1cbiJdfQ==
