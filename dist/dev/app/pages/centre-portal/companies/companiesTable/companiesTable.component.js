"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../index');
var local_data_source_1 = require('../../../../shared/ng2-smart-table/ng2-smart-table/lib/data-source/local/local.data-source');
var angular2_toaster_1 = require('angular2-toaster');
var index_2 = require('../../../../shared/index');
var index_3 = require('../../../../api/index');
var CompaniesTableComponent = (function () {
    function CompaniesTableComponent(_companiesService, _toasterService) {
        this._companiesService = _companiesService;
        this._toasterService = _toasterService;
        this.source = new local_data_source_1.LocalDataSource();
    }
    CompaniesTableComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._companiesService.GetCompanies$()
            .subscribe(function (res) {
            _this.initSettings();
            _this.companies = res;
            _this.source.load(_this.companies);
        });
    };
    CompaniesTableComponent.prototype.addCompany = function (event) {
        this.createUpdateCompany(event);
    };
    CompaniesTableComponent.prototype.editCompany = function (event) {
        this.createUpdateCompany(event);
    };
    CompaniesTableComponent.prototype.createUpdateCompany = function (event) {
        var _this = this;
        if (event.newData.name === '') {
            this._toasterService
                .popAsync('error', 'Missing Information', 'All fields must be provided');
            event.confirm.reject();
        }
        else {
            var savingToast_1 = this._toasterService.pop(new index_2.SpinningToast());
            var createCompanyDto = new index_3.CompanyDto();
            createCompanyDto.init({
                Id: event.newData.id,
                Name: event.newData.name,
                IsExclussiveNetwork: event.newData.isExclussiveNetwork
            });
            this._companiesService.CreateUpdateCompany$(createCompanyDto)
                .toPromise()
                .then(function (res) {
                _this._toasterService.clear(savingToast_1.toastId, savingToast_1.toastContainerId);
                _this._toasterService.popAsync('success', 'Saved', '');
                return event.confirm.resolve(res);
            })
                .catch(function (error) {
                _this._toasterService.clear(savingToast_1.toastId, savingToast_1.toastContainerId);
                return event.confirm.reject();
            });
        }
    };
    CompaniesTableComponent.prototype.removeCompany = function (event) {
        alert('That\'s not possible to delete company directly. Please ask your database administrator for that!');
    };
    CompaniesTableComponent.prototype.initSettings = function () {
        this.settings = {
            mode: 'internal',
            hideHeader: false,
            hideSubHeader: false,
            sort: false,
            actions: {
                columnTitle: '',
                add: true,
                edit: true,
                delete: true,
                position: 'right'
            },
            add: {
                addButtonContent: 'Add Company',
                createButtonContent: 'Create',
                cancelButtonContent: 'Cancel',
                confirmCreate: true
            },
            edit: {
                editButtonContent: 'Edit Company',
                saveButtonContent: 'Save',
                cancelButtonContent: 'Cancel',
                confirmSave: true
            },
            delete: {
                deleteButtonContent: 'Remove Company',
                confirmDelete: true
            },
            columns: {
                name: {
                    title: 'Company Name',
                    filter: false
                },
                isExclussiveNetwork: {
                    title: 'Is Exclussive',
                    filter: false,
                    type: 'custom',
                    renderComponent: index_1.CompanyExclussiveStatusCellRenderer,
                    editor: {
                        type: 'custom',
                        component: index_1.CompanyExclussiveStatusCellRenderer
                    }
                },
                id: {
                    type: 'custom',
                    renderComponent: index_1.CompanyButtonsCellRenderer,
                    editor: {
                        type: 'custom',
                        component: index_1.CompanyButtonsCellRenderer
                    }
                }
            },
            noDataMessage: 'No Companies'
        };
    };
    CompaniesTableComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'companies-table-cmp',
            templateUrl: './companiesTable.component.html'
        }), 
        __metadata('design:paramtypes', [index_1.CompaniesService, angular2_toaster_1.ToasterService])
    ], CompaniesTableComponent);
    return CompaniesTableComponent;
}());
exports.CompaniesTableComponent = CompaniesTableComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NvbXBhbmllcy9jb21wYW5pZXNUYWJsZS9jb21wYW5pZXNUYWJsZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUFrQyxlQUFlLENBQUMsQ0FBQTtBQUVsRCxzQkFBa0csVUFBVSxDQUFDLENBQUE7QUFDN0csa0NBQWdDLDRGQUE0RixDQUFDLENBQUE7QUFDN0gsaUNBQStCLGtCQUFrQixDQUFDLENBQUE7QUFDbEQsc0JBQThCLDBCQUEwQixDQUFDLENBQUE7QUFDekQsc0JBQTJCLHVCQUF1QixDQUFDLENBQUE7QUFRbkQ7SUFNSSxpQ0FBb0IsaUJBQW1DLEVBQzNDLGVBQStCO1FBRHZCLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBa0I7UUFDM0Msb0JBQWUsR0FBZixlQUFlLENBQWdCO1FBRm5DLFdBQU0sR0FBb0IsSUFBSSxtQ0FBZSxFQUFFLENBQUM7SUFHeEQsQ0FBQztJQUVELDBDQUFRLEdBQVI7UUFBQSxpQkFPQztRQU5HLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLEVBQUU7YUFDbkMsU0FBUyxDQUFDLFVBQUEsR0FBRztZQUNaLEtBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztZQUNwQixLQUFJLENBQUMsU0FBUyxHQUFHLEdBQUcsQ0FBQztZQUNyQixLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDbkMsQ0FBQyxDQUFDLENBQUM7SUFDVCxDQUFDO0lBRUQsNENBQVUsR0FBVixVQUFXLEtBQVU7UUFDakIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3BDLENBQUM7SUFFRCw2Q0FBVyxHQUFYLFVBQVksS0FBVTtRQUNsQixJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDcEMsQ0FBQztJQUVELHFEQUFtQixHQUFuQixVQUFvQixLQUFVO1FBQTlCLGlCQTRCQztRQTFCRyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBRTVCLElBQUksQ0FBQyxlQUFlO2lCQUNmLFFBQVEsQ0FBQyxPQUFPLEVBQUUscUJBQXFCLEVBQUUsNkJBQTZCLENBQUMsQ0FBQztZQUM3RSxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBRTNCLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLElBQUksYUFBVyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUkscUJBQWEsRUFBRSxDQUFDLENBQUM7WUFDaEUsSUFBSSxnQkFBZ0IsR0FBRyxJQUFJLGtCQUFVLEVBQUUsQ0FBQztZQUN4QyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUM7Z0JBQ2xCLEVBQUUsRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ3BCLElBQUksRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQ3hCLG1CQUFtQixFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsbUJBQW1CO2FBQ3pELENBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsQ0FBQztpQkFDeEQsU0FBUyxFQUFFO2lCQUNYLElBQUksQ0FBQyxVQUFDLEdBQUc7Z0JBQ04sS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsYUFBVyxDQUFDLE9BQU8sRUFBRSxhQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztnQkFDOUUsS0FBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLE9BQU8sRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDdEQsTUFBTSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3RDLENBQUMsQ0FBQztpQkFDRCxLQUFLLENBQUMsVUFBQyxLQUFLO2dCQUNULEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLGFBQVcsQ0FBQyxPQUFPLEVBQUUsYUFBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0JBQzlFLE1BQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQ2xDLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQztJQUNMLENBQUM7SUFFRCwrQ0FBYSxHQUFiLFVBQWMsS0FBVTtRQUNwQixLQUFLLENBQUMsbUdBQW1HLENBQUMsQ0FBQztJQUMvRyxDQUFDO0lBR08sOENBQVksR0FBcEI7UUFDSSxJQUFJLENBQUMsUUFBUSxHQUFHO1lBQ1osSUFBSSxFQUFFLFVBQVU7WUFDaEIsVUFBVSxFQUFFLEtBQUs7WUFDakIsYUFBYSxFQUFFLEtBQUs7WUFDcEIsSUFBSSxFQUFFLEtBQUs7WUFDWCxPQUFPLEVBQUU7Z0JBQ0wsV0FBVyxFQUFFLEVBQUU7Z0JBQ2YsR0FBRyxFQUFFLElBQUk7Z0JBQ1QsSUFBSSxFQUFFLElBQUk7Z0JBQ1YsTUFBTSxFQUFFLElBQUk7Z0JBQ1osUUFBUSxFQUFFLE9BQU87YUFDcEI7WUFDRCxHQUFHLEVBQUU7Z0JBQ0QsZ0JBQWdCLEVBQUUsYUFBYTtnQkFDL0IsbUJBQW1CLEVBQUUsUUFBUTtnQkFDN0IsbUJBQW1CLEVBQUUsUUFBUTtnQkFDN0IsYUFBYSxFQUFFLElBQUk7YUFDdEI7WUFDRCxJQUFJLEVBQUU7Z0JBQ0YsaUJBQWlCLEVBQUUsY0FBYztnQkFDakMsaUJBQWlCLEVBQUUsTUFBTTtnQkFDekIsbUJBQW1CLEVBQUUsUUFBUTtnQkFDN0IsV0FBVyxFQUFFLElBQUk7YUFDcEI7WUFDRCxNQUFNLEVBQUU7Z0JBQ0osbUJBQW1CLEVBQUUsZ0JBQWdCO2dCQUNyQyxhQUFhLEVBQUUsSUFBSTthQUN0QjtZQUNELE9BQU8sRUFBRTtnQkFDTCxJQUFJLEVBQUU7b0JBQ0YsS0FBSyxFQUFFLGNBQWM7b0JBQ3JCLE1BQU0sRUFBRSxLQUFLO2lCQUNoQjtnQkFDRCxtQkFBbUIsRUFBRTtvQkFDakIsS0FBSyxFQUFFLGVBQWU7b0JBQ3RCLE1BQU0sRUFBRSxLQUFLO29CQUNiLElBQUksRUFBRSxRQUFRO29CQUNkLGVBQWUsRUFBRSwyQ0FBbUM7b0JBQ3BELE1BQU0sRUFBRTt3QkFDTixJQUFJLEVBQUUsUUFBUTt3QkFDZCxTQUFTLEVBQUUsMkNBQW1DO3FCQUMvQztpQkFDSjtnQkFDRCxFQUFFLEVBQUU7b0JBQ0YsSUFBSSxFQUFFLFFBQVE7b0JBQ2QsZUFBZSxFQUFFLGtDQUEwQjtvQkFDM0MsTUFBTSxFQUFFO3dCQUNOLElBQUksRUFBRSxRQUFRO3dCQUNkLFNBQVMsRUFBRSxrQ0FBMEI7cUJBQ3RDO2lCQUNGO2FBQ0o7WUFDRCxhQUFhLEVBQUUsY0FBYztTQUNoQyxDQUFDO0lBQ04sQ0FBQztJQTNITDtRQUFDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLHFCQUFxQjtZQUMvQixXQUFXLEVBQUUsaUNBQWlDO1NBQ2pELENBQUM7OytCQUFBO0lBd0hGLDhCQUFDO0FBQUQsQ0F0SEEsQUFzSEMsSUFBQTtBQXRIWSwrQkFBdUIsMEJBc0huQyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NvbXBhbmllcy9jb21wYW5pZXNUYWJsZS9jb21wYW5pZXNUYWJsZS5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBDb21wYW5pZXNTZXJ2aWNlLCBDb21wYW55RXhjbHVzc2l2ZVN0YXR1c0NlbGxSZW5kZXJlciwgQ29tcGFueUJ1dHRvbnNDZWxsUmVuZGVyZXIgfSBmcm9tICcuLi9pbmRleCc7XG5pbXBvcnQgeyBMb2NhbERhdGFTb3VyY2UgfSBmcm9tICcuLi8uLi8uLi8uLi9zaGFyZWQvbmcyLXNtYXJ0LXRhYmxlL25nMi1zbWFydC10YWJsZS9saWIvZGF0YS1zb3VyY2UvbG9jYWwvbG9jYWwuZGF0YS1zb3VyY2UnO1xuaW1wb3J0IHsgVG9hc3RlclNlcnZpY2UgfSBmcm9tICdhbmd1bGFyMi10b2FzdGVyJztcbmltcG9ydCB7IFNwaW5uaW5nVG9hc3QgfSBmcm9tICcuLi8uLi8uLi8uLi9zaGFyZWQvaW5kZXgnO1xuaW1wb3J0IHsgQ29tcGFueUR0byB9IGZyb20gJy4uLy4uLy4uLy4uL2FwaS9pbmRleCc7XG5cbkBDb21wb25lbnQoe1xuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXG4gICAgc2VsZWN0b3I6ICdjb21wYW5pZXMtdGFibGUtY21wJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vY29tcGFuaWVzVGFibGUuY29tcG9uZW50Lmh0bWwnXG59KVxuXG5leHBvcnQgY2xhc3MgQ29tcGFuaWVzVGFibGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gICAgcHJpdmF0ZSBzZXR0aW5nczogYW55O1xuXG4gICAgcHJpdmF0ZSBjb21wYW5pZXM6IENvbXBhbnlEdG9bXTtcbiAgICBwcml2YXRlIHNvdXJjZTogTG9jYWxEYXRhU291cmNlID0gbmV3IExvY2FsRGF0YVNvdXJjZSgpO1xuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX2NvbXBhbmllc1NlcnZpY2U6IENvbXBhbmllc1NlcnZpY2UsXG4gICAgICAgIHByaXZhdGUgX3RvYXN0ZXJTZXJ2aWNlOiBUb2FzdGVyU2VydmljZSkge1xuICAgIH1cblxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xuICAgICAgICB0aGlzLl9jb21wYW5pZXNTZXJ2aWNlLkdldENvbXBhbmllcyQoKVxuICAgICAgICAgIC5zdWJzY3JpYmUocmVzID0+IHtcbiAgICAgICAgICAgIHRoaXMuaW5pdFNldHRpbmdzKCk7XG4gICAgICAgICAgICB0aGlzLmNvbXBhbmllcyA9IHJlcztcbiAgICAgICAgICAgIHRoaXMuc291cmNlLmxvYWQodGhpcy5jb21wYW5pZXMpO1xuICAgICAgICAgIH0pO1xuICAgIH1cblxuICAgIGFkZENvbXBhbnkoZXZlbnQ6IGFueSk6IHZvaWQge1xuICAgICAgICB0aGlzLmNyZWF0ZVVwZGF0ZUNvbXBhbnkoZXZlbnQpO1xuICAgIH1cblxuICAgIGVkaXRDb21wYW55KGV2ZW50OiBhbnkpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5jcmVhdGVVcGRhdGVDb21wYW55KGV2ZW50KTtcbiAgICB9XG5cbiAgICBjcmVhdGVVcGRhdGVDb21wYW55KGV2ZW50OiBhbnkpIHtcblxuICAgICAgICBpZiAoZXZlbnQubmV3RGF0YS5uYW1lID09PSAnJykge1xuXG4gICAgICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZVxuICAgICAgICAgICAgICAgIC5wb3BBc3luYygnZXJyb3InLCAnTWlzc2luZyBJbmZvcm1hdGlvbicsICdBbGwgZmllbGRzIG11c3QgYmUgcHJvdmlkZWQnKTtcbiAgICAgICAgICAgIGV2ZW50LmNvbmZpcm0ucmVqZWN0KCk7XG5cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGxldCBzYXZpbmdUb2FzdCA9IHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLnBvcChuZXcgU3Bpbm5pbmdUb2FzdCgpKTtcbiAgICAgICAgICAgIGxldCBjcmVhdGVDb21wYW55RHRvID0gbmV3IENvbXBhbnlEdG8oKTtcbiAgICAgICAgICAgIGNyZWF0ZUNvbXBhbnlEdG8uaW5pdCh7XG4gICAgICAgICAgICAgICAgSWQ6IGV2ZW50Lm5ld0RhdGEuaWQsXG4gICAgICAgICAgICAgICAgTmFtZTogZXZlbnQubmV3RGF0YS5uYW1lLFxuICAgICAgICAgICAgICAgIElzRXhjbHVzc2l2ZU5ldHdvcms6IGV2ZW50Lm5ld0RhdGEuaXNFeGNsdXNzaXZlTmV0d29ya1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB0aGlzLl9jb21wYW5pZXNTZXJ2aWNlLkNyZWF0ZVVwZGF0ZUNvbXBhbnkkKGNyZWF0ZUNvbXBhbnlEdG8pXG4gICAgICAgICAgICAgICAgLnRvUHJvbWlzZSgpXG4gICAgICAgICAgICAgICAgLnRoZW4oKHJlcykgPT4ge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcihzYXZpbmdUb2FzdC50b2FzdElkLCBzYXZpbmdUb2FzdC50b2FzdENvbnRhaW5lcklkKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UucG9wQXN5bmMoJ3N1Y2Nlc3MnLCAnU2F2ZWQnLCAnJyk7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBldmVudC5jb25maXJtLnJlc29sdmUocmVzKTtcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC5jYXRjaCgoZXJyb3IpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoc2F2aW5nVG9hc3QudG9hc3RJZCwgc2F2aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCk7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBldmVudC5jb25maXJtLnJlamVjdCgpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcmVtb3ZlQ29tcGFueShldmVudDogYW55KTogdm9pZCB7XG4gICAgICAgIGFsZXJ0KCdUaGF0XFwncyBub3QgcG9zc2libGUgdG8gZGVsZXRlIGNvbXBhbnkgZGlyZWN0bHkuIFBsZWFzZSBhc2sgeW91ciBkYXRhYmFzZSBhZG1pbmlzdHJhdG9yIGZvciB0aGF0IScpO1xuICAgIH1cblxuXG4gICAgcHJpdmF0ZSBpbml0U2V0dGluZ3MoKTogdm9pZCB7XG4gICAgICAgIHRoaXMuc2V0dGluZ3MgPSB7XG4gICAgICAgICAgICBtb2RlOiAnaW50ZXJuYWwnLFxuICAgICAgICAgICAgaGlkZUhlYWRlcjogZmFsc2UsXG4gICAgICAgICAgICBoaWRlU3ViSGVhZGVyOiBmYWxzZSxcbiAgICAgICAgICAgIHNvcnQ6IGZhbHNlLFxuICAgICAgICAgICAgYWN0aW9uczoge1xuICAgICAgICAgICAgICAgIGNvbHVtblRpdGxlOiAnJyxcbiAgICAgICAgICAgICAgICBhZGQ6IHRydWUsXG4gICAgICAgICAgICAgICAgZWRpdDogdHJ1ZSxcbiAgICAgICAgICAgICAgICBkZWxldGU6IHRydWUsXG4gICAgICAgICAgICAgICAgcG9zaXRpb246ICdyaWdodCdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBhZGQ6IHtcbiAgICAgICAgICAgICAgICBhZGRCdXR0b25Db250ZW50OiAnQWRkIENvbXBhbnknLFxuICAgICAgICAgICAgICAgIGNyZWF0ZUJ1dHRvbkNvbnRlbnQ6ICdDcmVhdGUnLFxuICAgICAgICAgICAgICAgIGNhbmNlbEJ1dHRvbkNvbnRlbnQ6ICdDYW5jZWwnLFxuICAgICAgICAgICAgICAgIGNvbmZpcm1DcmVhdGU6IHRydWVcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBlZGl0OiB7XG4gICAgICAgICAgICAgICAgZWRpdEJ1dHRvbkNvbnRlbnQ6ICdFZGl0IENvbXBhbnknLFxuICAgICAgICAgICAgICAgIHNhdmVCdXR0b25Db250ZW50OiAnU2F2ZScsXG4gICAgICAgICAgICAgICAgY2FuY2VsQnV0dG9uQ29udGVudDogJ0NhbmNlbCcsXG4gICAgICAgICAgICAgICAgY29uZmlybVNhdmU6IHRydWVcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBkZWxldGU6IHtcbiAgICAgICAgICAgICAgICBkZWxldGVCdXR0b25Db250ZW50OiAnUmVtb3ZlIENvbXBhbnknLFxuICAgICAgICAgICAgICAgIGNvbmZpcm1EZWxldGU6IHRydWVcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBjb2x1bW5zOiB7XG4gICAgICAgICAgICAgICAgbmFtZToge1xuICAgICAgICAgICAgICAgICAgICB0aXRsZTogJ0NvbXBhbnkgTmFtZScsXG4gICAgICAgICAgICAgICAgICAgIGZpbHRlcjogZmFsc2VcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGlzRXhjbHVzc2l2ZU5ldHdvcms6IHtcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6ICdJcyBFeGNsdXNzaXZlJyxcbiAgICAgICAgICAgICAgICAgICAgZmlsdGVyOiBmYWxzZSxcbiAgICAgICAgICAgICAgICAgICAgdHlwZTogJ2N1c3RvbScsXG4gICAgICAgICAgICAgICAgICAgIHJlbmRlckNvbXBvbmVudDogQ29tcGFueUV4Y2x1c3NpdmVTdGF0dXNDZWxsUmVuZGVyZXIsXG4gICAgICAgICAgICAgICAgICAgIGVkaXRvcjoge1xuICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICdjdXN0b20nLFxuICAgICAgICAgICAgICAgICAgICAgIGNvbXBvbmVudDogQ29tcGFueUV4Y2x1c3NpdmVTdGF0dXNDZWxsUmVuZGVyZXJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgaWQ6IHtcbiAgICAgICAgICAgICAgICAgIHR5cGU6ICdjdXN0b20nLFxuICAgICAgICAgICAgICAgICAgcmVuZGVyQ29tcG9uZW50OiBDb21wYW55QnV0dG9uc0NlbGxSZW5kZXJlcixcbiAgICAgICAgICAgICAgICAgIGVkaXRvcjoge1xuICAgICAgICAgICAgICAgICAgICB0eXBlOiAnY3VzdG9tJyxcbiAgICAgICAgICAgICAgICAgICAgY29tcG9uZW50OiBDb21wYW55QnV0dG9uc0NlbGxSZW5kZXJlclxuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBub0RhdGFNZXNzYWdlOiAnTm8gQ29tcGFuaWVzJ1xuICAgICAgICB9O1xuICAgIH1cbn1cbiJdfQ==
