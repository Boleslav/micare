"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var cell_1 = require("../../../../shared/ng2-smart-table/ng2-smart-table/lib/data-set/cell");
var CompanyExclussiveStatusCellRenderer = (function () {
    function CompanyExclussiveStatusCellRenderer() {
        this.onStopEditing = new core_1.EventEmitter();
        this.onEdited = new core_1.EventEmitter();
        this.onClick = new core_1.EventEmitter();
        this.isEditing = false;
    }
    CompanyExclussiveStatusCellRenderer.prototype.ngAfterViewInit = function () {
        if (this.valueEdit && (!this.isEditing || !!this.cell.getRow().getData().id)) {
            this.valueEdit.nativeElement.setAttribute('disabled', 'disabled');
        }
        else {
        }
    };
    CompanyExclussiveStatusCellRenderer.prototype.ngOnInit = function () {
        this.isEditing = this.cell && this.cell.getRow().isInEditing;
        this.isNew = this.isEditing && !this.cell.getRow().getData().id;
        if (this.isNew) {
        }
        else {
            var value = this.cell ? this.cell.getValue() : this.value;
            this.displayValue = value ? 'Exclussive' : 'Regular';
            this.statusStyle = value ? { color: '#96cbfe' } : null;
        }
    };
    CompanyExclussiveStatusCellRenderer.prototype.updateValue = function () {
        this.cell.newValue = !this.cell.newValue;
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', cell_1.Cell)
    ], CompanyExclussiveStatusCellRenderer.prototype, "cell", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], CompanyExclussiveStatusCellRenderer.prototype, "inputClass", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], CompanyExclussiveStatusCellRenderer.prototype, "value", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], CompanyExclussiveStatusCellRenderer.prototype, "onStopEditing", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], CompanyExclussiveStatusCellRenderer.prototype, "onEdited", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], CompanyExclussiveStatusCellRenderer.prototype, "onClick", void 0);
    __decorate([
        core_1.ViewChild('valueView'), 
        __metadata('design:type', core_1.ElementRef)
    ], CompanyExclussiveStatusCellRenderer.prototype, "valueView", void 0);
    __decorate([
        core_1.ViewChild('valueEdit'), 
        __metadata('design:type', core_1.ElementRef)
    ], CompanyExclussiveStatusCellRenderer.prototype, "valueEdit", void 0);
    CompanyExclussiveStatusCellRenderer = __decorate([
        core_1.Component({
            moduleId: module.id,
            template: "\n        <label *ngIf=\"!isNew\"\n          [ngStyle]=\"statusStyle\" \n          #valueView \n          [innerHTML]=\"displayValue\"></label>\n        <input #valueEdit \n            *ngIf=\"isNew\"\n            type=\"checkbox\"\n            title=\"You are able to set the Exclussive status only on the Company creation.\"\n            class=\"\"\n            (click)=\"updateValue()\" />",
        }), 
        __metadata('design:paramtypes', [])
    ], CompanyExclussiveStatusCellRenderer);
    return CompanyExclussiveStatusCellRenderer;
}());
exports.CompanyExclussiveStatusCellRenderer = CompanyExclussiveStatusCellRenderer;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NvbXBhbmllcy9jb21wYW5pZXNUYWJsZS9jb21wYW55RXhjbHVzc2l2ZVN0YXR1c0NlbGxSZW5kZXJlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUFxRyxlQUFlLENBQUMsQ0FBQTtBQUNySCxxQkFBbUIsc0VBQXNFLENBQUMsQ0FBQTtBQWtCMUY7SUFBQTtRQU1ZLGtCQUFhLEdBQUcsSUFBSSxtQkFBWSxFQUFPLENBQUM7UUFDeEMsYUFBUSxHQUFHLElBQUksbUJBQVksRUFBTyxDQUFDO1FBQ25DLFlBQU8sR0FBRyxJQUFJLG1CQUFZLEVBQU8sQ0FBQztRQU01QyxjQUFTLEdBQVksS0FBSyxDQUFDO0lBNkI3QixDQUFDO0lBekJDLDZEQUFlLEdBQWY7UUFFRSxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsU0FBUyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM1RSxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsVUFBVSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1FBQ3BFLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztRQUVSLENBQUM7SUFDSCxDQUFDO0lBRUQsc0RBQVEsR0FBUjtRQUNFLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLFdBQVcsQ0FBQztRQUM3RCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQztRQUVoRSxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztRQUVoQixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztZQUMxRCxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssR0FBRyxZQUFZLEdBQUcsU0FBUyxDQUFDO1lBQ3JELElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxHQUFHLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxHQUFHLElBQUksQ0FBQztRQUN6RCxDQUFDO0lBQ0gsQ0FBQztJQUVBLHlEQUFXLEdBQVg7UUFDRSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQzNDLENBQUM7SUF4Q0Y7UUFBQyxZQUFLLEVBQUU7O3FFQUFBO0lBQ1I7UUFBQyxZQUFLLEVBQUU7OzJFQUFBO0lBQ1I7UUFBQyxZQUFLLEVBQUU7O3NFQUFBO0lBRVI7UUFBQyxhQUFNLEVBQUU7OzhFQUFBO0lBQ1Q7UUFBQyxhQUFNLEVBQUU7O3lFQUFBO0lBQ1Q7UUFBQyxhQUFNLEVBQUU7O3dFQUFBO0lBRVQ7UUFBQyxnQkFBUyxDQUFDLFdBQVcsQ0FBQzs7MEVBQUE7SUFDdkI7UUFBQyxnQkFBUyxDQUFDLFdBQVcsQ0FBQzs7MEVBQUE7SUF6QnpCO1FBQUMsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsMFlBVXlCO1NBQ3RDLENBQUM7OzJDQUFBO0lBNENGLDBDQUFDO0FBQUQsQ0EzQ0EsQUEyQ0MsSUFBQTtBQTNDWSwyQ0FBbUMsc0NBMkMvQyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NvbXBhbmllcy9jb21wYW5pZXNUYWJsZS9jb21wYW55RXhjbHVzc2l2ZVN0YXR1c0NlbGxSZW5kZXJlci5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIFZpZXdDaGlsZCwgRWxlbWVudFJlZiwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBBZnRlclZpZXdJbml0LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Q2VsbH0gZnJvbSBcIi4uLy4uLy4uLy4uL3NoYXJlZC9uZzItc21hcnQtdGFibGUvbmcyLXNtYXJ0LXRhYmxlL2xpYi9kYXRhLXNldC9jZWxsXCI7XG5pbXBvcnQge1ZpZXdDZWxsfSBmcm9tIFwiLi4vLi4vLi4vLi4vc2hhcmVkL25nMi1zbWFydC10YWJsZS9uZzItc21hcnQtdGFibGUvY29tcG9uZW50cy9jZWxsL2NlbGwtdmlldy1tb2RlL3ZpZXctY2VsbFwiO1xuaW1wb3J0IHtFZGl0b3J9IGZyb20gXCIuLi8uLi8uLi8uLi9zaGFyZWQvbmcyLXNtYXJ0LXRhYmxlL25nMi1zbWFydC10YWJsZS9jb21wb25lbnRzL2NlbGwvY2VsbC1lZGl0b3JzL2RlZmF1bHQtZWRpdG9yXCI7XG5cbkBDb21wb25lbnQoe1xuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXG4gICAgdGVtcGxhdGU6IGBcbiAgICAgICAgPGxhYmVsICpuZ0lmPVwiIWlzTmV3XCJcbiAgICAgICAgICBbbmdTdHlsZV09XCJzdGF0dXNTdHlsZVwiIFxuICAgICAgICAgICN2YWx1ZVZpZXcgXG4gICAgICAgICAgW2lubmVySFRNTF09XCJkaXNwbGF5VmFsdWVcIj48L2xhYmVsPlxuICAgICAgICA8aW5wdXQgI3ZhbHVlRWRpdCBcbiAgICAgICAgICAgICpuZ0lmPVwiaXNOZXdcIlxuICAgICAgICAgICAgdHlwZT1cImNoZWNrYm94XCJcbiAgICAgICAgICAgIHRpdGxlPVwiWW91IGFyZSBhYmxlIHRvIHNldCB0aGUgRXhjbHVzc2l2ZSBzdGF0dXMgb25seSBvbiB0aGUgQ29tcGFueSBjcmVhdGlvbi5cIlxuICAgICAgICAgICAgY2xhc3M9XCJcIlxuICAgICAgICAgICAgKGNsaWNrKT1cInVwZGF0ZVZhbHVlKClcIiAvPmAsXG59KVxuZXhwb3J0IGNsYXNzIENvbXBhbnlFeGNsdXNzaXZlU3RhdHVzQ2VsbFJlbmRlcmVyIGltcGxlbWVudHMgT25Jbml0LCBBZnRlclZpZXdJbml0LCBWaWV3Q2VsbCwgRWRpdG9yIHtcblxuICBASW5wdXQoKSBjZWxsOiBDZWxsO1xuICBASW5wdXQoKSBpbnB1dENsYXNzOiBzdHJpbmc7XG4gIEBJbnB1dCgpIHZhbHVlOiBzdHJpbmc7XG5cbiAgQE91dHB1dCgpIG9uU3RvcEVkaXRpbmcgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcbiAgQE91dHB1dCgpIG9uRWRpdGVkID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XG4gIEBPdXRwdXQoKSBvbkNsaWNrID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XG5cbiAgQFZpZXdDaGlsZCgndmFsdWVWaWV3JykgdmFsdWVWaWV3OiBFbGVtZW50UmVmO1xuICBAVmlld0NoaWxkKCd2YWx1ZUVkaXQnKSB2YWx1ZUVkaXQ6IEVsZW1lbnRSZWY7XG5cbiAgZGlzcGxheVZhbHVlOiBzdHJpbmc7XG4gIGlzRWRpdGluZzogYm9vbGVhbiA9IGZhbHNlO1xuICBzdGF0dXNTdHlsZTogYW55O1xuICBpc05ldzogYm9vbGVhbjtcblxuICBuZ0FmdGVyVmlld0luaXQoKTogdm9pZCB7XG5cbiAgICBpZih0aGlzLnZhbHVlRWRpdCAmJiAoIXRoaXMuaXNFZGl0aW5nIHx8ICEhdGhpcy5jZWxsLmdldFJvdygpLmdldERhdGEoKS5pZCkpIHsvLyBlZGl0aW5nXG4gICAgICB0aGlzLnZhbHVlRWRpdC5uYXRpdmVFbGVtZW50LnNldEF0dHJpYnV0ZSgnZGlzYWJsZWQnLCAnZGlzYWJsZWQnKTtcbiAgICB9IGVsc2Uge1xuXG4gICAgfVxuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgdGhpcy5pc0VkaXRpbmcgPSB0aGlzLmNlbGwgJiYgdGhpcy5jZWxsLmdldFJvdygpLmlzSW5FZGl0aW5nO1xuICAgIHRoaXMuaXNOZXcgPSB0aGlzLmlzRWRpdGluZyAmJiAhdGhpcy5jZWxsLmdldFJvdygpLmdldERhdGEoKS5pZDtcblxuICAgIGlmKHRoaXMuaXNOZXcpIHsvLyBlZGl0aW5nXG5cbiAgICB9IGVsc2Uge1xuICAgICAgbGV0IHZhbHVlID0gdGhpcy5jZWxsID8gdGhpcy5jZWxsLmdldFZhbHVlKCkgOiB0aGlzLnZhbHVlO1xuICAgICAgdGhpcy5kaXNwbGF5VmFsdWUgPSB2YWx1ZSA/ICdFeGNsdXNzaXZlJyA6ICdSZWd1bGFyJztcbiAgICAgIHRoaXMuc3RhdHVzU3R5bGUgPSB2YWx1ZSA/IHsgY29sb3I6ICcjOTZjYmZlJyB9IDogbnVsbDtcbiAgICB9XG4gIH1cblxuICAgdXBkYXRlVmFsdWUoKSB7XG4gICAgIHRoaXMuY2VsbC5uZXdWYWx1ZSA9ICF0aGlzLmNlbGwubmV3VmFsdWU7XG4gICB9XG59XG4iXX0=
