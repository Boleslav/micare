"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var cell_1 = require("../../../../shared/ng2-smart-table/ng2-smart-table/lib/data-set/cell");
var CompanyButtonsCellRenderer = (function () {
    function CompanyButtonsCellRenderer() {
        this.onStopEditing = new core_1.EventEmitter();
        this.onEdited = new core_1.EventEmitter();
        this.onClick = new core_1.EventEmitter();
    }
    CompanyButtonsCellRenderer.prototype.ngAfterViewInit = function () {
    };
    CompanyButtonsCellRenderer.prototype.ngOnInit = function () {
        if (this.cell && this.cell.getRow().getData()) {
            this.companyId = this.cell.getRow().getData().id;
        }
        else {
            this.companyId = this.value;
        }
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', cell_1.Cell)
    ], CompanyButtonsCellRenderer.prototype, "cell", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], CompanyButtonsCellRenderer.prototype, "inputClass", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], CompanyButtonsCellRenderer.prototype, "value", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], CompanyButtonsCellRenderer.prototype, "onStopEditing", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], CompanyButtonsCellRenderer.prototype, "onEdited", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], CompanyButtonsCellRenderer.prototype, "onClick", void 0);
    CompanyButtonsCellRenderer = __decorate([
        core_1.Component({
            moduleId: module.id,
            template: "\n      <a *ngIf=\"companyId\" class=\"btn btn-primary\" [routerLink]=\"[ companyId, 'centres']\">Manage Centres</a>\n      <a *ngIf=\"companyId\" class=\"btn btn-primary\" [routerLink]=\"[ companyId, 'staff']\">Manage Staff</a>",
        }), 
        __metadata('design:paramtypes', [])
    ], CompanyButtonsCellRenderer);
    return CompanyButtonsCellRenderer;
}());
exports.CompanyButtonsCellRenderer = CompanyButtonsCellRenderer;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NvbXBhbmllcy9jb21wYW5pZXNUYWJsZS9jb21wYW55QnV0dG9uc0NlbGxSZW5kZXJlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUFxRyxlQUFlLENBQUMsQ0FBQTtBQUNySCxxQkFBbUIsc0VBQXNFLENBQUMsQ0FBQTtBQVUxRjtJQUFBO1FBTVksa0JBQWEsR0FBRyxJQUFJLG1CQUFZLEVBQU8sQ0FBQztRQUN4QyxhQUFRLEdBQUcsSUFBSSxtQkFBWSxFQUFPLENBQUM7UUFDbkMsWUFBTyxHQUFHLElBQUksbUJBQVksRUFBTyxDQUFDO0lBZTlDLENBQUM7SUFWQyxvREFBZSxHQUFmO0lBQ0EsQ0FBQztJQUVELDZDQUFRLEdBQVI7UUFDRSxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzdDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxFQUFFLENBQUM7UUFDbkQsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQzlCLENBQUM7SUFDSCxDQUFDO0lBcEJEO1FBQUMsWUFBSyxFQUFFOzs0REFBQTtJQUNSO1FBQUMsWUFBSyxFQUFFOztrRUFBQTtJQUNSO1FBQUMsWUFBSyxFQUFFOzs2REFBQTtJQUVSO1FBQUMsYUFBTSxFQUFFOztxRUFBQTtJQUNUO1FBQUMsYUFBTSxFQUFFOztnRUFBQTtJQUNUO1FBQUMsYUFBTSxFQUFFOzsrREFBQTtJQWRYO1FBQUMsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsc09BRTJGO1NBQ3hHLENBQUM7O2tDQUFBO0lBd0JGLGlDQUFDO0FBQUQsQ0F2QkEsQUF1QkMsSUFBQTtBQXZCWSxrQ0FBMEIsNkJBdUJ0QyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NvbXBhbmllcy9jb21wYW5pZXNUYWJsZS9jb21wYW55QnV0dG9uc0NlbGxSZW5kZXJlci5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIFZpZXdDaGlsZCwgRWxlbWVudFJlZiwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBBZnRlclZpZXdJbml0LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Q2VsbH0gZnJvbSBcIi4uLy4uLy4uLy4uL3NoYXJlZC9uZzItc21hcnQtdGFibGUvbmcyLXNtYXJ0LXRhYmxlL2xpYi9kYXRhLXNldC9jZWxsXCI7XG5pbXBvcnQge1ZpZXdDZWxsfSBmcm9tIFwiLi4vLi4vLi4vLi4vc2hhcmVkL25nMi1zbWFydC10YWJsZS9uZzItc21hcnQtdGFibGUvY29tcG9uZW50cy9jZWxsL2NlbGwtdmlldy1tb2RlL3ZpZXctY2VsbFwiO1xuaW1wb3J0IHtFZGl0b3J9IGZyb20gXCIuLi8uLi8uLi8uLi9zaGFyZWQvbmcyLXNtYXJ0LXRhYmxlL25nMi1zbWFydC10YWJsZS9jb21wb25lbnRzL2NlbGwvY2VsbC1lZGl0b3JzL2RlZmF1bHQtZWRpdG9yXCI7XG5cbkBDb21wb25lbnQoe1xuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXG4gICAgdGVtcGxhdGU6IGBcbiAgICAgIDxhICpuZ0lmPVwiY29tcGFueUlkXCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnlcIiBbcm91dGVyTGlua109XCJbIGNvbXBhbnlJZCwgJ2NlbnRyZXMnXVwiPk1hbmFnZSBDZW50cmVzPC9hPlxuICAgICAgPGEgKm5nSWY9XCJjb21wYW55SWRcIiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeVwiIFtyb3V0ZXJMaW5rXT1cIlsgY29tcGFueUlkLCAnc3RhZmYnXVwiPk1hbmFnZSBTdGFmZjwvYT5gLFxufSlcbmV4cG9ydCBjbGFzcyBDb21wYW55QnV0dG9uc0NlbGxSZW5kZXJlciBpbXBsZW1lbnRzIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCwgVmlld0NlbGwsIEVkaXRvciB7XG5cbiAgQElucHV0KCkgY2VsbDogQ2VsbDtcbiAgQElucHV0KCkgaW5wdXRDbGFzczogc3RyaW5nO1xuICBASW5wdXQoKSB2YWx1ZTogc3RyaW5nO1xuXG4gIEBPdXRwdXQoKSBvblN0b3BFZGl0aW5nID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XG4gIEBPdXRwdXQoKSBvbkVkaXRlZCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xuICBAT3V0cHV0KCkgb25DbGljayA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xuXG4gIGRpc3BsYXlWYWx1ZTogc3RyaW5nO1xuICBjb21wYW55SWQ6IHN0cmluZztcblxuICBuZ0FmdGVyVmlld0luaXQoKTogdm9pZCB7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICBpZih0aGlzLmNlbGwgJiYgdGhpcy5jZWxsLmdldFJvdygpLmdldERhdGEoKSkge1xuICAgICAgdGhpcy5jb21wYW55SWQgPSB0aGlzLmNlbGwuZ2V0Um93KCkuZ2V0RGF0YSgpLmlkO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmNvbXBhbnlJZCA9IHRoaXMudmFsdWU7XG4gICAgfVxuICB9XG59XG4iXX0=
