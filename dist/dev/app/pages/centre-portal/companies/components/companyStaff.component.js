"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var index_1 = require('../index');
var angular2_toaster_1 = require('angular2-toaster');
var index_2 = require('../../../../api/index');
var CompanyStaffComponent = (function () {
    function CompanyStaffComponent(route, _companiesService, _toasterService) {
        var _this = this;
        this.route = route;
        this._companiesService = _companiesService;
        this._toasterService = _toasterService;
        this.company = new index_2.CompanyDto();
        this.staffToAdd = null;
        this.selectedStaff = null;
        this.searchStaff = '';
        this.isSysAdmin = false;
        this.route.params
            .map(function (params) { return params['companyId']; })
            .subscribe(function (id) {
            _this.companyId = id;
            _this._companiesService.GetCompany$(id)
                .subscribe(function (company) { return _this.company = company; });
        });
    }
    CompanyStaffComponent.prototype.ngOnInit = function () {
    };
    CompanyStaffComponent.prototype.selectStaff = function (staff) {
        this.selectedStaff = staff;
    };
    CompanyStaffComponent.prototype.addStaff = function () {
        var staff = this.selectedStaff;
        if (!staff) {
            return;
        }
    };
    CompanyStaffComponent.prototype.setActive = function (staff, setActive) {
    };
    CompanyStaffComponent.prototype.setActiveEx = function (staff, setActive) {
    };
    CompanyStaffComponent.prototype.changeTypeaheadLoading = function (e) {
        this.typeaheadLoading = e;
    };
    CompanyStaffComponent.prototype.changeTypeaheadNoResults = function (e) {
        this.typeaheadNoResults = e;
    };
    CompanyStaffComponent.prototype.typeaheadOnSelect = function (e) {
        var staff = e.item;
        this.selectStaff(staff);
    };
    CompanyStaffComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'company-staff-cmp',
            templateUrl: './companyStaff.component.html'
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, index_1.CompaniesService, angular2_toaster_1.ToasterService])
    ], CompanyStaffComponent);
    return CompanyStaffComponent;
}());
exports.CompanyStaffComponent = CompanyStaffComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NvbXBhbmllcy9jb21wb25lbnRzL2NvbXBhbnlTdGFmZi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUFrQyxlQUFlLENBQUMsQ0FBQTtBQUNsRCx1QkFBK0IsaUJBQWlCLENBQUMsQ0FBQTtBQUlqRCxzQkFBaUMsVUFBVSxDQUFDLENBQUE7QUFFNUMsaUNBQStCLGtCQUFrQixDQUFDLENBQUE7QUFFbEQsc0JBQTJCLHVCQUF1QixDQUFDLENBQUE7QUFTbkQ7SUFpQkUsK0JBQW9CLEtBQXFCLEVBQ25CLGlCQUFtQyxFQUUzQyxlQUErQjtRQXBCL0MsaUJBb0dDO1FBbkZxQixVQUFLLEdBQUwsS0FBSyxDQUFnQjtRQUNuQixzQkFBaUIsR0FBakIsaUJBQWlCLENBQWtCO1FBRTNDLG9CQUFlLEdBQWYsZUFBZSxDQUFnQjtRQWZuQyxZQUFPLEdBQWUsSUFBSSxrQkFBVSxFQUFFLENBQUM7UUFLdkMsZUFBVSxHQUErQixJQUFJLENBQUM7UUFDOUMsa0JBQWEsR0FBNkIsSUFBSSxDQUFDO1FBRS9DLGdCQUFXLEdBQVcsRUFBRSxDQUFDO1FBQ3pCLGVBQVUsR0FBWSxLQUFLLENBQUM7UUFRbEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNO2FBQ2QsR0FBRyxDQUFDLFVBQUEsTUFBTSxJQUFJLE9BQUEsTUFBTSxDQUFDLFdBQVcsQ0FBQyxFQUFuQixDQUFtQixDQUFDO2FBQ2xDLFNBQVMsQ0FBQyxVQUFDLEVBQUU7WUFDWixLQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztZQUlwQixLQUFJLENBQUMsaUJBQWlCLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQztpQkFDbkMsU0FBUyxDQUFDLFVBQUEsT0FBTyxJQUFJLE9BQUEsS0FBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLEVBQXRCLENBQXNCLENBQUMsQ0FBQztRQUNsRCxDQUFDLENBQUMsQ0FBQztJQVFULENBQUM7SUFFQyx3Q0FBUSxHQUFSO0lBT0EsQ0FBQztJQUVILDJDQUFXLEdBQVgsVUFBWSxLQUErQjtRQUN6QyxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztJQUM3QixDQUFDO0lBRUQsd0NBQVEsR0FBUjtRQUNFLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUM7UUFDL0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ1gsTUFBTSxDQUFDO1FBQ1QsQ0FBQztJQWdCSCxDQUFDO0lBRUQseUNBQVMsR0FBVCxVQUFVLEtBQStCLEVBQUUsU0FBa0I7SUFLN0QsQ0FBQztJQUVELDJDQUFXLEdBQVgsVUFBWSxLQUErQixFQUFFLFNBQWtCO0lBRS9ELENBQUM7SUFFTSxzREFBc0IsR0FBN0IsVUFBOEIsQ0FBVTtRQUN0QyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDO0lBQzVCLENBQUM7SUFFTSx3REFBd0IsR0FBL0IsVUFBZ0MsQ0FBVTtRQUN4QyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxDQUFDO0lBQzlCLENBQUM7SUFFRCxpREFBaUIsR0FBakIsVUFBa0IsQ0FBaUI7UUFDakMsSUFBSSxLQUFLLEdBQXVELENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDdkUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUMxQixDQUFDO0lBeEdIO1FBQUMsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsbUJBQW1CO1lBQzdCLFdBQVcsRUFBRSwrQkFBK0I7U0FDL0MsQ0FBQzs7NkJBQUE7SUFzR0YsNEJBQUM7QUFBRCxDQXBHQSxBQW9HQyxJQUFBO0FBcEdZLDZCQUFxQix3QkFvR2pDLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL2NlbnRyZS1wb3J0YWwvY29tcGFuaWVzL2NvbXBvbmVudHMvY29tcGFueVN0YWZmLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBUeXBlYWhlYWRNYXRjaCB9IGZyb20gJ25neC1ib290c3RyYXAvdHlwZWFoZWFkJztcblxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgQ29tcGFuaWVzU2VydmljZSB9IGZyb20gJy4uL2luZGV4JztcbmltcG9ydCB7IExvY2FsRGF0YVNvdXJjZSB9IGZyb20gJy4uLy4uLy4uLy4uL3NoYXJlZC9uZzItc21hcnQtdGFibGUvbmcyLXNtYXJ0LXRhYmxlL2xpYi9kYXRhLXNvdXJjZS9sb2NhbC9sb2NhbC5kYXRhLXNvdXJjZSc7XG5pbXBvcnQgeyBUb2FzdGVyU2VydmljZSB9IGZyb20gJ2FuZ3VsYXIyLXRvYXN0ZXInO1xuaW1wb3J0IHsgU3Bpbm5pbmdUb2FzdCB9IGZyb20gJy4uLy4uLy4uLy4uL3NoYXJlZC9pbmRleCc7XG5pbXBvcnQgeyBDb21wYW55RHRvIH0gZnJvbSAnLi4vLi4vLi4vLi4vYXBpL2luZGV4JztcbmltcG9ydCB7Q29tcGFueUZhY2lsaXR5Vmlld01vZGVsfSBmcm9tIFwiLi4vLi4vLi4vLi4vYXBpL2NsaWVudC5zZXJ2aWNlXCI7XG5cbkBDb21wb25lbnQoe1xuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXG4gICAgc2VsZWN0b3I6ICdjb21wYW55LXN0YWZmLWNtcCcsXG4gICAgdGVtcGxhdGVVcmw6ICcuL2NvbXBhbnlTdGFmZi5jb21wb25lbnQuaHRtbCdcbn0pXG5cbmV4cG9ydCBjbGFzcyBDb21wYW55U3RhZmZDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gICAgcHJpdmF0ZSBjb21wYW55SWQ6IHN0cmluZztcblxuICAgIHByaXZhdGUgc3RhZmY6IENvbXBhbnlGYWNpbGl0eVZpZXdNb2RlbFtdO1xuICAgIHByaXZhdGUgY29tcGFueTogQ29tcGFueUR0byA9IG5ldyBDb21wYW55RHRvKCk7XG5cbiAgICAvLyBzdGFmZiBzZWFyY2hcbiAgICBwcml2YXRlIHR5cGVhaGVhZExvYWRpbmc6IGJvb2xlYW47XG4gICAgcHJpdmF0ZSB0eXBlYWhlYWROb1Jlc3VsdHM6IGJvb2xlYW47XG4gICAgcHJpdmF0ZSBzdGFmZlRvQWRkOiBDb21wYW55RmFjaWxpdHlWaWV3TW9kZWxbXSA9IG51bGw7XG4gICAgcHJpdmF0ZSBzZWxlY3RlZFN0YWZmOiBDb21wYW55RmFjaWxpdHlWaWV3TW9kZWwgPSBudWxsO1xuICAgIHByaXZhdGUgZGF0YVNvdXJjZTogT2JzZXJ2YWJsZTxDb21wYW55RmFjaWxpdHlWaWV3TW9kZWxbXT47XG4gICAgcHJpdmF0ZSBzZWFyY2hTdGFmZjogc3RyaW5nID0gJyc7XG4gICAgcHJpdmF0ZSBpc1N5c0FkbWluOiBib29sZWFuID0gZmFsc2U7XG5cblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcbiAgICAgICAgICAgICAgICBwcml2YXRlIF9jb21wYW5pZXNTZXJ2aWNlOiBDb21wYW5pZXNTZXJ2aWNlLFxuICAgICAgICAgICAgICAgIC8vIHByaXZhdGUgX3N0YWZmU2VydmljZTogU3RhZmZTZXJ2aWNlLFxuICAgICAgICBwcml2YXRlIF90b2FzdGVyU2VydmljZTogVG9hc3RlclNlcnZpY2UpIHtcblxuICAgICAgdGhpcy5yb3V0ZS5wYXJhbXNcbiAgICAgICAgLm1hcChwYXJhbXMgPT4gcGFyYW1zWydjb21wYW55SWQnXSlcbiAgICAgICAgLnN1YnNjcmliZSgoaWQpID0+IHtcbiAgICAgICAgICB0aGlzLmNvbXBhbnlJZCA9IGlkO1xuICAgICAgICAgIC8vIHRoaXMuX3N0YWZmU2VydmljZS5HZXRDb21wYW55U3RhZmYoaWQpXG4gICAgICAgICAgLy8gICAuc3Vic2NyaWJlKHN0YWZmID0+IHRoaXMuc3RhZmYgPSBzdGFmZik7XG5cbiAgICAgICAgICB0aGlzLl9jb21wYW5pZXNTZXJ2aWNlLkdldENvbXBhbnkkKGlkKVxuICAgICAgICAgICAgLnN1YnNjcmliZShjb21wYW55ID0+IHRoaXMuY29tcGFueSA9IGNvbXBhbnkpO1xuICAgICAgICB9KTtcblxuICAgICAgLy8gdGhpcy5kYXRhU291cmNlID0gT2JzZXJ2YWJsZVxuICAgICAgLy8gICAuY3JlYXRlKChvYnNlcnZlcjogYW55KSA9PiB7XG4gICAgICAvLyAgICAgb2JzZXJ2ZXIubmV4dCh0aGlzLnNlYXJjaFN0YWZmKTtcbiAgICAgIC8vICAgfSlcbiAgICAgIC8vICAgLm1lcmdlTWFwKChzZWFyY2hUZXJtOiBzdHJpbmcpID0+IHRoaXMuX3N0YWZmU2VydmljZS5HZXRVbmFzc2lnbmVkU3RhZmYoc2VhcmNoVGVybSkpO1xuXG4gIH1cblxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xuICAgICAgICAvLyB0aGlzLl9jb21wYW5pZXNTZXJ2aWNlLkdldENvbXBhbmllcyQoKVxuICAgICAgICAvLyAgIC5zdWJzY3JpYmUocmVzID0+IHtcbiAgICAgICAgLy8gICAgIHRoaXMuaW5pdFNldHRpbmdzKCk7XG4gICAgICAgIC8vICAgICB0aGlzLmNvbXBhbmllcyA9IHJlcztcbiAgICAgICAgLy8gICAgIHRoaXMuc291cmNlLmxvYWQodGhpcy5jb21wYW5pZXMpO1xuICAgICAgICAvLyAgIH0pO1xuICAgIH1cblxuICBzZWxlY3RTdGFmZihzdGFmZjogQ29tcGFueUZhY2lsaXR5Vmlld01vZGVsKSB7XG4gICAgdGhpcy5zZWxlY3RlZFN0YWZmID0gc3RhZmY7XG4gIH1cblxuICBhZGRTdGFmZigpIHtcbiAgICBsZXQgc3RhZmYgPSB0aGlzLnNlbGVjdGVkU3RhZmY7XG4gICAgaWYgKCFzdGFmZikge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuLypcbiAgICB0aGlzLnNldEFjdGl2ZUV4KHN0YWZmLCBmYWxzZSlcbiAgICAgIC5zdWJzY3JpYmUoKCkgPT4ge1xuICAgICAgICBsZXQgYyA9IG5ldyBDb21wYW55RmFjaWxpdHlWaWV3TW9kZWwoKTtcbiAgICAgICAgYy5mYWNpbGl0eUlkID0gc3RhZmYuZmFjaWxpdHlJZDtcbiAgICAgICAgYy5mYWNpbGl0eU5hbWUgPSBzdGFmZi5mYWNpbGl0eU5hbWU7XG4gICAgICAgIGMuY29tcGFueUlkID0gdGhpcy5jb21wYW55SWQ7XG4gICAgICAgIGMuaXNBY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5zdGFmZi5wdXNoKGMpO1xuXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRTdGFmZiA9IG51bGw7XG5cbiAgICAgIH0pXG4qL1xuICB9XG5cbiAgc2V0QWN0aXZlKHN0YWZmOiBDb21wYW55RmFjaWxpdHlWaWV3TW9kZWwsIHNldEFjdGl2ZTogYm9vbGVhbikge1xuICAgIC8vIHRoaXMuc2V0QWN0aXZlRXgoc3RhZmYsIHNldEFjdGl2ZSlcbiAgICAvLyAgIC5zdWJzY3JpYmUoKCkgPT4ge1xuICAgIC8vICAgICBzdGFmZi5pc0FjdGl2ZSA9IHNldEFjdGl2ZTtcbiAgICAvLyAgIH0pO1xuICB9XG5cbiAgc2V0QWN0aXZlRXgoc3RhZmY6IENvbXBhbnlGYWNpbGl0eVZpZXdNb2RlbCwgc2V0QWN0aXZlOiBib29sZWFuKSB7XG4gICAgLy8gcmV0dXJuIHRoaXMuX3N0YWZmU2VydmljZS5TZXRBY3RpdmVDb21wYW55U3RhZmYodGhpcy5jb21wYW55SWQsIHN0YWZmLmZhY2lsaXR5SWQsIHNldEFjdGl2ZSk7XG4gIH1cblxuICBwdWJsaWMgY2hhbmdlVHlwZWFoZWFkTG9hZGluZyhlOiBib29sZWFuKTogdm9pZCB7XG4gICAgdGhpcy50eXBlYWhlYWRMb2FkaW5nID0gZTtcbiAgfVxuXG4gIHB1YmxpYyBjaGFuZ2VUeXBlYWhlYWROb1Jlc3VsdHMoZTogYm9vbGVhbik6IHZvaWQge1xuICAgIHRoaXMudHlwZWFoZWFkTm9SZXN1bHRzID0gZTtcbiAgfVxuXG4gIHR5cGVhaGVhZE9uU2VsZWN0KGU6IFR5cGVhaGVhZE1hdGNoKTogdm9pZCB7XG4gICAgbGV0IHN0YWZmOiBDb21wYW55RmFjaWxpdHlWaWV3TW9kZWwgPSA8Q29tcGFueUZhY2lsaXR5Vmlld01vZGVsPmUuaXRlbTtcbiAgICB0aGlzLnNlbGVjdFN0YWZmKHN0YWZmKTtcbiAgfVxuXG59XG4iXX0=
