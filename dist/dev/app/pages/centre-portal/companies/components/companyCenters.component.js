"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rxjs_1 = require('rxjs');
var index_1 = require('../index');
var angular2_toaster_1 = require('angular2-toaster');
var index_2 = require('../../../../api/index');
var client_service_1 = require("../../../../api/client.service");
var centre_service_1 = require("../../centre/centreService/centre.service");
var CompanyCentresComponent = (function () {
    function CompanyCentresComponent(route, _companiesService, _centreService, _toasterService) {
        var _this = this;
        this.route = route;
        this._companiesService = _companiesService;
        this._centreService = _centreService;
        this._toasterService = _toasterService;
        this.company = new index_2.CompanyDto();
        this.centresToAdd = null;
        this.selectedCentre = null;
        this.searchCentre = '';
        this.isSysAdmin = false;
        this.route.params
            .map(function (params) { return params['companyId']; })
            .subscribe(function (id) {
            _this.companyId = id;
            _this._centreService.GetCompanyCentres(id)
                .subscribe(function (centres) { return _this.centres = centres; });
            _this._companiesService.GetCompany$(id)
                .subscribe(function (company) { return _this.company = company; });
        });
        this.dataSource = rxjs_1.Observable
            .create(function (observer) {
            observer.next(_this.searchCentre);
        })
            .mergeMap(function (searchTerm) { return _this._centreService.GetUnassignedCentres(searchTerm); });
    }
    CompanyCentresComponent.prototype.ngOnInit = function () {
    };
    CompanyCentresComponent.prototype.selectCentre = function (centre) {
        this.selectedCentre = centre;
    };
    CompanyCentresComponent.prototype.addCentre = function () {
        var _this = this;
        var centre = this.selectedCentre;
        if (!centre) {
            return;
        }
        var selectedAlreadyAssigned = false;
        this.centres.forEach(function (c) { return selectedAlreadyAssigned = selectedAlreadyAssigned || (c.facilityId == centre.facilityId); });
        if (selectedAlreadyAssigned) {
            this.selectedCentre = null;
            return;
        }
        this.setActiveEx(centre, false)
            .subscribe(function () {
            var c = new client_service_1.CompanyFacilityViewModel();
            c.facilityId = centre.facilityId;
            c.facilityName = centre.facilityName;
            c.companyId = _this.companyId;
            c.isActive = false;
            _this.centres.push(c);
            _this.selectedCentre = null;
            _this.dataSource = rxjs_1.Observable
                .create(function (observer) {
                observer.next(_this.searchCentre);
            })
                .mergeMap(function (searchTerm) { return _this._centreService.GetUnassignedCentres(searchTerm); });
        });
    };
    CompanyCentresComponent.prototype.setActive = function (centre, setActive) {
        this.setActiveEx(centre, setActive)
            .subscribe(function () {
            centre.isActive = setActive;
        });
    };
    CompanyCentresComponent.prototype.setActiveEx = function (centre, setActive) {
        return this._centreService.SetActiveCompanyCentre(this.companyId, centre.facilityId, setActive);
    };
    CompanyCentresComponent.prototype.changeTypeaheadLoading = function (e) {
        this.typeaheadLoading = e;
    };
    CompanyCentresComponent.prototype.changeTypeaheadNoResults = function (e) {
        this.typeaheadNoResults = e;
    };
    CompanyCentresComponent.prototype.typeaheadOnSelect = function (e) {
        var centre = e.item;
        this.selectCentre(centre);
    };
    CompanyCentresComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'company-centres-cmp',
            templateUrl: './companyCenters.component.html'
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, index_1.CompaniesService, centre_service_1.CentreService, angular2_toaster_1.ToasterService])
    ], CompanyCentresComponent);
    return CompanyCentresComponent;
}());
exports.CompanyCentresComponent = CompanyCentresComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NvbXBhbmllcy9jb21wb25lbnRzL2NvbXBhbnlDZW50ZXJzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQWtDLGVBQWUsQ0FBQyxDQUFBO0FBQ2xELHVCQUErQixpQkFBaUIsQ0FBQyxDQUFBO0FBR2pELHFCQUEyQixNQUFNLENBQUMsQ0FBQTtBQUNsQyxzQkFBaUMsVUFBVSxDQUFDLENBQUE7QUFFNUMsaUNBQStCLGtCQUFrQixDQUFDLENBQUE7QUFFbEQsc0JBQTJCLHVCQUF1QixDQUFDLENBQUE7QUFDbkQsK0JBQXVDLGdDQUFnQyxDQUFDLENBQUE7QUFDeEUsK0JBQTRCLDJDQUEyQyxDQUFDLENBQUE7QUFReEU7SUFpQkUsaUNBQW9CLEtBQXFCLEVBQ25CLGlCQUFtQyxFQUNuQyxjQUE2QixFQUNyQyxlQUErQjtRQXBCL0MsaUJBK0dDO1FBOUZxQixVQUFLLEdBQUwsS0FBSyxDQUFnQjtRQUNuQixzQkFBaUIsR0FBakIsaUJBQWlCLENBQWtCO1FBQ25DLG1CQUFjLEdBQWQsY0FBYyxDQUFlO1FBQ3JDLG9CQUFlLEdBQWYsZUFBZSxDQUFnQjtRQWZuQyxZQUFPLEdBQWUsSUFBSSxrQkFBVSxFQUFFLENBQUM7UUFLdkMsaUJBQVksR0FBK0IsSUFBSSxDQUFDO1FBQ2hELG1CQUFjLEdBQTZCLElBQUksQ0FBQztRQUVoRCxpQkFBWSxHQUFXLEVBQUUsQ0FBQztRQUMxQixlQUFVLEdBQVksS0FBSyxDQUFDO1FBUWxDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTTthQUNkLEdBQUcsQ0FBQyxVQUFBLE1BQU0sSUFBSSxPQUFBLE1BQU0sQ0FBQyxXQUFXLENBQUMsRUFBbkIsQ0FBbUIsQ0FBQzthQUNsQyxTQUFTLENBQUMsVUFBQyxFQUFFO1lBQ1osS0FBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7WUFDcEIsS0FBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFLENBQUM7aUJBQ3RDLFNBQVMsQ0FBQyxVQUFBLE9BQU8sSUFBSSxPQUFBLEtBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxFQUF0QixDQUFzQixDQUFDLENBQUM7WUFFaEQsS0FBSSxDQUFDLGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUM7aUJBQ25DLFNBQVMsQ0FBQyxVQUFBLE9BQU8sSUFBSSxPQUFBLEtBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxFQUF0QixDQUFzQixDQUFDLENBQUM7UUFDbEQsQ0FBQyxDQUFDLENBQUM7UUFFTCxJQUFJLENBQUMsVUFBVSxHQUFHLGlCQUFVO2FBQ3pCLE1BQU0sQ0FBQyxVQUFDLFFBQWE7WUFDcEIsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDbkMsQ0FBQyxDQUFDO2FBQ0QsUUFBUSxDQUFDLFVBQUMsVUFBa0IsSUFBSyxPQUFBLEtBQUksQ0FBQyxjQUFjLENBQUMsb0JBQW9CLENBQUMsVUFBVSxDQUFDLEVBQXBELENBQW9ELENBQUMsQ0FBQztJQUU5RixDQUFDO0lBRUMsMENBQVEsR0FBUjtJQU9BLENBQUM7SUFFSCw4Q0FBWSxHQUFaLFVBQWEsTUFBZ0M7UUFDM0MsSUFBSSxDQUFDLGNBQWMsR0FBRyxNQUFNLENBQUM7SUFDL0IsQ0FBQztJQUVELDJDQUFTLEdBQVQ7UUFBQSxpQkErQkM7UUE5QkMsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQztRQUNqQyxFQUFFLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDWixNQUFNLENBQUM7UUFDVCxDQUFDO1FBRUQsSUFBSSx1QkFBdUIsR0FBRyxLQUFLLENBQUM7UUFDcEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSx1QkFBdUIsR0FBRyx1QkFBdUIsSUFBSSxDQUFDLENBQUMsQ0FBQyxVQUFVLElBQUksTUFBTSxDQUFDLFVBQVUsQ0FBQyxFQUF4RixDQUF3RixDQUFDLENBQUM7UUFDcEgsRUFBRSxDQUFBLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxDQUFDO1lBQzNCLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1lBQzNCLE1BQU0sQ0FBQztRQUNULENBQUM7UUFFRCxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUM7YUFDNUIsU0FBUyxDQUFDO1lBQ1QsSUFBSSxDQUFDLEdBQUcsSUFBSSx5Q0FBd0IsRUFBRSxDQUFDO1lBQ3ZDLENBQUMsQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDLFVBQVUsQ0FBQztZQUNqQyxDQUFDLENBQUMsWUFBWSxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUM7WUFDckMsQ0FBQyxDQUFDLFNBQVMsR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDO1lBQzdCLENBQUMsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1lBQ25CLEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRXJCLEtBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1lBRTNCLEtBQUksQ0FBQyxVQUFVLEdBQUcsaUJBQVU7aUJBQ3pCLE1BQU0sQ0FBQyxVQUFDLFFBQWE7Z0JBQ3BCLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ25DLENBQUMsQ0FBQztpQkFDRCxRQUFRLENBQUMsVUFBQyxVQUFrQixJQUFLLE9BQUEsS0FBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsQ0FBQyxVQUFVLENBQUMsRUFBcEQsQ0FBb0QsQ0FBQyxDQUFDO1FBRTVGLENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQUVELDJDQUFTLEdBQVQsVUFBVSxNQUFnQyxFQUFFLFNBQWtCO1FBQzVELElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLFNBQVMsQ0FBQzthQUNoQyxTQUFTLENBQUM7WUFDVCxNQUFNLENBQUMsUUFBUSxHQUFHLFNBQVMsQ0FBQztRQUM5QixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCw2Q0FBVyxHQUFYLFVBQVksTUFBZ0MsRUFBRSxTQUFrQjtRQUM5RCxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxVQUFVLEVBQUUsU0FBUyxDQUFDLENBQUM7SUFDbEcsQ0FBQztJQUVNLHdEQUFzQixHQUE3QixVQUE4QixDQUFVO1FBQ3RDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLENBQUM7SUFDNUIsQ0FBQztJQUVNLDBEQUF3QixHQUEvQixVQUFnQyxDQUFVO1FBQ3hDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxDQUFDLENBQUM7SUFDOUIsQ0FBQztJQUVELG1EQUFpQixHQUFqQixVQUFrQixDQUFpQjtRQUNqQyxJQUFJLE1BQU0sR0FBdUQsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUN4RSxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzVCLENBQUM7SUFuSEg7UUFBQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxxQkFBcUI7WUFDL0IsV0FBVyxFQUFFLGlDQUFpQztTQUNqRCxDQUFDOzsrQkFBQTtJQWlIRiw4QkFBQztBQUFELENBL0dBLEFBK0dDLElBQUE7QUEvR1ksK0JBQXVCLDBCQStHbkMsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9jb21wYW5pZXMvY29tcG9uZW50cy9jb21wYW55Q2VudGVycy5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgVHlwZWFoZWFkTWF0Y2ggfSBmcm9tICduZ3gtYm9vdHN0cmFwL3R5cGVhaGVhZCc7XG5cbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IENvbXBhbmllc1NlcnZpY2UgfSBmcm9tICcuLi9pbmRleCc7XG5pbXBvcnQgeyBMb2NhbERhdGFTb3VyY2UgfSBmcm9tICcuLi8uLi8uLi8uLi9zaGFyZWQvbmcyLXNtYXJ0LXRhYmxlL25nMi1zbWFydC10YWJsZS9saWIvZGF0YS1zb3VyY2UvbG9jYWwvbG9jYWwuZGF0YS1zb3VyY2UnO1xuaW1wb3J0IHsgVG9hc3RlclNlcnZpY2UgfSBmcm9tICdhbmd1bGFyMi10b2FzdGVyJztcbmltcG9ydCB7IFNwaW5uaW5nVG9hc3QgfSBmcm9tICcuLi8uLi8uLi8uLi9zaGFyZWQvaW5kZXgnO1xuaW1wb3J0IHsgQ29tcGFueUR0byB9IGZyb20gJy4uLy4uLy4uLy4uL2FwaS9pbmRleCc7XG5pbXBvcnQge0NvbXBhbnlGYWNpbGl0eVZpZXdNb2RlbH0gZnJvbSBcIi4uLy4uLy4uLy4uL2FwaS9jbGllbnQuc2VydmljZVwiO1xuaW1wb3J0IHtDZW50cmVTZXJ2aWNlfSBmcm9tIFwiLi4vLi4vY2VudHJlL2NlbnRyZVNlcnZpY2UvY2VudHJlLnNlcnZpY2VcIjtcblxuQENvbXBvbmVudCh7XG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbiAgICBzZWxlY3RvcjogJ2NvbXBhbnktY2VudHJlcy1jbXAnLFxuICAgIHRlbXBsYXRlVXJsOiAnLi9jb21wYW55Q2VudGVycy5jb21wb25lbnQuaHRtbCdcbn0pXG5cbmV4cG9ydCBjbGFzcyBDb21wYW55Q2VudHJlc0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgICBwcml2YXRlIGNvbXBhbnlJZDogc3RyaW5nO1xuXG4gICAgcHJpdmF0ZSBjZW50cmVzOiBDb21wYW55RmFjaWxpdHlWaWV3TW9kZWxbXTtcbiAgICBwcml2YXRlIGNvbXBhbnk6IENvbXBhbnlEdG8gPSBuZXcgQ29tcGFueUR0bygpO1xuXG4gICAgLy8gY2VudHJlIHNlYXJjaFxuICAgIHByaXZhdGUgdHlwZWFoZWFkTG9hZGluZzogYm9vbGVhbjtcbiAgICBwcml2YXRlIHR5cGVhaGVhZE5vUmVzdWx0czogYm9vbGVhbjtcbiAgICBwcml2YXRlIGNlbnRyZXNUb0FkZDogQ29tcGFueUZhY2lsaXR5Vmlld01vZGVsW10gPSBudWxsO1xuICAgIHByaXZhdGUgc2VsZWN0ZWRDZW50cmU6IENvbXBhbnlGYWNpbGl0eVZpZXdNb2RlbCA9IG51bGw7XG4gICAgcHJpdmF0ZSBkYXRhU291cmNlOiBPYnNlcnZhYmxlPENvbXBhbnlGYWNpbGl0eVZpZXdNb2RlbFtdPjtcbiAgICBwcml2YXRlIHNlYXJjaENlbnRyZTogc3RyaW5nID0gJyc7XG4gICAgcHJpdmF0ZSBpc1N5c0FkbWluOiBib29sZWFuID0gZmFsc2U7XG5cblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcbiAgICAgICAgICAgICAgICBwcml2YXRlIF9jb21wYW5pZXNTZXJ2aWNlOiBDb21wYW5pZXNTZXJ2aWNlLFxuICAgICAgICAgICAgICAgIHByaXZhdGUgX2NlbnRyZVNlcnZpY2U6IENlbnRyZVNlcnZpY2UsXG4gICAgICAgIHByaXZhdGUgX3RvYXN0ZXJTZXJ2aWNlOiBUb2FzdGVyU2VydmljZSkge1xuXG4gICAgICB0aGlzLnJvdXRlLnBhcmFtc1xuICAgICAgICAubWFwKHBhcmFtcyA9PiBwYXJhbXNbJ2NvbXBhbnlJZCddKVxuICAgICAgICAuc3Vic2NyaWJlKChpZCkgPT4ge1xuICAgICAgICAgIHRoaXMuY29tcGFueUlkID0gaWQ7XG4gICAgICAgICAgdGhpcy5fY2VudHJlU2VydmljZS5HZXRDb21wYW55Q2VudHJlcyhpZClcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoY2VudHJlcyA9PiB0aGlzLmNlbnRyZXMgPSBjZW50cmVzKTtcblxuICAgICAgICAgIHRoaXMuX2NvbXBhbmllc1NlcnZpY2UuR2V0Q29tcGFueSQoaWQpXG4gICAgICAgICAgICAuc3Vic2NyaWJlKGNvbXBhbnkgPT4gdGhpcy5jb21wYW55ID0gY29tcGFueSk7XG4gICAgICAgIH0pO1xuXG4gICAgICB0aGlzLmRhdGFTb3VyY2UgPSBPYnNlcnZhYmxlXG4gICAgICAgIC5jcmVhdGUoKG9ic2VydmVyOiBhbnkpID0+IHtcbiAgICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuc2VhcmNoQ2VudHJlKTtcbiAgICAgICAgfSlcbiAgICAgICAgLm1lcmdlTWFwKChzZWFyY2hUZXJtOiBzdHJpbmcpID0+IHRoaXMuX2NlbnRyZVNlcnZpY2UuR2V0VW5hc3NpZ25lZENlbnRyZXMoc2VhcmNoVGVybSkpO1xuXG4gIH1cblxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xuICAgICAgICAvLyB0aGlzLl9jb21wYW5pZXNTZXJ2aWNlLkdldENvbXBhbmllcyQoKVxuICAgICAgICAvLyAgIC5zdWJzY3JpYmUocmVzID0+IHtcbiAgICAgICAgLy8gICAgIHRoaXMuaW5pdFNldHRpbmdzKCk7XG4gICAgICAgIC8vICAgICB0aGlzLmNvbXBhbmllcyA9IHJlcztcbiAgICAgICAgLy8gICAgIHRoaXMuc291cmNlLmxvYWQodGhpcy5jb21wYW5pZXMpO1xuICAgICAgICAvLyAgIH0pO1xuICAgIH1cblxuICBzZWxlY3RDZW50cmUoY2VudHJlOiBDb21wYW55RmFjaWxpdHlWaWV3TW9kZWwpIHtcbiAgICB0aGlzLnNlbGVjdGVkQ2VudHJlID0gY2VudHJlO1xuICB9XG5cbiAgYWRkQ2VudHJlKCkge1xuICAgIGxldCBjZW50cmUgPSB0aGlzLnNlbGVjdGVkQ2VudHJlO1xuICAgIGlmICghY2VudHJlKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgbGV0IHNlbGVjdGVkQWxyZWFkeUFzc2lnbmVkID0gZmFsc2U7XG4gICAgdGhpcy5jZW50cmVzLmZvckVhY2goYyA9PiBzZWxlY3RlZEFscmVhZHlBc3NpZ25lZCA9IHNlbGVjdGVkQWxyZWFkeUFzc2lnbmVkIHx8IChjLmZhY2lsaXR5SWQgPT0gY2VudHJlLmZhY2lsaXR5SWQpKTtcbiAgICBpZihzZWxlY3RlZEFscmVhZHlBc3NpZ25lZCkge1xuICAgICAgdGhpcy5zZWxlY3RlZENlbnRyZSA9IG51bGw7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdGhpcy5zZXRBY3RpdmVFeChjZW50cmUsIGZhbHNlKVxuICAgICAgLnN1YnNjcmliZSgoKSA9PiB7XG4gICAgICAgIGxldCBjID0gbmV3IENvbXBhbnlGYWNpbGl0eVZpZXdNb2RlbCgpO1xuICAgICAgICBjLmZhY2lsaXR5SWQgPSBjZW50cmUuZmFjaWxpdHlJZDtcbiAgICAgICAgYy5mYWNpbGl0eU5hbWUgPSBjZW50cmUuZmFjaWxpdHlOYW1lO1xuICAgICAgICBjLmNvbXBhbnlJZCA9IHRoaXMuY29tcGFueUlkO1xuICAgICAgICBjLmlzQWN0aXZlID0gZmFsc2U7XG4gICAgICAgIHRoaXMuY2VudHJlcy5wdXNoKGMpO1xuXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRDZW50cmUgPSBudWxsO1xuXG4gICAgICAgIHRoaXMuZGF0YVNvdXJjZSA9IE9ic2VydmFibGVcbiAgICAgICAgICAuY3JlYXRlKChvYnNlcnZlcjogYW55KSA9PiB7XG4gICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuc2VhcmNoQ2VudHJlKTtcbiAgICAgICAgICB9KVxuICAgICAgICAgIC5tZXJnZU1hcCgoc2VhcmNoVGVybTogc3RyaW5nKSA9PiB0aGlzLl9jZW50cmVTZXJ2aWNlLkdldFVuYXNzaWduZWRDZW50cmVzKHNlYXJjaFRlcm0pKTtcblxuICAgICAgfSlcbiAgfVxuXG4gIHNldEFjdGl2ZShjZW50cmU6IENvbXBhbnlGYWNpbGl0eVZpZXdNb2RlbCwgc2V0QWN0aXZlOiBib29sZWFuKSB7XG4gICAgdGhpcy5zZXRBY3RpdmVFeChjZW50cmUsIHNldEFjdGl2ZSlcbiAgICAgIC5zdWJzY3JpYmUoKCkgPT4ge1xuICAgICAgICBjZW50cmUuaXNBY3RpdmUgPSBzZXRBY3RpdmU7XG4gICAgICB9KTtcbiAgfVxuXG4gIHNldEFjdGl2ZUV4KGNlbnRyZTogQ29tcGFueUZhY2lsaXR5Vmlld01vZGVsLCBzZXRBY3RpdmU6IGJvb2xlYW4pIHtcbiAgICByZXR1cm4gdGhpcy5fY2VudHJlU2VydmljZS5TZXRBY3RpdmVDb21wYW55Q2VudHJlKHRoaXMuY29tcGFueUlkLCBjZW50cmUuZmFjaWxpdHlJZCwgc2V0QWN0aXZlKTtcbiAgfVxuXG4gIHB1YmxpYyBjaGFuZ2VUeXBlYWhlYWRMb2FkaW5nKGU6IGJvb2xlYW4pOiB2b2lkIHtcbiAgICB0aGlzLnR5cGVhaGVhZExvYWRpbmcgPSBlO1xuICB9XG5cbiAgcHVibGljIGNoYW5nZVR5cGVhaGVhZE5vUmVzdWx0cyhlOiBib29sZWFuKTogdm9pZCB7XG4gICAgdGhpcy50eXBlYWhlYWROb1Jlc3VsdHMgPSBlO1xuICB9XG5cbiAgdHlwZWFoZWFkT25TZWxlY3QoZTogVHlwZWFoZWFkTWF0Y2gpOiB2b2lkIHtcbiAgICBsZXQgY2VudHJlOiBDb21wYW55RmFjaWxpdHlWaWV3TW9kZWwgPSA8Q29tcGFueUZhY2lsaXR5Vmlld01vZGVsPmUuaXRlbTtcbiAgICB0aGlzLnNlbGVjdENlbnRyZShjZW50cmUpO1xuICB9XG5cbn1cbiJdfQ==
