"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var Observable_1 = require('rxjs/Observable');
var index_1 = require('../../../../core/index');
var index_2 = require('../../../../shared/index');
var SidebarComponent = (function () {
    function SidebarComponent(_userService) {
        this._userService = _userService;
        this.isActive = false;
        this.showMenu = '';
        this.isSysAdmin = false;
        this.isCentreManager = false;
        this.enrolmentFeatureEnabled = false;
        if (index_2.Config.ENABLE_ENROLMENT_FEATURE === 'true') {
            this.enrolmentFeatureEnabled = true;
        }
    }
    SidebarComponent.prototype.ngOnInit = function () {
        var _this = this;
        Observable_1.Observable.forkJoin([
            this._userService.IsSysAdmin$(),
            this._userService.IsCentreManager$()
        ]).subscribe(function (res) {
            var sysAdmin = res[0];
            var centreAdmin = res[1];
            _this.isSysAdmin = sysAdmin;
            _this.isCentreManager = centreAdmin;
        });
    };
    SidebarComponent.prototype.eventCalled = function () {
        this.isActive = !this.isActive;
    };
    SidebarComponent.prototype.addExpandClass = function (element) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        }
        else {
            this.showMenu = element;
        }
    };
    SidebarComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'sidebar-cmp',
            templateUrl: 'sidebar.html'
        }), 
        __metadata('design:paramtypes', [index_1.CentreUserService])
    ], SidebarComponent);
    return SidebarComponent;
}());
exports.SidebarComponent = SidebarComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3NoYXJlZC9zaWRlYmFyL3NpZGViYXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUFrQyxlQUFlLENBQUMsQ0FBQTtBQUNsRCwyQkFBMkIsaUJBQWlCLENBQUMsQ0FBQTtBQUU3QyxzQkFBa0Msd0JBQXdCLENBQUMsQ0FBQTtBQUMzRCxzQkFBdUIsMEJBQTBCLENBQUMsQ0FBQTtBQVFsRDtJQVNDLDBCQUFvQixZQUErQjtRQUEvQixpQkFBWSxHQUFaLFlBQVksQ0FBbUI7UUFQbkQsYUFBUSxHQUFHLEtBQUssQ0FBQztRQUNqQixhQUFRLEdBQVcsRUFBRSxDQUFDO1FBQ3RCLGVBQVUsR0FBWSxLQUFLLENBQUM7UUFDNUIsb0JBQWUsR0FBWSxLQUFLLENBQUM7UUFFekIsNEJBQXVCLEdBQVksS0FBSyxDQUFDO1FBR2hELEVBQUUsQ0FBQyxDQUFDLGNBQU0sQ0FBQyx3QkFBd0IsS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ3ZDLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLENBQUM7UUFDeEMsQ0FBQztJQUNSLENBQUM7SUFFRCxtQ0FBUSxHQUFSO1FBQUEsaUJBWUM7UUFWQSx1QkFBVSxDQUFDLFFBQVEsQ0FBQztZQUNuQixJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRTtZQUMvQixJQUFJLENBQUMsWUFBWSxDQUFDLGdCQUFnQixFQUFFO1NBQ3BDLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxHQUFHO1lBQ2YsSUFBSSxRQUFRLEdBQVksR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQy9CLElBQUksV0FBVyxHQUFZLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUVsQyxLQUFJLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQztZQUMzQixLQUFJLENBQUMsZUFBZSxHQUFHLFdBQVcsQ0FBQztRQUNwQyxDQUFDLENBQUMsQ0FBQztJQUNKLENBQUM7SUFFRCxzQ0FBVyxHQUFYO1FBQ0MsSUFBSSxDQUFDLFFBQVEsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDaEMsQ0FBQztJQUVELHlDQUFjLEdBQWQsVUFBZSxPQUFZO1FBQzFCLEVBQUUsQ0FBQyxDQUFDLE9BQU8sS0FBSyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUMvQixJQUFJLENBQUMsUUFBUSxHQUFHLEdBQUcsQ0FBQztRQUNyQixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDUCxJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQztRQUN6QixDQUFDO0lBQ0YsQ0FBQztJQTdDRjtRQUFDLGdCQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLGFBQWE7WUFDdkIsV0FBVyxFQUFFLGNBQWM7U0FDM0IsQ0FBQzs7d0JBQUE7SUEwQ0YsdUJBQUM7QUFBRCxDQXhDQSxBQXdDQyxJQUFBO0FBeENZLHdCQUFnQixtQkF3QzVCLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL2NlbnRyZS1wb3J0YWwvc2hhcmVkL3NpZGViYXIvc2lkZWJhci5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzL09ic2VydmFibGUnO1xyXG5cclxuaW1wb3J0IHsgQ2VudHJlVXNlclNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi9jb3JlL2luZGV4JztcclxuaW1wb3J0IHsgQ29uZmlnIH0gZnJvbSAnLi4vLi4vLi4vLi4vc2hhcmVkL2luZGV4JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG5cdG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcblx0c2VsZWN0b3I6ICdzaWRlYmFyLWNtcCcsXHJcblx0dGVtcGxhdGVVcmw6ICdzaWRlYmFyLmh0bWwnXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgU2lkZWJhckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG5cdGlzQWN0aXZlID0gZmFsc2U7XHJcblx0c2hvd01lbnU6IHN0cmluZyA9ICcnO1xyXG5cdGlzU3lzQWRtaW46IGJvb2xlYW4gPSBmYWxzZTtcclxuXHRpc0NlbnRyZU1hbmFnZXI6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcblx0cHJpdmF0ZSBlbnJvbG1lbnRGZWF0dXJlRW5hYmxlZDogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuXHRjb25zdHJ1Y3Rvcihwcml2YXRlIF91c2VyU2VydmljZTogQ2VudHJlVXNlclNlcnZpY2UsICkge1xyXG5cdFx0aWYgKENvbmZpZy5FTkFCTEVfRU5ST0xNRU5UX0ZFQVRVUkUgPT09ICd0cnVlJykge1xyXG4gICAgICAgICAgICB0aGlzLmVucm9sbWVudEZlYXR1cmVFbmFibGVkID0gdHJ1ZTtcclxuICAgICAgICB9XHJcblx0fVxyXG5cclxuXHRuZ09uSW5pdCgpOiB2b2lkIHtcclxuXHJcblx0XHRPYnNlcnZhYmxlLmZvcmtKb2luKFtcclxuXHRcdFx0dGhpcy5fdXNlclNlcnZpY2UuSXNTeXNBZG1pbiQoKSxcclxuXHRcdFx0dGhpcy5fdXNlclNlcnZpY2UuSXNDZW50cmVNYW5hZ2VyJCgpXHJcblx0XHRdKS5zdWJzY3JpYmUocmVzID0+IHtcclxuXHRcdFx0bGV0IHN5c0FkbWluOiBib29sZWFuID0gcmVzWzBdO1xyXG5cdFx0XHRsZXQgY2VudHJlQWRtaW46IGJvb2xlYW4gPSByZXNbMV07XHJcblxyXG5cdFx0XHR0aGlzLmlzU3lzQWRtaW4gPSBzeXNBZG1pbjtcclxuXHRcdFx0dGhpcy5pc0NlbnRyZU1hbmFnZXIgPSBjZW50cmVBZG1pbjtcclxuXHRcdH0pO1xyXG5cdH1cclxuXHJcblx0ZXZlbnRDYWxsZWQoKSB7XHJcblx0XHR0aGlzLmlzQWN0aXZlID0gIXRoaXMuaXNBY3RpdmU7XHJcblx0fVxyXG5cclxuXHRhZGRFeHBhbmRDbGFzcyhlbGVtZW50OiBhbnkpIHtcclxuXHRcdGlmIChlbGVtZW50ID09PSB0aGlzLnNob3dNZW51KSB7XHJcblx0XHRcdHRoaXMuc2hvd01lbnUgPSAnMCc7XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHR0aGlzLnNob3dNZW51ID0gZWxlbWVudDtcclxuXHRcdH1cclxuXHR9XHJcbn1cclxuIl19
