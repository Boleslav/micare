"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rxjs_1 = require('rxjs');
var index_1 = require('../../../../core/index');
var TopNavComponent = (function () {
    function TopNavComponent(_userService, _router, _route) {
        var _this = this;
        this._userService = _userService;
        this._router = _router;
        this._route = _route;
        this.centres = null;
        this.selectedCentre = null;
        this.searchCentre = '';
        this.isSysAdmin = false;
        this.dataSource = rxjs_1.Observable
            .create(function (observer) {
            observer.next(_this.searchCentre);
        })
            .mergeMap(function (searchTerm) { return _this._userService.searchCentres(searchTerm); });
    }
    TopNavComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._userService
            .getCentres$()
            .subscribe(function (res) {
            _this.centres = res;
            _this.selectedCentre = _this.centres.find(function (centre) { return centre.id === _this._userService.SelectedCentre.id; });
            if (_this.selectedCentre !== undefined) {
                _this.searchCentre = _this.selectedCentre.name;
            }
        });
        this._userService
            .IsSysAdmin$()
            .subscribe(function (res) { return _this.isSysAdmin = res; });
    };
    TopNavComponent.prototype.selectCentre = function (centre) {
        this.selectedCentre = centre;
        this._userService.SelectedCentre = centre;
        var redirectTo = encodeURI(this._router.url);
        this._router.navigate(['centre-portal', 'redirect', redirectTo]);
    };
    TopNavComponent.prototype.changeTypeaheadLoading = function (e) {
        this.typeaheadLoading = e;
    };
    TopNavComponent.prototype.changeTypeaheadNoResults = function (e) {
        this.typeaheadNoResults = e;
    };
    TopNavComponent.prototype.typeaheadOnSelect = function (e) {
        var centre = e.item;
        this.selectCentre(centre);
    };
    TopNavComponent.prototype.changeTheme = function (color) {
        var link = $('<link>');
        link
            .appendTo('head')
            .attr({ type: 'text/css', rel: 'stylesheet' })
            .attr('href', 'themes/app-' + color + '.css');
    };
    TopNavComponent.prototype.rtl = function () {
        var body = $('body');
        body.toggleClass('rtl');
    };
    TopNavComponent.prototype.sidebarToggler = function () {
        var sidebar = $('#sidebar');
        var mainContainer = $('.main-container');
        sidebar.toggleClass('sidebar-left-zero');
        mainContainer.toggleClass('main-container-ml-zero');
    };
    TopNavComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'top-nav',
            templateUrl: 'topnav.html',
        }), 
        __metadata('design:paramtypes', [index_1.CentreUserService, router_1.Router, router_1.ActivatedRoute])
    ], TopNavComponent);
    return TopNavComponent;
}());
exports.TopNavComponent = TopNavComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3NoYXJlZC90b3BuYXYvdG9wbmF2LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBa0MsZUFBZSxDQUFDLENBQUE7QUFDbEQsdUJBQXVDLGlCQUFpQixDQUFDLENBQUE7QUFDekQscUJBQTJCLE1BQU0sQ0FBQyxDQUFBO0FBR2xDLHNCQUFrQyx3QkFBd0IsQ0FBQyxDQUFBO0FBUTNEO0lBVUMseUJBQW9CLFlBQStCLEVBQzFDLE9BQWUsRUFDZixNQUFzQjtRQVpoQyxpQkEyRUM7UUFqRW9CLGlCQUFZLEdBQVosWUFBWSxDQUFtQjtRQUMxQyxZQUFPLEdBQVAsT0FBTyxDQUFRO1FBQ2YsV0FBTSxHQUFOLE1BQU0sQ0FBZ0I7UUFSdkIsWUFBTyxHQUFnQixJQUFJLENBQUM7UUFDNUIsbUJBQWMsR0FBYyxJQUFJLENBQUM7UUFFakMsaUJBQVksR0FBVyxFQUFFLENBQUM7UUFDMUIsZUFBVSxHQUFZLEtBQUssQ0FBQztRQUtuQyxJQUFJLENBQUMsVUFBVSxHQUFHLGlCQUFVO2FBQzFCLE1BQU0sQ0FBQyxVQUFDLFFBQWE7WUFDckIsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDbEMsQ0FBQyxDQUFDO2FBQ0QsUUFBUSxDQUFDLFVBQUMsVUFBa0IsSUFBSyxPQUFBLEtBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxFQUEzQyxDQUEyQyxDQUFDLENBQUM7SUFDakYsQ0FBQztJQUVELGtDQUFRLEdBQVI7UUFBQSxpQkFjQztRQWJBLElBQUksQ0FBQyxZQUFZO2FBQ2YsV0FBVyxFQUFFO2FBQ2IsU0FBUyxDQUFDLFVBQUEsR0FBRztZQUNiLEtBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1lBQ25CLEtBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBQSxNQUFNLElBQUksT0FBQSxNQUFNLENBQUMsRUFBRSxLQUFLLEtBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLEVBQUUsRUFBakQsQ0FBaUQsQ0FBQyxDQUFDO1lBQ3JHLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxjQUFjLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFDdkMsS0FBSSxDQUFDLFlBQVksR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQztZQUM5QyxDQUFDO1FBQ0YsQ0FBQyxDQUFDLENBQUM7UUFFSixJQUFJLENBQUMsWUFBWTthQUNmLFdBQVcsRUFBRTthQUNiLFNBQVMsQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEtBQUksQ0FBQyxVQUFVLEdBQUcsR0FBRyxFQUFyQixDQUFxQixDQUFDLENBQUM7SUFDM0MsQ0FBQztJQUVELHNDQUFZLEdBQVosVUFBYSxNQUFpQjtRQUM3QixJQUFJLENBQUMsY0FBYyxHQUFHLE1BQU0sQ0FBQztRQUM3QixJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsR0FBRyxNQUFNLENBQUM7UUFDMUMsSUFBSSxVQUFVLEdBQVcsU0FBUyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDckQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxlQUFlLEVBQUUsVUFBVSxFQUFFLFVBQVUsQ0FBQyxDQUFDLENBQUM7SUFDbEUsQ0FBQztJQUVNLGdEQUFzQixHQUE3QixVQUE4QixDQUFVO1FBQ3ZDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLENBQUM7SUFDM0IsQ0FBQztJQUVNLGtEQUF3QixHQUEvQixVQUFnQyxDQUFVO1FBQ3pDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxDQUFDLENBQUM7SUFDN0IsQ0FBQztJQUVELDJDQUFpQixHQUFqQixVQUFrQixDQUFpQjtRQUNsQyxJQUFJLE1BQU0sR0FBeUIsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUMxQyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzNCLENBQUM7SUFFRCxxQ0FBVyxHQUFYLFVBQVksS0FBYTtRQUN4QixJQUFJLElBQUksR0FBUSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDNUIsSUFBSTthQUNGLFFBQVEsQ0FBQyxNQUFNLENBQUM7YUFDaEIsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxHQUFHLEVBQUUsWUFBWSxFQUFFLENBQUM7YUFDN0MsSUFBSSxDQUFDLE1BQU0sRUFBRSxhQUFhLEdBQUcsS0FBSyxHQUFHLE1BQU0sQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCw2QkFBRyxHQUFIO1FBQ0MsSUFBSSxJQUFJLEdBQVEsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzFCLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDekIsQ0FBQztJQUVELHdDQUFjLEdBQWQ7UUFDQyxJQUFJLE9BQU8sR0FBUSxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDakMsSUFBSSxhQUFhLEdBQVEsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUM7UUFDOUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1FBQ3pDLGFBQWEsQ0FBQyxXQUFXLENBQUMsd0JBQXdCLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBL0VGO1FBQUMsZ0JBQVMsQ0FBQztZQUNWLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsU0FBUztZQUNuQixXQUFXLEVBQUUsYUFBYTtTQUMxQixDQUFDOzt1QkFBQTtJQTRFRixzQkFBQztBQUFELENBM0VBLEFBMkVDLElBQUE7QUEzRVksdUJBQWUsa0JBMkUzQixDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3NoYXJlZC90b3BuYXYvdG9wbmF2LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgUm91dGVyLCBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgVHlwZWFoZWFkTWF0Y2ggfSBmcm9tICduZ3gtYm9vdHN0cmFwL3R5cGVhaGVhZCc7XHJcblxyXG5pbXBvcnQgeyBDZW50cmVVc2VyU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uL2NvcmUvaW5kZXgnO1xyXG5pbXBvcnQgeyBDZW50cmVEdG8gfSBmcm9tICcuLi8uLi8uLi8uLi9hcGkvaW5kZXgnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcblx0bW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuXHRzZWxlY3RvcjogJ3RvcC1uYXYnLFxyXG5cdHRlbXBsYXRlVXJsOiAndG9wbmF2Lmh0bWwnLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgVG9wTmF2Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcblx0cHJpdmF0ZSB0eXBlYWhlYWRMb2FkaW5nOiBib29sZWFuO1xyXG5cdHByaXZhdGUgdHlwZWFoZWFkTm9SZXN1bHRzOiBib29sZWFuO1xyXG5cdHByaXZhdGUgY2VudHJlczogQ2VudHJlRHRvW10gPSBudWxsO1xyXG5cdHByaXZhdGUgc2VsZWN0ZWRDZW50cmU6IENlbnRyZUR0byA9IG51bGw7XHJcblx0cHJpdmF0ZSBkYXRhU291cmNlOiBPYnNlcnZhYmxlPENlbnRyZUR0b1tdPjtcclxuXHRwcml2YXRlIHNlYXJjaENlbnRyZTogc3RyaW5nID0gJyc7XHJcblx0cHJpdmF0ZSBpc1N5c0FkbWluOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG5cdGNvbnN0cnVjdG9yKHByaXZhdGUgX3VzZXJTZXJ2aWNlOiBDZW50cmVVc2VyU2VydmljZSxcclxuXHRcdHByaXZhdGUgX3JvdXRlcjogUm91dGVyLFxyXG5cdFx0cHJpdmF0ZSBfcm91dGU6IEFjdGl2YXRlZFJvdXRlKSB7XHJcblx0XHR0aGlzLmRhdGFTb3VyY2UgPSBPYnNlcnZhYmxlXHJcblx0XHRcdC5jcmVhdGUoKG9ic2VydmVyOiBhbnkpID0+IHtcclxuXHRcdFx0XHRvYnNlcnZlci5uZXh0KHRoaXMuc2VhcmNoQ2VudHJlKTtcclxuXHRcdFx0fSlcclxuXHRcdFx0Lm1lcmdlTWFwKChzZWFyY2hUZXJtOiBzdHJpbmcpID0+IHRoaXMuX3VzZXJTZXJ2aWNlLnNlYXJjaENlbnRyZXMoc2VhcmNoVGVybSkpO1xyXG5cdH1cclxuXHJcblx0bmdPbkluaXQoKTogdm9pZCB7XHJcblx0XHR0aGlzLl91c2VyU2VydmljZVxyXG5cdFx0XHQuZ2V0Q2VudHJlcyQoKVxyXG5cdFx0XHQuc3Vic2NyaWJlKHJlcyA9PiB7XHJcblx0XHRcdFx0dGhpcy5jZW50cmVzID0gcmVzO1xyXG5cdFx0XHRcdHRoaXMuc2VsZWN0ZWRDZW50cmUgPSB0aGlzLmNlbnRyZXMuZmluZChjZW50cmUgPT4gY2VudHJlLmlkID09PSB0aGlzLl91c2VyU2VydmljZS5TZWxlY3RlZENlbnRyZS5pZCk7XHJcblx0XHRcdFx0aWYgKHRoaXMuc2VsZWN0ZWRDZW50cmUgIT09IHVuZGVmaW5lZCkge1xyXG5cdFx0XHRcdFx0dGhpcy5zZWFyY2hDZW50cmUgPSB0aGlzLnNlbGVjdGVkQ2VudHJlLm5hbWU7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9KTtcclxuXHJcblx0XHR0aGlzLl91c2VyU2VydmljZVxyXG5cdFx0XHQuSXNTeXNBZG1pbiQoKVxyXG5cdFx0XHQuc3Vic2NyaWJlKHJlcyA9PiB0aGlzLmlzU3lzQWRtaW4gPSByZXMpO1xyXG5cdH1cclxuXHJcblx0c2VsZWN0Q2VudHJlKGNlbnRyZTogQ2VudHJlRHRvKSB7XHJcblx0XHR0aGlzLnNlbGVjdGVkQ2VudHJlID0gY2VudHJlO1xyXG5cdFx0dGhpcy5fdXNlclNlcnZpY2UuU2VsZWN0ZWRDZW50cmUgPSBjZW50cmU7XHJcblx0XHRsZXQgcmVkaXJlY3RUbzogc3RyaW5nID0gZW5jb2RlVVJJKHRoaXMuX3JvdXRlci51cmwpO1xyXG5cdFx0dGhpcy5fcm91dGVyLm5hdmlnYXRlKFsnY2VudHJlLXBvcnRhbCcsICdyZWRpcmVjdCcsIHJlZGlyZWN0VG9dKTtcclxuXHR9XHJcblxyXG5cdHB1YmxpYyBjaGFuZ2VUeXBlYWhlYWRMb2FkaW5nKGU6IGJvb2xlYW4pOiB2b2lkIHtcclxuXHRcdHRoaXMudHlwZWFoZWFkTG9hZGluZyA9IGU7XHJcblx0fVxyXG5cclxuXHRwdWJsaWMgY2hhbmdlVHlwZWFoZWFkTm9SZXN1bHRzKGU6IGJvb2xlYW4pOiB2b2lkIHtcclxuXHRcdHRoaXMudHlwZWFoZWFkTm9SZXN1bHRzID0gZTtcclxuXHR9XHJcblxyXG5cdHR5cGVhaGVhZE9uU2VsZWN0KGU6IFR5cGVhaGVhZE1hdGNoKTogdm9pZCB7XHJcblx0XHRsZXQgY2VudHJlOiBDZW50cmVEdG8gPSA8Q2VudHJlRHRvPmUuaXRlbTtcclxuXHRcdHRoaXMuc2VsZWN0Q2VudHJlKGNlbnRyZSk7XHJcblx0fVxyXG5cclxuXHRjaGFuZ2VUaGVtZShjb2xvcjogc3RyaW5nKTogdm9pZCB7XHJcblx0XHR2YXIgbGluazogYW55ID0gJCgnPGxpbms+Jyk7XHJcblx0XHRsaW5rXHJcblx0XHRcdC5hcHBlbmRUbygnaGVhZCcpXHJcblx0XHRcdC5hdHRyKHsgdHlwZTogJ3RleHQvY3NzJywgcmVsOiAnc3R5bGVzaGVldCcgfSlcclxuXHRcdFx0LmF0dHIoJ2hyZWYnLCAndGhlbWVzL2FwcC0nICsgY29sb3IgKyAnLmNzcycpO1xyXG5cdH1cclxuXHJcblx0cnRsKCk6IHZvaWQge1xyXG5cdFx0dmFyIGJvZHk6IGFueSA9ICQoJ2JvZHknKTtcclxuXHRcdGJvZHkudG9nZ2xlQ2xhc3MoJ3J0bCcpO1xyXG5cdH1cclxuXHJcblx0c2lkZWJhclRvZ2dsZXIoKTogdm9pZCB7XHJcblx0XHR2YXIgc2lkZWJhcjogYW55ID0gJCgnI3NpZGViYXInKTtcclxuXHRcdHZhciBtYWluQ29udGFpbmVyOiBhbnkgPSAkKCcubWFpbi1jb250YWluZXInKTtcclxuXHRcdHNpZGViYXIudG9nZ2xlQ2xhc3MoJ3NpZGViYXItbGVmdC16ZXJvJyk7XHJcblx0XHRtYWluQ29udGFpbmVyLnRvZ2dsZUNsYXNzKCdtYWluLWNvbnRhaW5lci1tbC16ZXJvJyk7XHJcblx0fVxyXG59XHJcbiJdfQ==
