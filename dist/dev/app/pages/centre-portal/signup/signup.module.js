"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../shared/index');
var index_2 = require('./index');
var angular2_recaptcha_1 = require('angular2-recaptcha');
var SignupModule = (function () {
    function SignupModule() {
    }
    SignupModule = __decorate([
        core_1.NgModule({
            imports: [index_1.SharedModule, angular2_recaptcha_1.ReCaptchaModule],
            declarations: [index_2.SignupComponent, index_2.SignupFormComponent],
            providers: [index_2.SignupService]
        }), 
        __metadata('design:paramtypes', [])
    ], SignupModule);
    return SignupModule;
}());
exports.SignupModule = SignupModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3NpZ251cC9zaWdudXAubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBeUIsZUFBZSxDQUFDLENBQUE7QUFDekMsc0JBQTZCLHVCQUF1QixDQUFDLENBQUE7QUFFckQsc0JBQW9FLFNBQVMsQ0FBQyxDQUFBO0FBRTlFLG1DQUFnQyxvQkFBb0IsQ0FBQyxDQUFBO0FBU3JEO0lBQUE7SUFBNEIsQ0FBQztJQU43QjtRQUFDLGVBQVEsQ0FBQztZQUNOLE9BQU8sRUFBRSxDQUFDLG9CQUFZLEVBQUMsb0NBQWUsQ0FBQztZQUN2QyxZQUFZLEVBQUUsQ0FBQyx1QkFBZSxFQUFFLDJCQUFtQixDQUFDO1lBQ3BELFNBQVMsRUFBRSxDQUFDLHFCQUFhLENBQUM7U0FDN0IsQ0FBQzs7b0JBQUE7SUFFMEIsbUJBQUM7QUFBRCxDQUE1QixBQUE2QixJQUFBO0FBQWhCLG9CQUFZLGVBQUksQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9zaWdudXAvc2lnbnVwLm1vZHVsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFNoYXJlZE1vZHVsZSB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9pbmRleCc7XHJcblxyXG5pbXBvcnQgeyBTaWdudXBDb21wb25lbnQsIFNpZ251cEZvcm1Db21wb25lbnQsIFNpZ251cFNlcnZpY2UgfSBmcm9tICcuL2luZGV4JztcclxuXHJcbmltcG9ydCB7IFJlQ2FwdGNoYU1vZHVsZSB9IGZyb20gJ2FuZ3VsYXIyLXJlY2FwdGNoYSc7XHJcblxyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6IFtTaGFyZWRNb2R1bGUsUmVDYXB0Y2hhTW9kdWxlXSxcclxuICAgIGRlY2xhcmF0aW9uczogW1NpZ251cENvbXBvbmVudCwgU2lnbnVwRm9ybUNvbXBvbmVudF0sXHJcbiAgICBwcm92aWRlcnM6IFtTaWdudXBTZXJ2aWNlXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFNpZ251cE1vZHVsZSB7IH1cclxuIl19
