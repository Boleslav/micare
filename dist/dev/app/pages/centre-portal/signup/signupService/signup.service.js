"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var rxjs_1 = require('rxjs');
var index_1 = require('../../../../api/index');
var SignupService = (function () {
    function SignupService(_apiClient) {
        this._apiClient = _apiClient;
    }
    SignupService.prototype.RegisterCentreAdmin = function (newCentreAdmin) {
        return this._apiClient
            .account_RegisterCentreAdmin(newCentreAdmin).catch(function (error) {
            if (error.status === 403 || error.status === 417) {
                return rxjs_1.Observable.throw(error);
            }
            else {
                return rxjs_1.Observable.throw('Something went wrong, but we are not quite sure what happened..');
            }
        });
    };
    SignupService.prototype.EnrollFamilies = function (newFamilies) {
        return this._apiClient
            .account_RegisterFamilies(newFamilies).catch(function (error) {
            if (error.status === 403 || error.status === 417) {
                return rxjs_1.Observable.throw(error);
            }
            else {
                return rxjs_1.Observable.throw('Something went wrong, but we are not quite sure what happened..');
            }
        });
    };
    SignupService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.ApiClientService])
    ], SignupService);
    return SignupService;
}());
exports.SignupService = SignupService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3NpZ251cC9zaWdudXBTZXJ2aWNlL3NpZ251cC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBMkIsZUFBZSxDQUFDLENBQUE7QUFDM0MscUJBQTJCLE1BQU0sQ0FBQyxDQUFBO0FBRWxDLHNCQUEyRCx1QkFBdUIsQ0FBQyxDQUFBO0FBSW5GO0lBRUksdUJBQW9CLFVBQTRCO1FBQTVCLGVBQVUsR0FBVixVQUFVLENBQWtCO0lBQ2hELENBQUM7SUFFTSwyQ0FBbUIsR0FBMUIsVUFBMkIsY0FBd0M7UUFDL0QsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVO2FBQ2pCLDJCQUEyQixDQUFDLGNBQWMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFDLEtBQVU7WUFFMUQsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLE1BQU0sS0FBSyxHQUFHLElBQUksS0FBSyxDQUFDLE1BQU0sS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUMvQyxNQUFNLENBQUMsaUJBQVUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDbkMsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLE1BQU0sQ0FBQyxpQkFBVSxDQUFDLEtBQUssQ0FBQyxpRUFBaUUsQ0FBQyxDQUFDO1lBQy9GLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFTSxzQ0FBYyxHQUFyQixVQUFzQixXQUFxQztRQUN2RCxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVU7YUFDakIsd0JBQXdCLENBQUMsV0FBVyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUMsS0FBVTtZQUVwRCxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxLQUFLLEdBQUcsSUFBSSxLQUFLLENBQUMsTUFBTSxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQy9DLE1BQU0sQ0FBQyxpQkFBVSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNuQyxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osTUFBTSxDQUFDLGlCQUFVLENBQUMsS0FBSyxDQUFDLGlFQUFpRSxDQUFDLENBQUM7WUFDL0YsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQTVCTDtRQUFDLGlCQUFVLEVBQUU7O3FCQUFBO0lBOEJiLG9CQUFDO0FBQUQsQ0E3QkEsQUE2QkMsSUFBQTtBQTdCWSxxQkFBYSxnQkE2QnpCLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL2NlbnRyZS1wb3J0YWwvc2lnbnVwL3NpZ251cFNlcnZpY2Uvc2lnbnVwLnNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5cbmltcG9ydCB7IEFwaUNsaWVudFNlcnZpY2UsIFJlZ2lzdGVyQ2VudGVyQWRtaW5Nb2RlbCB9IGZyb20gJy4uLy4uLy4uLy4uL2FwaS9pbmRleCc7XG5pbXBvcnQge1JlZ2lzdGVyTmV3RmFtaWx5TW9kZWx9IGZyb20gXCIuLi8uLi8uLi8uLi9hcGkvY2xpZW50LnNlcnZpY2VcIjtcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIFNpZ251cFNlcnZpY2Uge1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfYXBpQ2xpZW50OiBBcGlDbGllbnRTZXJ2aWNlKSB7XG4gICAgfVxuXG4gICAgcHVibGljIFJlZ2lzdGVyQ2VudHJlQWRtaW4obmV3Q2VudHJlQWRtaW46IFJlZ2lzdGVyQ2VudGVyQWRtaW5Nb2RlbCk6IE9ic2VydmFibGU8UmVnaXN0ZXJDZW50ZXJBZG1pbk1vZGVsPiB7XG4gICAgICAgIHJldHVybiB0aGlzLl9hcGlDbGllbnRcbiAgICAgICAgICAgIC5hY2NvdW50X1JlZ2lzdGVyQ2VudHJlQWRtaW4obmV3Q2VudHJlQWRtaW4pLmNhdGNoKChlcnJvcjogYW55KSA9PiB7XG5cbiAgICAgICAgICAgICAgICBpZiAoZXJyb3Iuc3RhdHVzID09PSA0MDMgfHwgZXJyb3Iuc3RhdHVzID09PSA0MTcpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIE9ic2VydmFibGUudGhyb3coZXJyb3IpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBPYnNlcnZhYmxlLnRocm93KCdTb21ldGhpbmcgd2VudCB3cm9uZywgYnV0IHdlIGFyZSBub3QgcXVpdGUgc3VyZSB3aGF0IGhhcHBlbmVkLi4nKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBwdWJsaWMgRW5yb2xsRmFtaWxpZXMobmV3RmFtaWxpZXM6IFJlZ2lzdGVyTmV3RmFtaWx5TW9kZWxbXSk6IE9ic2VydmFibGU8YW55fG51bGw+IHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2FwaUNsaWVudFxuICAgICAgICAgICAgLmFjY291bnRfUmVnaXN0ZXJGYW1pbGllcyhuZXdGYW1pbGllcykuY2F0Y2goKGVycm9yOiBhbnkpID0+IHtcblxuICAgICAgICAgICAgICAgIGlmIChlcnJvci5zdGF0dXMgPT09IDQwMyB8fCBlcnJvci5zdGF0dXMgPT09IDQxNykge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gT2JzZXJ2YWJsZS50aHJvdyhlcnJvcik7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIE9ic2VydmFibGUudGhyb3coJ1NvbWV0aGluZyB3ZW50IHdyb25nLCBidXQgd2UgYXJlIG5vdCBxdWl0ZSBzdXJlIHdoYXQgaGFwcGVuZWQuLicpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgIH1cblxufVxuIl19
