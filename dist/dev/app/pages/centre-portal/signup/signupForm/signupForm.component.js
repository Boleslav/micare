"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var index_1 = require('../../../../validators/index');
var signup_service_1 = require('../signupService/signup.service');
var angular2_toaster_1 = require('angular2-toaster');
var index_2 = require('../../../../shared/index');
var index_3 = require('../../../../api/index');
var SignupFormComponent = (function () {
    function SignupFormComponent(fb, _signupService, _toasterService) {
        this._signupService = _signupService;
        this._toasterService = _toasterService;
        this.submitted = false;
        this.userCreated = false;
        this.captcha = false;
        this.captchaToken = null;
        this.captcha_key = index_2.Config.RECAPTCHA_UI_KEY;
        this.captcha_enabled = this.convertStringToBool(index_2.Config.RecaptchaEnabled);
        this.form = fb.group({
            'firstName': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(1)])],
            'lastName': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(1)])],
            'email': ['', forms_1.Validators.compose([forms_1.Validators.required, index_1.EmailValidator.validate])],
            'mobilePhone': ['', forms_1.Validators.compose([forms_1.Validators.required, index_1.MobileValidator.validate])],
            'passwords': fb.group({
                'password': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(8), index_1.ComplexPasswordValidator.validate])],
                'confirmPassword': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(8)])]
            }, { validator: index_1.EqualPasswordsValidator.validate('password', 'confirmPassword') })
        });
        this.firstName = this.form.controls['firstName'];
        this.lastName = this.form.controls['lastName'];
        this.email = this.form.controls['email'];
        this.mobilePhone = this.form.controls['mobilePhone'];
        this.passwords = this.form.controls['passwords'];
        this.password = this.passwords.controls['password'];
        this.confirmPassword = this.passwords.controls['confirmPassword'];
        if (this.captcha_enabled === false) {
            this.captcha = true;
        }
    }
    SignupFormComponent.prototype.onSubmit = function (values) {
        var _this = this;
        this.submitted = true;
        var savingToast = this._toasterService.pop(new index_2.SpinningToast());
        var newCentreAdmin = new index_3.RegisterCenterAdminModel();
        newCentreAdmin.init({
            FirstName: values.firstName,
            LastName: values.lastName,
            Email: values.email,
            MobilePhone: values.mobilePhone,
            Password: values.passwords.password,
            ConfirmPassword: values.passwords.confirmPassword,
            CaptchaToken: this.captchaToken
        });
        this._signupService
            .RegisterCentreAdmin(newCentreAdmin)
            .subscribe(function (res) {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this._toasterService.pop('success', 'User Created', '');
            _this.userCreated = true;
        }, function (err) {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this.submitted = false;
            _this.recaptcha.reset();
        });
    };
    ;
    SignupFormComponent.prototype.handleCorrectCaptcha = function (event) {
        this.captchaToken = event;
        this.captcha = true;
    };
    ;
    SignupFormComponent.prototype.handleCaptchaExpired = function () {
        this.captchaToken = null;
    };
    SignupFormComponent.prototype.convertStringToBool = function (input) {
        var result = false;
        if (input === 'true') {
            result = true;
        }
        return result;
    };
    ;
    __decorate([
        core_1.ViewChild('recaptcha'), 
        __metadata('design:type', Object)
    ], SignupFormComponent.prototype, "recaptcha", void 0);
    SignupFormComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'signup-form-cmp',
            templateUrl: 'signupForm.component.html'
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, signup_service_1.SignupService, angular2_toaster_1.ToasterService])
    ], SignupFormComponent);
    return SignupFormComponent;
}());
exports.SignupFormComponent = SignupFormComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3NpZ251cC9zaWdudXBGb3JtL3NpZ251cEZvcm0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBcUMsZUFBZSxDQUFDLENBQUE7QUFDckQsc0JBQW9FLGdCQUFnQixDQUFDLENBQUE7QUFDckYsc0JBQW1HLDhCQUE4QixDQUFDLENBQUE7QUFFbEksK0JBQThCLGlDQUFpQyxDQUFDLENBQUE7QUFDaEUsaUNBQStCLGtCQUFrQixDQUFDLENBQUE7QUFDbEQsc0JBQXNDLDBCQUEwQixDQUFDLENBQUE7QUFDakUsc0JBQXlDLHVCQUF1QixDQUFDLENBQUE7QUFRakU7SUFvQkksNkJBQVksRUFBZSxFQUNmLGNBQTZCLEVBQzdCLGVBQStCO1FBRC9CLG1CQUFjLEdBQWQsY0FBYyxDQUFlO1FBQzdCLG9CQUFlLEdBQWYsZUFBZSxDQUFnQjtRQVRwQyxjQUFTLEdBQVksS0FBSyxDQUFDO1FBQzNCLGdCQUFXLEdBQVksS0FBSyxDQUFDO1FBQzdCLFlBQU8sR0FBWSxLQUFLLENBQUM7UUFDekIsaUJBQVksR0FBVyxJQUFJLENBQUM7UUFDNUIsZ0JBQVcsR0FBVyxjQUFNLENBQUMsZ0JBQWdCLENBQUM7UUFDOUMsb0JBQWUsR0FBWSxJQUFJLENBQUMsbUJBQW1CLENBQUMsY0FBTSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFNaEYsSUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDO1lBQ2pCLFdBQVcsRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLGtCQUFVLENBQUMsUUFBUSxFQUFFLGtCQUFVLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNyRixVQUFVLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDcEYsT0FBTyxFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsa0JBQVUsQ0FBQyxRQUFRLEVBQUUsc0JBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ2pGLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLGtCQUFVLENBQUMsUUFBUSxFQUFFLHVCQUFlLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUN4RixXQUFXLEVBQUUsRUFBRSxDQUFDLEtBQUssQ0FBQztnQkFDbEIsVUFBVSxFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsa0JBQVUsQ0FBQyxRQUFRLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUUsZ0NBQXdCLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDdkgsaUJBQWlCLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDOUYsRUFBRSxFQUFFLFNBQVMsRUFBRSwrQkFBdUIsQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLGlCQUFpQixDQUFDLEVBQUUsQ0FBQztTQUNyRixDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ2pELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDL0MsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ3JELElBQUksQ0FBQyxTQUFTLEdBQWMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDNUQsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNwRCxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLENBQUM7UUFFbEUsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBRWpDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3hCLENBQUM7SUFFTCxDQUFDO0lBRUQsc0NBQVEsR0FBUixVQUFTLE1BQVc7UUFBcEIsaUJBNkJDO1FBM0JHLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBRXRCLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUkscUJBQWEsRUFBRSxDQUFDLENBQUM7UUFFaEUsSUFBSSxjQUFjLEdBQUcsSUFBSSxnQ0FBd0IsRUFBRSxDQUFDO1FBQ3BELGNBQWMsQ0FBQyxJQUFJLENBQUM7WUFDaEIsU0FBUyxFQUFFLE1BQU0sQ0FBQyxTQUFTO1lBQzNCLFFBQVEsRUFBRSxNQUFNLENBQUMsUUFBUTtZQUN6QixLQUFLLEVBQUUsTUFBTSxDQUFDLEtBQUs7WUFDbkIsV0FBVyxFQUFFLE1BQU0sQ0FBQyxXQUFXO1lBQy9CLFFBQVEsRUFBRSxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVE7WUFDbkMsZUFBZSxFQUFFLE1BQU0sQ0FBQyxTQUFTLENBQUMsZUFBZTtZQUNqRCxZQUFZLEVBQUUsSUFBSSxDQUFDLFlBQVk7U0FDbEMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLGNBQWM7YUFDZCxtQkFBbUIsQ0FBQyxjQUFjLENBQUM7YUFDbkMsU0FBUyxDQUFDLFVBQUEsR0FBRztZQUNWLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDOUUsS0FBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLGNBQWMsRUFBRSxFQUFFLENBQUMsQ0FBQztZQUN4RCxLQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztRQUM1QixDQUFDLEVBQ0QsVUFBQSxHQUFHO1lBQ0MsS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxXQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUM5RSxLQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixLQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQzNCLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQzs7SUFFRCxrREFBb0IsR0FBcEIsVUFBcUIsS0FBYTtRQUM5QixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztJQUN4QixDQUFDOztJQUVELGtEQUFvQixHQUFwQjtRQUNJLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO0lBQzdCLENBQUM7SUFFRCxpREFBbUIsR0FBbkIsVUFBb0IsS0FBYTtRQUM3QixJQUFJLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDbkIsRUFBRSxDQUFDLENBQUMsS0FBSyxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDbkIsTUFBTSxHQUFHLElBQUksQ0FBQztRQUNsQixDQUFDO1FBRUQsTUFBTSxDQUFDLE1BQU0sQ0FBQztJQUNsQixDQUFDOztJQS9GRDtRQUFDLGdCQUFTLENBQUMsV0FBVyxDQUFDOzswREFBQTtJQVIzQjtRQUFDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLGlCQUFpQjtZQUMzQixXQUFXLEVBQUUsMkJBQTJCO1NBQzNDLENBQUM7OzJCQUFBO0lBb0dGLDBCQUFDO0FBQUQsQ0FsR0EsQUFrR0MsSUFBQTtBQWxHWSwyQkFBbUIsc0JBa0cvQixDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3NpZ251cC9zaWdudXBGb3JtL3NpZ251cEZvcm0uY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBWaWV3Q2hpbGQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybUdyb3VwLCBBYnN0cmFjdENvbnRyb2wsIEZvcm1CdWlsZGVyLCBWYWxpZGF0b3JzIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBFbWFpbFZhbGlkYXRvciwgTW9iaWxlVmFsaWRhdG9yLCBDb21wbGV4UGFzc3dvcmRWYWxpZGF0b3IsIEVxdWFsUGFzc3dvcmRzVmFsaWRhdG9yIH0gZnJvbSAnLi4vLi4vLi4vLi4vdmFsaWRhdG9ycy9pbmRleCc7XHJcblxyXG5pbXBvcnQgeyBTaWdudXBTZXJ2aWNlIH0gZnJvbSAnLi4vc2lnbnVwU2VydmljZS9zaWdudXAuc2VydmljZSc7XHJcbmltcG9ydCB7IFRvYXN0ZXJTZXJ2aWNlIH0gZnJvbSAnYW5ndWxhcjItdG9hc3Rlcic7XHJcbmltcG9ydCB7IENvbmZpZywgU3Bpbm5pbmdUb2FzdCB9IGZyb20gJy4uLy4uLy4uLy4uL3NoYXJlZC9pbmRleCc7XHJcbmltcG9ydCB7IFJlZ2lzdGVyQ2VudGVyQWRtaW5Nb2RlbCB9IGZyb20gJy4uLy4uLy4uLy4uL2FwaS9pbmRleCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICBzZWxlY3RvcjogJ3NpZ251cC1mb3JtLWNtcCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJ3NpZ251cEZvcm0uY29tcG9uZW50Lmh0bWwnXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgU2lnbnVwRm9ybUNvbXBvbmVudCB7XHJcblxyXG4gICAgQFZpZXdDaGlsZCgncmVjYXB0Y2hhJykgcmVjYXB0Y2hhOiBhbnk7XHJcblxyXG4gICAgcHVibGljIGZvcm06IEZvcm1Hcm91cDtcclxuICAgIHB1YmxpYyBmaXJzdE5hbWU6IEFic3RyYWN0Q29udHJvbDtcclxuICAgIHB1YmxpYyBsYXN0TmFtZTogQWJzdHJhY3RDb250cm9sO1xyXG4gICAgcHVibGljIGVtYWlsOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgICBwdWJsaWMgbW9iaWxlUGhvbmU6IEFic3RyYWN0Q29udHJvbDtcclxuICAgIHB1YmxpYyBwYXNzd29yZDogQWJzdHJhY3RDb250cm9sO1xyXG4gICAgcHVibGljIGNvbmZpcm1QYXNzd29yZDogQWJzdHJhY3RDb250cm9sO1xyXG4gICAgcHVibGljIHBhc3N3b3JkczogRm9ybUdyb3VwO1xyXG5cclxuICAgIHB1YmxpYyBzdWJtaXR0ZWQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIHB1YmxpYyB1c2VyQ3JlYXRlZDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgcHVibGljIGNhcHRjaGE6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIHB1YmxpYyBjYXB0Y2hhVG9rZW46IHN0cmluZyA9IG51bGw7XHJcbiAgICBwdWJsaWMgY2FwdGNoYV9rZXk6IHN0cmluZyA9IENvbmZpZy5SRUNBUFRDSEFfVUlfS0VZO1xyXG4gICAgcHVibGljIGNhcHRjaGFfZW5hYmxlZDogYm9vbGVhbiA9IHRoaXMuY29udmVydFN0cmluZ1RvQm9vbChDb25maWcuUmVjYXB0Y2hhRW5hYmxlZCk7XHJcblxyXG4gICAgY29uc3RydWN0b3IoZmI6IEZvcm1CdWlsZGVyLFxyXG4gICAgICAgIHByaXZhdGUgX3NpZ251cFNlcnZpY2U6IFNpZ251cFNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBfdG9hc3RlclNlcnZpY2U6IFRvYXN0ZXJTZXJ2aWNlKSB7XHJcblxyXG4gICAgICAgIHRoaXMuZm9ybSA9IGZiLmdyb3VwKHtcclxuICAgICAgICAgICAgJ2ZpcnN0TmFtZSc6IFsnJywgVmFsaWRhdG9ycy5jb21wb3NlKFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1pbkxlbmd0aCgxKV0pXSxcclxuICAgICAgICAgICAgJ2xhc3ROYW1lJzogWycnLCBWYWxpZGF0b3JzLmNvbXBvc2UoW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWluTGVuZ3RoKDEpXSldLFxyXG4gICAgICAgICAgICAnZW1haWwnOiBbJycsIFZhbGlkYXRvcnMuY29tcG9zZShbVmFsaWRhdG9ycy5yZXF1aXJlZCwgRW1haWxWYWxpZGF0b3IudmFsaWRhdGVdKV0sXHJcbiAgICAgICAgICAgICdtb2JpbGVQaG9uZSc6IFsnJywgVmFsaWRhdG9ycy5jb21wb3NlKFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBNb2JpbGVWYWxpZGF0b3IudmFsaWRhdGVdKV0sXHJcbiAgICAgICAgICAgICdwYXNzd29yZHMnOiBmYi5ncm91cCh7XHJcbiAgICAgICAgICAgICAgICAncGFzc3dvcmQnOiBbJycsIFZhbGlkYXRvcnMuY29tcG9zZShbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5taW5MZW5ndGgoOCksIENvbXBsZXhQYXNzd29yZFZhbGlkYXRvci52YWxpZGF0ZV0pXSxcclxuICAgICAgICAgICAgICAgICdjb25maXJtUGFzc3dvcmQnOiBbJycsIFZhbGlkYXRvcnMuY29tcG9zZShbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5taW5MZW5ndGgoOCldKV1cclxuICAgICAgICAgICAgfSwgeyB2YWxpZGF0b3I6IEVxdWFsUGFzc3dvcmRzVmFsaWRhdG9yLnZhbGlkYXRlKCdwYXNzd29yZCcsICdjb25maXJtUGFzc3dvcmQnKSB9KVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICB0aGlzLmZpcnN0TmFtZSA9IHRoaXMuZm9ybS5jb250cm9sc1snZmlyc3ROYW1lJ107XHJcbiAgICAgICAgdGhpcy5sYXN0TmFtZSA9IHRoaXMuZm9ybS5jb250cm9sc1snbGFzdE5hbWUnXTtcclxuICAgICAgICB0aGlzLmVtYWlsID0gdGhpcy5mb3JtLmNvbnRyb2xzWydlbWFpbCddO1xyXG4gICAgICAgIHRoaXMubW9iaWxlUGhvbmUgPSB0aGlzLmZvcm0uY29udHJvbHNbJ21vYmlsZVBob25lJ107XHJcbiAgICAgICAgdGhpcy5wYXNzd29yZHMgPSA8Rm9ybUdyb3VwPnRoaXMuZm9ybS5jb250cm9sc1sncGFzc3dvcmRzJ107XHJcbiAgICAgICAgdGhpcy5wYXNzd29yZCA9IHRoaXMucGFzc3dvcmRzLmNvbnRyb2xzWydwYXNzd29yZCddO1xyXG4gICAgICAgIHRoaXMuY29uZmlybVBhc3N3b3JkID0gdGhpcy5wYXNzd29yZHMuY29udHJvbHNbJ2NvbmZpcm1QYXNzd29yZCddO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5jYXB0Y2hhX2VuYWJsZWQgPT09IGZhbHNlKSB7XHJcbiAgICAgICAgICAgIC8vaWYgaXRzIGRpc2FibGVkIGxldCB1c2VyIGNsaWNrIGJ1dHRvblxyXG4gICAgICAgICAgICB0aGlzLmNhcHRjaGEgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICB9XHJcblxyXG4gICAgb25TdWJtaXQodmFsdWVzOiBhbnkpIHtcclxuXHJcbiAgICAgICAgdGhpcy5zdWJtaXR0ZWQgPSB0cnVlO1xyXG5cclxuICAgICAgICBsZXQgc2F2aW5nVG9hc3QgPSB0aGlzLl90b2FzdGVyU2VydmljZS5wb3AobmV3IFNwaW5uaW5nVG9hc3QoKSk7XHJcblxyXG4gICAgICAgIHZhciBuZXdDZW50cmVBZG1pbiA9IG5ldyBSZWdpc3RlckNlbnRlckFkbWluTW9kZWwoKTtcclxuICAgICAgICBuZXdDZW50cmVBZG1pbi5pbml0KHtcclxuICAgICAgICAgICAgRmlyc3ROYW1lOiB2YWx1ZXMuZmlyc3ROYW1lLFxyXG4gICAgICAgICAgICBMYXN0TmFtZTogdmFsdWVzLmxhc3ROYW1lLFxyXG4gICAgICAgICAgICBFbWFpbDogdmFsdWVzLmVtYWlsLFxyXG4gICAgICAgICAgICBNb2JpbGVQaG9uZTogdmFsdWVzLm1vYmlsZVBob25lLFxyXG4gICAgICAgICAgICBQYXNzd29yZDogdmFsdWVzLnBhc3N3b3Jkcy5wYXNzd29yZCxcclxuICAgICAgICAgICAgQ29uZmlybVBhc3N3b3JkOiB2YWx1ZXMucGFzc3dvcmRzLmNvbmZpcm1QYXNzd29yZCxcclxuICAgICAgICAgICAgQ2FwdGNoYVRva2VuOiB0aGlzLmNhcHRjaGFUb2tlblxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICB0aGlzLl9zaWdudXBTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5SZWdpc3RlckNlbnRyZUFkbWluKG5ld0NlbnRyZUFkbWluKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKHJlcyA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcihzYXZpbmdUb2FzdC50b2FzdElkLCBzYXZpbmdUb2FzdC50b2FzdENvbnRhaW5lcklkKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLnBvcCgnc3VjY2VzcycsICdVc2VyIENyZWF0ZWQnLCAnJyk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnVzZXJDcmVhdGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgZXJyID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLmNsZWFyKHNhdmluZ1RvYXN0LnRvYXN0SWQsIHNhdmluZ1RvYXN0LnRvYXN0Q29udGFpbmVySWQpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zdWJtaXR0ZWQgPSBmYWxzZTsgLy91c2VyIG5lZWRzIHRvIGJlIGFibGUgdG8gY2hhbmdlIHRoaW5ncyBhbmQgdHJ5IGFnYWluXHJcbiAgICAgICAgICAgICAgICB0aGlzLnJlY2FwdGNoYS5yZXNldCgpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgaGFuZGxlQ29ycmVjdENhcHRjaGEoZXZlbnQ6IHN0cmluZyk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuY2FwdGNoYVRva2VuID0gZXZlbnQ7XHJcbiAgICAgICAgdGhpcy5jYXB0Y2hhID0gdHJ1ZTtcclxuICAgIH07XHJcblxyXG4gICAgaGFuZGxlQ2FwdGNoYUV4cGlyZWQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5jYXB0Y2hhVG9rZW4gPSBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnZlcnRTdHJpbmdUb0Jvb2woaW5wdXQ6IHN0cmluZyk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHZhciByZXN1bHQgPSBmYWxzZTtcclxuICAgICAgICBpZiAoaW5wdXQgPT09ICd0cnVlJykge1xyXG4gICAgICAgICAgICByZXN1bHQgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgIH07XHJcbn1cclxuIl19
