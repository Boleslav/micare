"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var angular2_toaster_1 = require('angular2-toaster');
var index_1 = require('../../core/index');
var index_2 = require('../../shared/index');
var CentrePortalComponent = (function () {
    function CentrePortalComponent(_userService, _toasterService, _intercomService) {
        this._userService = _userService;
        this._toasterService = _toasterService;
        this._intercomService = _intercomService;
        _intercomService.initUserLoggedIn(index_2.Config.INTERCOM_APP_ID, this._userService.Email);
    }
    CentrePortalComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            encapsulation: core_1.ViewEncapsulation.None,
            selector: 'centre-portal-cmp',
            templateUrl: 'centre-portal.component.html',
            styleUrls: ['centre-portal.component.css']
        }), 
        __metadata('design:paramtypes', [index_1.CentreUserService, angular2_toaster_1.ToasterService, index_2.IntercomService])
    ], CentrePortalComponent);
    return CentrePortalComponent;
}());
exports.CentrePortalComponent = CentrePortalComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NlbnRyZS1wb3J0YWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBNkMsZUFBZSxDQUFDLENBQUE7QUFFN0QsaUNBQStCLGtCQUFrQixDQUFDLENBQUE7QUFDbEQsc0JBQWtDLGtCQUFrQixDQUFDLENBQUE7QUFDckQsc0JBQXdDLG9CQUFvQixDQUFDLENBQUE7QUFTN0Q7SUFFSSwrQkFBb0IsWUFBK0IsRUFDdkMsZUFBK0IsRUFDL0IsZ0JBQWlDO1FBRnpCLGlCQUFZLEdBQVosWUFBWSxDQUFtQjtRQUN2QyxvQkFBZSxHQUFmLGVBQWUsQ0FBZ0I7UUFDL0IscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFpQjtRQUV6QyxnQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFNLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDdkYsQ0FBQztJQWRMO1FBQUMsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixhQUFhLEVBQUUsd0JBQWlCLENBQUMsSUFBSTtZQUNyQyxRQUFRLEVBQUUsbUJBQW1CO1lBQzdCLFdBQVcsRUFBRSw4QkFBOEI7WUFDM0MsU0FBUyxFQUFFLENBQUMsNkJBQTZCLENBQUM7U0FDN0MsQ0FBQzs7NkJBQUE7SUFTRiw0QkFBQztBQUFELENBUkEsQUFRQyxJQUFBO0FBUlksNkJBQXFCLHdCQVFqQyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NlbnRyZS1wb3J0YWwuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBWaWV3RW5jYXBzdWxhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgVG9hc3RlclNlcnZpY2UgfSBmcm9tICdhbmd1bGFyMi10b2FzdGVyJztcclxuaW1wb3J0IHsgQ2VudHJlVXNlclNlcnZpY2UgfSBmcm9tICcuLi8uLi9jb3JlL2luZGV4JztcclxuaW1wb3J0IHsgQ29uZmlnLCBJbnRlcmNvbVNlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvaW5kZXgnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcclxuICAgIHNlbGVjdG9yOiAnY2VudHJlLXBvcnRhbC1jbXAnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICdjZW50cmUtcG9ydGFsLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWydjZW50cmUtcG9ydGFsLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ2VudHJlUG9ydGFsQ29tcG9uZW50IHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF91c2VyU2VydmljZTogQ2VudHJlVXNlclNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBfdG9hc3RlclNlcnZpY2U6IFRvYXN0ZXJTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgX2ludGVyY29tU2VydmljZTogSW50ZXJjb21TZXJ2aWNlKSB7XHJcblxyXG4gICAgICAgIF9pbnRlcmNvbVNlcnZpY2UuaW5pdFVzZXJMb2dnZWRJbihDb25maWcuSU5URVJDT01fQVBQX0lELCB0aGlzLl91c2VyU2VydmljZS5FbWFpbCk7XHJcbiAgICB9XHJcbn1cclxuIl19
