"use strict";
var centre_portal_component_1 = require('./centre-portal.component');
var index_1 = require('../../auth/index');
var index_2 = require('../../shared/index');
var index_3 = require('./admin-dashboard/index');
var index_4 = require('./admin-approvals/index');
var index_5 = require('./centre-dashboard/index');
var index_6 = require('./companies/index');
var index_7 = require('./centre/index');
var index_8 = require('./rooms/index');
var index_9 = require('./staff/index');
var index_10 = require('./bookings/index');
var index_11 = require('./shutdowndays/index');
var index_12 = require('./swaps/index');
var index_13 = require('./vacancies/index');
var index_14 = require('./enrolment/index');
var index_15 = require('./login/index');
var index_16 = require('./logout/index');
var index_17 = require('./signup/index');
var index_18 = require('./staff-signup/index');
var index_19 = require('./enrolment-approvals/index');
var index_20 = require('./family-enrolment-form/index');
var tools_routing_1 = require("./tools/tools.routing");
exports.CentrePortalRoutes = [
    {
        path: 'centre-portal',
        component: centre_portal_component_1.CentrePortalComponent,
        canActivate: [index_1.CentreAuthGuardService, index_2.RolesGuardService],
        canActivateChild: [index_2.MixpanelGuard],
        children: [
            { path: '', redirectTo: 'centre-dashboard', canActivate: [index_2.NavigationGuardService], pathMatch: 'full' }
        ].concat(index_5.CentreDashboardRoutes, index_9.StaffRoutes, index_6.CompaniesRoutes, index_7.CentreRoutes, index_8.RoomsRoutes, index_10.BookingsRoutes, index_11.ShutdownDaysRoutes, index_12.SwapsRoutes, index_13.VacanciesRoutes, index_14.EnrolmentRoutes, index_3.AdminDashboardRoutes, index_4.AdminApprovalsRoutes, index_19.EnrolmentApprovalsRoutes, index_20.FamilyEnrolmentFormRoutes, tools_routing_1.ToolsRoutes, [
            { path: 'redirect/:redirectTo', component: index_2.RedirectComponent },
            { path: '**', redirectTo: 'swaps' }
        ])
    }
].concat(index_15.LoginRoutes, index_16.LogoutRoutes, index_17.SignupRoutes, index_18.StaffSignupRoutes);

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NlbnRyZS1wb3J0YWwucm91dGluZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0Esd0NBQXNDLDJCQUEyQixDQUFDLENBQUE7QUFDbEUsc0JBQXVDLGtCQUFrQixDQUFDLENBQUE7QUFDMUQsc0JBR08sb0JBQW9CLENBQUMsQ0FBQTtBQUU1QixzQkFBcUMseUJBQXlCLENBQUMsQ0FBQTtBQUMvRCxzQkFBcUMseUJBQXlCLENBQUMsQ0FBQTtBQUMvRCxzQkFBc0MsMEJBQTBCLENBQUMsQ0FBQTtBQUNqRSxzQkFBZ0MsbUJBQW1CLENBQUMsQ0FBQTtBQUNwRCxzQkFBNkIsZ0JBQWdCLENBQUMsQ0FBQTtBQUM5QyxzQkFBNEIsZUFBZSxDQUFDLENBQUE7QUFDNUMsc0JBQTRCLGVBQWUsQ0FBQyxDQUFBO0FBQzVDLHVCQUErQixrQkFBa0IsQ0FBQyxDQUFBO0FBQ2xELHVCQUFtQyxzQkFBc0IsQ0FBQyxDQUFBO0FBQzFELHVCQUE0QixlQUFlLENBQUMsQ0FBQTtBQUM1Qyx1QkFBZ0MsbUJBQW1CLENBQUMsQ0FBQTtBQUNwRCx1QkFBZ0MsbUJBQW1CLENBQUMsQ0FBQTtBQUNwRCx1QkFBNEIsZUFBZSxDQUFDLENBQUE7QUFDNUMsdUJBQTZCLGdCQUFnQixDQUFDLENBQUE7QUFDOUMsdUJBQTZCLGdCQUFnQixDQUFDLENBQUE7QUFDOUMsdUJBQWtDLHNCQUFzQixDQUFDLENBQUE7QUFDekQsdUJBQXlDLDZCQUE2QixDQUFDLENBQUE7QUFDdkUsdUJBQTBDLCtCQUErQixDQUFDLENBQUE7QUFDMUUsOEJBQTJCLHVCQUF1QixDQUFDLENBQUE7QUFFdEMsMEJBQWtCLEdBQVc7SUFDdEM7UUFDSSxJQUFJLEVBQUUsZUFBZTtRQUNyQixTQUFTLEVBQUUsK0NBQXFCO1FBQ2hDLFdBQVcsRUFBRSxDQUFDLDhCQUFzQixFQUFFLHlCQUFpQixDQUFDO1FBQ3hELGdCQUFnQixFQUFFLENBQUMscUJBQWEsQ0FBQztRQUNqQyxRQUFRLEVBQUU7WUFDTixFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsVUFBVSxFQUFFLGtCQUFrQixFQUFFLFdBQVcsRUFBRSxDQUFDLDhCQUFzQixDQUFDLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRTtpQkFDbkcsNkJBQXFCLEVBQ3JCLG1CQUFXLEVBQ1gsdUJBQWUsRUFDZixvQkFBWSxFQUNaLG1CQUFXLEVBQ1gsdUJBQWMsRUFDZCwyQkFBa0IsRUFDbEIsb0JBQVcsRUFDWCx3QkFBZSxFQUNmLHdCQUFlLEVBQ2YsNEJBQW9CLEVBQ3BCLDRCQUFvQixFQUNwQixpQ0FBd0IsRUFDeEIsa0NBQXlCLEVBQ3pCLDJCQUFXO1lBQ2QsRUFBRSxJQUFJLEVBQUUsc0JBQXNCLEVBQUUsU0FBUyxFQUFFLHlCQUFpQixFQUFFO1lBQzlELEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFFO1VBQ3RDO0tBQ0o7U0FDRSxvQkFBVyxFQUNYLHFCQUFZLEVBQ1oscUJBQVksRUFDWiwwQkFBaUIsQ0FDdkIsQ0FBQyIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9jZW50cmUtcG9ydGFsLnJvdXRpbmcuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSb3V0ZXMgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgQ2VudHJlUG9ydGFsQ29tcG9uZW50IH0gZnJvbSAnLi9jZW50cmUtcG9ydGFsLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDZW50cmVBdXRoR3VhcmRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vYXV0aC9pbmRleCc7XG5pbXBvcnQge1xuICAgIE5hdmlnYXRpb25HdWFyZFNlcnZpY2UsIFJvbGVzR3VhcmRTZXJ2aWNlLFxuICAgIE1peHBhbmVsR3VhcmQsIFJlZGlyZWN0Q29tcG9uZW50XG59IGZyb20gJy4uLy4uL3NoYXJlZC9pbmRleCc7XG5cbmltcG9ydCB7IEFkbWluRGFzaGJvYXJkUm91dGVzIH0gZnJvbSAnLi9hZG1pbi1kYXNoYm9hcmQvaW5kZXgnO1xuaW1wb3J0IHsgQWRtaW5BcHByb3ZhbHNSb3V0ZXMgfSBmcm9tICcuL2FkbWluLWFwcHJvdmFscy9pbmRleCc7XG5pbXBvcnQgeyBDZW50cmVEYXNoYm9hcmRSb3V0ZXMgfSBmcm9tICcuL2NlbnRyZS1kYXNoYm9hcmQvaW5kZXgnO1xuaW1wb3J0IHsgQ29tcGFuaWVzUm91dGVzIH0gZnJvbSAnLi9jb21wYW5pZXMvaW5kZXgnO1xuaW1wb3J0IHsgQ2VudHJlUm91dGVzIH0gZnJvbSAnLi9jZW50cmUvaW5kZXgnO1xuaW1wb3J0IHsgUm9vbXNSb3V0ZXMgfSBmcm9tICcuL3Jvb21zL2luZGV4JztcbmltcG9ydCB7IFN0YWZmUm91dGVzIH0gZnJvbSAnLi9zdGFmZi9pbmRleCc7XG5pbXBvcnQgeyBCb29raW5nc1JvdXRlcyB9IGZyb20gJy4vYm9va2luZ3MvaW5kZXgnO1xuaW1wb3J0IHsgU2h1dGRvd25EYXlzUm91dGVzIH0gZnJvbSAnLi9zaHV0ZG93bmRheXMvaW5kZXgnO1xuaW1wb3J0IHsgU3dhcHNSb3V0ZXMgfSBmcm9tICcuL3N3YXBzL2luZGV4JztcbmltcG9ydCB7IFZhY2FuY2llc1JvdXRlcyB9IGZyb20gJy4vdmFjYW5jaWVzL2luZGV4JztcbmltcG9ydCB7IEVucm9sbWVudFJvdXRlcyB9IGZyb20gJy4vZW5yb2xtZW50L2luZGV4JztcbmltcG9ydCB7IExvZ2luUm91dGVzIH0gZnJvbSAnLi9sb2dpbi9pbmRleCc7XG5pbXBvcnQgeyBMb2dvdXRSb3V0ZXMgfSBmcm9tICcuL2xvZ291dC9pbmRleCc7XG5pbXBvcnQgeyBTaWdudXBSb3V0ZXMgfSBmcm9tICcuL3NpZ251cC9pbmRleCc7XG5pbXBvcnQgeyBTdGFmZlNpZ251cFJvdXRlcyB9IGZyb20gJy4vc3RhZmYtc2lnbnVwL2luZGV4JztcbmltcG9ydCB7IEVucm9sbWVudEFwcHJvdmFsc1JvdXRlcyB9IGZyb20gJy4vZW5yb2xtZW50LWFwcHJvdmFscy9pbmRleCc7XG5pbXBvcnQgeyBGYW1pbHlFbnJvbG1lbnRGb3JtUm91dGVzIH0gZnJvbSAnLi9mYW1pbHktZW5yb2xtZW50LWZvcm0vaW5kZXgnO1xuaW1wb3J0IHsgVG9vbHNSb3V0ZXN9IGZyb20gXCIuL3Rvb2xzL3Rvb2xzLnJvdXRpbmdcIjtcblxuZXhwb3J0IGNvbnN0IENlbnRyZVBvcnRhbFJvdXRlczogUm91dGVzID0gW1xuICAgIHtcbiAgICAgICAgcGF0aDogJ2NlbnRyZS1wb3J0YWwnLFxuICAgICAgICBjb21wb25lbnQ6IENlbnRyZVBvcnRhbENvbXBvbmVudCxcbiAgICAgICAgY2FuQWN0aXZhdGU6IFtDZW50cmVBdXRoR3VhcmRTZXJ2aWNlLCBSb2xlc0d1YXJkU2VydmljZV0sXG4gICAgICAgIGNhbkFjdGl2YXRlQ2hpbGQ6IFtNaXhwYW5lbEd1YXJkXSxcbiAgICAgICAgY2hpbGRyZW46IFtcbiAgICAgICAgICAgIHsgcGF0aDogJycsIHJlZGlyZWN0VG86ICdjZW50cmUtZGFzaGJvYXJkJywgY2FuQWN0aXZhdGU6IFtOYXZpZ2F0aW9uR3VhcmRTZXJ2aWNlXSwgcGF0aE1hdGNoOiAnZnVsbCcgfSxcbiAgICAgICAgICAgIC4uLkNlbnRyZURhc2hib2FyZFJvdXRlcyxcbiAgICAgICAgICAgIC4uLlN0YWZmUm91dGVzLFxuICAgICAgICAgICAgLi4uQ29tcGFuaWVzUm91dGVzLFxuICAgICAgICAgICAgLi4uQ2VudHJlUm91dGVzLFxuICAgICAgICAgICAgLi4uUm9vbXNSb3V0ZXMsXG4gICAgICAgICAgICAuLi5Cb29raW5nc1JvdXRlcyxcbiAgICAgICAgICAgIC4uLlNodXRkb3duRGF5c1JvdXRlcyxcbiAgICAgICAgICAgIC4uLlN3YXBzUm91dGVzLFxuICAgICAgICAgICAgLi4uVmFjYW5jaWVzUm91dGVzLFxuICAgICAgICAgICAgLi4uRW5yb2xtZW50Um91dGVzLFxuICAgICAgICAgICAgLi4uQWRtaW5EYXNoYm9hcmRSb3V0ZXMsXG4gICAgICAgICAgICAuLi5BZG1pbkFwcHJvdmFsc1JvdXRlcyxcbiAgICAgICAgICAgIC4uLkVucm9sbWVudEFwcHJvdmFsc1JvdXRlcyxcbiAgICAgICAgICAgIC4uLkZhbWlseUVucm9sbWVudEZvcm1Sb3V0ZXMsXG4gICAgICAgICAgICAuLi5Ub29sc1JvdXRlcyxcbiAgICAgICAgICAgIHsgcGF0aDogJ3JlZGlyZWN0LzpyZWRpcmVjdFRvJywgY29tcG9uZW50OiBSZWRpcmVjdENvbXBvbmVudCB9LFxuICAgICAgICAgICAgeyBwYXRoOiAnKionLCByZWRpcmVjdFRvOiAnc3dhcHMnIH1cbiAgICAgICAgXVxuICAgIH0sXG4gICAgLi4uTG9naW5Sb3V0ZXMsXG4gICAgLi4uTG9nb3V0Um91dGVzLFxuICAgIC4uLlNpZ251cFJvdXRlcyxcbiAgICAuLi5TdGFmZlNpZ251cFJvdXRlcyxcbl07XG4iXX0=
