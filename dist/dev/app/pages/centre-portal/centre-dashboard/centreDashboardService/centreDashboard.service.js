"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../../api/index');
var CentreDashboardService = (function () {
    function CentreDashboardService(_apiService) {
        this._apiService = _apiService;
    }
    CentreDashboardService.prototype.getCentrePendingApprovalsCount = function (centreId) {
        return this._apiService.centreDashboard_GetCentrePendingApprovalsCount(centreId);
    };
    CentreDashboardService.prototype.getCentreAvailableDaysCount = function (centreId) {
        return this._apiService.centreDashboard_GetCentreAvailableDaysCount(centreId);
    };
    CentreDashboardService.prototype.getCentreParentCount = function (centreId) {
        return this._apiService.centreDashboard_GetParentCountAsync(centreId);
    };
    CentreDashboardService.prototype.getCentreTotalIncome = function (centreId) {
        return this._apiService.centreDashboard_GetCentreTotalIncome(centreId);
    };
    CentreDashboardService.prototype.getCentreDashboardData$ = function (centreId) {
        return this._apiService.centreDashboard_GetGraphDashboardData(centreId);
    };
    CentreDashboardService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.ApiClientService])
    ], CentreDashboardService);
    return CentreDashboardService;
}());
exports.CentreDashboardService = CentreDashboardService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NlbnRyZS1kYXNoYm9hcmQvY2VudHJlRGFzaGJvYXJkU2VydmljZS9jZW50cmVEYXNoYm9hcmQuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQTJCLGVBQWUsQ0FBQyxDQUFBO0FBRzNDLHNCQUEwRCx1QkFBdUIsQ0FBQyxDQUFBO0FBR2xGO0lBRUksZ0NBQW9CLFdBQTZCO1FBQTdCLGdCQUFXLEdBQVgsV0FBVyxDQUFrQjtJQUVqRCxDQUFDO0lBRU0sK0RBQThCLEdBQXJDLFVBQXNDLFFBQWdCO1FBQ2xELE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLDhDQUE4QyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3JGLENBQUM7SUFFTSw0REFBMkIsR0FBbEMsVUFBbUMsUUFBZ0I7UUFDL0MsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsMkNBQTJDLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDbEYsQ0FBQztJQUVNLHFEQUFvQixHQUEzQixVQUE0QixRQUFnQjtRQUN4QyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQ0FBbUMsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUMxRSxDQUFDO0lBRU0scURBQW9CLEdBQTNCLFVBQTRCLFFBQWdCO1FBQ3hDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLG9DQUFvQyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQzNFLENBQUM7SUFFTSx3REFBdUIsR0FBOUIsVUFBK0IsUUFBZ0I7UUFDM0MsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMscUNBQXFDLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDNUUsQ0FBQztJQXpCTDtRQUFDLGlCQUFVLEVBQUU7OzhCQUFBO0lBMEJiLDZCQUFDO0FBQUQsQ0F6QkEsQUF5QkMsSUFBQTtBQXpCWSw4QkFBc0IseUJBeUJsQyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NlbnRyZS1kYXNoYm9hcmQvY2VudHJlRGFzaGJvYXJkU2VydmljZS9jZW50cmVEYXNoYm9hcmQuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgQXBpQ2xpZW50U2VydmljZSwgQ2VudHJlRGFzaGJvYXJkR3JhcGhEdG8gfSBmcm9tICcuLi8uLi8uLi8uLi9hcGkvaW5kZXgnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgQ2VudHJlRGFzaGJvYXJkU2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfYXBpU2VydmljZTogQXBpQ2xpZW50U2VydmljZSkge1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0Q2VudHJlUGVuZGluZ0FwcHJvdmFsc0NvdW50KGNlbnRyZUlkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPG51bWJlcj4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9hcGlTZXJ2aWNlLmNlbnRyZURhc2hib2FyZF9HZXRDZW50cmVQZW5kaW5nQXBwcm92YWxzQ291bnQoY2VudHJlSWQpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRDZW50cmVBdmFpbGFibGVEYXlzQ291bnQoY2VudHJlSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8bnVtYmVyPiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2FwaVNlcnZpY2UuY2VudHJlRGFzaGJvYXJkX0dldENlbnRyZUF2YWlsYWJsZURheXNDb3VudChjZW50cmVJZCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldENlbnRyZVBhcmVudENvdW50KGNlbnRyZUlkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPG51bWJlcj4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9hcGlTZXJ2aWNlLmNlbnRyZURhc2hib2FyZF9HZXRQYXJlbnRDb3VudEFzeW5jKGNlbnRyZUlkKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0Q2VudHJlVG90YWxJbmNvbWUoY2VudHJlSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8bnVtYmVyPiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2FwaVNlcnZpY2UuY2VudHJlRGFzaGJvYXJkX0dldENlbnRyZVRvdGFsSW5jb21lKGNlbnRyZUlkKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0Q2VudHJlRGFzaGJvYXJkRGF0YSQoY2VudHJlSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8Q2VudHJlRGFzaGJvYXJkR3JhcGhEdG8+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fYXBpU2VydmljZS5jZW50cmVEYXNoYm9hcmRfR2V0R3JhcGhEYXNoYm9hcmREYXRhKGNlbnRyZUlkKTtcclxuICAgIH1cclxufVxyXG4iXX0=
