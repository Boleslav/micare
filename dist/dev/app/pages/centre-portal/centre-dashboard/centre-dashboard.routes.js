"use strict";
var index_1 = require('./index');
var index_2 = require('../../../shared/index');
exports.CentreDashboardRoutes = [
    {
        path: 'centre-dashboard',
        component: index_1.CentreDashboardComponent,
        canActivate: [index_2.NavigationGuardService]
    },
];

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NlbnRyZS1kYXNoYm9hcmQvY2VudHJlLWRhc2hib2FyZC5yb3V0ZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBLHNCQUF5QyxTQUFTLENBQUMsQ0FBQTtBQUNuRCxzQkFBdUMsdUJBQXVCLENBQUMsQ0FBQTtBQUVsRCw2QkFBcUIsR0FBWTtJQUM3QztRQUNDLElBQUksRUFBRSxrQkFBa0I7UUFDeEIsU0FBUyxFQUFFLGdDQUF3QjtRQUNuQyxXQUFXLEVBQUUsQ0FBQyw4QkFBc0IsQ0FBQztLQUNyQztDQUNELENBQUMiLCJmaWxlIjoiYXBwL3BhZ2VzL2NlbnRyZS1wb3J0YWwvY2VudHJlLWRhc2hib2FyZC9jZW50cmUtZGFzaGJvYXJkLnJvdXRlcy5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgQ2VudHJlRGFzaGJvYXJkQ29tcG9uZW50IH0gZnJvbSAnLi9pbmRleCc7XHJcbmltcG9ydCB7IE5hdmlnYXRpb25HdWFyZFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvaW5kZXgnO1xyXG5cclxuZXhwb3J0IGNvbnN0IENlbnRyZURhc2hib2FyZFJvdXRlczogUm91dGVbXSA9IFtcclxuXHR7XHJcblx0XHRwYXRoOiAnY2VudHJlLWRhc2hib2FyZCcsXHJcblx0XHRjb21wb25lbnQ6IENlbnRyZURhc2hib2FyZENvbXBvbmVudCxcclxuXHRcdGNhbkFjdGl2YXRlOiBbTmF2aWdhdGlvbkd1YXJkU2VydmljZV1cclxuXHR9LFxyXG5dO1xyXG4iXX0=
