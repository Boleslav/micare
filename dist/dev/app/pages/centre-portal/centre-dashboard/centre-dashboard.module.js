"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var ng2_bootstrap_1 = require('ng2-bootstrap');
var index_1 = require('../../../shared/index');
var index_2 = require('./index');
var CentreDashboardModule = (function () {
    function CentreDashboardModule() {
    }
    CentreDashboardModule = __decorate([
        core_1.NgModule({
            imports: [
                index_1.SharedModule,
                common_1.CommonModule,
                ng2_bootstrap_1.BsDropdownModule,
                ng2_bootstrap_1.ModalModule
            ],
            declarations: [index_2.CentreDashboardComponent],
            providers: [index_2.CentreDashboardService],
            exports: [index_2.CentreDashboardComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], CentreDashboardModule);
    return CentreDashboardModule;
}());
exports.CentreDashboardModule = CentreDashboardModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NlbnRyZS1kYXNoYm9hcmQvY2VudHJlLWRhc2hib2FyZC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUF5QixlQUFlLENBQUMsQ0FBQTtBQUN6Qyx1QkFBNkIsaUJBQWlCLENBQUMsQ0FBQTtBQUMvQyw4QkFBOEMsZUFBZSxDQUFDLENBQUE7QUFFOUQsc0JBQTZCLHVCQUF1QixDQUFDLENBQUE7QUFDckQsc0JBQWlFLFNBQVMsQ0FBQyxDQUFBO0FBYzNFO0lBQUE7SUFBcUMsQ0FBQztJQVp0QztRQUFDLGVBQVEsQ0FBQztZQUNOLE9BQU8sRUFBRTtnQkFDTCxvQkFBWTtnQkFDWixxQkFBWTtnQkFDWixnQ0FBZ0I7Z0JBQ2hCLDJCQUFXO2FBQ2Q7WUFDRCxZQUFZLEVBQUUsQ0FBQyxnQ0FBd0IsQ0FBQztZQUN4QyxTQUFTLEVBQUUsQ0FBQyw4QkFBc0IsQ0FBQztZQUNuQyxPQUFPLEVBQUUsQ0FBQyxnQ0FBd0IsQ0FBQztTQUN0QyxDQUFDOzs2QkFBQTtJQUVtQyw0QkFBQztBQUFELENBQXJDLEFBQXNDLElBQUE7QUFBekIsNkJBQXFCLHdCQUFJLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL2NlbnRyZS1wb3J0YWwvY2VudHJlLWRhc2hib2FyZC9jZW50cmUtZGFzaGJvYXJkLm1vZHVsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IEJzRHJvcGRvd25Nb2R1bGUsIE1vZGFsTW9kdWxlIH0gZnJvbSAnbmcyLWJvb3RzdHJhcCc7XHJcblxyXG5pbXBvcnQgeyBTaGFyZWRNb2R1bGUgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvaW5kZXgnO1xyXG5pbXBvcnQgeyBDZW50cmVEYXNoYm9hcmRDb21wb25lbnQsIENlbnRyZURhc2hib2FyZFNlcnZpY2UgfSBmcm9tICcuL2luZGV4JztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiBbXHJcbiAgICAgICAgU2hhcmVkTW9kdWxlLFxyXG4gICAgICAgIENvbW1vbk1vZHVsZSxcclxuICAgICAgICBCc0Ryb3Bkb3duTW9kdWxlLFxyXG4gICAgICAgIE1vZGFsTW9kdWxlXHJcbiAgICBdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBbQ2VudHJlRGFzaGJvYXJkQ29tcG9uZW50XSxcclxuICAgIHByb3ZpZGVyczogW0NlbnRyZURhc2hib2FyZFNlcnZpY2VdLFxyXG4gICAgZXhwb3J0czogW0NlbnRyZURhc2hib2FyZENvbXBvbmVudF1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBDZW50cmVEYXNoYm9hcmRNb2R1bGUgeyB9XHJcbiJdfQ==
