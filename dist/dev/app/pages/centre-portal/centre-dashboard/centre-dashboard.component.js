"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var centreDashboard_service_1 = require('./centreDashboardService/centreDashboard.service');
var index_1 = require('../../../core/index');
var CentreDashboardComponent = (function () {
    function CentreDashboardComponent(_userService, _centreDashboardService) {
        this._userService = _userService;
        this._centreDashboardService = _centreDashboardService;
        this.currentYear = new Date().getFullYear();
        this.prevYear = new Date().getFullYear() - 1;
        this.nextYear = new Date().getFullYear() + 1;
        this.pendingApprovals = null;
        this.availableDays = null;
        this.parentCount = null;
        this.totalIncome = null;
        this.daysForSale = null;
        this.daysSold = null;
        this.monthlyRevenue = null;
        this.estimatedFutureIncome = null;
        this.centreName = this._userService.SelectedCentre.name;
    }
    CentreDashboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        var centreId = this._userService.SelectedCentre.id;
        this._centreDashboardService
            .getCentrePendingApprovalsCount(centreId)
            .subscribe(function (res) { return _this.pendingApprovals = res; });
        this._centreDashboardService
            .getCentreAvailableDaysCount(centreId)
            .subscribe(function (res) { return _this.availableDays = res; });
        this._centreDashboardService
            .getCentreParentCount(centreId)
            .subscribe(function (res) { return _this.parentCount = res; });
        this._centreDashboardService
            .getCentreTotalIncome(centreId)
            .subscribe(function (res) { return _this.totalIncome = res; });
        this._centreDashboardService
            .getCentreDashboardData$(centreId)
            .subscribe(function (res) {
            _this.daysForSale = _this.mapGraphData(res.daysForSale);
            _this.daysSold = _this.mapGraphData(res.daysSold);
            _this.monthlyRevenue = _this.mapGraphData(res.monthlyRevenue);
            _this.estimatedFutureIncome = _this.mapGraphData(res.estimatedFutureIncome);
            _this.initDaySalesChart();
            _this.initIncomeChart();
        });
    };
    CentreDashboardComponent.prototype.mapGraphData = function (days) {
        var arr = new Array();
        for (var key in days) {
            if (days.hasOwnProperty(key)) {
                var day = [new Date(key).getTime(), days[key]];
                arr.push(day);
            }
        }
        return arr;
    };
    CentreDashboardComponent.prototype.initDaySalesChart = function () {
        var miSwaps = $('#miSwaps-table');
        miSwaps.highcharts({
            chart: {
                type: 'spline'
            },
            title: {
                text: 'MiCare ' + this.currentYear
            },
            subtitle: {
                text: 'Number of MiCare days'
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: {
                    month: '%e. %b',
                    year: '%b'
                },
                title: {
                    text: 'Date'
                }
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Total'
                },
                min: 0
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x:%e. %b}: {point.y}'
            },
            plotOptions: {
                spline: {
                    marker: {
                        enabled: true
                    }
                }
            },
            series: [{
                    name: 'To Give',
                    data: this.daysForSale
                }, {
                    name: 'Given',
                    data: this.daysSold
                }
            ]
        });
    };
    CentreDashboardComponent.prototype.initIncomeChart = function () {
        var miSwapsIncome = $('#miSwaps-income-table');
        miSwapsIncome.highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'MiCare Income'
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: {
                    month: '%e. %b',
                    year: '%b'
                },
                title: {
                    text: 'Week'
                }
            },
            yAxis: {
                allowDecimals: true,
                min: 0,
                title: {
                    text: 'Income'
                }
            },
            tooltip: {
                formatter: function () {
                    return '<b>Total<b>: ' + this.point.stackTotal;
                }
            },
            plotOptions: {
                column: {
                    stacking: 'normal'
                }
            },
            series: [{
                    name: 'MiCare Income ' + this.prevYear + '-' + this.currentYear,
                    data: this.monthlyRevenue,
                    stack: 'actualIncome'
                }, {
                    name: 'MiCare Projected Income ' + this.currentYear + '-' + this.nextYear,
                    data: this.estimatedFutureIncome,
                    stack: 'estimatedFutureIncome'
                }
            ]
        });
    };
    CentreDashboardComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'centre-dashboard-cmp',
            templateUrl: 'centre-dashboard.component.html'
        }), 
        __metadata('design:paramtypes', [index_1.CentreUserService, centreDashboard_service_1.CentreDashboardService])
    ], CentreDashboardComponent);
    return CentreDashboardComponent;
}());
exports.CentreDashboardComponent = CentreDashboardComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NlbnRyZS1kYXNoYm9hcmQvY2VudHJlLWRhc2hib2FyZC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUFrQyxlQUFlLENBQUMsQ0FBQTtBQUVsRCx3Q0FBdUMsa0RBQWtELENBQUMsQ0FBQTtBQUMxRixzQkFBa0MscUJBQXFCLENBQUMsQ0FBQTtBQVF4RDtJQWlCSSxrQ0FBb0IsWUFBK0IsRUFDdkMsdUJBQStDO1FBRHZDLGlCQUFZLEdBQVosWUFBWSxDQUFtQjtRQUN2Qyw0QkFBdUIsR0FBdkIsdUJBQXVCLENBQXdCO1FBaEJuRCxnQkFBVyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDdkMsYUFBUSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ3hDLGFBQVEsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDLFdBQVcsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUd4QyxxQkFBZ0IsR0FBVyxJQUFJLENBQUM7UUFDaEMsa0JBQWEsR0FBVyxJQUFJLENBQUM7UUFDN0IsZ0JBQVcsR0FBVyxJQUFJLENBQUM7UUFDM0IsZ0JBQVcsR0FBVyxJQUFJLENBQUM7UUFFM0IsZ0JBQVcsR0FBb0IsSUFBSSxDQUFDO1FBQ3BDLGFBQVEsR0FBb0IsSUFBSSxDQUFDO1FBQ2pDLG1CQUFjLEdBQW9CLElBQUksQ0FBQztRQUN2QywwQkFBcUIsR0FBb0IsSUFBSSxDQUFDO1FBSWxELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDO0lBQzVELENBQUM7SUFFRCwyQ0FBUSxHQUFSO1FBQUEsaUJBOEJDO1FBN0JHLElBQUksUUFBUSxHQUFXLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FBQztRQUUzRCxJQUFJLENBQUMsdUJBQXVCO2FBQ3ZCLDhCQUE4QixDQUFDLFFBQVEsQ0FBQzthQUN4QyxTQUFTLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsR0FBRyxFQUEzQixDQUEyQixDQUFDLENBQUM7UUFFbkQsSUFBSSxDQUFDLHVCQUF1QjthQUN2QiwyQkFBMkIsQ0FBQyxRQUFRLENBQUM7YUFDckMsU0FBUyxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsS0FBSSxDQUFDLGFBQWEsR0FBRyxHQUFHLEVBQXhCLENBQXdCLENBQUMsQ0FBQztRQUVoRCxJQUFJLENBQUMsdUJBQXVCO2FBQ3ZCLG9CQUFvQixDQUFDLFFBQVEsQ0FBQzthQUM5QixTQUFTLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxLQUFJLENBQUMsV0FBVyxHQUFHLEdBQUcsRUFBdEIsQ0FBc0IsQ0FBQyxDQUFDO1FBRTlDLElBQUksQ0FBQyx1QkFBdUI7YUFDdkIsb0JBQW9CLENBQUMsUUFBUSxDQUFDO2FBQzlCLFNBQVMsQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEtBQUksQ0FBQyxXQUFXLEdBQUcsR0FBRyxFQUF0QixDQUFzQixDQUFDLENBQUM7UUFHOUMsSUFBSSxDQUFDLHVCQUF1QjthQUN2Qix1QkFBdUIsQ0FBQyxRQUFRLENBQUM7YUFDakMsU0FBUyxDQUFDLFVBQUEsR0FBRztZQUNWLEtBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDdEQsS0FBSSxDQUFDLFFBQVEsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNoRCxLQUFJLENBQUMsY0FBYyxHQUFHLEtBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQzVELEtBQUksQ0FBQyxxQkFBcUIsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQzFFLEtBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1lBQ3pCLEtBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUMzQixDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFTywrQ0FBWSxHQUFwQixVQUFxQixJQUFnQztRQUNqRCxJQUFJLEdBQUcsR0FBRyxJQUFJLEtBQUssRUFBWSxDQUFDO1FBQ2hDLEdBQUcsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDbkIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzNCLElBQUksR0FBRyxHQUFhLENBQUMsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsT0FBTyxFQUFFLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ3pELEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDbEIsQ0FBQztRQUNMLENBQUM7UUFDRCxNQUFNLENBQUMsR0FBRyxDQUFDO0lBQ2YsQ0FBQztJQUVPLG9EQUFpQixHQUF6QjtRQUNJLElBQUksT0FBTyxHQUFRLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ3ZDLE9BQU8sQ0FBQyxVQUFVLENBQUM7WUFDZixLQUFLLEVBQUU7Z0JBQ0gsSUFBSSxFQUFFLFFBQVE7YUFDakI7WUFDRCxLQUFLLEVBQUU7Z0JBQ0gsSUFBSSxFQUFFLFNBQVMsR0FBRyxJQUFJLENBQUMsV0FBVzthQUNyQztZQUNELFFBQVEsRUFBRTtnQkFDTixJQUFJLEVBQUUsdUJBQXVCO2FBQ2hDO1lBQ0QsS0FBSyxFQUFFO2dCQUNILElBQUksRUFBRSxVQUFVO2dCQUNoQixvQkFBb0IsRUFBRTtvQkFDbEIsS0FBSyxFQUFFLFFBQVE7b0JBQ2YsSUFBSSxFQUFFLElBQUk7aUJBQ2I7Z0JBQ0QsS0FBSyxFQUFFO29CQUNILElBQUksRUFBRSxNQUFNO2lCQUNmO2FBQ0o7WUFDRCxLQUFLLEVBQUU7Z0JBQ0gsYUFBYSxFQUFFLEtBQUs7Z0JBQ3BCLEtBQUssRUFBRTtvQkFDSCxJQUFJLEVBQUUsT0FBTztpQkFDaEI7Z0JBQ0QsR0FBRyxFQUFFLENBQUM7YUFDVDtZQUNELE9BQU8sRUFBRTtnQkFDTCxZQUFZLEVBQUUsMEJBQTBCO2dCQUN4QyxXQUFXLEVBQUUsNkJBQTZCO2FBQzdDO1lBRUQsV0FBVyxFQUFFO2dCQUNULE1BQU0sRUFBRTtvQkFDSixNQUFNLEVBQUU7d0JBQ0osT0FBTyxFQUFFLElBQUk7cUJBQ2hCO2lCQUNKO2FBQ0o7WUFFRCxNQUFNLEVBQUUsQ0FBQztvQkFDTCxJQUFJLEVBQUUsU0FBUztvQkFDZixJQUFJLEVBQUUsSUFBSSxDQUFDLFdBQVc7aUJBQ3pCLEVBQUU7b0JBQ0MsSUFBSSxFQUFFLE9BQU87b0JBQ2IsSUFBSSxFQUFFLElBQUksQ0FBQyxRQUFRO2lCQUN0QjthQUNBO1NBQ0osQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVPLGtEQUFlLEdBQXZCO1FBQ0ksSUFBSSxhQUFhLEdBQVEsQ0FBQyxDQUFDLHVCQUF1QixDQUFDLENBQUM7UUFDcEQsYUFBYSxDQUFDLFVBQVUsQ0FBQztZQUNyQixLQUFLLEVBQUU7Z0JBQ0gsSUFBSSxFQUFFLFFBQVE7YUFDakI7WUFFRCxLQUFLLEVBQUU7Z0JBQ0gsSUFBSSxFQUFFLGVBQWU7YUFDeEI7WUFFRCxLQUFLLEVBQUU7Z0JBQ0gsSUFBSSxFQUFFLFVBQVU7Z0JBQ2hCLG9CQUFvQixFQUFFO29CQUNsQixLQUFLLEVBQUUsUUFBUTtvQkFDZixJQUFJLEVBQUUsSUFBSTtpQkFDYjtnQkFDRCxLQUFLLEVBQUU7b0JBQ0gsSUFBSSxFQUFFLE1BQU07aUJBQ2Y7YUFDSjtZQUVELEtBQUssRUFBRTtnQkFDSCxhQUFhLEVBQUUsSUFBSTtnQkFDbkIsR0FBRyxFQUFFLENBQUM7Z0JBQ04sS0FBSyxFQUFFO29CQUNILElBQUksRUFBRSxRQUFRO2lCQUNqQjthQUNKO1lBRUQsT0FBTyxFQUFFO2dCQUNMLFNBQVMsRUFBRTtvQkFDUCxNQUFNLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDO2dCQUNuRCxDQUFDO2FBQ0o7WUFFRCxXQUFXLEVBQUU7Z0JBQ1QsTUFBTSxFQUFFO29CQUNKLFFBQVEsRUFBRSxRQUFRO2lCQUNyQjthQUNKO1lBQ0QsTUFBTSxFQUFFLENBQUM7b0JBQ0wsSUFBSSxFQUFFLGdCQUFnQixHQUFHLElBQUksQ0FBQyxRQUFRLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxXQUFXO29CQUMvRCxJQUFJLEVBQUUsSUFBSSxDQUFDLGNBQWM7b0JBQ3pCLEtBQUssRUFBRSxjQUFjO2lCQUN4QixFQUFFO29CQUNDLElBQUksRUFBRSwwQkFBMEIsR0FBRyxJQUFJLENBQUMsV0FBVyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsUUFBUTtvQkFDekUsSUFBSSxFQUFFLElBQUksQ0FBQyxxQkFBcUI7b0JBQ2hDLEtBQUssRUFBRSx1QkFBdUI7aUJBQ2pDO2FBQ0E7U0FDSixDQUFDLENBQUM7SUFDUCxDQUFDO0lBaExMO1FBQUMsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsc0JBQXNCO1lBQ2hDLFdBQVcsRUFBRSxpQ0FBaUM7U0FDakQsQ0FBQzs7Z0NBQUE7SUE2S0YsK0JBQUM7QUFBRCxDQTNLQSxBQTJLQyxJQUFBO0FBM0tZLGdDQUF3QiwyQkEyS3BDLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL2NlbnRyZS1wb3J0YWwvY2VudHJlLWRhc2hib2FyZC9jZW50cmUtZGFzaGJvYXJkLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBDZW50cmVEYXNoYm9hcmRTZXJ2aWNlIH0gZnJvbSAnLi9jZW50cmVEYXNoYm9hcmRTZXJ2aWNlL2NlbnRyZURhc2hib2FyZC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQ2VudHJlVXNlclNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9jb3JlL2luZGV4JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHNlbGVjdG9yOiAnY2VudHJlLWRhc2hib2FyZC1jbXAnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICdjZW50cmUtZGFzaGJvYXJkLmNvbXBvbmVudC5odG1sJ1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIENlbnRyZURhc2hib2FyZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgcHJpdmF0ZSBjdXJyZW50WWVhciA9IG5ldyBEYXRlKCkuZ2V0RnVsbFllYXIoKTtcclxuICAgIHByaXZhdGUgcHJldlllYXIgPSBuZXcgRGF0ZSgpLmdldEZ1bGxZZWFyKCkgLSAxO1xyXG4gICAgcHJpdmF0ZSBuZXh0WWVhciA9IG5ldyBEYXRlKCkuZ2V0RnVsbFllYXIoKSArIDE7XHJcblxyXG4gICAgcHJpdmF0ZSBjZW50cmVOYW1lOiBzdHJpbmc7XHJcbiAgICBwcml2YXRlIHBlbmRpbmdBcHByb3ZhbHM6IG51bWJlciA9IG51bGw7XHJcbiAgICBwcml2YXRlIGF2YWlsYWJsZURheXM6IG51bWJlciA9IG51bGw7XHJcbiAgICBwcml2YXRlIHBhcmVudENvdW50OiBudW1iZXIgPSBudWxsO1xyXG4gICAgcHJpdmF0ZSB0b3RhbEluY29tZTogbnVtYmVyID0gbnVsbDtcclxuXHJcbiAgICBwcml2YXRlIGRheXNGb3JTYWxlOiBBcnJheTxudW1iZXJbXT4gPSBudWxsO1xyXG4gICAgcHJpdmF0ZSBkYXlzU29sZDogQXJyYXk8bnVtYmVyW10+ID0gbnVsbDtcclxuICAgIHByaXZhdGUgbW9udGhseVJldmVudWU6IEFycmF5PG51bWJlcltdPiA9IG51bGw7XHJcbiAgICBwcml2YXRlIGVzdGltYXRlZEZ1dHVyZUluY29tZTogQXJyYXk8bnVtYmVyW10+ID0gbnVsbDtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF91c2VyU2VydmljZTogQ2VudHJlVXNlclNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBfY2VudHJlRGFzaGJvYXJkU2VydmljZTogQ2VudHJlRGFzaGJvYXJkU2VydmljZSkge1xyXG4gICAgICAgIHRoaXMuY2VudHJlTmFtZSA9IHRoaXMuX3VzZXJTZXJ2aWNlLlNlbGVjdGVkQ2VudHJlLm5hbWU7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgbGV0IGNlbnRyZUlkOiBzdHJpbmcgPSB0aGlzLl91c2VyU2VydmljZS5TZWxlY3RlZENlbnRyZS5pZDtcclxuXHJcbiAgICAgICAgdGhpcy5fY2VudHJlRGFzaGJvYXJkU2VydmljZVxyXG4gICAgICAgICAgICAuZ2V0Q2VudHJlUGVuZGluZ0FwcHJvdmFsc0NvdW50KGNlbnRyZUlkKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKHJlcyA9PiB0aGlzLnBlbmRpbmdBcHByb3ZhbHMgPSByZXMpO1xyXG5cclxuICAgICAgICB0aGlzLl9jZW50cmVEYXNoYm9hcmRTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5nZXRDZW50cmVBdmFpbGFibGVEYXlzQ291bnQoY2VudHJlSWQpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzID0+IHRoaXMuYXZhaWxhYmxlRGF5cyA9IHJlcyk7XHJcblxyXG4gICAgICAgIHRoaXMuX2NlbnRyZURhc2hib2FyZFNlcnZpY2VcclxuICAgICAgICAgICAgLmdldENlbnRyZVBhcmVudENvdW50KGNlbnRyZUlkKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKHJlcyA9PiB0aGlzLnBhcmVudENvdW50ID0gcmVzKTtcclxuXHJcbiAgICAgICAgdGhpcy5fY2VudHJlRGFzaGJvYXJkU2VydmljZVxyXG4gICAgICAgICAgICAuZ2V0Q2VudHJlVG90YWxJbmNvbWUoY2VudHJlSWQpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzID0+IHRoaXMudG90YWxJbmNvbWUgPSByZXMpO1xyXG5cclxuXHJcbiAgICAgICAgdGhpcy5fY2VudHJlRGFzaGJvYXJkU2VydmljZVxyXG4gICAgICAgICAgICAuZ2V0Q2VudHJlRGFzaGJvYXJkRGF0YSQoY2VudHJlSWQpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGF5c0ZvclNhbGUgPSB0aGlzLm1hcEdyYXBoRGF0YShyZXMuZGF5c0ZvclNhbGUpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kYXlzU29sZCA9IHRoaXMubWFwR3JhcGhEYXRhKHJlcy5kYXlzU29sZCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm1vbnRobHlSZXZlbnVlID0gdGhpcy5tYXBHcmFwaERhdGEocmVzLm1vbnRobHlSZXZlbnVlKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZXN0aW1hdGVkRnV0dXJlSW5jb21lID0gdGhpcy5tYXBHcmFwaERhdGEocmVzLmVzdGltYXRlZEZ1dHVyZUluY29tZSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmluaXREYXlTYWxlc0NoYXJ0KCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmluaXRJbmNvbWVDaGFydCgpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIG1hcEdyYXBoRGF0YShkYXlzOiB7IFtrZXk6IHN0cmluZ106IG51bWJlcjsgfSkge1xyXG4gICAgICAgIGxldCBhcnIgPSBuZXcgQXJyYXk8bnVtYmVyW10+KCk7XHJcbiAgICAgICAgZm9yICh2YXIga2V5IGluIGRheXMpIHtcclxuICAgICAgICAgICAgaWYgKGRheXMuaGFzT3duUHJvcGVydHkoa2V5KSkge1xyXG4gICAgICAgICAgICAgICAgdmFyIGRheTogbnVtYmVyW10gPSBbbmV3IERhdGUoa2V5KS5nZXRUaW1lKCksIGRheXNba2V5XV07XHJcbiAgICAgICAgICAgICAgICBhcnIucHVzaChkYXkpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBhcnI7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBpbml0RGF5U2FsZXNDaGFydCgpOiB2b2lkIHtcclxuICAgICAgICB2YXIgbWlTd2FwczogYW55ID0gJCgnI21pU3dhcHMtdGFibGUnKTtcclxuICAgICAgICBtaVN3YXBzLmhpZ2hjaGFydHMoe1xyXG4gICAgICAgICAgICBjaGFydDoge1xyXG4gICAgICAgICAgICAgICAgdHlwZTogJ3NwbGluZSdcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgdGl0bGU6IHtcclxuICAgICAgICAgICAgICAgIHRleHQ6ICdNaUNhcmUgJyArIHRoaXMuY3VycmVudFllYXJcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgc3VidGl0bGU6IHtcclxuICAgICAgICAgICAgICAgIHRleHQ6ICdOdW1iZXIgb2YgTWlDYXJlIGRheXMnXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHhBeGlzOiB7XHJcbiAgICAgICAgICAgICAgICB0eXBlOiAnZGF0ZXRpbWUnLFxyXG4gICAgICAgICAgICAgICAgZGF0ZVRpbWVMYWJlbEZvcm1hdHM6IHsgLy8gZG9uJ3QgZGlzcGxheSB0aGUgZHVtbXkgeWVhclxyXG4gICAgICAgICAgICAgICAgICAgIG1vbnRoOiAnJWUuICViJyxcclxuICAgICAgICAgICAgICAgICAgICB5ZWFyOiAnJWInXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgdGl0bGU6IHtcclxuICAgICAgICAgICAgICAgICAgICB0ZXh0OiAnRGF0ZSdcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgeUF4aXM6IHtcclxuICAgICAgICAgICAgICAgIGFsbG93RGVjaW1hbHM6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgdGl0bGU6IHtcclxuICAgICAgICAgICAgICAgICAgICB0ZXh0OiAnVG90YWwnXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgbWluOiAwXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHRvb2x0aXA6IHtcclxuICAgICAgICAgICAgICAgIGhlYWRlckZvcm1hdDogJzxiPntzZXJpZXMubmFtZX08L2I+PGJyPicsXHJcbiAgICAgICAgICAgICAgICBwb2ludEZvcm1hdDogJ3twb2ludC54OiVlLiAlYn06IHtwb2ludC55fSdcclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIHBsb3RPcHRpb25zOiB7XHJcbiAgICAgICAgICAgICAgICBzcGxpbmU6IHtcclxuICAgICAgICAgICAgICAgICAgICBtYXJrZXI6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZW5hYmxlZDogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIHNlcmllczogW3tcclxuICAgICAgICAgICAgICAgIG5hbWU6ICdUbyBHaXZlJyxcclxuICAgICAgICAgICAgICAgIGRhdGE6IHRoaXMuZGF5c0ZvclNhbGVcclxuICAgICAgICAgICAgfSwge1xyXG4gICAgICAgICAgICAgICAgbmFtZTogJ0dpdmVuJyxcclxuICAgICAgICAgICAgICAgIGRhdGE6IHRoaXMuZGF5c1NvbGRcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBdXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBpbml0SW5jb21lQ2hhcnQoKTogdm9pZCB7XHJcbiAgICAgICAgdmFyIG1pU3dhcHNJbmNvbWU6IGFueSA9ICQoJyNtaVN3YXBzLWluY29tZS10YWJsZScpO1xyXG4gICAgICAgIG1pU3dhcHNJbmNvbWUuaGlnaGNoYXJ0cyh7XHJcbiAgICAgICAgICAgIGNoYXJ0OiB7XHJcbiAgICAgICAgICAgICAgICB0eXBlOiAnY29sdW1uJ1xyXG4gICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgdGl0bGU6IHtcclxuICAgICAgICAgICAgICAgIHRleHQ6ICdNaUNhcmUgSW5jb21lJ1xyXG4gICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgeEF4aXM6IHtcclxuICAgICAgICAgICAgICAgIHR5cGU6ICdkYXRldGltZScsXHJcbiAgICAgICAgICAgICAgICBkYXRlVGltZUxhYmVsRm9ybWF0czogeyAvLyBkb24ndCBkaXNwbGF5IHRoZSBkdW1teSB5ZWFyXHJcbiAgICAgICAgICAgICAgICAgICAgbW9udGg6ICclZS4gJWInLFxyXG4gICAgICAgICAgICAgICAgICAgIHllYXI6ICclYidcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB0aXRsZToge1xyXG4gICAgICAgICAgICAgICAgICAgIHRleHQ6ICdXZWVrJ1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgeUF4aXM6IHtcclxuICAgICAgICAgICAgICAgIGFsbG93RGVjaW1hbHM6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBtaW46IDAsXHJcbiAgICAgICAgICAgICAgICB0aXRsZToge1xyXG4gICAgICAgICAgICAgICAgICAgIHRleHQ6ICdJbmNvbWUnXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICAgICB0b29sdGlwOiB7XHJcbiAgICAgICAgICAgICAgICBmb3JtYXR0ZXI6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gJzxiPlRvdGFsPGI+OiAnICsgdGhpcy5wb2ludC5zdGFja1RvdGFsO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgcGxvdE9wdGlvbnM6IHtcclxuICAgICAgICAgICAgICAgIGNvbHVtbjoge1xyXG4gICAgICAgICAgICAgICAgICAgIHN0YWNraW5nOiAnbm9ybWFsJ1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBzZXJpZXM6IFt7XHJcbiAgICAgICAgICAgICAgICBuYW1lOiAnTWlDYXJlIEluY29tZSAnICsgdGhpcy5wcmV2WWVhciArICctJyArIHRoaXMuY3VycmVudFllYXIsXHJcbiAgICAgICAgICAgICAgICBkYXRhOiB0aGlzLm1vbnRobHlSZXZlbnVlLFxyXG4gICAgICAgICAgICAgICAgc3RhY2s6ICdhY3R1YWxJbmNvbWUnXHJcbiAgICAgICAgICAgIH0sIHtcclxuICAgICAgICAgICAgICAgIG5hbWU6ICdNaUNhcmUgUHJvamVjdGVkIEluY29tZSAnICsgdGhpcy5jdXJyZW50WWVhciArICctJyArIHRoaXMubmV4dFllYXIsXHJcbiAgICAgICAgICAgICAgICBkYXRhOiB0aGlzLmVzdGltYXRlZEZ1dHVyZUluY29tZSxcclxuICAgICAgICAgICAgICAgIHN0YWNrOiAnZXN0aW1hdGVkRnV0dXJlSW5jb21lJ1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIF1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG4iXX0=
