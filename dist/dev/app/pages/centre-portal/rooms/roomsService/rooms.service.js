"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var rxjs_1 = require('rxjs');
var index_1 = require('../../../../api/index');
var RoomsService = (function () {
    function RoomsService(_apiService) {
        this._apiService = _apiService;
        this.careTypes = null;
    }
    RoomsService.prototype.CreateUpdateRoom$ = function (roomDto) {
        return this._apiService.room_CreateUpdateRoomAsync(roomDto);
    };
    RoomsService.prototype.GetRooms$ = function (centreId) {
        return this._apiService.room_GetRooms(centreId);
    };
    RoomsService.prototype.DeleteRoom$ = function (centreId, roomId) {
        return this._apiService.room_DeleteRoomAsync(centreId, roomId);
    };
    RoomsService.prototype.getCareTypeValues$ = function () {
        var _this = this;
        if (this.careTypes !== null)
            return rxjs_1.Observable.of(this.careTypes);
        return this._apiService
            .generics_GetEnumDescriptions('CareType')
            .map(function (res) { return _this.careTypes = res; });
    };
    RoomsService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.ApiClientService])
    ], RoomsService);
    return RoomsService;
}());
exports.RoomsService = RoomsService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3Jvb21zL3Jvb21zU2VydmljZS9yb29tcy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBMkIsZUFBZSxDQUFDLENBQUE7QUFDM0MscUJBQTJCLE1BQU0sQ0FBQyxDQUFBO0FBRWxDLHNCQUErRCx1QkFBdUIsQ0FBQyxDQUFBO0FBR3ZGO0lBR0ksc0JBQW9CLFdBQTZCO1FBQTdCLGdCQUFXLEdBQVgsV0FBVyxDQUFrQjtRQUR6QyxjQUFTLEdBQThCLElBQUksQ0FBQztJQUdwRCxDQUFDO0lBRU0sd0NBQWlCLEdBQXhCLFVBQXlCLE9BQTRCO1FBQ2pELE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLDBCQUEwQixDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ2hFLENBQUM7SUFFTSxnQ0FBUyxHQUFoQixVQUFpQixRQUFnQjtRQUM3QixNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDcEQsQ0FBQztJQUVNLGtDQUFXLEdBQWxCLFVBQW1CLFFBQWdCLEVBQUUsTUFBYztRQUMvQyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDbkUsQ0FBQztJQUVNLHlDQUFrQixHQUF6QjtRQUFBLGlCQU1DO1FBTEcsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsS0FBSyxJQUFJLENBQUM7WUFDeEIsTUFBTSxDQUFDLGlCQUFVLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUN6QyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVc7YUFDbEIsNEJBQTRCLENBQUMsVUFBVSxDQUFDO2FBQ3hDLEdBQUcsQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEtBQUksQ0FBQyxTQUFTLEdBQUcsR0FBRyxFQUFwQixDQUFvQixDQUFDLENBQUM7SUFDMUMsQ0FBQztJQTFCTDtRQUFDLGlCQUFVLEVBQUU7O29CQUFBO0lBMkJiLG1CQUFDO0FBQUQsQ0ExQkEsQUEwQkMsSUFBQTtBQTFCWSxvQkFBWSxlQTBCeEIsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9yb29tcy9yb29tc1NlcnZpY2Uvcm9vbXMuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgQXBpQ2xpZW50U2VydmljZSwgQ3JlYXRlVXBkYXRlUm9vbUR0bywgUm9vbUR0byB9IGZyb20gJy4uLy4uLy4uLy4uL2FwaS9pbmRleCc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBSb29tc1NlcnZpY2Uge1xyXG5cclxuICAgIHByaXZhdGUgY2FyZVR5cGVzOiB7IFtrZXk6IHN0cmluZ106IHN0cmluZyB9ID0gbnVsbDtcclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX2FwaVNlcnZpY2U6IEFwaUNsaWVudFNlcnZpY2UpIHtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIENyZWF0ZVVwZGF0ZVJvb20kKHJvb21EdG86IENyZWF0ZVVwZGF0ZVJvb21EdG8pOiBPYnNlcnZhYmxlPFJvb21EdG8+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fYXBpU2VydmljZS5yb29tX0NyZWF0ZVVwZGF0ZVJvb21Bc3luYyhyb29tRHRvKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgR2V0Um9vbXMkKGNlbnRyZUlkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPFJvb21EdG9bXT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9hcGlTZXJ2aWNlLnJvb21fR2V0Um9vbXMoY2VudHJlSWQpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBEZWxldGVSb29tJChjZW50cmVJZDogc3RyaW5nLCByb29tSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8dm9pZD4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9hcGlTZXJ2aWNlLnJvb21fRGVsZXRlUm9vbUFzeW5jKGNlbnRyZUlkLCByb29tSWQpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRDYXJlVHlwZVZhbHVlcyQoKTogT2JzZXJ2YWJsZTx7IFtrZXk6IHN0cmluZ106IHN0cmluZyB9PiB7XHJcbiAgICAgICAgaWYgKHRoaXMuY2FyZVR5cGVzICE9PSBudWxsKVxyXG4gICAgICAgICAgICByZXR1cm4gT2JzZXJ2YWJsZS5vZih0aGlzLmNhcmVUeXBlcyk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2FwaVNlcnZpY2VcclxuICAgICAgICAgICAgLmdlbmVyaWNzX0dldEVudW1EZXNjcmlwdGlvbnMoJ0NhcmVUeXBlJylcclxuICAgICAgICAgICAgLm1hcChyZXMgPT4gdGhpcy5jYXJlVHlwZXMgPSByZXMpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
