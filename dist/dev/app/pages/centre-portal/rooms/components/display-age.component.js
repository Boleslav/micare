"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var age_converter_1 = require('./age-converter');
var DisplayAgeComponent = (function () {
    function DisplayAgeComponent() {
    }
    DisplayAgeComponent.prototype.ngOnInit = function () {
        var parseYears = parseFloat(this.value.toString());
        this.years = age_converter_1.AgeConverter.getYearFraction(parseYears);
        var parseMonths = parseFloat(this.value.toString());
        this.months = age_converter_1.AgeConverter.getMonthFraction(parseMonths);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], DisplayAgeComponent.prototype, "value", void 0);
    DisplayAgeComponent = __decorate([
        core_1.Component({
            template: "\n    <span>{{years}}</span>&#160;<small>Y</small>&#160;&#160;<span>{{months}}</span>&#160;<small>M</small>\n    "
        }), 
        __metadata('design:paramtypes', [])
    ], DisplayAgeComponent);
    return DisplayAgeComponent;
}());
exports.DisplayAgeComponent = DisplayAgeComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3Jvb21zL2NvbXBvbmVudHMvZGlzcGxheS1hZ2UuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBeUMsZUFBZSxDQUFDLENBQUE7QUFHekQsOEJBQTZCLGlCQUFpQixDQUFDLENBQUE7QUFPL0M7SUFBQTtJQWNBLENBQUM7SUFSRyxzQ0FBUSxHQUFSO1FBRUksSUFBSSxVQUFVLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztRQUNuRCxJQUFJLENBQUMsS0FBSyxHQUFHLDRCQUFZLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBRXRELElBQUksV0FBVyxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7UUFDcEQsSUFBSSxDQUFDLE1BQU0sR0FBRyw0QkFBWSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBQzdELENBQUM7SUFYRDtRQUFDLFlBQUssRUFBRTs7c0RBQUE7SUFQWjtRQUFDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsbUhBRVQ7U0FDSixDQUFDOzsyQkFBQTtJQWVGLDBCQUFDO0FBQUQsQ0FkQSxBQWNDLElBQUE7QUFkWSwyQkFBbUIsc0JBYy9CLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL2NlbnRyZS1wb3J0YWwvcm9vbXMvY29tcG9uZW50cy9kaXNwbGF5LWFnZS5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmltcG9ydCB7IFZpZXdDZWxsIH0gZnJvbSAnLi4vLi4vLi4vLi4vc2hhcmVkL25nMi1zbWFydC10YWJsZS9pbmRleCc7XHJcbmltcG9ydCB7IEFnZUNvbnZlcnRlciB9IGZyb20gJy4vYWdlLWNvbnZlcnRlcic7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHRlbXBsYXRlOiBgXHJcbiAgICA8c3Bhbj57e3llYXJzfX08L3NwYW4+JiMxNjA7PHNtYWxsPlk8L3NtYWxsPiYjMTYwOyYjMTYwOzxzcGFuPnt7bW9udGhzfX08L3NwYW4+JiMxNjA7PHNtYWxsPk08L3NtYWxsPlxyXG4gICAgYFxyXG59KVxyXG5leHBvcnQgY2xhc3MgRGlzcGxheUFnZUNvbXBvbmVudCBpbXBsZW1lbnRzIFZpZXdDZWxsLCBPbkluaXQge1xyXG5cclxuICAgIEBJbnB1dCgpIHZhbHVlOiBzdHJpbmcgfCBudW1iZXI7XHJcbiAgICBwcml2YXRlIHllYXJzOiBudW1iZXI7XHJcbiAgICBwcml2YXRlIG1vbnRoczogbnVtYmVyO1xyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG5cclxuICAgICAgICBsZXQgcGFyc2VZZWFycyA9IHBhcnNlRmxvYXQodGhpcy52YWx1ZS50b1N0cmluZygpKTtcclxuICAgICAgICB0aGlzLnllYXJzID0gQWdlQ29udmVydGVyLmdldFllYXJGcmFjdGlvbihwYXJzZVllYXJzKTtcclxuXHJcbiAgICAgICAgbGV0IHBhcnNlTW9udGhzID0gcGFyc2VGbG9hdCh0aGlzLnZhbHVlLnRvU3RyaW5nKCkpO1xyXG4gICAgICAgIHRoaXMubW9udGhzID0gQWdlQ29udmVydGVyLmdldE1vbnRoRnJhY3Rpb24ocGFyc2VNb250aHMpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
