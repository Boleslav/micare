"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../../shared/ng2-smart-table/index');
var age_converter_1 = require('./age-converter');
var SelectAgeComponent = (function (_super) {
    __extends(SelectAgeComponent, _super);
    function SelectAgeComponent() {
        _super.call(this);
        this.allowedYears = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14];
        this.allowedMonths = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
    }
    SelectAgeComponent.prototype.ngAfterViewInit = function () {
        if (this.cell.newValue !== '') {
            this.year.nativeElement.value = this.getYearValue();
            this.month.nativeElement.value = this.getMonthValue();
        }
        else {
            this.cell.newValue = 0;
        }
    };
    SelectAgeComponent.prototype.updateValue = function () {
        var month = parseFloat(this.month.nativeElement.value);
        var year = parseFloat(this.year.nativeElement.value);
        this.cell.newValue = age_converter_1.AgeConverter.getAgeFraction(year, month);
    };
    SelectAgeComponent.prototype.getYearValue = function () {
        var years = parseFloat(this.htmlValue.nativeElement.innerText);
        return age_converter_1.AgeConverter.getYearFraction(years);
    };
    SelectAgeComponent.prototype.getMonthValue = function () {
        var months = parseFloat(this.htmlValue.nativeElement.innerText);
        return age_converter_1.AgeConverter.getMonthFraction(months);
    };
    __decorate([
        core_1.ViewChild('year'), 
        __metadata('design:type', core_1.ElementRef)
    ], SelectAgeComponent.prototype, "year", void 0);
    __decorate([
        core_1.ViewChild('month'), 
        __metadata('design:type', core_1.ElementRef)
    ], SelectAgeComponent.prototype, "month", void 0);
    __decorate([
        core_1.ViewChild('htmlValue'), 
        __metadata('design:type', core_1.ElementRef)
    ], SelectAgeComponent.prototype, "htmlValue", void 0);
    SelectAgeComponent = __decorate([
        core_1.Component({
            template: "\n    <div>\n        <div style=\"display: inline-block;\" >\n            <div style=\"display: inline-block;\" >\n                <select #year id=\"select-years\" class=\"form-control\" (change)=\"updateValue()\" >\n                    <option *ngFor=\"let year of allowedYears\" [ngValue]=\"year\">{{year}}</option>\n                </select>\n            </div>\n            <div style=\"display: inline-block;\" >\n                <small>Y</small>\n            </div>\n        </div>\n        <div style=\"display: inline-block;\" >\n            <span>&#160;</span>\n        </div>\n        <div style=\"display: inline-block;\" >\n            <div style=\"display: inline-block;\" >\n                <select #month id=\"select-months\" class=\"form-control\" (change)=\"updateValue()\" >\n                    <option *ngFor=\"let month of allowedMonths\" [ngValue]=\"month\">{{ month }}</option>\n                </select>\n            </div>\n            <div style=\"display: inline-block;\" >\n                <small>M</small>\n            </div>\n        </div>\n    <div>\n    <div [hidden]=\"true\" [innerHTML]=\"cell.getValue()\" #htmlValue></div>\n    "
        }), 
        __metadata('design:paramtypes', [])
    ], SelectAgeComponent);
    return SelectAgeComponent;
}(index_1.DefaultEditor));
exports.SelectAgeComponent = SelectAgeComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3Jvb21zL2NvbXBvbmVudHMvc2VsZWN0LWFnZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7O0FBQUEscUJBQWdFLGVBQWUsQ0FBQyxDQUFBO0FBRWhGLHNCQUE4QiwwQ0FBMEMsQ0FBQyxDQUFBO0FBQ3pFLDhCQUE2QixpQkFBaUIsQ0FBQyxDQUFBO0FBZ0MvQztJQUF3QyxzQ0FBYTtJQVNqRDtRQUNJLGlCQUFPLENBQUM7UUFKSCxpQkFBWSxHQUFhLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztRQUM1RSxrQkFBYSxHQUFhLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUkxRSxDQUFDO0lBRUQsNENBQWUsR0FBZjtRQUNJLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDNUIsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztZQUNwRCxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQzFELENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQztRQUMzQixDQUFDO0lBQ0wsQ0FBQztJQUVELHdDQUFXLEdBQVg7UUFDSSxJQUFNLEtBQUssR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDekQsSUFBTSxJQUFJLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3ZELElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLDRCQUFZLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztJQUNsRSxDQUFDO0lBRUQseUNBQVksR0FBWjtRQUNJLElBQUksS0FBSyxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUMvRCxNQUFNLENBQUMsNEJBQVksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDL0MsQ0FBQztJQUVELDBDQUFhLEdBQWI7UUFDSSxJQUFJLE1BQU0sR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDaEUsTUFBTSxDQUFDLDRCQUFZLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDakQsQ0FBQztJQWxDRDtRQUFDLGdCQUFTLENBQUMsTUFBTSxDQUFDOztvREFBQTtJQUNsQjtRQUFDLGdCQUFTLENBQUMsT0FBTyxDQUFDOztxREFBQTtJQUNuQjtRQUFDLGdCQUFTLENBQUMsV0FBVyxDQUFDOzt5REFBQTtJQWxDM0I7UUFBQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLGdwQ0EyQlQ7U0FDSixDQUFDOzswQkFBQTtJQXNDRix5QkFBQztBQUFELENBckNBLEFBcUNDLENBckN1QyxxQkFBYSxHQXFDcEQ7QUFyQ1ksMEJBQWtCLHFCQXFDOUIsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9yb29tcy9jb21wb25lbnRzL3NlbGVjdC1hZ2UuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBBZnRlclZpZXdJbml0LCBWaWV3Q2hpbGQsIEVsZW1lbnRSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmltcG9ydCB7IERlZmF1bHRFZGl0b3IgfSBmcm9tICcuLi8uLi8uLi8uLi9zaGFyZWQvbmcyLXNtYXJ0LXRhYmxlL2luZGV4JztcclxuaW1wb3J0IHsgQWdlQ29udmVydGVyIH0gZnJvbSAnLi9hZ2UtY29udmVydGVyJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgdGVtcGxhdGU6IGBcclxuICAgIDxkaXY+XHJcbiAgICAgICAgPGRpdiBzdHlsZT1cImRpc3BsYXk6IGlubGluZS1ibG9jaztcIiA+XHJcbiAgICAgICAgICAgIDxkaXYgc3R5bGU9XCJkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XCIgPlxyXG4gICAgICAgICAgICAgICAgPHNlbGVjdCAjeWVhciBpZD1cInNlbGVjdC15ZWFyc1wiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgKGNoYW5nZSk9XCJ1cGRhdGVWYWx1ZSgpXCIgPlxyXG4gICAgICAgICAgICAgICAgICAgIDxvcHRpb24gKm5nRm9yPVwibGV0IHllYXIgb2YgYWxsb3dlZFllYXJzXCIgW25nVmFsdWVdPVwieWVhclwiPnt7eWVhcn19PC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICA8L3NlbGVjdD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgc3R5bGU9XCJkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XCIgPlxyXG4gICAgICAgICAgICAgICAgPHNtYWxsPlk8L3NtYWxsPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8ZGl2IHN0eWxlPVwiZGlzcGxheTogaW5saW5lLWJsb2NrO1wiID5cclxuICAgICAgICAgICAgPHNwYW4+JiMxNjA7PC9zcGFuPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDxkaXYgc3R5bGU9XCJkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XCIgPlxyXG4gICAgICAgICAgICA8ZGl2IHN0eWxlPVwiZGlzcGxheTogaW5saW5lLWJsb2NrO1wiID5cclxuICAgICAgICAgICAgICAgIDxzZWxlY3QgI21vbnRoIGlkPVwic2VsZWN0LW1vbnRoc1wiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgKGNoYW5nZSk9XCJ1cGRhdGVWYWx1ZSgpXCIgPlxyXG4gICAgICAgICAgICAgICAgICAgIDxvcHRpb24gKm5nRm9yPVwibGV0IG1vbnRoIG9mIGFsbG93ZWRNb250aHNcIiBbbmdWYWx1ZV09XCJtb250aFwiPnt7IG1vbnRoIH19PC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICA8L3NlbGVjdD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgc3R5bGU9XCJkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XCIgPlxyXG4gICAgICAgICAgICAgICAgPHNtYWxsPk08L3NtYWxsPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgIDxkaXY+XHJcbiAgICA8ZGl2IFtoaWRkZW5dPVwidHJ1ZVwiIFtpbm5lckhUTUxdPVwiY2VsbC5nZXRWYWx1ZSgpXCIgI2h0bWxWYWx1ZT48L2Rpdj5cclxuICAgIGBcclxufSlcclxuZXhwb3J0IGNsYXNzIFNlbGVjdEFnZUNvbXBvbmVudCBleHRlbmRzIERlZmF1bHRFZGl0b3IgaW1wbGVtZW50cyBBZnRlclZpZXdJbml0IHtcclxuXHJcbiAgICBAVmlld0NoaWxkKCd5ZWFyJykgeWVhcjogRWxlbWVudFJlZjtcclxuICAgIEBWaWV3Q2hpbGQoJ21vbnRoJykgbW9udGg6IEVsZW1lbnRSZWY7XHJcbiAgICBAVmlld0NoaWxkKCdodG1sVmFsdWUnKSBodG1sVmFsdWU6IEVsZW1lbnRSZWY7XHJcblxyXG4gICAgcmVhZG9ubHkgYWxsb3dlZFllYXJzOiBudW1iZXJbXSA9IFswLCAxLCAyLCAzLCA0LCA1LCA2LCA3LCA4LCA5LCAxMCwgMTEsIDEyLCAxMywgMTRdO1xyXG4gICAgcmVhZG9ubHkgYWxsb3dlZE1vbnRoczogbnVtYmVyW10gPSBbMCwgMSwgMiwgMywgNCwgNSwgNiwgNywgOCwgOSwgMTAsIDExXTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nQWZ0ZXJWaWV3SW5pdCgpOiB2b2lkIHtcclxuICAgICAgICBpZiAodGhpcy5jZWxsLm5ld1ZhbHVlICE9PSAnJykge1xyXG4gICAgICAgICAgICB0aGlzLnllYXIubmF0aXZlRWxlbWVudC52YWx1ZSA9IHRoaXMuZ2V0WWVhclZhbHVlKCk7XHJcbiAgICAgICAgICAgIHRoaXMubW9udGgubmF0aXZlRWxlbWVudC52YWx1ZSA9IHRoaXMuZ2V0TW9udGhWYWx1ZSgpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuY2VsbC5uZXdWYWx1ZSA9IDA7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZVZhbHVlKCk6IHZvaWQge1xyXG4gICAgICAgIGNvbnN0IG1vbnRoID0gcGFyc2VGbG9hdCh0aGlzLm1vbnRoLm5hdGl2ZUVsZW1lbnQudmFsdWUpO1xyXG4gICAgICAgIGNvbnN0IHllYXIgPSBwYXJzZUZsb2F0KHRoaXMueWVhci5uYXRpdmVFbGVtZW50LnZhbHVlKTtcclxuICAgICAgICB0aGlzLmNlbGwubmV3VmFsdWUgPSBBZ2VDb252ZXJ0ZXIuZ2V0QWdlRnJhY3Rpb24oeWVhciwgbW9udGgpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFllYXJWYWx1ZSgpOiBudW1iZXIge1xyXG4gICAgICAgIGxldCB5ZWFycyA9IHBhcnNlRmxvYXQodGhpcy5odG1sVmFsdWUubmF0aXZlRWxlbWVudC5pbm5lclRleHQpO1xyXG4gICAgICAgIHJldHVybiBBZ2VDb252ZXJ0ZXIuZ2V0WWVhckZyYWN0aW9uKHllYXJzKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRNb250aFZhbHVlKCk6IG51bWJlciB7XHJcbiAgICAgICAgbGV0IG1vbnRocyA9IHBhcnNlRmxvYXQodGhpcy5odG1sVmFsdWUubmF0aXZlRWxlbWVudC5pbm5lclRleHQpO1xyXG4gICAgICAgIHJldHVybiBBZ2VDb252ZXJ0ZXIuZ2V0TW9udGhGcmFjdGlvbihtb250aHMpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
