"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../index');
var DisplayCareTypeComponent = (function () {
    function DisplayCareTypeComponent(_roomsService) {
        this._roomsService = _roomsService;
        this.careTypeName = '';
    }
    DisplayCareTypeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._roomsService
            .getCareTypeValues$()
            .subscribe(function (res) {
            var name = res[_this.value];
            if (name !== undefined)
                _this.careTypeName = name;
        });
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], DisplayCareTypeComponent.prototype, "value", void 0);
    DisplayCareTypeComponent = __decorate([
        core_1.Component({
            template: "\n    <span>{{ careTypeName }}</span>\n    "
        }), 
        __metadata('design:paramtypes', [index_1.RoomsService])
    ], DisplayCareTypeComponent);
    return DisplayCareTypeComponent;
}());
exports.DisplayCareTypeComponent = DisplayCareTypeComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3Jvb21zL2NvbXBvbmVudHMvZGlzcGxheS1jYXJlLXR5cGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBeUMsZUFBZSxDQUFDLENBQUE7QUFFekQsc0JBQTZCLFVBQVUsQ0FBQyxDQUFBO0FBU3hDO0lBSUksa0NBQW9CLGFBQTJCO1FBQTNCLGtCQUFhLEdBQWIsYUFBYSxDQUFjO1FBQzNDLElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO0lBQzNCLENBQUM7SUFFRCwyQ0FBUSxHQUFSO1FBQUEsaUJBUUM7UUFQRyxJQUFJLENBQUMsYUFBYTthQUNiLGtCQUFrQixFQUFFO2FBQ3BCLFNBQVMsQ0FBQyxVQUFBLEdBQUc7WUFDVixJQUFJLElBQUksR0FBVyxHQUFHLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ25DLEVBQUUsQ0FBQyxDQUFDLElBQUksS0FBSyxTQUFTLENBQUM7Z0JBQ25CLEtBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1FBQ2pDLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQWREO1FBQUMsWUFBSyxFQUFFOzsyREFBQTtJQVBaO1FBQUMsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSw2Q0FFVDtTQUNKLENBQUM7O2dDQUFBO0lBa0JGLCtCQUFDO0FBQUQsQ0FqQkEsQUFpQkMsSUFBQTtBQWpCWSxnQ0FBd0IsMkJBaUJwQyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3Jvb21zL2NvbXBvbmVudHMvZGlzcGxheS1jYXJlLXR5cGUuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBSb29tc1NlcnZpY2UgfSBmcm9tICcuLi9pbmRleCc7XHJcblxyXG5pbXBvcnQgeyBWaWV3Q2VsbCB9IGZyb20gJy4uLy4uLy4uLy4uL3NoYXJlZC9uZzItc21hcnQtdGFibGUvaW5kZXgnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICB0ZW1wbGF0ZTogYFxyXG4gICAgPHNwYW4+e3sgY2FyZVR5cGVOYW1lIH19PC9zcGFuPlxyXG4gICAgYFxyXG59KVxyXG5leHBvcnQgY2xhc3MgRGlzcGxheUNhcmVUeXBlQ29tcG9uZW50IGltcGxlbWVudHMgVmlld0NlbGwsIE9uSW5pdCB7XHJcblxyXG4gICAgQElucHV0KCkgdmFsdWU6IHN0cmluZyB8IG51bWJlcjtcclxuICAgIHByaXZhdGUgY2FyZVR5cGVOYW1lOiBzdHJpbmc7XHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9yb29tc1NlcnZpY2U6IFJvb21zU2VydmljZSkge1xyXG4gICAgICAgIHRoaXMuY2FyZVR5cGVOYW1lID0gJyc7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5fcm9vbXNTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5nZXRDYXJlVHlwZVZhbHVlcyQoKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKHJlcyA9PiB7XHJcbiAgICAgICAgICAgICAgICBsZXQgbmFtZTogc3RyaW5nID0gcmVzW3RoaXMudmFsdWVdO1xyXG4gICAgICAgICAgICAgICAgaWYgKG5hbWUgIT09IHVuZGVmaW5lZClcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNhcmVUeXBlTmFtZSA9IG5hbWU7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
