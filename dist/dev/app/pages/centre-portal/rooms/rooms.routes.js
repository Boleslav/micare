"use strict";
var index_1 = require('../../../shared/index');
var index_2 = require('./index');
exports.RoomsRoutes = [
    {
        path: 'rooms',
        component: index_2.RoomComponent,
        canActivate: [index_1.NavigationGuardService]
    },
];

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3Jvb21zL3Jvb21zLnJvdXRlcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBRUEsc0JBQXVDLHVCQUF1QixDQUFDLENBQUE7QUFDL0Qsc0JBQThCLFNBQVMsQ0FBQyxDQUFBO0FBRTNCLG1CQUFXLEdBQVk7SUFDbkM7UUFDQyxJQUFJLEVBQUUsT0FBTztRQUNiLFNBQVMsRUFBRSxxQkFBYTtRQUN4QixXQUFXLEVBQUUsQ0FBQyw4QkFBc0IsQ0FBQztLQUNyQztDQUNELENBQUMiLCJmaWxlIjoiYXBwL3BhZ2VzL2NlbnRyZS1wb3J0YWwvcm9vbXMvcm9vbXMucm91dGVzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5cclxuaW1wb3J0IHsgTmF2aWdhdGlvbkd1YXJkU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9pbmRleCc7XHJcbmltcG9ydCB7IFJvb21Db21wb25lbnQgfSBmcm9tICcuL2luZGV4JztcclxuXHJcbmV4cG9ydCBjb25zdCBSb29tc1JvdXRlczogUm91dGVbXSA9IFtcclxuXHR7XHJcblx0XHRwYXRoOiAncm9vbXMnLFxyXG5cdFx0Y29tcG9uZW50OiBSb29tQ29tcG9uZW50LFxyXG5cdFx0Y2FuQWN0aXZhdGU6IFtOYXZpZ2F0aW9uR3VhcmRTZXJ2aWNlXVxyXG5cdH0sXHJcbl07XHJcbiJdfQ==
