"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../shared/index');
var index_2 = require('./index');
var RoomsModule = (function () {
    function RoomsModule() {
    }
    RoomsModule = __decorate([
        core_1.NgModule({
            imports: [
                index_1.SharedModule
            ],
            declarations: [
                index_2.RoomComponent,
                index_2.RoomsTableComponent,
                index_2.SelectAgeComponent,
                index_2.DisplayAgeComponent,
                index_2.DisplayCareTypeComponent
            ],
            entryComponents: [index_2.SelectAgeComponent, index_2.DisplayAgeComponent, index_2.DisplayCareTypeComponent],
            providers: [index_2.RoomsService],
            exports: [
                index_2.RoomComponent,
                index_2.RoomsTableComponent,
                index_2.SelectAgeComponent,
                index_2.DisplayAgeComponent,
                index_2.DisplayCareTypeComponent
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], RoomsModule);
    return RoomsModule;
}());
exports.RoomsModule = RoomsModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3Jvb21zL3Jvb21zLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQXlCLGVBQWUsQ0FBQyxDQUFBO0FBQ3pDLHNCQUE2Qix1QkFBdUIsQ0FBQyxDQUFBO0FBRXJELHNCQUtPLFNBQVMsQ0FBQyxDQUFBO0FBd0JqQjtJQUFBO0lBQTJCLENBQUM7SUF0QjVCO1FBQUMsZUFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFO2dCQUNMLG9CQUFZO2FBQ2Y7WUFDRCxZQUFZLEVBQUU7Z0JBQ1YscUJBQWE7Z0JBQ2IsMkJBQW1CO2dCQUNuQiwwQkFBa0I7Z0JBQ2xCLDJCQUFtQjtnQkFDbkIsZ0NBQXdCO2FBQzNCO1lBQ0QsZUFBZSxFQUFFLENBQUMsMEJBQWtCLEVBQUUsMkJBQW1CLEVBQUUsZ0NBQXdCLENBQUM7WUFDcEYsU0FBUyxFQUFFLENBQUMsb0JBQVksQ0FBQztZQUN6QixPQUFPLEVBQUU7Z0JBQ0wscUJBQWE7Z0JBQ2IsMkJBQW1CO2dCQUNuQiwwQkFBa0I7Z0JBQ2xCLDJCQUFtQjtnQkFDbkIsZ0NBQXdCO2FBQzNCO1NBQ0osQ0FBQzs7bUJBQUE7SUFFeUIsa0JBQUM7QUFBRCxDQUEzQixBQUE0QixJQUFBO0FBQWYsbUJBQVcsY0FBSSxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3Jvb21zL3Jvb21zLm1vZHVsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFNoYXJlZE1vZHVsZSB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9pbmRleCc7XHJcblxyXG5pbXBvcnQge1xyXG4gICAgUm9vbUNvbXBvbmVudCwgUm9vbXNUYWJsZUNvbXBvbmVudCxcclxuICAgIFNlbGVjdEFnZUNvbXBvbmVudCwgRGlzcGxheUFnZUNvbXBvbmVudCxcclxuICAgIERpc3BsYXlDYXJlVHlwZUNvbXBvbmVudCxcclxuICAgIFJvb21zU2VydmljZVxyXG59IGZyb20gJy4vaW5kZXgnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBTaGFyZWRNb2R1bGVcclxuICAgIF0sXHJcbiAgICBkZWNsYXJhdGlvbnM6IFtcclxuICAgICAgICBSb29tQ29tcG9uZW50LFxyXG4gICAgICAgIFJvb21zVGFibGVDb21wb25lbnQsXHJcbiAgICAgICAgU2VsZWN0QWdlQ29tcG9uZW50LFxyXG4gICAgICAgIERpc3BsYXlBZ2VDb21wb25lbnQsXHJcbiAgICAgICAgRGlzcGxheUNhcmVUeXBlQ29tcG9uZW50XHJcbiAgICBdLFxyXG4gICAgZW50cnlDb21wb25lbnRzOiBbU2VsZWN0QWdlQ29tcG9uZW50LCBEaXNwbGF5QWdlQ29tcG9uZW50LCBEaXNwbGF5Q2FyZVR5cGVDb21wb25lbnRdLFxyXG4gICAgcHJvdmlkZXJzOiBbUm9vbXNTZXJ2aWNlXSxcclxuICAgIGV4cG9ydHM6IFtcclxuICAgICAgICBSb29tQ29tcG9uZW50LFxyXG4gICAgICAgIFJvb21zVGFibGVDb21wb25lbnQsXHJcbiAgICAgICAgU2VsZWN0QWdlQ29tcG9uZW50LFxyXG4gICAgICAgIERpc3BsYXlBZ2VDb21wb25lbnQsXHJcbiAgICAgICAgRGlzcGxheUNhcmVUeXBlQ29tcG9uZW50XHJcbiAgICBdXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgUm9vbXNNb2R1bGUgeyB9XHJcbiJdfQ==
