"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../index');
var index_2 = require('../../../../core/index');
var local_data_source_1 = require('../../../../shared/ng2-smart-table/ng2-smart-table/lib/data-source/local/local.data-source');
var angular2_toaster_1 = require('angular2-toaster');
var index_3 = require('../../../../shared/index');
var index_4 = require('../../../../api/index');
var RoomsTableComponent = (function () {
    function RoomsTableComponent(_roomsService, _userService, _toasterService) {
        this._roomsService = _roomsService;
        this._userService = _userService;
        this._toasterService = _toasterService;
        this.ageRegex = /^(\d+\.?\d{0,})$/;
        this.wrongAgeFormatMessage = 'Must be a number or at most one decimal place (e.g. 1.5)';
        this.source = new local_data_source_1.LocalDataSource();
        this.careTypes = new Array();
    }
    RoomsTableComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._roomsService
            .getCareTypeValues$()
            .map(function (res) {
            for (var key in res) {
                if (res.hasOwnProperty(key)) {
                    _this.careTypes.push({
                        title: res[key],
                        value: key
                    });
                }
            }
        })
            .flatMap(function () { return _this._roomsService.GetRooms$(_this._userService.SelectedCentre.id); })
            .subscribe(function (roomsRes) {
            _this.initSettings();
            _this.rooms = roomsRes;
            _this.source.load(_this.rooms);
        });
    };
    RoomsTableComponent.prototype.addRoom = function (event) {
        this.createUpdateRoom(event);
    };
    RoomsTableComponent.prototype.editRoom = function (event) {
        this.createUpdateRoom(event);
    };
    RoomsTableComponent.prototype.createUpdateRoom = function (event) {
        var _this = this;
        if (event.newData.name === ''
            || event.newData.description === ''
            || event.newData.minAge.toString() === ''
            || event.newData.maxAge.toString() === '') {
            this._toasterService
                .popAsync('error', 'Missing Information', 'All fields must be provided');
            event.confirm.reject();
        }
        else if (event.newData.minAge.toString().match(this.ageRegex) === null) {
            this._toasterService.popAsync('error', 'Invalid Age', this.wrongAgeFormatMessage);
        }
        else if (event.newData.maxAge.toString().match(this.ageRegex) === null) {
            this._toasterService.popAsync('error', 'Invalid Age', this.wrongAgeFormatMessage);
        }
        else if (!this.isValidAgeRange(event.newData.minAge.toString(), event.newData.maxAge.toString())) {
            this._toasterService.popAsync('error', 'Invalid Age Range');
        }
        else {
            var savingToast_1 = this._toasterService.pop(new index_3.SpinningToast());
            var createRoomDto = new index_4.CreateUpdateRoomDto();
            createRoomDto.init({
                Id: event.newData.id,
                CentreId: this._userService.SelectedCentre.id,
                Name: event.newData.name,
                Description: event.newData.description,
                MinAge: event.newData.minAge,
                MaxAge: event.newData.maxAge,
                CareType: event.newData.careType
            });
            this._roomsService.CreateUpdateRoom$(createRoomDto)
                .toPromise()
                .then(function (res) {
                _this._toasterService.clear(savingToast_1.toastId, savingToast_1.toastContainerId);
                _this._toasterService.popAsync('success', 'Saved', '');
                return event.confirm.resolve(res);
            })
                .catch(function (error) {
                _this._toasterService.clear(savingToast_1.toastId, savingToast_1.toastContainerId);
                return event.confirm.reject();
            });
        }
    };
    RoomsTableComponent.prototype.removeRoom = function (event) {
        var _this = this;
        if (window.confirm('Are you sure you want to delete?')) {
            var savingToast_2 = this._toasterService.pop(new index_3.SpinningToast());
            var roomId = event.data.id;
            this._roomsService
                .DeleteRoom$(this._userService.SelectedCentre.id, roomId)
                .toPromise()
                .then(function (res) {
                _this._toasterService.clear(savingToast_2.toastId, savingToast_2.toastContainerId);
                _this._toasterService.popAsync('success', 'Deleted', '');
                return event.confirm.resolve();
            })
                .catch(function (error) {
                _this._toasterService.clear(savingToast_2.toastId, savingToast_2.toastContainerId);
                return event.confirm.reject();
            });
        }
        else {
            event.confirm.reject();
        }
    };
    RoomsTableComponent.prototype.isValidAgeRange = function (minAgeStr, maxAgeStr) {
        var minAge = parseFloat(minAgeStr);
        var maxAge = parseFloat(maxAgeStr);
        if (0 > minAge || 0 > maxAge)
            return false;
        if (minAge > maxAge)
            return false;
        return true;
    };
    RoomsTableComponent.prototype.initSettings = function () {
        this.settings = {
            mode: 'internal',
            hideHeader: false,
            hideSubHeader: false,
            sort: false,
            actions: {
                columnTitle: '',
                add: true,
                edit: true,
                delete: true,
                position: 'right'
            },
            add: {
                addButtonContent: 'Add Room',
                createButtonContent: 'Create',
                cancelButtonContent: 'Cancel',
                confirmCreate: true
            },
            edit: {
                editButtonContent: 'Edit Room',
                saveButtonContent: 'Save',
                cancelButtonContent: 'Cancel',
                confirmSave: true
            },
            delete: {
                deleteButtonContent: 'Remove Room',
                confirmDelete: true
            },
            columns: {
                name: {
                    title: 'Room Name',
                    filter: false
                },
                description: {
                    title: 'Description',
                    filter: false
                },
                minAge: {
                    title: 'Minimum Age',
                    filter: false,
                    type: 'custom',
                    renderComponent: index_1.DisplayAgeComponent,
                    editor: {
                        type: 'custom',
                        component: index_1.SelectAgeComponent
                    }
                },
                maxAge: {
                    title: 'Maximum Age',
                    filter: false,
                    type: 'custom',
                    renderComponent: index_1.DisplayAgeComponent,
                    editor: {
                        type: 'custom',
                        component: index_1.SelectAgeComponent
                    }
                },
                careType: {
                    title: 'Care Type',
                    filter: false,
                    type: 'custom',
                    defaultValue: '0',
                    renderComponent: index_1.DisplayCareTypeComponent,
                    editor: {
                        type: 'list',
                        config: {
                            list: this.careTypes
                        }
                    },
                }
            },
            noDataMessage: 'No Rooms'
        };
    };
    RoomsTableComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'rooms-table-cmp',
            templateUrl: './roomsTable.component.html'
        }), 
        __metadata('design:paramtypes', [index_1.RoomsService, index_2.CentreUserService, angular2_toaster_1.ToasterService])
    ], RoomsTableComponent);
    return RoomsTableComponent;
}());
exports.RoomsTableComponent = RoomsTableComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3Jvb21zL3Jvb21zVGFibGUvcm9vbXNUYWJsZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUFrQyxlQUFlLENBQUMsQ0FBQTtBQUVsRCxzQkFBZ0csVUFBVSxDQUFDLENBQUE7QUFDM0csc0JBQWtDLHdCQUF3QixDQUFDLENBQUE7QUFDM0Qsa0NBQWdDLDRGQUE0RixDQUFDLENBQUE7QUFDN0gsaUNBQStCLGtCQUFrQixDQUFDLENBQUE7QUFDbEQsc0JBQThCLDBCQUEwQixDQUFDLENBQUE7QUFDekQsc0JBQTZDLHVCQUF1QixDQUFDLENBQUE7QUFPckU7SUFTSSw2QkFBb0IsYUFBMkIsRUFDbkMsWUFBK0IsRUFDL0IsZUFBK0I7UUFGdkIsa0JBQWEsR0FBYixhQUFhLENBQWM7UUFDbkMsaUJBQVksR0FBWixZQUFZLENBQW1CO1FBQy9CLG9CQUFlLEdBQWYsZUFBZSxDQUFnQjtRQUxuQyxhQUFRLEdBQVcsa0JBQWtCLENBQUM7UUFDdEMsMEJBQXFCLEdBQVcsMERBQTBELENBQUM7UUFDM0YsV0FBTSxHQUFvQixJQUFJLG1DQUFlLEVBQUUsQ0FBQztRQUlwRCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksS0FBSyxFQUE2QixDQUFDO0lBQzVELENBQUM7SUFFRCxzQ0FBUSxHQUFSO1FBQUEsaUJBbUJDO1FBbEJHLElBQUksQ0FBQyxhQUFhO2FBQ2Isa0JBQWtCLEVBQUU7YUFDcEIsR0FBRyxDQUFDLFVBQUEsR0FBRztZQUNKLEdBQUcsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xCLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUMxQixLQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQzt3QkFDaEIsS0FBSyxFQUFFLEdBQUcsQ0FBQyxHQUFHLENBQUM7d0JBQ2YsS0FBSyxFQUFFLEdBQUc7cUJBQ2IsQ0FBQyxDQUFDO2dCQUNQLENBQUM7WUFDTCxDQUFDO1FBQ0wsQ0FBQyxDQUFDO2FBQ0QsT0FBTyxDQUFDLGNBQU0sT0FBQSxLQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxFQUFFLENBQUMsRUFBakUsQ0FBaUUsQ0FBQzthQUNoRixTQUFTLENBQUMsVUFBQSxRQUFRO1lBQ2YsS0FBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1lBQ3BCLEtBQUksQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDO1lBQ3RCLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNqQyxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFRCxxQ0FBTyxHQUFQLFVBQVEsS0FBVTtRQUNkLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNqQyxDQUFDO0lBRUQsc0NBQVEsR0FBUixVQUFTLEtBQVU7UUFDZixJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDakMsQ0FBQztJQUVELDhDQUFnQixHQUFoQixVQUFpQixLQUFVO1FBQTNCLGlCQXlDQztRQXZDRyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksS0FBSyxFQUFFO2VBQ3RCLEtBQUssQ0FBQyxPQUFPLENBQUMsV0FBVyxLQUFLLEVBQUU7ZUFDaEMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLEtBQUssRUFBRTtlQUN0QyxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBRTVDLElBQUksQ0FBQyxlQUFlO2lCQUNmLFFBQVEsQ0FBQyxPQUFPLEVBQUUscUJBQXFCLEVBQUUsNkJBQTZCLENBQUMsQ0FBQztZQUM3RSxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBRTNCLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ3ZFLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxhQUFhLEVBQUUsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUM7UUFDdEYsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDdkUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLGFBQWEsRUFBRSxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUN0RixDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNqRyxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsbUJBQW1CLENBQUMsQ0FBQztRQUNoRSxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixJQUFJLGFBQVcsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxJQUFJLHFCQUFhLEVBQUUsQ0FBQyxDQUFDO1lBQ2hFLElBQUksYUFBYSxHQUFHLElBQUksMkJBQW1CLEVBQUUsQ0FBQztZQUM5QyxhQUFhLENBQUMsSUFBSSxDQUFDO2dCQUNmLEVBQUUsRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ3BCLFFBQVEsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxFQUFFO2dCQUM3QyxJQUFJLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUN4QixXQUFXLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxXQUFXO2dCQUN0QyxNQUFNLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNO2dCQUM1QixNQUFNLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNO2dCQUM1QixRQUFRLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRO2FBQ25DLENBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsYUFBYSxDQUFDO2lCQUM5QyxTQUFTLEVBQUU7aUJBQ1gsSUFBSSxDQUFDLFVBQUMsR0FBRztnQkFDTixLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxhQUFXLENBQUMsT0FBTyxFQUFFLGFBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2dCQUM5RSxLQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsT0FBTyxFQUFFLEVBQUUsQ0FBQyxDQUFDO2dCQUN0RCxNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDdEMsQ0FBQyxDQUFDO2lCQUNELEtBQUssQ0FBQyxVQUFDLEtBQUs7Z0JBQ1QsS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsYUFBVyxDQUFDLE9BQU8sRUFBRSxhQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztnQkFDOUUsTUFBTSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUM7WUFDbEMsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDO0lBQ0wsQ0FBQztJQUVELHdDQUFVLEdBQVYsVUFBVyxLQUFVO1FBQXJCLGlCQW9CQztRQW5CRyxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLGtDQUFrQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRXJELElBQUksYUFBVyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUkscUJBQWEsRUFBRSxDQUFDLENBQUM7WUFDaEUsSUFBSSxNQUFNLEdBQVcsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUM7WUFDbkMsSUFBSSxDQUFDLGFBQWE7aUJBQ2IsV0FBVyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLEVBQUUsRUFBRSxNQUFNLENBQUM7aUJBQ3hELFNBQVMsRUFBRTtpQkFDWCxJQUFJLENBQUMsVUFBQyxHQUFHO2dCQUNOLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLGFBQVcsQ0FBQyxPQUFPLEVBQUUsYUFBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0JBQzlFLEtBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLFNBQVMsRUFBRSxTQUFTLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQ3hELE1BQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ25DLENBQUMsQ0FBQztpQkFDRCxLQUFLLENBQUMsVUFBQyxLQUFLO2dCQUNULEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLGFBQVcsQ0FBQyxPQUFPLEVBQUUsYUFBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0JBQzlFLE1BQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQ2xDLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUMzQixDQUFDO0lBQ0wsQ0FBQztJQUVPLDZDQUFlLEdBQXZCLFVBQXdCLFNBQWlCLEVBQUUsU0FBaUI7UUFDeEQsSUFBSSxNQUFNLEdBQUcsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ25DLElBQUksTUFBTSxHQUFHLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUVuQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsTUFBTSxJQUFJLENBQUMsR0FBRyxNQUFNLENBQUM7WUFDekIsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUVqQixFQUFFLENBQUMsQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1lBQ2hCLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFFakIsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRU8sMENBQVksR0FBcEI7UUFDSSxJQUFJLENBQUMsUUFBUSxHQUFHO1lBQ1osSUFBSSxFQUFFLFVBQVU7WUFDaEIsVUFBVSxFQUFFLEtBQUs7WUFDakIsYUFBYSxFQUFFLEtBQUs7WUFDcEIsSUFBSSxFQUFFLEtBQUs7WUFDWCxPQUFPLEVBQUU7Z0JBQ0wsV0FBVyxFQUFFLEVBQUU7Z0JBQ2YsR0FBRyxFQUFFLElBQUk7Z0JBQ1QsSUFBSSxFQUFFLElBQUk7Z0JBQ1YsTUFBTSxFQUFFLElBQUk7Z0JBQ1osUUFBUSxFQUFFLE9BQU87YUFDcEI7WUFDRCxHQUFHLEVBQUU7Z0JBQ0QsZ0JBQWdCLEVBQUUsVUFBVTtnQkFDNUIsbUJBQW1CLEVBQUUsUUFBUTtnQkFDN0IsbUJBQW1CLEVBQUUsUUFBUTtnQkFDN0IsYUFBYSxFQUFFLElBQUk7YUFDdEI7WUFDRCxJQUFJLEVBQUU7Z0JBQ0YsaUJBQWlCLEVBQUUsV0FBVztnQkFDOUIsaUJBQWlCLEVBQUUsTUFBTTtnQkFDekIsbUJBQW1CLEVBQUUsUUFBUTtnQkFDN0IsV0FBVyxFQUFFLElBQUk7YUFDcEI7WUFDRCxNQUFNLEVBQUU7Z0JBQ0osbUJBQW1CLEVBQUUsYUFBYTtnQkFDbEMsYUFBYSxFQUFFLElBQUk7YUFDdEI7WUFDRCxPQUFPLEVBQUU7Z0JBQ0wsSUFBSSxFQUFFO29CQUNGLEtBQUssRUFBRSxXQUFXO29CQUNsQixNQUFNLEVBQUUsS0FBSztpQkFDaEI7Z0JBQ0QsV0FBVyxFQUFFO29CQUNULEtBQUssRUFBRSxhQUFhO29CQUNwQixNQUFNLEVBQUUsS0FBSztpQkFDaEI7Z0JBQ0QsTUFBTSxFQUFFO29CQUNKLEtBQUssRUFBRSxhQUFhO29CQUNwQixNQUFNLEVBQUUsS0FBSztvQkFDYixJQUFJLEVBQUUsUUFBUTtvQkFDZCxlQUFlLEVBQUUsMkJBQW1CO29CQUNwQyxNQUFNLEVBQUU7d0JBQ0osSUFBSSxFQUFFLFFBQVE7d0JBQ2QsU0FBUyxFQUFFLDBCQUFrQjtxQkFDaEM7aUJBQ0o7Z0JBQ0QsTUFBTSxFQUFFO29CQUNKLEtBQUssRUFBRSxhQUFhO29CQUNwQixNQUFNLEVBQUUsS0FBSztvQkFDYixJQUFJLEVBQUUsUUFBUTtvQkFDZCxlQUFlLEVBQUUsMkJBQW1CO29CQUNwQyxNQUFNLEVBQUU7d0JBQ0osSUFBSSxFQUFFLFFBQVE7d0JBQ2QsU0FBUyxFQUFFLDBCQUFrQjtxQkFDaEM7aUJBQ0o7Z0JBQ0QsUUFBUSxFQUFFO29CQUNOLEtBQUssRUFBRSxXQUFXO29CQUNsQixNQUFNLEVBQUUsS0FBSztvQkFDYixJQUFJLEVBQUUsUUFBUTtvQkFDZCxZQUFZLEVBQUUsR0FBRztvQkFDakIsZUFBZSxFQUFFLGdDQUF3QjtvQkFDekMsTUFBTSxFQUFFO3dCQUNKLElBQUksRUFBRSxNQUFNO3dCQUNaLE1BQU0sRUFBRTs0QkFDSixJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVM7eUJBQ3ZCO3FCQUNKO2lCQUNKO2FBQ0o7WUFDRCxhQUFhLEVBQUUsVUFBVTtTQUM1QixDQUFDO0lBQ04sQ0FBQztJQXpNTDtRQUFDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLGlCQUFpQjtZQUMzQixXQUFXLEVBQUUsNkJBQTZCO1NBQzdDLENBQUM7OzJCQUFBO0lBc01GLDBCQUFDO0FBQUQsQ0FyTUEsQUFxTUMsSUFBQTtBQXJNWSwyQkFBbUIsc0JBcU0vQixDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3Jvb21zL3Jvb21zVGFibGUvcm9vbXNUYWJsZS5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgRGlzcGxheUFnZUNvbXBvbmVudCwgU2VsZWN0QWdlQ29tcG9uZW50LCBEaXNwbGF5Q2FyZVR5cGVDb21wb25lbnQsIFJvb21zU2VydmljZSB9IGZyb20gJy4uL2luZGV4JztcclxuaW1wb3J0IHsgQ2VudHJlVXNlclNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi9jb3JlL2luZGV4JztcclxuaW1wb3J0IHsgTG9jYWxEYXRhU291cmNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vc2hhcmVkL25nMi1zbWFydC10YWJsZS9uZzItc21hcnQtdGFibGUvbGliL2RhdGEtc291cmNlL2xvY2FsL2xvY2FsLmRhdGEtc291cmNlJztcclxuaW1wb3J0IHsgVG9hc3RlclNlcnZpY2UgfSBmcm9tICdhbmd1bGFyMi10b2FzdGVyJztcclxuaW1wb3J0IHsgU3Bpbm5pbmdUb2FzdCB9IGZyb20gJy4uLy4uLy4uLy4uL3NoYXJlZC9pbmRleCc7XHJcbmltcG9ydCB7IENyZWF0ZVVwZGF0ZVJvb21EdG8sIFJvb21EdG8gfSBmcm9tICcuLi8uLi8uLi8uLi9hcGkvaW5kZXgnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgc2VsZWN0b3I6ICdyb29tcy10YWJsZS1jbXAnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL3Jvb21zVGFibGUuY29tcG9uZW50Lmh0bWwnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBSb29tc1RhYmxlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBwcml2YXRlIGNhcmVUeXBlczogeyBba2V5OiBzdHJpbmddOiBzdHJpbmcgfVtdO1xyXG4gICAgcHJpdmF0ZSBzZXR0aW5nczogYW55O1xyXG5cclxuICAgIHByaXZhdGUgcm9vbXM6IFJvb21EdG9bXTtcclxuICAgIHByaXZhdGUgYWdlUmVnZXg6IFJlZ0V4cCA9IC9eKFxcZCtcXC4/XFxkezAsfSkkLztcclxuICAgIHByaXZhdGUgd3JvbmdBZ2VGb3JtYXRNZXNzYWdlOiBzdHJpbmcgPSAnTXVzdCBiZSBhIG51bWJlciBvciBhdCBtb3N0IG9uZSBkZWNpbWFsIHBsYWNlIChlLmcuIDEuNSknO1xyXG4gICAgcHJpdmF0ZSBzb3VyY2U6IExvY2FsRGF0YVNvdXJjZSA9IG5ldyBMb2NhbERhdGFTb3VyY2UoKTtcclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX3Jvb21zU2VydmljZTogUm9vbXNTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgX3VzZXJTZXJ2aWNlOiBDZW50cmVVc2VyU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIF90b2FzdGVyU2VydmljZTogVG9hc3RlclNlcnZpY2UpIHtcclxuICAgICAgICB0aGlzLmNhcmVUeXBlcyA9IG5ldyBBcnJheTx7IFtrZXk6IHN0cmluZ106IHN0cmluZyB9PigpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuX3Jvb21zU2VydmljZVxyXG4gICAgICAgICAgICAuZ2V0Q2FyZVR5cGVWYWx1ZXMkKClcclxuICAgICAgICAgICAgLm1hcChyZXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgZm9yIChsZXQga2V5IGluIHJlcykge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXMuaGFzT3duUHJvcGVydHkoa2V5KSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNhcmVUeXBlcy5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiByZXNba2V5XSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBrZXlcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAuZmxhdE1hcCgoKSA9PiB0aGlzLl9yb29tc1NlcnZpY2UuR2V0Um9vbXMkKHRoaXMuX3VzZXJTZXJ2aWNlLlNlbGVjdGVkQ2VudHJlLmlkKSlcclxuICAgICAgICAgICAgLnN1YnNjcmliZShyb29tc1JlcyA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmluaXRTZXR0aW5ncygpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yb29tcyA9IHJvb21zUmVzO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zb3VyY2UubG9hZCh0aGlzLnJvb21zKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgYWRkUm9vbShldmVudDogYW55KTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5jcmVhdGVVcGRhdGVSb29tKGV2ZW50KTtcclxuICAgIH1cclxuXHJcbiAgICBlZGl0Um9vbShldmVudDogYW55KTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5jcmVhdGVVcGRhdGVSb29tKGV2ZW50KTtcclxuICAgIH1cclxuXHJcbiAgICBjcmVhdGVVcGRhdGVSb29tKGV2ZW50OiBhbnkpIHtcclxuXHJcbiAgICAgICAgaWYgKGV2ZW50Lm5ld0RhdGEubmFtZSA9PT0gJydcclxuICAgICAgICAgICAgfHwgZXZlbnQubmV3RGF0YS5kZXNjcmlwdGlvbiA9PT0gJydcclxuICAgICAgICAgICAgfHwgZXZlbnQubmV3RGF0YS5taW5BZ2UudG9TdHJpbmcoKSA9PT0gJydcclxuICAgICAgICAgICAgfHwgZXZlbnQubmV3RGF0YS5tYXhBZ2UudG9TdHJpbmcoKSA9PT0gJycpIHtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlXHJcbiAgICAgICAgICAgICAgICAucG9wQXN5bmMoJ2Vycm9yJywgJ01pc3NpbmcgSW5mb3JtYXRpb24nLCAnQWxsIGZpZWxkcyBtdXN0IGJlIHByb3ZpZGVkJyk7XHJcbiAgICAgICAgICAgIGV2ZW50LmNvbmZpcm0ucmVqZWN0KCk7XHJcblxyXG4gICAgICAgIH0gZWxzZSBpZiAoZXZlbnQubmV3RGF0YS5taW5BZ2UudG9TdHJpbmcoKS5tYXRjaCh0aGlzLmFnZVJlZ2V4KSA9PT0gbnVsbCkge1xyXG4gICAgICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5wb3BBc3luYygnZXJyb3InLCAnSW52YWxpZCBBZ2UnLCB0aGlzLndyb25nQWdlRm9ybWF0TWVzc2FnZSk7XHJcbiAgICAgICAgfSBlbHNlIGlmIChldmVudC5uZXdEYXRhLm1heEFnZS50b1N0cmluZygpLm1hdGNoKHRoaXMuYWdlUmVnZXgpID09PSBudWxsKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLnBvcEFzeW5jKCdlcnJvcicsICdJbnZhbGlkIEFnZScsIHRoaXMud3JvbmdBZ2VGb3JtYXRNZXNzYWdlKTtcclxuICAgICAgICB9IGVsc2UgaWYgKCF0aGlzLmlzVmFsaWRBZ2VSYW5nZShldmVudC5uZXdEYXRhLm1pbkFnZS50b1N0cmluZygpLCBldmVudC5uZXdEYXRhLm1heEFnZS50b1N0cmluZygpKSkge1xyXG4gICAgICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5wb3BBc3luYygnZXJyb3InLCAnSW52YWxpZCBBZ2UgUmFuZ2UnKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBsZXQgc2F2aW5nVG9hc3QgPSB0aGlzLl90b2FzdGVyU2VydmljZS5wb3AobmV3IFNwaW5uaW5nVG9hc3QoKSk7XHJcbiAgICAgICAgICAgIGxldCBjcmVhdGVSb29tRHRvID0gbmV3IENyZWF0ZVVwZGF0ZVJvb21EdG8oKTtcclxuICAgICAgICAgICAgY3JlYXRlUm9vbUR0by5pbml0KHtcclxuICAgICAgICAgICAgICAgIElkOiBldmVudC5uZXdEYXRhLmlkLFxyXG4gICAgICAgICAgICAgICAgQ2VudHJlSWQ6IHRoaXMuX3VzZXJTZXJ2aWNlLlNlbGVjdGVkQ2VudHJlLmlkLFxyXG4gICAgICAgICAgICAgICAgTmFtZTogZXZlbnQubmV3RGF0YS5uYW1lLFxyXG4gICAgICAgICAgICAgICAgRGVzY3JpcHRpb246IGV2ZW50Lm5ld0RhdGEuZGVzY3JpcHRpb24sXHJcbiAgICAgICAgICAgICAgICBNaW5BZ2U6IGV2ZW50Lm5ld0RhdGEubWluQWdlLFxyXG4gICAgICAgICAgICAgICAgTWF4QWdlOiBldmVudC5uZXdEYXRhLm1heEFnZSxcclxuICAgICAgICAgICAgICAgIENhcmVUeXBlOiBldmVudC5uZXdEYXRhLmNhcmVUeXBlXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB0aGlzLl9yb29tc1NlcnZpY2UuQ3JlYXRlVXBkYXRlUm9vbSQoY3JlYXRlUm9vbUR0bylcclxuICAgICAgICAgICAgICAgIC50b1Byb21pc2UoKVxyXG4gICAgICAgICAgICAgICAgLnRoZW4oKHJlcykgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLmNsZWFyKHNhdmluZ1RvYXN0LnRvYXN0SWQsIHNhdmluZ1RvYXN0LnRvYXN0Q29udGFpbmVySWQpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLnBvcEFzeW5jKCdzdWNjZXNzJywgJ1NhdmVkJywgJycpO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBldmVudC5jb25maXJtLnJlc29sdmUocmVzKTtcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoc2F2aW5nVG9hc3QudG9hc3RJZCwgc2F2aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGV2ZW50LmNvbmZpcm0ucmVqZWN0KCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcmVtb3ZlUm9vbShldmVudDogYW55KTogdm9pZCB7XHJcbiAgICAgICAgaWYgKHdpbmRvdy5jb25maXJtKCdBcmUgeW91IHN1cmUgeW91IHdhbnQgdG8gZGVsZXRlPycpKSB7XHJcblxyXG4gICAgICAgICAgICBsZXQgc2F2aW5nVG9hc3QgPSB0aGlzLl90b2FzdGVyU2VydmljZS5wb3AobmV3IFNwaW5uaW5nVG9hc3QoKSk7XHJcbiAgICAgICAgICAgIGxldCByb29tSWQ6IHN0cmluZyA9IGV2ZW50LmRhdGEuaWQ7XHJcbiAgICAgICAgICAgIHRoaXMuX3Jvb21zU2VydmljZVxyXG4gICAgICAgICAgICAgICAgLkRlbGV0ZVJvb20kKHRoaXMuX3VzZXJTZXJ2aWNlLlNlbGVjdGVkQ2VudHJlLmlkLCByb29tSWQpXHJcbiAgICAgICAgICAgICAgICAudG9Qcm9taXNlKClcclxuICAgICAgICAgICAgICAgIC50aGVuKChyZXMpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcihzYXZpbmdUb2FzdC50b2FzdElkLCBzYXZpbmdUb2FzdC50b2FzdENvbnRhaW5lcklkKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5wb3BBc3luYygnc3VjY2VzcycsICdEZWxldGVkJywgJycpO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBldmVudC5jb25maXJtLnJlc29sdmUoKTtcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoc2F2aW5nVG9hc3QudG9hc3RJZCwgc2F2aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGV2ZW50LmNvbmZpcm0ucmVqZWN0KCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBldmVudC5jb25maXJtLnJlamVjdCgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGlzVmFsaWRBZ2VSYW5nZShtaW5BZ2VTdHI6IHN0cmluZywgbWF4QWdlU3RyOiBzdHJpbmcpOiBib29sZWFuIHtcclxuICAgICAgICBsZXQgbWluQWdlID0gcGFyc2VGbG9hdChtaW5BZ2VTdHIpO1xyXG4gICAgICAgIGxldCBtYXhBZ2UgPSBwYXJzZUZsb2F0KG1heEFnZVN0cik7XHJcblxyXG4gICAgICAgIGlmICgwID4gbWluQWdlIHx8IDAgPiBtYXhBZ2UpXHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuXHJcbiAgICAgICAgaWYgKG1pbkFnZSA+IG1heEFnZSlcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG5cclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGluaXRTZXR0aW5ncygpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLnNldHRpbmdzID0ge1xyXG4gICAgICAgICAgICBtb2RlOiAnaW50ZXJuYWwnLFxyXG4gICAgICAgICAgICBoaWRlSGVhZGVyOiBmYWxzZSxcclxuICAgICAgICAgICAgaGlkZVN1YkhlYWRlcjogZmFsc2UsXHJcbiAgICAgICAgICAgIHNvcnQ6IGZhbHNlLFxyXG4gICAgICAgICAgICBhY3Rpb25zOiB7XHJcbiAgICAgICAgICAgICAgICBjb2x1bW5UaXRsZTogJycsXHJcbiAgICAgICAgICAgICAgICBhZGQ6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBlZGl0OiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgZGVsZXRlOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgcG9zaXRpb246ICdyaWdodCdcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgYWRkOiB7XHJcbiAgICAgICAgICAgICAgICBhZGRCdXR0b25Db250ZW50OiAnQWRkIFJvb20nLFxyXG4gICAgICAgICAgICAgICAgY3JlYXRlQnV0dG9uQ29udGVudDogJ0NyZWF0ZScsXHJcbiAgICAgICAgICAgICAgICBjYW5jZWxCdXR0b25Db250ZW50OiAnQ2FuY2VsJyxcclxuICAgICAgICAgICAgICAgIGNvbmZpcm1DcmVhdGU6IHRydWVcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgZWRpdDoge1xyXG4gICAgICAgICAgICAgICAgZWRpdEJ1dHRvbkNvbnRlbnQ6ICdFZGl0IFJvb20nLFxyXG4gICAgICAgICAgICAgICAgc2F2ZUJ1dHRvbkNvbnRlbnQ6ICdTYXZlJyxcclxuICAgICAgICAgICAgICAgIGNhbmNlbEJ1dHRvbkNvbnRlbnQ6ICdDYW5jZWwnLFxyXG4gICAgICAgICAgICAgICAgY29uZmlybVNhdmU6IHRydWVcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgZGVsZXRlOiB7XHJcbiAgICAgICAgICAgICAgICBkZWxldGVCdXR0b25Db250ZW50OiAnUmVtb3ZlIFJvb20nLFxyXG4gICAgICAgICAgICAgICAgY29uZmlybURlbGV0ZTogdHJ1ZVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBjb2x1bW5zOiB7XHJcbiAgICAgICAgICAgICAgICBuYW1lOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6ICdSb29tIE5hbWUnLFxyXG4gICAgICAgICAgICAgICAgICAgIGZpbHRlcjogZmFsc2VcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjoge1xyXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiAnRGVzY3JpcHRpb24nLFxyXG4gICAgICAgICAgICAgICAgICAgIGZpbHRlcjogZmFsc2VcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBtaW5BZ2U6IHtcclxuICAgICAgICAgICAgICAgICAgICB0aXRsZTogJ01pbmltdW0gQWdlJyxcclxuICAgICAgICAgICAgICAgICAgICBmaWx0ZXI6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6ICdjdXN0b20nLFxyXG4gICAgICAgICAgICAgICAgICAgIHJlbmRlckNvbXBvbmVudDogRGlzcGxheUFnZUNvbXBvbmVudCxcclxuICAgICAgICAgICAgICAgICAgICBlZGl0b3I6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ2N1c3RvbScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbXBvbmVudDogU2VsZWN0QWdlQ29tcG9uZW50XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIG1heEFnZToge1xyXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiAnTWF4aW11bSBBZ2UnLFxyXG4gICAgICAgICAgICAgICAgICAgIGZpbHRlcjogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZTogJ2N1c3RvbScsXHJcbiAgICAgICAgICAgICAgICAgICAgcmVuZGVyQ29tcG9uZW50OiBEaXNwbGF5QWdlQ29tcG9uZW50LFxyXG4gICAgICAgICAgICAgICAgICAgIGVkaXRvcjoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAnY3VzdG9tJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29tcG9uZW50OiBTZWxlY3RBZ2VDb21wb25lbnRcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgY2FyZVR5cGU6IHtcclxuICAgICAgICAgICAgICAgICAgICB0aXRsZTogJ0NhcmUgVHlwZScsXHJcbiAgICAgICAgICAgICAgICAgICAgZmlsdGVyOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICB0eXBlOiAnY3VzdG9tJyxcclxuICAgICAgICAgICAgICAgICAgICBkZWZhdWx0VmFsdWU6ICcwJyxcclxuICAgICAgICAgICAgICAgICAgICByZW5kZXJDb21wb25lbnQ6IERpc3BsYXlDYXJlVHlwZUNvbXBvbmVudCxcclxuICAgICAgICAgICAgICAgICAgICBlZGl0b3I6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ2xpc3QnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25maWc6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxpc3Q6IHRoaXMuY2FyZVR5cGVzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBub0RhdGFNZXNzYWdlOiAnTm8gUm9vbXMnXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxufVxyXG4iXX0=
