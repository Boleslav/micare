"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var forms_1 = require('@angular/forms');
var index_1 = require('../../../../validators/index');
var staffSignup_service_1 = require('../staffSignupService/staffSignup.service');
var angular2_toaster_1 = require('angular2-toaster');
var index_2 = require('../../../../shared/index');
var index_3 = require('../../../../api/index');
var StaffSignupFormComponent = (function () {
    function StaffSignupFormComponent(fb, _staffSignupService, _toasterService, _route) {
        this._staffSignupService = _staffSignupService;
        this._toasterService = _toasterService;
        this._route = _route;
        this.submitted = false;
        this.userCreated = false;
        this.form = fb.group({
            'firstName': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(1)])],
            'lastName': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(1)])],
            'mobilePhone': ['', forms_1.Validators.compose([forms_1.Validators.required, index_1.MobileValidator.validate])],
            'passwords': fb.group({
                'password': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(8), index_1.ComplexPasswordValidator.validate])],
                'confirmPassword': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(8)])]
            }, { validator: index_1.EqualPasswordsValidator.validate('password', 'confirmPassword') })
        });
        this.firstName = this.form.controls['firstName'];
        this.lastName = this.form.controls['lastName'];
        this.mobilePhone = this.form.controls['mobilePhone'];
        this.passwords = this.form.controls['passwords'];
        this.password = this.passwords.controls['password'];
        this.confirmPassword = this.passwords.controls['confirmPassword'];
    }
    StaffSignupFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route
            .params
            .switchMap(function (params) {
            _this.token = params['token'];
            return _this._staffSignupService.GetStaffMemberInvite$(_this.token);
        }).subscribe(function (res) {
            _this.email = res;
        });
    };
    StaffSignupFormComponent.prototype.onSubmit = function (values) {
        var _this = this;
        this.submitted = true;
        var savingToast = this._toasterService.pop(new index_2.SpinningToast());
        var acceptStaffInvite = new index_3.AcceptStaffInviteDto();
        acceptStaffInvite.init({
            FirstName: values.firstName,
            LastName: values.lastName,
            Email: this.email,
            MobilePhone: values.mobilePhone,
            Password: values.passwords.password,
            ConfirmPassword: values.passwords.confirmPassword,
            Token: this.token
        });
        this._staffSignupService
            .AcceptStaffMemberInvite$(acceptStaffInvite)
            .subscribe(function (res) {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this._toasterService.pop('success', 'Staff Member Created', '');
            _this.userCreated = true;
        }, function (err) {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this.submitted = false;
        });
    };
    ;
    StaffSignupFormComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'staff-signup-form-cmp',
            templateUrl: 'staffSignupForm.component.html'
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, staffSignup_service_1.StaffSignupService, angular2_toaster_1.ToasterService, router_1.ActivatedRoute])
    ], StaffSignupFormComponent);
    return StaffSignupFormComponent;
}());
exports.StaffSignupFormComponent = StaffSignupFormComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3N0YWZmLXNpZ251cC9zdGFmZlNpZ251cEZvcm0vc3RhZmZTaWdudXBGb3JtLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQWtDLGVBQWUsQ0FBQyxDQUFBO0FBQ2xELHVCQUF1QyxpQkFBaUIsQ0FBQyxDQUFBO0FBQ3pELHNCQUFvRSxnQkFBZ0IsQ0FBQyxDQUFBO0FBQ3JGLHNCQUFtRiw4QkFBOEIsQ0FBQyxDQUFBO0FBRWxILG9DQUFtQywyQ0FBMkMsQ0FBQyxDQUFBO0FBQy9FLGlDQUErQixrQkFBa0IsQ0FBQyxDQUFBO0FBQ2xELHNCQUE4QiwwQkFBMEIsQ0FBQyxDQUFBO0FBQ3pELHNCQUFxQyx1QkFBdUIsQ0FBQyxDQUFBO0FBUTdEO0lBZUksa0NBQVksRUFBZSxFQUNmLG1CQUF1QyxFQUN2QyxlQUErQixFQUMvQixNQUFzQjtRQUZ0Qix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQW9CO1FBQ3ZDLG9CQUFlLEdBQWYsZUFBZSxDQUFnQjtRQUMvQixXQUFNLEdBQU4sTUFBTSxDQUFnQjtRQU4xQixjQUFTLEdBQVksS0FBSyxDQUFDO1FBQzNCLGdCQUFXLEdBQVksS0FBSyxDQUFDO1FBT2pDLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQztZQUNqQixXQUFXLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDckYsVUFBVSxFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsa0JBQVUsQ0FBQyxRQUFRLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3BGLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLGtCQUFVLENBQUMsUUFBUSxFQUFFLHVCQUFlLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUN4RixXQUFXLEVBQUUsRUFBRSxDQUFDLEtBQUssQ0FBQztnQkFDbEIsVUFBVSxFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsa0JBQVUsQ0FBQyxRQUFRLEVBQUUsa0JBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUUsZ0NBQXdCLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDdkgsaUJBQWlCLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsRUFBRSxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDOUYsRUFBRSxFQUFFLFNBQVMsRUFBRSwrQkFBdUIsQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLGlCQUFpQixDQUFDLEVBQUUsQ0FBQztTQUNyRixDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ2pELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDL0MsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNyRCxJQUFJLENBQUMsU0FBUyxHQUFjLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQzVELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDcEQsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0lBQ3RFLENBQUM7SUFFRCwyQ0FBUSxHQUFSO1FBQUEsaUJBU0M7UUFSRyxJQUFJLENBQUMsTUFBTTthQUNOLE1BQU07YUFDTixTQUFTLENBQUMsVUFBQyxNQUFjO1lBQ3RCLEtBQUksQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzdCLE1BQU0sQ0FBQyxLQUFJLENBQUMsbUJBQW1CLENBQUMscUJBQXFCLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3RFLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLEdBQUc7WUFDWixLQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztRQUNyQixDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFRCwyQ0FBUSxHQUFSLFVBQVMsTUFBVztRQUFwQixpQkE0QkM7UUExQkcsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFFdEIsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsSUFBSSxxQkFBYSxFQUFFLENBQUMsQ0FBQztRQUVoRSxJQUFJLGlCQUFpQixHQUFHLElBQUksNEJBQW9CLEVBQUUsQ0FBQztRQUNuRCxpQkFBaUIsQ0FBQyxJQUFJLENBQUM7WUFDbkIsU0FBUyxFQUFFLE1BQU0sQ0FBQyxTQUFTO1lBQzNCLFFBQVEsRUFBRSxNQUFNLENBQUMsUUFBUTtZQUN6QixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsV0FBVyxFQUFFLE1BQU0sQ0FBQyxXQUFXO1lBQy9CLFFBQVEsRUFBRSxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVE7WUFDbkMsZUFBZSxFQUFFLE1BQU0sQ0FBQyxTQUFTLENBQUMsZUFBZTtZQUNqRCxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7U0FDcEIsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLG1CQUFtQjthQUNuQix3QkFBd0IsQ0FBQyxpQkFBaUIsQ0FBQzthQUMzQyxTQUFTLENBQUMsVUFBQSxHQUFHO1lBQ1YsS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxXQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUM5RSxLQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsc0JBQXNCLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDaEUsS0FBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7UUFDNUIsQ0FBQyxFQUNELFVBQUEsR0FBRztZQUNDLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDOUUsS0FBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7UUFDM0IsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDOztJQW5GTDtRQUFDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLHVCQUF1QjtZQUNqQyxXQUFXLEVBQUUsZ0NBQWdDO1NBQ2hELENBQUM7O2dDQUFBO0lBZ0ZGLCtCQUFDO0FBQUQsQ0E5RUEsQUE4RUMsSUFBQTtBQTlFWSxnQ0FBd0IsMkJBOEVwQyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3N0YWZmLXNpZ251cC9zdGFmZlNpZ251cEZvcm0vc3RhZmZTaWdudXBGb3JtLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlLCBQYXJhbXMgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBGb3JtR3JvdXAsIEFic3RyYWN0Q29udHJvbCwgRm9ybUJ1aWxkZXIsIFZhbGlkYXRvcnMgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IE1vYmlsZVZhbGlkYXRvciwgQ29tcGxleFBhc3N3b3JkVmFsaWRhdG9yLCBFcXVhbFBhc3N3b3Jkc1ZhbGlkYXRvciB9IGZyb20gJy4uLy4uLy4uLy4uL3ZhbGlkYXRvcnMvaW5kZXgnO1xyXG5cclxuaW1wb3J0IHsgU3RhZmZTaWdudXBTZXJ2aWNlIH0gZnJvbSAnLi4vc3RhZmZTaWdudXBTZXJ2aWNlL3N0YWZmU2lnbnVwLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBUb2FzdGVyU2VydmljZSB9IGZyb20gJ2FuZ3VsYXIyLXRvYXN0ZXInO1xyXG5pbXBvcnQgeyBTcGlubmluZ1RvYXN0IH0gZnJvbSAnLi4vLi4vLi4vLi4vc2hhcmVkL2luZGV4JztcclxuaW1wb3J0IHsgQWNjZXB0U3RhZmZJbnZpdGVEdG8gfSBmcm9tICcuLi8uLi8uLi8uLi9hcGkvaW5kZXgnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgc2VsZWN0b3I6ICdzdGFmZi1zaWdudXAtZm9ybS1jbXAnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICdzdGFmZlNpZ251cEZvcm0uY29tcG9uZW50Lmh0bWwnXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgU3RhZmZTaWdudXBGb3JtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIHByaXZhdGUgdG9rZW46IHN0cmluZztcclxuICAgIHByaXZhdGUgZW1haWw6IHN0cmluZztcclxuXHJcbiAgICBwcml2YXRlIGZvcm06IEZvcm1Hcm91cDtcclxuICAgIHByaXZhdGUgZmlyc3ROYW1lOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgICBwcml2YXRlIGxhc3ROYW1lOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgICBwcml2YXRlIG1vYmlsZVBob25lOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgICBwcml2YXRlIHBhc3N3b3JkOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgICBwcml2YXRlIGNvbmZpcm1QYXNzd29yZDogQWJzdHJhY3RDb250cm9sO1xyXG4gICAgcHJpdmF0ZSBwYXNzd29yZHM6IEZvcm1Hcm91cDtcclxuXHJcbiAgICBwcml2YXRlIHN1Ym1pdHRlZDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgcHJpdmF0ZSB1c2VyQ3JlYXRlZDogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGZiOiBGb3JtQnVpbGRlcixcclxuICAgICAgICBwcml2YXRlIF9zdGFmZlNpZ251cFNlcnZpY2U6IFN0YWZmU2lnbnVwU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIF90b2FzdGVyU2VydmljZTogVG9hc3RlclNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBfcm91dGU6IEFjdGl2YXRlZFJvdXRlKSB7XHJcblxyXG4gICAgICAgIHRoaXMuZm9ybSA9IGZiLmdyb3VwKHtcclxuICAgICAgICAgICAgJ2ZpcnN0TmFtZSc6IFsnJywgVmFsaWRhdG9ycy5jb21wb3NlKFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1pbkxlbmd0aCgxKV0pXSxcclxuICAgICAgICAgICAgJ2xhc3ROYW1lJzogWycnLCBWYWxpZGF0b3JzLmNvbXBvc2UoW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWluTGVuZ3RoKDEpXSldLFxyXG4gICAgICAgICAgICAnbW9iaWxlUGhvbmUnOiBbJycsIFZhbGlkYXRvcnMuY29tcG9zZShbVmFsaWRhdG9ycy5yZXF1aXJlZCwgTW9iaWxlVmFsaWRhdG9yLnZhbGlkYXRlXSldLFxyXG4gICAgICAgICAgICAncGFzc3dvcmRzJzogZmIuZ3JvdXAoe1xyXG4gICAgICAgICAgICAgICAgJ3Bhc3N3b3JkJzogWycnLCBWYWxpZGF0b3JzLmNvbXBvc2UoW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWluTGVuZ3RoKDgpLCBDb21wbGV4UGFzc3dvcmRWYWxpZGF0b3IudmFsaWRhdGVdKV0sXHJcbiAgICAgICAgICAgICAgICAnY29uZmlybVBhc3N3b3JkJzogWycnLCBWYWxpZGF0b3JzLmNvbXBvc2UoW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWluTGVuZ3RoKDgpXSldXHJcbiAgICAgICAgICAgIH0sIHsgdmFsaWRhdG9yOiBFcXVhbFBhc3N3b3Jkc1ZhbGlkYXRvci52YWxpZGF0ZSgncGFzc3dvcmQnLCAnY29uZmlybVBhc3N3b3JkJykgfSlcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgdGhpcy5maXJzdE5hbWUgPSB0aGlzLmZvcm0uY29udHJvbHNbJ2ZpcnN0TmFtZSddO1xyXG4gICAgICAgIHRoaXMubGFzdE5hbWUgPSB0aGlzLmZvcm0uY29udHJvbHNbJ2xhc3ROYW1lJ107XHJcbiAgICAgICAgdGhpcy5tb2JpbGVQaG9uZSA9IHRoaXMuZm9ybS5jb250cm9sc1snbW9iaWxlUGhvbmUnXTtcclxuICAgICAgICB0aGlzLnBhc3N3b3JkcyA9IDxGb3JtR3JvdXA+dGhpcy5mb3JtLmNvbnRyb2xzWydwYXNzd29yZHMnXTtcclxuICAgICAgICB0aGlzLnBhc3N3b3JkID0gdGhpcy5wYXNzd29yZHMuY29udHJvbHNbJ3Bhc3N3b3JkJ107XHJcbiAgICAgICAgdGhpcy5jb25maXJtUGFzc3dvcmQgPSB0aGlzLnBhc3N3b3Jkcy5jb250cm9sc1snY29uZmlybVBhc3N3b3JkJ107XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5fcm91dGVcclxuICAgICAgICAgICAgLnBhcmFtc1xyXG4gICAgICAgICAgICAuc3dpdGNoTWFwKChwYXJhbXM6IFBhcmFtcykgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50b2tlbiA9IHBhcmFtc1sndG9rZW4nXTtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLl9zdGFmZlNpZ251cFNlcnZpY2UuR2V0U3RhZmZNZW1iZXJJbnZpdGUkKHRoaXMudG9rZW4pO1xyXG4gICAgICAgICAgICB9KS5zdWJzY3JpYmUocmVzID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZW1haWwgPSByZXM7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIG9uU3VibWl0KHZhbHVlczogYW55KSB7XHJcblxyXG4gICAgICAgIHRoaXMuc3VibWl0dGVkID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgbGV0IHNhdmluZ1RvYXN0ID0gdGhpcy5fdG9hc3RlclNlcnZpY2UucG9wKG5ldyBTcGlubmluZ1RvYXN0KCkpO1xyXG5cclxuICAgICAgICB2YXIgYWNjZXB0U3RhZmZJbnZpdGUgPSBuZXcgQWNjZXB0U3RhZmZJbnZpdGVEdG8oKTtcclxuICAgICAgICBhY2NlcHRTdGFmZkludml0ZS5pbml0KHtcclxuICAgICAgICAgICAgRmlyc3ROYW1lOiB2YWx1ZXMuZmlyc3ROYW1lLFxyXG4gICAgICAgICAgICBMYXN0TmFtZTogdmFsdWVzLmxhc3ROYW1lLFxyXG4gICAgICAgICAgICBFbWFpbDogdGhpcy5lbWFpbCxcclxuICAgICAgICAgICAgTW9iaWxlUGhvbmU6IHZhbHVlcy5tb2JpbGVQaG9uZSxcclxuICAgICAgICAgICAgUGFzc3dvcmQ6IHZhbHVlcy5wYXNzd29yZHMucGFzc3dvcmQsXHJcbiAgICAgICAgICAgIENvbmZpcm1QYXNzd29yZDogdmFsdWVzLnBhc3N3b3Jkcy5jb25maXJtUGFzc3dvcmQsXHJcbiAgICAgICAgICAgIFRva2VuOiB0aGlzLnRva2VuXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRoaXMuX3N0YWZmU2lnbnVwU2VydmljZVxyXG4gICAgICAgICAgICAuQWNjZXB0U3RhZmZNZW1iZXJJbnZpdGUkKGFjY2VwdFN0YWZmSW52aXRlKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKHJlcyA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcihzYXZpbmdUb2FzdC50b2FzdElkLCBzYXZpbmdUb2FzdC50b2FzdENvbnRhaW5lcklkKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLnBvcCgnc3VjY2VzcycsICdTdGFmZiBNZW1iZXIgQ3JlYXRlZCcsICcnKTtcclxuICAgICAgICAgICAgICAgIHRoaXMudXNlckNyZWF0ZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBlcnIgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UuY2xlYXIoc2F2aW5nVG9hc3QudG9hc3RJZCwgc2F2aW5nVG9hc3QudG9hc3RDb250YWluZXJJZCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnN1Ym1pdHRlZCA9IGZhbHNlOyAvL3VzZXIgbmVlZHMgdG8gYmUgYWJsZSB0byBjaGFuZ2UgdGhpbmdzIGFuZCB0cnkgYWdhaW5cclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9O1xyXG59XHJcbiJdfQ==
