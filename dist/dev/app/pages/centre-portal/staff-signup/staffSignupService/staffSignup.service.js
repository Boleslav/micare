"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../../api/index');
var StaffSignupService = (function () {
    function StaffSignupService(_apiClient) {
        this._apiClient = _apiClient;
    }
    StaffSignupService.prototype.GetStaffMemberInvite$ = function (token) {
        return this._apiClient.account_GetStaffMemberInvite(token);
    };
    StaffSignupService.prototype.AcceptStaffMemberInvite$ = function (acceptStaffInviteDto) {
        return this._apiClient.account_AcceptStaffMemberInvite(acceptStaffInviteDto);
    };
    StaffSignupService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.ApiClientService])
    ], StaffSignupService);
    return StaffSignupService;
}());
exports.StaffSignupService = StaffSignupService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3N0YWZmLXNpZ251cC9zdGFmZlNpZ251cFNlcnZpY2Uvc3RhZmZTaWdudXAuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQTJCLGVBQWUsQ0FBQyxDQUFBO0FBRzNDLHNCQUF1RCx1QkFBdUIsQ0FBQyxDQUFBO0FBRy9FO0lBRUksNEJBQW9CLFVBQTRCO1FBQTVCLGVBQVUsR0FBVixVQUFVLENBQWtCO0lBQ2hELENBQUM7SUFFTSxrREFBcUIsR0FBNUIsVUFBNkIsS0FBYTtRQUN0QyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyw0QkFBNEIsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUMvRCxDQUFDO0lBRU0scURBQXdCLEdBQS9CLFVBQWdDLG9CQUEwQztRQUN0RSxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQywrQkFBK0IsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO0lBQ2pGLENBQUM7SUFaTDtRQUFDLGlCQUFVLEVBQUU7OzBCQUFBO0lBYWIseUJBQUM7QUFBRCxDQVpBLEFBWUMsSUFBQTtBQVpZLDBCQUFrQixxQkFZOUIsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9zdGFmZi1zaWdudXAvc3RhZmZTaWdudXBTZXJ2aWNlL3N0YWZmU2lnbnVwLnNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuXHJcbmltcG9ydCB7IEFwaUNsaWVudFNlcnZpY2UsIEFjY2VwdFN0YWZmSW52aXRlRHRvIH0gZnJvbSAnLi4vLi4vLi4vLi4vYXBpL2luZGV4JztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIFN0YWZmU2lnbnVwU2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfYXBpQ2xpZW50OiBBcGlDbGllbnRTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIEdldFN0YWZmTWVtYmVySW52aXRlJCh0b2tlbjogc3RyaW5nKTogT2JzZXJ2YWJsZTxzdHJpbmc+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fYXBpQ2xpZW50LmFjY291bnRfR2V0U3RhZmZNZW1iZXJJbnZpdGUodG9rZW4pO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBBY2NlcHRTdGFmZk1lbWJlckludml0ZSQoYWNjZXB0U3RhZmZJbnZpdGVEdG86IEFjY2VwdFN0YWZmSW52aXRlRHRvKTogT2JzZXJ2YWJsZTx2b2lkPiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2FwaUNsaWVudC5hY2NvdW50X0FjY2VwdFN0YWZmTWVtYmVySW52aXRlKGFjY2VwdFN0YWZmSW52aXRlRHRvKTtcclxuICAgIH1cclxufVxyXG4iXX0=
