"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../shared/index');
var index_2 = require('./index');
var StaffSignupModule = (function () {
    function StaffSignupModule() {
    }
    StaffSignupModule = __decorate([
        core_1.NgModule({
            imports: [index_1.SharedModule],
            declarations: [index_2.StaffSignupComponent, index_2.StaffSignupFormComponent],
            providers: [index_2.StaffSignupService]
        }), 
        __metadata('design:paramtypes', [])
    ], StaffSignupModule);
    return StaffSignupModule;
}());
exports.StaffSignupModule = StaffSignupModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3N0YWZmLXNpZ251cC9zdGFmZlNpZ251cC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUF5QixlQUFlLENBQUMsQ0FBQTtBQUN6QyxzQkFBNkIsdUJBQXVCLENBQUMsQ0FBQTtBQUVyRCxzQkFBbUYsU0FBUyxDQUFDLENBQUE7QUFRN0Y7SUFBQTtJQUFpQyxDQUFDO0lBTmxDO1FBQUMsZUFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFLENBQUMsb0JBQVksQ0FBQztZQUN2QixZQUFZLEVBQUUsQ0FBQyw0QkFBb0IsRUFBRSxnQ0FBd0IsQ0FBQztZQUM5RCxTQUFTLEVBQUUsQ0FBQywwQkFBa0IsQ0FBQztTQUNsQyxDQUFDOzt5QkFBQTtJQUUrQix3QkFBQztBQUFELENBQWpDLEFBQWtDLElBQUE7QUFBckIseUJBQWlCLG9CQUFJLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL2NlbnRyZS1wb3J0YWwvc3RhZmYtc2lnbnVwL3N0YWZmU2lnbnVwLm1vZHVsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFNoYXJlZE1vZHVsZSB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9pbmRleCc7XHJcblxyXG5pbXBvcnQgeyBTdGFmZlNpZ251cENvbXBvbmVudCwgU3RhZmZTaWdudXBGb3JtQ29tcG9uZW50LCBTdGFmZlNpZ251cFNlcnZpY2UgfSBmcm9tICcuL2luZGV4JztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiBbU2hhcmVkTW9kdWxlXSxcclxuICAgIGRlY2xhcmF0aW9uczogW1N0YWZmU2lnbnVwQ29tcG9uZW50LCBTdGFmZlNpZ251cEZvcm1Db21wb25lbnRdLFxyXG4gICAgcHJvdmlkZXJzOiBbU3RhZmZTaWdudXBTZXJ2aWNlXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFN0YWZmU2lnbnVwTW9kdWxlIHsgfVxyXG4iXX0=
