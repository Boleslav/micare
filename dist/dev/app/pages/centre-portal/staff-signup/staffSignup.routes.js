"use strict";
var index_1 = require('./index');
exports.StaffSignupRoutes = [
    {
        path: 'staff-signup/:token',
        component: index_1.StaffSignupComponent
    }
];

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3N0YWZmLXNpZ251cC9zdGFmZlNpZ251cC5yb3V0ZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBLHNCQUFxQyxTQUFTLENBQUMsQ0FBQTtBQUVsQyx5QkFBaUIsR0FBWTtJQUN2QztRQUNFLElBQUksRUFBRSxxQkFBcUI7UUFDM0IsU0FBUyxFQUFFLDRCQUFvQjtLQUNoQztDQUNILENBQUMiLCJmaWxlIjoiYXBwL3BhZ2VzL2NlbnRyZS1wb3J0YWwvc3RhZmYtc2lnbnVwL3N0YWZmU2lnbnVwLnJvdXRlcy5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgU3RhZmZTaWdudXBDb21wb25lbnQgfSBmcm9tICcuL2luZGV4JztcclxuXHJcbmV4cG9ydCBjb25zdCBTdGFmZlNpZ251cFJvdXRlczogUm91dGVbXSA9IFtcclxuICBcdHtcclxuICAgIFx0cGF0aDogJ3N0YWZmLXNpZ251cC86dG9rZW4nLFxyXG4gICAgXHRjb21wb25lbnQ6IFN0YWZmU2lnbnVwQ29tcG9uZW50XHJcbiAgXHR9XHJcbl07XHJcbiJdfQ==
