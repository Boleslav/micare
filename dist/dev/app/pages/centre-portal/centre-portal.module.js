"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../shared/index');
var index_2 = require('./shared/index');
var index_3 = require('./index');
var index_4 = require('./centre-dashboard/index');
var rooms_module_1 = require('./rooms/rooms.module');
var index_5 = require('./staff/index');
var index_6 = require('./companies/index');
var index_7 = require('./centre/index');
var index_8 = require('./admin-dashboard/index');
var index_9 = require('./admin-approvals/index');
var index_10 = require('./bookings/index');
var index_11 = require('./shutdowndays/index');
var index_12 = require('./swaps/index');
var index_13 = require('./vacancies/index');
var index_14 = require('./enrolment/index');
var index_15 = require('./login/index');
var index_16 = require('./logout/index');
var index_17 = require('./signup/index');
var index_18 = require('./staff-signup/index');
var index_19 = require('./enrolment-approvals/index');
var index_20 = require('./family-enrolment-form/index');
var tools_module_1 = require("./tools/tools.module");
var CentrePortalModule = (function () {
    function CentrePortalModule() {
    }
    CentrePortalModule = __decorate([
        core_1.NgModule({
            imports: [
                index_9.AdminApprovalsModule,
                index_8.AdminDashboardModule,
                index_4.CentreDashboardModule,
                index_6.CompaniesModule,
                index_7.CentreModule,
                index_5.StaffModule,
                rooms_module_1.RoomsModule,
                index_10.BookingsModule,
                index_11.ShutdownDaysModule,
                index_12.SwapsModule,
                index_13.VacanciesModule,
                index_14.EnrolmentModule,
                index_1.SharedModule,
                index_15.LoginModule,
                index_16.LogoutModule,
                index_17.SignupModule,
                index_18.StaffSignupModule,
                index_19.EnrolmentApprovalsModule,
                index_20.FamilyEnrolmentFormModule,
                tools_module_1.ToolsModule
            ],
            declarations: [
                index_2.TopNavComponent,
                index_2.SidebarComponent,
                index_3.CentrePortalComponent
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], CentrePortalModule);
    return CentrePortalModule;
}());
exports.CentrePortalModule = CentrePortalModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NlbnRyZS1wb3J0YWwubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBeUIsZUFBZSxDQUFDLENBQUE7QUFDekMsc0JBQTZCLG9CQUFvQixDQUFDLENBQUE7QUFDbEQsc0JBQWtELGdCQUFnQixDQUFDLENBQUE7QUFDbkUsc0JBQXNDLFNBQVMsQ0FBQyxDQUFBO0FBQ2hELHNCQUFzQywwQkFBMEIsQ0FBQyxDQUFBO0FBQ2pFLDZCQUE0QixzQkFBc0IsQ0FBQyxDQUFBO0FBQ25ELHNCQUE0QixlQUFlLENBQUMsQ0FBQTtBQUM1QyxzQkFBZ0MsbUJBQW1CLENBQUMsQ0FBQTtBQUNwRCxzQkFBNkIsZ0JBQWdCLENBQUMsQ0FBQTtBQUM5QyxzQkFBcUMseUJBQXlCLENBQUMsQ0FBQTtBQUMvRCxzQkFBcUMseUJBQXlCLENBQUMsQ0FBQTtBQUMvRCx1QkFBK0Isa0JBQWtCLENBQUMsQ0FBQTtBQUNsRCx1QkFBbUMsc0JBQXNCLENBQUMsQ0FBQTtBQUMxRCx1QkFBNEIsZUFBZSxDQUFDLENBQUE7QUFDNUMsdUJBQWdDLG1CQUFtQixDQUFDLENBQUE7QUFDcEQsdUJBQWdDLG1CQUFtQixDQUFDLENBQUE7QUFDcEQsdUJBQTRCLGVBQWUsQ0FBQyxDQUFBO0FBQzVDLHVCQUE2QixnQkFBZ0IsQ0FBQyxDQUFBO0FBQzlDLHVCQUE2QixnQkFBZ0IsQ0FBQyxDQUFBO0FBQzlDLHVCQUFrQyxzQkFBc0IsQ0FBQyxDQUFBO0FBQ3pELHVCQUF5Qyw2QkFBNkIsQ0FBQyxDQUFBO0FBQ3ZFLHVCQUEwQywrQkFBK0IsQ0FBQyxDQUFBO0FBQzFFLDZCQUE0QixzQkFBc0IsQ0FBQyxDQUFBO0FBK0JuRDtJQUFBO0lBQWtDLENBQUM7SUE3Qm5DO1FBQUMsZUFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFO2dCQUNMLDRCQUFvQjtnQkFDcEIsNEJBQW9CO2dCQUNwQiw2QkFBcUI7Z0JBQ3JCLHVCQUFlO2dCQUNmLG9CQUFZO2dCQUNaLG1CQUFXO2dCQUNYLDBCQUFXO2dCQUNYLHVCQUFjO2dCQUNkLDJCQUFrQjtnQkFDbEIsb0JBQVc7Z0JBQ1gsd0JBQWU7Z0JBQ2Ysd0JBQWU7Z0JBQ2Ysb0JBQVk7Z0JBQ1osb0JBQVc7Z0JBQ1gscUJBQVk7Z0JBQ1oscUJBQVk7Z0JBQ1osMEJBQWlCO2dCQUNqQixpQ0FBd0I7Z0JBQ3hCLGtDQUF5QjtnQkFDekIsMEJBQVc7YUFDZDtZQUNELFlBQVksRUFBRTtnQkFDVix1QkFBZTtnQkFDZix3QkFBZ0I7Z0JBQ2hCLDZCQUFxQjthQUN4QjtTQUNKLENBQUM7OzBCQUFBO0lBQ2dDLHlCQUFDO0FBQUQsQ0FBbEMsQUFBbUMsSUFBQTtBQUF0QiwwQkFBa0IscUJBQUksQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9jZW50cmUtcG9ydGFsLm1vZHVsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBTaGFyZWRNb2R1bGUgfSBmcm9tICcuLi8uLi9zaGFyZWQvaW5kZXgnO1xuaW1wb3J0IHsgVG9wTmF2Q29tcG9uZW50LCBTaWRlYmFyQ29tcG9uZW50IH0gZnJvbSAnLi9zaGFyZWQvaW5kZXgnO1xuaW1wb3J0IHsgQ2VudHJlUG9ydGFsQ29tcG9uZW50IH0gZnJvbSAnLi9pbmRleCc7XG5pbXBvcnQgeyBDZW50cmVEYXNoYm9hcmRNb2R1bGUgfSBmcm9tICcuL2NlbnRyZS1kYXNoYm9hcmQvaW5kZXgnO1xuaW1wb3J0IHsgUm9vbXNNb2R1bGUgfSBmcm9tICcuL3Jvb21zL3Jvb21zLm1vZHVsZSc7XG5pbXBvcnQgeyBTdGFmZk1vZHVsZSB9IGZyb20gJy4vc3RhZmYvaW5kZXgnO1xuaW1wb3J0IHsgQ29tcGFuaWVzTW9kdWxlIH0gZnJvbSAnLi9jb21wYW5pZXMvaW5kZXgnO1xuaW1wb3J0IHsgQ2VudHJlTW9kdWxlIH0gZnJvbSAnLi9jZW50cmUvaW5kZXgnO1xuaW1wb3J0IHsgQWRtaW5EYXNoYm9hcmRNb2R1bGUgfSBmcm9tICcuL2FkbWluLWRhc2hib2FyZC9pbmRleCc7XG5pbXBvcnQgeyBBZG1pbkFwcHJvdmFsc01vZHVsZSB9IGZyb20gJy4vYWRtaW4tYXBwcm92YWxzL2luZGV4JztcbmltcG9ydCB7IEJvb2tpbmdzTW9kdWxlIH0gZnJvbSAnLi9ib29raW5ncy9pbmRleCc7XG5pbXBvcnQgeyBTaHV0ZG93bkRheXNNb2R1bGUgfSBmcm9tICcuL3NodXRkb3duZGF5cy9pbmRleCc7XG5pbXBvcnQgeyBTd2Fwc01vZHVsZSB9IGZyb20gJy4vc3dhcHMvaW5kZXgnO1xuaW1wb3J0IHsgVmFjYW5jaWVzTW9kdWxlIH0gZnJvbSAnLi92YWNhbmNpZXMvaW5kZXgnO1xuaW1wb3J0IHsgRW5yb2xtZW50TW9kdWxlIH0gZnJvbSAnLi9lbnJvbG1lbnQvaW5kZXgnO1xuaW1wb3J0IHsgTG9naW5Nb2R1bGUgfSBmcm9tICcuL2xvZ2luL2luZGV4JztcbmltcG9ydCB7IExvZ291dE1vZHVsZSB9IGZyb20gJy4vbG9nb3V0L2luZGV4JztcbmltcG9ydCB7IFNpZ251cE1vZHVsZSB9IGZyb20gJy4vc2lnbnVwL2luZGV4JztcbmltcG9ydCB7IFN0YWZmU2lnbnVwTW9kdWxlIH0gZnJvbSAnLi9zdGFmZi1zaWdudXAvaW5kZXgnO1xuaW1wb3J0IHsgRW5yb2xtZW50QXBwcm92YWxzTW9kdWxlIH0gZnJvbSAnLi9lbnJvbG1lbnQtYXBwcm92YWxzL2luZGV4JztcbmltcG9ydCB7IEZhbWlseUVucm9sbWVudEZvcm1Nb2R1bGUgfSBmcm9tICcuL2ZhbWlseS1lbnJvbG1lbnQtZm9ybS9pbmRleCc7XG5pbXBvcnQgeyBUb29sc01vZHVsZSB9IGZyb20gXCIuL3Rvb2xzL3Rvb2xzLm1vZHVsZVwiO1xuXG5ATmdNb2R1bGUoe1xuICAgIGltcG9ydHM6IFtcbiAgICAgICAgQWRtaW5BcHByb3ZhbHNNb2R1bGUsXG4gICAgICAgIEFkbWluRGFzaGJvYXJkTW9kdWxlLFxuICAgICAgICBDZW50cmVEYXNoYm9hcmRNb2R1bGUsXG4gICAgICAgIENvbXBhbmllc01vZHVsZSxcbiAgICAgICAgQ2VudHJlTW9kdWxlLFxuICAgICAgICBTdGFmZk1vZHVsZSxcbiAgICAgICAgUm9vbXNNb2R1bGUsXG4gICAgICAgIEJvb2tpbmdzTW9kdWxlLFxuICAgICAgICBTaHV0ZG93bkRheXNNb2R1bGUsXG4gICAgICAgIFN3YXBzTW9kdWxlLFxuICAgICAgICBWYWNhbmNpZXNNb2R1bGUsXG4gICAgICAgIEVucm9sbWVudE1vZHVsZSxcbiAgICAgICAgU2hhcmVkTW9kdWxlLFxuICAgICAgICBMb2dpbk1vZHVsZSxcbiAgICAgICAgTG9nb3V0TW9kdWxlLFxuICAgICAgICBTaWdudXBNb2R1bGUsXG4gICAgICAgIFN0YWZmU2lnbnVwTW9kdWxlLFxuICAgICAgICBFbnJvbG1lbnRBcHByb3ZhbHNNb2R1bGUsXG4gICAgICAgIEZhbWlseUVucm9sbWVudEZvcm1Nb2R1bGUsXG4gICAgICAgIFRvb2xzTW9kdWxlXG4gICAgXSxcbiAgICBkZWNsYXJhdGlvbnM6IFtcbiAgICAgICAgVG9wTmF2Q29tcG9uZW50LFxuICAgICAgICBTaWRlYmFyQ29tcG9uZW50LFxuICAgICAgICBDZW50cmVQb3J0YWxDb21wb25lbnRcbiAgICBdXG59KVxuZXhwb3J0IGNsYXNzIENlbnRyZVBvcnRhbE1vZHVsZSB7IH1cbiJdfQ==
