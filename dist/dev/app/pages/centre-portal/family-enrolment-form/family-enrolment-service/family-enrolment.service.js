"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var rxjs_1 = require('rxjs');
var index_1 = require('../../../../api/index');
var FamilyEnrolmentService = (function () {
    function FamilyEnrolmentService(_apiService) {
        this._apiService = _apiService;
        this.titles = null;
        this.genders = null;
        this.careTypes = null;
        this.relationships = null;
    }
    FamilyEnrolmentService.prototype.getTitles$ = function () {
        var _this = this;
        if (this.titles !== null)
            return rxjs_1.Observable.of(this.titles);
        return this._apiService
            .generics_GetEnumDescriptions('NameTitle')
            .map(function (res) { return _this.titles = res; });
    };
    FamilyEnrolmentService.prototype.getGenders$ = function () {
        var _this = this;
        if (this.genders !== null)
            return rxjs_1.Observable.of(this.genders);
        return this._apiService
            .generics_GetEnumDescriptions('Gender')
            .map(function (res) { return _this.genders = res; });
    };
    FamilyEnrolmentService.prototype.getCareTypes$ = function () {
        var _this = this;
        if (this.careTypes !== null)
            return rxjs_1.Observable.of(this.careTypes);
        return this._apiService
            .generics_GetEnumDescriptions('CareType')
            .map(function (res) { return _this.careTypes = res; });
    };
    FamilyEnrolmentService.prototype.getRelationships$ = function () {
        var _this = this;
        if (this.relationships !== null)
            return rxjs_1.Observable.of(this.relationships);
        return this._apiService
            .generics_GetEnumDescriptions('Relationship')
            .map(function (res) { return _this.relationships = res; });
    };
    FamilyEnrolmentService.prototype.getEnrolmentForm$ = function (parentId, centreId) {
        return this._apiService.enrolmentForm_GetCompleteEnrolmentForm(parentId, centreId);
    };
    FamilyEnrolmentService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.ApiClientService])
    ], FamilyEnrolmentService);
    return FamilyEnrolmentService;
}());
exports.FamilyEnrolmentService = FamilyEnrolmentService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2ZhbWlseS1lbnJvbG1lbnQtZm9ybS9mYW1pbHktZW5yb2xtZW50LXNlcnZpY2UvZmFtaWx5LWVucm9sbWVudC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBMkIsZUFBZSxDQUFDLENBQUE7QUFDM0MscUJBQTJCLE1BQU0sQ0FBQyxDQUFBO0FBRWxDLHNCQUFpRSx1QkFBdUIsQ0FBQyxDQUFBO0FBR3pGO0lBUUksZ0NBQW9CLFdBQTZCO1FBQTdCLGdCQUFXLEdBQVgsV0FBVyxDQUFrQjtRQU56QyxXQUFNLEdBQThCLElBQUksQ0FBQztRQUN6QyxZQUFPLEdBQThCLElBQUksQ0FBQztRQUMxQyxjQUFTLEdBQThCLElBQUksQ0FBQztRQUM1QyxrQkFBYSxHQUE4QixJQUFJLENBQUM7SUFJeEQsQ0FBQztJQUVNLDJDQUFVLEdBQWpCO1FBQUEsaUJBTUM7UUFMRyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQztZQUNyQixNQUFNLENBQUMsaUJBQVUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3RDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVzthQUNsQiw0QkFBNEIsQ0FBQyxXQUFXLENBQUM7YUFDekMsR0FBRyxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsS0FBSSxDQUFDLE1BQU0sR0FBRyxHQUFHLEVBQWpCLENBQWlCLENBQUMsQ0FBQztJQUN2QyxDQUFDO0lBRU0sNENBQVcsR0FBbEI7UUFBQSxpQkFNQztRQUxHLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLEtBQUssSUFBSSxDQUFDO1lBQ3RCLE1BQU0sQ0FBQyxpQkFBVSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDdkMsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXO2FBQ2xCLDRCQUE0QixDQUFDLFFBQVEsQ0FBQzthQUN0QyxHQUFHLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxLQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsRUFBbEIsQ0FBa0IsQ0FBQyxDQUFDO0lBQ3hDLENBQUM7SUFFTSw4Q0FBYSxHQUFwQjtRQUFBLGlCQU1DO1FBTEcsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsS0FBSyxJQUFJLENBQUM7WUFDeEIsTUFBTSxDQUFDLGlCQUFVLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUN6QyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVc7YUFDbEIsNEJBQTRCLENBQUMsVUFBVSxDQUFDO2FBQ3hDLEdBQUcsQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEtBQUksQ0FBQyxTQUFTLEdBQUcsR0FBRyxFQUFwQixDQUFvQixDQUFDLENBQUM7SUFDMUMsQ0FBQztJQUVNLGtEQUFpQixHQUF4QjtRQUFBLGlCQU1DO1FBTEcsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsS0FBSyxJQUFJLENBQUM7WUFDNUIsTUFBTSxDQUFDLGlCQUFVLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUM3QyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVc7YUFDbEIsNEJBQTRCLENBQUMsY0FBYyxDQUFDO2FBQzVDLEdBQUcsQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEtBQUksQ0FBQyxhQUFhLEdBQUcsR0FBRyxFQUF4QixDQUF3QixDQUFDLENBQUM7SUFDOUMsQ0FBQztJQUVNLGtEQUFpQixHQUF4QixVQUF5QixRQUFnQixFQUFFLFFBQWdCO1FBQ3ZELE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLHNDQUFzQyxDQUFDLFFBQVEsRUFBRSxRQUFRLENBQUMsQ0FBQztJQUN2RixDQUFDO0lBOUNMO1FBQUMsaUJBQVUsRUFBRTs7OEJBQUE7SUErQ2IsNkJBQUM7QUFBRCxDQTlDQSxBQThDQyxJQUFBO0FBOUNZLDhCQUFzQix5QkE4Q2xDLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL2NlbnRyZS1wb3J0YWwvZmFtaWx5LWVucm9sbWVudC1mb3JtL2ZhbWlseS1lbnJvbG1lbnQtc2VydmljZS9mYW1pbHktZW5yb2xtZW50LnNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuXHJcbmltcG9ydCB7IEFwaUNsaWVudFNlcnZpY2UsIENvbXBsZXRlRW5yb2xtZW50Rm9ybVZpZXdNb2RlbCB9IGZyb20gJy4uLy4uLy4uLy4uL2FwaS9pbmRleCc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBGYW1pbHlFbnJvbG1lbnRTZXJ2aWNlIHtcclxuXHJcbiAgICBwcml2YXRlIHRpdGxlczogeyBba2V5OiBzdHJpbmddOiBzdHJpbmcgfSA9IG51bGw7XHJcbiAgICBwcml2YXRlIGdlbmRlcnM6IHsgW2tleTogc3RyaW5nXTogc3RyaW5nIH0gPSBudWxsO1xyXG4gICAgcHJpdmF0ZSBjYXJlVHlwZXM6IHsgW2tleTogc3RyaW5nXTogc3RyaW5nIH0gPSBudWxsO1xyXG4gICAgcHJpdmF0ZSByZWxhdGlvbnNoaXBzOiB7IFtrZXk6IHN0cmluZ106IHN0cmluZyB9ID0gbnVsbDtcclxuXHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfYXBpU2VydmljZTogQXBpQ2xpZW50U2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRUaXRsZXMkKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnRpdGxlcyAhPT0gbnVsbClcclxuICAgICAgICAgICAgcmV0dXJuIE9ic2VydmFibGUub2YodGhpcy50aXRsZXMpO1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9hcGlTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5nZW5lcmljc19HZXRFbnVtRGVzY3JpcHRpb25zKCdOYW1lVGl0bGUnKVxyXG4gICAgICAgICAgICAubWFwKHJlcyA9PiB0aGlzLnRpdGxlcyA9IHJlcyk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldEdlbmRlcnMkKCkge1xyXG4gICAgICAgIGlmICh0aGlzLmdlbmRlcnMgIT09IG51bGwpXHJcbiAgICAgICAgICAgIHJldHVybiBPYnNlcnZhYmxlLm9mKHRoaXMuZ2VuZGVycyk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2FwaVNlcnZpY2VcclxuICAgICAgICAgICAgLmdlbmVyaWNzX0dldEVudW1EZXNjcmlwdGlvbnMoJ0dlbmRlcicpXHJcbiAgICAgICAgICAgIC5tYXAocmVzID0+IHRoaXMuZ2VuZGVycyA9IHJlcyk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldENhcmVUeXBlcyQoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuY2FyZVR5cGVzICE9PSBudWxsKVxyXG4gICAgICAgICAgICByZXR1cm4gT2JzZXJ2YWJsZS5vZih0aGlzLmNhcmVUeXBlcyk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2FwaVNlcnZpY2VcclxuICAgICAgICAgICAgLmdlbmVyaWNzX0dldEVudW1EZXNjcmlwdGlvbnMoJ0NhcmVUeXBlJylcclxuICAgICAgICAgICAgLm1hcChyZXMgPT4gdGhpcy5jYXJlVHlwZXMgPSByZXMpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRSZWxhdGlvbnNoaXBzJCgpIHtcclxuICAgICAgICBpZiAodGhpcy5yZWxhdGlvbnNoaXBzICE9PSBudWxsKVxyXG4gICAgICAgICAgICByZXR1cm4gT2JzZXJ2YWJsZS5vZih0aGlzLnJlbGF0aW9uc2hpcHMpO1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9hcGlTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5nZW5lcmljc19HZXRFbnVtRGVzY3JpcHRpb25zKCdSZWxhdGlvbnNoaXAnKVxyXG4gICAgICAgICAgICAubWFwKHJlcyA9PiB0aGlzLnJlbGF0aW9uc2hpcHMgPSByZXMpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRFbnJvbG1lbnRGb3JtJChwYXJlbnRJZDogc3RyaW5nLCBjZW50cmVJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxDb21wbGV0ZUVucm9sbWVudEZvcm1WaWV3TW9kZWw+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fYXBpU2VydmljZS5lbnJvbG1lbnRGb3JtX0dldENvbXBsZXRlRW5yb2xtZW50Rm9ybShwYXJlbnRJZCwgY2VudHJlSWQpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
