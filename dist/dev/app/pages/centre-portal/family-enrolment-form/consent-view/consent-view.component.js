"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../../api/index');
var ConsentViewComponent = (function () {
    function ConsentViewComponent() {
        ;
    }
    ConsentViewComponent.prototype.ngOnInit = function () { ; };
    __decorate([
        core_1.Input('consent'), 
        __metadata('design:type', index_1.EnrolmentFormDto)
    ], ConsentViewComponent.prototype, "consent", void 0);
    ConsentViewComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'consent-view-cmp',
            templateUrl: 'consent-view.component.html'
        }), 
        __metadata('design:paramtypes', [])
    ], ConsentViewComponent);
    return ConsentViewComponent;
}());
exports.ConsentViewComponent = ConsentViewComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2ZhbWlseS1lbnJvbG1lbnQtZm9ybS9jb25zZW50LXZpZXcvY29uc2VudC12aWV3LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQXlDLGVBQWUsQ0FBQyxDQUFBO0FBQ3pELHNCQUFpQyx1QkFBdUIsQ0FBQyxDQUFBO0FBUXpEO0lBR0k7UUFBZ0IsQ0FBQztJQUFDLENBQUM7SUFFbkIsdUNBQVEsR0FBUixjQUFtQixDQUFDLENBQUMsQ0FBQztJQUh0QjtRQUFDLFlBQUssQ0FBQyxTQUFTLENBQUM7O3lEQUFBO0lBUnJCO1FBQUMsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsa0JBQWtCO1lBQzVCLFdBQVcsRUFBRSw2QkFBNkI7U0FDN0MsQ0FBQzs7NEJBQUE7SUFRRiwyQkFBQztBQUFELENBTkEsQUFNQyxJQUFBO0FBTlksNEJBQW9CLHVCQU1oQyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2ZhbWlseS1lbnJvbG1lbnQtZm9ybS9jb25zZW50LXZpZXcvY29uc2VudC12aWV3LmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBFbnJvbG1lbnRGb3JtRHRvIH0gZnJvbSAnLi4vLi4vLi4vLi4vYXBpL2luZGV4JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHNlbGVjdG9yOiAnY29uc2VudC12aWV3LWNtcCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJ2NvbnNlbnQtdmlldy5jb21wb25lbnQuaHRtbCdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBDb25zZW50Vmlld0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgQElucHV0KCdjb25zZW50JykgY29uc2VudDogRW5yb2xtZW50Rm9ybUR0bztcclxuICAgIGNvbnN0cnVjdG9yKCkgeyA7IH1cclxuXHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkIHsgOyB9XHJcbn1cclxuIl19
