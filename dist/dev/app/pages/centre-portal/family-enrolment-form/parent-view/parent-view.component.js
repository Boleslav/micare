"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var rxjs_1 = require('rxjs');
var index_1 = require('../../../../api/index');
var index_2 = require('../index');
var ParentViewComponent = (function () {
    function ParentViewComponent(_enrolmentService) {
        this._enrolmentService = _enrolmentService;
        this.titles = {};
        this.genders = {};
        ;
    }
    ParentViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        rxjs_1.Observable.forkJoin(this._enrolmentService.getTitles$(), this._enrolmentService.getGenders$())
            .subscribe(function (res) {
            _this.titles = res[0];
            _this.genders = res[1];
        });
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', index_1.ParentDto)
    ], ParentViewComponent.prototype, "parent", void 0);
    ParentViewComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'parent-view-cmp',
            templateUrl: 'parent-view.component.html'
        }), 
        __metadata('design:paramtypes', [index_2.FamilyEnrolmentService])
    ], ParentViewComponent);
    return ParentViewComponent;
}());
exports.ParentViewComponent = ParentViewComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2ZhbWlseS1lbnJvbG1lbnQtZm9ybS9wYXJlbnQtdmlldy9wYXJlbnQtdmlldy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUF5QyxlQUFlLENBQUMsQ0FBQTtBQUN6RCxxQkFBMkIsTUFBTSxDQUFDLENBQUE7QUFDbEMsc0JBQTBCLHVCQUF1QixDQUFDLENBQUE7QUFDbEQsc0JBQXVDLFVBQVUsQ0FBQyxDQUFBO0FBUWxEO0lBTUksNkJBQW9CLGlCQUF5QztRQUF6QyxzQkFBaUIsR0FBakIsaUJBQWlCLENBQXdCO1FBSHJELFdBQU0sR0FBOEIsRUFBRSxDQUFDO1FBQ3ZDLFlBQU8sR0FBOEIsRUFBRSxDQUFDO1FBRWlCLENBQUM7SUFBQyxDQUFDO0lBRXBFLHNDQUFRLEdBQVI7UUFBQSxpQkFRQztRQVBHLGlCQUFVLENBQUMsUUFBUSxDQUNmLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLEVBQUUsRUFDbkMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFdBQVcsRUFBRSxDQUFDO2FBQ3BDLFNBQVMsQ0FBQyxVQUFBLEdBQUc7WUFDVixLQUFJLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNyQixLQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUMxQixDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFkRDtRQUFDLFlBQUssRUFBRTs7dURBQUE7SUFSWjtRQUFDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLGlCQUFpQjtZQUMzQixXQUFXLEVBQUUsNEJBQTRCO1NBQzVDLENBQUM7OzJCQUFBO0lBbUJGLDBCQUFDO0FBQUQsQ0FqQkEsQUFpQkMsSUFBQTtBQWpCWSwyQkFBbUIsc0JBaUIvQixDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2ZhbWlseS1lbnJvbG1lbnQtZm9ybS9wYXJlbnQtdmlldy9wYXJlbnQtdmlldy5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBQYXJlbnREdG8gfSBmcm9tICcuLi8uLi8uLi8uLi9hcGkvaW5kZXgnO1xyXG5pbXBvcnQgeyBGYW1pbHlFbnJvbG1lbnRTZXJ2aWNlIH0gZnJvbSAnLi4vaW5kZXgnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgc2VsZWN0b3I6ICdwYXJlbnQtdmlldy1jbXAnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICdwYXJlbnQtdmlldy5jb21wb25lbnQuaHRtbCdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBQYXJlbnRWaWV3Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBASW5wdXQoKSBwYXJlbnQ6IFBhcmVudER0bztcclxuICAgIHByaXZhdGUgdGl0bGVzOiB7IFtrZXk6IHN0cmluZ106IHN0cmluZyB9ID0ge307XHJcbiAgICBwcml2YXRlIGdlbmRlcnM6IHsgW2tleTogc3RyaW5nXTogc3RyaW5nIH0gPSB7fTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9lbnJvbG1lbnRTZXJ2aWNlOiBGYW1pbHlFbnJvbG1lbnRTZXJ2aWNlKSB7IDsgfVxyXG5cclxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgICAgIE9ic2VydmFibGUuZm9ya0pvaW4oXHJcbiAgICAgICAgICAgIHRoaXMuX2Vucm9sbWVudFNlcnZpY2UuZ2V0VGl0bGVzJCgpLFxyXG4gICAgICAgICAgICB0aGlzLl9lbnJvbG1lbnRTZXJ2aWNlLmdldEdlbmRlcnMkKCkpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMudGl0bGVzID0gcmVzWzBdO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5nZW5kZXJzID0gcmVzWzFdO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG4iXX0=
