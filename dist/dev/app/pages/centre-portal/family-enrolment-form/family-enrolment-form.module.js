"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../shared/index');
var index_2 = require('./index');
var index_3 = require('./parent-view/index');
var index_4 = require('./child-view/index');
var index_5 = require('./guardian-view/index');
var index_6 = require('./contact-view/index');
var index_7 = require('./consent-view/index');
var FamilyEnrolmentFormModule = (function () {
    function FamilyEnrolmentFormModule() {
    }
    FamilyEnrolmentFormModule = __decorate([
        core_1.NgModule({
            imports: [index_1.SharedModule],
            declarations: [
                index_2.FamilyEnrolmentFormComponent,
                index_3.ParentViewComponent,
                index_4.ChildViewComponent,
                index_4.ChildDetailsViewComponent,
                index_4.ChildDesiredDaysViewComponent,
                index_4.ChildHealthInfoViewComponent,
                index_4.ChildCustodyInfoViewComponent,
                index_5.GuardianViewComponent,
                index_6.ContactViewComponent,
                index_7.ConsentViewComponent
            ],
            providers: [index_2.FamilyEnrolmentService]
        }), 
        __metadata('design:paramtypes', [])
    ], FamilyEnrolmentFormModule);
    return FamilyEnrolmentFormModule;
}());
exports.FamilyEnrolmentFormModule = FamilyEnrolmentFormModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2ZhbWlseS1lbnJvbG1lbnQtZm9ybS9mYW1pbHktZW5yb2xtZW50LWZvcm0ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBeUIsZUFBZSxDQUFDLENBQUE7QUFDekMsc0JBQTZCLHVCQUF1QixDQUFDLENBQUE7QUFFckQsc0JBQXFFLFNBQVMsQ0FBQyxDQUFBO0FBQy9FLHNCQUFvQyxxQkFBcUIsQ0FBQyxDQUFBO0FBQzFELHNCQUlPLG9CQUFvQixDQUFDLENBQUE7QUFDNUIsc0JBQXNDLHVCQUF1QixDQUFDLENBQUE7QUFDOUQsc0JBQXFDLHNCQUFzQixDQUFDLENBQUE7QUFDNUQsc0JBQXFDLHNCQUFzQixDQUFDLENBQUE7QUFtQjVEO0lBQUE7SUFBeUMsQ0FBQztJQWpCMUM7UUFBQyxlQUFRLENBQUM7WUFDTixPQUFPLEVBQUUsQ0FBQyxvQkFBWSxDQUFDO1lBQ3ZCLFlBQVksRUFBRTtnQkFDVixvQ0FBNEI7Z0JBQzVCLDJCQUFtQjtnQkFDbkIsMEJBQWtCO2dCQUNsQixpQ0FBeUI7Z0JBQ3pCLHFDQUE2QjtnQkFDN0Isb0NBQTRCO2dCQUM1QixxQ0FBNkI7Z0JBQzdCLDZCQUFxQjtnQkFDckIsNEJBQW9CO2dCQUNwQiw0QkFBb0I7YUFDdkI7WUFDRCxTQUFTLEVBQUUsQ0FBQyw4QkFBc0IsQ0FBQztTQUN0QyxDQUFDOztpQ0FBQTtJQUV1QyxnQ0FBQztBQUFELENBQXpDLEFBQTBDLElBQUE7QUFBN0IsaUNBQXlCLDRCQUFJLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL2NlbnRyZS1wb3J0YWwvZmFtaWx5LWVucm9sbWVudC1mb3JtL2ZhbWlseS1lbnJvbG1lbnQtZm9ybS5tb2R1bGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTaGFyZWRNb2R1bGUgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvaW5kZXgnO1xyXG5cclxuaW1wb3J0IHsgRmFtaWx5RW5yb2xtZW50Rm9ybUNvbXBvbmVudCwgRmFtaWx5RW5yb2xtZW50U2VydmljZSB9IGZyb20gJy4vaW5kZXgnO1xyXG5pbXBvcnQgeyBQYXJlbnRWaWV3Q29tcG9uZW50IH0gZnJvbSAnLi9wYXJlbnQtdmlldy9pbmRleCc7XHJcbmltcG9ydCB7XHJcbiAgICBDaGlsZFZpZXdDb21wb25lbnQsIENoaWxkRGV0YWlsc1ZpZXdDb21wb25lbnQsXHJcbiAgICBDaGlsZERlc2lyZWREYXlzVmlld0NvbXBvbmVudCxcclxuICAgIENoaWxkSGVhbHRoSW5mb1ZpZXdDb21wb25lbnQsIENoaWxkQ3VzdG9keUluZm9WaWV3Q29tcG9uZW50XHJcbn0gZnJvbSAnLi9jaGlsZC12aWV3L2luZGV4JztcclxuaW1wb3J0IHsgR3VhcmRpYW5WaWV3Q29tcG9uZW50IH0gZnJvbSAnLi9ndWFyZGlhbi12aWV3L2luZGV4JztcclxuaW1wb3J0IHsgQ29udGFjdFZpZXdDb21wb25lbnQgfSBmcm9tICcuL2NvbnRhY3Qtdmlldy9pbmRleCc7XHJcbmltcG9ydCB7IENvbnNlbnRWaWV3Q29tcG9uZW50IH0gZnJvbSAnLi9jb25zZW50LXZpZXcvaW5kZXgnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6IFtTaGFyZWRNb2R1bGVdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICAgICAgRmFtaWx5RW5yb2xtZW50Rm9ybUNvbXBvbmVudCxcclxuICAgICAgICBQYXJlbnRWaWV3Q29tcG9uZW50LFxyXG4gICAgICAgIENoaWxkVmlld0NvbXBvbmVudCxcclxuICAgICAgICBDaGlsZERldGFpbHNWaWV3Q29tcG9uZW50LFxyXG4gICAgICAgIENoaWxkRGVzaXJlZERheXNWaWV3Q29tcG9uZW50LFxyXG4gICAgICAgIENoaWxkSGVhbHRoSW5mb1ZpZXdDb21wb25lbnQsXHJcbiAgICAgICAgQ2hpbGRDdXN0b2R5SW5mb1ZpZXdDb21wb25lbnQsXHJcbiAgICAgICAgR3VhcmRpYW5WaWV3Q29tcG9uZW50LFxyXG4gICAgICAgIENvbnRhY3RWaWV3Q29tcG9uZW50LFxyXG4gICAgICAgIENvbnNlbnRWaWV3Q29tcG9uZW50XHJcbiAgICBdLFxyXG4gICAgcHJvdmlkZXJzOiBbRmFtaWx5RW5yb2xtZW50U2VydmljZV1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBGYW1pbHlFbnJvbG1lbnRGb3JtTW9kdWxlIHsgfVxyXG4iXX0=
