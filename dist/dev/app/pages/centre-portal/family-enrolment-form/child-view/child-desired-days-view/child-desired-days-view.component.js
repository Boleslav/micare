"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../index');
var ChildDesiredDaysViewComponent = (function () {
    function ChildDesiredDaysViewComponent(_enrolmentService) {
        this._enrolmentService = _enrolmentService;
        this.careTypes = {};
        ;
    }
    ChildDesiredDaysViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._enrolmentService.getCareTypes$()
            .subscribe(function (res) { return _this.careTypes = res; });
    };
    __decorate([
        core_1.Input('desiredDays'), 
        __metadata('design:type', Array)
    ], ChildDesiredDaysViewComponent.prototype, "desiredDays", void 0);
    ChildDesiredDaysViewComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'child-desired-days-view-cmp',
            templateUrl: 'child-desired-days-view.component.html'
        }), 
        __metadata('design:paramtypes', [index_1.FamilyEnrolmentService])
    ], ChildDesiredDaysViewComponent);
    return ChildDesiredDaysViewComponent;
}());
exports.ChildDesiredDaysViewComponent = ChildDesiredDaysViewComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2ZhbWlseS1lbnJvbG1lbnQtZm9ybS9jaGlsZC12aWV3L2NoaWxkLWRlc2lyZWQtZGF5cy12aWV3L2NoaWxkLWRlc2lyZWQtZGF5cy12aWV3LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQXlDLGVBQWUsQ0FBQyxDQUFBO0FBRXpELHNCQUF1QyxhQUFhLENBQUMsQ0FBQTtBQVFyRDtJQUlJLHVDQUFvQixpQkFBeUM7UUFBekMsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUF3QjtRQURyRCxjQUFTLEdBQThCLEVBQUUsQ0FBQztRQUNlLENBQUM7SUFBQyxDQUFDO0lBRXBFLGdEQUFRLEdBQVI7UUFBQSxpQkFHQztRQUZHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLEVBQUU7YUFDakMsU0FBUyxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsS0FBSSxDQUFDLFNBQVMsR0FBRyxHQUFHLEVBQXBCLENBQW9CLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBUEQ7UUFBQyxZQUFLLENBQUMsYUFBYSxDQUFDOztzRUFBQTtJQVJ6QjtRQUFDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLDZCQUE2QjtZQUN2QyxXQUFXLEVBQUUsd0NBQXdDO1NBQ3hELENBQUM7O3FDQUFBO0lBWUYsb0NBQUM7QUFBRCxDQVZBLEFBVUMsSUFBQTtBQVZZLHFDQUE2QixnQ0FVekMsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9mYW1pbHktZW5yb2xtZW50LWZvcm0vY2hpbGQtdmlldy9jaGlsZC1kZXNpcmVkLWRheXMtdmlldy9jaGlsZC1kZXNpcmVkLWRheXMtdmlldy5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRW5yb2xtZW50Rm9ybURlc2lyZWREYXlzRHRvIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vYXBpL2luZGV4JztcclxuaW1wb3J0IHsgRmFtaWx5RW5yb2xtZW50U2VydmljZSB9IGZyb20gJy4uLy4uL2luZGV4JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHNlbGVjdG9yOiAnY2hpbGQtZGVzaXJlZC1kYXlzLXZpZXctY21wJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnY2hpbGQtZGVzaXJlZC1kYXlzLXZpZXcuY29tcG9uZW50Lmh0bWwnXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgQ2hpbGREZXNpcmVkRGF5c1ZpZXdDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIEBJbnB1dCgnZGVzaXJlZERheXMnKSBkZXNpcmVkRGF5czogRW5yb2xtZW50Rm9ybURlc2lyZWREYXlzRHRvW107XHJcbiAgICBwcml2YXRlIGNhcmVUeXBlczogeyBba2V5OiBzdHJpbmddOiBzdHJpbmcgfSA9IHt9O1xyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfZW5yb2xtZW50U2VydmljZTogRmFtaWx5RW5yb2xtZW50U2VydmljZSkgeyA7IH1cclxuXHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLl9lbnJvbG1lbnRTZXJ2aWNlLmdldENhcmVUeXBlcyQoKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKHJlcyA9PiB0aGlzLmNhcmVUeXBlcyA9IHJlcyk7XHJcbiAgICB9XHJcbn1cclxuIl19
