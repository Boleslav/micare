"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../../api/index');
var ChildViewComponent = (function () {
    function ChildViewComponent() {
        ;
    }
    ChildViewComponent.prototype.ngOnInit = function () { ; };
    __decorate([
        core_1.Input('child'), 
        __metadata('design:type', index_1.ChildEnrolmentFormViewModel)
    ], ChildViewComponent.prototype, "child", void 0);
    ChildViewComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'child-view-cmp',
            templateUrl: 'child-view.component.html'
        }), 
        __metadata('design:paramtypes', [])
    ], ChildViewComponent);
    return ChildViewComponent;
}());
exports.ChildViewComponent = ChildViewComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2ZhbWlseS1lbnJvbG1lbnQtZm9ybS9jaGlsZC12aWV3L2NoaWxkLXZpZXcuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBeUMsZUFBZSxDQUFDLENBQUE7QUFDekQsc0JBQTRDLHVCQUF1QixDQUFDLENBQUE7QUFRcEU7SUFHSTtRQUFnQixDQUFDO0lBQUMsQ0FBQztJQUVuQixxQ0FBUSxHQUFSLGNBQW1CLENBQUMsQ0FBQyxDQUFDO0lBSHRCO1FBQUMsWUFBSyxDQUFDLE9BQU8sQ0FBQzs7cURBQUE7SUFSbkI7UUFBQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxnQkFBZ0I7WUFDMUIsV0FBVyxFQUFFLDJCQUEyQjtTQUMzQyxDQUFDOzswQkFBQTtJQVFGLHlCQUFDO0FBQUQsQ0FOQSxBQU1DLElBQUE7QUFOWSwwQkFBa0IscUJBTTlCLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL2NlbnRyZS1wb3J0YWwvZmFtaWx5LWVucm9sbWVudC1mb3JtL2NoaWxkLXZpZXcvY2hpbGQtdmlldy5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ2hpbGRFbnJvbG1lbnRGb3JtVmlld01vZGVsIH0gZnJvbSAnLi4vLi4vLi4vLi4vYXBpL2luZGV4JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHNlbGVjdG9yOiAnY2hpbGQtdmlldy1jbXAnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICdjaGlsZC12aWV3LmNvbXBvbmVudC5odG1sJ1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIENoaWxkVmlld0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgQElucHV0KCdjaGlsZCcpIGNoaWxkOiBDaGlsZEVucm9sbWVudEZvcm1WaWV3TW9kZWw7XHJcbiAgICBjb25zdHJ1Y3RvcigpIHsgOyB9XHJcblxyXG4gICAgbmdPbkluaXQoKTogdm9pZCB7IDsgfVxyXG59XHJcbiJdfQ==
