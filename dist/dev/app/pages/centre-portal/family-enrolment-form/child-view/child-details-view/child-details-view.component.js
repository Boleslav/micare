"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../index');
var index_2 = require('../../../../../api/index');
var ChildDetailsViewComponent = (function () {
    function ChildDetailsViewComponent(_enrolmentService) {
        this._enrolmentService = _enrolmentService;
        this.genders = {};
        ;
    }
    ChildDetailsViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._enrolmentService
            .getGenders$()
            .subscribe(function (res) { return _this.genders = res; });
    };
    __decorate([
        core_1.Input('child'), 
        __metadata('design:type', index_2.KidDto)
    ], ChildDetailsViewComponent.prototype, "child", void 0);
    ChildDetailsViewComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'child-details-view-cmp',
            templateUrl: 'child-details-view.component.html'
        }), 
        __metadata('design:paramtypes', [index_1.FamilyEnrolmentService])
    ], ChildDetailsViewComponent);
    return ChildDetailsViewComponent;
}());
exports.ChildDetailsViewComponent = ChildDetailsViewComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2ZhbWlseS1lbnJvbG1lbnQtZm9ybS9jaGlsZC12aWV3L2NoaWxkLWRldGFpbHMtdmlldy9jaGlsZC1kZXRhaWxzLXZpZXcuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBeUMsZUFBZSxDQUFDLENBQUE7QUFDekQsc0JBQXVDLGFBQWEsQ0FBQyxDQUFBO0FBQ3JELHNCQUF1QiwwQkFBMEIsQ0FBQyxDQUFBO0FBUWxEO0lBSUksbUNBQW9CLGlCQUF5QztRQUF6QyxzQkFBaUIsR0FBakIsaUJBQWlCLENBQXdCO1FBRHJELFlBQU8sR0FBOEIsRUFBRSxDQUFDO1FBQ2lCLENBQUM7SUFBQyxDQUFDO0lBRXBFLDRDQUFRLEdBQVI7UUFBQSxpQkFJQztRQUhHLElBQUksQ0FBQyxpQkFBaUI7YUFDakIsV0FBVyxFQUFFO2FBQ2IsU0FBUyxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsS0FBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLEVBQWxCLENBQWtCLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBUkQ7UUFBQyxZQUFLLENBQUMsT0FBTyxDQUFDOzs0REFBQTtJQVJuQjtRQUFDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLHdCQUF3QjtZQUNsQyxXQUFXLEVBQUUsbUNBQW1DO1NBQ25ELENBQUM7O2lDQUFBO0lBYUYsZ0NBQUM7QUFBRCxDQVhBLEFBV0MsSUFBQTtBQVhZLGlDQUF5Qiw0QkFXckMsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9mYW1pbHktZW5yb2xtZW50LWZvcm0vY2hpbGQtdmlldy9jaGlsZC1kZXRhaWxzLXZpZXcvY2hpbGQtZGV0YWlscy12aWV3LmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGYW1pbHlFbnJvbG1lbnRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vaW5kZXgnO1xyXG5pbXBvcnQgeyBLaWREdG8gfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9hcGkvaW5kZXgnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgc2VsZWN0b3I6ICdjaGlsZC1kZXRhaWxzLXZpZXctY21wJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnY2hpbGQtZGV0YWlscy12aWV3LmNvbXBvbmVudC5odG1sJ1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIENoaWxkRGV0YWlsc1ZpZXdDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIEBJbnB1dCgnY2hpbGQnKSBjaGlsZDogS2lkRHRvO1xyXG4gICAgcHJpdmF0ZSBnZW5kZXJzOiB7IFtrZXk6IHN0cmluZ106IHN0cmluZyB9ID0ge307XHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9lbnJvbG1lbnRTZXJ2aWNlOiBGYW1pbHlFbnJvbG1lbnRTZXJ2aWNlKSB7IDsgfVxyXG5cclxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuX2Vucm9sbWVudFNlcnZpY2VcclxuICAgICAgICAgICAgLmdldEdlbmRlcnMkKClcclxuICAgICAgICAgICAgLnN1YnNjcmliZShyZXMgPT4gdGhpcy5nZW5kZXJzID0gcmVzKTtcclxuICAgIH1cclxufVxyXG4iXX0=
