"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../../../api/index');
var ChildCustodyInfoViewComponent = (function () {
    function ChildCustodyInfoViewComponent() {
        ;
    }
    ChildCustodyInfoViewComponent.prototype.ngOnInit = function () { ; };
    __decorate([
        core_1.Input('custodyInfo'), 
        __metadata('design:type', index_1.ChildCustodyInformationDto)
    ], ChildCustodyInfoViewComponent.prototype, "custodyInfo", void 0);
    ChildCustodyInfoViewComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'child-custody-info-view-cmp',
            templateUrl: 'child-custody-info-view.component.html'
        }), 
        __metadata('design:paramtypes', [])
    ], ChildCustodyInfoViewComponent);
    return ChildCustodyInfoViewComponent;
}());
exports.ChildCustodyInfoViewComponent = ChildCustodyInfoViewComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2ZhbWlseS1lbnJvbG1lbnQtZm9ybS9jaGlsZC12aWV3L2NoaWxkLWN1c3RvZHktaW5mby12aWV3L2NoaWxkLWN1c3RvZHktaW5mby12aWV3LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQXlDLGVBQWUsQ0FBQyxDQUFBO0FBQ3pELHNCQUEyQywwQkFBMEIsQ0FBQyxDQUFBO0FBUXRFO0lBR0k7UUFBZ0IsQ0FBQztJQUFDLENBQUM7SUFFbkIsZ0RBQVEsR0FBUixjQUFtQixDQUFDLENBQUMsQ0FBQztJQUh0QjtRQUFDLFlBQUssQ0FBQyxhQUFhLENBQUM7O3NFQUFBO0lBUnpCO1FBQUMsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsNkJBQTZCO1lBQ3ZDLFdBQVcsRUFBRSx3Q0FBd0M7U0FDeEQsQ0FBQzs7cUNBQUE7SUFRRixvQ0FBQztBQUFELENBTkEsQUFNQyxJQUFBO0FBTlkscUNBQTZCLGdDQU16QyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2ZhbWlseS1lbnJvbG1lbnQtZm9ybS9jaGlsZC12aWV3L2NoaWxkLWN1c3RvZHktaW5mby12aWV3L2NoaWxkLWN1c3RvZHktaW5mby12aWV3LmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDaGlsZEN1c3RvZHlJbmZvcm1hdGlvbkR0byB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL2FwaS9pbmRleCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICBzZWxlY3RvcjogJ2NoaWxkLWN1c3RvZHktaW5mby12aWV3LWNtcCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJ2NoaWxkLWN1c3RvZHktaW5mby12aWV3LmNvbXBvbmVudC5odG1sJ1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIENoaWxkQ3VzdG9keUluZm9WaWV3Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBASW5wdXQoJ2N1c3RvZHlJbmZvJykgY3VzdG9keUluZm86IENoaWxkQ3VzdG9keUluZm9ybWF0aW9uRHRvO1xyXG4gICAgY29uc3RydWN0b3IoKSB7IDsgfVxyXG5cclxuICAgIG5nT25Jbml0KCk6IHZvaWQgeyA7IH1cclxufVxyXG4iXX0=
