"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../../../api/index');
var ChildHealthInfoViewComponent = (function () {
    function ChildHealthInfoViewComponent() {
        ;
    }
    ChildHealthInfoViewComponent.prototype.ngOnInit = function () { ; };
    __decorate([
        core_1.Input('healthInfo'), 
        __metadata('design:type', index_1.KidHealthInformationDto)
    ], ChildHealthInfoViewComponent.prototype, "healthInfo", void 0);
    ChildHealthInfoViewComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'child-health-info-view-cmp',
            templateUrl: 'child-health-info-view.component.html'
        }), 
        __metadata('design:paramtypes', [])
    ], ChildHealthInfoViewComponent);
    return ChildHealthInfoViewComponent;
}());
exports.ChildHealthInfoViewComponent = ChildHealthInfoViewComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2ZhbWlseS1lbnJvbG1lbnQtZm9ybS9jaGlsZC12aWV3L2NoaWxkLWhlYWx0aC1pbmZvLXZpZXcvY2hpbGQtaGVhbHRoLWluZm8tdmlldy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUF5QyxlQUFlLENBQUMsQ0FBQTtBQUN6RCxzQkFBd0MsMEJBQTBCLENBQUMsQ0FBQTtBQVFuRTtJQUdJO1FBQWdCLENBQUM7SUFBQyxDQUFDO0lBRW5CLCtDQUFRLEdBQVIsY0FBbUIsQ0FBQyxDQUFDLENBQUM7SUFIdEI7UUFBQyxZQUFLLENBQUMsWUFBWSxDQUFDOztvRUFBQTtJQVJ4QjtRQUFDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLDRCQUE0QjtZQUN0QyxXQUFXLEVBQUUsdUNBQXVDO1NBQ3ZELENBQUM7O29DQUFBO0lBUUYsbUNBQUM7QUFBRCxDQU5BLEFBTUMsSUFBQTtBQU5ZLG9DQUE0QiwrQkFNeEMsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9mYW1pbHktZW5yb2xtZW50LWZvcm0vY2hpbGQtdmlldy9jaGlsZC1oZWFsdGgtaW5mby12aWV3L2NoaWxkLWhlYWx0aC1pbmZvLXZpZXcuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEtpZEhlYWx0aEluZm9ybWF0aW9uRHRvIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vYXBpL2luZGV4JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHNlbGVjdG9yOiAnY2hpbGQtaGVhbHRoLWluZm8tdmlldy1jbXAnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICdjaGlsZC1oZWFsdGgtaW5mby12aWV3LmNvbXBvbmVudC5odG1sJ1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIENoaWxkSGVhbHRoSW5mb1ZpZXdDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIEBJbnB1dCgnaGVhbHRoSW5mbycpIGhlYWx0aEluZm86IEtpZEhlYWx0aEluZm9ybWF0aW9uRHRvO1xyXG4gICAgY29uc3RydWN0b3IoKSB7IDsgfVxyXG5cclxuICAgIG5nT25Jbml0KCk6IHZvaWQgeyA7IH1cclxufVxyXG4iXX0=
