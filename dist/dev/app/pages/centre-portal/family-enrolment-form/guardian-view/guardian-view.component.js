"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var rxjs_1 = require('rxjs');
var index_1 = require('../../../../api/index');
var index_2 = require('../index');
var GuardianViewComponent = (function () {
    function GuardianViewComponent(_enrolmentService) {
        this._enrolmentService = _enrolmentService;
        this.titles = {};
        this.genders = {};
        this.relationships = {};
        ;
    }
    GuardianViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        rxjs_1.Observable.forkJoin(this._enrolmentService.getTitles$(), this._enrolmentService.getGenders$(), this._enrolmentService.getRelationships$())
            .subscribe(function (res) {
            _this.titles = res[0];
            _this.genders = res[1];
            _this.relationships = res[2];
        });
    };
    __decorate([
        core_1.Input('guardian'), 
        __metadata('design:type', index_1.GuardianDto)
    ], GuardianViewComponent.prototype, "guardian", void 0);
    GuardianViewComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'guardian-view-cmp',
            templateUrl: 'guardian-view.component.html'
        }), 
        __metadata('design:paramtypes', [index_2.FamilyEnrolmentService])
    ], GuardianViewComponent);
    return GuardianViewComponent;
}());
exports.GuardianViewComponent = GuardianViewComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2ZhbWlseS1lbnJvbG1lbnQtZm9ybS9ndWFyZGlhbi12aWV3L2d1YXJkaWFuLXZpZXcuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBeUMsZUFBZSxDQUFDLENBQUE7QUFDekQscUJBQTJCLE1BQU0sQ0FBQyxDQUFBO0FBQ2xDLHNCQUE0Qix1QkFBdUIsQ0FBQyxDQUFBO0FBQ3BELHNCQUF1QyxVQUFVLENBQUMsQ0FBQTtBQVFsRDtJQU9JLCtCQUFvQixpQkFBeUM7UUFBekMsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUF3QjtRQUpyRCxXQUFNLEdBQThCLEVBQUUsQ0FBQztRQUN2QyxZQUFPLEdBQThCLEVBQUUsQ0FBQztRQUN4QyxrQkFBYSxHQUE4QixFQUFFLENBQUM7UUFFVyxDQUFDO0lBQUMsQ0FBQztJQUVwRSx3Q0FBUSxHQUFSO1FBQUEsaUJBVUM7UUFURyxpQkFBVSxDQUFDLFFBQVEsQ0FDZixJQUFJLENBQUMsaUJBQWlCLENBQUMsVUFBVSxFQUFFLEVBQ25DLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLEVBQUUsRUFDcEMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGlCQUFpQixFQUFFLENBQUM7YUFDMUMsU0FBUyxDQUFDLFVBQUEsR0FBRztZQUNWLEtBQUksQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3JCLEtBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3RCLEtBQUksQ0FBQyxhQUFhLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2hDLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQWpCRDtRQUFDLFlBQUssQ0FBQyxVQUFVLENBQUM7OzJEQUFBO0lBUnRCO1FBQUMsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsbUJBQW1CO1lBQzdCLFdBQVcsRUFBRSw4QkFBOEI7U0FDOUMsQ0FBQzs7NkJBQUE7SUFzQkYsNEJBQUM7QUFBRCxDQXBCQSxBQW9CQyxJQUFBO0FBcEJZLDZCQUFxQix3QkFvQmpDLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL2NlbnRyZS1wb3J0YWwvZmFtaWx5LWVucm9sbWVudC1mb3JtL2d1YXJkaWFuLXZpZXcvZ3VhcmRpYW4tdmlldy5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBHdWFyZGlhbkR0byB9IGZyb20gJy4uLy4uLy4uLy4uL2FwaS9pbmRleCc7XHJcbmltcG9ydCB7IEZhbWlseUVucm9sbWVudFNlcnZpY2UgfSBmcm9tICcuLi9pbmRleCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICBzZWxlY3RvcjogJ2d1YXJkaWFuLXZpZXctY21wJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnZ3VhcmRpYW4tdmlldy5jb21wb25lbnQuaHRtbCdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBHdWFyZGlhblZpZXdDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIEBJbnB1dCgnZ3VhcmRpYW4nKSBndWFyZGlhbjogR3VhcmRpYW5EdG87XHJcbiAgICBwcml2YXRlIHRpdGxlczogeyBba2V5OiBzdHJpbmddOiBzdHJpbmcgfSA9IHt9O1xyXG4gICAgcHJpdmF0ZSBnZW5kZXJzOiB7IFtrZXk6IHN0cmluZ106IHN0cmluZyB9ID0ge307XHJcbiAgICBwcml2YXRlIHJlbGF0aW9uc2hpcHM6IHsgW2tleTogc3RyaW5nXTogc3RyaW5nIH0gPSB7fTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9lbnJvbG1lbnRTZXJ2aWNlOiBGYW1pbHlFbnJvbG1lbnRTZXJ2aWNlKSB7IDsgfVxyXG5cclxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgICAgIE9ic2VydmFibGUuZm9ya0pvaW4oXHJcbiAgICAgICAgICAgIHRoaXMuX2Vucm9sbWVudFNlcnZpY2UuZ2V0VGl0bGVzJCgpLFxyXG4gICAgICAgICAgICB0aGlzLl9lbnJvbG1lbnRTZXJ2aWNlLmdldEdlbmRlcnMkKCksXHJcbiAgICAgICAgICAgIHRoaXMuX2Vucm9sbWVudFNlcnZpY2UuZ2V0UmVsYXRpb25zaGlwcyQoKSlcclxuICAgICAgICAgICAgLnN1YnNjcmliZShyZXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50aXRsZXMgPSByZXNbMF07XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdlbmRlcnMgPSByZXNbMV07XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJlbGF0aW9uc2hpcHMgPSByZXNbMl07XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
