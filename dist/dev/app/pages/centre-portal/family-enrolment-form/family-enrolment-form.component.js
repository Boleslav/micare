"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var rxjs_1 = require('rxjs');
var index_1 = require('../../../core/index');
var index_2 = require('./index');
var FamilyEnrolmentFormComponent = (function () {
    function FamilyEnrolmentFormComponent(_enrolmentService, _userService, _route) {
        this._enrolmentService = _enrolmentService;
        this._userService = _userService;
        this._route = _route;
        this.centreName = null;
        this.logo = null;
        this.completeEnrolmentForm = null;
        ;
    }
    FamilyEnrolmentFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route
            .params
            .flatMap(function (params) {
            var parentId = params['parentId'];
            return rxjs_1.Observable.forkJoin(_this._enrolmentService.getEnrolmentForm$(parentId, _this._userService.SelectedCentre.id), _this._enrolmentService.getTitles$(), _this._enrolmentService.getGenders$(), _this._enrolmentService.getCareTypes$(), _this._enrolmentService.getRelationships$());
        })
            .subscribe(function (res) {
            _this.completeEnrolmentForm = res[0];
        });
        this._userService
            .getCentreLogo$(this._userService.SelectedCentre.id)
            .subscribe(function (res) {
            _this.logo = res.imageBase64;
        });
        this.centreName = this._userService.SelectedCentre.name;
    };
    FamilyEnrolmentFormComponent.prototype.print = function () {
        window.print();
    };
    FamilyEnrolmentFormComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'family-enrolment-form-cmp',
            templateUrl: 'family-enrolment-form.component.html',
            styleUrls: ['family-enrolment-form.component.css']
        }), 
        __metadata('design:paramtypes', [index_2.FamilyEnrolmentService, index_1.CentreUserService, router_1.ActivatedRoute])
    ], FamilyEnrolmentFormComponent);
    return FamilyEnrolmentFormComponent;
}());
exports.FamilyEnrolmentFormComponent = FamilyEnrolmentFormComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2ZhbWlseS1lbnJvbG1lbnQtZm9ybS9mYW1pbHktZW5yb2xtZW50LWZvcm0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBa0MsZUFBZSxDQUFDLENBQUE7QUFDbEQsdUJBQXVDLGlCQUFpQixDQUFDLENBQUE7QUFDekQscUJBQTJCLE1BQU0sQ0FBQyxDQUFBO0FBRWxDLHNCQUFrQyxxQkFBcUIsQ0FBQyxDQUFBO0FBQ3hELHNCQUF1QyxTQUFTLENBQUMsQ0FBQTtBQVNqRDtJQUtDLHNDQUNTLGlCQUF5QyxFQUN6QyxZQUErQixFQUMvQixNQUFzQjtRQUZ0QixzQkFBaUIsR0FBakIsaUJBQWlCLENBQXdCO1FBQ3pDLGlCQUFZLEdBQVosWUFBWSxDQUFtQjtRQUMvQixXQUFNLEdBQU4sTUFBTSxDQUFnQjtRQU52QixlQUFVLEdBQVcsSUFBSSxDQUFDO1FBQzFCLFNBQUksR0FBVyxJQUFJLENBQUM7UUFDcEIsMEJBQXFCLEdBQW1DLElBQUksQ0FBQztRQUlsQyxDQUFDO0lBQUMsQ0FBQztJQUV0QywrQ0FBUSxHQUFSO1FBQUEsaUJBd0JDO1FBdEJBLElBQUksQ0FBQyxNQUFNO2FBQ1QsTUFBTTthQUNOLE9BQU8sQ0FBQyxVQUFDLE1BQWM7WUFDdkIsSUFBSSxRQUFRLEdBQVcsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzFDLE1BQU0sQ0FBQyxpQkFBVSxDQUFDLFFBQVEsQ0FDekIsS0FBSSxDQUFDLGlCQUFpQixDQUFDLGlCQUFpQixDQUFDLFFBQVEsRUFBRSxLQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxFQUFFLENBQUMsRUFDdkYsS0FBSSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsRUFBRSxFQUNuQyxLQUFJLENBQUMsaUJBQWlCLENBQUMsV0FBVyxFQUFFLEVBQ3BDLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLEVBQUUsRUFDdEMsS0FBSSxDQUFDLGlCQUFpQixDQUFDLGlCQUFpQixFQUFFLENBQUMsQ0FBQztRQUM5QyxDQUFDLENBQUM7YUFDRCxTQUFTLENBQUMsVUFBQSxHQUFHO1lBQ2IsS0FBSSxDQUFDLHFCQUFxQixHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNyQyxDQUFDLENBQUMsQ0FBQztRQUVKLElBQUksQ0FBQyxZQUFZO2FBQ2YsY0FBYyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FBQzthQUNuRCxTQUFTLENBQUMsVUFBQSxHQUFHO1lBQ2IsS0FBSSxDQUFDLElBQUksR0FBRyxHQUFHLENBQUMsV0FBVyxDQUFDO1FBQzdCLENBQUMsQ0FBQyxDQUFDO1FBRUosSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUM7SUFDekQsQ0FBQztJQUVELDRDQUFLLEdBQUw7UUFDQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDaEIsQ0FBQztJQTdDRjtRQUFDLGdCQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLDJCQUEyQjtZQUNyQyxXQUFXLEVBQUUsc0NBQXNDO1lBQ25ELFNBQVMsRUFBRSxDQUFDLHFDQUFxQyxDQUFDO1NBQ2xELENBQUM7O29DQUFBO0lBeUNGLG1DQUFDO0FBQUQsQ0F2Q0EsQUF1Q0MsSUFBQTtBQXZDWSxvQ0FBNEIsK0JBdUN4QyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2ZhbWlseS1lbnJvbG1lbnQtZm9ybS9mYW1pbHktZW5yb2xtZW50LWZvcm0uY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUsIFBhcmFtcyB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQ29tcGxldGVFbnJvbG1lbnRGb3JtVmlld01vZGVsIH0gZnJvbSAnLi4vLi4vLi4vYXBpL2luZGV4JztcclxuaW1wb3J0IHsgQ2VudHJlVXNlclNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9jb3JlL2luZGV4JztcclxuaW1wb3J0IHsgRmFtaWx5RW5yb2xtZW50U2VydmljZSB9IGZyb20gJy4vaW5kZXgnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcblx0bW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuXHRzZWxlY3RvcjogJ2ZhbWlseS1lbnJvbG1lbnQtZm9ybS1jbXAnLFxyXG5cdHRlbXBsYXRlVXJsOiAnZmFtaWx5LWVucm9sbWVudC1mb3JtLmNvbXBvbmVudC5odG1sJyxcclxuXHRzdHlsZVVybHM6IFsnZmFtaWx5LWVucm9sbWVudC1mb3JtLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEZhbWlseUVucm9sbWVudEZvcm1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuXHRwcml2YXRlIGNlbnRyZU5hbWU6IHN0cmluZyA9IG51bGw7XHJcblx0cHJpdmF0ZSBsb2dvOiBzdHJpbmcgPSBudWxsO1xyXG5cdHByaXZhdGUgY29tcGxldGVFbnJvbG1lbnRGb3JtOiBDb21wbGV0ZUVucm9sbWVudEZvcm1WaWV3TW9kZWwgPSBudWxsO1xyXG5cdGNvbnN0cnVjdG9yKFxyXG5cdFx0cHJpdmF0ZSBfZW5yb2xtZW50U2VydmljZTogRmFtaWx5RW5yb2xtZW50U2VydmljZSxcclxuXHRcdHByaXZhdGUgX3VzZXJTZXJ2aWNlOiBDZW50cmVVc2VyU2VydmljZSxcclxuXHRcdHByaXZhdGUgX3JvdXRlOiBBY3RpdmF0ZWRSb3V0ZSkgeyA7IH1cclxuXHJcblx0bmdPbkluaXQoKTogdm9pZCB7XHJcblxyXG5cdFx0dGhpcy5fcm91dGVcclxuXHRcdFx0LnBhcmFtc1xyXG5cdFx0XHQuZmxhdE1hcCgocGFyYW1zOiBQYXJhbXMpID0+IHtcclxuXHRcdFx0XHRsZXQgcGFyZW50SWQ6IHN0cmluZyA9IHBhcmFtc1sncGFyZW50SWQnXTtcclxuXHRcdFx0XHRyZXR1cm4gT2JzZXJ2YWJsZS5mb3JrSm9pbihcclxuXHRcdFx0XHRcdHRoaXMuX2Vucm9sbWVudFNlcnZpY2UuZ2V0RW5yb2xtZW50Rm9ybSQocGFyZW50SWQsIHRoaXMuX3VzZXJTZXJ2aWNlLlNlbGVjdGVkQ2VudHJlLmlkKSxcclxuXHRcdFx0XHRcdHRoaXMuX2Vucm9sbWVudFNlcnZpY2UuZ2V0VGl0bGVzJCgpLFxyXG5cdFx0XHRcdFx0dGhpcy5fZW5yb2xtZW50U2VydmljZS5nZXRHZW5kZXJzJCgpLFxyXG5cdFx0XHRcdFx0dGhpcy5fZW5yb2xtZW50U2VydmljZS5nZXRDYXJlVHlwZXMkKCksXHJcblx0XHRcdFx0XHR0aGlzLl9lbnJvbG1lbnRTZXJ2aWNlLmdldFJlbGF0aW9uc2hpcHMkKCkpO1xyXG5cdFx0XHR9KVxyXG5cdFx0XHQuc3Vic2NyaWJlKHJlcyA9PiB7XHJcblx0XHRcdFx0dGhpcy5jb21wbGV0ZUVucm9sbWVudEZvcm0gPSByZXNbMF07XHJcblx0XHRcdH0pO1xyXG5cclxuXHRcdHRoaXMuX3VzZXJTZXJ2aWNlXHJcblx0XHRcdC5nZXRDZW50cmVMb2dvJCh0aGlzLl91c2VyU2VydmljZS5TZWxlY3RlZENlbnRyZS5pZClcclxuXHRcdFx0LnN1YnNjcmliZShyZXMgPT4ge1xyXG5cdFx0XHRcdHRoaXMubG9nbyA9IHJlcy5pbWFnZUJhc2U2NDtcclxuXHRcdFx0fSk7XHJcblxyXG5cdFx0dGhpcy5jZW50cmVOYW1lID0gdGhpcy5fdXNlclNlcnZpY2UuU2VsZWN0ZWRDZW50cmUubmFtZTtcclxuXHR9XHJcblxyXG5cdHByaW50KCk6IHZvaWQge1xyXG5cdFx0d2luZG93LnByaW50KCk7XHJcblx0fVxyXG59XHJcbiJdfQ==
