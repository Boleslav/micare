"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var rxjs_1 = require('rxjs');
var index_1 = require('../index');
var index_2 = require('../../../../api/index');
var ContactViewComponent = (function () {
    function ContactViewComponent(_enrolmentService) {
        this._enrolmentService = _enrolmentService;
        this.titles = {};
        this.genders = {};
        this.relationships = {};
        ;
    }
    ContactViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        rxjs_1.Observable.forkJoin(this._enrolmentService.getTitles$(), this._enrolmentService.getGenders$(), this._enrolmentService.getRelationships$())
            .subscribe(function (res) {
            _this.titles = res[0];
            _this.genders = res[1];
            _this.relationships = res[2];
        });
    };
    __decorate([
        core_1.Input('contact'), 
        __metadata('design:type', index_2.ContactDto)
    ], ContactViewComponent.prototype, "contact", void 0);
    ContactViewComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'contact-view-cmp',
            templateUrl: 'contact-view.component.html'
        }), 
        __metadata('design:paramtypes', [index_1.FamilyEnrolmentService])
    ], ContactViewComponent);
    return ContactViewComponent;
}());
exports.ContactViewComponent = ContactViewComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2ZhbWlseS1lbnJvbG1lbnQtZm9ybS9jb250YWN0LXZpZXcvY29udGFjdC12aWV3LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQXlDLGVBQWUsQ0FBQyxDQUFBO0FBQ3pELHFCQUEyQixNQUFNLENBQUMsQ0FBQTtBQUNsQyxzQkFBdUMsVUFBVSxDQUFDLENBQUE7QUFDbEQsc0JBQTJCLHVCQUF1QixDQUFDLENBQUE7QUFRbkQ7SUFNSSw4QkFBb0IsaUJBQXlDO1FBQXpDLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBd0I7UUFIckQsV0FBTSxHQUE4QixFQUFFLENBQUM7UUFDdkMsWUFBTyxHQUE4QixFQUFFLENBQUM7UUFDeEMsa0JBQWEsR0FBOEIsRUFBRSxDQUFDO1FBQ1csQ0FBQztJQUFDLENBQUM7SUFFcEUsdUNBQVEsR0FBUjtRQUFBLGlCQVVDO1FBVEcsaUJBQVUsQ0FBQyxRQUFRLENBQ2YsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsRUFBRSxFQUNuQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsV0FBVyxFQUFFLEVBQ3BDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO2FBQzFDLFNBQVMsQ0FBQyxVQUFBLEdBQUc7WUFDVixLQUFJLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNyQixLQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN0QixLQUFJLENBQUMsYUFBYSxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNoQyxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFoQkQ7UUFBQyxZQUFLLENBQUMsU0FBUyxDQUFDOzt5REFBQTtJQVJyQjtRQUFDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLGtCQUFrQjtZQUM1QixXQUFXLEVBQUUsNkJBQTZCO1NBQzdDLENBQUM7OzRCQUFBO0lBcUJGLDJCQUFDO0FBQUQsQ0FuQkEsQUFtQkMsSUFBQTtBQW5CWSw0QkFBb0IsdUJBbUJoQyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2ZhbWlseS1lbnJvbG1lbnQtZm9ybS9jb250YWN0LXZpZXcvY29udGFjdC12aWV3LmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEZhbWlseUVucm9sbWVudFNlcnZpY2UgfSBmcm9tICcuLi9pbmRleCc7XHJcbmltcG9ydCB7IENvbnRhY3REdG8gfSBmcm9tICcuLi8uLi8uLi8uLi9hcGkvaW5kZXgnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgc2VsZWN0b3I6ICdjb250YWN0LXZpZXctY21wJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnY29udGFjdC12aWV3LmNvbXBvbmVudC5odG1sJ1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIENvbnRhY3RWaWV3Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBASW5wdXQoJ2NvbnRhY3QnKSBjb250YWN0OiBDb250YWN0RHRvO1xyXG4gICAgcHJpdmF0ZSB0aXRsZXM6IHsgW2tleTogc3RyaW5nXTogc3RyaW5nIH0gPSB7fTtcclxuICAgIHByaXZhdGUgZ2VuZGVyczogeyBba2V5OiBzdHJpbmddOiBzdHJpbmcgfSA9IHt9O1xyXG4gICAgcHJpdmF0ZSByZWxhdGlvbnNoaXBzOiB7IFtrZXk6IHN0cmluZ106IHN0cmluZyB9ID0ge307XHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9lbnJvbG1lbnRTZXJ2aWNlOiBGYW1pbHlFbnJvbG1lbnRTZXJ2aWNlKSB7IDsgfVxyXG5cclxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgICAgIE9ic2VydmFibGUuZm9ya0pvaW4oXHJcbiAgICAgICAgICAgIHRoaXMuX2Vucm9sbWVudFNlcnZpY2UuZ2V0VGl0bGVzJCgpLFxyXG4gICAgICAgICAgICB0aGlzLl9lbnJvbG1lbnRTZXJ2aWNlLmdldEdlbmRlcnMkKCksXHJcbiAgICAgICAgICAgIHRoaXMuX2Vucm9sbWVudFNlcnZpY2UuZ2V0UmVsYXRpb25zaGlwcyQoKSlcclxuICAgICAgICAgICAgLnN1YnNjcmliZShyZXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50aXRsZXMgPSByZXNbMF07XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdlbmRlcnMgPSByZXNbMV07XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJlbGF0aW9uc2hpcHMgPSByZXNbMl07XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
