"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
__export(require('./centre-portal.component'));
__export(require('./centre-portal.routing'));
__export(require('./centre-portal.module'));

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2luZGV4LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxpQkFBYywyQkFBMkIsQ0FBQyxFQUFBO0FBQzFDLGlCQUFjLHlCQUF5QixDQUFDLEVBQUE7QUFDeEMsaUJBQWMsd0JBQXdCLENBQUMsRUFBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9pbmRleC5qcyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCAqIGZyb20gJy4vY2VudHJlLXBvcnRhbC5jb21wb25lbnQnO1xyXG5leHBvcnQgKiBmcm9tICcuL2NlbnRyZS1wb3J0YWwucm91dGluZyc7XHJcbmV4cG9ydCAqIGZyb20gJy4vY2VudHJlLXBvcnRhbC5tb2R1bGUnO1xyXG4iXX0=
