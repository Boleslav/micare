"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var local_data_source_1 = require("../../../../shared/ng2-smart-table/ng2-smart-table/lib/data-source/local/local.data-source");
var signup_service_1 = require("../../signup/signupService/signup.service");
var angular2_toaster_1 = require('angular2-toaster');
var client_service_1 = require("../../../../api/client.service");
var centre_user_service_1 = require("../../../../core/centre-user.service");
var ToolsMassInvitesComponent = (function () {
    function ToolsMassInvitesComponent(_signupService, _toasterService, _userService) {
        this._signupService = _signupService;
        this._toasterService = _toasterService;
        this._userService = _userService;
        this.source = new local_data_source_1.LocalDataSource();
        this.inputContent = '';
        this.initSettings();
    }
    ToolsMassInvitesComponent.prototype.importData = function () {
        var _this = this;
        this.families = [];
        var records = this.inputContent.split('\n');
        records.forEach(function (r) {
            var cells = r.split('\t');
            var family = new ToolsMassInvitesComponent.Family();
            family.parentFirstName = cells[4] || '';
            family.parentLastName = cells[5] || '';
            family.parentEmail = cells[6] || '';
            family.childFirstName = cells[1] || '';
            family.childLastName = cells[2] || '';
            family.childDob = cells[3] || '';
            _this.families.push(family);
        });
        this.source.load(this.families);
        this.inputContent = '';
    };
    ToolsMassInvitesComponent.prototype.processEnrollment = function () {
        var _this = this;
        if (!this.families.length)
            return;
        var familiesToSend = [];
        this.families.forEach(function (f) {
            if (!f.parentEmail)
                return;
            var parent = new client_service_1.RegisterNewFamilyModel();
            parent.firstName = f.parentFirstName;
            parent.lastName = f.parentLastName;
            parent.email = f.parentEmail;
            var kid = new client_service_1.KidCreateViewModel();
            kid.firstName = f.childFirstName;
            kid.lastName = f.childLastName;
            kid.dob = new Date(f.childDob);
            kid.facilityId = parseInt(_this._userService.SelectedCentre.id);
            parent.kids = [kid];
            familiesToSend.push(parent);
        });
        this.isProcessing = true;
        this._signupService.EnrollFamilies(familiesToSend)
            .subscribe(function () {
            _this._toasterService.popAsync('success', 'Process finished successfully', '');
            _this.families = [];
            _this.source.load(_this.families);
            _this.isProcessing = false;
        }, function () { return _this.isProcessing = false; });
    };
    ToolsMassInvitesComponent.prototype.addPerson = function (event) {
        this.createUpdatePerson(event);
    };
    ToolsMassInvitesComponent.prototype.editPerson = function (event) {
        this.createUpdatePerson(event);
    };
    ToolsMassInvitesComponent.prototype.createUpdatePerson = function (event) {
    };
    ToolsMassInvitesComponent.prototype.removePerson = function (event) {
    };
    ToolsMassInvitesComponent.prototype.initSettings = function () {
        this.settings = {
            mode: 'internal',
            hideHeader: false,
            hideSubHeader: true,
            sort: false,
            actions: false,
            delete: {
                deleteButtonContent: 'Remove Person',
                confirmDelete: true
            },
            columns: {
                childLastName: {
                    title: 'Child Family Name',
                    filter: false
                },
                childFirstName: {
                    title: 'Child First Name',
                    filter: false
                },
                childDob: {
                    title: 'Child DOB',
                    filter: false
                },
                parentFirstName: {
                    title: 'Parent First Name',
                    filter: false
                },
                parentLastName: {
                    title: 'Parent Family Name',
                    filter: false
                },
                parentEmail: {
                    title: 'Parent Email',
                    filter: false
                },
            },
            noDataMessage: 'No Families to Enroll Provided'
        };
    };
    ToolsMassInvitesComponent.Family = (function () {
        function class_1() {
        }
        return class_1;
    }());
    ToolsMassInvitesComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'mass-invites-cmp',
            templateUrl: './mass-invites.component.html'
        }), 
        __metadata('design:paramtypes', [signup_service_1.SignupService, angular2_toaster_1.ToasterService, centre_user_service_1.CentreUserService])
    ], ToolsMassInvitesComponent);
    return ToolsMassInvitesComponent;
}());
exports.ToolsMassInvitesComponent = ToolsMassInvitesComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3Rvb2xzL21hc3MtaW52aXRlcy9tYXNzLWludml0ZXMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBd0IsZUFBZSxDQUFDLENBQUE7QUFDeEMsa0NBQThCLDRGQUE0RixDQUFDLENBQUE7QUFDM0gsK0JBQTRCLDJDQUEyQyxDQUFDLENBQUE7QUFDeEUsaUNBQStCLGtCQUFrQixDQUFDLENBQUE7QUFDbEQsK0JBQXlELGdDQUFnQyxDQUFDLENBQUE7QUFDMUYsb0NBQWdDLHNDQUFzQyxDQUFDLENBQUE7QUFRdkU7SUFtQkUsbUNBQW9CLGNBQTZCLEVBQzdCLGVBQStCLEVBQy9CLFlBQStCO1FBRi9CLG1CQUFjLEdBQWQsY0FBYyxDQUFlO1FBQzdCLG9CQUFlLEdBQWYsZUFBZSxDQUFnQjtRQUMvQixpQkFBWSxHQUFaLFlBQVksQ0FBbUI7UUFqQjNDLFdBQU0sR0FBb0IsSUFBSSxtQ0FBZSxFQUFFLENBQUM7UUFDaEQsaUJBQVksR0FBVyxFQUFFLENBQUM7UUFtQmhDLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUN0QixDQUFDO0lBRUQsOENBQVUsR0FBVjtRQUFBLGlCQXFCQztRQXBCQyxJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztRQUVuQixJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QyxPQUFPLENBQUMsT0FBTyxDQUFDLFVBQUEsQ0FBQztZQUNmLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7WUFFMUIsSUFBSSxNQUFNLEdBQUcsSUFBSSx5QkFBeUIsQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUNwRCxNQUFNLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDeEMsTUFBTSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1lBQ3ZDLE1BQU0sQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUVwQyxNQUFNLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDdkMsTUFBTSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1lBQ3RDLE1BQU0sQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUVqQyxLQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM3QixDQUFDLENBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNoQyxJQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztJQUN6QixDQUFDO0lBRUQscURBQWlCLEdBQWpCO1FBQUEsaUJBb0NDO1FBbkNDLEVBQUUsQ0FBQSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7WUFDdkIsTUFBTSxDQUFDO1FBRVQsSUFBSSxjQUFjLEdBQTZCLEVBQUUsQ0FBQztRQUNsRCxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFBLENBQUM7WUFDckIsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDO2dCQUNqQixNQUFNLENBQUM7WUFFVCxJQUFJLE1BQU0sR0FBRyxJQUFJLHVDQUFzQixFQUFFLENBQUM7WUFDMUMsTUFBTSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsZUFBZSxDQUFDO1lBQ3JDLE1BQU0sQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDLGNBQWMsQ0FBQztZQUNuQyxNQUFNLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxXQUFXLENBQUM7WUFFN0IsSUFBSSxHQUFHLEdBQUcsSUFBSSxtQ0FBa0IsRUFBRSxDQUFDO1lBQ25DLEdBQUcsQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLGNBQWMsQ0FBQztZQUNqQyxHQUFHLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7WUFDL0IsR0FBRyxDQUFDLEdBQUcsR0FBRyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUM7WUFFL0IsR0FBRyxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsRUFBRSxDQUFDLENBQUM7WUFFL0QsTUFBTSxDQUFDLElBQUksR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRXBCLGNBQWMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDOUIsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztRQUN6QixJQUFJLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUM7YUFDL0MsU0FBUyxDQUFDO1lBQ1AsS0FBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLCtCQUErQixFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQzlFLEtBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO1lBQ25CLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNoQyxLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUM1QixDQUFDLEVBQ0QsY0FBTSxPQUFBLEtBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxFQUF6QixDQUF5QixDQUNoQyxDQUFDO0lBQ04sQ0FBQztJQUNELDZDQUFTLEdBQVQsVUFBVSxLQUFVO1FBQ2xCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNqQyxDQUFDO0lBRUQsOENBQVUsR0FBVixVQUFXLEtBQVU7UUFDbkIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2pDLENBQUM7SUFFRCxzREFBa0IsR0FBbEIsVUFBbUIsS0FBVTtJQUU3QixDQUFDO0lBRUQsZ0RBQVksR0FBWixVQUFhLEtBQVU7SUFFdkIsQ0FBQztJQUdPLGdEQUFZLEdBQXBCO1FBQ0UsSUFBSSxDQUFDLFFBQVEsR0FBRztZQUNkLElBQUksRUFBRSxVQUFVO1lBQ2hCLFVBQVUsRUFBRSxLQUFLO1lBQ2pCLGFBQWEsRUFBRSxJQUFJO1lBQ25CLElBQUksRUFBRSxLQUFLO1lBQ1gsT0FBTyxFQUFFLEtBQUs7WUFtQmQsTUFBTSxFQUFFO2dCQUNOLG1CQUFtQixFQUFFLGVBQWU7Z0JBQ3BDLGFBQWEsRUFBRSxJQUFJO2FBQ3BCO1lBQ0QsT0FBTyxFQUFFO2dCQUNQLGFBQWEsRUFBRTtvQkFDYixLQUFLLEVBQUUsbUJBQW1CO29CQUMxQixNQUFNLEVBQUUsS0FBSztpQkFDZDtnQkFDRCxjQUFjLEVBQUU7b0JBQ2QsS0FBSyxFQUFFLGtCQUFrQjtvQkFDekIsTUFBTSxFQUFFLEtBQUs7aUJBQ2Q7Z0JBQ0QsUUFBUSxFQUFFO29CQUNSLEtBQUssRUFBRSxXQUFXO29CQUNsQixNQUFNLEVBQUUsS0FBSztpQkFDZDtnQkFDRCxlQUFlLEVBQUU7b0JBQ2YsS0FBSyxFQUFFLG1CQUFtQjtvQkFDMUIsTUFBTSxFQUFFLEtBQUs7aUJBQ2Q7Z0JBQ0QsY0FBYyxFQUFFO29CQUNkLEtBQUssRUFBRSxvQkFBb0I7b0JBQzNCLE1BQU0sRUFBRSxLQUFLO2lCQUNkO2dCQUNELFdBQVcsRUFBRTtvQkFDWCxLQUFLLEVBQUUsY0FBYztvQkFDckIsTUFBTSxFQUFFLEtBQUs7aUJBQ2Q7YUFDRjtZQUNELGFBQWEsRUFBRSxnQ0FBZ0M7U0FDaEQsQ0FBQztJQUNKLENBQUM7SUF4Sk0sZ0NBQU0sR0FBRztRQUFBO1FBUWhCLENBQUM7UUFBRCxjQUFDO0lBQUQsQ0FSZ0IsQUFRZixHQUFBLENBQUE7SUF2Qkg7UUFBQyxnQkFBUyxDQUFDO1lBQ1YsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ2hCLFFBQVEsRUFBRSxrQkFBa0I7WUFDNUIsV0FBVyxFQUFFLCtCQUErQjtTQUMvQyxDQUFDOztpQ0FBQTtJQW9LRixnQ0FBQztBQUFELENBbEtBLEFBa0tDLElBQUE7QUFsS1ksaUNBQXlCLDRCQWtLckMsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC90b29scy9tYXNzLWludml0ZXMvbWFzcy1pbnZpdGVzLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7TG9jYWxEYXRhU291cmNlfSBmcm9tIFwiLi4vLi4vLi4vLi4vc2hhcmVkL25nMi1zbWFydC10YWJsZS9uZzItc21hcnQtdGFibGUvbGliL2RhdGEtc291cmNlL2xvY2FsL2xvY2FsLmRhdGEtc291cmNlXCI7XG5pbXBvcnQge1NpZ251cFNlcnZpY2V9IGZyb20gXCIuLi8uLi9zaWdudXAvc2lnbnVwU2VydmljZS9zaWdudXAuc2VydmljZVwiO1xuaW1wb3J0IHsgVG9hc3RlclNlcnZpY2UgfSBmcm9tICdhbmd1bGFyMi10b2FzdGVyJztcbmltcG9ydCB7S2lkQ3JlYXRlVmlld01vZGVsLCBSZWdpc3Rlck5ld0ZhbWlseU1vZGVsfSBmcm9tIFwiLi4vLi4vLi4vLi4vYXBpL2NsaWVudC5zZXJ2aWNlXCI7XG5pbXBvcnQge0NlbnRyZVVzZXJTZXJ2aWNlfSBmcm9tIFwiLi4vLi4vLi4vLi4vY29yZS9jZW50cmUtdXNlci5zZXJ2aWNlXCI7XG5cbkBDb21wb25lbnQoe1xuXHRtb2R1bGVJZDogbW9kdWxlLmlkLFxuICAgIHNlbGVjdG9yOiAnbWFzcy1pbnZpdGVzLWNtcCcsXG4gICAgdGVtcGxhdGVVcmw6ICcuL21hc3MtaW52aXRlcy5jb21wb25lbnQuaHRtbCdcbn0pXG5cbmV4cG9ydCBjbGFzcyBUb29sc01hc3NJbnZpdGVzQ29tcG9uZW50IHtcbiAgcHJpdmF0ZSBzZXR0aW5nczogYW55O1xuXG4gIHByaXZhdGUgZmFtaWxpZXM6IGFueVtdO1xuICBwcml2YXRlIHNvdXJjZTogTG9jYWxEYXRhU291cmNlID0gbmV3IExvY2FsRGF0YVNvdXJjZSgpO1xuICBwcml2YXRlIGlucHV0Q29udGVudDogc3RyaW5nID0gJyc7XG5cbiAgaXNQcm9jZXNzaW5nOiBib29sZWFuO1xuXG4gIHN0YXRpYyBGYW1pbHkgPSBjbGFzcyB7XG4gICAgcGFyZW50RW1haWw6IHN0cmluZztcbiAgICBwYXJlbnRGaXJzdE5hbWU6IHN0cmluZztcbiAgICBwYXJlbnRMYXN0TmFtZTogc3RyaW5nO1xuXG4gICAgY2hpbGRGaXJzdE5hbWU6IHN0cmluZztcbiAgICBjaGlsZExhc3ROYW1lOiBzdHJpbmc7XG4gICAgY2hpbGREb2I6IHN0cmluZztcbiAgfVxuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgX3NpZ251cFNlcnZpY2U6IFNpZ251cFNlcnZpY2UsXG4gICAgICAgICAgICAgIHByaXZhdGUgX3RvYXN0ZXJTZXJ2aWNlOiBUb2FzdGVyU2VydmljZSxcbiAgICAgICAgICAgICAgcHJpdmF0ZSBfdXNlclNlcnZpY2U6IENlbnRyZVVzZXJTZXJ2aWNlXG4gICkge1xuXG4gICAgdGhpcy5pbml0U2V0dGluZ3MoKTtcbiAgfVxuXG4gIGltcG9ydERhdGEgKCkge1xuICAgIHRoaXMuZmFtaWxpZXMgPSBbXTtcblxuICAgIHZhciByZWNvcmRzID0gdGhpcy5pbnB1dENvbnRlbnQuc3BsaXQoJ1xcbicpO1xuICAgIHJlY29yZHMuZm9yRWFjaChyID0+IHtcbiAgICAgIHZhciBjZWxscyA9IHIuc3BsaXQoJ1xcdCcpO1xuXG4gICAgICB2YXIgZmFtaWx5ID0gbmV3IFRvb2xzTWFzc0ludml0ZXNDb21wb25lbnQuRmFtaWx5KCk7XG4gICAgICBmYW1pbHkucGFyZW50Rmlyc3ROYW1lID0gY2VsbHNbNF0gfHwgJyc7XG4gICAgICBmYW1pbHkucGFyZW50TGFzdE5hbWUgPSBjZWxsc1s1XSB8fCAnJztcbiAgICAgIGZhbWlseS5wYXJlbnRFbWFpbCA9IGNlbGxzWzZdIHx8ICcnO1xuXG4gICAgICBmYW1pbHkuY2hpbGRGaXJzdE5hbWUgPSBjZWxsc1sxXSB8fCAnJztcbiAgICAgIGZhbWlseS5jaGlsZExhc3ROYW1lID0gY2VsbHNbMl0gfHwgJyc7XG4gICAgICBmYW1pbHkuY2hpbGREb2IgPSBjZWxsc1szXSB8fCAnJztcblxuICAgICAgdGhpcy5mYW1pbGllcy5wdXNoKGZhbWlseSk7XG4gICAgfSk7XG5cbiAgICB0aGlzLnNvdXJjZS5sb2FkKHRoaXMuZmFtaWxpZXMpO1xuICAgIHRoaXMuaW5wdXRDb250ZW50ID0gJyc7XG4gIH1cblxuICBwcm9jZXNzRW5yb2xsbWVudCgpIHtcbiAgICBpZighdGhpcy5mYW1pbGllcy5sZW5ndGgpXG4gICAgICByZXR1cm47XG5cbiAgICB2YXIgZmFtaWxpZXNUb1NlbmQ6IFJlZ2lzdGVyTmV3RmFtaWx5TW9kZWxbXSA9IFtdO1xuICAgIHRoaXMuZmFtaWxpZXMuZm9yRWFjaChmID0+IHtcbiAgICAgIGlmICghZi5wYXJlbnRFbWFpbClcbiAgICAgICAgcmV0dXJuO1xuXG4gICAgICB2YXIgcGFyZW50ID0gbmV3IFJlZ2lzdGVyTmV3RmFtaWx5TW9kZWwoKTtcbiAgICAgIHBhcmVudC5maXJzdE5hbWUgPSBmLnBhcmVudEZpcnN0TmFtZTtcbiAgICAgIHBhcmVudC5sYXN0TmFtZSA9IGYucGFyZW50TGFzdE5hbWU7XG4gICAgICBwYXJlbnQuZW1haWwgPSBmLnBhcmVudEVtYWlsO1xuXG4gICAgICB2YXIga2lkID0gbmV3IEtpZENyZWF0ZVZpZXdNb2RlbCgpO1xuICAgICAga2lkLmZpcnN0TmFtZSA9IGYuY2hpbGRGaXJzdE5hbWU7XG4gICAgICBraWQubGFzdE5hbWUgPSBmLmNoaWxkTGFzdE5hbWU7XG4gICAgICBraWQuZG9iID0gbmV3IERhdGUoZi5jaGlsZERvYik7XG5cbiAgICAgIGtpZC5mYWNpbGl0eUlkID0gcGFyc2VJbnQodGhpcy5fdXNlclNlcnZpY2UuU2VsZWN0ZWRDZW50cmUuaWQpO1xuXG4gICAgICBwYXJlbnQua2lkcyA9IFtraWRdO1xuXG4gICAgICBmYW1pbGllc1RvU2VuZC5wdXNoKHBhcmVudCk7XG4gICAgfSk7XG5cbiAgICB0aGlzLmlzUHJvY2Vzc2luZyA9IHRydWU7XG4gICAgdGhpcy5fc2lnbnVwU2VydmljZS5FbnJvbGxGYW1pbGllcyhmYW1pbGllc1RvU2VuZClcbiAgICAgIC5zdWJzY3JpYmUoKCkgPT4gIHtcbiAgICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5wb3BBc3luYygnc3VjY2VzcycsICdQcm9jZXNzIGZpbmlzaGVkIHN1Y2Nlc3NmdWxseScsICcnKTtcbiAgICAgICAgICB0aGlzLmZhbWlsaWVzID0gW107XG4gICAgICAgICAgdGhpcy5zb3VyY2UubG9hZCh0aGlzLmZhbWlsaWVzKTtcbiAgICAgICAgICB0aGlzLmlzUHJvY2Vzc2luZyA9IGZhbHNlO1xuICAgICAgICB9LFxuICAgICAgICAoKSA9PiB0aGlzLmlzUHJvY2Vzc2luZyA9IGZhbHNlXG4gICAgICApO1xuICB9XG4gIGFkZFBlcnNvbihldmVudDogYW55KTogdm9pZCB7XG4gICAgdGhpcy5jcmVhdGVVcGRhdGVQZXJzb24oZXZlbnQpO1xuICB9XG5cbiAgZWRpdFBlcnNvbihldmVudDogYW55KTogdm9pZCB7XG4gICAgdGhpcy5jcmVhdGVVcGRhdGVQZXJzb24oZXZlbnQpO1xuICB9XG5cbiAgY3JlYXRlVXBkYXRlUGVyc29uKGV2ZW50OiBhbnkpIHtcblxuICB9XG5cbiAgcmVtb3ZlUGVyc29uKGV2ZW50OiBhbnkpOiB2b2lkIHtcblxuICB9XG5cblxuICBwcml2YXRlIGluaXRTZXR0aW5ncygpOiB2b2lkIHtcbiAgICB0aGlzLnNldHRpbmdzID0ge1xuICAgICAgbW9kZTogJ2ludGVybmFsJyxcbiAgICAgIGhpZGVIZWFkZXI6IGZhbHNlLFxuICAgICAgaGlkZVN1YkhlYWRlcjogdHJ1ZSxcbiAgICAgIHNvcnQ6IGZhbHNlLFxuICAgICAgYWN0aW9uczogZmFsc2UvKntcbiAgICAgICAgY29sdW1uVGl0bGU6ICcnLFxuICAgICAgICBhZGQ6IGZhbHNlLFxuICAgICAgICAvLyBlZGl0OiB0cnVlLFxuICAgICAgICBkZWxldGU6IHRydWUsXG4gICAgICAgIHBvc2l0aW9uOiAncmlnaHQnXG4gICAgICB9Ki8sXG4gICAgICAvLyBhZGQ6IHtcbiAgICAgIC8vICAgYWRkQnV0dG9uQ29udGVudDogJ0FkZCBQZXJzb24nLFxuICAgICAgLy8gICBjcmVhdGVCdXR0b25Db250ZW50OiAnQ3JlYXRlJyxcbiAgICAgIC8vICAgY2FuY2VsQnV0dG9uQ29udGVudDogJ0NhbmNlbCcsXG4gICAgICAvLyAgIGNvbmZpcm1DcmVhdGU6IHRydWVcbiAgICAgIC8vIH0sXG4gICAgICAvLyBlZGl0OiB7XG4gICAgICAvLyAgIGVkaXRCdXR0b25Db250ZW50OiAnRWRpdCBQZXJzb24nLFxuICAgICAgLy8gICBzYXZlQnV0dG9uQ29udGVudDogJ1NhdmUnLFxuICAgICAgLy8gICBjYW5jZWxCdXR0b25Db250ZW50OiAnQ2FuY2VsJyxcbiAgICAgIC8vICAgY29uZmlybVNhdmU6IHRydWVcbiAgICAgIC8vIH0sXG4gICAgICBkZWxldGU6IHtcbiAgICAgICAgZGVsZXRlQnV0dG9uQ29udGVudDogJ1JlbW92ZSBQZXJzb24nLFxuICAgICAgICBjb25maXJtRGVsZXRlOiB0cnVlXG4gICAgICB9LFxuICAgICAgY29sdW1uczoge1xuICAgICAgICBjaGlsZExhc3ROYW1lOiB7XG4gICAgICAgICAgdGl0bGU6ICdDaGlsZCBGYW1pbHkgTmFtZScsXG4gICAgICAgICAgZmlsdGVyOiBmYWxzZVxuICAgICAgICB9LFxuICAgICAgICBjaGlsZEZpcnN0TmFtZToge1xuICAgICAgICAgIHRpdGxlOiAnQ2hpbGQgRmlyc3QgTmFtZScsXG4gICAgICAgICAgZmlsdGVyOiBmYWxzZVxuICAgICAgICB9LFxuICAgICAgICBjaGlsZERvYjoge1xuICAgICAgICAgIHRpdGxlOiAnQ2hpbGQgRE9CJyxcbiAgICAgICAgICBmaWx0ZXI6IGZhbHNlXG4gICAgICAgIH0sXG4gICAgICAgIHBhcmVudEZpcnN0TmFtZToge1xuICAgICAgICAgIHRpdGxlOiAnUGFyZW50IEZpcnN0IE5hbWUnLFxuICAgICAgICAgIGZpbHRlcjogZmFsc2VcbiAgICAgICAgfSxcbiAgICAgICAgcGFyZW50TGFzdE5hbWU6IHtcbiAgICAgICAgICB0aXRsZTogJ1BhcmVudCBGYW1pbHkgTmFtZScsXG4gICAgICAgICAgZmlsdGVyOiBmYWxzZVxuICAgICAgICB9LFxuICAgICAgICBwYXJlbnRFbWFpbDoge1xuICAgICAgICAgIHRpdGxlOiAnUGFyZW50IEVtYWlsJyxcbiAgICAgICAgICBmaWx0ZXI6IGZhbHNlXG4gICAgICAgIH0sXG4gICAgICB9LFxuICAgICAgbm9EYXRhTWVzc2FnZTogJ05vIEZhbWlsaWVzIHRvIEVucm9sbCBQcm92aWRlZCdcbiAgICB9O1xuICB9XG59XG4iXX0=
