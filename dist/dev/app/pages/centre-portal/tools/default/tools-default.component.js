"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ToolsDefaultComponent = (function () {
    function ToolsDefaultComponent() {
    }
    ToolsDefaultComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            encapsulation: core_1.ViewEncapsulation.None,
            selector: 'tools-default-cmp',
            templateUrl: 'tools-default.component.html',
            styleUrls: ['tools-default.component.css']
        }), 
        __metadata('design:paramtypes', [])
    ], ToolsDefaultComponent);
    return ToolsDefaultComponent;
}());
exports.ToolsDefaultComponent = ToolsDefaultComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3Rvb2xzL2RlZmF1bHQvdG9vbHMtZGVmYXVsdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUE2QyxlQUFlLENBQUMsQ0FBQTtBQVM3RDtJQUVJO0lBRUEsQ0FBQztJQVhMO1FBQUMsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixhQUFhLEVBQUUsd0JBQWlCLENBQUMsSUFBSTtZQUNyQyxRQUFRLEVBQUUsbUJBQW1CO1lBQzdCLFdBQVcsRUFBRSw4QkFBOEI7WUFDM0MsU0FBUyxFQUFFLENBQUMsNkJBQTZCLENBQUM7U0FDN0MsQ0FBQzs7NkJBQUE7SUFNRiw0QkFBQztBQUFELENBTEEsQUFLQyxJQUFBO0FBTFksNkJBQXFCLHdCQUtqQyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL3Rvb2xzL2RlZmF1bHQvdG9vbHMtZGVmYXVsdC5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcbiAgICBzZWxlY3RvcjogJ3Rvb2xzLWRlZmF1bHQtY21wJyxcbiAgICB0ZW1wbGF0ZVVybDogJ3Rvb2xzLWRlZmF1bHQuY29tcG9uZW50Lmh0bWwnLFxuICAgIHN0eWxlVXJsczogWyd0b29scy1kZWZhdWx0LmNvbXBvbmVudC5jc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBUb29sc0RlZmF1bHRDb21wb25lbnQge1xuXG4gICAgY29uc3RydWN0b3IoKSB7XG5cbiAgICB9XG59XG4iXX0=
