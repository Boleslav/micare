"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var rxjs_1 = require('rxjs');
var index_1 = require('../../../../api/index');
var client_service_1 = require("../../../../api/client.service");
var CentreService = (function () {
    function CentreService(_apiService) {
        this._apiService = _apiService;
        this.setupBankAccountSubject = new rxjs_1.Subject();
    }
    CentreService.prototype.GetCentre$ = function (centreId) {
        return this._apiService.centre_GetCentreAsync(centreId);
    };
    CentreService.prototype.CreateCentre$ = function (createCentreDto) {
        return this._apiService.centre_CreateUpdateCentreAsync(createCentreDto);
    };
    CentreService.prototype.getTimeZones$ = function () {
        return this._apiService.centre_GetTimeZones();
    };
    CentreService.prototype.SaveBankAccountDetails$ = function (bankAccount) {
        return this._apiService.centre_PostBankAccountAsync(bankAccount);
    };
    CentreService.prototype.CanSetupBankAccount$ = function (centreId) {
        this.setupBankAccountSubject.next(centreId);
    };
    CentreService.prototype.GetSetupBankAccount$ = function () {
        var _this = this;
        return this.setupBankAccountSubject
            .switchMap(function (val) {
            if (val === undefined || val === null || val === '')
                return rxjs_1.Observable.of(false);
            return _this._apiService.centre_CanSetupBankAccount(val);
        });
    };
    CentreService.prototype.GetRedactedPaymentMethod = function (centreId) {
        return this._apiService.centre_GetRedactedBankAccountNumber(centreId);
    };
    CentreService.prototype.GetCompanyCentres = function (companyId) {
        return this._apiService.centre_GetCentresByCompany(companyId);
    };
    CentreService.prototype.GetUnassignedCentres = function (search) {
        return this._apiService.centre_GetUnassignedCentres(search);
    };
    CentreService.prototype.SetActiveCompanyCentre = function (companyId, facilityId, isActive) {
        var vm = new client_service_1.CompanyFacilityViewModel();
        vm.companyId = companyId;
        vm.facilityId = facilityId;
        vm.isActive = isActive;
        return this._apiService.centre_SetCompanyFacilityActiveAsync(vm);
    };
    CentreService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.ApiClientService])
    ], CentreService);
    return CentreService;
}());
exports.CentreService = CentreService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NlbnRyZS9jZW50cmVTZXJ2aWNlL2NlbnRyZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBMkIsZUFBZSxDQUFDLENBQUE7QUFDM0MscUJBQW9DLE1BQU0sQ0FBQyxDQUFBO0FBRTNDLHNCQUF5Rix1QkFBdUIsQ0FBQyxDQUFBO0FBQ2pILCtCQUF1QyxnQ0FBZ0MsQ0FBQyxDQUFBO0FBR3hFO0lBR0ksdUJBQW9CLFdBQTZCO1FBQTdCLGdCQUFXLEdBQVgsV0FBVyxDQUFrQjtRQUM3QyxJQUFJLENBQUMsdUJBQXVCLEdBQUcsSUFBSSxjQUFPLEVBQVUsQ0FBQztJQUN6RCxDQUFDO0lBRU0sa0NBQVUsR0FBakIsVUFBa0IsUUFBZ0I7UUFDOUIsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDNUQsQ0FBQztJQUVNLHFDQUFhLEdBQXBCLFVBQXFCLGVBQXNDO1FBRXZELE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLDhCQUE4QixDQUFDLGVBQWUsQ0FBQyxDQUFDO0lBQzVFLENBQUM7SUFFTSxxQ0FBYSxHQUFwQjtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLG1CQUFtQixFQUFFLENBQUM7SUFDbEQsQ0FBQztJQUVNLCtDQUF1QixHQUE5QixVQUErQixXQUFpQztRQUM1RCxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQywyQkFBMkIsQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUNyRSxDQUFDO0lBRU0sNENBQW9CLEdBQTNCLFVBQTRCLFFBQWdCO1FBQ3hDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUVNLDRDQUFvQixHQUEzQjtRQUFBLGlCQU9DO1FBTkcsTUFBTSxDQUFDLElBQUksQ0FBQyx1QkFBdUI7YUFDOUIsU0FBUyxDQUFDLFVBQUMsR0FBVztZQUNuQixFQUFFLENBQUMsQ0FBQyxHQUFHLEtBQUssU0FBUyxJQUFJLEdBQUcsS0FBSyxJQUFJLElBQUksR0FBRyxLQUFLLEVBQUUsQ0FBQztnQkFDaEQsTUFBTSxDQUFDLGlCQUFVLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2hDLE1BQU0sQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLDBCQUEwQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzVELENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVNLGdEQUF3QixHQUEvQixVQUFnQyxRQUFnQjtRQUM1QyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQ0FBbUMsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUMxRSxDQUFDO0lBRU0seUNBQWlCLEdBQXhCLFVBQXlCLFNBQWlCO1FBQ3RDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLDBCQUEwQixDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ2xFLENBQUM7SUFFTSw0Q0FBb0IsR0FBM0IsVUFBNEIsTUFBYztRQUN0QyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQywyQkFBMkIsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNoRSxDQUFDO0lBRU0sOENBQXNCLEdBQTdCLFVBQThCLFNBQWlCLEVBQUUsVUFBa0IsRUFBRSxRQUFpQjtRQUNwRixJQUFJLEVBQUUsR0FBRyxJQUFJLHlDQUF3QixFQUFFLENBQUM7UUFDeEMsRUFBRSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7UUFDekIsRUFBRSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7UUFDM0IsRUFBRSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7UUFDdkIsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsb0NBQW9DLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDbkUsQ0FBQztJQXhETDtRQUFDLGlCQUFVLEVBQUU7O3FCQUFBO0lBeURiLG9CQUFDO0FBQUQsQ0F4REEsQUF3REMsSUFBQTtBQXhEWSxxQkFBYSxnQkF3RHpCLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL2NlbnRyZS1wb3J0YWwvY2VudHJlL2NlbnRyZVNlcnZpY2UvY2VudHJlLnNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XG5cbmltcG9ydCB7IEFwaUNsaWVudFNlcnZpY2UsIENyZWF0ZVVwZGF0ZUNlbnRyZUR0bywgQ2VudHJlRHRvLCBDZW50cmVCYW5rQWNjb3VudER0byB9IGZyb20gJy4uLy4uLy4uLy4uL2FwaS9pbmRleCc7XG5pbXBvcnQge0NvbXBhbnlGYWNpbGl0eVZpZXdNb2RlbH0gZnJvbSBcIi4uLy4uLy4uLy4uL2FwaS9jbGllbnQuc2VydmljZVwiO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgQ2VudHJlU2VydmljZSB7XG5cbiAgICBwcml2YXRlIHNldHVwQmFua0FjY291bnRTdWJqZWN0OiBTdWJqZWN0PHN0cmluZz47XG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfYXBpU2VydmljZTogQXBpQ2xpZW50U2VydmljZSkge1xuICAgICAgICB0aGlzLnNldHVwQmFua0FjY291bnRTdWJqZWN0ID0gbmV3IFN1YmplY3Q8c3RyaW5nPigpO1xuICAgIH1cblxuICAgIHB1YmxpYyBHZXRDZW50cmUkKGNlbnRyZUlkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPENlbnRyZUR0bz4ge1xuICAgICAgICByZXR1cm4gdGhpcy5fYXBpU2VydmljZS5jZW50cmVfR2V0Q2VudHJlQXN5bmMoY2VudHJlSWQpO1xuICAgIH1cblxuICAgIHB1YmxpYyBDcmVhdGVDZW50cmUkKGNyZWF0ZUNlbnRyZUR0bzogQ3JlYXRlVXBkYXRlQ2VudHJlRHRvKTogT2JzZXJ2YWJsZTxDcmVhdGVVcGRhdGVDZW50cmVEdG8+IHtcblxuICAgICAgICByZXR1cm4gdGhpcy5fYXBpU2VydmljZS5jZW50cmVfQ3JlYXRlVXBkYXRlQ2VudHJlQXN5bmMoY3JlYXRlQ2VudHJlRHRvKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgZ2V0VGltZVpvbmVzJCgpOiBPYnNlcnZhYmxlPHN0cmluZ1tdPiB7XG4gICAgICAgIHJldHVybiB0aGlzLl9hcGlTZXJ2aWNlLmNlbnRyZV9HZXRUaW1lWm9uZXMoKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgU2F2ZUJhbmtBY2NvdW50RGV0YWlscyQoYmFua0FjY291bnQ6IENlbnRyZUJhbmtBY2NvdW50RHRvKTogT2JzZXJ2YWJsZTx2b2lkPiB7XG4gICAgICAgIHJldHVybiB0aGlzLl9hcGlTZXJ2aWNlLmNlbnRyZV9Qb3N0QmFua0FjY291bnRBc3luYyhiYW5rQWNjb3VudCk7XG4gICAgfVxuXG4gICAgcHVibGljIENhblNldHVwQmFua0FjY291bnQkKGNlbnRyZUlkOiBzdHJpbmcpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5zZXR1cEJhbmtBY2NvdW50U3ViamVjdC5uZXh0KGNlbnRyZUlkKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgR2V0U2V0dXBCYW5rQWNjb3VudCQoKTogT2JzZXJ2YWJsZTxib29sZWFuPiB7XG4gICAgICAgIHJldHVybiB0aGlzLnNldHVwQmFua0FjY291bnRTdWJqZWN0XG4gICAgICAgICAgICAuc3dpdGNoTWFwKCh2YWw6IHN0cmluZykgPT4ge1xuICAgICAgICAgICAgICAgIGlmICh2YWwgPT09IHVuZGVmaW5lZCB8fCB2YWwgPT09IG51bGwgfHwgdmFsID09PSAnJylcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIE9ic2VydmFibGUub2YoZmFsc2UpO1xuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLl9hcGlTZXJ2aWNlLmNlbnRyZV9DYW5TZXR1cEJhbmtBY2NvdW50KHZhbCk7XG4gICAgICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBwdWJsaWMgR2V0UmVkYWN0ZWRQYXltZW50TWV0aG9kKGNlbnRyZUlkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPHN0cmluZz4ge1xuICAgICAgICByZXR1cm4gdGhpcy5fYXBpU2VydmljZS5jZW50cmVfR2V0UmVkYWN0ZWRCYW5rQWNjb3VudE51bWJlcihjZW50cmVJZCk7XG4gICAgfVxuXG4gICAgcHVibGljIEdldENvbXBhbnlDZW50cmVzKGNvbXBhbnlJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxDb21wYW55RmFjaWxpdHlWaWV3TW9kZWxbXT4ge1xuICAgICAgICByZXR1cm4gdGhpcy5fYXBpU2VydmljZS5jZW50cmVfR2V0Q2VudHJlc0J5Q29tcGFueShjb21wYW55SWQpO1xuICAgIH1cblxuICAgIHB1YmxpYyBHZXRVbmFzc2lnbmVkQ2VudHJlcyhzZWFyY2g6IHN0cmluZyk6IE9ic2VydmFibGU8Q29tcGFueUZhY2lsaXR5Vmlld01vZGVsW10+IHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2FwaVNlcnZpY2UuY2VudHJlX0dldFVuYXNzaWduZWRDZW50cmVzKHNlYXJjaCk7XG4gICAgfVxuXG4gICAgcHVibGljIFNldEFjdGl2ZUNvbXBhbnlDZW50cmUoY29tcGFueUlkOiBzdHJpbmcsIGZhY2lsaXR5SWQ6IG51bWJlciwgaXNBY3RpdmU6IGJvb2xlYW4pOiBPYnNlcnZhYmxlPHZvaWQ+IHtcbiAgICAgIHZhciB2bSA9IG5ldyBDb21wYW55RmFjaWxpdHlWaWV3TW9kZWwoKTtcbiAgICAgIHZtLmNvbXBhbnlJZCA9IGNvbXBhbnlJZDtcbiAgICAgIHZtLmZhY2lsaXR5SWQgPSBmYWNpbGl0eUlkO1xuICAgICAgdm0uaXNBY3RpdmUgPSBpc0FjdGl2ZTtcbiAgICAgIHJldHVybiB0aGlzLl9hcGlTZXJ2aWNlLmNlbnRyZV9TZXRDb21wYW55RmFjaWxpdHlBY3RpdmVBc3luYyh2bSk7XG4gICAgfVxufVxuIl19
