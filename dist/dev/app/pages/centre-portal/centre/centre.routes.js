"use strict";
var index_1 = require('../../../shared/index');
var index_2 = require('./index');
exports.CentreRoutes = [
    {
        path: 'centre',
        component: index_2.CentreComponent,
        canActivate: [index_1.NavigationGuardService]
    },
];

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NlbnRyZS9jZW50cmUucm91dGVzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFDQSxzQkFBdUMsdUJBQXVCLENBQUMsQ0FBQTtBQUMvRCxzQkFBZ0MsU0FBUyxDQUFDLENBQUE7QUFFN0Isb0JBQVksR0FBWTtJQUNwQztRQUNDLElBQUksRUFBRSxRQUFRO1FBQ2QsU0FBUyxFQUFFLHVCQUFlO1FBQzFCLFdBQVcsRUFBRSxDQUFDLDhCQUFzQixDQUFDO0tBQ3JDO0NBQ0QsQ0FBQyIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9jZW50cmUvY2VudHJlLnJvdXRlcy5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgTmF2aWdhdGlvbkd1YXJkU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9pbmRleCc7XHJcbmltcG9ydCB7IENlbnRyZUNvbXBvbmVudCB9IGZyb20gJy4vaW5kZXgnO1xyXG5cclxuZXhwb3J0IGNvbnN0IENlbnRyZVJvdXRlczogUm91dGVbXSA9IFtcclxuXHR7XHJcblx0XHRwYXRoOiAnY2VudHJlJyxcclxuXHRcdGNvbXBvbmVudDogQ2VudHJlQ29tcG9uZW50LFxyXG5cdFx0Y2FuQWN0aXZhdGU6IFtOYXZpZ2F0aW9uR3VhcmRTZXJ2aWNlXVxyXG5cdH0sXHJcbl07XHJcbiJdfQ==
