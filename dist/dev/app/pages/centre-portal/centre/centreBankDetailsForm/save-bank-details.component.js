"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var angular2_toaster_1 = require('angular2-toaster');
var index_1 = require('../index');
var index_2 = require('../../../../api/index');
var index_3 = require('../../../../shared/index');
var SaveBankDetailsComponent = (function () {
    function SaveBankDetailsComponent(fb, _centreService, _toasterService) {
        this._centreService = _centreService;
        this._toasterService = _toasterService;
        this.onEdit = new core_1.EventEmitter();
        this.onSave = new core_1.EventEmitter();
        this.intervalId = null;
        this.submitted = false;
        this.form = fb.group({
            'bankBsbControl': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.pattern('\\d([\\s\\-]?\\d){5}')])],
            'bankAcnControl': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.pattern('\\d+')])]
        });
        this.bankBsbControl = this.form.controls['bankBsbControl'];
        this.bankAcnControl = this.form.controls['bankAcnControl'];
    }
    SaveBankDetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._centreService
            .GetSetupBankAccount$()
            .subscribe(function (res) {
            _this.isReadyToSetup = res;
            if (_this.isReadyToSetup) {
                clearInterval(_this.intervalId);
                _this.enableControls();
            }
            else {
                _this.disableControls();
            }
        });
        this._centreService.CanSetupBankAccount$(this.centreId);
    };
    SaveBankDetailsComponent.prototype.ngOnChanges = function (changes) {
        var _this = this;
        var centreChange = changes['centreId'];
        var approvalChange = changes['isApproved'];
        if (centreChange !== undefined && centreChange.currentValue === undefined) {
            this.disableControls();
        }
        if (approvalChange !== undefined
            && approvalChange.currentValue !== undefined
            && approvalChange.currentValue === false) {
            this.disableControls();
        }
        if (centreChange !== undefined && centreChange.currentValue !== undefined
            && approvalChange !== undefined && approvalChange.currentValue === true) {
            this._centreService.CanSetupBankAccount$(centreChange.currentValue);
            if (this.intervalId === null) {
                this.intervalId = setInterval(function () {
                    return _this._centreService.CanSetupBankAccount$(centreChange.currentValue);
                }, 10000);
            }
        }
    };
    SaveBankDetailsComponent.prototype.ngOnDestroy = function () {
        clearInterval(this.intervalId);
    };
    SaveBankDetailsComponent.prototype.onSubmit = function () {
        var _this = this;
        this.submitted = true;
        var savingToast = this._toasterService.pop(new index_3.SpinningToast());
        var bankAccount = new index_2.CentreBankAccountDto();
        bankAccount.init({
            'CentreId': this.centreId,
            'Bsb': this.bankBsbControl.value,
            'Acn': this.bankAcnControl.value
        });
        this._centreService
            .SaveBankAccountDetails$(bankAccount)
            .subscribe(function () {
            _this._toasterService.clear();
            _this._toasterService.pop('success', 'Bank Details Saved');
            _this.submitted = false;
            _this.onSave.emit(null);
        }, function (err) {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this.submitted = false;
        });
    };
    SaveBankDetailsComponent.prototype.setEditMode = function () {
        this.onEdit.emit(false);
    };
    SaveBankDetailsComponent.prototype.enableControls = function () {
        this.bankAcnControl.enable();
        this.bankBsbControl.enable();
    };
    SaveBankDetailsComponent.prototype.disableControls = function () {
        this.bankBsbControl.disable();
        this.bankAcnControl.disable();
    };
    __decorate([
        core_1.Output(), 
        __metadata('design:type', core_1.EventEmitter)
    ], SaveBankDetailsComponent.prototype, "onEdit", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', core_1.EventEmitter)
    ], SaveBankDetailsComponent.prototype, "onSave", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], SaveBankDetailsComponent.prototype, "centreId", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], SaveBankDetailsComponent.prototype, "isApproved", void 0);
    SaveBankDetailsComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'save-bank-details-cmp',
            templateUrl: './save-bank-details.component.html'
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, index_1.CentreService, angular2_toaster_1.ToasterService])
    ], SaveBankDetailsComponent);
    return SaveBankDetailsComponent;
}());
exports.SaveBankDetailsComponent = SaveBankDetailsComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NlbnRyZS9jZW50cmVCYW5rRGV0YWlsc0Zvcm0vc2F2ZS1iYW5rLWRldGFpbHMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFJTyxlQUFlLENBQUMsQ0FBQTtBQUN2QixzQkFBb0UsZ0JBQWdCLENBQUMsQ0FBQTtBQUNyRixpQ0FBK0Isa0JBQWtCLENBQUMsQ0FBQTtBQUVsRCxzQkFBOEIsVUFBVSxDQUFDLENBQUE7QUFDekMsc0JBQXFDLHVCQUF1QixDQUFDLENBQUE7QUFDN0Qsc0JBQThCLDBCQUEwQixDQUFDLENBQUE7QUFPekQ7SUFjSSxrQ0FBWSxFQUFlLEVBQ2YsY0FBNkIsRUFDN0IsZUFBK0I7UUFEL0IsbUJBQWMsR0FBZCxjQUFjLENBQWU7UUFDN0Isb0JBQWUsR0FBZixlQUFlLENBQWdCO1FBZGpDLFdBQU0sR0FBc0IsSUFBSSxtQkFBWSxFQUFFLENBQUM7UUFDL0MsV0FBTSxHQUFzQixJQUFJLG1CQUFZLEVBQUUsQ0FBQztRQUdqRCxlQUFVLEdBQWlCLElBQUksQ0FBQztRQUVoQyxjQUFTLEdBQVksS0FBSyxDQUFDO1FBUy9CLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQztZQUNqQixnQkFBZ0IsRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLGtCQUFVLENBQUMsUUFBUSxFQUFFLGtCQUFVLENBQUMsT0FBTyxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzdHLGdCQUFnQixFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsa0JBQVUsQ0FBQyxRQUFRLEVBQUUsa0JBQVUsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ2hHLENBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUMzRCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUVELDJDQUFRLEdBQVI7UUFBQSxpQkFnQkM7UUFkRyxJQUFJLENBQUMsY0FBYzthQUNkLG9CQUFvQixFQUFFO2FBQ3RCLFNBQVMsQ0FBQyxVQUFDLEdBQUc7WUFDWCxLQUFJLENBQUMsY0FBYyxHQUFHLEdBQUcsQ0FBQztZQUUxQixFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztnQkFDdEIsYUFBYSxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDL0IsS0FBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQzFCLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDSixLQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDM0IsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO1FBRVAsSUFBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDNUQsQ0FBQztJQUVELDhDQUFXLEdBQVgsVUFBWSxPQUFzQjtRQUFsQyxpQkF3QkM7UUF0QkcsSUFBSSxZQUFZLEdBQWlCLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNyRCxJQUFJLGNBQWMsR0FBaUIsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBRXpELEVBQUUsQ0FBQyxDQUFDLFlBQVksS0FBSyxTQUFTLElBQUksWUFBWSxDQUFDLFlBQVksS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ3hFLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUMzQixDQUFDO1FBRUQsRUFBRSxDQUFDLENBQUMsY0FBYyxLQUFLLFNBQVM7ZUFDekIsY0FBYyxDQUFDLFlBQVksS0FBSyxTQUFTO2VBQ3pDLGNBQWMsQ0FBQyxZQUFZLEtBQUssS0FBSyxDQUFDLENBQUMsQ0FBQztZQUMzQyxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDM0IsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLFlBQVksS0FBSyxTQUFTLElBQUksWUFBWSxDQUFDLFlBQVksS0FBSyxTQUFTO2VBQ2xFLGNBQWMsS0FBSyxTQUFTLElBQUksY0FBYyxDQUFDLFlBQVksS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQzFFLElBQUksQ0FBQyxjQUFjLENBQUMsb0JBQW9CLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ3BFLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDM0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxXQUFXLENBQUM7b0JBQzFCLE9BQUEsS0FBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDO2dCQUFuRSxDQUFtRSxFQUNqRSxLQUFLLENBQUMsQ0FBQztZQUNqQixDQUFDO1FBQ0wsQ0FBQztJQUNMLENBQUM7SUFFRCw4Q0FBVyxHQUFYO1FBQ0ksYUFBYSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRUQsMkNBQVEsR0FBUjtRQUFBLGlCQXdCQztRQXRCRyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUV0QixJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxJQUFJLHFCQUFhLEVBQUUsQ0FBQyxDQUFDO1FBRWhFLElBQUksV0FBVyxHQUFHLElBQUksNEJBQW9CLEVBQUUsQ0FBQztRQUM3QyxXQUFXLENBQUMsSUFBSSxDQUFDO1lBQ2IsVUFBVSxFQUFFLElBQUksQ0FBQyxRQUFRO1lBQ3pCLEtBQUssRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUs7WUFDaEMsS0FBSyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSztTQUNuQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsY0FBYzthQUNkLHVCQUF1QixDQUFDLFdBQVcsQ0FBQzthQUNwQyxTQUFTLENBQUM7WUFDUCxLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQzdCLEtBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxvQkFBb0IsQ0FBQyxDQUFDO1lBQzFELEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1lBQ3ZCLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzNCLENBQUMsRUFDRCxVQUFDLEdBQUc7WUFDQSxLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1lBQzlFLEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1FBQzNCLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVELDhDQUFXLEdBQVg7UUFDSSxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM1QixDQUFDO0lBRU8saURBQWMsR0FBdEI7UUFDSSxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQzdCLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDakMsQ0FBQztJQUVPLGtEQUFlLEdBQXZCO1FBQ0ksSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUM5QixJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ2xDLENBQUM7SUE5R0Q7UUFBQyxhQUFNLEVBQUU7OzREQUFBO0lBQ1Q7UUFBQyxhQUFNLEVBQUU7OzREQUFBO0lBQ1Q7UUFBQyxZQUFLLEVBQUU7OzhEQUFBO0lBQ1I7UUFBQyxZQUFLLEVBQUU7O2dFQUFBO0lBVlo7UUFBQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSx1QkFBdUI7WUFDakMsV0FBVyxFQUFFLG9DQUFvQztTQUNwRCxDQUFDOztnQ0FBQTtJQWtIRiwrQkFBQztBQUFELENBakhBLEFBaUhDLElBQUE7QUFqSFksZ0NBQXdCLDJCQWlIcEMsQ0FBQSIsImZpbGUiOiJhcHAvcGFnZXMvY2VudHJlLXBvcnRhbC9jZW50cmUvY2VudHJlQmFua0RldGFpbHNGb3JtL3NhdmUtYmFuay1kZXRhaWxzLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcbiAgICBDb21wb25lbnQsIElucHV0LCBPbkluaXQsIE9uQ2hhbmdlcyxcclxuICAgIE9uRGVzdHJveSwgU2ltcGxlQ2hhbmdlcywgU2ltcGxlQ2hhbmdlLFxyXG4gICAgT3V0cHV0LCBFdmVudEVtaXR0ZXJcclxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybUdyb3VwLCBBYnN0cmFjdENvbnRyb2wsIEZvcm1CdWlsZGVyLCBWYWxpZGF0b3JzIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBUb2FzdGVyU2VydmljZSB9IGZyb20gJ2FuZ3VsYXIyLXRvYXN0ZXInO1xyXG5cclxuaW1wb3J0IHsgQ2VudHJlU2VydmljZSB9IGZyb20gJy4uL2luZGV4JztcclxuaW1wb3J0IHsgQ2VudHJlQmFua0FjY291bnREdG8gfSBmcm9tICcuLi8uLi8uLi8uLi9hcGkvaW5kZXgnO1xyXG5pbXBvcnQgeyBTcGlubmluZ1RvYXN0IH0gZnJvbSAnLi4vLi4vLi4vLi4vc2hhcmVkL2luZGV4JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHNlbGVjdG9yOiAnc2F2ZS1iYW5rLWRldGFpbHMtY21wJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9zYXZlLWJhbmstZGV0YWlscy5jb21wb25lbnQuaHRtbCdcclxufSlcclxuZXhwb3J0IGNsYXNzIFNhdmVCYW5rRGV0YWlsc0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95LCBPbkNoYW5nZXMge1xyXG5cclxuICAgIEBPdXRwdXQoKSBvbkVkaXQ6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gICAgQE91dHB1dCgpIG9uU2F2ZTogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgICBASW5wdXQoKSBjZW50cmVJZDogc3RyaW5nO1xyXG4gICAgQElucHV0KCkgaXNBcHByb3ZlZDogYm9vbGVhbjtcclxuICAgIHByaXZhdGUgaW50ZXJ2YWxJZDogTm9kZUpTLlRpbWVyID0gbnVsbDtcclxuICAgIHByaXZhdGUgaXNSZWFkeVRvU2V0dXA6IGJvb2xlYW47XHJcbiAgICBwcml2YXRlIHN1Ym1pdHRlZDogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIHByaXZhdGUgZm9ybTogRm9ybUdyb3VwO1xyXG4gICAgcHJpdmF0ZSBiYW5rQnNiQ29udHJvbDogQWJzdHJhY3RDb250cm9sO1xyXG4gICAgcHJpdmF0ZSBiYW5rQWNuQ29udHJvbDogQWJzdHJhY3RDb250cm9sO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGZiOiBGb3JtQnVpbGRlcixcclxuICAgICAgICBwcml2YXRlIF9jZW50cmVTZXJ2aWNlOiBDZW50cmVTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgX3RvYXN0ZXJTZXJ2aWNlOiBUb2FzdGVyU2VydmljZSkge1xyXG4gICAgICAgIHRoaXMuZm9ybSA9IGZiLmdyb3VwKHtcclxuICAgICAgICAgICAgJ2JhbmtCc2JDb250cm9sJzogWycnLCBWYWxpZGF0b3JzLmNvbXBvc2UoW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMucGF0dGVybignXFxcXGQoW1xcXFxzXFxcXC1dP1xcXFxkKXs1fScpXSldLFxyXG4gICAgICAgICAgICAnYmFua0FjbkNvbnRyb2wnOiBbJycsIFZhbGlkYXRvcnMuY29tcG9zZShbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5wYXR0ZXJuKCdcXFxcZCsnKV0pXVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICB0aGlzLmJhbmtCc2JDb250cm9sID0gdGhpcy5mb3JtLmNvbnRyb2xzWydiYW5rQnNiQ29udHJvbCddO1xyXG4gICAgICAgIHRoaXMuYmFua0FjbkNvbnRyb2wgPSB0aGlzLmZvcm0uY29udHJvbHNbJ2JhbmtBY25Db250cm9sJ107XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKTogdm9pZCB7XHJcblxyXG4gICAgICAgIHRoaXMuX2NlbnRyZVNlcnZpY2VcclxuICAgICAgICAgICAgLkdldFNldHVwQmFua0FjY291bnQkKClcclxuICAgICAgICAgICAgLnN1YnNjcmliZSgocmVzKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmlzUmVhZHlUb1NldHVwID0gcmVzO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLmlzUmVhZHlUb1NldHVwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY2xlYXJJbnRlcnZhbCh0aGlzLmludGVydmFsSWQpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZW5hYmxlQ29udHJvbHMoKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5kaXNhYmxlQ29udHJvbHMoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRoaXMuX2NlbnRyZVNlcnZpY2UuQ2FuU2V0dXBCYW5rQWNjb3VudCQodGhpcy5jZW50cmVJZCk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xyXG5cclxuICAgICAgICBsZXQgY2VudHJlQ2hhbmdlOiBTaW1wbGVDaGFuZ2UgPSBjaGFuZ2VzWydjZW50cmVJZCddO1xyXG4gICAgICAgIGxldCBhcHByb3ZhbENoYW5nZTogU2ltcGxlQ2hhbmdlID0gY2hhbmdlc1snaXNBcHByb3ZlZCddO1xyXG5cclxuICAgICAgICBpZiAoY2VudHJlQ2hhbmdlICE9PSB1bmRlZmluZWQgJiYgY2VudHJlQ2hhbmdlLmN1cnJlbnRWYWx1ZSA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzYWJsZUNvbnRyb2xzKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoYXBwcm92YWxDaGFuZ2UgIT09IHVuZGVmaW5lZFxyXG4gICAgICAgICAgICAmJiBhcHByb3ZhbENoYW5nZS5jdXJyZW50VmFsdWUgIT09IHVuZGVmaW5lZFxyXG4gICAgICAgICAgICAmJiBhcHByb3ZhbENoYW5nZS5jdXJyZW50VmFsdWUgPT09IGZhbHNlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzYWJsZUNvbnRyb2xzKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoY2VudHJlQ2hhbmdlICE9PSB1bmRlZmluZWQgJiYgY2VudHJlQ2hhbmdlLmN1cnJlbnRWYWx1ZSAhPT0gdW5kZWZpbmVkXHJcbiAgICAgICAgICAgICYmIGFwcHJvdmFsQ2hhbmdlICE9PSB1bmRlZmluZWQgJiYgYXBwcm92YWxDaGFuZ2UuY3VycmVudFZhbHVlID09PSB0cnVlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2NlbnRyZVNlcnZpY2UuQ2FuU2V0dXBCYW5rQWNjb3VudCQoY2VudHJlQ2hhbmdlLmN1cnJlbnRWYWx1ZSk7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmludGVydmFsSWQgPT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaW50ZXJ2YWxJZCA9IHNldEludGVydmFsKCgpID0+XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fY2VudHJlU2VydmljZS5DYW5TZXR1cEJhbmtBY2NvdW50JChjZW50cmVDaGFuZ2UuY3VycmVudFZhbHVlKVxyXG4gICAgICAgICAgICAgICAgICAgICwgMTAwMDApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG5nT25EZXN0cm95KCk6IHZvaWQge1xyXG4gICAgICAgIGNsZWFySW50ZXJ2YWwodGhpcy5pbnRlcnZhbElkKTtcclxuICAgIH1cclxuXHJcbiAgICBvblN1Ym1pdCgpOiB2b2lkIHtcclxuXHJcbiAgICAgICAgdGhpcy5zdWJtaXR0ZWQgPSB0cnVlO1xyXG5cclxuICAgICAgICBsZXQgc2F2aW5nVG9hc3QgPSB0aGlzLl90b2FzdGVyU2VydmljZS5wb3AobmV3IFNwaW5uaW5nVG9hc3QoKSk7XHJcblxyXG4gICAgICAgIGxldCBiYW5rQWNjb3VudCA9IG5ldyBDZW50cmVCYW5rQWNjb3VudER0bygpO1xyXG4gICAgICAgIGJhbmtBY2NvdW50LmluaXQoe1xyXG4gICAgICAgICAgICAnQ2VudHJlSWQnOiB0aGlzLmNlbnRyZUlkLFxyXG4gICAgICAgICAgICAnQnNiJzogdGhpcy5iYW5rQnNiQ29udHJvbC52YWx1ZSxcclxuICAgICAgICAgICAgJ0Fjbic6IHRoaXMuYmFua0FjbkNvbnRyb2wudmFsdWVcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLl9jZW50cmVTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5TYXZlQmFua0FjY291bnREZXRhaWxzJChiYW5rQWNjb3VudClcclxuICAgICAgICAgICAgLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcigpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fdG9hc3RlclNlcnZpY2UucG9wKCdzdWNjZXNzJywgJ0JhbmsgRGV0YWlscyBTYXZlZCcpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zdWJtaXR0ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIHRoaXMub25TYXZlLmVtaXQobnVsbCk7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIChlcnIpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLmNsZWFyKHNhdmluZ1RvYXN0LnRvYXN0SWQsIHNhdmluZ1RvYXN0LnRvYXN0Q29udGFpbmVySWQpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zdWJtaXR0ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0RWRpdE1vZGUoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5vbkVkaXQuZW1pdChmYWxzZSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBlbmFibGVDb250cm9scygpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmJhbmtBY25Db250cm9sLmVuYWJsZSgpO1xyXG4gICAgICAgIHRoaXMuYmFua0JzYkNvbnRyb2wuZW5hYmxlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBkaXNhYmxlQ29udHJvbHMoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5iYW5rQnNiQ29udHJvbC5kaXNhYmxlKCk7XHJcbiAgICAgICAgdGhpcy5iYW5rQWNuQ29udHJvbC5kaXNhYmxlKCk7XHJcbiAgICB9XHJcbn1cclxuIl19
