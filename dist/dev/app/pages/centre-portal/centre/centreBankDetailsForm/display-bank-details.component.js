"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var DisplayBankDetailsComponent = (function () {
    function DisplayBankDetailsComponent() {
        this.onEdit = new core_1.EventEmitter();
    }
    DisplayBankDetailsComponent.prototype.setEditMode = function () {
        this.onEdit.emit(true);
    };
    __decorate([
        core_1.Output(), 
        __metadata('design:type', core_1.EventEmitter)
    ], DisplayBankDetailsComponent.prototype, "onEdit", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], DisplayBankDetailsComponent.prototype, "redactedPayoutMethod", void 0);
    DisplayBankDetailsComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'display-bank-details-cmp',
            templateUrl: './display-bank-details.component.html'
        }), 
        __metadata('design:paramtypes', [])
    ], DisplayBankDetailsComponent);
    return DisplayBankDetailsComponent;
}());
exports.DisplayBankDetailsComponent = DisplayBankDetailsComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NlbnRyZS9jZW50cmVCYW5rRGV0YWlsc0Zvcm0vZGlzcGxheS1iYW5rLWRldGFpbHMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBdUQsZUFBZSxDQUFDLENBQUE7QUFPdkU7SUFBQTtRQUVjLFdBQU0sR0FBc0IsSUFBSSxtQkFBWSxFQUFFLENBQUM7SUFNN0QsQ0FBQztJQUhHLGlEQUFXLEdBQVg7UUFDSSxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMzQixDQUFDO0lBTEQ7UUFBQyxhQUFNLEVBQUU7OytEQUFBO0lBQ1Q7UUFBQyxZQUFLLEVBQUU7OzZFQUFBO0lBUlo7UUFBQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSwwQkFBMEI7WUFDcEMsV0FBVyxFQUFFLHVDQUF1QztTQUN2RCxDQUFDOzttQ0FBQTtJQVNGLGtDQUFDO0FBQUQsQ0FSQSxBQVFDLElBQUE7QUFSWSxtQ0FBMkIsOEJBUXZDLENBQUEiLCJmaWxlIjoiYXBwL3BhZ2VzL2NlbnRyZS1wb3J0YWwvY2VudHJlL2NlbnRyZUJhbmtEZXRhaWxzRm9ybS9kaXNwbGF5LWJhbmstZGV0YWlscy5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgc2VsZWN0b3I6ICdkaXNwbGF5LWJhbmstZGV0YWlscy1jbXAnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2Rpc3BsYXktYmFuay1kZXRhaWxzLmNvbXBvbmVudC5odG1sJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgRGlzcGxheUJhbmtEZXRhaWxzQ29tcG9uZW50IHtcclxuXHJcbiAgICBAT3V0cHV0KCkgb25FZGl0OiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuICAgIEBJbnB1dCgpIHJlZGFjdGVkUGF5b3V0TWV0aG9kOiBzdHJpbmc7XHJcblxyXG4gICAgc2V0RWRpdE1vZGUoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5vbkVkaXQuZW1pdCh0cnVlKTtcclxuICAgIH1cclxufVxyXG4iXX0=
