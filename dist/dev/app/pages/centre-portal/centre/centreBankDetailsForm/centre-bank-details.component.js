"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../index');
var CentreBankDetailsFormComponent = (function () {
    function CentreBankDetailsFormComponent(_centreService) {
        this._centreService = _centreService;
    }
    CentreBankDetailsFormComponent.prototype.ngOnChanges = function (changes) {
        var centreChange = changes['centreId'];
        if (centreChange !== undefined
            && centreChange.currentValue !== undefined
            && centreChange.currentValue !== null
            && centreChange.currentValue !== '') {
            this.GetRedactedPayment(centreChange.currentValue);
        }
        else {
            this.setEditMode(true);
        }
    };
    CentreBankDetailsFormComponent.prototype.bankAccountSaved = function () {
        this.GetRedactedPayment(this.centreId);
    };
    CentreBankDetailsFormComponent.prototype.setEditMode = function (event) {
        this.isEditMode = event;
    };
    CentreBankDetailsFormComponent.prototype.GetRedactedPayment = function (centreId) {
        var _this = this;
        this._centreService
            .GetRedactedPaymentMethod(centreId)
            .subscribe(function (res) {
            _this.redactedPayoutMethod = res;
            _this.setEditMode(_this.redactedPayoutMethod === undefined || _this.redactedPayoutMethod === null);
        });
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], CentreBankDetailsFormComponent.prototype, "centreId", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], CentreBankDetailsFormComponent.prototype, "isApproved", void 0);
    CentreBankDetailsFormComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'centre-bank-details-form-cmp',
            templateUrl: './centre-bank-details.component.html'
        }), 
        __metadata('design:paramtypes', [index_1.CentreService])
    ], CentreBankDetailsFormComponent);
    return CentreBankDetailsFormComponent;
}());
exports.CentreBankDetailsFormComponent = CentreBankDetailsFormComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NlbnRyZS9jZW50cmVCYW5rRGV0YWlsc0Zvcm0vY2VudHJlLWJhbmstZGV0YWlscy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUF5RSxlQUFlLENBQUMsQ0FBQTtBQUV6RixzQkFBOEIsVUFBVSxDQUFDLENBQUE7QUFPekM7SUFRSSx3Q0FBb0IsY0FBNkI7UUFBN0IsbUJBQWMsR0FBZCxjQUFjLENBQWU7SUFDakQsQ0FBQztJQUVELG9EQUFXLEdBQVgsVUFBWSxPQUFzQjtRQUM5QixJQUFJLFlBQVksR0FBaUIsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBRXJELEVBQUUsQ0FBQyxDQUFDLFlBQVksS0FBSyxTQUFTO2VBQ3ZCLFlBQVksQ0FBQyxZQUFZLEtBQUssU0FBUztlQUN2QyxZQUFZLENBQUMsWUFBWSxLQUFLLElBQUk7ZUFDbEMsWUFBWSxDQUFDLFlBQVksS0FBSyxFQUNyQyxDQUFDLENBQUMsQ0FBQztZQUVDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDdkQsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzQixDQUFDO0lBQ0wsQ0FBQztJQUVELHlEQUFnQixHQUFoQjtRQUNJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDM0MsQ0FBQztJQUVELG9EQUFXLEdBQVgsVUFBWSxLQUFVO1FBQ2xCLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO0lBQzVCLENBQUM7SUFFRCwyREFBa0IsR0FBbEIsVUFBbUIsUUFBZ0I7UUFBbkMsaUJBT0M7UUFORyxJQUFJLENBQUMsY0FBYzthQUNkLHdCQUF3QixDQUFDLFFBQVEsQ0FBQzthQUNsQyxTQUFTLENBQUMsVUFBQSxHQUFHO1lBQ1YsS0FBSSxDQUFDLG9CQUFvQixHQUFHLEdBQUcsQ0FBQztZQUNoQyxLQUFJLENBQUMsV0FBVyxDQUFDLEtBQUksQ0FBQyxvQkFBb0IsS0FBSyxTQUFTLElBQUksS0FBSSxDQUFDLG9CQUFvQixLQUFLLElBQUksQ0FBQyxDQUFDO1FBQ3BHLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQXZDRDtRQUFDLFlBQUssRUFBRTs7b0VBQUE7SUFDUjtRQUFDLFlBQUssRUFBRTs7c0VBQUE7SUFSWjtRQUFDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLDhCQUE4QjtZQUN4QyxXQUFXLEVBQUUsc0NBQXNDO1NBQ3RELENBQUM7O3NDQUFBO0lBMkNGLHFDQUFDO0FBQUQsQ0ExQ0EsQUEwQ0MsSUFBQTtBQTFDWSxzQ0FBOEIsaUNBMEMxQyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NlbnRyZS9jZW50cmVCYW5rRGV0YWlsc0Zvcm0vY2VudHJlLWJhbmstZGV0YWlscy5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uQ2hhbmdlcywgSW5wdXQsIFNpbXBsZUNoYW5nZXMsIFNpbXBsZUNoYW5nZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgQ2VudHJlU2VydmljZSB9IGZyb20gJy4uL2luZGV4JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHNlbGVjdG9yOiAnY2VudHJlLWJhbmstZGV0YWlscy1mb3JtLWNtcCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vY2VudHJlLWJhbmstZGV0YWlscy5jb21wb25lbnQuaHRtbCdcclxufSlcclxuZXhwb3J0IGNsYXNzIENlbnRyZUJhbmtEZXRhaWxzRm9ybUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uQ2hhbmdlcyB7XHJcblxyXG4gICAgQElucHV0KCkgY2VudHJlSWQ6IHN0cmluZztcclxuICAgIEBJbnB1dCgpIGlzQXBwcm92ZWQ6IGJvb2xlYW47XHJcbiAgICBwcml2YXRlIHJlZGFjdGVkUGF5b3V0TWV0aG9kOiBzdHJpbmc7XHJcbiAgICBwcml2YXRlIGlzRWRpdE1vZGU6IGJvb2xlYW47XHJcblxyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX2NlbnRyZVNlcnZpY2U6IENlbnRyZVNlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKSB7XHJcbiAgICAgICAgbGV0IGNlbnRyZUNoYW5nZTogU2ltcGxlQ2hhbmdlID0gY2hhbmdlc1snY2VudHJlSWQnXTtcclxuXHJcbiAgICAgICAgaWYgKGNlbnRyZUNoYW5nZSAhPT0gdW5kZWZpbmVkXHJcbiAgICAgICAgICAgICYmIGNlbnRyZUNoYW5nZS5jdXJyZW50VmFsdWUgIT09IHVuZGVmaW5lZFxyXG4gICAgICAgICAgICAmJiBjZW50cmVDaGFuZ2UuY3VycmVudFZhbHVlICE9PSBudWxsXHJcbiAgICAgICAgICAgICYmIGNlbnRyZUNoYW5nZS5jdXJyZW50VmFsdWUgIT09ICcnXHJcbiAgICAgICAgKSB7XHJcblxyXG4gICAgICAgICAgICB0aGlzLkdldFJlZGFjdGVkUGF5bWVudChjZW50cmVDaGFuZ2UuY3VycmVudFZhbHVlKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnNldEVkaXRNb2RlKHRydWUpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBiYW5rQWNjb3VudFNhdmVkKCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuR2V0UmVkYWN0ZWRQYXltZW50KHRoaXMuY2VudHJlSWQpO1xyXG4gICAgfVxyXG5cclxuICAgIHNldEVkaXRNb2RlKGV2ZW50OiBhbnkpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmlzRWRpdE1vZGUgPSBldmVudDtcclxuICAgIH1cclxuXHJcbiAgICBHZXRSZWRhY3RlZFBheW1lbnQoY2VudHJlSWQ6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuX2NlbnRyZVNlcnZpY2VcclxuICAgICAgICAgICAgLkdldFJlZGFjdGVkUGF5bWVudE1ldGhvZChjZW50cmVJZClcclxuICAgICAgICAgICAgLnN1YnNjcmliZShyZXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yZWRhY3RlZFBheW91dE1ldGhvZCA9IHJlcztcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0RWRpdE1vZGUodGhpcy5yZWRhY3RlZFBheW91dE1ldGhvZCA9PT0gdW5kZWZpbmVkIHx8IHRoaXMucmVkYWN0ZWRQYXlvdXRNZXRob2QgPT09IG51bGwpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG4iXX0=
