"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../../../shared/index');
var index_2 = require('./index');
var CentreModule = (function () {
    function CentreModule() {
    }
    CentreModule = __decorate([
        core_1.NgModule({
            imports: [
                index_1.SharedModule
            ],
            declarations: [
                index_2.CentreComponent,
                index_2.CentreFormComponent,
                index_2.CentreBankDetailsFormComponent,
                index_2.SaveBankDetailsComponent,
                index_2.DisplayBankDetailsComponent
            ],
            providers: [index_2.CentreService],
            exports: [
                index_2.CentreComponent,
                index_2.CentreFormComponent,
                index_2.CentreBankDetailsFormComponent,
                index_2.SaveBankDetailsComponent,
                index_2.DisplayBankDetailsComponent
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], CentreModule);
    return CentreModule;
}());
exports.CentreModule = CentreModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NlbnRyZS9jZW50cmUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBeUIsZUFBZSxDQUFDLENBQUE7QUFFekMsc0JBQTZCLHVCQUF1QixDQUFDLENBQUE7QUFDckQsc0JBR08sU0FBUyxDQUFDLENBQUE7QUF1QmpCO0lBQUE7SUFBNEIsQ0FBQztJQXJCN0I7UUFBQyxlQUFRLENBQUM7WUFDTixPQUFPLEVBQUU7Z0JBQ0wsb0JBQVk7YUFDZjtZQUNELFlBQVksRUFBRTtnQkFDVix1QkFBZTtnQkFDZiwyQkFBbUI7Z0JBQ25CLHNDQUE4QjtnQkFDOUIsZ0NBQXdCO2dCQUN4QixtQ0FBMkI7YUFDOUI7WUFDRCxTQUFTLEVBQUUsQ0FBQyxxQkFBYSxDQUFDO1lBQzFCLE9BQU8sRUFBRTtnQkFDTCx1QkFBZTtnQkFDZiwyQkFBbUI7Z0JBQ25CLHNDQUE4QjtnQkFDOUIsZ0NBQXdCO2dCQUN4QixtQ0FBMkI7YUFDOUI7U0FDSixDQUFDOztvQkFBQTtJQUUwQixtQkFBQztBQUFELENBQTVCLEFBQTZCLElBQUE7QUFBaEIsb0JBQVksZUFBSSxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NlbnRyZS9jZW50cmUubW9kdWxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmltcG9ydCB7IFNoYXJlZE1vZHVsZSB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9pbmRleCc7XHJcbmltcG9ydCB7XHJcbiAgICBDZW50cmVDb21wb25lbnQsIENlbnRyZUZvcm1Db21wb25lbnQsIENlbnRyZUJhbmtEZXRhaWxzRm9ybUNvbXBvbmVudCxcclxuICAgIFNhdmVCYW5rRGV0YWlsc0NvbXBvbmVudCwgRGlzcGxheUJhbmtEZXRhaWxzQ29tcG9uZW50LCBDZW50cmVTZXJ2aWNlXHJcbn0gZnJvbSAnLi9pbmRleCc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgaW1wb3J0czogW1xyXG4gICAgICAgIFNoYXJlZE1vZHVsZVxyXG4gICAgXSxcclxuICAgIGRlY2xhcmF0aW9uczogW1xyXG4gICAgICAgIENlbnRyZUNvbXBvbmVudCxcclxuICAgICAgICBDZW50cmVGb3JtQ29tcG9uZW50LFxyXG4gICAgICAgIENlbnRyZUJhbmtEZXRhaWxzRm9ybUNvbXBvbmVudCxcclxuICAgICAgICBTYXZlQmFua0RldGFpbHNDb21wb25lbnQsXHJcbiAgICAgICAgRGlzcGxheUJhbmtEZXRhaWxzQ29tcG9uZW50XHJcbiAgICBdLFxyXG4gICAgcHJvdmlkZXJzOiBbQ2VudHJlU2VydmljZV0sXHJcbiAgICBleHBvcnRzOiBbXHJcbiAgICAgICAgQ2VudHJlQ29tcG9uZW50LFxyXG4gICAgICAgIENlbnRyZUZvcm1Db21wb25lbnQsXHJcbiAgICAgICAgQ2VudHJlQmFua0RldGFpbHNGb3JtQ29tcG9uZW50LFxyXG4gICAgICAgIFNhdmVCYW5rRGV0YWlsc0NvbXBvbmVudCxcclxuICAgICAgICBEaXNwbGF5QmFua0RldGFpbHNDb21wb25lbnRcclxuICAgIF1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBDZW50cmVNb2R1bGUgeyB9XHJcbiJdfQ==
