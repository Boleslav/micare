"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var angular2_toaster_1 = require('angular2-toaster');
var index_1 = require('../index');
var index_2 = require('../../../../api/index');
var index_3 = require('../../../../core/index');
var index_4 = require('../../../../shared/index');
var CentreFormComponent = (function () {
    function CentreFormComponent(fb, _centreService, _userService, _toasterService) {
        this._centreService = _centreService;
        this._userService = _userService;
        this._toasterService = _toasterService;
        this.centreLoaded = false;
        this.approvalStatus = -1;
        this.timezones = null;
        this.submitted = false;
        this.createUpdateCentreDto = new index_2.CreateUpdateCentreDto();
        this.createUpdateCentreDto.init({ 'Id': '' });
        this.form = fb.group({
            'centreName': ['', forms_1.Validators.required],
            'streetAddress': ['', forms_1.Validators.required],
            'suburb': ['', forms_1.Validators.required],
            'postcode': ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.pattern('(\\d{4})')])],
            'state': ['', forms_1.Validators.required],
            'timezone': ['', forms_1.Validators.required],
            'postalAddress': ['', forms_1.Validators.required],
            'contactPhone': ['', forms_1.Validators.required],
            'contactName': ['']
        });
        this.centreName = this.form.controls['centreName'];
        this.streetAddress = this.form.controls['streetAddress'];
        this.suburb = this.form.controls['suburb'];
        this.postcode = this.form.controls['postcode'];
        this.state = this.form.controls['state'];
        this.postalAddress = this.form.controls['postalAddress'];
        this.contactPhone = this.form.controls['contactPhone'];
        this.contactName = this.form.controls['contactName'];
        this.timezone = this.form.controls['timezone'];
    }
    CentreFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this._userService.SelectedCentre !== null) {
            this._centreService
                .GetCentre$(this._userService.SelectedCentre.id)
                .subscribe(function (centreDto) {
                _this.createUpdateCentreDto = index_2.CreateUpdateCentreDto.fromJS(centreDto.toJSON());
                _this.approvalStatus = centreDto.approvalStatus;
                _this.disablePostalAddressInput();
                _this.centreLoaded = true;
            });
        }
        else {
            this.createUpdateCentreDto = new index_2.CreateUpdateCentreDto();
            this.centreLoaded = true;
        }
        this._centreService
            .getTimeZones$()
            .subscribe(function (res) { return _this.timezones = res; });
    };
    CentreFormComponent.prototype.onSubmit = function () {
        var _this = this;
        var savingToast = this._toasterService.pop(new index_4.SpinningToast());
        this.submitted = true;
        this._centreService
            .CreateCentre$(this.createUpdateCentreDto)
            .subscribe(function (res) {
            _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
            _this._toasterService.pop('success', 'Saved');
            _this.submitted = false;
            _this.createUpdateCentreDto = res;
            _this._userService.Centres = null;
            var selectedCentre = new index_2.CentreDto();
            selectedCentre.init(_this.createUpdateCentreDto.toJSON());
            _this._userService.SelectedCentre = selectedCentre;
        }, function (err) { return _this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId); });
    };
    CentreFormComponent.prototype.disablePostalAddressInput = function () {
        if (this.createUpdateCentreDto.postToPhysicalAddress) {
            this.postalAddress.reset();
            this.postalAddress.disable();
        }
        else {
            this.postalAddress.enable();
            this.postalAddress.setValidators(forms_1.Validators.required);
        }
    };
    CentreFormComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'centre-form-cmp',
            templateUrl: './centreForm.component.html',
            styleUrls: ['centreForm.component.css']
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, index_1.CentreService, index_3.CentreUserService, angular2_toaster_1.ToasterService])
    ], CentreFormComponent);
    return CentreFormComponent;
}());
exports.CentreFormComponent = CentreFormComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NlbnRyZS9jZW50cmVGb3JtL2NlbnRyZUZvcm0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBa0MsZUFBZSxDQUFDLENBQUE7QUFDbEQsc0JBQW9FLGdCQUFnQixDQUFDLENBQUE7QUFDckYsaUNBQStCLGtCQUFrQixDQUFDLENBQUE7QUFFbEQsc0JBQThCLFVBQVUsQ0FBQyxDQUFBO0FBQ3pDLHNCQUFpRCx1QkFBdUIsQ0FBQyxDQUFBO0FBQ3pFLHNCQUFrQyx3QkFBd0IsQ0FBQyxDQUFBO0FBQzNELHNCQUE4QiwwQkFBMEIsQ0FBQyxDQUFBO0FBU3pEO0lBbUJJLDZCQUFZLEVBQWUsRUFDZixjQUE2QixFQUM3QixZQUErQixFQUMvQixlQUErQjtRQUYvQixtQkFBYyxHQUFkLGNBQWMsQ0FBZTtRQUM3QixpQkFBWSxHQUFaLFlBQVksQ0FBbUI7UUFDL0Isb0JBQWUsR0FBZixlQUFlLENBQWdCO1FBVG5DLGlCQUFZLEdBQVksS0FBSyxDQUFDO1FBRTlCLG1CQUFjLEdBQVcsQ0FBQyxDQUFDLENBQUM7UUFDNUIsY0FBUyxHQUFhLElBQUksQ0FBQztRQUMzQixjQUFTLEdBQVksS0FBSyxDQUFDO1FBTy9CLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLDZCQUFxQixFQUFFLENBQUM7UUFDekQsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQzlDLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQztZQUNqQixZQUFZLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxRQUFRLENBQUM7WUFDdkMsZUFBZSxFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsUUFBUSxDQUFDO1lBQzFDLFFBQVEsRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLFFBQVEsQ0FBQztZQUNuQyxVQUFVLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxrQkFBVSxDQUFDLFFBQVEsRUFBRSxrQkFBVSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDM0YsT0FBTyxFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsUUFBUSxDQUFDO1lBQ2xDLFVBQVUsRUFBRSxDQUFDLEVBQUUsRUFBRSxrQkFBVSxDQUFDLFFBQVEsQ0FBQztZQUNyQyxlQUFlLEVBQUUsQ0FBQyxFQUFFLEVBQUUsa0JBQVUsQ0FBQyxRQUFRLENBQUM7WUFDMUMsY0FBYyxFQUFFLENBQUMsRUFBRSxFQUFFLGtCQUFVLENBQUMsUUFBUSxDQUFDO1lBQ3pDLGFBQWEsRUFBRSxDQUFDLEVBQUUsQ0FBQztTQUN0QixDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ25ELElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDekQsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMzQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQy9DLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDekMsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUN6RCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ3ZELElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDckQsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUNuRCxDQUFDO0lBRUQsc0NBQVEsR0FBUjtRQUFBLGlCQW1CQztRQWpCRyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQzVDLElBQUksQ0FBQyxjQUFjO2lCQUNkLFVBQVUsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxFQUFFLENBQUM7aUJBQy9DLFNBQVMsQ0FBQyxVQUFDLFNBQVM7Z0JBQ2pCLEtBQUksQ0FBQyxxQkFBcUIsR0FBRyw2QkFBcUIsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7Z0JBQzlFLEtBQUksQ0FBQyxjQUFjLEdBQUcsU0FBUyxDQUFDLGNBQWMsQ0FBQztnQkFDL0MsS0FBSSxDQUFDLHlCQUF5QixFQUFFLENBQUM7Z0JBQ2pDLEtBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1lBQzdCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksNkJBQXFCLEVBQUUsQ0FBQztZQUN6RCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztRQUM3QixDQUFDO1FBRUQsSUFBSSxDQUFDLGNBQWM7YUFDZCxhQUFhLEVBQUU7YUFDZixTQUFTLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxLQUFJLENBQUMsU0FBUyxHQUFHLEdBQUcsRUFBcEIsQ0FBb0IsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCxzQ0FBUSxHQUFSO1FBQUEsaUJBbUJDO1FBakJHLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUkscUJBQWEsRUFBRSxDQUFDLENBQUM7UUFDaEUsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFFdEIsSUFBSSxDQUFDLGNBQWM7YUFDZCxhQUFhLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDO2FBQ3pDLFNBQVMsQ0FBQyxVQUFBLEdBQUc7WUFDVixLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1lBQzlFLEtBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxPQUFPLENBQUMsQ0FBQztZQUM3QyxLQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixLQUFJLENBQUMscUJBQXFCLEdBQUcsR0FBRyxDQUFDO1lBRWpDLEtBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztZQUNqQyxJQUFJLGNBQWMsR0FBRyxJQUFJLGlCQUFTLEVBQUUsQ0FBQztZQUNyQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO1lBQ3pELEtBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxHQUFHLGNBQWMsQ0FBQztRQUN0RCxDQUFDLEVBQ0QsVUFBQyxHQUFHLElBQUssT0FBQSxLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUE3RSxDQUE2RSxDQUFDLENBQUM7SUFDaEcsQ0FBQztJQUVELHVEQUF5QixHQUF6QjtRQUVJLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUM7WUFDbkQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUMzQixJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ2pDLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLENBQUM7WUFDNUIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsa0JBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMxRCxDQUFDO0lBQ0wsQ0FBQztJQTNHTDtRQUFDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLGlCQUFpQjtZQUMzQixXQUFXLEVBQUUsNkJBQTZCO1lBQzFDLFNBQVMsRUFBRSxDQUFDLDBCQUEwQixDQUFDO1NBQzFDLENBQUM7OzJCQUFBO0lBdUdGLDBCQUFDO0FBQUQsQ0FyR0EsQUFxR0MsSUFBQTtBQXJHWSwyQkFBbUIsc0JBcUcvQixDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NlbnRyZS9jZW50cmVGb3JtL2NlbnRyZUZvcm0uY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybUdyb3VwLCBBYnN0cmFjdENvbnRyb2wsIEZvcm1CdWlsZGVyLCBWYWxpZGF0b3JzIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBUb2FzdGVyU2VydmljZSB9IGZyb20gJ2FuZ3VsYXIyLXRvYXN0ZXInO1xyXG5cclxuaW1wb3J0IHsgQ2VudHJlU2VydmljZSB9IGZyb20gJy4uL2luZGV4JztcclxuaW1wb3J0IHsgQ2VudHJlRHRvLCBDcmVhdGVVcGRhdGVDZW50cmVEdG8gfSBmcm9tICcuLi8uLi8uLi8uLi9hcGkvaW5kZXgnO1xyXG5pbXBvcnQgeyBDZW50cmVVc2VyU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uL2NvcmUvaW5kZXgnO1xyXG5pbXBvcnQgeyBTcGlubmluZ1RvYXN0IH0gZnJvbSAnLi4vLi4vLi4vLi4vc2hhcmVkL2luZGV4JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHNlbGVjdG9yOiAnY2VudHJlLWZvcm0tY21wJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9jZW50cmVGb3JtLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWydjZW50cmVGb3JtLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIENlbnRyZUZvcm1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIHByaXZhdGUgZm9ybTogRm9ybUdyb3VwO1xyXG4gICAgcHJpdmF0ZSBjZW50cmVOYW1lOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgICBwcml2YXRlIHN0cmVldEFkZHJlc3M6IEFic3RyYWN0Q29udHJvbDtcclxuICAgIHByaXZhdGUgc3VidXJiOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgICBwcml2YXRlIHBvc3Rjb2RlOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgICBwcml2YXRlIHN0YXRlOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgICBwcml2YXRlIHBvc3RhbEFkZHJlc3M6IEFic3RyYWN0Q29udHJvbDtcclxuICAgIHByaXZhdGUgY29udGFjdFBob25lOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgICBwcml2YXRlIGNvbnRhY3ROYW1lOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgICBwcml2YXRlIHRpbWV6b25lOiBBYnN0cmFjdENvbnRyb2w7XHJcblxyXG4gICAgcHJpdmF0ZSBjZW50cmVMb2FkZWQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIHByaXZhdGUgY3JlYXRlVXBkYXRlQ2VudHJlRHRvOiBDcmVhdGVVcGRhdGVDZW50cmVEdG87XHJcbiAgICBwcml2YXRlIGFwcHJvdmFsU3RhdHVzOiBudW1iZXIgPSAtMTtcclxuICAgIHByaXZhdGUgdGltZXpvbmVzOiBzdHJpbmdbXSA9IG51bGw7XHJcbiAgICBwcml2YXRlIHN1Ym1pdHRlZDogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGZiOiBGb3JtQnVpbGRlcixcclxuICAgICAgICBwcml2YXRlIF9jZW50cmVTZXJ2aWNlOiBDZW50cmVTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgX3VzZXJTZXJ2aWNlOiBDZW50cmVVc2VyU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIF90b2FzdGVyU2VydmljZTogVG9hc3RlclNlcnZpY2UpIHtcclxuXHJcbiAgICAgICAgdGhpcy5jcmVhdGVVcGRhdGVDZW50cmVEdG8gPSBuZXcgQ3JlYXRlVXBkYXRlQ2VudHJlRHRvKCk7XHJcbiAgICAgICAgdGhpcy5jcmVhdGVVcGRhdGVDZW50cmVEdG8uaW5pdCh7ICdJZCc6ICcnIH0pO1xyXG4gICAgICAgIHRoaXMuZm9ybSA9IGZiLmdyb3VwKHtcclxuICAgICAgICAgICAgJ2NlbnRyZU5hbWUnOiBbJycsIFZhbGlkYXRvcnMucmVxdWlyZWRdLFxyXG4gICAgICAgICAgICAnc3RyZWV0QWRkcmVzcyc6IFsnJywgVmFsaWRhdG9ycy5yZXF1aXJlZF0sXHJcbiAgICAgICAgICAgICdzdWJ1cmInOiBbJycsIFZhbGlkYXRvcnMucmVxdWlyZWRdLFxyXG4gICAgICAgICAgICAncG9zdGNvZGUnOiBbJycsIFZhbGlkYXRvcnMuY29tcG9zZShbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5wYXR0ZXJuKCcoXFxcXGR7NH0pJyldKV0sXHJcbiAgICAgICAgICAgICdzdGF0ZSc6IFsnJywgVmFsaWRhdG9ycy5yZXF1aXJlZF0sXHJcbiAgICAgICAgICAgICd0aW1lem9uZSc6IFsnJywgVmFsaWRhdG9ycy5yZXF1aXJlZF0sXHJcbiAgICAgICAgICAgICdwb3N0YWxBZGRyZXNzJzogWycnLCBWYWxpZGF0b3JzLnJlcXVpcmVkXSxcclxuICAgICAgICAgICAgJ2NvbnRhY3RQaG9uZSc6IFsnJywgVmFsaWRhdG9ycy5yZXF1aXJlZF0sXHJcbiAgICAgICAgICAgICdjb250YWN0TmFtZSc6IFsnJ11cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgdGhpcy5jZW50cmVOYW1lID0gdGhpcy5mb3JtLmNvbnRyb2xzWydjZW50cmVOYW1lJ107XHJcbiAgICAgICAgdGhpcy5zdHJlZXRBZGRyZXNzID0gdGhpcy5mb3JtLmNvbnRyb2xzWydzdHJlZXRBZGRyZXNzJ107XHJcbiAgICAgICAgdGhpcy5zdWJ1cmIgPSB0aGlzLmZvcm0uY29udHJvbHNbJ3N1YnVyYiddO1xyXG4gICAgICAgIHRoaXMucG9zdGNvZGUgPSB0aGlzLmZvcm0uY29udHJvbHNbJ3Bvc3Rjb2RlJ107XHJcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHRoaXMuZm9ybS5jb250cm9sc1snc3RhdGUnXTtcclxuICAgICAgICB0aGlzLnBvc3RhbEFkZHJlc3MgPSB0aGlzLmZvcm0uY29udHJvbHNbJ3Bvc3RhbEFkZHJlc3MnXTtcclxuICAgICAgICB0aGlzLmNvbnRhY3RQaG9uZSA9IHRoaXMuZm9ybS5jb250cm9sc1snY29udGFjdFBob25lJ107XHJcbiAgICAgICAgdGhpcy5jb250YWN0TmFtZSA9IHRoaXMuZm9ybS5jb250cm9sc1snY29udGFjdE5hbWUnXTtcclxuICAgICAgICB0aGlzLnRpbWV6b25lID0gdGhpcy5mb3JtLmNvbnRyb2xzWyd0aW1lem9uZSddO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xyXG5cclxuICAgICAgICBpZiAodGhpcy5fdXNlclNlcnZpY2UuU2VsZWN0ZWRDZW50cmUgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgdGhpcy5fY2VudHJlU2VydmljZVxyXG4gICAgICAgICAgICAgICAgLkdldENlbnRyZSQodGhpcy5fdXNlclNlcnZpY2UuU2VsZWN0ZWRDZW50cmUuaWQpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKChjZW50cmVEdG8pID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNyZWF0ZVVwZGF0ZUNlbnRyZUR0byA9IENyZWF0ZVVwZGF0ZUNlbnRyZUR0by5mcm9tSlMoY2VudHJlRHRvLnRvSlNPTigpKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmFwcHJvdmFsU3RhdHVzID0gY2VudHJlRHRvLmFwcHJvdmFsU3RhdHVzO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGlzYWJsZVBvc3RhbEFkZHJlc3NJbnB1dCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2VudHJlTG9hZGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuY3JlYXRlVXBkYXRlQ2VudHJlRHRvID0gbmV3IENyZWF0ZVVwZGF0ZUNlbnRyZUR0bygpO1xyXG4gICAgICAgICAgICB0aGlzLmNlbnRyZUxvYWRlZCA9IHRydWU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLl9jZW50cmVTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5nZXRUaW1lWm9uZXMkKClcclxuICAgICAgICAgICAgLnN1YnNjcmliZShyZXMgPT4gdGhpcy50aW1lem9uZXMgPSByZXMpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uU3VibWl0KCk6IHZvaWQge1xyXG5cclxuICAgICAgICBsZXQgc2F2aW5nVG9hc3QgPSB0aGlzLl90b2FzdGVyU2VydmljZS5wb3AobmV3IFNwaW5uaW5nVG9hc3QoKSk7XHJcbiAgICAgICAgdGhpcy5zdWJtaXR0ZWQgPSB0cnVlO1xyXG5cclxuICAgICAgICB0aGlzLl9jZW50cmVTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5DcmVhdGVDZW50cmUkKHRoaXMuY3JlYXRlVXBkYXRlQ2VudHJlRHRvKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKHJlcyA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcihzYXZpbmdUb2FzdC50b2FzdElkLCBzYXZpbmdUb2FzdC50b2FzdENvbnRhaW5lcklkKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlLnBvcCgnc3VjY2VzcycsICdTYXZlZCcpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zdWJtaXR0ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIHRoaXMuY3JlYXRlVXBkYXRlQ2VudHJlRHRvID0gcmVzO1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuX3VzZXJTZXJ2aWNlLkNlbnRyZXMgPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgbGV0IHNlbGVjdGVkQ2VudHJlID0gbmV3IENlbnRyZUR0bygpO1xyXG4gICAgICAgICAgICAgICAgc2VsZWN0ZWRDZW50cmUuaW5pdCh0aGlzLmNyZWF0ZVVwZGF0ZUNlbnRyZUR0by50b0pTT04oKSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl91c2VyU2VydmljZS5TZWxlY3RlZENlbnRyZSA9IHNlbGVjdGVkQ2VudHJlO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAoZXJyKSA9PiB0aGlzLl90b2FzdGVyU2VydmljZS5jbGVhcihzYXZpbmdUb2FzdC50b2FzdElkLCBzYXZpbmdUb2FzdC50b2FzdENvbnRhaW5lcklkKSk7XHJcbiAgICB9XHJcblxyXG4gICAgZGlzYWJsZVBvc3RhbEFkZHJlc3NJbnB1dCgpOiB2b2lkIHtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuY3JlYXRlVXBkYXRlQ2VudHJlRHRvLnBvc3RUb1BoeXNpY2FsQWRkcmVzcykge1xyXG4gICAgICAgICAgICB0aGlzLnBvc3RhbEFkZHJlc3MucmVzZXQoKTtcclxuICAgICAgICAgICAgdGhpcy5wb3N0YWxBZGRyZXNzLmRpc2FibGUoKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnBvc3RhbEFkZHJlc3MuZW5hYmxlKCk7XHJcbiAgICAgICAgICAgIHRoaXMucG9zdGFsQWRkcmVzcy5zZXRWYWxpZGF0b3JzKFZhbGlkYXRvcnMucmVxdWlyZWQpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=
