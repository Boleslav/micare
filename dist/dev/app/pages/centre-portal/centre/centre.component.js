"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var CentreComponent = (function () {
    function CentreComponent() {
    }
    CentreComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'centre-cmp',
            templateUrl: './centre.component.html'
        }), 
        __metadata('design:paramtypes', [])
    ], CentreComponent);
    return CentreComponent;
}());
exports.CentreComponent = CentreComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NlbnRyZS9jZW50cmUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBd0IsZUFBZSxDQUFDLENBQUE7QUFReEM7SUFBQTtJQUE4QixDQUFDO0lBTi9CO1FBQUMsZ0JBQVMsQ0FBQztZQUNWLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNoQixRQUFRLEVBQUUsWUFBWTtZQUN0QixXQUFXLEVBQUUseUJBQXlCO1NBQ3pDLENBQUM7O3VCQUFBO0lBRTRCLHNCQUFDO0FBQUQsQ0FBOUIsQUFBK0IsSUFBQTtBQUFsQix1QkFBZSxrQkFBRyxDQUFBIiwiZmlsZSI6ImFwcC9wYWdlcy9jZW50cmUtcG9ydGFsL2NlbnRyZS9jZW50cmUuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcblx0bW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHNlbGVjdG9yOiAnY2VudHJlLWNtcCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vY2VudHJlLmNvbXBvbmVudC5odG1sJ1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIENlbnRyZUNvbXBvbmVudCB7fVxyXG4iXX0=
