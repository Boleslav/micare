"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('./shared/index');
var angular2_toaster_1 = require('angular2-toaster');
var AppComponent = (function () {
    function AppComponent(viewContainerRef, _mixpanelService) {
        this.viewContainerRef = viewContainerRef;
        _mixpanelService.init(index_1.Config.MIXPANEL_TOKEN);
        this.toasterConfig =
            new angular2_toaster_1.ToasterConfig({
                showCloseButton: true,
                tapToDismiss: false,
                timeout: { success: 2000, info: 2000, error: 3000 }
            });
    }
    AppComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'sd-app',
            templateUrl: 'app.component.html',
            styles: ["h1 { color: red; }"]
        }), 
        __metadata('design:paramtypes', [core_1.ViewContainerRef, index_1.MixpanelService])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9hcHAuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBNEMsZUFBZSxDQUFDLENBQUE7QUFDNUQsc0JBQXdDLGdCQUFnQixDQUFDLENBQUE7QUFDekQsaUNBQThCLGtCQUFrQixDQUFDLENBQUE7QUFhakQ7SUFJQyxzQkFBbUIsZ0JBQWtDLEVBQ3BELGdCQUFpQztRQUlqQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsZ0JBQWdCLENBQUM7UUFFekMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLGNBQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUU3QyxJQUFJLENBQUMsYUFBYTtZQUNqQixJQUFJLGdDQUFhLENBQUM7Z0JBQ2pCLGVBQWUsRUFBRSxJQUFJO2dCQUNyQixZQUFZLEVBQUUsS0FBSztnQkFDbkIsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUU7YUFDbkQsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQTFCRjtRQUFDLGdCQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLFFBQVE7WUFDbEIsV0FBVyxFQUFFLG9CQUFvQjtZQUNqQyxNQUFNLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQztTQUM5QixDQUFDOztvQkFBQTtJQXNCRixtQkFBQztBQUFELENBcEJBLEFBb0JDLElBQUE7QUFwQlksb0JBQVksZUFvQnhCLENBQUEiLCJmaWxlIjoiYXBwL2FwcC5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIFZpZXdDb250YWluZXJSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29uZmlnLCBNaXhwYW5lbFNlcnZpY2UgfSBmcm9tICcuL3NoYXJlZC9pbmRleCc7XHJcbmltcG9ydCB7IFRvYXN0ZXJDb25maWcgfSBmcm9tICdhbmd1bGFyMi10b2FzdGVyJztcclxuXHJcbi8qKlxyXG4gKiBUaGlzIGNsYXNzIHJlcHJlc2VudHMgdGhlIG1haW4gYXBwbGljYXRpb24gY29tcG9uZW50LiBXaXRoaW4gdGhlIEBSb3V0ZXMgYW5ub3RhdGlvbiBpcyB0aGUgY29uZmlndXJhdGlvbiBvZiB0aGVcclxuICogYXBwbGljYXRpb25zIHJvdXRlcywgY29uZmlndXJpbmcgdGhlIHBhdGhzIGZvciB0aGUgbGF6eSBsb2FkZWQgY29tcG9uZW50cyAoSG9tZUNvbXBvbmVudCwgQWJvdXRDb21wb25lbnQpLlxyXG4gKi9cclxuQENvbXBvbmVudCh7XHJcblx0bW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuXHRzZWxlY3RvcjogJ3NkLWFwcCcsXHJcblx0dGVtcGxhdGVVcmw6ICdhcHAuY29tcG9uZW50Lmh0bWwnLFxyXG5cdHN0eWxlczogW2BoMSB7IGNvbG9yOiByZWQ7IH1gXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEFwcENvbXBvbmVudCB7XHJcblxyXG5cdHB1YmxpYyB0b2FzdGVyQ29uZmlnOiBUb2FzdGVyQ29uZmlnO1xyXG5cdHByaXZhdGUgdmlld0NvbnRhaW5lclJlZjogVmlld0NvbnRhaW5lclJlZjtcclxuXHRwdWJsaWMgY29uc3RydWN0b3Iodmlld0NvbnRhaW5lclJlZjogVmlld0NvbnRhaW5lclJlZixcclxuXHRcdF9taXhwYW5lbFNlcnZpY2U6IE1peHBhbmVsU2VydmljZVxyXG5cdCkge1xyXG5cclxuXHRcdC8vIFlvdSBuZWVkIHRoaXMgc21hbGwgaGFjayBpbiBvcmRlciB0byBjYXRjaCBhcHBsaWNhdGlvbiByb290IHZpZXcgY29udGFpbmVyIHJlZlxyXG5cdFx0dGhpcy52aWV3Q29udGFpbmVyUmVmID0gdmlld0NvbnRhaW5lclJlZjtcclxuXHJcblx0XHRfbWl4cGFuZWxTZXJ2aWNlLmluaXQoQ29uZmlnLk1JWFBBTkVMX1RPS0VOKTtcclxuXHJcblx0XHR0aGlzLnRvYXN0ZXJDb25maWcgPVxyXG5cdFx0XHRuZXcgVG9hc3RlckNvbmZpZyh7XHJcblx0XHRcdFx0c2hvd0Nsb3NlQnV0dG9uOiB0cnVlLFxyXG5cdFx0XHRcdHRhcFRvRGlzbWlzczogZmFsc2UsXHJcblx0XHRcdFx0dGltZW91dDogeyBzdWNjZXNzOiAyMDAwLCBpbmZvOiAyMDAwLCBlcnJvcjogMzAwMCB9XHJcblx0XHRcdH0pO1xyXG5cdH1cclxufVxyXG4iXX0=
