"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var rxjs_1 = require('rxjs');
var index_1 = require('./index');
var index_2 = require('../shared/index');
var ApiClientService = (function (_super) {
    __extends(ApiClientService, _super);
    function ApiClientService(configuration, http) {
        _super.call(this, configuration, http, index_2.Config.API);
        this._http = http;
    }
    ApiClientService.prototype.token = function (grant_type, username, password, client_id) {
        var _this = this;
        var url_ = index_2.Config.API + '/token';
        var content_ = new http_1.URLSearchParams();
        if (grant_type === null)
            throw new Error('The parameter \'grant_type\' cannot be null.');
        content_.append('grant_type', grant_type.toString());
        if (username === null)
            throw new Error('The parameter \'username\' cannot be null.');
        content_.append('username', username.toString());
        if (password === null)
            throw new Error('The parameter \'password\' cannot be null.');
        content_.append('password', password.toString());
        if (client_id === null)
            throw new Error('The parameter \'client_id\' cannot be null.');
        content_.append('client_id', client_id.toString());
        return this._http.request(url_, {
            body: content_.toString(),
            method: 'post',
            headers: new http_1.Headers({
                'Content-Type': undefined
            })
        }).map(function (response) {
            return _this.transformResult(url_, response, function (response) { return _this.processToken(response); });
        }).catch(function (response, caught) {
            if (response instanceof http_1.Response) {
                try {
                    return rxjs_1.Observable.of(_this.transformResult(url_, response, function (response) { return _this.processToken(response); }));
                }
                catch (e) {
                    return rxjs_1.Observable.throw(e);
                }
            }
            else
                return rxjs_1.Observable.throw(response);
        });
    };
    ApiClientService = __decorate([
        core_1.Injectable(),
        __param(0, core_1.Inject(index_1.BaseConfiguration)), 
        __metadata('design:paramtypes', [index_1.BaseConfiguration, index_1.HttpService])
    ], ApiClientService);
    return ApiClientService;
}(index_1.Client));
exports.ApiClientService = ApiClientService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9hcGkvYXBpLWNsaWVudC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLHFCQUFtQyxlQUFlLENBQUMsQ0FBQTtBQUNuRCxxQkFBbUQsZUFBZSxDQUFDLENBQUE7QUFDbkUscUJBQTJCLE1BQU0sQ0FBQyxDQUFBO0FBRWxDLHNCQUF1RCxTQUFTLENBQUMsQ0FBQTtBQUNqRSxzQkFBdUIsaUJBQWlCLENBQUMsQ0FBQTtBQUd6QztJQUFzQyxvQ0FBTTtJQUd4QywwQkFDK0IsYUFBZ0MsRUFDM0QsSUFBaUI7UUFDakIsa0JBQU0sYUFBYSxFQUFFLElBQUksRUFBRSxjQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDdkMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7SUFDdEIsQ0FBQztJQUVNLGdDQUFLLEdBQVosVUFBYSxVQUFrQixFQUFFLFFBQWdCLEVBQUUsUUFBZ0IsRUFBRSxTQUFpQjtRQUF0RixpQkFrQ0M7UUFqQ0csSUFBSSxJQUFJLEdBQUcsY0FBTSxDQUFDLEdBQUcsR0FBRyxRQUFRLENBQUM7UUFFakMsSUFBTSxRQUFRLEdBQUcsSUFBSSxzQkFBZSxFQUFFLENBQUM7UUFDdkMsRUFBRSxDQUFDLENBQUMsVUFBVSxLQUFLLElBQUksQ0FBQztZQUNwQixNQUFNLElBQUksS0FBSyxDQUFDLDhDQUE4QyxDQUFDLENBQUM7UUFDcEUsUUFBUSxDQUFDLE1BQU0sQ0FBQyxZQUFZLEVBQUUsVUFBVSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7UUFDckQsRUFBRSxDQUFDLENBQUMsUUFBUSxLQUFLLElBQUksQ0FBQztZQUNsQixNQUFNLElBQUksS0FBSyxDQUFDLDRDQUE0QyxDQUFDLENBQUM7UUFDbEUsUUFBUSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7UUFDakQsRUFBRSxDQUFDLENBQUMsUUFBUSxLQUFLLElBQUksQ0FBQztZQUNsQixNQUFNLElBQUksS0FBSyxDQUFDLDRDQUE0QyxDQUFDLENBQUM7UUFDbEUsUUFBUSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7UUFDakQsRUFBRSxDQUFDLENBQUMsU0FBUyxLQUFLLElBQUksQ0FBQztZQUNuQixNQUFNLElBQUksS0FBSyxDQUFDLDZDQUE2QyxDQUFDLENBQUM7UUFDbkUsUUFBUSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsU0FBUyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7UUFDbkQsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRTtZQUM1QixJQUFJLEVBQUUsUUFBUSxDQUFDLFFBQVEsRUFBRTtZQUN6QixNQUFNLEVBQUUsTUFBTTtZQUNkLE9BQU8sRUFBRSxJQUFJLGNBQU8sQ0FBQztnQkFDakIsY0FBYyxFQUFFLFNBQVM7YUFDNUIsQ0FBQztTQUNMLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBQyxRQUFRO1lBQ1osTUFBTSxDQUFDLEtBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxFQUFFLFFBQVEsRUFBRSxVQUFDLFFBQVEsSUFBSyxPQUFBLEtBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLEVBQTNCLENBQTJCLENBQUMsQ0FBQztRQUMzRixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQyxRQUFhLEVBQUUsTUFBVztZQUNoQyxFQUFFLENBQUMsQ0FBQyxRQUFRLFlBQVksZUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDL0IsSUFBSSxDQUFDO29CQUNELE1BQU0sQ0FBQyxpQkFBVSxDQUFDLEVBQUUsQ0FBQyxLQUFJLENBQUMsZUFBZSxDQUFDLElBQUksRUFBRSxRQUFRLEVBQUUsVUFBQyxRQUFRLElBQUssT0FBQSxLQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxFQUEzQixDQUEyQixDQUFDLENBQUMsQ0FBQztnQkFDMUcsQ0FBRTtnQkFBQSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNULE1BQU0sQ0FBdUIsaUJBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JELENBQUM7WUFDTCxDQUFDO1lBQUMsSUFBSTtnQkFDRixNQUFNLENBQXVCLGlCQUFVLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ2hFLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQTdDTDtRQUFDLGlCQUFVLEVBQUU7bUJBS0osYUFBTSxDQUFDLHlCQUFpQixDQUFDOzt3QkFMckI7SUE4Q2IsdUJBQUM7QUFBRCxDQTdDQSxBQTZDQyxDQTdDcUMsY0FBTSxHQTZDM0M7QUE3Q1ksd0JBQWdCLG1CQTZDNUIsQ0FBQSIsImZpbGUiOiJhcHAvYXBpL2FwaS1jbGllbnQuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIEluamVjdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBIZWFkZXJzLCBVUkxTZWFyY2hQYXJhbXMsIFJlc3BvbnNlIH0gZnJvbSAnQGFuZ3VsYXIvaHR0cCc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuXHJcbmltcG9ydCB7IENsaWVudCwgQmFzZUNvbmZpZ3VyYXRpb24sIEh0dHBTZXJ2aWNlIH0gZnJvbSAnLi9pbmRleCc7XHJcbmltcG9ydCB7IENvbmZpZyB9IGZyb20gJy4uL3NoYXJlZC9pbmRleCc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBBcGlDbGllbnRTZXJ2aWNlIGV4dGVuZHMgQ2xpZW50IHtcclxuXHJcbiAgICBwcml2YXRlIF9odHRwOiBIdHRwU2VydmljZTtcclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIEBJbmplY3QoQmFzZUNvbmZpZ3VyYXRpb24pIGNvbmZpZ3VyYXRpb246IEJhc2VDb25maWd1cmF0aW9uLFxyXG4gICAgICAgIGh0dHA6IEh0dHBTZXJ2aWNlKSB7XHJcbiAgICAgICAgc3VwZXIoY29uZmlndXJhdGlvbiwgaHR0cCwgQ29uZmlnLkFQSSk7XHJcbiAgICAgICAgdGhpcy5faHR0cCA9IGh0dHA7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHRva2VuKGdyYW50X3R5cGU6IHN0cmluZywgdXNlcm5hbWU6IHN0cmluZywgcGFzc3dvcmQ6IHN0cmluZywgY2xpZW50X2lkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGxldCB1cmxfID0gQ29uZmlnLkFQSSArICcvdG9rZW4nO1xyXG5cclxuICAgICAgICBjb25zdCBjb250ZW50XyA9IG5ldyBVUkxTZWFyY2hQYXJhbXMoKTtcclxuICAgICAgICBpZiAoZ3JhbnRfdHlwZSA9PT0gbnVsbClcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdUaGUgcGFyYW1ldGVyIFxcJ2dyYW50X3R5cGVcXCcgY2Fubm90IGJlIG51bGwuJyk7XHJcbiAgICAgICAgY29udGVudF8uYXBwZW5kKCdncmFudF90eXBlJywgZ3JhbnRfdHlwZS50b1N0cmluZygpKTtcclxuICAgICAgICBpZiAodXNlcm5hbWUgPT09IG51bGwpXHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignVGhlIHBhcmFtZXRlciBcXCd1c2VybmFtZVxcJyBjYW5ub3QgYmUgbnVsbC4nKTtcclxuICAgICAgICBjb250ZW50Xy5hcHBlbmQoJ3VzZXJuYW1lJywgdXNlcm5hbWUudG9TdHJpbmcoKSk7XHJcbiAgICAgICAgaWYgKHBhc3N3b3JkID09PSBudWxsKVxyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1RoZSBwYXJhbWV0ZXIgXFwncGFzc3dvcmRcXCcgY2Fubm90IGJlIG51bGwuJyk7XHJcbiAgICAgICAgY29udGVudF8uYXBwZW5kKCdwYXNzd29yZCcsIHBhc3N3b3JkLnRvU3RyaW5nKCkpO1xyXG4gICAgICAgIGlmIChjbGllbnRfaWQgPT09IG51bGwpXHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignVGhlIHBhcmFtZXRlciBcXCdjbGllbnRfaWRcXCcgY2Fubm90IGJlIG51bGwuJyk7XHJcbiAgICAgICAgY29udGVudF8uYXBwZW5kKCdjbGllbnRfaWQnLCBjbGllbnRfaWQudG9TdHJpbmcoKSk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2h0dHAucmVxdWVzdCh1cmxfLCB7XHJcbiAgICAgICAgICAgIGJvZHk6IGNvbnRlbnRfLnRvU3RyaW5nKCksXHJcbiAgICAgICAgICAgIG1ldGhvZDogJ3Bvc3QnLFxyXG4gICAgICAgICAgICBoZWFkZXJzOiBuZXcgSGVhZGVycyh7XHJcbiAgICAgICAgICAgICAgICAnQ29udGVudC1UeXBlJzogdW5kZWZpbmVkXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgfSkubWFwKChyZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy50cmFuc2Zvcm1SZXN1bHQodXJsXywgcmVzcG9uc2UsIChyZXNwb25zZSkgPT4gdGhpcy5wcm9jZXNzVG9rZW4ocmVzcG9uc2UpKTtcclxuICAgICAgICB9KS5jYXRjaCgocmVzcG9uc2U6IGFueSwgY2F1Z2h0OiBhbnkpID0+IHtcclxuICAgICAgICAgICAgaWYgKHJlc3BvbnNlIGluc3RhbmNlb2YgUmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIE9ic2VydmFibGUub2YodGhpcy50cmFuc2Zvcm1SZXN1bHQodXJsXywgcmVzcG9uc2UsIChyZXNwb25zZSkgPT4gdGhpcy5wcm9jZXNzVG9rZW4ocmVzcG9uc2UpKSk7XHJcbiAgICAgICAgICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIDxPYnNlcnZhYmxlPGFueT4+PGFueT5PYnNlcnZhYmxlLnRocm93KGUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2VcclxuICAgICAgICAgICAgICAgIHJldHVybiA8T2JzZXJ2YWJsZTxhbnk+Pjxhbnk+T2JzZXJ2YWJsZS50aHJvdyhyZXNwb25zZSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn1cclxuIl19
