"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var angular2_toaster_1 = require('angular2-toaster');
var index_1 = require('../core/index');
var BaseConfiguration = (function () {
    function BaseConfiguration(_authService, _toasterService) {
        this._authService = _authService;
        this._toasterService = _toasterService;
    }
    BaseConfiguration.prototype.getToasterService = function () {
        return this._toasterService;
    };
    BaseConfiguration.prototype.getAccessTokenService = function () {
        return this._authService;
    };
    BaseConfiguration = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.AccessTokenService, angular2_toaster_1.ToasterService])
    ], BaseConfiguration);
    return BaseConfiguration;
}());
exports.BaseConfiguration = BaseConfiguration;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9hcGkvYmFzZS1jb25maWd1cmF0aW9uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUEyQixlQUFlLENBQUMsQ0FBQTtBQUUzQyxpQ0FBK0Isa0JBQWtCLENBQUMsQ0FBQTtBQUNsRCxzQkFBbUMsZUFBZSxDQUFDLENBQUE7QUFHbkQ7SUFFSSwyQkFBb0IsWUFBZ0MsRUFDeEMsZUFBK0I7UUFEdkIsaUJBQVksR0FBWixZQUFZLENBQW9CO1FBQ3hDLG9CQUFlLEdBQWYsZUFBZSxDQUFnQjtJQUFJLENBQUM7SUFFekMsNkNBQWlCLEdBQXhCO1FBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUM7SUFDaEMsQ0FBQztJQUVNLGlEQUFxQixHQUE1QjtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDO0lBQzdCLENBQUM7SUFaTDtRQUFDLGlCQUFVLEVBQUU7O3lCQUFBO0lBYWIsd0JBQUM7QUFBRCxDQVpBLEFBWUMsSUFBQTtBQVpZLHlCQUFpQixvQkFZN0IsQ0FBQSIsImZpbGUiOiJhcHAvYXBpL2Jhc2UtY29uZmlndXJhdGlvbi5zZXJ2aWNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgVG9hc3RlclNlcnZpY2UgfSBmcm9tICdhbmd1bGFyMi10b2FzdGVyJztcclxuaW1wb3J0IHsgQWNjZXNzVG9rZW5TZXJ2aWNlIH0gZnJvbSAnLi4vY29yZS9pbmRleCc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBCYXNlQ29uZmlndXJhdGlvbiB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfYXV0aFNlcnZpY2U6IEFjY2Vzc1Rva2VuU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIF90b2FzdGVyU2VydmljZTogVG9hc3RlclNlcnZpY2UpIHsgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRUb2FzdGVyU2VydmljZSgpOiBUb2FzdGVyU2VydmljZSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3RvYXN0ZXJTZXJ2aWNlO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRBY2Nlc3NUb2tlblNlcnZpY2UoKTogQWNjZXNzVG9rZW5TZXJ2aWNlIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fYXV0aFNlcnZpY2U7XHJcbiAgICB9XHJcbn1cclxuIl19
