"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var http_1 = require('@angular/http');
var angular2_toaster_1 = require('angular2-toaster');
var index_1 = require('../core/index');
var index_2 = require('./index');
var ApiModule = (function () {
    function ApiModule() {
    }
    ApiModule.forRoot = function () {
        return {
            ngModule: ApiModule,
            providers: [
                index_2.BaseConfiguration,
                index_2.Client,
                index_2.ApiClientService,
                {
                    provide: index_2.HttpService,
                    useFactory: function (authenticationService, router, toaster, backend, options) {
                        return new index_2.HttpService(authenticationService, router, toaster, backend, options);
                    },
                    deps: [index_1.AccessTokenService, router_1.Router, angular2_toaster_1.ToasterService, http_1.XHRBackend, http_1.RequestOptions]
                }
            ]
        };
    };
    ApiModule = __decorate([
        core_1.NgModule(), 
        __metadata('design:paramtypes', [])
    ], ApiModule);
    return ApiModule;
}());
exports.ApiModule = ApiModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9hcGkvYXBpLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQThDLGVBQWUsQ0FBQyxDQUFBO0FBQzlELHVCQUF1QixpQkFBaUIsQ0FBQyxDQUFBO0FBQ3pDLHFCQUEyQyxlQUFlLENBQUMsQ0FBQTtBQUUzRCxpQ0FBK0Isa0JBQWtCLENBQUMsQ0FBQTtBQUNsRCxzQkFBbUMsZUFBZSxDQUFDLENBQUE7QUFDbkQsc0JBQXlFLFNBQVMsQ0FBQyxDQUFBO0FBR25GO0lBQUE7SUFtQkEsQ0FBQztJQWxCVSxpQkFBTyxHQUFkO1FBQ0ksTUFBTSxDQUFDO1lBQ0gsUUFBUSxFQUFFLFNBQVM7WUFDbkIsU0FBUyxFQUFFO2dCQUNQLHlCQUFpQjtnQkFDakIsY0FBTTtnQkFDTix3QkFBZ0I7Z0JBQ2hCO29CQUNJLE9BQU8sRUFBRSxtQkFBVztvQkFDcEIsVUFBVSxFQUFFLFVBQUMscUJBQXlDLEVBQUUsTUFBYyxFQUNsRSxPQUF1QixFQUFFLE9BQW1CLEVBQUUsT0FBdUI7d0JBQ3JFLE1BQU0sQ0FBQyxJQUFJLG1CQUFXLENBQUMscUJBQXFCLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsT0FBTyxDQUFDLENBQUM7b0JBQ3JGLENBQUM7b0JBQ0QsSUFBSSxFQUFFLENBQUMsMEJBQWtCLEVBQUUsZUFBTSxFQUFFLGlDQUFjLEVBQUUsaUJBQVUsRUFBRSxxQkFBYyxDQUFDO2lCQUNqRjthQUNKO1NBQ0osQ0FBQztJQUNOLENBQUM7SUFuQkw7UUFBQyxlQUFRLEVBQUU7O2lCQUFBO0lBb0JYLGdCQUFDO0FBQUQsQ0FuQkEsQUFtQkMsSUFBQTtBQW5CWSxpQkFBUyxZQW1CckIsQ0FBQSIsImZpbGUiOiJhcHAvYXBpL2FwaS5tb2R1bGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSwgTW9kdWxlV2l0aFByb3ZpZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBYSFJCYWNrZW5kLCBSZXF1ZXN0T3B0aW9ucyB9IGZyb20gJ0Bhbmd1bGFyL2h0dHAnO1xyXG5cclxuaW1wb3J0IHsgVG9hc3RlclNlcnZpY2UgfSBmcm9tICdhbmd1bGFyMi10b2FzdGVyJztcclxuaW1wb3J0IHsgQWNjZXNzVG9rZW5TZXJ2aWNlIH0gZnJvbSAnLi4vY29yZS9pbmRleCc7XHJcbmltcG9ydCB7IEJhc2VDb25maWd1cmF0aW9uLCBDbGllbnQsIEFwaUNsaWVudFNlcnZpY2UsIEh0dHBTZXJ2aWNlIH0gZnJvbSAnLi9pbmRleCc7XHJcblxyXG5ATmdNb2R1bGUoKVxyXG5leHBvcnQgY2xhc3MgQXBpTW9kdWxlIHtcclxuICAgIHN0YXRpYyBmb3JSb290KCk6IE1vZHVsZVdpdGhQcm92aWRlcnMge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIG5nTW9kdWxlOiBBcGlNb2R1bGUsXHJcbiAgICAgICAgICAgIHByb3ZpZGVyczogW1xyXG4gICAgICAgICAgICAgICAgQmFzZUNvbmZpZ3VyYXRpb24sXHJcbiAgICAgICAgICAgICAgICBDbGllbnQsXHJcbiAgICAgICAgICAgICAgICBBcGlDbGllbnRTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIHByb3ZpZGU6IEh0dHBTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgICAgIHVzZUZhY3Rvcnk6IChhdXRoZW50aWNhdGlvblNlcnZpY2U6IEFjY2Vzc1Rva2VuU2VydmljZSwgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvYXN0ZXI6IFRvYXN0ZXJTZXJ2aWNlLCBiYWNrZW5kOiBYSFJCYWNrZW5kLCBvcHRpb25zOiBSZXF1ZXN0T3B0aW9ucykgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gbmV3IEh0dHBTZXJ2aWNlKGF1dGhlbnRpY2F0aW9uU2VydmljZSwgcm91dGVyLCB0b2FzdGVyLCBiYWNrZW5kLCBvcHRpb25zKTtcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIGRlcHM6IFtBY2Nlc3NUb2tlblNlcnZpY2UsIFJvdXRlciwgVG9hc3RlclNlcnZpY2UsIFhIUkJhY2tlbmQsIFJlcXVlc3RPcHRpb25zXVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBdXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxufVxyXG4iXX0=
