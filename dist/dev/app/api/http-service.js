"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var router_1 = require('@angular/router');
var Observable_1 = require('rxjs/Observable');
var index_1 = require('../core/index');
var index_2 = require('../shared/index');
var angular2_toaster_1 = require('angular2-toaster');
var _ = require('lodash');
var HttpService = (function (_super) {
    __extends(HttpService, _super);
    function HttpService(_authenticationService, router, toasterService, backend, options) {
        _super.call(this, backend, options);
        this._authenticationService = _authenticationService;
        this.router = router;
        this.toasterService = toasterService;
        this.url_ = index_2.Config.API + '/token';
    }
    HttpService.prototype.request = function (url, options) {
        var _this = this;
        return _super.prototype.request.call(this, url, options)
            .catch(function (response, caught) {
            var status = response.status;
            if (status === 401) {
                var hasValidToken = _this._authenticationService.hasValidAccessToken();
                if (!hasValidToken) {
                    var refreshToken = _this._authenticationService.getRefreshToken();
                    return _this.updateToken$('refresh_token', refreshToken, index_2.Config.CLIENT_ID)
                        .flatMap(function () {
                        var token = _this._authenticationService.getAccessToken();
                        options.headers.delete('Authorization');
                        options.headers.append('Authorization', 'Bearer ' + token);
                        return _super.prototype.request.call(_this, url, options);
                    })
                        .catch(function (response, caught) {
                        var status = response.status;
                        if (status === 400) {
                            _this.handleUnauthorisedError();
                        }
                        return Observable_1.Observable.throw(response);
                    });
                }
                _this.handleUnauthorisedError();
            }
            return Observable_1.Observable.throw(response);
        });
    };
    HttpService.prototype.updateToken$ = function (grant_type, refresh_token, client_id) {
        var _this = this;
        var content_ = new http_1.URLSearchParams();
        if (grant_type === null)
            throw new Error('The parameter \'grant_type\' cannot be null.');
        content_.append('grant_type', grant_type.toString());
        if (refresh_token === null)
            throw new Error('The parameter \'refresh_token\' cannot be null.');
        content_.append('refresh_token', refresh_token.toString());
        if (client_id === null)
            throw new Error('The parameter \'client_id\' cannot be null.');
        content_.append('client_id', client_id.toString());
        return _super.prototype.request.call(this, this.url_, {
            body: content_.toString(),
            method: 'post',
            headers: new http_1.Headers({
                'Content-Type': undefined
            })
        }).map(function (res) {
            var token = res.json();
            _this._authenticationService.clearStorage();
            _this._authenticationService.saveAccessToken(token, _this._authenticationService.IsParent);
        }).catch(function (response, caught) {
            return Observable_1.Observable.throw(response);
        });
    };
    HttpService.prototype.handleUnauthorisedError = function () {
        this.toasterService.clear();
        this.toasterService.pop('error', 'Unauthorised', 'Please login again');
        if (!_.includes(this.router.url, 'logout')) {
            var url = this._authenticationService.IsParent ? 'parent-logout/' + this._authenticationService.CentreName : 'logout';
            this.router.navigate([url]);
        }
    };
    HttpService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.AccessTokenService, router_1.Router, angular2_toaster_1.ToasterService, http_1.ConnectionBackend, http_1.RequestOptions])
    ], HttpService);
    return HttpService;
}(http_1.Http));
exports.HttpService = HttpService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9hcGkvaHR0cC1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7OztBQUFBLHFCQUEyQixlQUFlLENBQUMsQ0FBQTtBQUMzQyxxQkFHTyxlQUFlLENBQUMsQ0FBQTtBQUN2Qix1QkFBdUIsaUJBQWlCLENBQUMsQ0FBQTtBQUN6QywyQkFBMkIsaUJBQWlCLENBQUMsQ0FBQTtBQUU3QyxzQkFBbUMsZUFBZSxDQUFDLENBQUE7QUFDbkQsc0JBQXVCLGlCQUFpQixDQUFDLENBQUE7QUFDekMsaUNBQStCLGtCQUFrQixDQUFDLENBQUE7QUFDbEQsSUFBWSxDQUFDLFdBQU0sUUFBUSxDQUFDLENBQUE7QUFHNUI7SUFBaUMsK0JBQUk7SUFJakMscUJBQW9CLHNCQUEwQyxFQUNsRCxNQUFjLEVBQ2QsY0FBOEIsRUFDdEMsT0FBMEIsRUFDMUIsT0FBdUI7UUFDdkIsa0JBQU0sT0FBTyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBTFIsMkJBQXNCLEdBQXRCLHNCQUFzQixDQUFvQjtRQUNsRCxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBSmxDLFNBQUksR0FBRyxjQUFNLENBQUMsR0FBRyxHQUFHLFFBQVEsQ0FBQztJQVFyQyxDQUFDO0lBQ00sNkJBQU8sR0FBZCxVQUFlLEdBQXFCLEVBQUUsT0FBNEI7UUFBbEUsaUJBaUNDO1FBOUJHLE1BQU0sQ0FBQyxnQkFBSyxDQUFDLE9BQU8sWUFBQyxHQUFHLEVBQUUsT0FBTyxDQUFDO2FBQzdCLEtBQUssQ0FBQyxVQUFDLFFBQWEsRUFBRSxNQUFXO1lBQzlCLElBQU0sTUFBTSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUM7WUFDL0IsRUFBRSxDQUFDLENBQUMsTUFBTSxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBRWpCLElBQUksYUFBYSxHQUFZLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO2dCQUMvRSxFQUFFLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7b0JBQ2pCLElBQUksWUFBWSxHQUFXLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxlQUFlLEVBQUUsQ0FBQztvQkFDekUsTUFBTSxDQUFDLEtBQUksQ0FBQyxZQUFZLENBQUMsZUFBZSxFQUFFLFlBQVksRUFBRSxjQUFNLENBQUMsU0FBUyxDQUFDO3lCQUNwRSxPQUFPLENBQUM7d0JBRUwsSUFBSSxLQUFLLEdBQUcsS0FBSSxDQUFDLHNCQUFzQixDQUFDLGNBQWMsRUFBRSxDQUFDO3dCQUV6RCxPQUFPLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQzt3QkFFeEMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsZUFBZSxFQUFFLFNBQVMsR0FBRyxLQUFLLENBQUMsQ0FBQzt3QkFDM0QsTUFBTSxDQUFDLGdCQUFLLENBQUMsT0FBTyxhQUFDLEdBQUcsRUFBRSxPQUFPLENBQUMsQ0FBQztvQkFDdkMsQ0FBQyxDQUFDO3lCQUNELEtBQUssQ0FBQyxVQUFDLFFBQWEsRUFBRSxNQUFXO3dCQUM5QixJQUFNLE1BQU0sR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDO3dCQUMvQixFQUFFLENBQUMsQ0FBQyxNQUFNLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQzs0QkFDakIsS0FBSSxDQUFDLHVCQUF1QixFQUFFLENBQUM7d0JBQ25DLENBQUM7d0JBQ0QsTUFBTSxDQUE0Qix1QkFBVSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDakUsQ0FBQyxDQUFDLENBQUM7Z0JBQ1gsQ0FBQztnQkFDRCxLQUFJLENBQUMsdUJBQXVCLEVBQUUsQ0FBQztZQUNuQyxDQUFDO1lBQ0QsTUFBTSxDQUE0Qix1QkFBVSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNqRSxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFTyxrQ0FBWSxHQUFwQixVQUFxQixVQUFrQixFQUFFLGFBQXFCLEVBQUUsU0FBaUI7UUFBakYsaUJBMkJDO1FBekJHLElBQU0sUUFBUSxHQUFHLElBQUksc0JBQWUsRUFBRSxDQUFDO1FBQ3ZDLEVBQUUsQ0FBQyxDQUFDLFVBQVUsS0FBSyxJQUFJLENBQUM7WUFDcEIsTUFBTSxJQUFJLEtBQUssQ0FBQyw4Q0FBOEMsQ0FBQyxDQUFDO1FBQ3BFLFFBQVEsQ0FBQyxNQUFNLENBQUMsWUFBWSxFQUFFLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1FBQ3JELEVBQUUsQ0FBQyxDQUFDLGFBQWEsS0FBSyxJQUFJLENBQUM7WUFDdkIsTUFBTSxJQUFJLEtBQUssQ0FBQyxpREFBaUQsQ0FBQyxDQUFDO1FBQ3ZFLFFBQVEsQ0FBQyxNQUFNLENBQUMsZUFBZSxFQUFFLGFBQWEsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1FBQzNELEVBQUUsQ0FBQyxDQUFDLFNBQVMsS0FBSyxJQUFJLENBQUM7WUFDbkIsTUFBTSxJQUFJLEtBQUssQ0FBQyw2Q0FBNkMsQ0FBQyxDQUFDO1FBQ25FLFFBQVEsQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLFNBQVMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1FBRW5ELE1BQU0sQ0FBQyxnQkFBSyxDQUFDLE9BQU8sWUFBQyxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQzVCLElBQUksRUFBRSxRQUFRLENBQUMsUUFBUSxFQUFFO1lBQ3pCLE1BQU0sRUFBRSxNQUFNO1lBQ2QsT0FBTyxFQUFFLElBQUksY0FBTyxDQUFDO2dCQUNqQixjQUFjLEVBQUUsU0FBUzthQUM1QixDQUFDO1NBQ0wsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFDLEdBQUc7WUFDUCxJQUFJLEtBQUssR0FBRyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDdkIsS0FBSSxDQUFDLHNCQUFzQixDQUFDLFlBQVksRUFBRSxDQUFDO1lBQzNDLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxlQUFlLENBQUMsS0FBSyxFQUM3QyxLQUFJLENBQUMsc0JBQXNCLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDOUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUMsUUFBYSxFQUFFLE1BQVc7WUFDaEMsTUFBTSxDQUF3Qix1QkFBVSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUM3RCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFTyw2Q0FBdUIsR0FBL0I7UUFFSSxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQzVCLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxjQUFjLEVBQUUsb0JBQW9CLENBQUMsQ0FBQztRQUd2RSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3pDLElBQUksR0FBRyxHQUFXLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLEdBQUcsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUM7WUFDOUgsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQ2hDLENBQUM7SUFDTCxDQUFDO0lBdEZMO1FBQUMsaUJBQVUsRUFBRTs7bUJBQUE7SUF1RmIsa0JBQUM7QUFBRCxDQXRGQSxBQXNGQyxDQXRGZ0MsV0FBSSxHQXNGcEM7QUF0RlksbUJBQVcsY0FzRnZCLENBQUEiLCJmaWxlIjoiYXBwL2FwaS9odHRwLXNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7XHJcbiAgICBIdHRwLCBIZWFkZXJzLCBVUkxTZWFyY2hQYXJhbXMsIFJlcXVlc3QsIFJlc3BvbnNlLFxyXG4gICAgUmVxdWVzdE9wdGlvbnMsIFJlcXVlc3RPcHRpb25zQXJncywgQ29ubmVjdGlvbkJhY2tlbmRcclxufSBmcm9tICdAYW5ndWxhci9odHRwJztcclxuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMvT2JzZXJ2YWJsZSc7XHJcblxyXG5pbXBvcnQgeyBBY2Nlc3NUb2tlblNlcnZpY2UgfSBmcm9tICcuLi9jb3JlL2luZGV4JztcclxuaW1wb3J0IHsgQ29uZmlnIH0gZnJvbSAnLi4vc2hhcmVkL2luZGV4JztcclxuaW1wb3J0IHsgVG9hc3RlclNlcnZpY2UgfSBmcm9tICdhbmd1bGFyMi10b2FzdGVyJztcclxuaW1wb3J0ICogYXMgXyBmcm9tICdsb2Rhc2gnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgSHR0cFNlcnZpY2UgZXh0ZW5kcyBIdHRwIHtcclxuXHJcbiAgICBwcml2YXRlIHVybF8gPSBDb25maWcuQVBJICsgJy90b2tlbic7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfYXV0aGVudGljYXRpb25TZXJ2aWNlOiBBY2Nlc3NUb2tlblNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcclxuICAgICAgICBwcml2YXRlIHRvYXN0ZXJTZXJ2aWNlOiBUb2FzdGVyU2VydmljZSxcclxuICAgICAgICBiYWNrZW5kOiBDb25uZWN0aW9uQmFja2VuZCxcclxuICAgICAgICBvcHRpb25zOiBSZXF1ZXN0T3B0aW9ucykge1xyXG4gICAgICAgIHN1cGVyKGJhY2tlbmQsIG9wdGlvbnMpO1xyXG4gICAgfVxyXG4gICAgcHVibGljIHJlcXVlc3QodXJsOiBzdHJpbmcgfCBSZXF1ZXN0LCBvcHRpb25zPzogUmVxdWVzdE9wdGlvbnNBcmdzKTogT2JzZXJ2YWJsZTxSZXNwb25zZT4ge1xyXG5cclxuICAgICAgICAvLyBJZiBhbHJlYWR5IGdldHRpbmcgYSB0b2tlbiwgZG9uJ3QgcmVxdWVzdCBhIHJlZnJlc2ggdG9rZW5cclxuICAgICAgICByZXR1cm4gc3VwZXIucmVxdWVzdCh1cmwsIG9wdGlvbnMpXHJcbiAgICAgICAgICAgIC5jYXRjaCgocmVzcG9uc2U6IGFueSwgY2F1Z2h0OiBhbnkpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHN0YXR1cyA9IHJlc3BvbnNlLnN0YXR1cztcclxuICAgICAgICAgICAgICAgIGlmIChzdGF0dXMgPT09IDQwMSkge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBsZXQgaGFzVmFsaWRUb2tlbjogYm9vbGVhbiA9IHRoaXMuX2F1dGhlbnRpY2F0aW9uU2VydmljZS5oYXNWYWxpZEFjY2Vzc1Rva2VuKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFoYXNWYWxpZFRva2VuKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCByZWZyZXNoVG9rZW46IHN0cmluZyA9IHRoaXMuX2F1dGhlbnRpY2F0aW9uU2VydmljZS5nZXRSZWZyZXNoVG9rZW4oKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMudXBkYXRlVG9rZW4kKCdyZWZyZXNoX3Rva2VuJywgcmVmcmVzaFRva2VuLCBDb25maWcuQ0xJRU5UX0lEKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLmZsYXRNYXAoKCkgPT4ge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgdG9rZW4gPSB0aGlzLl9hdXRoZW50aWNhdGlvblNlcnZpY2UuZ2V0QWNjZXNzVG9rZW4oKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBSZW1vdmUgYWNjZXNzIHRva2VuIGluaXRpYWxseSBwYXNzZWQgb24gaGVhZGVyXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9ucy5oZWFkZXJzLmRlbGV0ZSgnQXV0aG9yaXphdGlvbicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIEFkZCBuZXcgYWNjZXNzIHRva2VuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9ucy5oZWFkZXJzLmFwcGVuZCgnQXV0aG9yaXphdGlvbicsICdCZWFyZXIgJyArIHRva2VuKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gc3VwZXIucmVxdWVzdCh1cmwsIG9wdGlvbnMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5jYXRjaCgocmVzcG9uc2U6IGFueSwgY2F1Z2h0OiBhbnkpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBzdGF0dXMgPSByZXNwb25zZS5zdGF0dXM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHN0YXR1cyA9PT0gNDAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaGFuZGxlVW5hdXRob3Jpc2VkRXJyb3IoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIDxPYnNlcnZhYmxlPFJlc3BvbnNlPj48YW55Pk9ic2VydmFibGUudGhyb3cocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaGFuZGxlVW5hdXRob3Jpc2VkRXJyb3IoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHJldHVybiA8T2JzZXJ2YWJsZTxSZXNwb25zZT4+PGFueT5PYnNlcnZhYmxlLnRocm93KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSB1cGRhdGVUb2tlbiQoZ3JhbnRfdHlwZTogc3RyaW5nLCByZWZyZXNoX3Rva2VuOiBzdHJpbmcsIGNsaWVudF9pZDogc3RyaW5nKTogT2JzZXJ2YWJsZTx2b2lkPiB7XHJcblxyXG4gICAgICAgIGNvbnN0IGNvbnRlbnRfID0gbmV3IFVSTFNlYXJjaFBhcmFtcygpO1xyXG4gICAgICAgIGlmIChncmFudF90eXBlID09PSBudWxsKVxyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1RoZSBwYXJhbWV0ZXIgXFwnZ3JhbnRfdHlwZVxcJyBjYW5ub3QgYmUgbnVsbC4nKTtcclxuICAgICAgICBjb250ZW50Xy5hcHBlbmQoJ2dyYW50X3R5cGUnLCBncmFudF90eXBlLnRvU3RyaW5nKCkpO1xyXG4gICAgICAgIGlmIChyZWZyZXNoX3Rva2VuID09PSBudWxsKVxyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1RoZSBwYXJhbWV0ZXIgXFwncmVmcmVzaF90b2tlblxcJyBjYW5ub3QgYmUgbnVsbC4nKTtcclxuICAgICAgICBjb250ZW50Xy5hcHBlbmQoJ3JlZnJlc2hfdG9rZW4nLCByZWZyZXNoX3Rva2VuLnRvU3RyaW5nKCkpO1xyXG4gICAgICAgIGlmIChjbGllbnRfaWQgPT09IG51bGwpXHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignVGhlIHBhcmFtZXRlciBcXCdjbGllbnRfaWRcXCcgY2Fubm90IGJlIG51bGwuJyk7XHJcbiAgICAgICAgY29udGVudF8uYXBwZW5kKCdjbGllbnRfaWQnLCBjbGllbnRfaWQudG9TdHJpbmcoKSk7XHJcblxyXG4gICAgICAgIHJldHVybiBzdXBlci5yZXF1ZXN0KHRoaXMudXJsXywge1xyXG4gICAgICAgICAgICBib2R5OiBjb250ZW50Xy50b1N0cmluZygpLFxyXG4gICAgICAgICAgICBtZXRob2Q6ICdwb3N0JyxcclxuICAgICAgICAgICAgaGVhZGVyczogbmV3IEhlYWRlcnMoe1xyXG4gICAgICAgICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6IHVuZGVmaW5lZFxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgIH0pLm1hcCgocmVzKSA9PiB7XHJcbiAgICAgICAgICAgIGxldCB0b2tlbiA9IHJlcy5qc29uKCk7XHJcbiAgICAgICAgICAgIHRoaXMuX2F1dGhlbnRpY2F0aW9uU2VydmljZS5jbGVhclN0b3JhZ2UoKTtcclxuICAgICAgICAgICAgdGhpcy5fYXV0aGVudGljYXRpb25TZXJ2aWNlLnNhdmVBY2Nlc3NUb2tlbih0b2tlbixcclxuICAgICAgICAgICAgICAgIHRoaXMuX2F1dGhlbnRpY2F0aW9uU2VydmljZS5Jc1BhcmVudCk7XHJcbiAgICAgICAgfSkuY2F0Y2goKHJlc3BvbnNlOiBhbnksIGNhdWdodDogYW55KSA9PiB7XHJcbiAgICAgICAgICAgIHJldHVybiA8T2JzZXJ2YWJsZTx2b2lkPj48YW55Pk9ic2VydmFibGUudGhyb3cocmVzcG9uc2UpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgaGFuZGxlVW5hdXRob3Jpc2VkRXJyb3IoKTogdm9pZCB7XHJcbiAgICAgICAgLy8gU2hvdyB0b2FzdHJcclxuICAgICAgICB0aGlzLnRvYXN0ZXJTZXJ2aWNlLmNsZWFyKCk7XHJcbiAgICAgICAgdGhpcy50b2FzdGVyU2VydmljZS5wb3AoJ2Vycm9yJywgJ1VuYXV0aG9yaXNlZCcsICdQbGVhc2UgbG9naW4gYWdhaW4nKTtcclxuICAgICAgICAvLyBSZWRpcmVjdFxyXG4gICAgICAgIC8vIERvIG5vdCByZWRpcmVjdCB0byBsb2dvdXQgcGFnZXMgaWYgYWxyZWFkeSBvbiB0aGF0IHBhZ2VcclxuICAgICAgICBpZiAoIV8uaW5jbHVkZXModGhpcy5yb3V0ZXIudXJsLCAnbG9nb3V0JykpIHtcclxuICAgICAgICAgICAgbGV0IHVybDogc3RyaW5nID0gdGhpcy5fYXV0aGVudGljYXRpb25TZXJ2aWNlLklzUGFyZW50ID8gJ3BhcmVudC1sb2dvdXQvJyArIHRoaXMuX2F1dGhlbnRpY2F0aW9uU2VydmljZS5DZW50cmVOYW1lIDogJ2xvZ291dCc7XHJcbiAgICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFt1cmxdKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19
