"use strict";
var EqualPasswordsValidator = (function () {
    function EqualPasswordsValidator() {
    }
    EqualPasswordsValidator.validate = function (firstField, secondField) {
        return function (c) {
            return (c.controls && c.controls[firstField].value === c.controls[secondField].value) ? null : {
                passwordsEqual: {
                    valid: false
                }
            };
        };
    };
    return EqualPasswordsValidator;
}());
exports.EqualPasswordsValidator = EqualPasswordsValidator;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC92YWxpZGF0b3JzL2VxdWFsUGFzc3dvcmRzLnZhbGlkYXRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBRUE7SUFBQTtJQWFBLENBQUM7SUFYZSxnQ0FBUSxHQUF0QixVQUF1QixVQUFrQixFQUFFLFdBQW1CO1FBRTVELE1BQU0sQ0FBQyxVQUFDLENBQVc7WUFFakIsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsSUFBSSxDQUFDLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDLEtBQUssS0FBSyxDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksR0FBRztnQkFDN0YsY0FBYyxFQUFFO29CQUNkLEtBQUssRUFBRSxLQUFLO2lCQUNiO2FBQ0YsQ0FBQztRQUNKLENBQUMsQ0FBQztJQUNKLENBQUM7SUFDSCw4QkFBQztBQUFELENBYkEsQUFhQyxJQUFBO0FBYlksK0JBQXVCLDBCQWFuQyxDQUFBIiwiZmlsZSI6ImFwcC92YWxpZGF0b3JzL2VxdWFsUGFzc3dvcmRzLnZhbGlkYXRvci5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Rm9ybUdyb3VwfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcblxyXG5leHBvcnQgY2xhc3MgRXF1YWxQYXNzd29yZHNWYWxpZGF0b3Ige1xyXG5cclxuICBwdWJsaWMgc3RhdGljIHZhbGlkYXRlKGZpcnN0RmllbGQ6IHN0cmluZywgc2Vjb25kRmllbGQ6IHN0cmluZykge1xyXG5cclxuICAgIHJldHVybiAoYzpGb3JtR3JvdXApID0+IHtcclxuXHJcbiAgICAgIHJldHVybiAoYy5jb250cm9scyAmJiBjLmNvbnRyb2xzW2ZpcnN0RmllbGRdLnZhbHVlID09PSBjLmNvbnRyb2xzW3NlY29uZEZpZWxkXS52YWx1ZSkgPyBudWxsIDoge1xyXG4gICAgICAgIHBhc3N3b3Jkc0VxdWFsOiB7XHJcbiAgICAgICAgICB2YWxpZDogZmFsc2VcclxuICAgICAgICB9XHJcbiAgICAgIH07XHJcbiAgICB9O1xyXG4gIH1cclxufVxyXG4iXX0=
