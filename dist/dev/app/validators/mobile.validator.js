"use strict";
var MobileValidator = (function () {
    function MobileValidator() {
    }
    MobileValidator.validate = function (c) {
        var MOBILE_REGEXP = /^(04(\d\s?){8})$/i;
        if (c.value === '' || c.value === null || c.value === undefined)
            return null;
        return MOBILE_REGEXP.test(c.value) ? null : {
            validateMobile: {
                valid: false
            }
        };
    };
    return MobileValidator;
}());
exports.MobileValidator = MobileValidator;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC92YWxpZGF0b3JzL21vYmlsZS52YWxpZGF0b3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUVBO0lBQUE7SUFjQSxDQUFDO0lBWmUsd0JBQVEsR0FBdEIsVUFBdUIsQ0FBa0I7UUFDdkMsSUFBSSxhQUFhLEdBQUcsbUJBQW1CLENBQUM7UUFFeEMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDLEtBQUssS0FBSyxJQUFJLElBQUksQ0FBQyxDQUFDLEtBQUssS0FBSyxTQUFTLENBQUM7WUFDOUQsTUFBTSxDQUFDLElBQUksQ0FBQztRQUVkLE1BQU0sQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLEdBQUc7WUFDMUMsY0FBYyxFQUFFO2dCQUNkLEtBQUssRUFBRSxLQUFLO2FBQ2I7U0FDRixDQUFDO0lBQ0osQ0FBQztJQUNILHNCQUFDO0FBQUQsQ0FkQSxBQWNDLElBQUE7QUFkWSx1QkFBZSxrQkFjM0IsQ0FBQSIsImZpbGUiOiJhcHAvdmFsaWRhdG9ycy9tb2JpbGUudmFsaWRhdG9yLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQWJzdHJhY3RDb250cm9sIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5cclxuZXhwb3J0IGNsYXNzIE1vYmlsZVZhbGlkYXRvciB7XHJcblxyXG4gIHB1YmxpYyBzdGF0aWMgdmFsaWRhdGUoYzogQWJzdHJhY3RDb250cm9sKSB7XHJcbiAgICBsZXQgTU9CSUxFX1JFR0VYUCA9IC9eKDA0KFxcZFxccz8pezh9KSQvaTtcclxuXHJcbiAgICBpZiAoYy52YWx1ZSA9PT0gJycgfHwgYy52YWx1ZSA9PT0gbnVsbCB8fCBjLnZhbHVlID09PSB1bmRlZmluZWQpXHJcbiAgICAgIHJldHVybiBudWxsO1xyXG5cclxuICAgIHJldHVybiBNT0JJTEVfUkVHRVhQLnRlc3QoYy52YWx1ZSkgPyBudWxsIDoge1xyXG4gICAgICB2YWxpZGF0ZU1vYmlsZToge1xyXG4gICAgICAgIHZhbGlkOiBmYWxzZVxyXG4gICAgICB9XHJcbiAgICB9O1xyXG4gIH1cclxufVxyXG4iXX0=
