"use strict";
var ComplexPasswordValidator = (function () {
    function ComplexPasswordValidator() {
    }
    ComplexPasswordValidator.validate = function (c) {
        var PASSWORD_REGEXP = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d.*)(?=.*\W.*)[a-zA-Z0-9\S]{8,50}$/;
        return PASSWORD_REGEXP.test(c.value) ? null : {
            PasswordComplex: {
                valid: false
            }
        };
    };
    return ComplexPasswordValidator;
}());
exports.ComplexPasswordValidator = ComplexPasswordValidator;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC92YWxpZGF0b3JzL2NvbXBsZXhQYXNzd29yZC52YWxpZGF0b3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUVBO0lBQUE7SUFXQSxDQUFDO0lBVGUsaUNBQVEsR0FBdEIsVUFBdUIsQ0FBaUI7UUFDdEMsSUFBSSxlQUFlLEdBQUcsaUVBQWlFLENBQUM7UUFFeEYsTUFBTSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksR0FBRztZQUM1QyxlQUFlLEVBQUU7Z0JBQ2YsS0FBSyxFQUFFLEtBQUs7YUFDYjtTQUNGLENBQUM7SUFDSixDQUFDO0lBQ0gsK0JBQUM7QUFBRCxDQVhBLEFBV0MsSUFBQTtBQVhZLGdDQUF3QiwyQkFXcEMsQ0FBQSIsImZpbGUiOiJhcHAvdmFsaWRhdG9ycy9jb21wbGV4UGFzc3dvcmQudmFsaWRhdG9yLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtBYnN0cmFjdENvbnRyb2x9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuXHJcbmV4cG9ydCBjbGFzcyBDb21wbGV4UGFzc3dvcmRWYWxpZGF0b3Ige1xyXG5cclxuICBwdWJsaWMgc3RhdGljIHZhbGlkYXRlKGM6QWJzdHJhY3RDb250cm9sKSB7XHJcbiAgICBsZXQgUEFTU1dPUkRfUkVHRVhQID0gL14oPz0uKlthLXpdKSg/PS4qW0EtWl0pKD89LipcXGQuKikoPz0uKlxcVy4qKVthLXpBLVowLTlcXFNdezgsNTB9JC87XHJcblxyXG4gICAgcmV0dXJuIFBBU1NXT1JEX1JFR0VYUC50ZXN0KGMudmFsdWUpID8gbnVsbCA6IHtcclxuICAgICAgUGFzc3dvcmRDb21wbGV4OiB7XHJcbiAgICAgICAgdmFsaWQ6IGZhbHNlXHJcbiAgICAgIH1cclxuICAgIH07XHJcbiAgfVxyXG59XHJcbiJdfQ==
