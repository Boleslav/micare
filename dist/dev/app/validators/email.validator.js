"use strict";
var EmailValidator = (function () {
    function EmailValidator() {
    }
    EmailValidator.validate = function (c) {
        var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
        if (c.value === '' || c.value === null || c.value === undefined)
            return null;
        return EMAIL_REGEXP.test(c.value) ? null : {
            validateEmail: {
                valid: false
            }
        };
    };
    return EmailValidator;
}());
exports.EmailValidator = EmailValidator;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC92YWxpZGF0b3JzL2VtYWlsLnZhbGlkYXRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBRUE7SUFBQTtJQWNBLENBQUM7SUFaZSx1QkFBUSxHQUF0QixVQUF1QixDQUFrQjtRQUN2QyxJQUFJLFlBQVksR0FBRyxtR0FBbUcsQ0FBQztRQUV2SCxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUMsS0FBSyxLQUFLLElBQUksSUFBSSxDQUFDLENBQUMsS0FBSyxLQUFLLFNBQVMsQ0FBQztZQUM5RCxNQUFNLENBQUMsSUFBSSxDQUFDO1FBRWQsTUFBTSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksR0FBRztZQUN6QyxhQUFhLEVBQUU7Z0JBQ2IsS0FBSyxFQUFFLEtBQUs7YUFDYjtTQUNGLENBQUM7SUFDSixDQUFDO0lBQ0gscUJBQUM7QUFBRCxDQWRBLEFBY0MsSUFBQTtBQWRZLHNCQUFjLGlCQWMxQixDQUFBIiwiZmlsZSI6ImFwcC92YWxpZGF0b3JzL2VtYWlsLnZhbGlkYXRvci5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEFic3RyYWN0Q29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuXHJcbmV4cG9ydCBjbGFzcyBFbWFpbFZhbGlkYXRvciB7XHJcblxyXG4gIHB1YmxpYyBzdGF0aWMgdmFsaWRhdGUoYzogQWJzdHJhY3RDb250cm9sKSB7XHJcbiAgICBsZXQgRU1BSUxfUkVHRVhQID0gL15bYS16MC05ISMkJSYnKitcXC89P15fYHt8fX4uLV0rQFthLXowLTldKFthLXowLTktXSpbYS16MC05XSk/KFxcLlthLXowLTldKFthLXowLTktXSpbYS16MC05XSk/KSokL2k7XHJcblxyXG4gICAgaWYgKGMudmFsdWUgPT09ICcnIHx8IGMudmFsdWUgPT09IG51bGwgfHwgYy52YWx1ZSA9PT0gdW5kZWZpbmVkKVxyXG4gICAgICByZXR1cm4gbnVsbDtcclxuXHJcbiAgICByZXR1cm4gRU1BSUxfUkVHRVhQLnRlc3QoYy52YWx1ZSkgPyBudWxsIDoge1xyXG4gICAgICB2YWxpZGF0ZUVtYWlsOiB7XHJcbiAgICAgICAgdmFsaWQ6IGZhbHNlXHJcbiAgICAgIH1cclxuICAgIH07XHJcbiAgfVxyXG59XHJcbiJdfQ==
