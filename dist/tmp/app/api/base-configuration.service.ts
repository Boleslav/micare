import { Injectable } from '@angular/core';

import { ToasterService } from 'angular2-toaster';
import { AccessTokenService } from '../core/index';

@Injectable()
export class BaseConfiguration {

    constructor(private _authService: AccessTokenService,
        private _toasterService: ToasterService) { }

    public getToasterService(): ToasterService {
        return this._toasterService;
    }

    public getAccessTokenService(): AccessTokenService {
        return this._authService;
    }
}
