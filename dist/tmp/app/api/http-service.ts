import { Injectable } from '@angular/core';
import {
    Http, Headers, URLSearchParams, Request, Response,
    RequestOptions, RequestOptionsArgs, ConnectionBackend
} from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { AccessTokenService } from '../core/index';
import { Config } from '../shared/index';
import { ToasterService } from 'angular2-toaster';
import * as _ from 'lodash';

@Injectable()
export class HttpService extends Http {

    private url_ = Config.API + '/token';

    constructor(private _authenticationService: AccessTokenService,
        private router: Router,
        private toasterService: ToasterService,
        backend: ConnectionBackend,
        options: RequestOptions) {
        super(backend, options);
    }
    public request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {

        // If already getting a token, don't request a refresh token
        return super.request(url, options)
            .catch((response: any, caught: any) => {
                const status = response.status;
                if (status === 401) {

                    let hasValidToken: boolean = this._authenticationService.hasValidAccessToken();
                    if (!hasValidToken) {
                        let refreshToken: string = this._authenticationService.getRefreshToken();
                        return this.updateToken$('refresh_token', refreshToken, Config.CLIENT_ID)
                            .flatMap(() => {

                                let token = this._authenticationService.getAccessToken();
                                // Remove access token initially passed on header
                                options.headers.delete('Authorization');
                                // Add new access token
                                options.headers.append('Authorization', 'Bearer ' + token);
                                return super.request(url, options);
                            })
                            .catch((response: any, caught: any) => {
                                const status = response.status;
                                if (status === 400) {
                                    this.handleUnauthorisedError();
                                }
                                return <Observable<Response>><any>Observable.throw(response);
                            });
                    }
                    this.handleUnauthorisedError();
                }
                return <Observable<Response>><any>Observable.throw(response);
            });
    }

    private updateToken$(grant_type: string, refresh_token: string, client_id: string): Observable<void> {

        const content_ = new URLSearchParams();
        if (grant_type === null)
            throw new Error('The parameter \'grant_type\' cannot be null.');
        content_.append('grant_type', grant_type.toString());
        if (refresh_token === null)
            throw new Error('The parameter \'refresh_token\' cannot be null.');
        content_.append('refresh_token', refresh_token.toString());
        if (client_id === null)
            throw new Error('The parameter \'client_id\' cannot be null.');
        content_.append('client_id', client_id.toString());

        return super.request(this.url_, {
            body: content_.toString(),
            method: 'post',
            headers: new Headers({
                'Content-Type': undefined
            })
        }).map((res) => {
            let token = res.json();
            this._authenticationService.clearStorage();
            this._authenticationService.saveAccessToken(token,
                this._authenticationService.IsParent);
        }).catch((response: any, caught: any) => {
            return <Observable<void>><any>Observable.throw(response);
        });
    }

    private handleUnauthorisedError(): void {
        // Show toastr
        this.toasterService.clear();
        this.toasterService.pop('error', 'Unauthorised', 'Please login again');
        // Redirect
        // Do not redirect to logout pages if already on that page
        if (!_.includes(this.router.url, 'logout')) {
            let url: string = this._authenticationService.IsParent ? 'parent-logout/' + this._authenticationService.CentreName : 'logout';
            this.router.navigate([url]);
        }
    }
}
