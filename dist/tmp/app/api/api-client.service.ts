import { Injectable, Inject } from '@angular/core';
import { Headers, URLSearchParams, Response } from '@angular/http';
import { Observable } from 'rxjs';

import { Client, BaseConfiguration, HttpService } from './index';
import { Config } from '../shared/index';

@Injectable()
export class ApiClientService extends Client {

    private _http: HttpService;
    constructor(
        @Inject(BaseConfiguration) configuration: BaseConfiguration,
        http: HttpService) {
        super(configuration, http, Config.API);
        this._http = http;
    }

    public token(grant_type: string, username: string, password: string, client_id: string): Observable<any> {
        let url_ = Config.API + '/token';

        const content_ = new URLSearchParams();
        if (grant_type === null)
            throw new Error('The parameter \'grant_type\' cannot be null.');
        content_.append('grant_type', grant_type.toString());
        if (username === null)
            throw new Error('The parameter \'username\' cannot be null.');
        content_.append('username', username.toString());
        if (password === null)
            throw new Error('The parameter \'password\' cannot be null.');
        content_.append('password', password.toString());
        if (client_id === null)
            throw new Error('The parameter \'client_id\' cannot be null.');
        content_.append('client_id', client_id.toString());
        return this._http.request(url_, {
            body: content_.toString(),
            method: 'post',
            headers: new Headers({
                'Content-Type': undefined
            })
        }).map((response) => {
            return this.transformResult(url_, response, (response) => this.processToken(response));
        }).catch((response: any, caught: any) => {
            if (response instanceof Response) {
                try {
                    return Observable.of(this.transformResult(url_, response, (response) => this.processToken(response)));
                } catch (e) {
                    return <Observable<any>><any>Observable.throw(e);
                }
            } else
                return <Observable<any>><any>Observable.throw(response);
        });
    }
}
