import { NgModule, ModuleWithProviders } from '@angular/core';
import { Router } from '@angular/router';
import { XHRBackend, RequestOptions } from '@angular/http';

import { ToasterService } from 'angular2-toaster';
import { AccessTokenService } from '../core/index';
import { BaseConfiguration, Client, ApiClientService, HttpService } from './index';
import {HttpServiceFactory} from "./httpService.factory";

@NgModule()
export class ApiModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: ApiModule,
            providers: [
                BaseConfiguration,
                Client,
                ApiClientService,
                {
                    provide: HttpService,
                    useFactory: HttpServiceFactory,
                    deps: [AccessTokenService, Router, ToasterService, XHRBackend, RequestOptions]
                }
            ]
        };
    }
}
