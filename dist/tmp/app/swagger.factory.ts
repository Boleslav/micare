import {CentreUserService} from "./core/centre-user.service";

export function SwaggerFactory (userService: CentreUserService) {
  userService.SwaggerUrl$()
    .subscribe((url) => {
      window.location.href = url;
    });
  return '';
}
