import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/index';

import { ChangePasswordComponent } from './index';

@NgModule({
    imports: [SharedModule],
    declarations: [ChangePasswordComponent]
})

export class ChangePasswordModule { }
