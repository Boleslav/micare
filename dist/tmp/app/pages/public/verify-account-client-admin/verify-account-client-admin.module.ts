import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/index';

import { VerifyAccountClientAdminComponent } from './index';

@NgModule({
    imports: [SharedModule],
    declarations: [VerifyAccountClientAdminComponent],
    providers: []
})

export class VerifyAccountClientAdminModule { }
