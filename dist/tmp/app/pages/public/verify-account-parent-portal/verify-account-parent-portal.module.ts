import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/index';

import { VerifyAccountParentPortalComponent } from './index';

@NgModule({
    imports: [SharedModule],
    declarations: [VerifyAccountParentPortalComponent],
    providers: []
})

export class VerifyAccountParentPortalModule { }
