import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs';

import { ApiClientService, CentreLogoDto } from '../../../api/index';

@Component({
  moduleId: module.id,
  selector: 'verify-account-parent-portal-cmp',
  templateUrl: 'verify-account-parent-portal.component.html'
})

export class VerifyAccountParentPortalComponent implements OnInit {

  private verificationCompleted: boolean = undefined;
  private token: string = undefined;
  private centreName: string = undefined;
  private logo: string = null;

  constructor(
    private _apiClient: ApiClientService,
    private _route: ActivatedRoute) {
  }

  ngOnInit() {

    this._route
      .params
      .flatMap((params: Params) => this.getLogo$(this._route))
      .flatMap(() => {
        return this._route
          .params
          .switchMap((params: Params) => {
            this.token = params['token'];
            this.centreName = params['centreName'];
            return this._apiClient.account_VerifyEmail(this.token);
          });
      }).subscribe(res => this.verificationCompleted = true);
  }

  getLogo$(route: ActivatedRoute): Observable<void> {
    return route
      .data
      .flatMap((data: { centreLogo: CentreLogoDto }) => {
        if (data.centreLogo && data.centreLogo.imageBase64) {
          this.logo = data.centreLogo.imageBase64;
        }
        return Observable.of(null);
      });
  }
}
