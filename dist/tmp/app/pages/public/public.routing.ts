import { Routes } from '@angular/router';
import { PublicComponent } from './index';
import { MixpanelGuard } from '../../shared/index';

import { ChangePasswordRoutes } from './change-password/index';
import { ChangePasswordParentPortalRoutes } from './change-password-parent-portal/index';
import { ResetPasswordRoutes } from './reset-password/index';
import { ResetPasswordParentPortalRoutes } from './reset-password-parent-portal/index';
import { VerifyAccountClientAdminRoutes } from './verify-account-client-admin/index';
import { VerifyAccountParentRoutes } from './verify-account-parent/index';
import { VerifyAccountParentPortalRoutes } from './verify-account-parent-portal/index';

export const PublicRoutes: Routes = [
    {
        path: 'public',
        component: PublicComponent,
        canActivateChild: [MixpanelGuard],
        children: [
            ...ChangePasswordRoutes,
            ...ChangePasswordParentPortalRoutes,
            ...ResetPasswordRoutes,
            ...ResetPasswordParentPortalRoutes,
            ...VerifyAccountClientAdminRoutes,
            ...VerifyAccountParentRoutes,
            ...VerifyAccountParentPortalRoutes
        ]
    }
];
