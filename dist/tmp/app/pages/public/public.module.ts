import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/index';

import { PublicComponent } from './public.component';
import { ChangePasswordModule } from './change-password/index';
import { ChangePasswordParentPortalModule } from './change-password-parent-portal/index';
import { ResetPasswordModule } from './reset-password/index';
import { ResetPasswordParentPortalModule } from './reset-password-parent-portal/index';
import { VerifyAccountClientAdminModule } from './verify-account-client-admin/index';
import { VerifyAccountParentModule } from './verify-account-parent/index';
import { VerifyAccountParentPortalModule } from './verify-account-parent-portal/index';
import { CentreService, CentreLogoResolver } from './components/index';

@NgModule({
    imports: [
        ChangePasswordModule,
        ChangePasswordParentPortalModule,
        ResetPasswordModule,
        ResetPasswordParentPortalModule,
        VerifyAccountClientAdminModule,
        VerifyAccountParentModule,
        VerifyAccountParentPortalModule,
        SharedModule
    ],
    providers: [CentreLogoResolver, CentreService],
    declarations: [
        PublicComponent
    ]
})
export class PublicModule { }
