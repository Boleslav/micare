import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { ApiClientService, EmailModel, CentreLogoDto } from '../../../api/index';
import { EmailValidator } from '../../../validators/index';

@Component({
    moduleId: module.id,
    selector: 'reset-password-parent-portal-cmp',
    templateUrl: 'reset-password-parent-portal.component.html'
})
export class ResetPasswordParentPortalComponent implements OnInit {

    public form: FormGroup;
    public email: AbstractControl;
    private logo: string = null;
    private centreName: string = null;
    private emailStr: string = null;
    private resetPasswordSubmitted: boolean = false;
    private resetPasswordConfirmed: boolean = false;

    constructor(fb: FormBuilder, private _apiClient: ApiClientService, private _route: ActivatedRoute) {
        this.form = fb.group({
            'email': ['', Validators.compose([Validators.required, EmailValidator.validate])]
        });
        this.email = this.form.controls['email'];
    }

    ngOnInit() {

        this._route
            .params
            .subscribe((params: Params) => {
                this.centreName = params['centreName'];
                return this.getLogo$(this._route);
            });
    }

    getLogo$(route: ActivatedRoute) {
        route.data
            .subscribe((data: { centreLogo: CentreLogoDto }) => {
                if (data.centreLogo && data.centreLogo.imageBase64) {
                    this.logo = data.centreLogo.imageBase64;
                }
            });
    }

    resetPassword(): void {

        this.resetPasswordSubmitted = true;
        let emailModel = new EmailModel();
        emailModel.init({
            Email: this.emailStr
        });
        this._apiClient
            .account_RequestResetPasswordParentPortal(emailModel, this.centreName)
            .subscribe(res => {
                this.resetPasswordConfirmed = true;
            });
    }
}
