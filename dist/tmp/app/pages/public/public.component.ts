import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    moduleId: module.id,
    encapsulation: ViewEncapsulation.None,
    selector: 'public-cmp',
    templateUrl: 'public.component.html',
    styleUrls: ['public.component.css']
})
export class PublicComponent {
}
