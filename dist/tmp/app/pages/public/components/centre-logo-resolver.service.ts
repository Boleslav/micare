import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { CentreService } from './index';
import { CentreLogoDto } from '../../../api/index';

@Injectable()
export class CentreLogoResolver implements Resolve<CentreLogoDto> {
    constructor(private _centreService: CentreService) { }
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<CentreLogoDto> {
        let centreName = route.params['centreName'];
        if (centreName !== undefined) {
            return this._centreService.getCentreLogo$(centreName);
        } else {
            return Observable.of(null);
        }
    }
}
