import { Route } from '@angular/router';
import { VerifyAccountParentComponent } from './index';

export const VerifyAccountParentRoutes: Route[] = [
	{
		path: 'verify-account-parent/:token', component: VerifyAccountParentComponent
	}
];
