import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { ApiClientService, EmailModel } from '../../../api/index';
import { EmailValidator } from '../../../validators/index';

@Component({
    moduleId: module.id,
    selector: 'reset-password-cmp',
    templateUrl: 'reset-password.component.html'
})
export class ResetPasswordComponent {

    public form: FormGroup;
    public email: AbstractControl;
    private emailStr: string = null;
    private resetPasswordSubmitted: boolean = false;
    private resetPasswordConfirmed: boolean = false;

    constructor(fb: FormBuilder, private _apiClient: ApiClientService, private _route: ActivatedRoute) {
        this.form = fb.group({
            'email': ['', Validators.compose([Validators.required, EmailValidator.validate])]
        });
        this.email = this.form.controls['email'];
    }

    resetPassword(): void {

        this.resetPasswordSubmitted = true;
        let emailModel = new EmailModel();
        emailModel.init({
            Email: this.emailStr
        });
        this._apiClient
            .account_RequestResetPassword(emailModel)
            .subscribe(res => {
                this.resetPasswordConfirmed = true;
            });
    }
}
