import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { CentreService } from '../index';
import { ParentUserService, AccessTokenService } from '../../../core/index';
import { CentreLogoDto } from '../../../api/index';

@Component({
    moduleId: module.id,
    selector: 'parent-logout-cmp',
    templateUrl: 'parent-logout.component.html'
})

export class ParentLogoutComponent implements OnInit {

    private centreName: string = null;
    private logo: string = null;

    constructor(
        private _authenticationService: AccessTokenService,
        private _userService: ParentUserService,
        private _centreService: CentreService,
        private _route: ActivatedRoute) {
    }

    ngOnInit() {

        this._route
            .data
            .subscribe((data: { centreLogo: CentreLogoDto }) => {
                if (data.centreLogo.imageBase64) {
                    this.logo = data.centreLogo.imageBase64;
                }
            });

        this.centreName = this._centreService.SelectedCentreUniqueName;

        this._userService.Clear();
        this._centreService.logout();
        this._authenticationService.logout();
    }
}
