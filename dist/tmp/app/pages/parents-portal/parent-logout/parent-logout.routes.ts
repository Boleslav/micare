import { Route } from '@angular/router';
import { ParentLogoutComponent } from './index';
import { CentreLogoResolver } from '../index';

export const ParentLogoutRoutes: Route[] = [
    {
        path: 'parent-logout/:centreName',
        component: ParentLogoutComponent,
        resolve: {
            centreLogo: CentreLogoResolver
        }
    }
];
