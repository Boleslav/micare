import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { EnrolmentService } from '../index';
import { CentreService } from '../../index';

@Injectable()
export class EnrolmentFormConsentResolver implements Resolve<boolean> {
    constructor(
        private _enrolmentService: EnrolmentService,
        private _centreService: CentreService) { }
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> {
        return this._enrolmentService
            .enrolmentFormAccessGranted$(this._centreService.SelectedCentreId);
    }
}
