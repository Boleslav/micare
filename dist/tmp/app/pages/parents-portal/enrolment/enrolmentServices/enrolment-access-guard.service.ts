import { Injectable } from '@angular/core';
import {
    CanActivateChild, Router,
    ActivatedRouteSnapshot,
    RouterStateSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { EnrolmentService } from '../index';
import { CentreService } from '../../index';

@Injectable()
export class EnrolmentAccessGuardService implements CanActivateChild {

    constructor(
        private _enrolmentService: EnrolmentService,
        private _centreService: CentreService,
        private router: Router) {

    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {

        // If selected centre hasn't initialised yet, allow activation.
        // This method will be called again once SelectedCentreId initialised
        if (this._centreService.SelectedCentreId === null) {
            return Observable.of(false);
        }
        return this._enrolmentService
            .enrolmentFormAccessGranted$(this._centreService.SelectedCentreId)
            .flatMap((hasAccess: boolean) => {
                if (!hasAccess) {
                    this.router.navigate(['parents-portal', this._centreService.SelectedCentreUniqueName, 'enrolment']);
                    return Observable.of(false);
                } else {
                    return Observable.of(true);
                }
            });
    }
}
