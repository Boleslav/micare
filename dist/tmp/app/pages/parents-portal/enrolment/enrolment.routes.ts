import { Route } from '@angular/router';
import { EnrolmentComponent, EnrolmentFormConsentResolver, EnrolmentAccessGuardService } from './index';
import { EnrolmentFormRoutes } from './enrolment-form/index';
import { EnrolmentParentEditRoutes } from './parent-edit-enrolment/index';
import { EnrolmentGuardianEditRoutes } from './guardian-edit-enrolment/index';
import { EnrolmentContactEditRoutes } from './contact-edit-enrolment/index';
import { EnrolmentKidEditRoutes } from './kid-edit-enrolment/index';

export const EnrolmentRoutes: Route[] = [
	{
		path: 'enrolment',
		component: EnrolmentComponent,
		resolve: {
			enrolmentFormConsent: EnrolmentFormConsentResolver
		},
		canActivateChild: [EnrolmentAccessGuardService],
		children: [
			...EnrolmentFormRoutes,
			...EnrolmentParentEditRoutes,
			...EnrolmentGuardianEditRoutes,
			...EnrolmentContactEditRoutes,
			...EnrolmentKidEditRoutes
		]
	}
];
