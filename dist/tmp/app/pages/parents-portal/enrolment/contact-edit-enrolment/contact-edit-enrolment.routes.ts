import { Route } from '@angular/router';
import { EnrolmentContactEditComponent } from './index';

export const EnrolmentContactEditRoutes: Route[] = [
	{
		path: 'contact-edit-enrolment',
    component: EnrolmentContactEditComponent
	}
];
