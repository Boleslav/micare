import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../shared/index';

import { EnrolmentContactEditComponent } from './index';

@NgModule({
  imports: [SharedModule],
  declarations: [EnrolmentContactEditComponent],
    exports: [EnrolmentContactEditComponent]
})

export class EnrolmentContactEditModule { }
