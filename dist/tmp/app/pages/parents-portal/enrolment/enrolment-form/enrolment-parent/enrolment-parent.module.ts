import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../../shared/index';

import { EnrolmentParentComponent } from './index';

@NgModule({
    imports: [SharedModule],
    declarations: [EnrolmentParentComponent],
    providers: [],
    exports: [EnrolmentParentComponent]
})

export class EnrolmentParentModule { }
