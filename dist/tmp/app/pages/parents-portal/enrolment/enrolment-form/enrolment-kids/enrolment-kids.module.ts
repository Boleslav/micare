import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../../shared/index';

import { EnrolmentKidComponent, EnrolmentSingleKidComponent } from './index';

@NgModule({
    imports: [SharedModule],
    declarations: [EnrolmentKidComponent, EnrolmentSingleKidComponent],
    providers: [],
    exports: [EnrolmentKidComponent]
})

export class EnrolmentKidModule { }
