import { Route } from '@angular/router';
import { EnrolmentFormComponent } from './index';

export const EnrolmentFormRoutes: Route[] = [
    {
        path: 'enrolment-form',
        component: EnrolmentFormComponent
    }
];
