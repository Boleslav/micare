import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../../shared/index';

import { EnrolmentGuardianComponent, EnrolmentSingleGuardianComponent } from './index';

@NgModule({
    imports: [SharedModule],
    declarations: [EnrolmentGuardianComponent, EnrolmentSingleGuardianComponent],
    providers: [],
    exports: [EnrolmentGuardianComponent]
})

export class EnrolmentGuardianModule { }
