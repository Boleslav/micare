import { Component, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { GuardianDto } from '../../../../../../api/index';

@Component({
  moduleId: module.id,
  encapsulation: ViewEncapsulation.None,
  selector: 'enrolment-single-guardian-cmp',
  templateUrl: 'enrolment-single-guardian.component.html',
  styleUrls: ['enrolment-single-guardian.component.css']
})

export class EnrolmentSingleGuardianComponent {

  @Input() guardian: GuardianDto;
  @Output() onDeleteGuardian = new EventEmitter<string>();

  constructor(private _router: Router,
    private _route: ActivatedRoute) {
  }

  editGuardian(guardianId: string): void {
    let navigationExtras: NavigationExtras = {
      queryParams: { 'guardianId': guardianId },
      relativeTo: this._route
    };
    this._router.navigate(['../guardian-edit-enrolment'], navigationExtras);
  }

  deleteGuardian(guardianId: string): void {
    this.onDeleteGuardian.emit(guardianId);
  }
}
