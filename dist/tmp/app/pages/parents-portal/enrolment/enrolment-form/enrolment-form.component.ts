import { Component } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'enrolment-form-cmp',
  templateUrl: 'enrolment-form.component.html',
  styleUrls: ['enrolment-form.component.css']
})

export class EnrolmentFormComponent {
  private enrolmentFormId: string = null;

  public onEnrolmentFormId(value: any): void {
    this.enrolmentFormId = value;
  }
}
