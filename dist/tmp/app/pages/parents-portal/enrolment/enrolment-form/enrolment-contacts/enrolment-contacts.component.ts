import { Component, ViewEncapsulation, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { ToasterService, BodyOutputType } from 'angular2-toaster';

import { EnrolmentService } from '../../index';
import { SpinnerComponent } from '../../../../../shared/index';
import { ParentUserService } from '../../../../../core/index';
import { ContactDto } from '../../../../../api/index';

@Component({
  moduleId: module.id,
  encapsulation: ViewEncapsulation.None,
  selector: 'enrolment-contacts-cmp',
  templateUrl: 'enrolment-contacts.component.html',
  styleUrls: ['enrolment-contacts.component.css']
})

export class EnrolmentContactComponent implements OnInit {

  @Input() enrolmentFormId: string;
  private contactEnrolments: ContactDto[];
  constructor(private _router: Router,
    private _route: ActivatedRoute,
    private _enrolmentService: EnrolmentService,
    private _userService: ParentUserService,
    private _toasterService: ToasterService) {
    this.contactEnrolments = new Array();
  }

  ngOnInit(): void {
    this.getContacts();
  }

  getContacts() {
    this._enrolmentService
      .getContacts$(this.enrolmentFormId)
      .subscribe(res => {
        this.contactEnrolments = res;
      });
  }

  addContact(): void {
    let navigationExtras: NavigationExtras = {
      queryParams: { 'enrolmentFormId': this.enrolmentFormId },
      relativeTo: this._route
    };
    this._router.navigate(['../contact-edit-enrolment'], navigationExtras);
  }

  deleteContact(contactId: string): void {

    let savingToast = this._toasterService.pop({
      type: 'info',
      title: 'Saving',
      timeout: 0,
      body: SpinnerComponent,
      bodyOutputType: BodyOutputType.Component
    });

    this._enrolmentService
      .deleteContact$(contactId)
      .map(() => {
        this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
        this._toasterService.popAsync('success', 'Saved', '');
        this.contactEnrolments = null;
      })
      .subscribe(() => this.getContacts());
  }
}
