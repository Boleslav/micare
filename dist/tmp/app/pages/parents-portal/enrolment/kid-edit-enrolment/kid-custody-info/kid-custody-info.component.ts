import { Component, OnInit, ViewEncapsulation, } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs';

import { EnrolmentService } from '../../index';
import { CentreService } from '../../../index';
import { ChildCustodyInformationDto, CreateUpdateChildCustodyInformationDto } from '../../../../../api/index';
import { ToasterService } from 'angular2-toaster';
import { SpinningToast } from '../../../../../shared/index';

@Component({
  moduleId: module.id,
  encapsulation: ViewEncapsulation.None,
  selector: 'kid-custody-info-cmp',
  templateUrl: 'kid-custody-info.component.html',
  styleUrls: ['kid-custody-info.component.css']
})

export class KidCustodyInfoComponent implements OnInit {

  private centreName: string = null;
  private form: FormGroup;
  private kidDto: ChildCustodyInformationDto = null;
  private kidId: string;
  private jointCustody: AbstractControl;
  private courtOrderAccessibility: AbstractControl;
  private courtOrderResidence: AbstractControl;

  constructor(private _router: Router,
    private _route: ActivatedRoute,
    private _enrolmentService: EnrolmentService,
    private _centreService: CentreService,
    private _toasterService: ToasterService,
    fb: FormBuilder) {
    this.initForm(fb);
  }

  ngOnInit(): void {

    this.centreName = this._centreService.SelectedCentreQualifiedName;
    this._route
      .params
      .flatMap((params: Params) => {
        this.kidId = params['kidId'];
        return Observable.of(this.kidId);
      })
      .flatMap(kidId => this._enrolmentService.getChildCustodyInformation$(this.kidId))
      .subscribe(res => {
        this.kidDto = res;
      });
  }

  saveChanges() {
    let savingToast = this._toasterService.pop(new SpinningToast());
    let updateCustodyInfo = new CreateUpdateChildCustodyInformationDto();
    updateCustodyInfo.init(this.kidDto.toJSON());
    updateCustodyInfo.kidId = this.kidId;
    this._enrolmentService
      .addUpdateChildCustodyInformationDto$(updateCustodyInfo)
      .subscribe(res => {
        this.kidDto = res;
        this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
        this._toasterService.popAsync('success', 'Saved', '');
        this._router.navigate(['../../enrolment-form'], { relativeTo: this._route });
      },
      (error) => {
        this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
      });
  }

  cancelEdit() {
    this._router.navigate(['../../enrolment-form'], { relativeTo: this._route });
  }

  private initForm(fb: FormBuilder): void {
    this.form = fb.group({
      'jointCustody': [''],
      'courtOrderAccessibility': [''],
      'courtOrderResidence': [''],
    });

    this.jointCustody = this.form.controls['jointCustody'];
    this.courtOrderAccessibility = this.form.controls['courtOrderAccessibility'];
    this.courtOrderResidence = this.form.controls['courtOrderResidence'];
  }
}
