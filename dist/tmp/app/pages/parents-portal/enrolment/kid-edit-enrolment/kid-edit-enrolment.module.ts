import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../shared/index';

import { EnrolmentKidEditComponent } from './kid-edit-details/index';
import {
  KidDesiredDaysComponent, DisplayDisabledCheckboxComponent,
  DisplayCareTypeComponent, EditDateComponent
} from './kid-desired-days/index';
import { KidHealthInfoComponent } from './kid-health-info/index';
import { KidCustodyInfoComponent } from './kid-custody-info/index';

@NgModule({
  imports: [SharedModule],
  declarations: [
    EnrolmentKidEditComponent,
    KidDesiredDaysComponent,
    DisplayDisabledCheckboxComponent,
    DisplayCareTypeComponent,
    EditDateComponent,
    KidHealthInfoComponent,
    KidCustodyInfoComponent],
  entryComponents: [DisplayDisabledCheckboxComponent, DisplayCareTypeComponent, EditDateComponent]
})

export class EnrolmentKidEditModule { }
