import { FormGroup, AbstractControl } from '@angular/forms';

export class AboriginalOrTorresValidator {

  public static validate(firstField: string, secondField: string, thirdField: string) {

    return (c: FormGroup) => {

      return this.shouldDisable(c.controls, firstField, secondField, thirdField) ? {
        aboriginalOrTorres: {
          valid: false
        }
      } : null;
    };
  }

  private static shouldDisable(controls: { [key: string]: AbstractControl },
    firstField: string, secondField: string, thirdField: string): boolean {
    return (controls
      && (this.hasNoValues(controls[firstField].value, controls[secondField].value, controls[thirdField].value))
    );
  }

  private static hasNoValues(controlOneValue: string, controlTwoValue: string, controlThreeValue: string) {
    return this.isFalseOrEmpty(controlOneValue) && this.isFalseOrEmpty(controlTwoValue) && this.isFalseOrEmpty(controlThreeValue);
  }

  private static isFalseOrEmpty(value: string) {
    return (value === null || value === undefined || value === '' || this.isFalse(value));
  }

  private static isFalse(value: any) {
    return value === false;
  }
}
