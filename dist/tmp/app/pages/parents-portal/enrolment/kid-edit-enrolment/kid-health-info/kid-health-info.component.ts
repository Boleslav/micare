import { Component, OnInit, ViewChild, ViewEncapsulation, } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs';
import { ModalDirective } from 'ng2-bootstrap';
import * as _ from 'lodash';

import { EnrolmentService } from '../../index';
import { CentreService } from '../../../index';
import { KidHealthInformationDto, CreateUpdateKidHealthInformationDto } from '../../../../../api/index';
import { ToasterService } from 'angular2-toaster';
import { SpinningToast } from '../../../../../shared/index';

@Component({
  moduleId: module.id,
  encapsulation: ViewEncapsulation.None,
  selector: 'kid-health-info-cmp',
  templateUrl: 'kid-health-info.component.html',
  styleUrls: ['kid-health-info.component.css']
})

export class KidHealthInfoComponent implements OnInit {

  @ViewChild('staticModal') public staticModal: ModalDirective;
  private centreName: string = null;
  private pendingFunction: any = null;
  private form: FormGroup;
  private kidDto: KidHealthInformationDto = null;
  private kidDtoModified: KidHealthInformationDto = null;
  private kidId: string;
  private medicalServicesName: AbstractControl;
  private medicalServicesContactNumber: AbstractControl;
  private medicareNumber: AbstractControl;
  private medicareExpiryDate: AbstractControl;
  private healthcareCardNumber: AbstractControl;
  private healthcareCardExpiryDate: AbstractControl;
  private privateHealthInsurer: AbstractControl;
  private privateHealthMembershipNumber: AbstractControl;
  private hasAllergies: AbstractControl;
  private hasAnaphylaxis: AbstractControl;
  private hasInjectionDevice: AbstractControl;
  private hasAsthma: AbstractControl;
  private isImmunised: AbstractControl;
  private hasSpecialNeeds: AbstractControl;
  private hasSupportServices: AbstractControl;
  private dietaryNeeds: AbstractControl;
  private dentalNeeds: AbstractControl;
  private medicalCondition1: AbstractControl;
  private medicalCondition2: AbstractControl;
  private medicalCondition3: AbstractControl;
  private medicalCondition4: AbstractControl;
  private consentToAccuracy: AbstractControl;
  private consentToCollection: AbstractControl;
  private consentToTreatment: AbstractControl;
  private contentToCost: AbstractControl;

  constructor(private _router: Router,
    private _route: ActivatedRoute,
    private _enrolmentService: EnrolmentService,
    private _centreService: CentreService,
    private _toasterService: ToasterService,
    fb: FormBuilder) {
    this.initForm(fb);
  }

  ngOnInit(): void {

    this.centreName = this._centreService.SelectedCentreQualifiedName;
    this._route
      .params
      .flatMap((params: Params) => {
        this.kidId = params['kidId'];
        return Observable.of(this.kidId);
      })
      .flatMap(kidId => this._enrolmentService.getKidHealthInformation$(this.kidId))
      .subscribe(res => {
        this.kidDto = res;
        this.kidDtoModified = _.cloneDeep(this.kidDto);
      });
  }

  saveChangesAndGoBack(): void {
    this.createOrUpdateHealthInfo(['../../enrolment-form']);
  }

  saveChangesAndContinue(): void {
    this.createOrUpdateHealthInfo(['../../kid-custody-info', this.kidId]);
  }

  createOrUpdateHealthInfo(redirectUrl: string[]): void {

    if (this.kidDto.kidId === '00000000-0000-0000-0000-000000000000') {
      this.saveAndRedirect(redirectUrl);
    } else if (this.wasNotModified(this.kidDto, this.kidDtoModified)) {
      this.saveAndRedirect(redirectUrl);
    } else {
      this.pendingFunction = () => this.saveAndRedirect(redirectUrl);
      this.staticModal.show();
    }
  }

  saveAndRedirect(redirectUrl: string[]) {
    let savingToast = this._toasterService.pop(new SpinningToast());
    let updateHealthInfo = new CreateUpdateKidHealthInformationDto();
    updateHealthInfo.init(this.kidDto.toJSON());
    updateHealthInfo.kidId = this.kidId;
    this._enrolmentService
      .addUpdateKidHealthInformationDto$(updateHealthInfo)
      .subscribe(res => {
        this.kidDto = res;
        this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
        this._toasterService.popAsync('success', 'Saved', '');
        this._router.navigate(redirectUrl, { relativeTo: this._route });
      },
      (error) => {
        this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
      });
  }

  cancelEdit() {
    this._router.navigate(['../../enrolment-form'], { relativeTo: this._route });
  }

  onHide(event: any): void {
    this.pendingFunction();
  }

  private initForm(fb: FormBuilder): void {
    this.form = fb.group({
      'medicalServicesName': ['', Validators.maxLength(100)],
      'medicalServicesContactNumber': ['', Validators.maxLength(50)],
      'medicareNumber': ['', Validators.maxLength(50)],
      'medicareExpiryDate': [''],
      'healthcareCardNumber': ['', Validators.maxLength(50)],
      'healthcareCardExpiryDate': [''],
      'privateHealthInsurer': ['', Validators.maxLength(100)],
      'privateHealthMembershipNumber': ['', Validators.maxLength(100)],
      'hasAllergies': [''],
      'hasAnaphylaxis': [''],
      'hasInjectionDevice': [''],
      'hasAsthma': [''],
      'isImmunised': [''],
      'hasSpecialNeeds': [''],
      'hasSupportServices': [''],
      'dietaryNeeds': ['', Validators.maxLength(2000)],
      'dentalNeeds': ['', Validators.maxLength(2000)],
      'medicalCondition1': ['', Validators.maxLength(100)],
      'medicalCondition2': ['', Validators.maxLength(100)],
      'medicalCondition3': ['', Validators.maxLength(100)],
      'medicalCondition4': ['', Validators.maxLength(100)],
      'consentToAccuracy': ['', Validators.required],
      'consentToCollection': ['', Validators.required],
      'consentToTreatment': ['', Validators.required],
      'contentToCost': ['', Validators.required]
    });

    this.medicalServicesName = this.form.controls['medicalServicesName'];
    this.medicalServicesContactNumber = this.form.controls['medicalServicesContactNumber'];
    this.medicareNumber = this.form.controls['medicareNumber'];
    this.medicareExpiryDate = this.form.controls['medicareExpiryDate'];
    this.healthcareCardNumber = this.form.controls['healthcareCardNumber'];
    this.healthcareCardExpiryDate = this.form.controls['healthcareCardExpiryDate'];
    this.privateHealthInsurer = this.form.controls['privateHealthInsurer'];
    this.privateHealthMembershipNumber = this.form.controls['privateHealthMembershipNumber'];
    this.hasAllergies = this.form.controls['hasAllergies'];
    this.hasAnaphylaxis = this.form.controls['hasAnaphylaxis'];
    this.hasInjectionDevice = this.form.controls['hasInjectionDevice'];
    this.hasAsthma = this.form.controls['hasAsthma'];
    this.isImmunised = this.form.controls['isImmunised'];
    this.hasSpecialNeeds = this.form.controls['hasSpecialNeeds'];
    this.hasSupportServices = this.form.controls['hasSupportServices'];
    this.dietaryNeeds = this.form.controls['dietaryNeeds'];
    this.dentalNeeds = this.form.controls['dentalNeeds'];
    this.medicalCondition1 = this.form.controls['medicalCondition1'];
    this.medicalCondition2 = this.form.controls['medicalCondition2'];
    this.medicalCondition3 = this.form.controls['medicalCondition3'];
    this.medicalCondition4 = this.form.controls['medicalCondition4'];
    this.consentToAccuracy = this.form.controls['consentToAccuracy'];
    this.consentToCollection = this.form.controls['consentToCollection'];
    this.consentToTreatment = this.form.controls['consentToTreatment'];
    this.contentToCost = this.form.controls['contentToCost'];
  }

  private wasNotModified(modifiedKid: KidHealthInformationDto, originalKid: KidHealthInformationDto): boolean {
    return _.isEqual(modifiedKid, originalKid);
  }
}
