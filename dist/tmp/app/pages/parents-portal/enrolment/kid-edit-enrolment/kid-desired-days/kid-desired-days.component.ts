import { Component, OnInit, ViewEncapsulation, } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { DisplayDisabledCheckboxComponent, DisplayCareTypeComponent, EditDateComponent } from './index';
import { EnrolmentService } from '../../index';
import { CentreService } from '../../../index';
import {
  EnrolmentFormDesiredDaysDto,
  CreateUpdateEnrolmentFormDesiredDaysDto,
  DeleteEnrolmentFormDesiredDaysDto
} from '../../../../../api/index';
import { ToasterService } from 'angular2-toaster';
import { SpinningToast } from '../../../../../shared/index';
import { LocalDataSource } from '../../../../../shared/ng2-smart-table/ng2-smart-table/lib/data-source/local/local.data-source';

@Component({
  moduleId: module.id,
  encapsulation: ViewEncapsulation.None,
  selector: 'kid-desired-days-cmp',
  templateUrl: 'kid-desired-days.component.html',
  styleUrls: ['kid-desired-days.component.css']
})

export class KidDesiredDaysComponent implements OnInit {

  private settings: any;
  private source: LocalDataSource = new LocalDataSource();
  private desiredDays: EnrolmentFormDesiredDaysDto[] = null;
  private careTypes: { [key: string]: string };
  private centreId: string;
  private kidId: string;

  constructor(private _router: Router,
    private _route: ActivatedRoute,
    private _enrolmentService: EnrolmentService,
    private _centreService: CentreService,
    private _toasterService: ToasterService) {
  }

  ngOnInit(): void {

    this._enrolmentService
      .getCareTypesForCentre$(this._centreService.SelectedCentreId)
      .map(res => this.careTypes = res)
      .flatMap(() => this._route.params)
      .map((params: Params) => {
        this.kidId = params['kidId'];
        this.centreId = this._centreService.SelectedCentreId;
      })
      .flatMap(() => this._enrolmentService.getKidDesiredDays$(this.centreId, this.kidId))
      .subscribe(res => {
        this.initSettings();
        this.desiredDays = res;
        this.source.load(this.desiredDays);
      });
  }

  addDesiredDays(event: any): void {
    this.addUpdateDesiredDays(event);
  }

  editDesiredDays(event: any): void {
    this.addUpdateDesiredDays(event);
  }

  addUpdateDesiredDays(event: any): void {

    if (event.newData.desiredCareType === '') {
      this._toasterService
        .popAsync('error', 'Care type cannot be empty', '');
      return;
    }

    this.source.getElements()
      .then((elements: EnrolmentFormDesiredDaysDto[]) => {

        var desiredCareTypeExists =
          elements.filter((el: EnrolmentFormDesiredDaysDto) =>
            el.desiredCareType === parseInt(event.newData.desiredCareType) &&
            (event.data === undefined ||
              parseInt(event.newData.desiredCareType) !== event.data.desiredCareType)
          ).length > 0;

        if (desiredCareTypeExists) {
          this._toasterService
            .popAsync('error', 'Days for ' + this.careTypes[event.newData.desiredCareType] + ' already exist', '');
          return event.confirm.reject();
        } else {
          let savingToast = this._toasterService.pop(new SpinningToast());
          let updateDesiredDays: CreateUpdateEnrolmentFormDesiredDaysDto = new CreateUpdateEnrolmentFormDesiredDaysDto();
          updateDesiredDays.init({
            CentreId: this.centreId,
            KidId: this.kidId,
            DesiredCareType: event.newData.desiredCareType,
            DesiredStartDate: event.newData.desiredStartDate,
            Mon: event.newData.mon,
            Tue: event.newData.tue,
            Wed: event.newData.wed,
            Thu: event.newData.thu,
            Fri: event.newData.fri,
            Sat: event.newData.sat,
            Sun: event.newData.sun
          });

          this._enrolmentService
            .addUpdateKidDesiredDays$(updateDesiredDays)
            .toPromise()
            .then((res) => {
              this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
              this._toasterService.popAsync('success', 'Saved', '');
              return event.confirm.resolve(res);
            })
            .catch((error) => {
              this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
              return event.confirm.reject();
            });
        }
      });
  }

  removeDesiredDays(event: any): void {
    if (window.confirm('Are you sure you want to delete?')) {

      let savingToast = this._toasterService.pop(new SpinningToast());
      let daysToDelete = new DeleteEnrolmentFormDesiredDaysDto();
      daysToDelete.init({
        CentreId: this.centreId,
        KidId: this.kidId,
        CareType: event.data.desiredCareType,
      });
      this._enrolmentService
        .deleteKidDesiredDays$(daysToDelete)
        .toPromise()
        .then((res) => {
          this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
          this._toasterService.popAsync('success', 'Deleted', '');
          return event.confirm.resolve();
        })
        .catch((error) => {
          this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
          return event.confirm.reject();
        });
    } else {
      event.confirm.reject();
    }
  }

  continue(): void {
    this._router.navigate(['../../kid-health-info', this.kidId], { relativeTo: this._route });
  }

  cancelEdit(): void {
    this._router.navigate(['../../enrolment-form'], { relativeTo: this._route });
  }

  getCareTypeViewModel(): { [key: string]: string }[] {
    let careTypesView: { [key: string]: string }[] = new Array<{ [key: string]: string }>();
    for (let key in this.careTypes) {
      if (this.careTypes.hasOwnProperty(key)) {
        careTypesView.push({
          title: this.careTypes[key],
          value: key
        });
      }
    }
    return careTypesView;
  }

  private initSettings(): void {
    this.settings = {
      mode: 'internal',
      hideHeader: false,
      hideSubHeader: false,
      sort: false,
      actions: {
        columnTitle: '',
        add: true,
        edit: true,
        delete: true,
        position: 'right'
      },
      add: {
        addButtonContent: 'Add',
        createButtonContent: 'Create',
        cancelButtonContent: 'Cancel',
        confirmCreate: true
      },
      edit: {
        editButtonContent: 'Edit',
        saveButtonContent: 'Save',
        cancelButtonContent: 'Cancel',
        confirmSave: true
      },
      delete: {
        deleteButtonContent: 'Remove',
        confirmDelete: true
      },
      columns: {
        desiredCareType: {
          title: 'Care Type',
          filter: false,
          type: 'custom',
          renderComponent: DisplayCareTypeComponent,
          editor: {
            type: 'list',
            config: {
              list: this.getCareTypeViewModel()
            }
          },
        },
        mon: {
          title: 'Monday',
          filter: false,
          type: 'custom',
          renderComponent: DisplayDisabledCheckboxComponent,
          editor: {
            type: 'checkbox'
          }
        },
        tue: {
          title: 'Tuesday',
          filter: false,
          type: 'custom',
          renderComponent: DisplayDisabledCheckboxComponent,
          editor: {
            type: 'checkbox'
          }
        },
        wed: {
          title: 'Wednesday',
          filter: false,
          type: 'custom',
          renderComponent: DisplayDisabledCheckboxComponent,
          editor: {
            type: 'checkbox'
          }
        },
        thu: {
          title: 'Thursday',
          filter: false,
          type: 'custom',
          renderComponent: DisplayDisabledCheckboxComponent,
          editor: {
            type: 'checkbox'
          }
        },
        fri: {
          title: 'Friday',
          filter: false,
          type: 'custom',
          renderComponent: DisplayDisabledCheckboxComponent,
          editor: {
            type: 'checkbox'
          }
        },
        sat: {
          title: 'Saturday',
          filter: false,
          type: 'custom',
          renderComponent: DisplayDisabledCheckboxComponent,
          editor: {
            type: 'checkbox'
          }
        },
        sun: {
          title: 'Sunday',
          filter: false,
          type: 'custom',
          renderComponent: DisplayDisabledCheckboxComponent,
          editor: {
            type: 'checkbox'
          }
        },
        desiredStartDate: {
          title: 'Desired Start Date',
          filter: false,
          valuePrepareFunction: (value: any) => value !== '' ? value.toDateString() : '',
          editor: {
            type: 'custom',
            component: EditDateComponent
          }
        }
      },
      noDataMessage: 'No Desired Days'
    };
  }
}
