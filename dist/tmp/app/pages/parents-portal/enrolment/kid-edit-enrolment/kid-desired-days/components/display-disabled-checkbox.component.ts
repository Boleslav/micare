import { Component, Input } from '@angular/core';

import { ViewCell } from '../../../../../../shared/ng2-smart-table/index';

@Component({
    template: `
    <input name="inp-day" type="checkbox" [checked]="value" disabled="disabled" >
    `
})
export class DisplayDisabledCheckboxComponent implements ViewCell {

    @Input() value: string | number;
}
