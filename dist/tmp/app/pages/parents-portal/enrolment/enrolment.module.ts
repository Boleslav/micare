import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/index';

import {
    EnrolmentComponent, EnrolmentService,
    EnrolmentAccessGuardService, EnrolmentFormConsentResolver
} from './index';
import { EnrolmentFormModule } from './enrolment-form/index';
import { EnrolmentParentEditModule } from './parent-edit-enrolment/index';
import { EnrolmentGuardianEditModule } from './guardian-edit-enrolment/index';
import { EnrolmentContactEditModule } from './contact-edit-enrolment/index';
import { EnrolmentKidEditModule } from './kid-edit-enrolment/index';
import { ReCaptchaModule } from 'angular2-recaptcha';


@NgModule({
    imports: [
        SharedModule,
        ReCaptchaModule,
        EnrolmentFormModule,
        EnrolmentParentEditModule,
        EnrolmentGuardianEditModule,
        EnrolmentContactEditModule,
        EnrolmentKidEditModule
    ],
    declarations: [EnrolmentComponent],
    providers: [EnrolmentService, EnrolmentAccessGuardService, EnrolmentFormConsentResolver]
})

export class EnrolmentModule { }
