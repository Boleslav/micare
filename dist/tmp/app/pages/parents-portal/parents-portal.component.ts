import { Component, ViewEncapsulation, OnInit, AfterViewChecked } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
let colorString: any = require('color-string');
import * as _ from 'lodash';

import { ToasterService } from 'angular2-toaster';
import { CentreBrandingDto, CentreLogoDto } from '../../api/index';
import { Config } from '../../shared/index';

@Component({
    moduleId: module.id,
    encapsulation: ViewEncapsulation.None,
    selector: 'parents-portal-cmp',
    templateUrl: 'parents-portal.component.html',
    styleUrls: ['parents-portal.component.css']
})
export class ParentsPortalComponent implements OnInit, AfterViewChecked {

    private primaryColour: string;
    private primaryColourLight: string;
    private primaryColourExtraLight: string;
    private logo: string;

    constructor(
        private _toasterService: ToasterService,
        private _route: ActivatedRoute,
        private _router: Router) {

        if (Config.ENABLE_ENROLMENT_FEATURE !== 'true') {
            this._router.navigate(['/']);
        }

        this._toasterService.clear();

        if (Config.ENABLE_ENROLMENT_FEATURE !== 'true') {
            this._router.navigate(['/']);
        }
    }

    ngOnInit(): void {

        this._route
            .data
            .subscribe((data: { centreBrandingDto: CentreBrandingDto, centreLogo: CentreLogoDto }) => {
                this.primaryColour = data.centreBrandingDto.baseColour;
                let rgbVals = colorString.get.rgb(data.centreBrandingDto.baseColour);
                let rgbValsLight = _.clone(rgbVals);
                if (rgbValsLight.length === 4) {
                    rgbValsLight[3] = 0.6;
                }
                this.primaryColourLight = colorString.to.rgb(rgbValsLight);
                let rgbValsExtraLight = _.clone(rgbVals);
                if (rgbValsExtraLight.length === 4) {
                    rgbValsExtraLight[3] = 0.3;
                }
                this.primaryColourExtraLight = colorString.to.rgb(rgbValsExtraLight);
                this.logo = data.centreLogo.imageBase64;
            });
    }

    ngAfterViewChecked(): void {
        this.setPageHeaderStyle();
        this.setButtonPrimaryStyle();
        this.setBreadcrumbStyle();
        this.setLinkStyle();
        this.setProfileIconStyle();
    }

    setPageHeaderStyle(): void {
        const className: string = 'page-header';
        this.setCssClassStyle(className, 'color', this.primaryColour);
    }

    setButtonPrimaryStyle(): void {
        const className: string = 'btn-primary';
        this.setCssClassStyle(className, 'color', '#FFF');
        this.setCssClassStyle(className, 'background-color', this.primaryColour);
        this.setCssClassStyle(className, 'border-color', this.primaryColour);
    }

    setBreadcrumbStyle(): void {
        const className: string = 'breadcrumb';
        this.setCssClassStyle(className, 'background-color', this.primaryColourLight);
    }

    setLinkStyle(): void {
        const className: string = 'breadcrumb';
        const tagName: string = 'a';
        this.setTagStyle(className, tagName, 'color', this.primaryColour);
    }

    setProfileIconStyle(): void {
        const className: string = 'nav-link';
        this.setCssClassStyle(className, 'color', this.primaryColourExtraLight);
    }

    setCssClassStyle(className: string, property: string, color: string) {
        var elements = document.getElementsByClassName(className);

        for (let i: number = 0; i < elements.length; i++) {
            let elem = <HTMLElement>elements[i];
            let cssStyle: any = elem.style;
            cssStyle[property] = color;
        }
    }

    setTagStyle(parentClass: string, tagName: string, property: string, color: string) {
        var elements = document.getElementsByClassName(parentClass);

        for (let i: number = 0; i < elements.length; ++i) {
            let childElements = elements[i].getElementsByTagName(tagName);
            for (let j: number = 0; j < childElements.length; ++j) {
                let childElement = <HTMLElement>childElements[j];
                let cssStyle: any = childElement.style;
                cssStyle[property] = color;
            }
        }
    }
}
