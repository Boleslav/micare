import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ParentUserService } from '../../../../core/index';
import { ApiClientService, CentreLogoDto, CentreBrandingDto } from '../../../../api/index';

@Injectable()
export class CentreService {

    private readonly ImageEncoding: string = 'data:image/png;base64,';

    private logoDto: CentreLogoDto = null;
    private centreBrandingDto: CentreBrandingDto = null;

    private _selectedCentreQualifiedName: string = null;
    get SelectedCentreQualifiedName(): string {
        return this._selectedCentreQualifiedName;
    }

    private _selectedCentreUniqueName: string = null;
    get SelectedCentreUniqueName(): string {
        return this._selectedCentreUniqueName;
    }

    private _selectedCentreId: string = null;
    get SelectedCentreId(): string {
        return this._selectedCentreId;
    }

    constructor(private _apiClient: ApiClientService, private _parentService: ParentUserService) { }

    public getCentreBranding$(uniquePath: string): Observable<CentreBrandingDto> {

        if (this.centreBrandingDto !== null) {
            return Observable.of(this.centreBrandingDto);
        }
        return this.getCentreByUniquePath$(uniquePath)
            .flatMap(centreId => this._apiClient.centre_GetCentreBranding(centreId))
            .map(res => this.centreBrandingDto = res);
    }

    public getCentreByUniquePath$(uniquePath: string): Observable<string> {

        if (this._selectedCentreId !== null) {
            return Observable.of(this._selectedCentreId);
        }
        return this._apiClient
            .centre_GetCentreIdByUniqueName(uniquePath)
            .map((centre: { [key: string]: string }) => {
                let centreId: string = Object.keys(centre)[0];
                let centreName: string = centre[centreId];
                this._selectedCentreId = centreId;
                this._selectedCentreQualifiedName = centreName;
                this._selectedCentreUniqueName = uniquePath;
                this._parentService.SetLastSelectedCentreName(this._selectedCentreUniqueName);
            })
            .map(() => this.SelectedCentreId);
    }

    public getCentreLogo$(uniqueName: string): Observable<CentreLogoDto> {

        if (this.logoDto !== null) {
            return Observable.of(this.logoDto);
        }
        return this._apiClient
            .centre_GetCentreLogoByUniqueName(uniqueName)
            .map(res => {
                this.logoDto = res;
                this.logoDto.imageBase64 = this.ImageEncoding + this.logoDto.imageBase64;
            })
            .map(() => this.logoDto);
    }

    public doesCentreExist$(uniqueName: string): Observable<boolean> {
        return this
            ._apiClient
            .centre_CentreExistsByUniqueName(uniqueName);
    }

    public getParentEnrolmentForm$(parentId: string): Observable<boolean> {
        return this._apiClient.enrolmentForm_IsAccessEnrolmentFormGranted(parentId, this.SelectedCentreId);
    }

    public logout() {
        this.logoDto = null;
        this.centreBrandingDto = null;
        this._selectedCentreId = null;
        this._selectedCentreUniqueName = null;
        this._selectedCentreQualifiedName = null;
    }
}
