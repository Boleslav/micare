import { Injectable, Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { ParentLoginService } from '../index';
import { ToasterService } from 'angular2-toaster';
import { SpinningToast } from '../../../../shared/index';

@Component({
    moduleId: module.id,
    selector: 'parent-login-form-cmp',
    styleUrls: ['parentLoginForm.component.css'],
    templateUrl: 'parentLoginForm.component.html'
})

@Injectable()
export class ParentLoginFormComponent implements OnInit {
    public form: FormGroup;
    public email: AbstractControl;
    public password: AbstractControl;

    public submitted: boolean = false;
    private centreName: string = null;

    constructor(fb: FormBuilder,
        private _toasterService: ToasterService,
        private _loginService: ParentLoginService,
        private _route: ActivatedRoute,
        private _router: Router) {

        this.form = fb.group({
            'email': ['', Validators.required],
            'password': ['', Validators.required]
        });

        this.email = this.form.controls['email'];
        this.password = this.form.controls['password'];
    }

    ngOnInit(): void {
        this._route.params
            .subscribe((params: Params) => {
                this.centreName = params['centreName'];
            });
    }

    onSubmit(values: any) {
        this.submitted = true;
        var authenticatingToast = this._toasterService.pop(new SpinningToast('Authenticating'));

        this._loginService
            .login$(this.email.value, this.password.value)
            .subscribe(res => {
                this._router.navigate(['parents-portal', this.centreName]);
            },
            err => {
                this._toasterService.clear(authenticatingToast.toastId, authenticatingToast.toastContainerId);
                this.submitted = false;
            },
            () => {
                this.submitted = false;
            });
    };
}
