import { Component, OnInit } from '@angular/core';

import { ParentUserService } from '../../../../core/index';

@Component({
	moduleId: module.id,
	selector: 'sidebar-cmp',
	templateUrl: 'sidebar.html'
})

export class SidebarComponent implements OnInit {

	isActive = false;
	showMenu: string = '';
	isSysAdmin: boolean = false;
	isCentreManager: boolean = false;

	constructor(private _userService: ParentUserService) {
	}

	ngOnInit(): void { ; }

	eventCalled() {
		this.isActive = !this.isActive;
	}

	addExpandClass(element: any) {
		if (element === this.showMenu) {
			this.showMenu = '0';
		} else {
			this.showMenu = element;
		}
	}
}
