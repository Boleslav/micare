import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CentreLogoDto } from '../../../api/index';

@Component({
  moduleId: module.id,
  selector: 'parent-signup-cmp',
  templateUrl: 'parent-signup.component.html'
})

export class ParentSignupComponent implements OnInit {

  private logo: string = null;
  constructor(private _router: Router, private _route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this._route
      .params
      .subscribe((params: Params) => {
        let centreName = params['centreName'];
        return this._route.data
          .subscribe((data: { userExists: boolean, centreLogo: CentreLogoDto }) => {
            if (data.userExists) {
              this._router.navigate(['/parent-login', centreName]);
            }

            if (data.centreLogo.imageBase64) {
              this.logo = data.centreLogo.imageBase64;
            }
          });
      });
  }
}
