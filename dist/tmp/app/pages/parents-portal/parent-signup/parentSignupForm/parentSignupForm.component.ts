import { Component, ViewChild, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { EmailValidator, MobileValidator, ComplexPasswordValidator, EqualPasswordsValidator } from '../../../../validators/index';
import { ModalDirective } from 'ng2-bootstrap';
import * as _ from 'lodash';

import { ParentSignupService } from '../parentSignupService/parentSignup.service';
import { ToasterService } from 'angular2-toaster';
import { Config, SpinningToast } from '../../../../shared/index';
import { RegisterParentModel } from '../../../../api/index';

@Component({
    moduleId: module.id,
    selector: 'parent-signup-form-cmp',
    templateUrl: 'parentSignupForm.component.html',
    styleUrls: ['./parentSignupForm.component.css']
})

export class ParentSignupFormComponent implements OnInit {

    @ViewChild('recaptcha') recaptcha: any;
    @ViewChild('staticModal') public staticModal: ModalDirective;

    public form: FormGroup;
    public firstName: AbstractControl;
    public lastName: AbstractControl;
    public email: AbstractControl;
    public mobilePhone: AbstractControl;
    public password: AbstractControl;
    public confirmPassword: AbstractControl;
    public passwords: FormGroup;

    public submitted: boolean = false;
    public userCreated: boolean = false;
    public captcha: boolean = false;
    public captchaToken: string = null;
    public captcha_key: string = Config.RECAPTCHA_UI_KEY;
    public captcha_enabled: boolean = this.convertStringToBool(Config.RecaptchaEnabled);

    private centreName: string = null;
    constructor(fb: FormBuilder,
        private _signupService: ParentSignupService,
        private _toasterService: ToasterService,
        private _router: Router,
        private _route: ActivatedRoute) {

        this.form = fb.group({
            'firstName': ['', Validators.compose([Validators.required, Validators.minLength(1)])],
            'lastName': ['', Validators.compose([Validators.required, Validators.minLength(1)])],
            'email': ['', Validators.compose([Validators.required, EmailValidator.validate])],
            'mobilePhone': ['', Validators.compose([Validators.required, MobileValidator.validate])],
            'passwords': fb.group({
                'password': ['', Validators.compose([Validators.required, Validators.minLength(8), ComplexPasswordValidator.validate])],
                'confirmPassword': ['', Validators.compose([Validators.required, Validators.minLength(8)])]
            }, { validator: EqualPasswordsValidator.validate('password', 'confirmPassword') })
        });

        this.firstName = this.form.controls['firstName'];
        this.lastName = this.form.controls['lastName'];
        this.email = this.form.controls['email'];
        this.mobilePhone = this.form.controls['mobilePhone'];
        this.passwords = <FormGroup>this.form.controls['passwords'];
        this.password = this.passwords.controls['password'];
        this.confirmPassword = this.passwords.controls['confirmPassword'];

        if (this.captcha_enabled === false) {
            //if its disabled let user click button
            this.captcha = true;
        }
    }

    ngOnInit(): void {
        this._route
            .params
            .subscribe((params: Params) => {
                this.centreName = params['centreName'];
            });
    }

    onSubmit(values: any) {

        this.submitted = true;

        let savingToast = this._toasterService.pop(new SpinningToast());

        var newParent = new RegisterParentModel();
        newParent.init({
            FirstName: values.firstName,
            LastName: values.lastName,
            Email: values.email,
            PrimaryPhone: values.mobilePhone,
            Password: values.passwords.password,
            ConfirmPassword: values.passwords.confirmPassword,
            CaptchaToken: this.captchaToken,
            CentreUniqueName: this.centreName
        });

        this._signupService
            .RegisterParent(newParent)
            .subscribe(() => {
                this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                this._toasterService.pop('success', 'User Created', '');
                this.userCreated = true;
            },
            err => {
                if (_.includes(err.response, 'in use')) {
                    this._toasterService.clear();
                    this.staticModal.show();
                } else {
                    this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                    this.submitted = false;
                    if (this.captcha_enabled)
                        this.recaptcha.reset();
                }
            });
    };

    handleCorrectCaptcha(event: string): void {
        this.captchaToken = event;
        this.captcha = true;
    };

    handleCaptchaExpired(): void {
        this.captchaToken = null;
    }

    convertStringToBool(input: string): boolean {
        var result = false;
        if (input === 'true') {
            result = true;
        }

        return result;
    }

    confirmRedirect(): void {
        this._router.navigate(['/parent-login', this.centreName]);
    }
}
