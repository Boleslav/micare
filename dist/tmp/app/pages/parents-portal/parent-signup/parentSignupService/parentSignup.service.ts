import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiClientService, RegisterParentModel } from '../../../../api/index';

@Injectable()
export class ParentSignupService {

    constructor(private _apiClient: ApiClientService) {
    }

    public RegisterParent(newParent: RegisterParentModel): Observable<void> {
        return this._apiClient
            .account_RegisterParent(newParent).catch((error: any) => {

                if (error.status === 403 || error.status === 417) {
                    return Observable.throw(error);
                } else {
                    return Observable.throw('Something went wrong, but we are not quite sure what happened..');
                }
            });
    }
}
