import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/index';

import {
    ParentSignupComponent, ParentSignupFormComponent,
    ParentSignupService
} from './index';

import { ReCaptchaModule } from 'angular2-recaptcha';


@NgModule({
    imports: [SharedModule, ReCaptchaModule],
    declarations: [ParentSignupComponent, ParentSignupFormComponent],
    providers: [ParentSignupService]
})

export class ParentSignupModule { }
