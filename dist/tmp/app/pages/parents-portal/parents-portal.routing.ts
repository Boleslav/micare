import { Routes } from '@angular/router';

import { CentreBrandingResolver, CentreLogoResolver } from './index';
import { ParentAuthGuardService } from '../../auth/index';
import { MixpanelGuard } from '../../shared/index';
import { ParentsPortalComponent } from './parents-portal.component';
import { ParentSignupRoutes } from './parent-signup/index';
import { ParentLoginRoutes } from './parent-login/index';
import { ParentLogoutRoutes } from './parent-logout/index';
import { CentreNotFoundRoutes } from './centre-not-found/index';
import { EnrolmentRoutes } from './enrolment/index';

export const ParentsPortalRoutes: Routes = [
    {
        path: 'parents-portal/:centreName',
        component: ParentsPortalComponent,
        resolve: {
            centreBrandingDto: CentreBrandingResolver,
            centreLogo: CentreLogoResolver
        },
        canActivate: [ParentAuthGuardService],
        canActivateChild: [MixpanelGuard],
        children: [
            { path: '', redirectTo: 'enrolment', pathMatch: 'full' },
            ...EnrolmentRoutes
        ]
    },
    ...ParentSignupRoutes,
    ...ParentLoginRoutes,
    ...ParentLogoutRoutes,
    ...CentreNotFoundRoutes
];
