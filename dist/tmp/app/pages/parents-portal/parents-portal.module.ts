import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/index';

import {
    CentreService, CentreLogoResolver,
    CentreBrandingResolver, CentreExistsResolver
} from './index';
import { TopNavComponent, SidebarComponent } from './shared/index';
import { ParentsPortalComponent } from './parents-portal.component';
import { ParentSignupModule } from './parent-signup/index';
import { ParentLoginModule } from './parent-login/index';
import { ParentLogoutModule } from './parent-logout/index';
import { EnrolmentModule } from './enrolment/index';
import { CentreNotFoundModule } from './centre-not-found/index';

@NgModule({
    imports: [
        SharedModule,
        ParentSignupModule,
        ParentLoginModule,
        ParentLogoutModule,
        EnrolmentModule,
        CentreNotFoundModule
    ],
    providers: [
        CentreService,
        CentreExistsResolver,
        CentreLogoResolver,
        CentreBrandingResolver
    ],
    declarations: [
        TopNavComponent,
        SidebarComponent,
        ParentsPortalComponent
    ]
})
export class ParentsPortalModule { }
