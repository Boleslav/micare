export * from './centre.component';
export * from './centreService/centre.service';
export * from './centreForm/centreForm.component';
export * from './centreBankDetailsForm/centre-bank-details.component';
export * from './centreBankDetailsForm/save-bank-details.component';
export * from './centreBankDetailsForm/display-bank-details.component';
export * from './centre.routes';
export * from './centre.module';
