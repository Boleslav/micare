import { Component, OnChanges, Input, SimpleChanges, SimpleChange } from '@angular/core';

import { CentreService } from '../index';

@Component({
    moduleId: module.id,
    selector: 'centre-bank-details-form-cmp',
    templateUrl: './centre-bank-details.component.html'
})
export class CentreBankDetailsFormComponent implements OnChanges {

    @Input() centreId: string;
    @Input() isApproved: boolean;
    private redactedPayoutMethod: string;
    private isEditMode: boolean;


    constructor(private _centreService: CentreService) {
    }

    ngOnChanges(changes: SimpleChanges) {
        let centreChange: SimpleChange = changes['centreId'];

        if (centreChange !== undefined
            && centreChange.currentValue !== undefined
            && centreChange.currentValue !== null
            && centreChange.currentValue !== ''
        ) {

            this.GetRedactedPayment(centreChange.currentValue);
        } else {
            this.setEditMode(true);
        }
    }

    bankAccountSaved(): void {
        this.GetRedactedPayment(this.centreId);
    }

    setEditMode(event: any): void {
        this.isEditMode = event;
    }

    GetRedactedPayment(centreId: string) {
        this._centreService
            .GetRedactedPaymentMethod(centreId)
            .subscribe(res => {
                this.redactedPayoutMethod = res;
                this.setEditMode(this.redactedPayoutMethod === undefined || this.redactedPayoutMethod === null);
            });
    }
}
