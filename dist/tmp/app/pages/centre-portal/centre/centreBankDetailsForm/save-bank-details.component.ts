import {
    Component, Input, OnInit, OnChanges,
    OnDestroy, SimpleChanges, SimpleChange,
    Output, EventEmitter
} from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { ToasterService } from 'angular2-toaster';

import { CentreService } from '../index';
import { CentreBankAccountDto } from '../../../../api/index';
import { SpinningToast } from '../../../../shared/index';

@Component({
    moduleId: module.id,
    selector: 'save-bank-details-cmp',
    templateUrl: './save-bank-details.component.html'
})
export class SaveBankDetailsComponent implements OnInit, OnDestroy, OnChanges {

    @Output() onEdit: EventEmitter<any> = new EventEmitter();
    @Output() onSave: EventEmitter<any> = new EventEmitter();
    @Input() centreId: string;
    @Input() isApproved: boolean;
    private intervalId: NodeJS.Timer = null;
    private isReadyToSetup: boolean;
    private submitted: boolean = false;

    private form: FormGroup;
    private bankBsbControl: AbstractControl;
    private bankAcnControl: AbstractControl;

    constructor(fb: FormBuilder,
        private _centreService: CentreService,
        private _toasterService: ToasterService) {
        this.form = fb.group({
            'bankBsbControl': ['', Validators.compose([Validators.required, Validators.pattern('\\d([\\s\\-]?\\d){5}')])],
            'bankAcnControl': ['', Validators.compose([Validators.required, Validators.pattern('\\d+')])]
        });

        this.bankBsbControl = this.form.controls['bankBsbControl'];
        this.bankAcnControl = this.form.controls['bankAcnControl'];
    }

    ngOnInit(): void {

        this._centreService
            .GetSetupBankAccount$()
            .subscribe((res) => {
                this.isReadyToSetup = res;

                if (this.isReadyToSetup) {
                    clearInterval(this.intervalId);
                    this.enableControls();
                } else {
                    this.disableControls();
                }
            });

        this._centreService.CanSetupBankAccount$(this.centreId);
    }

    ngOnChanges(changes: SimpleChanges) {

        let centreChange: SimpleChange = changes['centreId'];
        let approvalChange: SimpleChange = changes['isApproved'];

        if (centreChange !== undefined && centreChange.currentValue === undefined) {
            this.disableControls();
        }

        if (approvalChange !== undefined
            && approvalChange.currentValue !== undefined
            && approvalChange.currentValue === false) {
            this.disableControls();
        }

        if (centreChange !== undefined && centreChange.currentValue !== undefined
            && approvalChange !== undefined && approvalChange.currentValue === true) {
            this._centreService.CanSetupBankAccount$(centreChange.currentValue);
            if (this.intervalId === null) {
                this.intervalId = setInterval(() =>
                    this._centreService.CanSetupBankAccount$(centreChange.currentValue)
                    , 10000);
            }
        }
    }

    ngOnDestroy(): void {
        clearInterval(this.intervalId);
    }

    onSubmit(): void {

        this.submitted = true;

        let savingToast = this._toasterService.pop(new SpinningToast());

        let bankAccount = new CentreBankAccountDto();
        bankAccount.init({
            'CentreId': this.centreId,
            'Bsb': this.bankBsbControl.value,
            'Acn': this.bankAcnControl.value
        });
        this._centreService
            .SaveBankAccountDetails$(bankAccount)
            .subscribe(() => {
                this._toasterService.clear();
                this._toasterService.pop('success', 'Bank Details Saved');
                this.submitted = false;
                this.onSave.emit(null);
            },
            (err) => {
                this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                this.submitted = false;
            });
    }

    setEditMode(): void {
        this.onEdit.emit(false);
    }

    private enableControls(): void {
        this.bankAcnControl.enable();
        this.bankBsbControl.enable();
    }

    private disableControls(): void {
        this.bankBsbControl.disable();
        this.bankAcnControl.disable();
    }
}
