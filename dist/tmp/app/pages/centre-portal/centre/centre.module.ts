import { NgModule } from '@angular/core';

import { SharedModule } from '../../../shared/index';
import {
    CentreComponent, CentreFormComponent, CentreBankDetailsFormComponent,
    SaveBankDetailsComponent, DisplayBankDetailsComponent, CentreService
} from './index';

@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [
        CentreComponent,
        CentreFormComponent,
        CentreBankDetailsFormComponent,
        SaveBankDetailsComponent,
        DisplayBankDetailsComponent
    ],
    providers: [CentreService],
    exports: [
        CentreComponent,
        CentreFormComponent,
        CentreBankDetailsFormComponent,
        SaveBankDetailsComponent,
        DisplayBankDetailsComponent
    ]
})

export class CentreModule { }
