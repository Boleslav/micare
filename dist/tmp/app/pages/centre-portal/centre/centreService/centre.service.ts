import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

import { ApiClientService, CreateUpdateCentreDto, CentreDto, CentreBankAccountDto } from '../../../../api/index';
import {CompanyFacilityViewModel} from "../../../../api/client.service";

@Injectable()
export class CentreService {

    private setupBankAccountSubject: Subject<string>;
    constructor(private _apiService: ApiClientService) {
        this.setupBankAccountSubject = new Subject<string>();
    }

    public GetCentre$(centreId: string): Observable<CentreDto> {
        return this._apiService.centre_GetCentreAsync(centreId);
    }

    public CreateCentre$(createCentreDto: CreateUpdateCentreDto): Observable<CreateUpdateCentreDto> {

        return this._apiService.centre_CreateUpdateCentreAsync(createCentreDto);
    }

    public getTimeZones$(): Observable<string[]> {
        return this._apiService.centre_GetTimeZones();
    }

    public SaveBankAccountDetails$(bankAccount: CentreBankAccountDto): Observable<void> {
        return this._apiService.centre_PostBankAccountAsync(bankAccount);
    }

    public CanSetupBankAccount$(centreId: string): void {
        this.setupBankAccountSubject.next(centreId);
    }

    public GetSetupBankAccount$(): Observable<boolean> {
        return this.setupBankAccountSubject
            .switchMap((val: string) => {
                if (val === undefined || val === null || val === '')
                    return Observable.of(false);
                return this._apiService.centre_CanSetupBankAccount(val);
            });
    }

    public GetRedactedPaymentMethod(centreId: string): Observable<string> {
        return this._apiService.centre_GetRedactedBankAccountNumber(centreId);
    }

    public GetCompanyCentres(companyId: string): Observable<CompanyFacilityViewModel[]> {
        return this._apiService.centre_GetCentresByCompany(companyId);
    }

    public GetUnassignedCentres(search: string): Observable<CompanyFacilityViewModel[]> {
        return this._apiService.centre_GetUnassignedCentres(search);
    }

    public SetActiveCompanyCentre(companyId: string, facilityId: number, isActive: boolean): Observable<void> {
      var vm = new CompanyFacilityViewModel();
      vm.companyId = companyId;
      vm.facilityId = facilityId;
      vm.isActive = isActive;
      return this._apiService.centre_SetCompanyFacilityActiveAsync(vm);
    }
}
