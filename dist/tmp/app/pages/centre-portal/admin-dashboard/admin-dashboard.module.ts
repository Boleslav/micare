import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/index';
import { AdminDashboardComponent, AdminDashboardService } from './index';

@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [AdminDashboardComponent],
    providers: [AdminDashboardService],
    exports: [AdminDashboardComponent]
})

export class AdminDashboardModule { }
