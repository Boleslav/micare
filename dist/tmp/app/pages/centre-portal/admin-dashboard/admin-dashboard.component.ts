import { Component, OnInit } from '@angular/core';

import { AdminDashboardService } from './adminDashboardService/adminDashboard.service';
import { CentreUserService } from '../../../core/index';

@Component({
    moduleId: module.id,
    selector: 'admin-dashboard-cmp',
    templateUrl: 'admin-dashboard.component.html'
})

export class AdminDashboardComponent implements OnInit {

    private currentYear = new Date().getFullYear();

    private totalCentres: number = null;
    private totalCentreAwaitingApproval: number = null;
    private totalIncome: number = null;
    private totalMiSwaps: number = null;

    private daysForSale: Array<number[]> = null;
    private daysSold: Array<number[]> = null;

    private totalCentresOverTime: Array<number[]> = null;
    private totalStaffOverTime: Array<number[]> = null;
    private totalParentsOverTime: Array<number[]> = null;

    constructor(private _userService: CentreUserService,
        private _adminDashboardService: AdminDashboardService) {
    }

    ngOnInit() {
        this._adminDashboardService
            .getTotalCentresCount$()
            .subscribe(res => this.totalCentres = res);

        this._adminDashboardService
            .getTotalCentresAwaitingApprovalCount$()
            .subscribe(res => this.totalCentreAwaitingApproval = res);

        this._adminDashboardService
            .getTotalIncome$()
            .subscribe(res => this.totalIncome = res);

        this._adminDashboardService
            .getTotalCompletedSwaps$()
            .subscribe(res => this.totalMiSwaps = res);


        this._adminDashboardService
            .getAdminDashboardData$()
            .subscribe(res => {
                this.daysForSale = this.mapGraphData(res.daysForSale);
                this.daysSold = this.mapGraphData(res.daysSold);
                this.totalCentresOverTime = this.mapGraphData(res.totalCentresOverTime);
                this.totalStaffOverTime = this.mapGraphData(res.totalStaffOverTime);
                this.totalParentsOverTime = this.mapGraphData(res.totalParentsOverTime);
                this.initDaySalesChart();
                this.initCentreSummaryChart();
            });
    }

    private mapGraphData(days: { [key: string]: number; }) {
        let arr = new Array<number[]>();
        for (var key in days) {
            if (days.hasOwnProperty(key)) {
                var day: number[] = [new Date(key).getTime(), days[key]];
                arr.push(day);
            }
        }
        return arr;
    }

    private initDaySalesChart(): void {
        var miSwaps: any = $('#miSwaps-table');
        miSwaps.highcharts({
            chart: {
                type: 'spline'
            },
            title: {
                text: 'MiCare ' + this.currentYear
            },
            subtitle: {
                text: 'Number of MiCare days'
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: { // don't display the dummy year
                    month: '%e. %b',
                    year: '%b'
                },
                title: {
                    text: 'Date'
                }
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Total'
                },
                min: 0
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x:%e. %b}: {point.y}'
            },

            plotOptions: {
                spline: {
                    marker: {
                        enabled: true
                    }
                }
            },

            series: [{
                name: 'To Give',
                data: this.daysForSale
            }, {
                name: 'Given',
                data: this.daysSold
            }
            ]
        });
    }

    private initCentreSummaryChart(): void {
        var miSwaps: any = $('#miSwaps-summary-table');
        miSwaps.highcharts({
            chart: {
                type: 'spline'
            },
            title: {
                text: 'MiCare Summary ' + this.currentYear
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: { // don't display the dummy year
                    month: '%e. %b',
                    year: '%b'
                },
                title: {
                    text: 'Date'
                }
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Total'
                },
                min: 0
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x:%e. %b}: {point.y}'
            },

            plotOptions: {
                spline: {
                    marker: {
                        enabled: true
                    }
                }
            },

            series: [{
                name: 'MiCare Centres',
                data: this.totalCentresOverTime
            },
            {
                name: 'MiCare Staff',
                data: this.totalStaffOverTime
            },
            {
                name: 'MiCare Parents',
                data: this.totalParentsOverTime
            }
            ]
        });
    }
}
