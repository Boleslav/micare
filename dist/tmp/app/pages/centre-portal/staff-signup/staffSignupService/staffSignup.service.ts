import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiClientService, AcceptStaffInviteDto } from '../../../../api/index';

@Injectable()
export class StaffSignupService {

    constructor(private _apiClient: ApiClientService) {
    }

    public GetStaffMemberInvite$(token: string): Observable<string> {
        return this._apiClient.account_GetStaffMemberInvite(token);
    }

    public AcceptStaffMemberInvite$(acceptStaffInviteDto: AcceptStaffInviteDto): Observable<void> {
        return this._apiClient.account_AcceptStaffMemberInvite(acceptStaffInviteDto);
    }
}
