import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/index';
import { ShutdownDaysComponent, ShutdownDaysTableComponent, ShutdownDaysService } from './index';

@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [ShutdownDaysComponent, ShutdownDaysTableComponent],
    providers: [ShutdownDaysService],
    exports: [ShutdownDaysComponent, ShutdownDaysTableComponent]
})

export class ShutdownDaysModule { }
