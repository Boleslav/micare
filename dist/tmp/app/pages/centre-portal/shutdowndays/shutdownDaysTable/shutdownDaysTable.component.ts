import { Component, OnInit } from '@angular/core';

import { ToasterService } from 'angular2-toaster';
import { ShutdownDaysService } from '../shutdownDaysService/shutdownDays.service';
import { CentreUserService } from '../../../../core/index';
import { SpinningToast } from '../../../../shared/index';
import { ShutdownDayDto, CreateUpdateShutdownDayDto } from '../../../../api/index';
import * as moment from 'moment/moment';

@Component({
    moduleId: module.id,
    selector: 'shutdowndays-table-cmp',
    templateUrl: './shutdownDaysTable.component.html'
})
export class ShutdownDaysTableComponent implements OnInit {

    public minDate: Date = new Date();

    private shutdownDays: ShutdownDayDto[];
    private addMode: boolean = false;
    private disableSave: boolean = false;
    private disableAddEdit: boolean = false;
    private editRef?: number = null;

    // Stores values before edit so we can rollback on error
    private preEditedDescription: string;
    private preEditedDate: Date;

    constructor(private _ShutdownDaysService: ShutdownDaysService,
        private _userService: CentreUserService,
        private _toasterService: ToasterService) {
    }

    ngOnInit(): void {
        this._ShutdownDaysService
            .GetCentreShutdownDays$(this._userService.SelectedCentre.id)
            .subscribe(shutdownDaysRes => {
                this.shutdownDays = shutdownDaysRes;
            });
    }

    addShutdownDay() {
        // Set add mode and row
        this.addMode = true;
        this.disableAddEdit = true;

        // New up shutdown day
        var newShutdownDay = new ShutdownDayDto();
        newShutdownDay.centreId = this._userService.SelectedCentre.id;
        this.shutdownDays.push(newShutdownDay);

        // set the editing index to the last row in the table
        this.editRef = this.shutdownDays.length - 1;
    }

    formatIsoDate(date: Date) : string {
        return moment(date).utc().format('DD/MM/YYYY');
    }

    updateShutdownDay(shutdownDay: CreateUpdateShutdownDayDto) {
        if (shutdownDay.date instanceof Date) {
            // convert to utc to fix issue where date is always yesterday.
            //var utcDate = new Date(Date.UTC(shutdownDay.date.getFullYear(),
                //shutdownDay.date.getMonth(), shutdownDay.date.getDate(), 0, 0, 0, 0));
            //shutdownDay.date = utcDate;
            //shutdownDay.date =
            //shutdownDay.date = moment(shutdownDay.date).utc().toString();
        }
        let savingToast = this._toasterService.pop(new SpinningToast());
        this.disableSave = true;
        this._ShutdownDaysService
            .CreateUpdateShutdownDay$(shutdownDay)
            .subscribe(
            res => {
                // reset edit indexs and preEditedShutdownDay
                this.resetEditingProperties();
                shutdownDay.id = res.id;
                this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                this._toasterService.popAsync('success', 'Saved', '');
            },
            (err) => this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId));
        this.disableSave = false;
    }

    deleteShutdownDay(shutdownDay: ShutdownDayDto) {
        if (window.confirm('Are you sure you want to remove the shutdown day?')) {

            let savingToast = this._toasterService.pop(new SpinningToast());
            this._ShutdownDaysService
                .DeleteShutdownDay$(shutdownDay.centreId, shutdownDay.id)
                .subscribe(res => {
                    var index = this.shutdownDays.indexOf(shutdownDay);
                    this.shutdownDays.splice(index, 1);
                    this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                    this._toasterService.popAsync('success', 'Deleted', '');
                },
                (err) => this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId));
        }
    }

    editShutdownDay(shutdownDayIdx: number, shutdownDay: ShutdownDayDto) {
        this.editRef = shutdownDayIdx;
        this.disableAddEdit = true;

        // store pre-edited shutdown day
        this.preEditedDescription = shutdownDay.description;
        this.preEditedDate = shutdownDay.date;
    }

    cancelEdit(shutdownDay: ShutdownDayDto) {
        if (this.addMode) {
            var index = this.shutdownDays.indexOf(shutdownDay);
            this.shutdownDays.splice(index, 1);
        } else {
            // Editing existing shutdown day, rollback shutdown day edit
            shutdownDay.description = this.preEditedDescription;
            shutdownDay.date = this.preEditedDate;
        }
        this.resetEditingProperties();
    }

    editingCurrentRow(shutdownDayIdx: number): boolean {
        if (shutdownDayIdx === this.editRef) {
            return true;
        }
        return false;
    }

    resetEditingProperties() {
        // Reset editing properties
        this.editRef = null;
        this.preEditedDescription = null;
        this.preEditedDate = null;
        this.addMode = false;
        this.disableAddEdit = false;
        this.disableSave = false;
    }
}
