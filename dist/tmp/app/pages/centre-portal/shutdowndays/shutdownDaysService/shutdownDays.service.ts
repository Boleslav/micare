import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiClientService, CreateUpdateShutdownDayDto, ShutdownDayDto } from '../../../../api/index';

@Injectable()
export class ShutdownDaysService {

    constructor(private _apiService: ApiClientService) {

    }

    public CreateUpdateShutdownDay$(shutdownDayDto: CreateUpdateShutdownDayDto): Observable<ShutdownDayDto> {

        return this._apiService.shutdownDay_CreateUpdateShutdownDay(shutdownDayDto);
    }

    public GetCentreShutdownDays$(centreId: string): Observable<ShutdownDayDto[]> {
        return this._apiService.shutdownDay_GetShutdownDays(centreId);
    }

    public DeleteShutdownDay$(centreId: string, shutdownDayId: string): Observable<void> {
        return this._apiService.shutdownDay_DeleteShutdownDay(centreId, shutdownDayId);
    }
}
