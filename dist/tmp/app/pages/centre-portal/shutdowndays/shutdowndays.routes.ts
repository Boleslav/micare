import { Route } from '@angular/router';
import { NavigationGuardService } from '../../../shared/index';
import { ShutdownDaysComponent } from './index';

export const ShutdownDaysRoutes: Route[] = [
	{
		path: 'shutdowndays',
		component: ShutdownDaysComponent,
		canActivate: [NavigationGuardService]
	}
];
