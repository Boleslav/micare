import { Component, ViewEncapsulation } from '@angular/core';

import { ToasterService } from 'angular2-toaster';
import { CentreUserService } from '../../core/index';
import { Config, IntercomService } from '../../shared/index';

@Component({
    moduleId: module.id,
    encapsulation: ViewEncapsulation.None,
    selector: 'centre-portal-cmp',
    templateUrl: 'centre-portal.component.html',
    styleUrls: ['centre-portal.component.css']
})
export class CentrePortalComponent {

    constructor(private _userService: CentreUserService,
        private _toasterService: ToasterService,
        private _intercomService: IntercomService) {

        _intercomService.initUserLoggedIn(Config.INTERCOM_APP_ID, this._userService.Email);
    }
}
