import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiClientService, CentreDashboardGraphDto } from '../../../../api/index';

@Injectable()
export class CentreDashboardService {

    constructor(private _apiService: ApiClientService) {

    }

    public getCentrePendingApprovalsCount(centreId: string): Observable<number> {
        return this._apiService.centreDashboard_GetCentrePendingApprovalsCount(centreId);
    }

    public getCentreAvailableDaysCount(centreId: string): Observable<number> {
        return this._apiService.centreDashboard_GetCentreAvailableDaysCount(centreId);
    }

    public getCentreParentCount(centreId: string): Observable<number> {
        return this._apiService.centreDashboard_GetParentCountAsync(centreId);
    }

    public getCentreTotalIncome(centreId: string): Observable<number> {
        return this._apiService.centreDashboard_GetCentreTotalIncome(centreId);
    }

    public getCentreDashboardData$(centreId: string): Observable<CentreDashboardGraphDto> {
        return this._apiService.centreDashboard_GetGraphDashboardData(centreId);
    }
}
