import { Route } from '@angular/router';
import { CentreDashboardComponent } from './index';
import { NavigationGuardService } from '../../../shared/index';

export const CentreDashboardRoutes: Route[] = [
	{
		path: 'centre-dashboard',
		component: CentreDashboardComponent,
		canActivate: [NavigationGuardService]
	},
];
