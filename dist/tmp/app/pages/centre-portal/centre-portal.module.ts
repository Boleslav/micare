import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/index';
import { TopNavComponent, SidebarComponent } from './shared/index';
import { CentrePortalComponent } from './index';
import { CentreDashboardModule } from './centre-dashboard/index';
import { RoomsModule } from './rooms/rooms.module';
import { StaffModule } from './staff/index';
import { CompaniesModule } from './companies/index';
import { CentreModule } from './centre/index';
import { AdminDashboardModule } from './admin-dashboard/index';
import { AdminApprovalsModule } from './admin-approvals/index';
import { BookingsModule } from './bookings/index';
import { ShutdownDaysModule } from './shutdowndays/index';
import { SwapsModule } from './swaps/index';
import { VacanciesModule } from './vacancies/index';
import { EnrolmentModule } from './enrolment/index';
import { LoginModule } from './login/index';
import { LogoutModule } from './logout/index';
import { SignupModule } from './signup/index';
import { StaffSignupModule } from './staff-signup/index';
import { EnrolmentApprovalsModule } from './enrolment-approvals/index';
import { FamilyEnrolmentFormModule } from './family-enrolment-form/index';

@NgModule({
    imports: [
        AdminApprovalsModule,
        AdminDashboardModule,
        CentreDashboardModule,
        CompaniesModule,
        CentreModule,
        StaffModule,
        RoomsModule,
        BookingsModule,
        ShutdownDaysModule,
        SwapsModule,
        VacanciesModule,
        EnrolmentModule,
        SharedModule,
        LoginModule,
        LogoutModule,
        SignupModule,
        StaffSignupModule,
        EnrolmentApprovalsModule,
        FamilyEnrolmentFormModule
    ],
    declarations: [
        TopNavComponent,
        SidebarComponent,
        CentrePortalComponent
    ]
})
export class CentrePortalModule { }
