import {Component} from '@angular/core';

@Component({
	moduleId: module.id,
    selector: 'admin-approvals-cmp',
    templateUrl: './adminApprovals.component.html'
})

export class AdminApprovalsComponent {}
