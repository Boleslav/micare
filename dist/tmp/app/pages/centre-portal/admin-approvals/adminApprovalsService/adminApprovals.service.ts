import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

import { ApiClientService, CentreDto, UpdateCentreApprovalStatusDto } from '../../../../api/index';

@Injectable()
export class AdminApprovalsService {

    private searchTermStream = new Subject<{ term: string, status: number }>();

    constructor(private _apiService: ApiClientService) {
    }

    public searchCentre(term: string, status: number) {
        this.searchTermStream.next({ term: term, status: status });
    }

    public getCentres$(): Observable<CentreDto[]> {
        return this.searchTermStream
            .debounceTime(300)
            .distinctUntilChanged()
            .switchMap((search: { term: string, status: number }) => this._apiService.sysAdmin_GetCentres(search.term, search.status));
    }

    public setCentreApprovalStatus$(updateApprovalStatusDto: UpdateCentreApprovalStatusDto): Observable<void> {
        return this._apiService.sysAdmin_SetCentreApprovalStatus(updateApprovalStatusDto);
    }
}
