import { Component, OnInit } from '@angular/core';

import { AdminApprovalsFilter } from './adminApprovalsFilter.model';
import { AdminApprovalsService } from '../adminApprovalsService/adminApprovals.service';
import {
    CentreDto, UpdateCentreApprovalStatusDto,
    UpdateCentreApprovalStatusDtoApprovalStatusType, Status
} from '../../../../api/index';

import { ToasterService } from 'angular2-toaster';
import { SpinningToast } from '../../../../shared/index';

@Component({
    moduleId: module.id,
    selector: 'admin-approvals-table-cmp',
    templateUrl: './adminApprovalsTable.component.html',
    styleUrls: ['./adminApprovalsTable.component.css']
})

export class AdminApprovalsTableComponent implements OnInit {

    readonly DefaultApprovalStatus: string = 'Show All';

    centres: CentreDto[] = null;
    approvalStatuses: any = AdminApprovalsFilter;
    approvalStatusFilter: any = {
        filterText: '',
        filterId: 0
    };

    private searchTerm: string = '';

    constructor(private _adminAdpprovalsService: AdminApprovalsService,
        private _toasterService: ToasterService) {
        this.setApprovalStatus(this.DefaultApprovalStatus);
    }

    ngOnInit(): void {

        this._adminAdpprovalsService
            .getCentres$()
            .subscribe(res => {
                this.centres = res;
                this._toasterService.clear();
            });
        this._adminAdpprovalsService.searchCentre(this.searchTerm, <Status>{});
    }

    keys(): Array<string> {
        return Object.keys(this.approvalStatuses);
    }

    setApprovalStatus(status: string): void {
        this.approvalStatusFilter.filterText = status;
        this.approvalStatusFilter.filterId = this.approvalStatuses[status];
    }

    filterByApprovalStatus(status: string): void {
        this.setApprovalStatus(status);
        this._toasterService.clear();
        this._toasterService.pop(new SpinningToast('Loading'));
        // There is currently no way in nswag to pass in optional parameters.
        // Planned solution will be delivered in an unknown time:
        // https://github.com/NSwag/NSwag/issues/608
        this._adminAdpprovalsService.searchCentre(
            this.searchTerm, this.approvalStatusFilter.filterId === 4 ? <Status>{} : this.approvalStatusFilter.filterId);
    }

    getDefaultApprovalStatusId(): number {
        return this.approvalStatuses[this.DefaultApprovalStatus];
    }

    setCentreApprovalStatus(centre: CentreDto, approvalStatus: string): void {

        let savingToast = this._toasterService.pop(new SpinningToast());

        let updateApprovalStatus = new UpdateCentreApprovalStatusDto();
        updateApprovalStatus.init({
            CentreId: centre.id
        });

        let approvalStatusId: number = this.approvalStatuses[approvalStatus];

        centre.approvalStatus = approvalStatusId;

        switch (approvalStatusId) {
            case 0:
                updateApprovalStatus.approvalStatusType = UpdateCentreApprovalStatusDtoApprovalStatusType.AwaitingApproval;
                break;

            case 1:
                updateApprovalStatus.approvalStatusType = UpdateCentreApprovalStatusDtoApprovalStatusType.DeniedApproval;
                break;

            case 2:
                updateApprovalStatus.approvalStatusType = UpdateCentreApprovalStatusDtoApprovalStatusType.Approved;
                break;

            case 3:
                updateApprovalStatus.approvalStatusType = UpdateCentreApprovalStatusDtoApprovalStatusType.UnClaimed;
                break;

            default:
                updateApprovalStatus.approvalStatusType = UpdateCentreApprovalStatusDtoApprovalStatusType.AwaitingApproval;
                break;
        }

        this._adminAdpprovalsService
            .setCentreApprovalStatus$(updateApprovalStatus)
            .subscribe(
            res => {
                this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                this._toasterService.popAsync('success', 'Saved', '');
            },
            (err) => this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId));
    }

    searchCentre(event: any): void {
        this._toasterService.clear();
        this._toasterService.pop(new SpinningToast('Loading'));
        // There is currently no way in nswag to pass in optional parameters.
        // Planned solution will be delivered in an unknown time:
        // https://github.com/NSwag/NSwag/issues/608
        this._adminAdpprovalsService.searchCentre(
            event.target.value, this.approvalStatusFilter.filterId === 4 ? <Status>{} : this.approvalStatusFilter.filterId);
    }
}
