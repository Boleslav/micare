import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs';
import { CompleteEnrolmentFormViewModel } from '../../../api/index';
import { CentreUserService } from '../../../core/index';
import { FamilyEnrolmentService } from './index';

@Component({
	moduleId: module.id,
	selector: 'family-enrolment-form-cmp',
	templateUrl: 'family-enrolment-form.component.html',
	styleUrls: ['family-enrolment-form.component.css']
})

export class FamilyEnrolmentFormComponent implements OnInit {

	private centreName: string = null;
	private logo: string = null;
	private completeEnrolmentForm: CompleteEnrolmentFormViewModel = null;
	constructor(
		private _enrolmentService: FamilyEnrolmentService,
		private _userService: CentreUserService,
		private _route: ActivatedRoute) { ; }

	ngOnInit(): void {

		this._route
			.params
			.flatMap((params: Params) => {
				let parentId: string = params['parentId'];
				return Observable.forkJoin(
					this._enrolmentService.getEnrolmentForm$(parentId, this._userService.SelectedCentre.id),
					this._enrolmentService.getTitles$(),
					this._enrolmentService.getGenders$(),
					this._enrolmentService.getCareTypes$(),
					this._enrolmentService.getRelationships$());
			})
			.subscribe(res => {
				this.completeEnrolmentForm = res[0];
			});

		this._userService
			.getCentreLogo$(this._userService.SelectedCentre.id)
			.subscribe(res => {
				this.logo = res.imageBase64;
			});

		this.centreName = this._userService.SelectedCentre.name;
	}

	print(): void {
		window.print();
	}
}
