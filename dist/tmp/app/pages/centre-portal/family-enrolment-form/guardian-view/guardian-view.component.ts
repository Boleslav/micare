import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { GuardianDto } from '../../../../api/index';
import { FamilyEnrolmentService } from '../index';

@Component({
    moduleId: module.id,
    selector: 'guardian-view-cmp',
    templateUrl: 'guardian-view.component.html'
})

export class GuardianViewComponent implements OnInit {

    @Input('guardian') guardian: GuardianDto;
    private titles: { [key: string]: string } = {};
    private genders: { [key: string]: string } = {};
    private relationships: { [key: string]: string } = {};

    constructor(private _enrolmentService: FamilyEnrolmentService) { ; }

    ngOnInit(): void {
        Observable.forkJoin(
            this._enrolmentService.getTitles$(),
            this._enrolmentService.getGenders$(),
            this._enrolmentService.getRelationships$())
            .subscribe(res => {
                this.titles = res[0];
                this.genders = res[1];
                this.relationships = res[2];
            });
    }
}
