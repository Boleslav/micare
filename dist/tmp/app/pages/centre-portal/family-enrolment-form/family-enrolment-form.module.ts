import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/index';

import { FamilyEnrolmentFormComponent, FamilyEnrolmentService } from './index';
import { ParentViewComponent } from './parent-view/index';
import {
    ChildViewComponent, ChildDetailsViewComponent,
    ChildDesiredDaysViewComponent,
    ChildHealthInfoViewComponent, ChildCustodyInfoViewComponent
} from './child-view/index';
import { GuardianViewComponent } from './guardian-view/index';
import { ContactViewComponent } from './contact-view/index';
import { ConsentViewComponent } from './consent-view/index';

@NgModule({
    imports: [SharedModule],
    declarations: [
        FamilyEnrolmentFormComponent,
        ParentViewComponent,
        ChildViewComponent,
        ChildDetailsViewComponent,
        ChildDesiredDaysViewComponent,
        ChildHealthInfoViewComponent,
        ChildCustodyInfoViewComponent,
        GuardianViewComponent,
        ContactViewComponent,
        ConsentViewComponent
    ],
    providers: [FamilyEnrolmentService]
})

export class FamilyEnrolmentFormModule { }
