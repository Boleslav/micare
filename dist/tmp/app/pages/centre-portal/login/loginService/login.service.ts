import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { AccessTokenService } from '../../../../core/index';
import { Config } from '../../../../shared/index';
import { ApiClientService } from '../../../../api/index';

@Injectable()
export class LoginService {

    constructor(
        private _authenticationService: AccessTokenService,
        private _apiService: ApiClientService) {
    }

    public login(username: string, password: string): Observable<void> {
        return this._apiService
            .token('password', username, password, Config.CLIENT_ID)
            .map(res => {
                this._authenticationService.saveAccessToken(res.value, false);
            });

    }
}
