import { Component, OnInit } from '@angular/core';
import { Config, IntercomService } from '../../../shared/index';

@Component({
	moduleId: module.id,
	selector: 'login-cmp',
	templateUrl: 'login.component.html',
	styleUrls: ['login.component.css']
})

export class LoginComponent implements OnInit {

	constructor(private _intercomService: IntercomService) {}

	ngOnInit(): void {

		this._intercomService
			.initUserNotLoggedIn(Config.INTERCOM_APP_ID);
	}
}
