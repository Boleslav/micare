import { Route } from '@angular/router';
import { StaffComponent } from './index';
import { NavigationGuardService } from '../../../shared/index';

export const StaffRoutes: Route[] = [
	{
		path: 'staff',
		component: StaffComponent,
		canActivate: [NavigationGuardService]
	},
];
