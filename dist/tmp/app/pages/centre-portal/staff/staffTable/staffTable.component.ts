import { Component, OnInit } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { StaffService } from '../staffService/staff.service';
import { CentreUserService } from '../../../../core/index';
import { SpinningToast } from '../../../../shared/index';
import { CentreStaffMemberDto } from '../../../../api/index';

@Component({
    moduleId: module.id,
    selector: 'staff-table-cmp',
    templateUrl: './staffTable.component.html'
})

export class StaffTableComponent implements OnInit {

    private staffMembers: CentreStaffMemberDto[];
    private email: string;

    // Flag to prevent double submit
    private disableButtons = false;

    // Flag to enter Add Staff Mode
    private addMode = false;

    constructor(private _staffService: StaffService,
        private _userService: CentreUserService,
        private _toasterService: ToasterService) {
    }

    ngOnInit(): void {
        this._staffService
            .GetCurrentStaffMembersForCentre$(this._userService.SelectedCentre.id)
            .subscribe(staffRes => {
                this.staffMembers = staffRes;
            });
    }

    setAddMode(flag: boolean) {
        this.addMode = flag;
    }

    inviteStaffMember(email: string) {
        console.log(email);
        let savingToast = this._toasterService.pop(new SpinningToast());
        this.disableButtons = true;
        this._staffService
            .InviteStaffMember$(this._userService.SelectedCentre.id, email)
            .subscribe(
            res => {
                // Get latest staff members
                this._staffService
                    .GetCurrentStaffMembersForCentre$(this._userService.SelectedCentre.id)
                    .subscribe(staffRes => {
                        this.staffMembers = staffRes;
                    });
                this.addMode = false;
                this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                this._toasterService.popAsync('success', 'Invite Sent', '');
            },
            (err) => this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId));
        this.disableButtons = false;
        this.email = '';
    }

    removeStaffMember(centreId: string, staffMemberId: string) {
        if (window.confirm('Are you sure you want to remove the staff member from the centre. Press Ok to continue.')) {
            let savingToast = this._toasterService.pop(new SpinningToast());
            this.disableButtons = true;
            this._staffService
                .RemoveCentreStaffMember$(centreId, staffMemberId)
                .subscribe(
                res => {
                    // Get latest staff members
                    this._staffService
                        .GetCurrentStaffMembersForCentre$(this._userService.SelectedCentre.id)
                        .subscribe(staffRes => {
                            this.staffMembers = staffRes;
                        });
                    this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                    this._toasterService.popAsync('success', 'Removed', '');
                },
                (err) => this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId));
            this.disableButtons = false;
        }
    }

    staffStatusString(status: number) {
        switch (status) {
            case 0:
                return 'Invited';
            case 1:
                return 'Active';
            case 2:
                return 'Inactive';
            case 3:
                return 'Locked Out';
            case 4:
                return 'Password Expired';
            default:
                return 'Unknown';
        }
    }

    staffStatusStyle(status: number) {
        switch (status) {
            case 0: // Invited
                return 'btn btn-sm btn-info btn-block';
            case 1: // Active
                return 'btn btn-sm btn-success btn-block';
            case 2: // Inactive
                return 'btn btn-sm btn-warning btn-block';
            case 3: // LockedOut
                return 'btn btn-sm btn-danger btn-block';
            case 4: // Password Expired
                return 'btn btn-sm btn-danger btn-block';
            default:    // Unkown
                return 'btn btn-sm btn-seconday btn-block';
        }
    }
}
