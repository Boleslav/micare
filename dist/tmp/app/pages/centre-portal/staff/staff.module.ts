import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/index';

import { StaffComponent, StaffTableComponent, StaffService } from './index';

@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [StaffComponent, StaffTableComponent],
    providers: [StaffService],
    exports: [StaffComponent, StaffTableComponent]
})

export class StaffModule { }
