import { Component, OnInit } from '@angular/core';

import { DisplayAgeComponent, SelectAgeComponent, DisplayCareTypeComponent, RoomsService } from '../index';
import { CentreUserService } from '../../../../core/index';
import { LocalDataSource } from '../../../../shared/ng2-smart-table/ng2-smart-table/lib/data-source/local/local.data-source';
import { ToasterService } from 'angular2-toaster';
import { SpinningToast } from '../../../../shared/index';
import { CreateUpdateRoomDto, RoomDto } from '../../../../api/index';

@Component({
    moduleId: module.id,
    selector: 'rooms-table-cmp',
    templateUrl: './roomsTable.component.html'
})
export class RoomsTableComponent implements OnInit {

    private careTypes: { [key: string]: string }[];
    private settings: any;

    private rooms: RoomDto[];
    private ageRegex: RegExp = /^(\d+\.?\d{0,})$/;
    private wrongAgeFormatMessage: string = 'Must be a number or at most one decimal place (e.g. 1.5)';
    private source: LocalDataSource = new LocalDataSource();
    constructor(private _roomsService: RoomsService,
        private _userService: CentreUserService,
        private _toasterService: ToasterService) {
        this.careTypes = new Array<{ [key: string]: string }>();
    }

    ngOnInit(): void {
        this._roomsService
            .getCareTypeValues$()
            .map(res => {
                for (let key in res) {
                    if (res.hasOwnProperty(key)) {
                        this.careTypes.push({
                            title: res[key],
                            value: key
                        });
                    }
                }
            })
            .flatMap(() => this._roomsService.GetRooms$(this._userService.SelectedCentre.id))
            .subscribe(roomsRes => {
                this.initSettings();
                this.rooms = roomsRes;
                this.source.load(this.rooms);
            });
    }

    addRoom(event: any): void {
        this.createUpdateRoom(event);
    }

    editRoom(event: any): void {
        this.createUpdateRoom(event);
    }

    createUpdateRoom(event: any) {

        if (event.newData.name === ''
            || event.newData.description === ''
            || event.newData.minAge.toString() === ''
            || event.newData.maxAge.toString() === '') {

            this._toasterService
                .popAsync('error', 'Missing Information', 'All fields must be provided');
            event.confirm.reject();

        } else if (event.newData.minAge.toString().match(this.ageRegex) === null) {
            this._toasterService.popAsync('error', 'Invalid Age', this.wrongAgeFormatMessage);
        } else if (event.newData.maxAge.toString().match(this.ageRegex) === null) {
            this._toasterService.popAsync('error', 'Invalid Age', this.wrongAgeFormatMessage);
        } else if (!this.isValidAgeRange(event.newData.minAge.toString(), event.newData.maxAge.toString())) {
            this._toasterService.popAsync('error', 'Invalid Age Range');
        } else {
            let savingToast = this._toasterService.pop(new SpinningToast());
            let createRoomDto = new CreateUpdateRoomDto();
            createRoomDto.init({
                Id: event.newData.id,
                CentreId: this._userService.SelectedCentre.id,
                Name: event.newData.name,
                Description: event.newData.description,
                MinAge: event.newData.minAge,
                MaxAge: event.newData.maxAge,
                CareType: event.newData.careType
            });
            this._roomsService.CreateUpdateRoom$(createRoomDto)
                .toPromise()
                .then((res) => {
                    this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                    this._toasterService.popAsync('success', 'Saved', '');
                    return event.confirm.resolve(res);
                })
                .catch((error) => {
                    this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                    return event.confirm.reject();
                });
        }
    }

    removeRoom(event: any): void {
        if (window.confirm('Are you sure you want to delete?')) {

            let savingToast = this._toasterService.pop(new SpinningToast());
            let roomId: string = event.data.id;
            this._roomsService
                .DeleteRoom$(this._userService.SelectedCentre.id, roomId)
                .toPromise()
                .then((res) => {
                    this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                    this._toasterService.popAsync('success', 'Deleted', '');
                    return event.confirm.resolve();
                })
                .catch((error) => {
                    this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                    return event.confirm.reject();
                });
        } else {
            event.confirm.reject();
        }
    }

    private isValidAgeRange(minAgeStr: string, maxAgeStr: string): boolean {
        let minAge = parseFloat(minAgeStr);
        let maxAge = parseFloat(maxAgeStr);

        if (0 > minAge || 0 > maxAge)
            return false;

        if (minAge > maxAge)
            return false;

        return true;
    }

    private initSettings(): void {
        this.settings = {
            mode: 'internal',
            hideHeader: false,
            hideSubHeader: false,
            sort: false,
            actions: {
                columnTitle: '',
                add: true,
                edit: true,
                delete: true,
                position: 'right'
            },
            add: {
                addButtonContent: 'Add Room',
                createButtonContent: 'Create',
                cancelButtonContent: 'Cancel',
                confirmCreate: true
            },
            edit: {
                editButtonContent: 'Edit Room',
                saveButtonContent: 'Save',
                cancelButtonContent: 'Cancel',
                confirmSave: true
            },
            delete: {
                deleteButtonContent: 'Remove Room',
                confirmDelete: true
            },
            columns: {
                name: {
                    title: 'Room Name',
                    filter: false
                },
                description: {
                    title: 'Description',
                    filter: false
                },
                minAge: {
                    title: 'Minimum Age',
                    filter: false,
                    type: 'custom',
                    renderComponent: DisplayAgeComponent,
                    editor: {
                        type: 'custom',
                        component: SelectAgeComponent
                    }
                },
                maxAge: {
                    title: 'Maximum Age',
                    filter: false,
                    type: 'custom',
                    renderComponent: DisplayAgeComponent,
                    editor: {
                        type: 'custom',
                        component: SelectAgeComponent
                    }
                },
                careType: {
                    title: 'Care Type',
                    filter: false,
                    type: 'custom',
                    defaultValue: '0',
                    renderComponent: DisplayCareTypeComponent,
                    editor: {
                        type: 'list',
                        config: {
                            list: this.careTypes
                        }
                    },
                }
            },
            noDataMessage: 'No Rooms'
        };
    }
}
