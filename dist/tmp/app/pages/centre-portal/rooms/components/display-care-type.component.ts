import { Component, Input, OnInit } from '@angular/core';

import { RoomsService } from '../index';

import { ViewCell } from '../../../../shared/ng2-smart-table/index';

@Component({
    template: `
    <span>{{ careTypeName }}</span>
    `
})
export class DisplayCareTypeComponent implements ViewCell, OnInit {

    @Input() value: string | number;
    private careTypeName: string;
    constructor(private _roomsService: RoomsService) {
        this.careTypeName = '';
    }

    ngOnInit() {
        this._roomsService
            .getCareTypeValues$()
            .subscribe(res => {
                let name: string = res[this.value];
                if (name !== undefined)
                    this.careTypeName = name;
            });
    }
}
