import { Component, AfterViewInit, ViewChild, ElementRef } from '@angular/core';

import { DefaultEditor } from '../../../../shared/ng2-smart-table/index';
import { AgeConverter } from './age-converter';

@Component({
    template: `
    <div>
        <div style="display: inline-block;" >
            <div style="display: inline-block;" >
                <select #year id="select-years" class="form-control" (change)="updateValue()" >
                    <option *ngFor="let year of allowedYears" [ngValue]="year">{{year}}</option>
                </select>
            </div>
            <div style="display: inline-block;" >
                <small>Y</small>
            </div>
        </div>
        <div style="display: inline-block;" >
            <span>&#160;</span>
        </div>
        <div style="display: inline-block;" >
            <div style="display: inline-block;" >
                <select #month id="select-months" class="form-control" (change)="updateValue()" >
                    <option *ngFor="let month of allowedMonths" [ngValue]="month">{{ month }}</option>
                </select>
            </div>
            <div style="display: inline-block;" >
                <small>M</small>
            </div>
        </div>
    <div>
    <div [hidden]="true" [innerHTML]="cell.getValue()" #htmlValue></div>
    `
})
export class SelectAgeComponent extends DefaultEditor implements AfterViewInit {

    @ViewChild('year') year: ElementRef;
    @ViewChild('month') month: ElementRef;
    @ViewChild('htmlValue') htmlValue: ElementRef;

    readonly allowedYears: number[] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14];
    readonly allowedMonths: number[] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];

    constructor() {
        super();
    }

    ngAfterViewInit(): void {
        if (this.cell.newValue !== '') {
            this.year.nativeElement.value = this.getYearValue();
            this.month.nativeElement.value = this.getMonthValue();
        } else {
            this.cell.newValue = 0;
        }
    }

    updateValue(): void {
        const month = parseFloat(this.month.nativeElement.value);
        const year = parseFloat(this.year.nativeElement.value);
        this.cell.newValue = AgeConverter.getAgeFraction(year, month);
    }

    getYearValue(): number {
        let years = parseFloat(this.htmlValue.nativeElement.innerText);
        return AgeConverter.getYearFraction(years);
    }

    getMonthValue(): number {
        let months = parseFloat(this.htmlValue.nativeElement.innerText);
        return AgeConverter.getMonthFraction(months);
    }
}
