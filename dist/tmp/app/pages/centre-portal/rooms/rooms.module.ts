import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/index';

import {
    RoomComponent, RoomsTableComponent,
    SelectAgeComponent, DisplayAgeComponent,
    DisplayCareTypeComponent,
    RoomsService
} from './index';

@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [
        RoomComponent,
        RoomsTableComponent,
        SelectAgeComponent,
        DisplayAgeComponent,
        DisplayCareTypeComponent
    ],
    entryComponents: [SelectAgeComponent, DisplayAgeComponent, DisplayCareTypeComponent],
    providers: [RoomsService],
    exports: [
        RoomComponent,
        RoomsTableComponent,
        SelectAgeComponent,
        DisplayAgeComponent,
        DisplayCareTypeComponent
    ]
})

export class RoomsModule { }
