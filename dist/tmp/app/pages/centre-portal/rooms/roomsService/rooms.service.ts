import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiClientService, CreateUpdateRoomDto, RoomDto } from '../../../../api/index';

@Injectable()
export class RoomsService {

    private careTypes: { [key: string]: string } = null;
    constructor(private _apiService: ApiClientService) {

    }

    public CreateUpdateRoom$(roomDto: CreateUpdateRoomDto): Observable<RoomDto> {
        return this._apiService.room_CreateUpdateRoomAsync(roomDto);
    }

    public GetRooms$(centreId: string): Observable<RoomDto[]> {
        return this._apiService.room_GetRooms(centreId);
    }

    public DeleteRoom$(centreId: string, roomId: string): Observable<void> {
        return this._apiService.room_DeleteRoomAsync(centreId, roomId);
    }

    public getCareTypeValues$(): Observable<{ [key: string]: string }> {
        if (this.careTypes !== null)
            return Observable.of(this.careTypes);
        return this._apiService
            .generics_GetEnumDescriptions('CareType')
            .map(res => this.careTypes = res);
    }
}
