import { Route } from '@angular/router';
import { NavigationGuardService } from '../../../shared/index';
import { BookingsComponent } from './index';

export const BookingsRoutes: Route[] = [
	{
		path: 'bookings',
		component: BookingsComponent,
		canActivate: [NavigationGuardService]
	}
];
