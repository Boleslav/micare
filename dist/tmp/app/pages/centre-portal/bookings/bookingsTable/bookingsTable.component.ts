import { Component, OnInit } from '@angular/core';

import { BookingsService } from '../bookingsService/bookings.service';
import { RoomsService } from '../../rooms/roomsService/rooms.service';
import { CentreUserService } from '../../../../core/index';
import { ToasterService } from 'angular2-toaster';
import { SpinningToast } from '../../../../shared/index';
import { ChildPermanentBookingDto, PermanentBookingDto, RoomDto } from '../../../../api/index';
import { AgeConverter } from '../../rooms/components/age-converter';

@Component({
    moduleId: module.id,
    selector: 'bookings-table-cmp',
    templateUrl: './bookingsTable.component.html',
    styleUrls: ['./bookingsTable.component.css']
})
export class BookingsTableComponent implements OnInit {

    private centreChildren: ChildPermanentBookingDto[];
    private editChildRef?: number;
    private editBookingRef?: number;
    private rooms: RoomDto[];
    // Stores values before edit so we can rollback on error
    private preEditedRoomId: string;
    private preEditedMon: boolean;
    private preEditedTue: boolean;
    private preEditedWed: boolean;
    private preEditedThu: boolean;
    private preEditedFri: boolean;
    private preEditedSat: boolean;
    private preEditedSun: boolean;

    private isAddMode: boolean;   // used to determine if we're addding a row
    private isDisableAddEdit: boolean = false;

    private disableSave: boolean = false;

    constructor(private _bookingsService: BookingsService,
        private _roomsService: RoomsService,
        private _userService: CentreUserService,
        private _toasterService: ToasterService) { }

    ngOnInit(): void {
        this.isAddMode = false;
        this._bookingsService
            .GetCentrePermanentBookings$(this._userService.SelectedCentre.id)
            .subscribe(
            res => {
                this.centreChildren = res;
            });

        this._roomsService
            .GetRooms$(this._userService.SelectedCentre.id)
            .subscribe(
            res => {
                this.rooms = res;
            });
    }

    addBooking(child: ChildPermanentBookingDto, childIdx: number, bookingIdx: number) {
        // Set add mode and row
        this.isAddMode = true;
        this.editChildRef = childIdx;
        this.isDisableAddEdit = true;

        // New up booking
        var newBooking = new PermanentBookingDto();
        newBooking.centreId = child.centreId;
        newBooking.kidId = child.kidId;

        // Add to childs bookings list
        child.permanentBookingList.push(newBooking);

        // set the booking index to the last booking in the booking array
        this.editBookingRef = child.permanentBookingList.length - 1;
    }

    updateBooking(booking: PermanentBookingDto, child: ChildPermanentBookingDto) {
        let savingToast = this._toasterService.pop(new SpinningToast());
        this.disableSave = true;
        this._bookingsService
            .UpdatePermanentBooking$(booking)
            .subscribe(
            res => {
                // reset edit indexs and preEditedBooking
                this.resetEditingProperties();
                // update permanent booking array in case api service has merged bookings.
                this._bookingsService.GetPermanentBookings$(child.centreId, child.kidId)
                    .subscribe(
                    res => {
                        child.permanentBookingList = res;
                    });
                this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                this._toasterService.popAsync('success', 'Booking updated', '');
            },
            (err) => this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId));
        this.disableSave = false;
    }

    deleteBooking(booking: PermanentBookingDto, child: ChildPermanentBookingDto) {
        if (window.confirm('Are you sure you want to remove the booking?')) {
            let savingToast = this._toasterService.pop(new SpinningToast());
            this._bookingsService
                .DeletePermanentBooking$(booking.centreId, booking.id)
                .subscribe(res => {
                    var index = child.permanentBookingList.indexOf(booking);
                    child.permanentBookingList.splice(index, 1);
                    this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                    this._toasterService.popAsync('success', 'Booking removed', '');
                },
                (err) => this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId));
        }
    }

    editBooking(childIdx: number, bookingIdx: number, permanentBooking: PermanentBookingDto) {
        this.editChildRef = childIdx;
        this.editBookingRef = bookingIdx;
        this.isDisableAddEdit = true;

        // store preEditedBooking
        this.preEditedRoomId = permanentBooking.roomId;
        this.preEditedMon = permanentBooking.mon;
        this.preEditedTue = permanentBooking.tue;
        this.preEditedWed = permanentBooking.wed;
        this.preEditedThu = permanentBooking.thu;
        this.preEditedFri = permanentBooking.fri;
        this.preEditedSat = permanentBooking.sat;
        this.preEditedSun = permanentBooking.sun;
    }

    cancelEdit(permanentBooking: PermanentBookingDto, child: ChildPermanentBookingDto) {

        if (this.isAddMode) {
            var index = child.permanentBookingList.indexOf(permanentBooking);
            child.permanentBookingList.splice(index, 1);
        } else {
            // Editing existing booking, rollback permanentBooking
            permanentBooking.roomId = this.preEditedRoomId;
            permanentBooking.mon = this.preEditedMon;
            permanentBooking.tue = this.preEditedTue;
            permanentBooking.wed = this.preEditedWed;
            permanentBooking.thu = this.preEditedThu;
            permanentBooking.fri = this.preEditedFri;
            permanentBooking.sat = this.preEditedSat;
            permanentBooking.sun = this.preEditedSun;
        }
        this.resetEditingProperties();
    }

    editMode(childIdx: number, bookingIdx: number): boolean {
        if (childIdx === this.editChildRef && bookingIdx === this.editBookingRef) {
            return true;
        }
        return false;
    }

    addMode(): boolean {
        return this.isAddMode;
    }

    disableAddEdit(): boolean {
        return this.isDisableAddEdit;
    }

    isInvalidRoom(booking: PermanentBookingDto): boolean {
        return (booking.roomId === '' || booking.roomId === null || booking.roomId === undefined);
    }

    getRoomName(roomId: string): string {

        if (this.rooms === undefined || this.rooms === null)
            return '';
        let filteredRooms: RoomDto[] = this.rooms.filter(x => x.id === roomId);
        return filteredRooms.length > 0 ? filteredRooms[0].name : '';
    }

    resetEditingProperties() {
        // Reset editing properties
        this.editChildRef = null;
        this.editBookingRef = null;
        this.preEditedRoomId = null;
        this.preEditedMon = null;
        this.preEditedTue = null;
        this.preEditedWed = null;
        this.preEditedThu = null;
        this.preEditedFri = null;
        this.preEditedSat = null;
        this.preEditedSun = null;
        this.isAddMode = false;
        this.isDisableAddEdit = false;
    }

    getChildAgeYears(age: string): string {
        return AgeConverter.getYearFraction(parseFloat(age)).toString();
    }

    getChildAgeMonths(age: string): string {
        return AgeConverter.getMonthFraction(parseFloat(age)).toString();
    }
}
