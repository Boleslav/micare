import { NgModule } from '@angular/core';

import { SharedModule } from '../../../shared/index';
import { BookingsComponent, BookingsTableComponent, BookingsService } from './index';

@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [BookingsComponent, BookingsTableComponent],
    providers: [BookingsService],
    exports: [BookingsComponent, BookingsTableComponent]
})

export class BookingsModule { }
