import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { CentreUserService } from '../../../../core/index';
import { Config } from '../../../../shared/index';

@Component({
	moduleId: module.id,
	selector: 'sidebar-cmp',
	templateUrl: 'sidebar.html'
})

export class SidebarComponent implements OnInit {

	isActive = false;
	showMenu: string = '';
	isSysAdmin: boolean = false;
	isCentreManager: boolean = false;

	private enrolmentFeatureEnabled: boolean = false;

	constructor(private _userService: CentreUserService, ) {
		if (Config.ENABLE_ENROLMENT_FEATURE === 'true') {
            this.enrolmentFeatureEnabled = true;
        }
	}

	ngOnInit(): void {

		Observable.forkJoin([
			this._userService.IsSysAdmin$(),
			this._userService.IsCentreManager$()
		]).subscribe(res => {
			let sysAdmin: boolean = res[0];
			let centreAdmin: boolean = res[1];

			this.isSysAdmin = sysAdmin;
			this.isCentreManager = centreAdmin;
		});
	}

	eventCalled() {
		this.isActive = !this.isActive;
	}

	addExpandClass(element: any) {
		if (element === this.showMenu) {
			this.showMenu = '0';
		} else {
			this.showMenu = element;
		}
	}
}
