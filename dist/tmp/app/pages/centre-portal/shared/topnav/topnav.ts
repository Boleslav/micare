import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';

import { CentreUserService } from '../../../../core/index';
import { CentreDto } from '../../../../api/index';

@Component({
	moduleId: module.id,
	selector: 'top-nav',
	templateUrl: 'topnav.html',
})
export class TopNavComponent implements OnInit {

	private typeaheadLoading: boolean;
	private typeaheadNoResults: boolean;
	private centres: CentreDto[] = null;
	private selectedCentre: CentreDto = null;
	private dataSource: Observable<CentreDto[]>;
	private searchCentre: string = '';
	private isSysAdmin: boolean = false;

	constructor(private _userService: CentreUserService,
		private _router: Router,
		private _route: ActivatedRoute) {
		this.dataSource = Observable
			.create((observer: any) => {
				observer.next(this.searchCentre);
			})
			.mergeMap((searchTerm: string) => this._userService.searchCentres(searchTerm));
	}

	ngOnInit(): void {
		this._userService
			.getCentres$()
			.subscribe(res => {
				this.centres = res;
				this.selectedCentre = this.centres.find(centre => centre.id === this._userService.SelectedCentre.id);
				if (this.selectedCentre !== undefined) {
					this.searchCentre = this.selectedCentre.name;
				}
			});

		this._userService
			.IsSysAdmin$()
			.subscribe(res => this.isSysAdmin = res);
	}

	selectCentre(centre: CentreDto) {
		this.selectedCentre = centre;
		this._userService.SelectedCentre = centre;
		let redirectTo: string = encodeURI(this._router.url);
		this._router.navigate(['centre-portal', 'redirect', redirectTo]);
	}

	public changeTypeaheadLoading(e: boolean): void {
		this.typeaheadLoading = e;
	}

	public changeTypeaheadNoResults(e: boolean): void {
		this.typeaheadNoResults = e;
	}

	typeaheadOnSelect(e: TypeaheadMatch): void {
		let centre: CentreDto = <CentreDto>e.item;
		this.selectCentre(centre);
	}

	changeTheme(color: string): void {
		var link: any = $('<link>');
		link
			.appendTo('head')
			.attr({ type: 'text/css', rel: 'stylesheet' })
			.attr('href', 'themes/app-' + color + '.css');
	}

	rtl(): void {
		var body: any = $('body');
		body.toggleClass('rtl');
	}

	sidebarToggler(): void {
		var sidebar: any = $('#sidebar');
		var mainContainer: any = $('.main-container');
		sidebar.toggleClass('sidebar-left-zero');
		mainContainer.toggleClass('main-container-ml-zero');
	}
}
