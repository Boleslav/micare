import { Component, ViewChild, ElementRef, Input, Output, EventEmitter, AfterViewInit, OnInit } from '@angular/core';
import {Cell} from "../../../../shared/ng2-smart-table/ng2-smart-table/lib/data-set/cell";
import {ViewCell} from "../../../../shared/ng2-smart-table/ng2-smart-table/components/cell/cell-view-mode/view-cell";
import {Editor} from "../../../../shared/ng2-smart-table/ng2-smart-table/components/cell/cell-editors/default-editor";

@Component({
    moduleId: module.id,
    template: `
        <label *ngIf="!isNew"
          [ngStyle]="statusStyle" 
          #valueView 
          [innerHTML]="displayValue"></label>
        <input #valueEdit 
            *ngIf="isNew"
            type="checkbox"
            title="You are able to set the Exclussive status only on the Company creation."
            class=""
            (click)="updateValue()" />`,
})
export class CompanyExclussiveStatusCellRenderer implements OnInit, AfterViewInit, ViewCell, Editor {

  @Input() cell: Cell;
  @Input() inputClass: string;
  @Input() value: string;

  @Output() onStopEditing = new EventEmitter<any>();
  @Output() onEdited = new EventEmitter<any>();
  @Output() onClick = new EventEmitter<any>();

  @ViewChild('valueView') valueView: ElementRef;
  @ViewChild('valueEdit') valueEdit: ElementRef;

  displayValue: string;
  isEditing: boolean = false;
  statusStyle: any;
  isNew: boolean;

  ngAfterViewInit(): void {

    if(this.valueEdit && (!this.isEditing || !!this.cell.getRow().getData().id)) {// editing
      this.valueEdit.nativeElement.setAttribute('disabled', 'disabled');
    } else {

    }
  }

  ngOnInit() {
    this.isEditing = this.cell && this.cell.getRow().isInEditing;
    this.isNew = this.isEditing && !this.cell.getRow().getData().id;

    if(this.isNew) {// editing

    } else {
      let value = this.cell ? this.cell.getValue() : this.value;
      this.displayValue = value ? 'Exclussive' : 'Regular';
      this.statusStyle = value ? { color: '#96cbfe' } : null;
    }
  }

   updateValue() {
     this.cell.newValue = !this.cell.newValue;
   }
}
