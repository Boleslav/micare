import { Component, ViewChild, ElementRef, Input, Output, EventEmitter, AfterViewInit, OnInit } from '@angular/core';
import {Cell} from "../../../../shared/ng2-smart-table/ng2-smart-table/lib/data-set/cell";
import {ViewCell} from "../../../../shared/ng2-smart-table/ng2-smart-table/components/cell/cell-view-mode/view-cell";
import {Editor} from "../../../../shared/ng2-smart-table/ng2-smart-table/components/cell/cell-editors/default-editor";

@Component({
    moduleId: module.id,
    template: `
      <a *ngIf="companyId" class="btn btn-primary" [routerLink]="[ companyId, 'centres']">Manage Centres</a>
      <a *ngIf="companyId" class="btn btn-primary" [routerLink]="[ companyId, 'staff']">Manage Staff</a>`,
})
export class CompanyButtonsCellRenderer implements OnInit, AfterViewInit, ViewCell, Editor {

  @Input() cell: Cell;
  @Input() inputClass: string;
  @Input() value: string;

  @Output() onStopEditing = new EventEmitter<any>();
  @Output() onEdited = new EventEmitter<any>();
  @Output() onClick = new EventEmitter<any>();

  displayValue: string;
  companyId: string;

  ngAfterViewInit(): void {
  }

  ngOnInit() {
    if(this.cell && this.cell.getRow().getData()) {
      this.companyId = this.cell.getRow().getData().id;
    } else {
      this.companyId = this.value;
    }
  }
}
