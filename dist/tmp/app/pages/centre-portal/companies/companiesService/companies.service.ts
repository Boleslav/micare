import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiClientService, CompanyDto } from '../../../../api/index';

@Injectable()
export class CompaniesService {

    constructor(private _apiService: ApiClientService) {

    }

    public CreateUpdateCompany$(companyDto: CompanyDto): Observable<CompanyDto> {
        return this._apiService.company_CreateUpdateCompanyAsync(companyDto);
    }

    public GetCompanies$(): Observable<CompanyDto[]> {
        return this._apiService.company_GetAll();
    }

    public GetCompany$(companyId :string): Observable<CompanyDto> {
        return this._apiService.company_Get(companyId);
    }



}
