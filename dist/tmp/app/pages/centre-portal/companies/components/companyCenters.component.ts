import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';

import { Observable } from 'rxjs';
import { CompaniesService } from '../index';
import { LocalDataSource } from '../../../../shared/ng2-smart-table/ng2-smart-table/lib/data-source/local/local.data-source';
import { ToasterService } from 'angular2-toaster';
import { SpinningToast } from '../../../../shared/index';
import { CompanyDto } from '../../../../api/index';
import {CompanyFacilityViewModel} from "../../../../api/client.service";
import {CentreService} from "../../centre/centreService/centre.service";

@Component({
    moduleId: module.id,
    selector: 'company-centres-cmp',
    templateUrl: './companyCenters.component.html'
})

export class CompanyCentresComponent implements OnInit {

    private companyId: string;

    private centres: CompanyFacilityViewModel[];
    private company: CompanyDto = new CompanyDto();

    // centre search
    private typeaheadLoading: boolean;
    private typeaheadNoResults: boolean;
    private centresToAdd: CompanyFacilityViewModel[] = null;
    private selectedCentre: CompanyFacilityViewModel = null;
    private dataSource: Observable<CompanyFacilityViewModel[]>;
    private searchCentre: string = '';
    private isSysAdmin: boolean = false;


  constructor(private route: ActivatedRoute,
                private _companiesService: CompaniesService,
                private _centreService: CentreService,
        private _toasterService: ToasterService) {

      this.route.params
        .map(params => params['companyId'])
        .subscribe((id) => {
          this.companyId = id;
          this._centreService.GetCompanyCentres(id)
            .subscribe(centres => this.centres = centres);

          this._companiesService.GetCompany$(id)
            .subscribe(company => this.company = company);
        });

      this.dataSource = Observable
        .create((observer: any) => {
          observer.next(this.searchCentre);
        })
        .mergeMap((searchTerm: string) => this._centreService.GetUnassignedCentres(searchTerm));

  }

    ngOnInit(): void {
        // this._companiesService.GetCompanies$()
        //   .subscribe(res => {
        //     this.initSettings();
        //     this.companies = res;
        //     this.source.load(this.companies);
        //   });
    }

  selectCentre(centre: CompanyFacilityViewModel) {
    this.selectedCentre = centre;
  }

  addCentre() {
    let centre = this.selectedCentre;
    if (!centre) {
      return;
    }

    let selectedAlreadyAssigned = false;
    this.centres.forEach(c => selectedAlreadyAssigned = selectedAlreadyAssigned || (c.facilityId == centre.facilityId));
    if(selectedAlreadyAssigned) {
      this.selectedCentre = null;
      return;
    }

    this.setActiveEx(centre, false)
      .subscribe(() => {
        let c = new CompanyFacilityViewModel();
        c.facilityId = centre.facilityId;
        c.facilityName = centre.facilityName;
        c.companyId = this.companyId;
        c.isActive = false;
        this.centres.push(c);

        this.selectedCentre = null;

        this.dataSource = Observable
          .create((observer: any) => {
            observer.next(this.searchCentre);
          })
          .mergeMap((searchTerm: string) => this._centreService.GetUnassignedCentres(searchTerm));

      })
  }

  setActive(centre: CompanyFacilityViewModel, setActive: boolean) {
    this.setActiveEx(centre, setActive)
      .subscribe(() => {
        centre.isActive = setActive;
      });
  }

  setActiveEx(centre: CompanyFacilityViewModel, setActive: boolean) {
    return this._centreService.SetActiveCompanyCentre(this.companyId, centre.facilityId, setActive);
  }

  public changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }

  public changeTypeaheadNoResults(e: boolean): void {
    this.typeaheadNoResults = e;
  }

  typeaheadOnSelect(e: TypeaheadMatch): void {
    let centre: CompanyFacilityViewModel = <CompanyFacilityViewModel>e.item;
    this.selectCentre(centre);
  }

}
