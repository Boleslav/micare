import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/index';

import {
    CompaniesComponent,
    CompaniesTableComponent,
    CompaniesService,
    CompanyExclussiveStatusCellRenderer,
    CompanyButtonsCellRenderer,
    CompanyCentresComponent,
    CompanyStaffComponent,
} from './index';

@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [
        CompaniesComponent,
        CompaniesTableComponent,
        CompanyExclussiveStatusCellRenderer,
        CompanyButtonsCellRenderer,
        CompanyCentresComponent,
        CompanyStaffComponent,
    ],
    entryComponents: [
        CompanyExclussiveStatusCellRenderer,
        CompanyButtonsCellRenderer
    ],
    providers: [CompaniesService],
    exports: [
        CompaniesComponent,
        CompaniesTableComponent
    ]
})

export class CompaniesModule { }
