import { Route } from '@angular/router';
import { NavigationGuardService } from '../../../shared/index';
import { EnrolmentApprovalsComponent } from './index';

export const EnrolmentApprovalsRoutes: Route[] = [
	{
		path: 'enrolment-approvals',
		component: EnrolmentApprovalsComponent,
		canActivate: [NavigationGuardService]
	},
];
