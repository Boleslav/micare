export * from './enrolmentApprovals.component';
export * from './enrolmentApprovalsTable/enrolmentApprovalsTable.component';
export * from './enrolmentApprovalsService/enrolmentApprovals.service';
export * from './enrolment-approvals.routes';
export * from './enrolment-approvals.module';
