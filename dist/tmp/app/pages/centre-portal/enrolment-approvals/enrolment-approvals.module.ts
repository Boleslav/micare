import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/index';
import { EnrolmentApprovalsComponent, EnrolmentApprovalsTableComponent, EnrolmentApprovalsService } from './index';

@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [EnrolmentApprovalsComponent, EnrolmentApprovalsTableComponent],
    providers: [EnrolmentApprovalsService],
    exports: [EnrolmentApprovalsComponent, EnrolmentApprovalsTableComponent]
})

export class EnrolmentApprovalsModule { }
