export let EnrolmentApprovalsFilter: any = {
    'Awaiting Approval': 1,
    'Denied': 2,
    'Approved': 3,
    'Show All': 4
};
