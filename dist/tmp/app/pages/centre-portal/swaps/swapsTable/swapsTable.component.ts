import { Component, OnInit } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { SwapsService } from '../swapsService/swaps.service';
import { CentreUserService } from '../../../../core/index';
import { SpinningToast } from '../../../../shared/index';
import { SwapDayDto } from '../../../../api/index';

@Component({
    moduleId: module.id,
    selector: 'swaps-table-cmp',
    templateUrl: './swapsTable.component.html'
})
export class SwapsTableComponent implements OnInit {

    private fromDate: Date = new Date();
    private toDate: Date = new Date();

    private swaps: SwapDayDto[];
    private disableButtons: boolean = false;

    constructor(private _swapsService: SwapsService,
        private _userService: CentreUserService,
        private _toasterService: ToasterService) {
    }

    ngOnInit(): void {
        this.toDate.setDate(this.fromDate.getDate() + 90);
        this._swapsService
            .GetSwapDays$(this._userService.SelectedCentre.id, this.fromDate)
            .subscribe(swapsRes => {
                this.swaps = swapsRes;
            });
    }

    removeSwapDay(miSwapId: string) {
        if (window.confirm('The parent giving the day will receive an email advising them of the removal. ' +
            'Do you want to remove this swap? Press Ok to continue.')) {
            let savingToast = this._toasterService.pop(new SpinningToast());
            this.disableButtons = true;
            this._swapsService
                .RemoveSwapDay$(miSwapId, this._userService.SelectedCentre.id)
                .subscribe(
                res => {
                    // get latest swap days
                    this._swapsService
                        .GetSwapDays$(this._userService.SelectedCentre.id, this.fromDate)
                        .subscribe(swapsRes => {
                            this.swaps = swapsRes;
                        });
                    this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                    this._toasterService.popAsync('success', 'Saved', '');
                },
                (err) => this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId));
            this.disableButtons = false;
        }
    }

    reverseSwapDay(miSwapId: string) {
        if (window.confirm('Reversing a day that was already given will result in a ' +
            'processing fee being deducted from the giving MiSwap account. Press Ok to continue.')) {
            let savingToast = this._toasterService.pop(new SpinningToast());
            this.disableButtons = true;
            this._swapsService
                .ReverseSwapDay$(miSwapId, this._userService.SelectedCentre.id)
                .subscribe(
                res => {
                    // get latest swap days
                    this._swapsService
                        .GetSwapDays$(this._userService.SelectedCentre.id, this.fromDate)
                        .subscribe(swapsRes => {
                            this.swaps = swapsRes;
                        });
                    this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                    this._toasterService.popAsync('success', 'Saved', '');
                },
                (err) => this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId));
            this.disableButtons = false;
        }
    }

    previousDay() {
        this.disableButtons = true;
        var fromDate: Date = new Date(this.fromDate);
        fromDate.setDate(fromDate.getDate() - 1);

        var toDate: Date = new Date(this.toDate);
        toDate.setDate(toDate.getDate() - 1);

        this.refreshSwaps(fromDate, toDate);
        this.disableButtons = false;
    }

    nextDay() {
        this.disableButtons = true;
        var fromDate: Date = new Date(this.fromDate);
        fromDate.setDate(fromDate.getDate() + 1);

        var toDate: Date = new Date(this.toDate);
        toDate.setDate(toDate.getDate() + 1);

        this.refreshSwaps(fromDate, toDate);
        this.disableButtons = false;
    }

    refreshSwaps(fromDate: Date, toDate: Date) {
        this._swapsService
            .GetSwapDays$(this._userService.SelectedCentre.id, fromDate)
            .subscribe(swapsRes => {
                this.swaps = swapsRes;
                this.fromDate = fromDate;
                this.toDate = toDate;
            });
    }

    swapStateString(swapState: number) {
        switch (swapState) {
            case 0:
                return 'To Give';
            case 1:
                return 'Given';
            case 2:
                return 'Processing';
            case 3:
                return 'Cancelled';
            case 4:
                return 'Expired';
            default:
                return 'Unknown';
        }
    }

    swapStateStyle(swapState: number) {
        switch (swapState) {
            case 0: // To Give
                return 'btn btn-sm btn-success btn-block';
            case 1: // Given
                return 'btn btn-sm btn-primary btn-block';
            case 2: // Processing
                return 'btn btn-sm btn-warning btn-block';
            case 3: // Cancelled
                return 'btn btn-sm btn-danger btn-block';
            case 4: // Expired
                return 'btn btn-sm btn-danger btn-block';
            default:    // Unkown
                return 'btn btn-sm btn-seconday btn-block';
        }
    }
}
