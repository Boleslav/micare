import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiClientService, SwapDayDto } from '../../../../api/index';

@Injectable()
export class SwapsService {

    constructor(private _apiService: ApiClientService) {

    }

    public GetSwapDays$(centreId: string, dateFrom: Date): Observable<SwapDayDto[]> {

        return this._apiService.swap_GetSwapDays(centreId, dateFrom);
    }

    public RemoveSwapDay$(miSwapId: string, centreId: string): Observable<SwapDayDto> {
        return this._apiService.swap_RemoveSwapDay(miSwapId, centreId);
    }

    public ReverseSwapDay$(miSwapId: string, centreId: string): Observable<SwapDayDto> {
        return this._apiService.swap_ReverseSwapDay(miSwapId, centreId);
    }
}
