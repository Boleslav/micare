import { Route } from '@angular/router';
import { NavigationGuardService } from '../../../shared/index';
import { SwapsComponent } from './index';

export const SwapsRoutes: Route[] = [
	{
		path: 'swaps',
		component: SwapsComponent,
		canActivate: [NavigationGuardService]
	}
];
