import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/index';

import { SwapsComponent, SwapsTableComponent, SwapsService } from './index';

@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [SwapsComponent, SwapsTableComponent],
    providers: [SwapsService],
    exports: [SwapsComponent, SwapsTableComponent]
})

export class SwapsModule { }
