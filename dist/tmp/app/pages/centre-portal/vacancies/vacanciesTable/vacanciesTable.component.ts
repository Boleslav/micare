import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ToasterService } from 'angular2-toaster';

import { VacanciesService } from '../vacanciesService/vacancies.service';
import { CentreUserService } from '../../../../core/index';
import { SpinningToast } from '../../../../shared/index';
import { VacancyDto, CreateUpdateVacancyDto } from '../../../../api/index';
import * as moment from 'moment/moment';
import * as _ from 'lodash';

@Component({
    moduleId: module.id,
    selector: 'vacancies-table-cmp',
    templateUrl: './vacanciesTable.component.html'
})
export class VacanciesTableComponent implements OnInit {

    private readonly dateKeyFormat = 'YYYY-MM-DD[T]HH:mm:ss';
    private roomVacancies: VacancyDto[] = null;
    private originalRoomVacancies: VacancyDto[] = null;
    private today: moment.Moment;
    private startOfWeek: moment.Moment = moment().startOf('isoWeek').utc(true);
    private daysOfWeek: moment.Moment[];
    private displayWeek: string;
    constructor(private _vacancyService: VacanciesService,
        private _userService: CentreUserService,
        private _toasterService: ToasterService) {
        this.today = moment().startOf('day');
    }

    ngOnInit(): void {
        this.getVacancies(this.startOfWeek);
    }

    public getVacancies(day: moment.Moment) {
        this.daysOfWeek = this.getDaysOfWeek();
        this.displayWeek = this.showWeek(day);
        this.roomVacancies = null;
        this.originalRoomVacancies = null;

        this._vacancyService
            .getVacancies$(this._userService.SelectedCentre.id, day.toDate())
            .subscribe(res => {
                this.roomVacancies = res;
                this.originalRoomVacancies = _.cloneDeep(this.roomVacancies);
            });
    }

    public getDaysOfWeek(): moment.Moment[] {
        let weekDays = new Array<any>();
        weekDays.push(this.startOfWeek);
        let startDate = this.startOfWeek.clone();
        for (let i: number = 0; i < 6; ++i) {
            startDate.add(1, 'days');
            weekDays.push(startDate.clone());
        }
        return weekDays;
    }

    public getVacanciesForDay(room: VacancyDto, forDay: moment.Moment): number {
        let dateKey: string = this.getDateKey(forDay);
        let numOfVacancies = room.vacancies[dateKey];
        return numOfVacancies === undefined ? 0 : numOfVacancies;
    }

    public getListedByParentsForDay(room: VacancyDto, forDay: moment.Moment): number {
        let dateKey: string = this.getDateKey(forDay);
        let numOfVacancies = room.listedByParents[dateKey];
        return numOfVacancies === undefined ? 0 : numOfVacancies;
    }

    public getSoldVacanciesForDay(room: VacancyDto, forDay: moment.Moment): number {
        let dateKey: string = this.getDateKey(forDay);
        let numOfVacancies = room.sold[dateKey];
        return numOfVacancies === undefined ? 0 : numOfVacancies;
    }

    public getTotalVacanciesForDay(room: VacancyDto, forDay: moment.Moment): number {
        return this.getVacanciesForDay(room, forDay) +
            this.getListedByParentsForDay(room, forDay) -
            this.getSoldVacanciesForDay(room, forDay);
    }

    public setNumberOfVacncies(room: VacancyDto, forDay: moment.Moment, input: string): void {
        let dateKey: string = this.getDateKey(forDay);
        let numberOfVacancies = parseFloat(input);
        if (!isNaN(numberOfVacancies)) {
            room.vacancies[dateKey] = numberOfVacancies;
        }
    }

    public createUpdateRoomVacancy(room: VacancyDto): void {
        let savingToast = this._toasterService.pop(new SpinningToast());
        this.createUpdateRoomVacancy$(room)
            .subscribe(() => {
                this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                this._toasterService.popAsync('success', 'Saved', '');
            },
            err => this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId));
    }

    public createUpdateRoomVacancy$(room: VacancyDto): Observable<void> {

        let createUpdateVacancyDto: CreateUpdateVacancyDto = CreateUpdateVacancyDto.fromJS(room.toJSON());
        return this._vacancyService
            .CreateUpdateVacancies$(createUpdateVacancyDto)
            .flatMap(res => {
                // replace roomId
                let currentRoomVacancies = this.roomVacancies.find((savedRoom) => savedRoom.roomId === room.roomId);
                currentRoomVacancies.vacancies = _.cloneDeep(res.vacancies);
                let origRoomVacancies = this.originalRoomVacancies.find((savedRoom) => savedRoom.roomId === room.roomId);
                origRoomVacancies.vacancies = _.cloneDeep(res.vacancies);
                return Observable.of(null);
            });
    }

    public getDateKey(forDay: moment.Moment): string {
        return forDay.format(this.dateKeyFormat) + 'Z';
    }

    public cancelEdit(room: VacancyDto) {
        let originalVacancies: { [key: string]: number } =
            this.originalRoomVacancies
                .find((savedRoom) => savedRoom.roomId === room.roomId)
                .vacancies;
        room.vacancies = _.cloneDeep(originalVacancies);
    }

    public clearVacancies(room: VacancyDto) {
        if (window.confirm('Are you sure you want to clear all vacancies for this room this week?')) {

            let savingToast = this._toasterService.pop(new SpinningToast());

            for (let date in room.vacancies)
                if (room.vacancies.hasOwnProperty(date))
                    room.vacancies[date] = 0;

            this._vacancyService
                .CreateUpdateVacancies$(room)
                .subscribe(res => {
                    let currentRoomVacancies = this.roomVacancies.find((savedRoom) => savedRoom.roomId === room.roomId);
                    currentRoomVacancies.vacancies = _.cloneDeep(res.vacancies);
                    let origRoomVacancies = this.originalRoomVacancies.find((savedRoom) => savedRoom.roomId === room.roomId);
                    origRoomVacancies.vacancies = _.cloneDeep(res.vacancies);
                    this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                    this._toasterService.popAsync('success', 'Saved', '');
                });
        }
    }

    public previousWeek() {
        this.getVacancies(this.startOfWeek.add(-7, 'days'));
    }

    public nextWeek() {
        this.getVacancies(this.startOfWeek.add(7, 'days'));
    }

    public copyVacancies(): void {
        if (window.confirm('Are you sure you want to copy all vacancies from the previous week?\n' +
            'This will overwrite any existing vacancies for this week')) {

            let savingToast = this._toasterService.pop(new SpinningToast());

            let lastWeek = this.startOfWeek.clone().add(-7, 'days');
            this._vacancyService
                .getVacancies$(this._userService.SelectedCentre.id, lastWeek.toDate())
                .flatMap(lastWeekVacancies => {
                    let observables = new Array<Observable<any>>();
                    for (let i: number = 0; i < lastWeekVacancies.length; ++i) {

                        let lastWeek: VacancyDto = lastWeekVacancies[i];

                        let thisWeekVacancies: VacancyDto = new VacancyDto();
                        thisWeekVacancies.init({
                            'CentreId': lastWeek.centreId,
                            'RoomId': lastWeek.roomId,
                            'Vacancies': {}
                        });
                        for (let date in lastWeek.vacancies)
                            if (lastWeek.vacancies.hasOwnProperty(date)) {
                                thisWeekVacancies.vacancies[this.getDateKey(moment(date).add(7, 'days'))]
                                    = lastWeek.vacancies[date];
                            }

                        let obs = this.createUpdateRoomVacancy$(thisWeekVacancies);
                        observables.push(obs);
                    }

                    return Observable.forkJoin(observables);
                })
                .subscribe(() => {
                    this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId);
                    this._toasterService.popAsync('success', 'Saved', '');
                },
                err => this._toasterService.clear(savingToast.toastId, savingToast.toastContainerId));
        }
    }

    public showWeek(day: moment.Moment): string {

        let displayWeek: string = '';
        if (moment().isSame(day, 'week')) {
            displayWeek = 'This Week';
        } else {
            displayWeek = `${this.daysOfWeek[0].format('Do MMM')} to 
                            ${this.daysOfWeek[this.daysOfWeek.length - 1].format('Do MMM')}`;
        }
        return displayWeek;
    }
}
