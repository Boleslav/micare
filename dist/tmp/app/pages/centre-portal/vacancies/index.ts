export * from './vacancies.component';
export * from './vacanciesTable/vacanciesTable.component';
export * from './vacanciesService/vacancies.service';
export * from './vacancies.routes';
export * from './vacancies.module';
