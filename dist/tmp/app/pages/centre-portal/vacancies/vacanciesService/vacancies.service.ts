import { Injectable } from '@angular/core';

import { ApiClientService, CreateUpdateVacancyDto } from '../../../../api/index';

@Injectable()
export class VacanciesService {

    constructor(private _apiService: ApiClientService) {
    }

    public getVacancies$(centreId: string, startDate: Date) {
        return this._apiService.room_GetAllVacanciesAsync(centreId, startDate);
    }

    public CreateUpdateVacancies$(createUpdateVacancyDto: CreateUpdateVacancyDto) {
        return this._apiService.room_CreateUpdateVacanciesAsync(createUpdateVacancyDto);
    }
}
