import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import {
    ApiClientService, CentreBrandingDto,
    CreateUpdateCentreBrandingDto, CentreLogoDto
} from '../../../../api/index';

@Injectable()
export class EnrolmentService {

    constructor(private _apiService: ApiClientService) {

    }

    public CreateUpdateCentreBranding$(centreBrandingDto: CreateUpdateCentreBrandingDto) {
        return this._apiService
            .centre_CreateUpdateCentreBranding(centreBrandingDto);
    }

    public GetCentreBranding$(centreId: string): Observable<CentreBrandingDto> {
        return this._apiService
            .centre_GetCentreBranding(centreId);
    }

    public GetCentreLogo$(centreId: string): Observable<CentreLogoDto> {
        return this._apiService.centre_GetCentreLogo(centreId);
    }
}
