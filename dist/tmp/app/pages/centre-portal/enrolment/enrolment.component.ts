import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { Config } from '../../../shared/index';

@Component({
    moduleId: module.id,
    selector: 'enrolment-cmp',
    templateUrl: './enrolment.component.html'
})

export class EnrolmentComponent {

    constructor(private _router: Router) {
        if (Config.ENABLE_ENROLMENT_FEATURE !== 'true') {
            this._router.navigate(['/']);
        }
    }
}
