import { NgModule } from '@angular/core';

import { SharedModule } from '../../../shared/index';
import {
    EnrolmentComponent, EnrolmentFormComponent,
    EnrolmentService, DomainNameValidatorService
} from './index';

@NgModule({
    imports: [
        SharedModule,
    ],
    declarations: [
        EnrolmentComponent,
        EnrolmentFormComponent
    ],
    providers: [EnrolmentService, DomainNameValidatorService]
})

export class EnrolmentModule { }
