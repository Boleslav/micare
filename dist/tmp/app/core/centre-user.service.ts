import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/find';

import { ApiClientService, CentreDto, CentreLogoDto, Status } from '../api/index';
import { LocalStorageService } from 'angular-2-local-storage';
import { AccessTokenService } from './index';

@Injectable()
export class CentreUserService {

    private static LAST_SELECTED_CENTRE = 'LastCentre_';
    private readonly ImageEncoding: string = 'data:image/png;base64,';

    private _userId: string = null;
    get UserId(): string {

        if (this._userId === null) {
            this._userId = this._storageService.get<string>(AccessTokenService.SUBJECT_ID);
        }
        return this._userId;
    }

    private _email: string = null;
    get Email(): string {

        if (this._email === null) {
            this._email = this._storageService.get<string>(AccessTokenService.USERNAME);
        }
        return this._email;
    }

    private _centres: CentreDto[] = null;
    get Centres(): CentreDto[] {
        return this._centres;
    }

    set Centres(val: CentreDto[]) {
        this._centres = val;
    }

    private _selectedCentre: CentreDto = null;
    get SelectedCentre(): CentreDto {
        if (this._selectedCentre === null && this.Centres.length > 0) {
            this._selectedCentre = this.Centres[0];
        }
        return this._selectedCentre;
    }

    set SelectedCentre(val: CentreDto) {
        this._selectedCentre = val;
        this.SetLastSelectedCentreId(val.id);
    }

    private _hangfireUrl: string = null;
    get HangfireUrl(): string {
        return this._hangfireUrl;
    }

    set HangfireUrl(val: string) {
        this._hangfireUrl = val;
    }

    private _swaggerUrl: string = null;
    get SwaggerUrl(): string {
        return this._swaggerUrl;
    }

    set SwaggerUrl(val: string) {
        this._swaggerUrl = val;
    }

    private isCentreManager: boolean = null;
    private isSystemAdmin: boolean = null;
    private logoDto: CentreLogoDto = null;

    constructor(private _apiClientService: ApiClientService,
        private _storageService: LocalStorageService) {

    }

    public hasCentres$(): Observable<boolean> {
        return this
            .getCentres$()
            .map(() => {
                return this.Centres.length > 0;
            });
    }

    public getCentres$(): Observable<CentreDto[]> {
        if (this.Centres !== null)
            return Observable.of(this.Centres);

        return this.IsSysAdmin$()
            .flatMap((isSysAdmin: boolean) => isSysAdmin ? this.getCentresForSysAdmin$() : this.getCentresForCentreStaff$())
            .map(res => this.Centres = res);
    }

    public IsCentreManager$(): Observable<boolean> {
        if (this.isCentreManager !== null)
            return Observable.of(this.isCentreManager);

        return this._apiClientService
            .sysAdmin_IsCentreAdmin()
            .map(res => this.isCentreManager = res);
    }

    public IsSysAdmin$(): Observable<boolean> {
        if (this.isSystemAdmin !== null)
            return Observable.of(this.isSystemAdmin);

        return this._apiClientService
            .sysAdmin_IsSystemAdmin()
            .map(res => this.isSystemAdmin = res);
    }

    public HangfireUrl$(): Observable<string> {
        if (this.HangfireUrl !== null)
            return Observable.of(this.HangfireUrl);

        return this._apiClientService
            .sysAdmin_HangfireUrl()
            .map(res => this.HangfireUrl = res);
    }

    public SwaggerUrl$(): Observable<string> {
        if (this.SwaggerUrl !== null)
            return Observable.of(this.SwaggerUrl);

        return this._apiClientService
            .sysAdmin_SwaggerUrl()
            .map(res => this.SwaggerUrl = res);
    }

    public GetCentreName(centreId: string): string {
        let centre: CentreDto = this.Centres.find(c => c.id === centreId);
        let centreName: string = centre !== undefined ? centre.name : '';
        return centreName;
    }

    public getCentreLogo$(centreId: string): Observable<CentreLogoDto> {

        if (this.logoDto !== null) {
            return Observable.of(this.logoDto);
        }
        return this._apiClientService
            .centre_GetCentreLogo(centreId)
            .map(res => {
                this.logoDto = res;
                this.logoDto.imageBase64 = this.ImageEncoding + this.logoDto.imageBase64;
            })
            .map(() => this.logoDto);
    }

    public Clear(): void {
        this._userId = null;
        this._email = null;
        this._centres = null;
        this._selectedCentre = null;
        this.isSystemAdmin = null;
        this.isCentreManager = null;
        this.logoDto = null;
    }

    public SetLastSelectedCentreId(centreId: string): void {
        this._storageService.set(CentreUserService.LAST_SELECTED_CENTRE + this.UserId, centreId);
    }

    public GetLastSelectedCentreId(): string {
        return this._storageService.get<string>(CentreUserService.LAST_SELECTED_CENTRE + this.UserId);
    }

    public searchCentres(searchTerm: string): Observable<CentreDto[]> {
        return this._apiClientService.sysAdmin_SearchCentres(searchTerm, 50);
    }

    private getCentresForCentreStaff$(): Observable<CentreDto[]> {
        return this._apiClientService
            .centre_GetCentresForUserAsync();
    }

    private getCentresForSysAdmin$(): Observable<CentreDto[]> {
        // There is currently no way in nswag to pass in optional parameters.
        // Planned solution will be delivered in an unknown time:
        // https://github.com/NSwag/NSwag/issues/608
        return this._apiClientService.sysAdmin_GetCentres('', <Status>{});
    }
}
