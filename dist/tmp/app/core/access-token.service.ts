import { Injectable } from '@angular/core';

import { ParentUserService } from './index';
import { LocalStorageService } from 'angular-2-local-storage';

@Injectable()
export class AccessTokenService {

    public static readonly SUBJECT_ID = 'as:sid';
    public static readonly USERNAME = 'userName';
    private static ACCESS_TOKEN: string = 'access_token';
    private static REFRESH_TOKEN: string = 'refresh_token';
    private static TOKEN_EXPIRATION = '.expires';

    private _isParent: boolean = false;
    get IsParent(): boolean {
        return this._isParent;
    }

    get CentreName(): string {
        return this._storageService.get<string>(ParentUserService.LAST_SELECTED_CENTRE);
    }

    constructor(private _storageService: LocalStorageService) {
    }

    public saveAccessToken(tokenJson: any, isParent: boolean) {
        this._storageService.set(AccessTokenService.ACCESS_TOKEN, tokenJson[AccessTokenService.ACCESS_TOKEN]);
        this._storageService.set(AccessTokenService.REFRESH_TOKEN, tokenJson[AccessTokenService.REFRESH_TOKEN]);
        this._storageService.set(AccessTokenService.TOKEN_EXPIRATION, tokenJson[AccessTokenService.TOKEN_EXPIRATION]);
        this._storageService.set(AccessTokenService.SUBJECT_ID, tokenJson[AccessTokenService.SUBJECT_ID]);
        this._storageService.set(AccessTokenService.USERNAME, tokenJson[AccessTokenService.USERNAME]);

        if (isParent !== undefined && isParent !== null)
            this._isParent = isParent;
        else
            this._isParent = false;
    }

    public getAccessToken(): string {
        let token: string = this._storageService.get<string>(AccessTokenService.ACCESS_TOKEN);
        return token;
    }

    public hasValidAccessToken(): boolean {

        if (this.getAccessToken()) {
            return !this.hasTokenExpired();
        }
        return false;
    }

    public hasTokenExpired(): boolean {
        let expires = this._storageService.get(AccessTokenService.TOKEN_EXPIRATION);
        let expiresAt = new Date(expires);
        var now = new Date();
        if (expiresAt.getTime() < now.getTime()) {
            return true;
        }
        return false;
    }

    public getRefreshToken() {
        let refreshToken: string = this._storageService.get<string>(AccessTokenService.REFRESH_TOKEN);
        return refreshToken;
    }

    public clearStorage() {
        this._storageService.remove(AccessTokenService.ACCESS_TOKEN);
        this._storageService.remove(AccessTokenService.REFRESH_TOKEN);
        this._storageService.remove(AccessTokenService.TOKEN_EXPIRATION);
        this._storageService.remove(AccessTokenService.SUBJECT_ID);
        this._storageService.remove(AccessTokenService.USERNAME);
    }

    public logout() {
        this.clearStorage();
        this._isParent = null;
    }
}
