import {AbstractControl} from '@angular/forms';

export class ComplexPasswordValidator {

  public static validate(c:AbstractControl) {
    let PASSWORD_REGEXP = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d.*)(?=.*\W.*)[a-zA-Z0-9\S]{8,50}$/;

    return PASSWORD_REGEXP.test(c.value) ? null : {
      PasswordComplex: {
        valid: false
      }
    };
  }
}
