import { AbstractControl } from '@angular/forms';

export class MobileValidator {

  public static validate(c: AbstractControl) {
    let MOBILE_REGEXP = /^(04(\d\s?){8})$/i;

    if (c.value === '' || c.value === null || c.value === undefined)
      return null;

    return MOBILE_REGEXP.test(c.value) ? null : {
      validateMobile: {
        valid: false
      }
    };
  }
}
