import { Injectable } from '@angular/core';
import {
    CanActivate, Router,
    ActivatedRouteSnapshot,
    RouterStateSnapshot
} from '@angular/router';

import { AccessTokenService } from '../../../core/index';
import { Config } from '../../../shared/index';

@Injectable()
export class ParentAuthGuardService implements CanActivate {

    constructor(private _authService: AccessTokenService, private router: Router) {

    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        if (Config.ENABLE_AUTHORISATION === 'false') {
            return true;
        }

        if (this._authService.hasValidAccessToken()) {
            return true;
        }

        // Navigate to the login page
        this.router.navigate(['parent-login', this._authService.CentreName]);
        return false;
    }
}
