import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {
    CanActivate, CanActivateChild,
    Router, ActivatedRouteSnapshot,
    RouterStateSnapshot
} from '@angular/router';

import { Config } from '../../../shared/index';
import { CentreUserService } from '../../../core/index';

@Injectable()
export class SysAdminGuardService implements CanActivate, CanActivateChild {

    constructor(private _userService: CentreUserService, private router: Router) {

    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {

        if (Config.ENABLE_AUTHORISATION === 'false') {
            return Observable.of(true);
        }

        return this._userService
            .IsSysAdmin$()
            .flatMap(isAdmin => {
                if (isAdmin === true) {
                    return Observable.of(true);
                } else {
                    return Observable.of(false);
                }
            });
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.canActivate(route, state);
    }
}
