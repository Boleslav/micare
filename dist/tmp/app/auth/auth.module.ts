import { NgModule, ModuleWithProviders } from '@angular/core';
import { SharedModule } from '../shared/index';
import { CoreModule } from '../core/index';

import {
    SysAdminGuardService, CentreAuthGuardService, ParentAuthGuardService
} from './index';

@NgModule({
    imports: [
        SharedModule,
        CoreModule
    ]
})

export class AuthModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [SysAdminGuardService,
                CentreAuthGuardService, ParentAuthGuardService]
        };
    }
}
