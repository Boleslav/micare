import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { APP_BASE_HREF } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { routes } from './app.routes';

import { CentrePortalModule } from './pages/centre-portal/centre-portal.module';
import { ParentsPortalModule } from './pages/parents-portal/parents-portal.module';
import { PublicModule } from './pages/public/index';
import { SharedModule } from './shared/index';
import { CoreModule } from './core/core.module';
import { AuthModule } from './auth/index';
import { ApiModule } from './api/index';

import { LocalStorageService/*, LOCAL_STORAGE_SERVICE_CONFIG*/ } from 'angular-2-local-storage';
import { MixpanelService, MixpanelGuard } from './shared/mixpanel/index';
import { IntercomService } from './shared/intercom/index';
import { CentreUserService } from './core/index';
import {HangFireFactory} from "./http.factory";
import {SwaggerFactory} from "./swagger.factory";

@NgModule({
	imports: [
		BrowserModule,
		FormsModule,
		HttpModule,
		RouterModule.forRoot(routes),
		CoreModule,
		SharedModule.forRoot(),
		AuthModule.forRoot(),
		ApiModule.forRoot(),
		CentrePortalModule,
		ParentsPortalModule,
		PublicModule
	],
	declarations: [AppComponent],
	providers: [
		{
			provide: APP_BASE_HREF,
			useValue: '<%= APP_BASE %>'
		},
		LocalStorageService,
		{
			provide: {},
			useValue: { prefix: 'miswap' }
		},
		{
			provide: 'hangfireUrlRedirectResolver',
			useFactory: HangFireFactory,
			deps: [CentreUserService]
		},
		{
			provide: 'swaggerUrlRedirectResolver',
			useFactory: SwaggerFactory,
			deps: [CentreUserService]
		},
		MixpanelService,
		MixpanelGuard,
		IntercomService
	],
	bootstrap: [AppComponent]

})

export class AppModule { }
