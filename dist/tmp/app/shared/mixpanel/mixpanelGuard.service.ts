import { Injectable } from '@angular/core';
import {
    CanActivate, CanActivateChild,
    ActivatedRouteSnapshot, RouterStateSnapshot
} from '@angular/router';

import { MixpanelService } from './index';

@Injectable()
export class MixpanelGuard implements CanActivate, CanActivateChild {

    constructor(private _mixpanelService: MixpanelService) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        this._mixpanelService.track(state.url);
        return true;
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        return this.canActivate(route, state);
    }
}
