import { Component, Input } from '@angular/core';

@Component({
	moduleId: module.id,
	selector: 'page-heading-cmp',
	templateUrl: 'page-heading.html'
})

export class PageHeadingComponent {
    @Input() pageDisplayName: string;
}
