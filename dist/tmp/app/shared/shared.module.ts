import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BsDropdownModule, DatepickerModule, ModalModule, AccordionModule, TypeaheadModule } from 'ng2-bootstrap';
import { NgUploaderModule } from 'ngx-uploader';

import { NavigationGuardService } from './navigationGuard/navigationGuard.service';
import { RolesGuardService } from './rolesGuard/rolesGuard.service';
import { NameListService } from './name-list/index';
import { ToasterModule } from 'angular2-toaster';
import { Ng2SmartTableModule } from './ng2-smart-table/ng2-smart-table.module';
import { SpinnerComponent } from './spinner/index';
import { RedirectComponent } from './redirect/index';
import { PageHeadingComponent } from './page-heading/index';
import { PageHeadingParentPortalComponent } from './page-heading-parent-portal/index';
import { PictureUploaderComponent } from './picture-uploader/picture-uploader.component';
import { DateValueAccessorModule } from 'angular-date-value-accessor';

/**
* Do not specify providers for modules that might be imported by a lazy loaded module.
*/

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        BsDropdownModule.forRoot(),
        DatepickerModule.forRoot(),
        ModalModule.forRoot(),
        AccordionModule.forRoot(),
        TypeaheadModule.forRoot(),
        NgUploaderModule
    ],
    declarations: [SpinnerComponent, RedirectComponent, PageHeadingComponent, PageHeadingParentPortalComponent, PictureUploaderComponent],
    entryComponents: [SpinnerComponent],
    exports: [
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        BsDropdownModule,
        DatepickerModule,
        ModalModule,
        AccordionModule,
        TypeaheadModule,
        ToasterModule,
        Ng2SmartTableModule,
        SpinnerComponent,
        RedirectComponent,
        PageHeadingComponent,
        PageHeadingParentPortalComponent,
        PictureUploaderComponent,
        NgUploaderModule,
        DateValueAccessorModule
    ]
})

export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [NameListService, NavigationGuardService, RolesGuardService]
        };
    }
}
