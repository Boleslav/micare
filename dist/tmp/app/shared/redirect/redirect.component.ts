import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
    selector: '',
    template: ''
})
export class RedirectComponent implements OnInit {

    /**
     * Angular 2 re-uses components, therefore switching between centres
     * means the component will not reload the centre information.
     * Current proposed solution is to use a redirect component to force a page refresh
     * See https://github.com/angular/angular/issues/9811
     */
    constructor(private _router: Router,
        private _route: ActivatedRoute) {

    }

    ngOnInit() {
        this._route.params.subscribe((params: Params) => {
            let redirectTo: string = params['redirectTo'];
            this._router.navigate([redirectTo]);
        });
    }
}
