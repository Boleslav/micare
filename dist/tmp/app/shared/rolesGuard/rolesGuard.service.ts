import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {
    CanActivate, Router,
    ActivatedRouteSnapshot,
    RouterStateSnapshot
} from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { CentreUserService } from '../../core/index';

@Injectable()
export class RolesGuardService implements CanActivate {

    constructor(private _userService: CentreUserService, private _toasterService: ToasterService, private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {

        return Observable.forkJoin([
            this._userService.IsSysAdmin$(),
            this._userService.IsCentreManager$()
        ])
            .flatMap(res => {
                let isSysAdmin: boolean = res[0];
                let isCentreAdmin: boolean = res[1];

                if (isSysAdmin) {
                    // if (state.url !== '/centre-portal/admin-dashboard') {
                    //     this.router.navigate(['centre-portal/admin-dashboard']);
                    //     return Observable.of(false);
                    // } else {
                        return this.allowRedirection();
                    // }
                } else if (isCentreAdmin) {
                    return this.allowRedirection();
                } else {
                    return this.redirectToLogin$();
                }
            }).catch(err => {
                return this.redirectToLogin$();
            });
    }

    private allowRedirection(): Observable<boolean> {
        this._toasterService.clear();
        return Observable.of(true);
    }

    private redirectToLogin$(): Observable<boolean> {
        this._toasterService.clear();
        this._toasterService.pop('error', '', 'Not authorised to access the Centre Admin Portal');
        this._userService.Clear();
        this.router.navigate(['login']);
        return Observable.of(false);
    }
}
