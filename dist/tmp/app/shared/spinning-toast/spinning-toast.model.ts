import { Toast, BodyOutputType } from 'angular2-toaster';
import { SpinnerComponent } from '../index';


export class SpinningToast implements Toast {
    public type: string = 'info';
    public title: string = 'Saving';
    public timeout: number = 0;
    public body: any = SpinnerComponent;
    public bodyOutputType: BodyOutputType = BodyOutputType.Component;

    constructor(message?: string) {
        if (message !== undefined)
            this.title = message;
    }
}
