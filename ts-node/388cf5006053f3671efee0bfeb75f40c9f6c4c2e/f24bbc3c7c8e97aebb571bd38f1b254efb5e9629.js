"use strict";
var gulp = require('gulp');
var config_1 = require('../../config');
module.exports = function () {
    return gulp.src(config_1.default.FONTS_SRC)
        .pipe(gulp.dest(config_1.default.FONTS_DEST));
};
//# sourceMappingURL=/Users/brendoh/Downloads/SB-Admin-BS4-Angular-2-master/ts-node/388cf5006053f3671efee0bfeb75f40c9f6c4c2e/f24bbc3c7c8e97aebb571bd38f1b254efb5e9629.js.map