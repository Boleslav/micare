"use strict";
var browserSync = require('browser-sync');
var config_1 = require('../../config');
var runServer = function () {
    browserSync.init(config_1.default.getPluginConfig('browser-sync'));
};
var listen = function () {
    runServer();
};
exports.listen = listen;
var changed = function (files) {
    if (!(files instanceof Array)) {
        files = [files];
    }
    browserSync.reload(files);
};
exports.changed = changed;
//# sourceMappingURL=/Users/brendoh/Downloads/SB-Admin-BS4-Angular-2-master/ts-node/388cf5006053f3671efee0bfeb75f40c9f6c4c2e/fc0d6f83d2a9542f21a8b57360a7f61a2f641f89.js.map