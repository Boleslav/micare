# MiSwap Centre Admin UI Project

Simple Dashboard Admin App built using Angular 2 and Bootstrap 4

##[Demo](http://rawgit.com/start-angular/SB-Admin-BS4-Angular-2/master/dist/prod/)

#Getting Started
What you'll need:
1. NodeJs (**Note** that this seed project requires node v6.9.x or higher and npm 2.14.7)
2. VS code

In order to start the project, open a new command line:

```bash
git clone https://patientzero.visualstudio.com/DefaultCollection/MiCare%20API/_git/MiCare%20Admin%20UI
# Go into the folder 
cd MiCare%20Admin%20UI
# install/update npm
npm install npm@latest -g
# install the project's dependencies
npm install
```

#Launch the Portal
1. As a pre-requisite you will have to launch the .NET Web Api project first. In Visual Studio 2017, launch the MiCareApi.Web project (as described in the [MiCare Api README](https://patientzero.visualstudio.com/MiCare%20API)).
2. Then, run the following command in the command line:
```bash
npm start
```
3. A new browser tab should open. In order to create your first user, click on the 'Register' button below the Password textbox:
![alt text](README Assets\images\MiSwapLogin.PNG "MiSwap Login")
4. Fill out all the required information. You can choose any Email address you like, as this will be intercepted by the local smtp server (Papercut).
5. Once you clicked 'Create an Account', you can open Papercut (installation described in [MiCare Api README](https://patientzero.visualstudio.com/MiCare%20API)). You should see the following email in your inbox:
![alt text](README Assets\images\MiSwapRegister.PNG "MiSwap Register")
![alt text](README Assets\images\MiSwapRegister2.PNG "MiSwap Register Completed")
![alt text](README Assets\images\RegistrationEmail.PNG "MiSwap Registration Email")
6. Due to the API project running on a separate web server (IIS) to the UI project (lite-server), you will have to change the protocol and port in the verification link. i.e. the link:
**https**://localhost:**44321**/public/verify-account-client-admin/8ca416e568944f99a67df8635a8046d6
Should change to (notice the change to http and port 5555):
**http**://localhost:**5555**/public/verify-account-client-admin/8ca416e568944f99a67df8635a8046d6
7. Copy the altered url into the web broswer address bar. Your Registration process should now be complete, and you can login to the portal:
![alt text](README Assets\images\MiSwapEmailVerified.PNG "MiSwap Email Verified")
![alt text](README Assets\images\MiSwapLoginVerified.PNG "MiSwap Login")
8. You're now a centre administrator in the MiSwap portal. Go ahead and create your first centre!

### Configuration

Default application server configuration

```js
var PORT             = 5555;
var LIVE_RELOAD_PORT = 4002;
var DOCS_PORT        = 4003;
var APP_BASE         = '/';
```

## Environment configuration

If you have different environments and you need to configure them to use different end points, settings, etc. you can use the files `dev.ts` or `prod.ts` in`./tools/env/`. The name of the file is environment you want to use.

The environment can be specified by using:


### Directory Structure

```
├── LICENSE
├── README.md
├── gulpfile.ts                <- configuration of the gulp tasks
├── karma.conf.js              <- configuration of the test runner
├── package.json               <- dependencies of the project
├── protractor.conf.js         <- e2e tests configuration
├── src                        <- source code of the application
│   └── client
│       ├── app
│       │   ├── login
│       │   │   ├── login.component.css
│       │   │   ├── login.component.e2e-spec.ts
│       │   │   ├── login.component.html
│       │   │   ├── login.component.spec.ts
│       │   │   ├── login.component.ts
│       │   │   ├── login.module.ts
│       │   │   ├── login.routes.ts
│       │   │   └── index.ts
│       │   ├── dashboard
│       │   │   ├── home
│       │   │   ├── bg-component
│       │   │   ├── blankpage
│       │   │   ├── dashboard.component.css
│       │   │   ├── dashboard.component.e2e-spec.ts
│       │   │   ├── dashboard.component.html
│       │   │   ├── dashboard.component.spec.ts
│       │   │   ├── dashboard.component.ts
│       │   │   ├── dashboard.module.ts
│       │   │   ├── dashboard.routes.ts
│       │   │   └── index.ts
│       │   ├── app.component.html
│       │   ├── app.component.ts
│       │   ├── app.module.ts
│       │   ├── app.routes.ts
│       │   ├── main.ts
│       │   └── shared
│       │       ├── config
│       │       │   └── env.config.ts
│       │       ├── index.ts
│       │       ├── shared.module.ts
│       │       ├── name-list
│       │       │   ├── index.ts
│       │       │   ├── name-list.service.spec.ts
│       │       │   └── name-list.service.ts
│       │       ├── sidebar
│       │       │   ├── index.ts
│       │       │   ├── sidebar.component.html
│       │       │   └── sidebar.component.ts
│       ├── assets
│       │   ├── bootstrap
│       │   └── images
│       │   └── sass
│       ├── testing
│       ├── index.html
│       ├── tsconfig.json
│       └── typings.d.ts
├── test-main.js               <- testing configuration
├── tools
│   ├── README.md              <- build documentation
│   ├── config
│   │   ├── project.config.ts  <- configuration of the specific project
│   │   ├── seed.config.interfaces.ts
│   │   └── seed.config.ts     <- generic configuration of the seed project
│   ├── config.ts              <- exported configuration (merge both seed.config and project.config, project.config overrides seed.config)
│   ├── debug.ts
│   ├── env                    <- environment configuration
│   │   ├── base.ts
│   │   ├── dev.ts
│   │   └── prod.ts
│   ├── manual_typings
│   │   ├── project            <- manual ambient typings for the project
│   │   │   └── sample.package.d.ts
│   │   └── seed               <- seed manual ambient typings
│   │       ├── angular2-hot-loader.d.ts
│   │       ├── autoprefixer.d.ts
│   │       ├── colorguard.d.ts
│   │       ├── connect-livereload.d.ts
│   │       ├── cssnano.d.ts
│   │       ├── doiuse.d.ts
│   │       ├── express-history-api-fallback.d.ts
│   │       ├── istream.d.ts
│   │       ├── karma.d.ts
│   │       ├── merge-stream.d.ts
│   │       ├── open.d.ts
│   │       ├── postcss-reporter.d.ts
│   │       ├── slash.d.ts
│   │       ├── stylelint.d.ts
│   │       ├── systemjs-builder.d.ts
│   │       ├── tildify.d.ts
│   │       ├── tiny-lr.d.ts
│   │       └── walk.d.ts
│   ├── tasks                  <- gulp tasks
│   │   ├── project            <- project specific gulp tasks
│   │   │   └── sample.task.ts
│   │   └── seed               <- seed generic gulp tasks. They can be overriden by the project specific gulp tasks
│   │       ├── build.assets.dev.ts
│   │       ├── build.assets.prod.ts
│   │       ├── build.bundles.app.ts
│   │       ├── build.bundles.ts
│   │       ├── build.docs.ts
│   │       ├── build.html_css.ts
│   │       ├── build.index.dev.ts
│   │       ├── build.index.prod.ts
│   │       ├── build.js.dev.ts
│   │       ├── build.js.e2e.ts
│   │       ├── build.js.prod.ts
│   │       ├── build.js.test.ts
│   │       ├── build.js.tools.ts
│   │       ├── check.versions.ts
│   │       ├── clean.all.ts
│   │       ├── clean.coverage.ts
│   │       ├── clean.dev.ts
│   │       ├── clean.prod.ts
│   │       ├── clean.tools.ts
│   │       ├── copy.js.prod.ts
│   │       ├── css-lint.ts
│   │       ├── e2e.ts
│   │       ├── generate.manifest.ts
│   │       ├── karma.start.ts
│   │       ├── serve.coverage.ts
│   │       ├── serve.docs.ts
│   │       ├── server.prod.ts
│   │       ├── server.start.ts
│   │       ├── tslint.ts
│   │       ├── watch.dev.ts
│   │       ├── watch.e2e.ts
│   │       ├── watch.test.ts
│   │       └── webdriver.ts
│   ├── utils                  <- build utils
│   │   ├── project            <- project specific gulp utils
│   │   │   └── sample_util.ts
│   │   ├── project.utils.ts
│   │   ├── seed               <- seed specific gulp utils
│   │   │   ├── clean.ts
│   │   │   ├── code_change_tools.ts
│   │   │   ├── server.ts
│   │   │   ├── tasks_tools.ts
│   │   │   ├── template_locals.ts
│   │   │   ├── tsproject.ts
│   │   │   └── watch.ts
│   │   └── seed.utils.ts
│   └── utils.ts
├── tsconfig.json              <- configuration of the typescript project (ts-node, which runs the tasks defined in gulpfile.ts)
├── tslint.json                <- tslint configuration
├── typings                    <- typings directory. Contains all the external typing definitions defined with typings
├── typings.json
└── appveyor.yml
```